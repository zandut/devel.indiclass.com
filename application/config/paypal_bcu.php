<?php
/** set your paypal credential **/

$config['client_id'] = 'Ae7qrQwyvHGAivJvQV8yEki05ArblKh6Q-iPzINnq2oNhqKla44PwWYl2WuIHR083q-6wIR9c3v1a1dD';
$config['secret'] = 'EOsfSckEkydSdA6xR5o0Fn2HUpmTwmFqFwmQ8FlUObPNJ5-ehC5egbWcOyaSPwHLdBISofiDObneFm5D';

/**
 * SDK configuration
 */
/**
 * Available option 'sandbox' or 'live'
 */
$config['settings'] = array(

    'mode' => 'sandbox',
    /**
     * Specify the max request time in seconds
     */
    'http.ConnectionTimeOut' => 1000,
    /**
     * Whether want to log to a file
     */
    'log.LogEnabled' => true,
    /**
     * Specify the file that want to write on
     */
    'log.FileName' => 'application/logs/paypal.log',
    /**
     * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
     *
     * Logging is most verbose in the 'FINE' level and decreases as you
     * proceed towards ERROR
     */
    'log.LogLevel' => 'FINE'
);