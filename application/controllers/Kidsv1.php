<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH."/libraries/REST_Controller.php";
// require APPPATH."/libraries/jwt/JWT.php";
// require APPPATH."/libraries/jwt/BeforeValidException.php";
// require APPPATH."/libraries/jwt/ExpiredException.php";
// require APPPATH."/libraries/jwt/SignatureInvalidException.php";

use \Firebase\JWT\JWT;

class Kidsv1 extends REST_Controller {

	public $sesi = array('nama' => null,'username' => null,'priv_level' => null);
	public $janus_server = "c2.classmiles.com";
	public $DB1 = null;
	
	public function __construct()
	{
		parent::__construct();
		
		// $this->load->database('default');
		// $this->DB1 = $this->load->database('db2', TRUE);
	}

	public function main_get($value='')
	{
		echo "Welcome to Kids Classmiles v1 ! Disini Dokumentasi nya yaa..";
	}

	public function login_post()
	{
		if($this->post('username') != '' && $this->post('password') != ''){
			$un = htmlentities($this->post('username'));
			$ps = htmlentities($this->post('password'));
			$data = $this->db->query("SELECT * FROM tbl_user WHERE username = '$un' && usertype_id = 'student kid' ")->row_array();
			// print_r($un+"|"+$ps);
			if(!empty($data) && password_verify($ps, $data['password']) ){
				$ids = $data['id_user'];
				$data_profile = $this->db->query("SELECT * FROM tbl_profile_kid WHERE kid_id = '$ids'")->row_array();
				$rets['status'] = true;
				$rets['code'] = 1;
				$rets['message'] = "Login Successful";
				$rets['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "usertype_id" => $data['usertype_id'], "status" => $data['status'], 'jenjang_id' => $data_profile['jenjang_id'], 'school_name' => $data_profile['school_name'], 'school_address' => $data_profile['school_address'], 'parent_id' => $data_profile['parent_id']);
				$access_token = $this->create_access_token( array_merge($data, $data_profile));
				$rets['access_token'] = $access_token;
				$this->response($rets);
			}
			$rets['status'] = false;
			$rets['code'] = 0;
			$rets['message'] = "Wrong username or password.";
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['code'] = 0;
		$rets['message'] = "Field incomplete.";
		$this->response($rets);
	}

	public function schedule_post()
	{

		/*$now = date('N');
		$nextMon = date('Y-m-d', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d')))); 
		$oneWeek = date('Y-m-d', strtotime('+6 day', strtotime($nextMon)));

		if($this->post('id_user') != ''){
			$ids = $this->post('id_user');
			$res = $this->db->query("SELECT tc.class_id, tc.participant, tc.subject_id,tc.name as subject_name, tc.description, tc.tutor_id, tu.user_name, tu.user_image, LEFT(tc.start_time,10) as date, SUBSTR(tc.start_time,12,5) as time, SUBSTR(tc.finish_time,12,5) as endtime FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN tbl_booking as tb ON tc.tutor_id=tb.tutor_id where tb.subject_id=tc.subject_id AND tb.id_user=$ids AND start_time>='$nextMon' AND finish_time<='$oneWeek' order by start_time ASC")->result_array();
			if(!empty($res)){
				$rets['status'] = true;
				$rets['message'] = "Succeed";
                foreach ($res as $key => $value) {
					$res[$key]['participant'] = json_decode($value['participant'],true);
				}
				$rets['data'] = $res;
				$this->response($rets);
			}else{
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete".$this->post('id_user');
		$this->response($rets);*/

		$date = $this->post('date');
		$user_utc = $this->post('user_utc');
		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";

		$now = date('N');
		$server_utc = $this->Rumus->getGMTOffset();
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		$interval   = $user_utc - $server_utc;

		/*$nextMon = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
		$nextMon->modify("+$interval minute");*/
		// $todayInClient = $date." 00:00:00";
		// $enddayInClient = $date." 23:59:59";
		$todayInClient = date($date.' 00:00:00', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
		$enddayInClient = date('Y-m-d 23:59:59', strtotime('+6 day', strtotime($todayInClient)));

		$nextMon = DateTime::createFromFormat('Y-m-d H:i:s',$todayInClient);
		$nextMon->modify("-$interval minute");
		$nextMon = $nextMon->format('Y-m-d H:i:s');

		$oneWeek = DateTime::createFromFormat('Y-m-d H:i:s',$enddayInClient);
		$oneWeek->modify("-$interval minute");
		$oneWeek = $oneWeek->format('Y-m-d H:i:s');

		if($this->post('id_user') != ''){
			$ids = $this->post('id_user');
			$res = $this->db->query("SELECT tc.class_id,tc.participant, tc.subject_id,tc.name as subject_name, tc.description, tc.tutor_id, tu.user_name, tu.first_name, tc.template_type, tc.class_type, mc.nicename as country, tu.user_image, tcp.harga, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN master_country as mc ON mc.nicename=tu.user_nationality INNER JOIN tbl_booking as tb ON tc.tutor_id=tb.tutor_id AND tc.subject_id=tb.subject_id AND tb.id_user=$ids LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id WHERE start_time>='$nextMon' AND finish_time<='$oneWeek'order by start_time ASC")->result_array();
			if(!empty($res)){
				$rets['status'] = true;
				$rets['message'] = "Succeed";
                foreach ($res as $key => $value) {
					$res[$key]['participant'] = json_decode($value['participant'],true);
				}
				$rets['data'] = $res;
				$this->response($rets);
			}else{
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$rets['qu'] = "SELECT tc.class_id,tc.participant, tc.subject_id,tc.name as subject_name, tc.description, tc.tutor_id, tu.user_name, tu.first_name, tc.template_type, tc.class_type, mc.nicename as country, tu.user_image, tcp.harga, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN master_country as mc ON mc.nicename=tu.user_nationality INNER JOIN tbl_booking as tb ON tc.tutor_id=tb.tutor_id AND tc.subject_id=tb.subject_id AND tb.id_user=$ids LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id WHERE start_time>='$nextMon' AND finish_time<='$oneWeek'order by start_time ASC";
				$this->response($rets);
			}
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete".$this->post('id_user');

		$this->response($rets);
	}

	public function scheduleone_post($value='')
	{
		$date = $this->post('date');
		$user_utc = $this->post('user_utc');
		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";
		$retok = $this->verify_access_token($this->get('access_token'));

		$now = date('N');
		// $thisBeginDay = date($date.' 00:00:00', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
		// $oneDayAfter = date($date.' 23:59:59', strtotime('+'.$now.' day', strtotime(date('Y-m-d'))));
		$server_utc = $this->Rumus->getGMTOffset();
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		$interval   = $user_utc - $server_utc;

		/*$thisBeginDay = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
		$thisBeginDay->modify("+$interval minute");*/
		$todayInClient = $date." 00:00:00";
		$enddayInClient = $date." 23:59:59";

		$thisBeginDay = DateTime::createFromFormat('Y-m-d H:i:s',$todayInClient);
		$thisBeginDay->modify("-$interval minute");
		$thisBeginDay = $thisBeginDay->format('Y-m-d H:i:s');

		$thisEndDay = DateTime::createFromFormat('Y-m-d H:i:s',$enddayInClient);
		$thisEndDay->modify("-$interval minute");
		$thisEndDay = $thisEndDay->format('Y-m-d H:i:s');

		$oneDayAfter = DateTime::createFromFormat('Y-m-d H:i:s',$thisEndDay);
		$oneDayAfter->modify("+1 day");
		$oneDayAfter = $oneDayAfter->format('Y-m-d H:i:s');

		$oneDayBefore = DateTime::createFromFormat('Y-m-d H:i:s',$thisBeginDay);
		$oneDayBefore->modify("-1 day");
		$oneDayBefore = $oneDayBefore->format('Y-m-d H:i:s');

		if($this->post('id_user') != ''){
			// $ids = $this->post('id_user');

			if($retok == false){
				$rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
				goto ending;
			}
			$ids = $retok['id_user'];

			// QUERY BY FOLLOW
			$res = $this->db->query("SELECT tc.class_id,tc.participant, tc.subject_id,tc.name as subject_name, tc.description, tc.tutor_id, tu.user_name, tu.first_name, tc.template_type, tc.class_type, mc.nicename as country, tu.user_image, tcp.harga, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN master_country as mc ON mc.nicename=tu.user_nationality INNER JOIN tbl_booking as tb ON tc.tutor_id=tb.tutor_id AND tc.subject_id=tb.subject_id AND tb.id_user=$ids LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id WHERE  ( ( start_time>='$thisBeginDay' AND start_time<'$thisEndDay' AND finish_time<='$oneDayAfter' ) || ( start_time>='$oneDayBefore' AND finish_time>'$thisBeginDay' AND finish_time<='$thisEndDay' ) ) order by start_time ASC")->result_array();

			$res2 = $this->db->query("SELECT tc.class_id,tc.participant, tc.subject_id,tc.name as subject_name, tc.description, tc.tutor_id, tu.user_name, tu.first_name, tc.template_type, tc.class_type, mc.nicename as country, tu.user_image, tcp.harga, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN master_country as mc ON mc.nicename=tu.user_nationality  LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id WHERE tc.participant LIKE '%\"id_user\":\"$ids\"%' AND $ids NOT IN (SELECT tb.id_user FROM tbl_booking as tb WHERE tb.tutor_id=tc.tutor_id AND tb.subject_id=tc.subject_id AND tb.id_user=$ids)  AND ( ( start_time>='$thisBeginDay' AND start_time<'$thisEndDay' AND finish_time<='$oneDayAfter' ) || ( start_time>='$oneDayBefore' AND finish_time>'$thisBeginDay' AND finish_time<='$thisEndDay' ) ) order by start_time ASC")->result_array();
			// ( ) AND
			 // INNER JOIN tbl_booking as tb ON tc.tutor_id=tb.tutor_id AND tc.subject_id=tb.subject_id AND tb.id_user=$ids
			// return null;
			if(!empty($res) || !empty($res2)){
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$new_arr = array();
				if(!empty($res)){
					foreach ($res as $key => $value) {
						$res[$key]['participant'] = json_decode($value['participant'],true);
						array_push($new_arr, $res[$key]);
					}
				}
                
				if(!empty($res2)){
					foreach ($res2 as $key => $value) {
						$res2[$key]['participant'] = json_decode($value['participant'],true);
						array_push($new_arr, $res2[$key]);
					}
				}
				function date_compare($a, $b)
				{
				    $t1 = strtotime($a['date']." ".$a['time']);
				    $t2 = strtotime($b['date']." ".$b['time']);
				    return $t1 - $t2;
				}    
				usort($new_arr, 'date_compare');


				$rets['data'] = $new_arr;
				$this->response($rets);
			}else{
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete".$this->post('id_user');
		ending:
		$this->response($rets);
	}

	public function update_fcm_post()
	{
		// $id_user = htmlentities($this->post('id_user'));
		$fcm_token = htmlentities($this->post('fcm_token'));
		$time = date('Y-m-d H:i:s');
		if ($fcm_token != '' && $this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
            $id_user = $retok['id_user'];

			$q = $this->db->simple_query("INSERT INTO master_fcm VALUES('{$fcm_token}','{$id_user}','{$time}')");
			if(!$q){
				$q =$this->db->simple_query("UPDATE master_fcm SET fcm_token='{$fcm_token}', update_time='{$time}' WHERE id_user='{$id_user}'");
				$s = $this->db->affected_rows();
				if($s<1 || !$q){
					$ret['status'] = false;
					$ret['message'] = 'Error Occured!';
					$this->response($ret);
				}
			}
			$ret['status'] = true;
			$ret['message'] = 'Successful';
			$this->response($ret);
		}
		$ret['status'] = false;
		$ret['message'] = "Field incomplete.";
		ending:
		$this->response($ret);
		


	}

	public function get_announcement_post()
	{
		$rets = array();
		$id_user = $this->post('id_user');
		$date = date("Y-m-d");
		if ($id_user != "" && $this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                // $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

			$dataget = $this->db->query("SELECT * FROM tbl_notif WHERE id_user = '$id_user' and timestamp like '$date%' ORDER BY notif_id DESC limit 1 ")->row_array();
			$datagets = $this->db->query("SELECT text_announ FROM tbl_announcement ORDER BY id DESC limit 1 ")->row_array();
			$datagetz = $this->db->query("SELECT * FROM tbl_version_app where name = 'kids' ")->row_array();

			$result = array();
			$dataget['timestamp'] = substr($dataget['timestamp'],0,10) ;
			$dataget['text_announ'] = $datagets['text_announ'];
			$dataget['version'] = $datagetz['version'];
			$dataget['force'] = $datagetz['force'];

			$rets['status'] = true;
			$rets['message'] = "Succeed";
			$rets['method'] = "GET ANNOUNCEMENT";
			$rets['data'] = $dataget;
			$this->response($rets);	
		} 
			$rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
			$rets['message'] = "Field incomplete";
			$rets['method'] = "GET ANNOUNCEMENT";
			ending:
			$this->response($rets);	
		
	}

	public function iamonline_post(){
		$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
        }

        $id_user = $this->post('id_user');
        $select_all = $this->db->query("SELECT * from temp_user_online where id_user = '".$id_user."' ")->row_array();
        if (!empty($select_all)) {
	        $res['status'] = false;
			$res['message'] = 'You in class';
			$res['code'] = 300;	
			$this->response($res);

        } else {
	        $res['status'] = true;
			$res['message'] = 'Welcome to class';
			$res['code'] = 200;	
			$this->response($res);
        }

        $res['status'] = false;
		$res['message'] = 'Field incomplete';
		$res['code'] = -300;	
		ending:
		$this->response($res);
	}

	public function submit_addt_info_post()
	{
		$res['method'] = "submit_addt_info";
		$res['transaction'] = $this->Rumus->RandomString();

		$lat = $this->post('lat');
		$lng = $this->post('lng');
		$manufacture = $this->post('manufacture');
		$model = $this->post('model');
		$api_android = $this->post('api_android');
		$mac_addrs1 = $this->post('mac_addrs1');
		$mac_addrs2 = $this->post('mac_addrs2');

		$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
            // $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
        }

		if($lat != '' && $lng != '' && $manufacture != '' && $model != '' && $api_android != ''){
			$id_user = $retok['id_user'];

			if(!$this->db->simple_query("INSERT INTO tbl_user_addt_info(id_user, lat, lng, manufacture, model, api_android, macaddrs1, macaddrs2) VALUES($id_user, $lat, $lng, '$manufacture', '$model', '$api_android', '$mac_addrs1', '$mac_addrs2')")){
				$error = $this->db->error(); // Has keys 'code' and 'message'
				$res['status'] = false;
				$res['message'] = $error['message'];
				$this->response($res);
			}

			$res['status'] = true;
			$res['message'] = 'Successful.';

			$this->response($res);
		}

		$res['status'] = false;
		$res['message'] = "Field incomplete";
		ending:
		$this->response($res);
	}

	// START SYSTEM //
	// ===================================================================================== //
	private function create_access_token($data='')
	{
		$token= array(
			// "exp" => time() + ( 60 * 60 ),
			"iss" => "kids classmiles jwt v1",
			"iat" => time(),
			"data" => $data
		);
		$jwt = JWT::encode($token, SECRET_KEY);
		return $jwt;
	}
	private function verify_access_token($access_token='')
	{
		try{
			$dc = JWT::decode($access_token, SECRET_KEY, array('HS256'));
		}catch(Exception $e){
			return false;
		}

		if(isset($dc->status) && $dc->status == false){
			return -1;
		}else{
			return json_decode(json_encode($dc->data),true);
		}
	}

	
}