<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajaxer_mb extends CI_Controller {

	public $sesi = array('user_name' => null,'username' => null,'priv_level' => null);
	
	public function __construct()
	{
		parent::__construct();

	}

	public function getPos($desa_id = null)
	{
		echo $this->Master_model->getPos($desa_id);
	}
	public function getJenjangLevel($value='')
	{

		$term = $this->input->get('term');
		$page  = $this->input->get('page');

		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		$this->db->query("SELECT jenjanglevel_name as id, jenjanglevel_name as text FROM master_jenjanglevel")->result_array();
		$res['results'] = $this->db->query("SELECT jenjanglevel_name as id, jenjanglevel_name as text FROM master_jenjanglevel WHERE jenjanglevel_name like '%".$term."%'  limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);

	}

	public function ajax_indonesia($par)
	{
		$_1 = substr($par, 0, strlen($par) - 3 );

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		$pfp = $this->input->get('pfp');
		$pfka = $this->input->get('pfka');
		$pfkec = $this->input->get('pfkec');
		$where = "";
		if($par == "kabupaten_id"){
			$where = "AND provinsi_id like '%".$pfp."%'";
		}else if($par == "kecamatan_id"){
			$where = "AND kabupaten_id='".$pfka."'";
		}else if($par == "desa_id"){
			$where = "AND kecamatan_id='".$pfkec."'";
		}
		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		$res['results'] = $this->db->query("SELECT ".$_1."_id as id,".$_1."_name as text FROM master_".$_1." WHERE ".$_1."_name like '%".$term."%' ".$where." limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}
	public function ajax_user_login()
	{

		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
			)
		{
			$dt = $_POST;
			
			$dt['col-display'] = array(
				'us.id_user',
				'us.user_name',
				'hl.logs',
				'hl.imei',
				'hl.datetime'
				);
			$sql  = "SELECT hl.id_user, us.user_name, hl.logs, hl.imei, hl.datetime FROM history_login as hl LEFT JOIN users as us ON hl.id_user=us.id_user";
			$sql1 = $sql;
			$data = $this->db->query($sql);
			$rowCount = $data->num_rows();
			$data->free_result();
        // pengkondisian aksi seperti next, search dan limit
			$columnd = $dt['col-display'];
			$count_c = count($columnd);
        // search
			$search = $dt['search']['value'];
	        /**
	         * Search Global
	         * pencarian global pada pojok kanan atas
	         */
	        $where = '';
	        if ($search != '') {   
	        	for ($i=0; $i < $count_c; $i++) {
	        		$where .= $columnd[$i] .' LIKE "%'. $search .'%"';

	        		if ($i < $count_c - 1) {
	        			$where .= ' OR ';
	        		}
	        	}
	        }
	        if($where != ''){
	        	$where = '( '.$where.' )';
	        }
	        /*if($where == ''){
	        	$where.= ' haji_yearhaji="'.TAHUN_HAJI.'"';
	        }else{
	        	$where.= ' AND haji_yearhaji="'.TAHUN_HAJI.'"';
	        }*/
	        
	        /**
	         * Search Individual Kolom
	         * pencarian dibawah kolom
	         */
	        for ($i=0; $i < $count_c; $i++) { 
	        	$searchCol = $dt['columns'][$i]['search']['value'];
	        	if ($searchCol != '') {
	        		if($where != ''){
	        			$where .= ' AND '. $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';	
	        		}else{
	        			$where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
	        		}
	        		
	        		// break;
	        	}
	        }
	        /**
	         * pengecekan Form pencarian
	         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
	         */
	        if ($where != '') {
	        	$sql .= " WHERE " . $where;

	        }
	        
        // sorting
	        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
	        
	        $list = $this->db->query($sql);
	        $rowCount2 = $list->num_rows();
	        $list->free_result();
        // limit
	        $start  = $dt['start'];
	        $length = $dt['length'];
	        if($length == -1){
	        	$length = $rowCount;
	        }
	        $sql .= " LIMIT {$start}, {$length}";
	        $list = $this->db->query($sql);
        // $vari = $this->db->escape_str(json_encode($sql));
    	// $this->db->query('INSERT INTO fail(fail) values("'.$vari.'")');
	        
	        /*$this->db->query('INSERT INTO fail(fail) values("'.$sql.'")');*/
	        /**
	         * convert to json
	         */
	        $option['draw']            = $dt['draw'];
	        $option['recordsTotal']    = $rowCount;
        // if($sql != $sql1){
	        $option['recordsFiltered'] = $rowCount2;
        // }else{
        	// $option['recordsFiltered'] = $rowCount;
        // }
	        
	        $option['data'] = array();
	        $reverse_column = array("mifare_id","imei","user_name","latitude","longitude","datetime");
	        foreach ($list->result_array() as $row) {
	        	$rows = array();

	        	// for ($i=0; $i < $count_c; $i++) {
	        	$rows[] = $row['id_user'];
	        	$rows[] = $row['user_name'];
	        	$rows[] = $row['logs'];
	        	$rows[] = $row['imei'];
	        	$rows[] = $row['datetime'];
	        	$option['data'][] = $rows;
	        	// }
	        }
        // eksekusi json
	        echo json_encode($option);
	        return null;

	    }
	}
	public function ajax_master_user($kind)
	{

		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
			)
		{
			$dt = $_POST;
			
			$dt['col-display'] = array(
				'id_user',
				'user_name',
				'user_birthdate',
				'user_birthplace',
				'user_email',
				);
			$sql  = "SELECT * FROM users";
			$sql1 = $sql;
			$data = $this->db->query($sql);
			$rowCount = $data->num_rows();
			$data->free_result();
        // pengkondisian aksi seperti next, search dan limit
			$columnd = $dt['col-display'];
			$count_c = count($columnd);
        // search
			$search = $dt['search']['value'];
	        /**
	         * Search Global
	         * pencarian global pada pojok kanan atas
	         */
	        $where = '';
	        if ($search != '') {   
	        	for ($i=0; $i < $count_c; $i++) {
	        		$where .= $columnd[$i] .' LIKE "%'. $search .'%"';

	        		if ($i < $count_c - 1) {
	        			$where .= ' OR ';
	        		}
	        	}
	        }
	        if($where != ''){
	        	$where = '( '.$where.' )';
	        }
	        /*if($where == ''){
	        	$where.= ' haji_yearhaji="'.TAHUN_HAJI.'"';
	        }else{
	        	$where.= ' AND haji_yearhaji="'.TAHUN_HAJI.'"';
	        }*/
	        
	        /**
	         * Search Individual Kolom
	         * pencarian dibawah kolom
	         */
	        for ($i=0; $i < $count_c; $i++) { 
	        	$searchCol = $dt['columns'][$i]['search']['value'];
	        	if ($searchCol != '') {
	        		if($where != ''){
	        			$where .= ' AND '. $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';	
	        		}else{
	        			$where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
	        		}
	        		
	        		// break;
	        	}
	        }
	        /**
	         * pengecekan Form pencarian
	         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
	         */
	        if ($where != '') {
	        	$sql .= " WHERE " . $where." AND id_priviledge='".$kind."'";

	        }else{
	        	$sql .= " WHERE id_priviledge='".$kind."'";	        	
	        }
	        
        // sorting
	        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
	        
	        $list = $this->db->query($sql);
	        $rowCount2 = $list->num_rows();
	        $list->free_result();
        // limit
	        $start  = $dt['start'];
	        $length = $dt['length'];
	        $sql .= " LIMIT {$start}, {$length}";
	        $list = $this->db->query($sql);
        // $vari = $this->db->escape_str(json_encode($sql));
    	// $this->db->query('INSERT INTO fail(fail) values("'.$vari.'")');
	        
	        /*$this->db->query('INSERT INTO fail(fail) values("'.$sql.'")');*/
	        /**
	         * convert to json
	         */
	        $option['draw']            = $dt['draw'];
	        $option['recordsTotal']    = $rowCount;
        // if($sql != $sql1){
	        $option['recordsFiltered'] = $rowCount2;
        // }else{
        	// $option['recordsFiltered'] = $rowCount;
        // }
	        
	        $option['data'] = array();
	        foreach ($list->result_array() as $row) {
	        	$rows = array();

	        	// for ($i=0; $i < $count_c; $i++) {
	        		$rows[] = $row['id_user'];
	        		$rows[] = $row['user_name'];
	        		$rows[] = $row['user_birthplace'].", ".$row['user_birthdate'];
	        		$rows[] = $row['user_email'];
	        		$rows[] = "<div class='col-md-6'><a href='".base_url()."first/edit_user/".$kind."/".$row['id_user']."' class='btns btn btn-info glyphicon glyphicon-pencil'></a></div><div class='col-md-6'><a class='btn btn-danger' href='".base_url()."master/delete_user/".$kind."/".$row['id_user']."' onclick='return delconf();'><i class='glyphicon glyphicon-trash'></i></a></div>";

	        	// }
	        	$option['data'][] = $rows;
	        }
        // eksekusi json
	        echo json_encode($option);
	        return null;

	    }
	}
	public function ajax_get_ondemand()
	{
		$rets = array();
		$subject_id = htmlentities($this->input->get('subject_id'));
		$date = htmlentities($this->input->get('date'));
		$time = htmlentities($this->input->get('time'));

		$jam = substr($time, 0,2)*3600;
		$menit = substr($time, 3,2)*60;
		$time = $jam + $menit;

		$offset0 = $time - 900;
		$offset1 = $time + 900;


		$list = $this->db->query("SELECT id_user FROM tbl_booking WHERE tutor_id='0' AND subject_id={$subject_id}")->result_array();

		$where_tutor = "(";
		foreach ($list as $key => $value) {
			$where_tutor.= " tav.tutor_id='".$value['id_user']."' OR";
		}
		$where_tutor = trim($where_tutor,'OR').")";


		// foreach ($list as $key => $value) {

		// }

		$total = 0;
		$new_arr = array();
		$data = $this->db->query("SELECT tav.tutor_id, tu.user_name, tav.date, tav.start_time, tav.stop_time, tu.user_image FROM tbl_avtime as tav INNER JOIN tbl_user as tu ON tav.tutor_id=tu.id_user WHERE tav.date='{$date}' AND tav.start_time <= {$time} AND tav.start_time >= {$offset0} AND tav.stop_time >= {$offset1} AND tav.filled='0' AND ".$where_tutor)->result_array();
		foreach ($data as $key => $value) {
			foreach ($data[$key] as $k => $v) {
				$new_arr[$total][$k] = $v;
				if($k == "user_image"){
					$new_arr[$total][$k] = base_url()."/aset/img/user/".$v;
				}
				if($k == "start_time"){
					$j = sprintf('%02d',$v/3600);
					$m = sprintf('%02d',($v%3600)/60);
					$new_arr[$total][$k] = $j.":".$m;
				}
			}
			$new_arr[$total]['exact'] = 1;
			$total++;
		}
		if($total < 5){
			$offset2 = $time - 900;
			$offset3 = $time - 3600 - 900;
			$offset4 = $time - 3600 + 900;
			$data2 = $this->db->query("SELECT tav.tutor_id, tu.user_name, tav.date, tav.start_time, tav.stop_time, tu.user_image FROM tbl_avtime as tav INNER JOIN tbl_user as tu ON tav.tutor_id=tu.id_user WHERE tav.date='{$date}' AND tav.start_time < {$offset2} AND tav.start_time >= {$offset3}   AND tav.stop_time >= {$offset4} AND tav.filled='0' AND ".$where_tutor)->result_array();
			foreach ($data2 as $key => $value) {
				foreach ($data2[$key] as $k => $v) {
					$new_arr[$total][$k] = $v;
					if($k == "user_image"){
						$new_arr[$total][$k] = base_url()."/aset/img/user/".$v;
					}
					if($k == "start_time"){
						$j = sprintf('%02d',$v/3600);
						$m = sprintf('%02d',($v%3600)/60);
						$new_arr[$total][$k] = $j.":".$m;
					}
				}
				$new_arr[$total]['exact'] = 0;
				$total++;
			}
			if($total <5){
				$offset2 = $time + 900;
				$offset3 = $time + 3600 + 1800;
				$offset4 = $time + 3600 + 900;
				$data2 = $this->db->query("SELECT tav.tutor_id, tu.user_name, tav.date, tav.start_time, tav.stop_time, tu.user_image FROM tbl_avtime as tav INNER JOIN tbl_user as tu ON tav.tutor_id=tu.id_user WHERE tav.date='{$date}' AND tav.start_time > {$offset2} AND tav.start_time <= {$offset4}   AND tav.stop_time >= {$offset3} AND tav.filled='0' AND ".$where_tutor)->result_array();
				foreach ($data2 as $key => $value) {
					foreach ($data2[$key] as $k => $v) {
						$new_arr[$total][$k] = $v;
						if($k == "user_image"){
							$new_arr[$total][$k] = base_url()."/aset/img/user/".$v;
						}
						if($k == "start_time"){
							$j = sprintf('%02d',$v/3600);
							$m = sprintf('%02d',($v%3600)/60);
							$new_arr[$total][$k] = $j.":".$m;
						}
					}
					$new_arr[$total]['exact'] = 0;
					$total++;
				}
			}
		}
		$rets['status'] = true;
		$rets['data'] = $new_arr;

		echo json_encode($rets);
	}
	public function ajax_jenjang_name($par='')
	{
		/*$_1 = substr($par, 0, strlen($par) - 3 );

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		$where = "";
		$resCount = 20;
		
		$offset = ($page - 1) * $resCount;*/
		$term = $this->input->get('jenjang_name');

		$res['results'] = $this->db->query("SELECT jenjang_name as id , jenjang_name as text FROM master_jenjang WHERE jenjang_name like '%".$term."%' group by jenjang_name")->result_array();
		/*$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}*/
		
		echo json_encode($res);
	}
	public function ajax_jenjang_level($par = '')
	{
		/*$_1 = substr($par, 0, strlen($par) - 3 );

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		$jname = $this->input->get('jname');
		if($jname == ''){
			$jname = 'AAAA';
		}
		$where = "";
		$resCount = 20;

		$offset = ($page - 1) * $resCount;*/

		// $term = $this->input->get('jenjang_level');
		// $jname = $this->input->get('jenjang_name');
		// if($jname == ''){
		// 	$jname = 'AAAA';
		// }
		$res = array();
		$res['results'] = $this->db->query("SELECT jenjang_id as id , CONCAT(jenjang_name,' Kelas ',jenjang_level) as text FROM master_jenjang WHERE jenjang_name NOT LIKE 'UMUM' AND curriculum ='Indonesia' AND jenjang_name NOT LIKE 'SD' AND jenjang_name NOT LIKE 'TK' order by jenjang_level")->result_array();
		/*$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}*/
		
		echo json_encode($res);
	}
	public function ajax_jenjang_levelSD($par = '')
	{
		/*$_1 = substr($par, 0, strlen($par) - 3 );

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		$jname = $this->input->get('jname');
		if($jname == ''){
			$jname = 'AAAA';
		}
		$where = "";
		$resCount = 20;

		$offset = ($page - 1) * $resCount;*/

		// $term = $this->input->get('jenjang_level');
		// $jname = $this->input->get('jenjang_name');
		// if($jname == ''){
		// 	$jname = 'AAAA';
		// }
		$res = array();
		$res['results'] = $this->db->query("SELECT jenjang_id as id , CONCAT(jenjang_name,' Kelas ',jenjang_level) as text FROM master_jenjang WHERE jenjang_name = 'SD' OR jenjang_name = 'TK' order by jenjang_level")->result_array();
		/*$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}*/
		
		echo json_encode($res);
	}
	public function ajax_users()
	{
		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		$res['results'] = $this->db->query("SELECT id_user as id,user_name as text FROM users WHERE user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}

	public function ajax_req_android() {


		$subject_id = $this->input->get('subject_id');
		$avtime_id = $this->input->get('avtime_id');
		$start_time = $this->input->get('start_time');
		$jam = substr($start_time, 0,2)*3600;
		$menit = substr($start_time, 3,2)*60;
		$start_time = $jam + $menit;
		$duration = $this->input->get('duration')*60;
		$data = $this->db->query("SELECT tav.date, tav.tutor_id, fcm.fcm_token FROM tbl_avtime as tav LEFT JOIN master_fcm as fcm ON tav.tutor_id=fcm.id_user WHERE uid={$avtime_id}")->row_array();
		$id_requester = $this->input->get('id_user');
		$student_name = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user=$id_requester")->row_array()['user_name'];
		$tutor_id = $data['tutor_id'];
		$date = $data['date'];
		$fcm_token = $data['fcm_token'];
		
		$qr = $this->db->simple_query("INSERT INTO tbl_request(avtime_id,id_user_requester,tutor_id,subject_id,date_requested,time_requested,duration_requested) VALUES('{$avtime_id}','{$id_requester}','{$tutor_id}','{$subject_id}','{$date}','{$start_time}','{$duration}')");
		if($qr){
			$rets['status'] = true;
			$rets['message'] = "Extra Class Succesfully Requested";

			echo json_encode($rets);
			$this->Rumus->notif_extra_class($tutor_id,$fcm_token,$student_name,$subject_id);


		}else{
			$rets['status'] = false;
			$rets['message'] = "Extra Class Request Failed";

			echo json_encode($rets);
		}
	}
	public function ajax_save_lavtime()
	{
		$tutor_id = $this->input->post('id_user');
		$data = json_decode($this->input->post('data'),true);
		// $all_date = $this->input->post('all_date');
		// $all_time = $this->input->post('all_time');

		foreach ($data as $key => $value) {
			$dt = substr($value['start_date'], 0,10);
			$t1 = substr($value['start_date'], 11,5);
			$t1 = (intval(substr($t1, 0,2))*3600)+(intval(substr($t1, 3,2))*60);
			$t2 = substr($value['end_date'], 11,5);
			$t2 = (intval(substr($t2, 0,2))*3600)+(intval(substr($t2, 3,2))*60);
			if($value['lavtime_id'] !== ''){
				$lavtime_id = $value['lavtime_id'];	
			}else{
				$lavtime_id = "";
			}
			

			// echo $dt." ".$t1." ".$t2."\n";

			/*$dt = $value;
			$tm = explode(";", $all_time[$key]);
			$t1 = $tm[0];
			$t2 = $tm[1];*/

			 // AND date='{$dt}' AND ( (start_time >= '{$t1}' AND start_time < '{$t2}') || (stop_time > '{$t1}' && stop_time <= '{$t2}') )
			$hasil = $this->db->query("SELECT * FROM log_avtime WHERE tutor_id='{$tutor_id}' AND lavtime_id='{$lavtime_id}' ")->row_array();
			if(!empty($hasil)){
				$ids = $hasil['lavtime_id'];
				$this->db->simple_query("UPDATE log_avtime SET start_time='{$t1}', stop_time='{$t2}', date='{$dt}' WHERE lavtime_id='{$ids}'");
			}else{
				$this->db->simple_query("INSERT INTO log_avtime(tutor_id, date, start_time,stop_time) VALUES('{$tutor_id}','{$dt}','{$t1}','{$t2}')");
			}
		}
		
		$load = $this->lang->line('saved');
		$res['status'] = true;
		$res['message'] = $load;

		echo json_encode($res);

	}
	public function ajax_delete_lavtime()
	{
		$lavtime_id = htmlentities($this->input->post('lavtime_id'));
		$tutor_id = $this->input->post('id_user');


		$this->db->simple_query("DELETE FROM log_avtime WHERE lavtime_id='{$lavtime_id}' AND tutor_id='{$tutor_id}'");

		$load = $this->lang->line('sukses_delete');
		$res['status'] = true;
		$res['message'] = $load;
		echo json_encode($res);
	}
	public function ajax_lavtimeMe()
	{
		$tutor_id = $this->input->post('id_user');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');

		$data = $this->Rumus->lavtimeMe($tutor_id,$start_date,$end_date);
		// print_r($data);
		$res['status'] = true;
		$res['message'] = 'successful';
		$res['data'] = $data;
		echo json_encode($res);

	}
}