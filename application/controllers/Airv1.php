<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH."/libraries/REST_Controller.php";
require APPPATH."/libraries/jwt/JWT.php";
require APPPATH."/libraries/jwt/BeforeValidException.php";
require APPPATH."/libraries/jwt/ExpiredException.php";
require APPPATH."/libraries/jwt/SignatureInvalidException.php";

use \Firebase\JWT\JWT;

class Airv1 extends REST_Controller {

	public $sesi = array('nama' => null,'username' => null,'priv_level' => null);
	public $janus_server = "c2.classmiles.com";
	public $DB1 = null;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		// $this->load->database('default');
		// $this->DB1 = $this->load->database('db2', TRUE);
		header("Access-Control-Allow-Origin: *");	
	}

	public function main_get($value='')
	{
		echo "Welcome to Air Classmiles v1 ! Disini Dokumentasi nya yaa..";
	}

	public function channel_login_post()
	{				
		if($this->post('username') != '' && $this->post('password') != ''){
			$un = htmlentities($this->post('username'));
			$ps = htmlentities($this->post('password'));
			$data = $this->db->query("SELECT * FROM tbl_user WHERE (username = '$un' OR email = '$un') && (usertype_id = 'user channel' OR usertype_id = 'student' OR usertype_id = 'tutor')")->row_array();
			// print_r($un+"|"+$ps);
			if(!empty($data) && password_verify($ps, $data['password']) ){
				if ($data['usertype_id'] == 'student') {
					$channel_id = substr($data['id_user'], -3);
					$cid = htmlentities($this->post('channel_id'));
					$data = $this->db->query("SELECT * FROM tbl_user AS tu INNER JOIN tbl_profile_student AS tps WHERE tu.email =  '$un' AND tps.student_id = tu.id_user")->row_array();
					$ids = $data['id_user'];
					$ckstu = $this->db->query("SELECT * FROM master_channel_student WHERE channel_id='$cid' AND id_user='$ids'")->row_array();
					if (!empty($ckstu)) {
						$rets['status'] = true;
						$rets['code'] = 1;
						$rets['message'] = "Succeed student";					
						$rets['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'], "member_ship" => $ckstu['member_ship'], "channel_id" => $cid);
						$access_token = $this->create_access_token($data);
						$rets['access_token'] = $access_token;	
					}
					else
					{
						$rets['status'] = false;
						$rets['code'] = -1;
						$rets['message'] = "Anda tidak terdaftar di channel tersebut";
					}
					$this->response($rets);
				}else if ($data['usertype_id'] == 'tutor') {
					$cid = htmlentities($this->post('channel_id'));
					$channel_id = substr($data['id_user'], -3);
					$data = $this->db->query("SELECT * FROM tbl_user AS tu INNER JOIN tbl_profile_tutor AS tps WHERE tu.email =  '$un' AND tps.tutor_id = tu.id_user")->row_array();
					$ids = $data['id_user'];
					$ckstu = $this->db->query("SELECT * FROM master_channel_tutor WHERE channel_id='$cid' AND tutor_id='$ids'")->row_array();
					if (!empty($ckstu)) {
						$data['education_background'] = unserialize($data['education_background']);
						$data['teaching_experience'] = unserialize($data['teaching_experience']);
						$rets['status'] = true;
						$rets['code'] = 1;
						$rets['message'] = "Succeed tutor";
						$rets['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'], "channel_id" => $cid );
						$access_token = $this->create_access_token($data);
						$rets['access_token'] = $access_token;
					}
					else
					{
						$rets['status'] = false;
						$rets['code'] = -1;
						$rets['message'] = "Anda tidak terdaftar di channel tersebut";
						$rets['data'] = "SELECT * FROM master_channel_tutor WHERE channel_id='$cid' AND tutor_id='$ids'";
					}
					$this->response($rets);
				}else if ($data['usertype_id'] == 'user channel') {
					$channel_id = substr($data['id_user'], -3);
					$cid = htmlentities($this->post('channel_id'));
					if ($channel_id != $cid) {
						$rets['status'] = false;
						$rets['code'] = -1;
						$rets['message'] = "Login failed";
					}
					else
					{
						$rets['status'] = true;
						$rets['code'] = 1;
						$rets['message'] = "Succeed Login";
						$rets['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'], "channel_id" => $cid );
						$access_token = $this->create_access_token($data);
						$rets['access_token'] = $access_token;
					}
					$this->response($rets);
				} else {
					$rets['status'] = true;
					$rets['code'] = 1;
					$rets['message'] = "Succeed akhir";
					$rets['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'] );
					$access_token = $this->create_access_token($data);
					$rets['access_token'] = $access_token;
					$this->response($rets);
				}
			}
			$rets['status'] = false;
			$rets['code'] = 0;
			$rets['message'] = "Wrong email or password.";
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['code'] = 0;
		$rets['message'] = "Field incomplete.";
		$this->response($rets);
	}

	function randomLink(){
		$digit = 30;
		$karakter = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+{}|:<>?';,./[]";
		srand((double)microtime()*1000000);
		$i = 0;
		$key = "";
		while ($i <= $digit-1)
		{
			$num = rand() % 30;
			$tmp = substr($karakter,$num,1);
			$key = $key.$tmp;
			$i++;
		}
		return $key;
	}

	public function channel_forgotpassword_post()
	{
		$email 		= $this->post('email');
		$channel_id = $this->post('channel_id');
		$namechannel = $this->db->query("SELECT channel_name FROM master_channel WHERE channel_id = '$channel_id'")->row_array()['channel_name'];
		$linkfromchannel = $this->post('link');
		$ckInClassmiles = $this->db->query("SELECT * FROM tbl_user WHERE email='$email'")->row_array();
		if (!empty($ckInClassmiles)) {
			$id_user 		= $ckInClassmiles['id_user'];
			$user_name 		= $ckInClassmiles['user_name'];
			$ckInChannel = $this->db->query("SELECT * FROM master_channel_student WHERE channel_id='$channel_id' AND id_user='$id_user'")->row_array();
			if ($channel_id == '46') {
				$banner_channel = "https://cdn.classmiles.com/sccontent/banner/Email_Header_rlt.png";
			}
			else if ($channel_id == '52') {
				$banner_channel = "https://cdn.classmiles.com/sccontent/banner/Email_Header_fisipro.png";
			}
			else
			{
				$banner_channel = "https://cdn.classmiles.com/sccontent/maillogo_classmiles.png";
			}
			$r_link = $this->randomLink();			
			
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
					// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
			);

			$this->load->library('email', $config);						
			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', $namechannel);
			$this->email->to($email);
			$this->email->subject($namechannel.' - Change Password');
			$template = 
			"
			<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
	            <table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
	                    <tbody>
	                      <tr>        
	                          <td style='padding-left:10px;padding-right:10px'>
	                              <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:700px' width='100%'>
	                                  <tbody>
	                                    <tr>
	                                        <td style='text-align:center;padding-top:3%'>
	                                            <a href='#' style='text-decoration:none' target='_blank'>
	                                                <img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:700px' width='100%'>
	                                            </a>
	                                        </td>
	                                    </tr>

	                                    <tr>
	                                        <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
	                                            <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
	                                                <tbody>
	                                                    <tr>
	                                                          <td style='padding-right:5%' width='72%'>
	                                                                <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
	                                                                    Dear ".$user_name.",
	                                                                </h1>
	                                                                <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
	                                                                    Make sure to keep ".$namechannel." account information securely and do not share information with others                                                                           
	                                                                </p> 
	                                                                <br>
	                                                                <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
	                                                                	Somebody recently asked to reset your ".$namechannel." password. Click below to change your password :
	                                                                </p>
	                                                                <center>
	                                                                	<br>
	                                                                	<table style='width:50%; border-collapse: collapse; color: #6C7A89; text-align: center; text-decoration:none;'>
	                                                                        <tr>                                                                                    
	                                                                            <th align='center' style='padding:15px; text-decoration:none; cursor:pointer;'>
	                                                                                <a href='".$linkfromchannel."first/newpassword?randomLink=".$r_link."&email=".$email."' target='_blank'>
	                                                                                    <button style='display: flex; overflow: hidden; margin: 10px; padding: 12px 12px; width: 100%; cursor: pointer; user-select: none; transition: all 150ms linear;text-align: center; white-space: nowrap;text-decoration: none !important;text-transform: none;text-transform: capitalize;color: #fff;border: 0 none;border-radius: 4px;font-size: 13px;font-weight: 500;line-height: 1.3;-webkit-appearance: none;-moz-appearance: none;appearance: none;justify-content: center;align-items: center;flex: 0 0 160px;box-shadow: 2px 5px 10px #e4e4e4; color: #ffffff;background: #3dd28d; text-align:center; text-decoration:none; cursor:pointer;'>
	                                                                                        <label style='margin-left:25%;'>Change Password</label>
	                                                                                    </button>
	                                                                                </a>
	                                                                            </th> 
	                                                                        </tr>
	                                                                    </table> 
	                                                                    <label style='color: gray;'>OR</label><br><br>
	                                                                    <label style='color: gray; font-size:12px; text-alignment:center; width:90%; margin-bottom:30px;'>
							                                                ".$linkfromchannel."first/newpassword?randomLink=$r_link&email=$email
							                                            </label>           
							                                            <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>                                                            	
	                                                                    <label style='color: #6C7A89; font-size: 12px; margin-top: 5%;'>This email is automatically created. Please do not reply to the email</label><br>
	                                                                    <label style='color: #6C7A89; font-size: 11px; margin-top: 2%;'>Best Regards!</label><br>
	                                                                    <label style='color: #6C7A89; font-size: 11px; margin-top: 1%;'>Classmiles Team</label>
	                                                                </center>	                                                     
	                                                          </td>
	                                                    </tr> 	                                                                                                       
	                                                </tbody>
	                                            </table>
	                                        </td>
	                                    </tr>
	                                    
	                                    <tr>
	                                        <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
	                                            <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
	                                                
	                                            </tbody></table>
	                                            <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
	                                                
	                                            </tbody></table>
	                                        </td>
	                                    </tr>
	                                </tbody>
	                              </table>
	                          </td>
	                      </tr>
	                </tbody>
	            </table>
	        </div>
			";
			$this->email->message($template);
			
			if ($this->email->send()) 
			{
	        	$query = $this->db->simple_query("INSERT INTO gen_link (random_link, email_user) VALUES('$r_link','$email')");

	        	$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Succeed";				
				$this->response($rets);
	        }
	        else
	        {
	        	$rets['status'] = false;
				$rets['code'] 	= 0;
				$rets['message'] = "Failed Send Email";				
				$this->response($rets);
	        }			
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] 	= 0;
			$rets['message'] = "Failed";			
			$this->response($rets);
		}
	}

	public function ckGenLink_post($value='')
	{
		$r_link 	= $this->post('r_link');
		$email_user = $this->post('email');

		$cek_valid = $this->db->query("SELECT * FROM gen_link WHERE random_link='$r_link' AND email_user='$email_user'")->row_array();
		if (empty($cek_valid)) {
			$rets['status'] = false;
			$rets['code'] = 0;
			$rets['message'] = null;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = true;
			$rets['code'] = 1;
			$rets['message'] = "Success";			
			$this->response($rets);
		}
	}

	public function channel_saveForgotPassword_post($value='')
	{
		$password 	= password_hash($this->post('password'),PASSWORD_DEFAULT);;
		$email 		= $this->post('email');
		$randomLink = $this->post('randomLink');

		$ckInClassmiles = $this->db->query("SELECT * FROM tbl_user WHERE email='$email'")->row_array();
		if (!empty($ckInClassmiles)) {
			$this->db->query("DELETE FROM gen_link WHERE random_link='$randomLink' AND email_user='$email'");
			$this->db->query("UPDATE tbl_user SET password='$password' WHERE email='$email'");

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Success";			
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = 0;
			$rets['message'] = null;
			$this->response($rets);
		}

	}
	public function verifikasi_post()
	{
		$r_key = $this->post('r_key');

		$res = $this->db->query("SELECT * FROM gen_auth WHERE random_key='$r_key'")->row_array();

		if (!empty($res)) {

			$upd = $this->db->simple_query("UPDATE tbl_user SET status='1' WHERE id_user='$res[id_user]'");
			if ($upd) {
				$del = $this->db->simple_query("DELETE FROM gen_auth WHERE id_user='$res[id_user]'");

				//TAMBAH NOTIF
				$notif_message = json_encode( array("code" => 34));
				$link = BASE_URL()."/Subjects";
				$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$userid','$link','0')");

				$data = 900;
				$rets['status'] = true;
				$rets['code'] = 0;
				$rets['data'] = $data;
				$this->response($rets);	
			}				
		}
		else
		{
			$data = 1;
			$rets['status'] = true;
			$rets['code'] = 0;
			$rets['data'] = $data;
			$this->response($rets);		
		}		
	}

	public function verifikasiTutor_post()
	{
		$r_key = $this->post('r_key');

		$res = $this->db->query("SELECT * FROM gen_auth WHERE random_key='$r_key'")->row_array();

		if (!empty($res)) {

			$upd = $this->db->simple_query("UPDATE tbl_user SET status='1' WHERE id_user='$res[id_user]'");
			if ($upd) {
				$del = $this->db->simple_query("DELETE FROM gen_auth WHERE id_user='$res[id_user]'");

				//TAMBAH NOTIF
				$notif_message = json_encode( array("code" => 34));
				$link = BASE_URL()."/Subjects";
				$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$userid','$link','0')");

				$data = 901;
				$rets['status'] = true;
				$rets['code'] = 0;
				$rets['data'] = $data;
				$this->response($rets);	
			}				
		}
		else
		{
			$data = 1;
			$rets['status'] = true;
			$rets['code'] = 0;
			$rets['data'] = $data;
			$this->response($rets);		
		}
		
	}

	public function channel_login_bcu_post()
	{
		if($this->post('username') != '' && $this->post('password') != ''){
			$un = htmlentities($this->post('username'));
			$ps = htmlentities($this->post('password'));
			$data = $this->db->query("SELECT * FROM tbl_user WHERE username = '$un' && usertype_id = 'user channel' ")->row_array();
			// print_r($un+"|"+$ps);
			if(!empty($data) && password_verify($ps, $data['password']) ){
				if ($data['usertype_id'] == 'student') {
					$data = $this->db->query("SELECT * FROM tbl_user AS tu INNER JOIN tbl_profile_student AS tps WHERE tu.email =  '$un' AND tps.student_id = tu.id_user")->row_array();
					$rets['status'] = true;
					$rets['code'] = 1;
					$rets['message'] = "Succeed student";					
					$rets['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'] );
					$access_token = $this->create_access_token($data);
					$rets['access_token'] = $access_token;
					$this->response($rets);
				}else if ($data['usertype_id'] == 'tutor') {
					$data = $this->db->query("SELECT * FROM tbl_user AS tu INNER JOIN tbl_profile_tutor AS tps WHERE tu.email =  '$un' AND tps.tutor_id = tu.id_user")->row_array();
					$data['education_background'] = unserialize($data['education_background']);
					$data['teaching_experience'] = unserialize($data['teaching_experience']);
					$rets['status'] = true;
					$rets['code'] = 1;
					$rets['message'] = "Succeed tutor";
					$rets['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'] );
					$access_token = $this->create_access_token($data);
					$rets['access_token'] = $access_token;
					$this->response($rets);
				}else if ($data['usertype_id'] == 'user channel') {
					$channel_id = substr($data['id_user'], -3);
					$rets['status'] = true;
					$rets['code'] = 1;
					$rets['message'] = "Succeed tutor";
					$rets['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'], "channel_id" => $channel_id );
					$access_token = $this->create_access_token($data);
					$rets['access_token'] = $access_token;
					$this->response($rets);
				} else {
					$rets['status'] = true;
					$rets['code'] = 1;
					$rets['message'] = "Succeed akhir";
					$rets['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'] );
					$access_token = $this->create_access_token($data);
					$rets['access_token'] = $access_token;
					$this->response($rets);
				}
			}
			$rets['status'] = false;
			$rets['code'] = 0;
			$rets['message'] = "Wrong email or password.";
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['code'] = 0;
		$rets['message'] = "Field incomplete.";
		$this->response($rets);
	}

	public function channel_classList_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$g_channelid = $this->post('channel_id');
		$user_utc = $this->post('user_utc');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		// echo $v_accesstoken['id_user'];
		$idaccess = substr($v_accesstoken['id_user'], -3);		
		if (!isset($user_utc)) {
			$rets['status'] = false;
			$rets['code'] = 0;
			$rets['message'] = "Forbidden access!!!";			
			$this->response($rets);
		}
		$now = date('N');
		// $thisBeginDay = date($date.' 00:00:00', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
		// $oneDayAfter = date($date.' 23:59:59', strtotime('+'.$now.' day', strtotime(date('Y-m-d'))));
		$server_utc = $this->Rumus->getGMTOffset();
		// if($user_device == 'mobile'){
		// 	$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		// }
		$interval   = $user_utc - $server_utc;

		// $g_channellist	= $this->db->query("SELECT tc.*, mc.channel_name FROM tbl_class as tc INNER JOIN master_channel as mc ON tc.channel_id=mc.channel_id WHERE tc.tutor_id='$g_tutorid' AND tc.channel_id != 0 AND ( tc.start_time>=now() OR ( tc.start_time<now() AND tc.finish_time>now() ) )")->result_array();
		$g_channellist  = $this->db->query("SELECT tu.email, tu.user_name, tu.user_image, tc.class_id, tc.subject_id, tc.name, tc.description, DATE_ADD(tc.start_time, INTERVAL $interval minute) as start_time, DATE_ADD(tc.finish_time, INTERVAL $interval minute) as finish_time, tc.icon, tc.urutan, tc.topic_plan, tc.topic_actual, tc.tutor_id, tc.duration, tc.participant, tc. class_type, tc.template_type, tc.channel_id, tc.break_state, tc.break_pos, tc.created_at, tc.channel_status FROM tbl_user as tu INNER JOIN tbl_class as tc ON tu.id_user=tc.tutor_id WHERE tc.channel_id='$g_channelid' AND ( tc.start_time>=now() OR ( tc.start_time<now() AND tc.finish_time>now() ) )")->result_array();
		if (!empty($g_channellist)) {
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Succeed";
			foreach ($g_channellist as $key => $value) {
				// Open connection
				$ch = curl_init();

	        	// Set the url, number of POST vars, POST data
				curl_setopt($ch, CURLOPT_URL, 'https://classmiles.com/Tools/get_c/'.$value['class_id']);
				// curl_setopt($ch, CURLOPT_POST, true);
				// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

				// curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

	        	// Execute post
				$result = curl_exec($ch);
				curl_close($ch);
				
				$link = "https://classmiles.com/class?c=".trim($result);
				$g_channellist[$key]['url'] = $link;
				$g_channellist[$key]['participant'] = json_decode($value['participant'],true);
			}
			$rets['data'] = $g_channellist;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = true;
			$rets['code'] = 0;
			$rets['message'] = "Succeed";
			$rets['data'] = null;
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function channel_classListTutor_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$g_channelid = $this->post('channel_id');
		$user_utc = $this->post('user_utc');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		// echo $v_accesstoken['id_user'];
		$idaccess = substr($v_accesstoken['id_user'], -3);				
		if (!isset($user_utc)) {
			$rets['status'] = false;
			$rets['code'] = 0;
			$rets['message'] = "Forbidden access!!!";			
			$this->response($rets);
		}
		$now = date('N');
		// $thisBeginDay = date($date.' 00:00:00', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
		// $oneDayAfter = date($date.' 23:59:59', strtotime('+'.$now.' day', strtotime(date('Y-m-d'))));
		$server_utc = $this->Rumus->getGMTOffset();
		// if($user_device == 'mobile'){
		// 	$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		// }
		$interval   = $user_utc - $server_utc;

		// $g_channellist	= $this->db->query("SELECT tc.*, mc.channel_name FROM tbl_class as tc INNER JOIN master_channel as mc ON tc.channel_id=mc.channel_id WHERE tc.tutor_id='$g_tutorid' AND tc.channel_id != 0 AND ( tc.start_time>=now() OR ( tc.start_time<now() AND tc.finish_time>now() ) )")->result_array();
		$g_channellist  = $this->db->query("SELECT tu.email, tu.user_name, tu.user_image, tc.class_id, tc.subject_id, tc.name, tc.description, DATE_ADD(tc.start_time, INTERVAL $interval minute) as start_time, DATE_ADD(tc.finish_time, INTERVAL $interval minute) as finish_time, tc.icon, tc.urutan, tc.topic_plan, tc.topic_actual, tc.tutor_id, tc.duration, tc.participant, tc. class_type, tc.template_type, tc.channel_id, tc.break_state, tc.break_pos, tc.created_at, tc.channel_status FROM tbl_user as tu INNER JOIN tbl_class as tc ON tu.id_user=tc.tutor_id WHERE tc.channel_id='$g_channelid' AND ( tc.start_time>=now() OR ( tc.start_time<now() AND tc.finish_time>now() ) )")->result_array();
		if (!empty($g_channellist)) {
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Succeed";
			foreach ($g_channellist as $key => $value) {
				// Open connection
				$ch = curl_init();

	        	// Set the url, number of POST vars, POST data
				curl_setopt($ch, CURLOPT_URL, 'https://classmiles.com/Tools/get_c/'.$value['class_id']);
				// curl_setopt($ch, CURLOPT_POST, true);
				// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

				// curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

	        	// Execute post
				$result = curl_exec($ch);
				curl_close($ch);
				
				$link = "https://classmiles.com/class?c=".trim($result);
				$g_channellist[$key]['url'] = $link;
				$g_channellist[$key]['participant'] = json_decode($value['participant'],true);
			}
			$rets['data'] = $g_channellist;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = true;
			$rets['code'] = 0;
			$rets['message'] = "Succeed";
			$rets['data'] = null;
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function channel_addStudentToClass_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));
		$class_id 		= $this->post('class_id');
		$channel_id 	= $this->post('channel_id');
		$g_email   		= $this->post('email');
		$user_utc = $this->post('user_utc');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($class_id != '' && $channel_id != '' && $g_email != '') {
			$idaccess = substr($v_accesstoken['id_user'], -3);
			if ($idaccess != $channel_id || !isset($user_utc)) {
				$rets['status'] = false;
				$rets['code'] = 0;
				$rets['message'] = "Forbidden access!!!";			
				$this->response($rets);
			}

			$g_iduser = $this->db->query("SELECT id_user FROM tbl_user WHERE email='$g_email'")->row_array();
			if (empty($g_iduser)) {
				$rets['status'] = false;
				$rets['code'] = -101;
				$rets['message'] = 'Email belum terdaftar di classmiles';
				$rets['data'] = null;
				//EMAIL
				$statusmail = "email_notregisted";
				$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);				
				$this->response($rets);
			}
			else
			{
				$participantt 	= $g_iduser['id_user'];
				$cek_classchannel = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id' AND channel_id='$channel_id' AND ( start_time>=now() OR ( start_time<now() AND finish_time>now() ) )")->row_array();
				$tutor_id = $cek_classchannel['tutor_id'];
				if (empty($cek_classchannel)) {
					$rets['status'] = false;
					$rets['message'] = 'Maaf, Kelas tersebut sudah selesai';
					$rets['code'] = -102;
					//EMAIL
					$statusmail = "class_expired";
					$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
					$this->response($rets);
				}
				else
				{
					$cek_studentinchannel = $this->db->query("SELECT * FROM master_channel_student WHERE id_user='$participantt' AND channel_id='$channel_id'")->row_array();

					if (!empty($cek_studentinchannel)) {
						$decode = json_decode($cek_classchannel['participant'],true);
						$im_exist = false;
						foreach ($decode['participant'] as $key => $value) {	
							$userid = $value['id_user'];			 
							if ($g_iduser['id_user'] == $userid) {
								$im_exist = true;
							}
						}

						if ($im_exist) {
							$rets['status'] = false;
							$rets['message'] = 'Maaf, siswa tersebut sudah ada dalam kelas ini.';
							$rets['code'] = -103;
							//EMAIL
							$statusmail = "student_alreadyexits";
							$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
							$this->response($rets);
						}
						else
						{
							if ($cek_classchannel['class_type'] == "private_channel") {
								$decode = json_decode($cek_classchannel['participant'],true);
								$length = count($decode['participant']);	

								// MENGURANGI POINT CHANNEL //
								$exist = $this->db->query("SELECT point_id, active_point FROM uangpoint_channel WHERE channel_id='$channel_id' ")->row_array();	
								if(!empty($exist)){
									$point = $exist['active_point'];
									$point_id = $exist['point_id'];
									$harga_private15menit = 10;
									$price = $harga_private15menit * ( ($cek_classchannel['duration'] / 60) / 15 );

									if($point >= $price){
										if ($length == 1) {
											$rets['status'] = false;
											$rets['message'] = 'Maaf, kelas private maksimal 1 siswa dalam 1 kelas.';
											//email
											$rets['code'] = -405;
											$this->response($rets);
										}
										else
										{
											$participant_arr = json_decode($cek_classchannel['participant'],true);
											if(!array_key_exists('participant', $participant_arr)){
												$participant_arr['participant'] = array();
											}
											array_push($participant_arr['participant'], array("id_user" => $participantt));
											$participant_arr = json_encode($participant_arr);
											$update = $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");

											$new_point = $point-$price;
											$this->db->simple_query("UPDATE uangpoint_channel SET active_point=$new_point WHERE point_id='$point_id'");
											$trx_id = $this->Rumus->getChnPointTrxID();
											$this->db->simple_query("INSERT INTO log_channel_point_transaction VALUES('$trx_id','$channel_id','$tutor_id','$class_id','$point','$price','$new_point',now())");

											if ($update) {
												$rets['status'] = true;
												$rets['message'] = 'successfully add student to private channel';
												$rets['code'] = 0;
												$statusmail = "success_add";
												$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
												$this->response($rets);
											}
											else
											{
												$rets['status'] = false;
												$rets['message'] = 'failed add student to private channel';
												$rets['code'] = -406;
												//EMAIL
												$statusmail = "failed_add";
												$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
												$this->response($rets);
											}
										}
									}
									else
									{
										$rets['status'] = false;
										$rets['message'] = "Point tidak mencukupi";
										$rets['code'] = -201;
										//EMAIL
										$statusmail = "point_notenough";
										$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
										$this->response($rets);
										
									}
								}
								else
								{
									$rets['status'] = false;								
									$rets['message'] = "Tidak memiliki point data";
									$rets['code'] = -202;
									//EMAIL
									$statusmail = "no_points";
									$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
									$this->response($rets);
									
								}
							}
							else if ($cek_classchannel['class_type'] == "group_channel") {
								$decode = json_decode($cek_classchannel['participant'],true);
								$length = count($decode['participant']);
								
								$exist = $this->db->query("SELECT point_id, active_point FROM uangpoint_channel WHERE channel_id='$channel_id' ")->row_array();
								if(!empty($exist)){
									$point = $exist['active_point'];
									$point_id = $exist['point_id'];
									$harga_group15menit = 20;
									$price = $harga_group15menit * ( ($cek_classchannel['duration'] / 60) / 15 );

									if($point >= $price){
										if ($length >= 4) {
											$rets['status'] = false;
											$rets['message'] = 'Maaf, kelas private maksimal 4 siswa dalam 1 kelas.';
											//email
											$rets['code'] = -405;
											$this->response($rets);
										}
										else
										{
											$participant_arr = json_decode($cek_classchannel['participant'],true);
											if(!array_key_exists('participant', $participant_arr)){
												$participant_arr['participant'] = array();
											}
											// foreach ($participantt as $key => $id) {
												array_push($participant_arr['participant'], array("id_user" => $participantt));
											// }
											$participant_arr = json_encode($participant_arr);
											$update = $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");

											$new_point = $point-$price;
											$this->db->simple_query("UPDATE uangpoint_channel SET active_point=$new_point WHERE point_id='$point_id'");
											$trx_id = $this->Rumus->getChnPointTrxID();
											$this->db->simple_query("INSERT INTO log_channel_point_transaction VALUES('$trx_id','$channel_id','$tutor_id','$class_id','$point','$price','$new_point',now())");

											if ($update) {
												$rets['status'] = true;
												$rets['message'] = 'successfully add student to group channel';												
												$rets['code'] = 0;
												$statusmail = "success_add";
												$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
												$this->response($rets);
											}
											else
											{
												$rets['status'] = false;
												$rets['message'] = 'failed add student to group channel';
												$rets['code'] = -406;
												//EMAIL
												$statusmail = "failed_add";
												$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
												$this->response($rets);
											}
										}
									}
									else
									{
										$rets['status'] = false;
										$rets['code'] = -201;
										$rets['message'] = "Point tidak mencukupi";
										//email
										$statusmail = "point_notenough";
										$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
										$this->response($rets);
									}
								}
								else
								{
									$rets['status'] = false;
									$rets['code'] = -202;
									$rets['message'] = "Tidak memiliki point data";
									//email
									$statusmail = "no_points";
									$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
									$this->response($rets);
									
								}
							}
							else if ($cek_classchannel['class_type'] == "multicast_channel_paid")
							{
								$exist = $this->db->query("SELECT point_id, active_point FROM uangpoint_channel WHERE channel_id='$channel_id'")->row_array();
								if(!empty($exist)){
									$point = $exist['active_point'];
									$point_id = $exist['point_id'];
									$harga_multicast15menit = 1;

									$price = $harga_multicast15menit * ( ($cek_classchannel['duration'] / 60) / 15 );								
									if($point >= $price){	
										$participant_arr = json_decode($cek_classchannel['participant'],true);
										if(!array_key_exists('participant', $participant_arr)){
											$participant_arr['participant'] = array();
										}
										// foreach ($participantt as $key => $id) {
											array_push($participant_arr['participant'], array("id_user" => $participantt));
										// }
										$participant_arr = json_encode($participant_arr);
										$update = $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");

										$new_point = $point-$price;
										$this->db->simple_query("UPDATE uangpoint_channel SET active_point=$new_point WHERE point_id='$point_id'");
										$trx_id = $this->Rumus->getChnPointTrxID();
										$this->db->simple_query("INSERT INTO log_channel_point_transaction VALUES('$trx_id','$channel_id','$tutor_id','$class_id','$point','$price','$new_point',now())");
										// print_r($participant_arr);
										// return false;
										if ($update) {
											$rets['status'] = true;
											$rets['message'] = 'successfully add student to multicast channel';											
											$rets['code'] = 0;
											//email
											$statusmail = "success_add";
											$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
											$this->response($rets);
										}
										else
										{
											$rets['status'] = false;
											$rets['message'] = 'failed add student to multicast channel';											
											$rets['code'] = -406;
											//EMAIL
											$statusmail = "failed_add";
											$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
											$this->response($rets);
										}
									}
									else
									{
										$rets['status'] = false;
										$rets['code'] = -201;
										$rets['message'] = "Point tidak mencukupi";
										//email
										$statusmail = "point_notenough";
										$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
										$this->response($rets);
										
									}
								}
								else
								{
									$rets['status'] = false;
									$rets['code'] = -202;
									$rets['message'] = "Tidak memiliki point data";
									//email
									$statusmail = "no_points";
									$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
									$this->response($rets);									
								}
							}
							else
							{
								$rets['status'] = false;
								$rets['message'] = 'Maaf, kelas ini bukan termasuk kelas channel!!!';								
								$rets['code'] = -104;
								//EMAIL
								$statusmail = "no_yourclass";
								$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
								$this->response($rets);
							}
						}
					}
					else
					{
						$rets['status'] = false;
						$rets['message'] = 'Maaf, siswa tersebut tidak terdaftar di channel ini.';
						$rets['code'] = -105;
						//EMAIL
						$statusmail = "no_studentinhere";
						$this->Send_Email($class_id,$channel_id,$g_email,$statusmail,$user_utc);
						$this->response($rets);
					}
				}
			}
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function channel_addStudent_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$g_email = $this->post('email');
		$g_channelid = $this->post('channel_id');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($g_email != '' && $g_channelid != '') {
			$idaccess = substr($v_accesstoken['id_user'], -3);
			if ($idaccess != $g_channelid) {
				$rets['status'] = false;
				$rets['code'] = 0;
				$rets['message'] = "Forbidden access!!!";			
				$this->response($rets);
			}

			$g_iduser = $this->db->query("SELECT id_user FROM tbl_user WHERE email='$g_email'")->row_array();

			if (empty($g_iduser)) {
				$rets['status'] = false;
				$rets['code'] = 0;
				$rets['message'] = 'Email tidak terdaftar';			
				$rets['data'] = null;
				$this->response($rets);
			}
			else
			{
				$iduser = $g_iduser['id_user'];
				$cekstudent = $this->db->query("SELECT * FROM master_channel_student WHERE id_user='$iduser' AND channel_id='$g_channelid'")->row_array();
				if (empty($cekstudent)) {
					$add = $this->db->query("INSERT INTO master_channel_student (channel_id, id_user) VALUES ('$g_channelid','$iduser')");
					if ($add) {
						$rets['status'] = true;
						$rets['message'] = 'successfully add student to channel';
						$rets['code'] = 200;
						$this->response($rets);
					}
					else
					{
						$rets['status'] = false;
						$rets['message'] = 'failed add student to channel';
						$rets['code'] = 404;
						$this->response($rets);
					}
				}
				else
				{
					$rets['status'] = false;
					$rets['message'] = 'failed, student already exits';
					$rets['code'] = -402;
					$this->response($rets);
				}
			}
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function channel_deleteStudent_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$g_email = $this->post('email');
		$g_channelid = $this->post('channel_id');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($g_email != '' && $g_channelid != '') {
			$idaccess = substr($v_accesstoken['id_user'], -3);
			if ($idaccess != $g_channelid) {
				$rets['status'] = false;
				$rets['code'] = 0;
				$rets['message'] = "Forbidden access!!!";			
				$this->response($rets);
			}

			$g_iduser = $this->db->query("SELECT id_user FROM tbl_user WHERE email='$g_email'")->row_array();

			if (empty($g_iduser)) {
				$rets['status'] = false;
				$rets['code'] = 0;
				$rets['message'] = 'Email tidak terdaftar';			
				$rets['data'] = null;
				$this->response($rets);
			}
			else
			{
				$iduser = $g_iduser['id_user'];
				$cekstudent = $this->db->query("SELECT * FROM master_channel_student WHERE id_user='$iduser' AND channel_id='$g_channelid'")->row_array();
				if (!empty($cekstudent)) {
					$add = $this->db->query("DELETE FROM master_channel_student WHERE id_user='$iduser' AND channel_id='$g_channelid'");
					if ($add) {
						$rets['status'] = true;
						$rets['message'] = 'successfully delete student from channel';
						$rets['code'] = 200;
						$this->response($rets);
					}
					else
					{
						$rets['status'] = false;
						$rets['message'] = 'failed delete student from channel';
						$rets['code'] = 404;
						$this->response($rets);
					}
				}
				else
				{
					$rets['status'] = false;
					$rets['message'] = 'failed, student empty';
					$rets['code'] = -402;
					$this->response($rets);
				}
			}
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function Send_Email($class_id='',$g_channelid='',$g_email='', $statusmail='', $user_utc=''){
		
		if ($g_channelid == '46') {
			$banner_channel = "https://cdn.classmiles.com/sccontent/banner/Email_Header_rlt.png";
		}
		else if ($g_channelid == '52') {
			$banner_channel = "https://cdn.classmiles.com/sccontent/banner/Email_Header_fisipro.png";
		}
		else
		{
			$banner_channel = "https://cdn.classmiles.com/sccontent/maillogo_classmiles.png";
		}

		if ($statusmail == "success_add") {
			$now = date('N');
			$server_utc = $this->Rumus->getGMTOffset();
			$interval   = $user_utc - $server_utc;

			$data_profile 		= $this->db->query("SELECT * FROM tbl_user WHERE email='$g_email'")->row_array();
			$data_class			= $this->db->query("SELECT DATE_ADD(tbl_class.start_time, INTERVAL $interval minute) as starttime, DATE_ADD(tbl_class.finish_time, INTERVAL $interval minute) as finishtime, tbl_class.name, tbl_class.description FROM tbl_class WHERE class_id='$class_id'")->row_array();
			$data_channel 		= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$g_channelid'")->row_array();

			$data_username 		= $data_profile['user_name'];
			$data_subjectname 	= $data_class['name'];
			$data_description	= $data_class['description'];
			$data_starttime		= $data_class['starttime'];
			$data_finishtime	= $data_class['finishtime'];
			$data_channelname 	= $data_channel['channel_name'];
			$data_channelemail 	= $data_channel['channel_email'];

			//CLOCK START
			$timestart = substr($data_starttime, 11);     	    
			$timefinish = substr($data_finishtime, 11);

			//START TIME
			$datestart = date_create($data_starttime);
            $datee = date_format($datestart, 'd/m/y');
            $haristart = date_format($datestart, 'd');
            $tahunstart = date_format($datestart, 'Y');                                                    
            $date = $datee;
            $sepparator = '/';
            $parts = explode($sepparator, $datestart);
            $bulanstart = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));			

            //FINISH TIME
			$datefinish = date_create($data_finishtime);
            $dateef = date_format($datefinish, 'd/m/y');
            $harifinish = date_format($datefinish, 'd');
            $tahunfinish = date_format($datefinish, 'Y');                                                    
            $sepparator = '/';
            $parts = explode($sepparator, $datefinish);
            $bulanfinish = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://classmiles.com/Tools/get_c/'.$class_id);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$result = curl_exec($ch);
			curl_close($ch);
			
			$link = "https://classmiles.com/class?c=".trim($result);			

			$config1 = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				'charset' => 'iso-8859-1'
			);

			$this->load->library('email', $config1);
			$this->email->initialize($config1);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Info RLTClass');
			$this->email->to($g_email);
			$this->email->subject('RLTClass - Information Class');
			$templateToStudent = 
				"
				<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
				   	<table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
				            <tbody>
				            	<tr>        
					                <td style='padding-left:10px;padding-right:10px'>
					                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:600px' width='100%'>
					                        <tbody>
					                        	<tr>
						                            <td style='text-align:center;padding-top:3%'>
						                                <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
						                                    <img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:700px' width='100%'>
						                                </a>
						                            </td>
						                        </tr>

						                        <tr>
						                            <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
					                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
					                                        <tbody>
					                                        	<tr>
						                                            <td style='padding-right:5%' width='72%'>
					                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
					                                                        Dear ".$data_username.",
					                                                    </h1>
					                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
					                                                    	Anda telah didaftarkan oleh <strong>".$data_channelname."</strong> untuk mengikuti kelas <strong>".$data_subjectname." - ".$data_description."</strong>. <br>Pada tanggal <strong>".$haristart." ". $bulanstart. " ".$tahunstart." ".$timestart." - ".$harifinish." ".$bulanfinish." ".$tahunfinish." ". $timefinish."</strong>		
					                                                    </p>		                                                    
						                                            </td>
					                                    		</tr>
					                                    		<tr>
								                                    <td style='padding-bottom:20px;line-height:20px'>
								                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
								                                    </td>
						                                		</tr>
					                                    	
						                                		<tr>
								                                    <td style='background-color:#ffffff'>
								                                    	
						                                                <table border='0' cellpadding='0' cellspacing='0' style='width:100%'>
						                                                    <tbody>
						                                                    	<tr>
										                                            <td style='padding-right:5%' width='72%'>
									                                                    <p style='color:#888;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:50px;'>
										                                                	Berikut ini tautan untuk mengikuti kelas tersebut : 
									                                                    </p>		                                                    
										                                            </td>				                                            
									                                    		</tr>
						                                                    	<tr>
							                                                        <td align='center' dir='ltr' style='vertical-align:top;padding-top:20px'>
							                                                            <table border='0' cellpadding='0' cellspacing='0' style='line-height:22px;margin:0 auto;height:34px'>
							                                                                <tbody><tr>
							                                                                    <td>
							                                                                        <p style='margin:0'><a href='https://classmiles.com/class?c=a695779952ceee170f125c8d701e34a8' target='_blank'><img height='38' src='https://ci3.googleusercontent.com/proxy/T5M8eyqrU1VEO17rXjdU46IKdwUYcKTbv3eJx0V159ksn7P67z9LXLFfXYzWBibIaL1VN37yLHFV1eQLmTjsdg6eTm1Q-uYySbCPazy5olwAZx281A=s0-d-e1-ft#http://static.duolingo.com/images/email/2014/green-btn-left.png' style='width:16px;height:38px;display:block;border:0;' width='16'></a></p>
							                                                                    </td>
							                                                                    <td height='38' style='background-color:#79c60b;font-size:14px' valign='middle'>
							                                                                        <a href='".$link."' style='color:#ffffff;font-weight:700;text-decoration:none;font-size:14px!important;padding-top:10px;padding-bottom:10px' target='_blank'>Go to Link</a>
							                                                                    </td>
							                                                                    <td>
							                                                                        <p style='margin:0'><a href='https://classmiles.com/class?c=a695779952ceee170f125c8d701e34a8' target='_blank'><img height='38' src='https://ci6.googleusercontent.com/proxy/y3htt3LdEcAaQA2KC_OVj2C18eZ74Bs1tF13Skt5niQhi4TqikA-emdXhyG3yuJwtrsJvtGNBBgZVGWqlutDdOhI6bEdwTLpzUVNYAMZ_H0x1DwM_gk=s0-d-e1-ft#http://static.duolingo.com/images/email/2014/green-btn-right.png' style='width:16px;height:38px;display:block;border:0' width='16'></a></p>
							                                                                    </td>
							                                                                </tr>
							                                                            </tbody></table>
							                                                        </td>
							                                                    </tr>
						                                                	</tbody>
						                                                </table>					                                                	    			                                                   
					                                                    <tbody>
					                                                    	<tr>
					                                                    		<td align='center' style='padding-right:5%' width='100%'>
						                                                    		<p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0 top:150px; text-align: center;'>
									                                                	or		                                                	
									                                                	<br><br>
								                                                    	<strong>".$link."</strong>
								                                                    </p>
							                                                    </td>
					                                                    	</tr>
					                                                    	<tr>
											                                    <td style='padding-bottom:20px;line-height:20px'>
											                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
											                                    </td>
									                                		</tr>
								                                        	<tr>
									                                            <td align='center' style='padding-right:5%' width='100%'>
								                                                    <p style='color:red;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
									                                                	Diharapkan jangan membalas email ini.<br>

																						Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
									                                                	<br>Terima kasih,                                          	
									                                                	<br>
								                                                    	Tim Classmiles
								                                                    </p>		                                                    
									                                            </td>				                                            
								                                    		</tr>
								                                    	</tbody>                                               
								                                    </td>
						                                    	</tr>
						                                	</tbody>
						                               	</table>
						                            </td>
						                        </tr>
						                        
						                        <tr>
						                            <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
						                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
						                                    
						                                </tbody></table>
						                                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
						                                    
						                                </tbody></table>
						                            </td>
						                        </tr>
					                   		</tbody>
					                   	</table>
					                </td>
					            </tr>
				        </tbody>
				    </table>

				</div>
				";
			/**/				
			$this->email->message($templateToStudent);			

			if ($this->email->send()) 
			{																
				$config2 = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' =>'info@classmiles.com',
					'smtp_pass' => 'kerjadiCLASSMILES',	
					'mailtype' => 'html',	
					// 'wordwrap' => true,
					'charset' => 'iso-8859-1'
					);

				$this->load->library('email', $config2);
				$this->email->initialize($config2);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('info@classmiles.com', 'Info Classmiles');
				$this->email->to($data_channelemail);
				$this->email->subject('Classmiles - Success add student');
				$templateToTutor = 
				"
				<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
				   	<table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
				            <tbody>
				            	<tr>        
					                <td style='padding-left:10px;padding-right:10px'>
					                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:600px' width='100%'>
					                        <tbody>
					                        	<tr>
						                            <td style='text-align:center;padding-top:3%'>
						                                <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
						                                    <img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:600px' width='100%'>
						                                </a>
						                            </td>
						                        </tr>

						                        <tr>
						                            <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
					                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
					                                        <tbody>
					                                        	<tr>
						                                            <td style='padding-right:5%' width='72%'>
					                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
					                                                        Dear ".$data_channelname.",
					                                                    </h1>
					                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
					                                                    	Anda telah mendaftarkan <strong>".$data_username." - ".$g_email."</strong> untuk mengikuti kelas <strong>".$data_subjectname." - ".$data_description."</strong>'.		
					                                                    </p>		                                                    
						                                            </td>
					                                    		</tr>
					                                    		<tr>
								                                    <td style='padding-bottom:20px;line-height:20px'>
								                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
								                                    </td>
						                                		</tr>
						                                		<tr>
								                                    <td style='background-color:#ffffff'>
					                                                    <tbody>					                                                    	
								                                        	<tr>
									                                            <td align='center' style='padding-right:5%' width='72%'>
								                                                    <p style='color:red;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
									                                                	Diharapkan jangan membalas email ini.<br>

																						Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
									                                                	<br>Terima kasih,                                          	
									                                                	<br>
								                                                    	Tim Classmiles
								                                                    </p>		                                                    
									                                            </td>				                                            
								                                    		</tr>
								                                    	</tbody>                                               
								                                    </td>
						                                    	</tr>					                                    							                                		
						                                	</tbody>
						                               	</table>
						                            </td>
						                        </tr>
						                        
						                        <tr>
						                            <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
						                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
						                                    
						                                </tbody></table>
						                                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
						                                    
						                                </tbody></table>
						                            </td>
						                        </tr>
					                   		</tbody>
					                   	</table>
					                </td>
					            </tr>
				        </tbody>
				    </table>

				</div>
				";			
				
				$this->email->message($templateToTutor);
				if ($this->email->send()) 
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}
		else if ($statusmail == "failed_add") {			
			$data_profile 		= $this->db->query("SELECT * FROM tbl_user WHERE email='$g_email'")->row_array();
			$data_class			= $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id'")->row_array();
			$data_channel 		= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$g_channelid'")->row_array();

			$data_username 		= $data_profile['user_name'];
			$data_subjectname 	= $data_class['name'];
			$data_description	= $data_class['description'];
			$data_starttime		= $data_class['start_time'];
			$data_finishtime	= $data_class['finish_time'];
			$data_channelname 	= $data_channel['channel_name'];
			$data_channelemail 	= $data_channel['channel_email'];

			$config2 = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config2);
			$this->email->initialize($config2);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Info Classmiles');
			$this->email->to($data_channelemail);
			$this->email->subject('Classmiles - Failed add student');
			$templateToTutor = 
			"
			<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
			   	<table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
			            <tbody>
			            	<tr>        
				                <td style='padding-left:10px;padding-right:10px'>
				                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:600px' width='100%'>
				                        <tbody>
				                        	<tr>
					                            <td style='text-align:center;padding-top:3%'>
					                                <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
					                                    <img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:600px' width='100%'>
					                                </a>
					                            </td>
					                        </tr>

					                        <tr>
					                            <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
				                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
				                                        <tbody>
				                                        	<tr>
					                                            <td style='padding-right:5%' width='72%'>
				                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
				                                                        Dear Channel Admin ".$data_channelname.",
				                                                    </h1>
				                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
				                                                    	Pendaftaran <strong>".$g_email."</strong> untuk mengikuti kelas <strong>".$data_subjectname." - ".$data_description." gagal</strong>'.		
				                                                    </p>		                                                    
					                                            </td>
				                                    		</tr>
				                                    		<tr>
							                                    <td style='padding-bottom:20px;line-height:20px'>
							                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
							                                    </td>
					                                		</tr>
					                                		<tr>
							                                    <td style='background-color:#ffffff'>
				                                                    <tbody>					                                                    	
							                                        	<tr>
								                                            <td align='center' style='padding-right:5%' width='72%'>
							                                                    <p style='color:red;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
								                                                	Diharapkan jangan membalas email ini.<br>

																					Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
								                                                	<br>Terima kasih,                                          	
								                                                	<br>
							                                                    	Tim Classmiles
							                                                    </p>		                                                    
								                                            </td>				                                            
							                                    		</tr>
							                                    	</tbody>                                               
							                                    </td>
					                                    	</tr>					                                    							                                		
					                                	</tbody>
					                               	</table>
					                            </td>
					                        </tr>
					                        
					                        <tr>
					                            <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
					                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                            </td>
					                        </tr>
				                   		</tbody>
				                   	</table>
				                </td>
				            </tr>
			        </tbody>
			    </table>

			</div>
			";			
			
			$this->email->message($templateToTutor);
			if ($this->email->send()) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else if ($statusmail == "student_alreadyexits") {			
			$data_class			= $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id'")->row_array();
			$data_channel 		= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$g_channelid'")->row_array();

			$data_subjectname 	= $data_class['name'];
			$data_description	= $data_class['description'];
			$data_starttime		= $data_class['start_time'];
			$data_finishtime	= $data_class['finish_time'];
			$data_channelname 	= $data_channel['channel_name'];
			$data_channelemail 	= $data_channel['channel_email'];

			$config2 = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config2);
			$this->email->initialize($config2);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Info Classmiles');
			$this->email->to($data_channelemail);
			$this->email->subject('Classmiles - Students already exitst');
			$templateToTutor = 
			"
			<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
			   	<table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
			            <tbody>
			            	<tr>        
				                <td style='padding-left:10px;padding-right:10px'>
				                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:600px' width='100%'>
				                        <tbody>
				                        	<tr>
					                            <td style='text-align:center;padding-top:3%'>
					                                <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
					                                    <img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:600px' width='100%'>
					                                </a>
					                            </td>
					                        </tr>

					                        <tr>
					                            <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
				                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
				                                        <tbody>
				                                        	<tr>
					                                            <td style='padding-right:5%' width='72%'>
				                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
				                                                        Dear Channel Admin ".$data_channelname.",
				                                                    </h1>
				                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
				                                                    	Pendaftaran <strong>".$g_email."</strong> untuk mengikuti kelas <strong>".$data_subjectname." - ".$data_description."</strong> gagal. Dikarenakan siswa tersebut sudah ada dalam kelas ini.		
				                                                    </p>		                                                    
					                                            </td>
				                                    		</tr>
				                                    		<tr>
							                                    <td style='padding-bottom:20px;line-height:20px'>
							                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
							                                    </td>
					                                		</tr>
					                                		<tr>
							                                    <td style='background-color:#ffffff'>
				                                                    <tbody>					                                                    	
							                                        	<tr>
								                                            <td align='center' style='padding-right:5%' width='72%'>
							                                                    <p style='color:red;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
								                                                	Diharapkan jangan membalas email ini.<br>

																					Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
								                                                	<br>Terima kasih,                                          	
								                                                	<br>
							                                                    	Tim Classmiles
							                                                    </p>		                                                    
								                                            </td>				                                            
							                                    		</tr>
							                                    	</tbody>                                               
							                                    </td>
					                                    	</tr>					                                    							                                		
					                                	</tbody>
					                               	</table>
					                            </td>
					                        </tr>
					                        
					                        <tr>
					                            <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
					                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                            </td>
					                        </tr>
				                   		</tbody>
				                   	</table>
				                </td>
				            </tr>
			        </tbody>
			    </table>

			</div>
			";			
			
			$this->email->message($templateToTutor);
			if ($this->email->send()) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else if ($statusmail == "point_notenough") {			
			$data_profile 		= $this->db->query("SELECT * FROM tbl_user WHERE email='$g_email'")->row_array();
			$data_class			= $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id'")->row_array();
			$data_channel 		= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$g_channelid'")->row_array();

			$data_username 		= $data_profile['user_name'];
			$data_subjectname 	= $data_class['name'];
			$data_description	= $data_class['description'];
			$data_starttime		= $data_class['start_time'];
			$data_finishtime	= $data_class['finish_time'];
			$data_channelname 	= $data_channel['channel_name'];
			$data_channelemail 	= $data_channel['channel_email'];

			$config2 = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config2);
			$this->email->initialize($config2);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Info Classmiles');
			$this->email->to($data_channelemail);
			$this->email->subject('Classmiles - Points are not Enough');
			$templateToTutor = 
			"
			<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
			   	<table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
			            <tbody>
			            	<tr>        
				                <td style='padding-left:10px;padding-right:10px'>
				                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:600px' width='100%'>
				                        <tbody>
				                        	<tr>
					                            <td style='text-align:center;padding-top:3%'>
					                                <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
					                                    <img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:600px' width='100%'>
					                                </a>
					                            </td>
					                        </tr>

					                        <tr>
					                            <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
				                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
				                                        <tbody>
				                                        	<tr>
					                                            <td style='padding-right:5%' width='72%'>
				                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
				                                                        Dear Channel Admin ".$data_channelname.",
				                                                    </h1>
				                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
				                                                    	Pendaftaran <strong>".$g_email."</strong> untuk mengikuti kelas <strong>".$data_subjectname." - ".$data_description."</strong> gagal. Dikarenakan point channel anda tidak mencukupi. Mohon untuk mengisi point anda terlebih dahulu'.		
				                                                    </p>		                                                    
					                                            </td>
				                                    		</tr>
				                                    		<tr>
							                                    <td style='padding-bottom:20px;line-height:20px'>
							                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
							                                    </td>
					                                		</tr>
					                                		<tr>
							                                    <td style='background-color:#ffffff'>
				                                                    <tbody>					                                                    	
							                                        	<tr>
								                                            <td align='center' style='padding-right:5%' width='72%'>
							                                                    <p style='color:red;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
								                                                	Diharapkan jangan membalas email ini.<br>

																					Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
								                                                	<br>Terima kasih,                                          	
								                                                	<br>
							                                                    	Tim Classmiles
							                                                    </p>		                                                    
								                                            </td>				                                            
							                                    		</tr>
							                                    	</tbody>                                               
							                                    </td>
					                                    	</tr>					                                    							                                		
					                                	</tbody>
					                               	</table>
					                            </td>
					                        </tr>
					                        
					                        <tr>
					                            <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
					                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                            </td>
					                        </tr>
				                   		</tbody>
				                   	</table>
				                </td>
				            </tr>
			        </tbody>
			    </table>

			</div>
			";			
			
			$this->email->message($templateToTutor);
			if ($this->email->send()) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else if ($statusmail == "no_points") {			
			$data_profile 		= $this->db->query("SELECT * FROM tbl_user WHERE email='$g_email'")->row_array();
			$data_class			= $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id'")->row_array();
			$data_channel 		= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$g_channelid'")->row_array();

			$data_username 		= $data_profile['user_name'];
			$data_subjectname 	= $data_class['name'];
			$data_description	= $data_class['description'];
			$data_starttime		= $data_class['start_time'];
			$data_finishtime	= $data_class['finish_time'];
			$data_channelname 	= $data_channel['channel_name'];
			$data_channelemail 	= $data_channel['channel_email'];

			$config2 = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config2);
			$this->email->initialize($config2);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Info Classmiles');
			$this->email->to($data_channelemail);
			$this->email->subject('Classmiles - No Point Data');
			$templateToTutor = 
			"
			<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
			   	<table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
			            <tbody>
			            	<tr>        
				                <td style='padding-left:10px;padding-right:10px'>
				                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:600px' width='100%'>
				                        <tbody>
				                        	<tr>
					                            <td style='text-align:center;padding-top:3%'>
					                                <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
					                                    <img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:600px' width='100%'>
					                                </a>
					                            </td>
					                        </tr>

					                        <tr>
					                            <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
				                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
				                                        <tbody>
				                                        	<tr>
					                                            <td style='padding-right:5%' width='72%'>
				                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
				                                                        Dear Channel Admin ".$data_channelname.",
				                                                    </h1>
				                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
				                                                    	Pendaftaran <strong>".$g_email."</strong> untuk mengikuti kelas <strong>".$data_subjectname." - ".$data_description."</strong> gagal. Dikarenakan channel anda tidak memiliki Point data.		
				                                                    </p>		                                                    
					                                            </td>
				                                    		</tr>
				                                    		<tr>
							                                    <td style='padding-bottom:20px;line-height:20px'>
							                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
							                                    </td>
					                                		</tr>
					                                		<tr>
							                                    <td style='background-color:#ffffff'>
				                                                    <tbody>					                                                    	
							                                        	<tr>
								                                            <td align='center' style='padding-right:5%' width='72%'>
							                                                    <p style='color:red;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
								                                                	Diharapkan jangan membalas email ini.<br>

																					Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
								                                                	<br>Terima kasih,                                          	
								                                                	<br>
							                                                    	Tim Classmiles
							                                                    </p>		                                                    
								                                            </td>				                                            
							                                    		</tr>
							                                    	</tbody>                                               
							                                    </td>
					                                    	</tr>					                                    							                                		
					                                	</tbody>
					                               	</table>
					                            </td>
					                        </tr>
					                        
					                        <tr>
					                            <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
					                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                            </td>
					                        </tr>
				                   		</tbody>
				                   	</table>
				                </td>
				            </tr>
			        </tbody>
			    </table>

			</div>
			";			
			
			$this->email->message($templateToTutor);
			if ($this->email->send()) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else if ($statusmail == "no_yourclass") {			
			$data_profile 		= $this->db->query("SELECT * FROM tbl_user WHERE email='$g_email'")->row_array();
			$data_class			= $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id'")->row_array();
			$data_channel 		= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$g_channelid'")->row_array();

			$data_username 		= $data_profile['user_name'];
			$data_subjectname 	= $data_class['name'];
			$data_description	= $data_class['description'];
			$data_starttime		= $data_class['start_time'];
			$data_finishtime	= $data_class['finish_time'];
			$data_channelname 	= $data_channel['channel_name'];
			$data_channelemail 	= $data_channel['channel_email'];

			$config2 = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config2);
			$this->email->initialize($config2);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Info Classmiles');
			$this->email->to($data_channelemail);
			$this->email->subject('Classmiles - This not your channel class');
			$templateToTutor = 
			"
			<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
			   	<table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
			            <tbody>
			            	<tr>        
				                <td style='padding-left:10px;padding-right:10px'>
				                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:600px' width='100%'>
				                        <tbody>
				                        	<tr>
					                            <td style='text-align:center;padding-top:3%'>
					                                <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
					                                    <img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:600px' width='100%'>
					                                </a>
					                            </td>
					                        </tr>

					                        <tr>
					                            <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
				                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
				                                        <tbody>
				                                        	<tr>
					                                            <td style='padding-right:5%' width='72%'>
				                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
				                                                        Dear Channel Admin ".$data_channelname.",
				                                                    </h1>
				                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
				                                                    	Pendaftaran <strong>".$g_email."</strong> untuk mengikuti kelas <strong>".$data_subjectname." - ".$data_description."</strong> gagal. Dikarenakan kelas ini bukan termasuk kelas channel anda.		
				                                                    </p>		                                                    
					                                            </td>
				                                    		</tr>
				                                    		<tr>
							                                    <td style='padding-bottom:20px;line-height:20px'>
							                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
							                                    </td>
					                                		</tr>
					                                		<tr>
							                                    <td style='background-color:#ffffff'>
				                                                    <tbody>					                                                    	
							                                        	<tr>
								                                            <td align='center' style='padding-right:5%' width='72%'>
							                                                    <p style='color:red;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
								                                                	Diharapkan jangan membalas email ini.<br>

																					Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
								                                                	<br>Terima kasih,                                          	
								                                                	<br>
							                                                    	Tim Classmiles
							                                                    </p>		                                                    
								                                            </td>				                                            
							                                    		</tr>
							                                    	</tbody>                                               
							                                    </td>
					                                    	</tr>					                                    							                                		
					                                	</tbody>
					                               	</table>
					                            </td>
					                        </tr>
					                        
					                        <tr>
					                            <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
					                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                            </td>
					                        </tr>
				                   		</tbody>
				                   	</table>
				                </td>
				            </tr>
			        </tbody>
			    </table>

			</div>
			";			
			
			$this->email->message($templateToTutor);
			if ($this->email->send()) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else if ($statusmail == "no_studentinhere") {			
			$data_class			= $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id'")->row_array();
			$data_channel 		= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$g_channelid'")->row_array();

			$data_subjectname 	= $data_class['name'];
			$data_description	= $data_class['description'];
			$data_starttime		= $data_class['start_time'];
			$data_finishtime	= $data_class['finish_time'];
			$data_channelname 	= $data_channel['channel_name'];
			$data_channelemail 	= $data_channel['channel_email'];

			$config2 = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config2);
			$this->email->initialize($config2);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Info Classmiles');
			$this->email->to($data_channelemail);
			$this->email->subject('Classmiles - Students are not registered');
			$templateToTutor = 
			"
			<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
			   	<table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
			            <tbody>
			            	<tr>        
				                <td style='padding-left:10px;padding-right:10px'>
				                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:600px' width='100%'>
				                        <tbody>
				                        	<tr>
					                            <td style='text-align:center;padding-top:3%'>
					                                <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
					                                    <img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:600px' width='100%'>
					                                </a>
					                            </td>
					                        </tr>

					                        <tr>
					                            <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
				                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
				                                        <tbody>
				                                        	<tr>
					                                            <td style='padding-right:5%' width='72%'>
				                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
				                                                        Dear Channel Admin ".$data_channelname.",
				                                                    </h1>
				                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
				                                                    	Pendaftaran <strong>".$g_email."</strong> untuk mengikuti kelas <strong>".$data_subjectname." - ".$data_description."</strong> gagal. Dikarenakan siswa tersebut tidak terdaftar di channel ini.		
				                                                    </p>		                                                    
					                                            </td>
				                                    		</tr>
				                                    		<tr>
							                                    <td style='padding-bottom:20px;line-height:20px'>
							                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
							                                    </td>
					                                		</tr>
					                                		<tr>
							                                    <td style='background-color:#ffffff'>
				                                                    <tbody>					                                                    	
							                                        	<tr>
								                                            <td align='center' style='padding-right:5%' width='72%'>
							                                                    <p style='color:red;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
								                                                	Diharapkan jangan membalas email ini.<br>

																					Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
								                                                	<br>Terima kasih,                                          	
								                                                	<br>
							                                                    	Tim Classmiles
							                                                    </p>		                                                    
								                                            </td>				                                            
							                                    		</tr>
							                                    	</tbody>                                               
							                                    </td>
					                                    	</tr>					                                    							                                		
					                                	</tbody>
					                               	</table>
					                            </td>
					                        </tr>
					                        
					                        <tr>
					                            <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
					                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                            </td>
					                        </tr>
				                   		</tbody>
				                   	</table>
				                </td>
				            </tr>
			        </tbody>
			    </table>

			</div>
			";			
			
			$this->email->message($templateToTutor);
			if ($this->email->send()) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else if ($statusmail == "class_expired") {			
			$data_class			= $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id'")->row_array();
			$data_channel 		= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$g_channelid'")->row_array();

			$data_subjectname 	= $data_class['name'];
			$data_description	= $data_class['description'];
			$data_starttime		= $data_class['start_time'];
			$data_finishtime	= $data_class['finish_time'];
			$data_channelname 	= $data_channel['channel_name'];
			$data_channelemail 	= $data_channel['channel_email'];

			$config2 = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config2);
			$this->email->initialize($config2);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Info Classmiles');
			$this->email->to($data_channelemail);
			$this->email->subject('Classmiles - Class Expired');
			$templateToTutor = 
			"
			<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
			   	<table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
			            <tbody>
			            	<tr>        
				                <td style='padding-left:10px;padding-right:10px'>
				                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:600px' width='100%'>
				                        <tbody>
				                        	<tr>
					                            <td style='text-align:center;padding-top:3%'>
					                                <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
					                                    <img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:600px' width='100%'>
					                                </a>
					                            </td>
					                        </tr>

					                        <tr>
					                            <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
				                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
				                                        <tbody>
				                                        	<tr>
					                                            <td style='padding-right:5%' width='72%'>
				                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
				                                                        Dear Channel Admin ".$data_channelname.",
				                                                    </h1>
				                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
				                                                    	Pendaftaran <strong>".$g_email."</strong> untuk mengikuti kelas <strong>".$data_subjectname." - ".$data_description."</strong> gagal. Dikarenakan Kelas sudah selesai.		
				                                                    </p>		                                                    
					                                            </td>
				                                    		</tr>
				                                    		<tr>
							                                    <td style='padding-bottom:20px;line-height:20px'>
							                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
							                                    </td>
					                                		</tr>
					                                		<tr>
							                                    <td style='background-color:#ffffff'>
				                                                    <tbody>					                                                    	
							                                        	<tr>
								                                            <td align='center' style='padding-right:5%' width='72%'>
							                                                    <p style='color:red;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
								                                                	Diharapkan jangan membalas email ini.<br>

																					Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
								                                                	<br>Terima kasih,                                          	
								                                                	<br>
							                                                    	Tim Classmiles
							                                                    </p>		                                                    
								                                            </td>				                                            
							                                    		</tr>
							                                    	</tbody>                                               
							                                    </td>
					                                    	</tr>					                                    							                                		
					                                	</tbody>
					                               	</table>
					                            </td>
					                        </tr>
					                        
					                        <tr>
					                            <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
					                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                            </td>
					                        </tr>
				                   		</tbody>
				                   	</table>
				                </td>
				            </tr>
			        </tbody>
			    </table>

			</div>
			";			
			
			$this->email->message($templateToTutor);
			if ($this->email->send()) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else if ($statusmail == "email_notregisted") {			
			$data_class			= $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id'")->row_array();
			$data_channel 		= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$g_channelid'")->row_array();

			$data_subjectname 	= $data_class['name'];
			$data_description	= $data_class['description'];
			$data_starttime		= $data_class['start_time'];
			$data_finishtime	= $data_class['finish_time'];
			$data_channelname 	= $data_channel['channel_name'];
			$data_channelemail 	= $data_channel['channel_email'];

			$config2 = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config2);
			$this->email->initialize($config2);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Info Classmiles');
			$this->email->to($data_channelemail);
			$this->email->subject('Classmiles - Email not Register');
			$templateToTutor = 
			"
			<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
			   	<table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
			            <tbody>
			            	<tr>        
				                <td style='padding-left:10px;padding-right:10px'>
				                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:600px' width='100%'>
				                        <tbody>
				                        	<tr>
					                            <td style='text-align:center;padding-top:3%'>
					                                <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
					                                   <img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:600px' width='100%'>
					                                </a>
					                            </td>
					                        </tr>

					                        <tr>
					                            <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
				                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
				                                        <tbody>
				                                        	<tr>
					                                            <td style='padding-right:5%' width='72%'>
				                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
				                                                        Dear Channel Admin ".$data_channelname.",
				                                                    </h1>
				                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
				                                                    	Pendaftaran <strong>".$g_email."</strong> untuk mengikuti kelas <strong>".$data_subjectname." - ".$data_description."</strong> gagal. Dikarenakan email tersebut belum terdaftar di classmiles.		
				                                                    </p>		                                                    
					                                            </td>
				                                    		</tr>
				                                    		<tr>
							                                    <td style='padding-bottom:20px;line-height:20px'>
							                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
							                                    </td>
					                                		</tr>
					                                		<tr>
							                                    <td style='background-color:#ffffff'>
				                                                    <tbody>					                                                    	
							                                        	<tr>
								                                            <td align='center' style='padding-right:5%' width='72%'>
							                                                    <p style='color:red;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
								                                                	Diharapkan jangan membalas email ini.<br>

																					Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
								                                                	<br>Terima kasih,                                          	
								                                                	<br>
							                                                    	Tim Classmiles
							                                                    </p>		                                                    
								                                            </td>				                                            
							                                    		</tr>
							                                    	</tbody>                                               
							                                    </td>
					                                    	</tr>					                                    							                                		
					                                	</tbody>
					                               	</table>
					                            </td>
					                        </tr>
					                        
					                        <tr>
					                            <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
					                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
					                                    
					                                </tbody></table>
					                            </td>
					                        </tr>
				                   		</tbody>
				                   	</table>
				                </td>
				            </tr>
			        </tbody>
			    </table>

			</div>
			";			
			
			$this->email->message($templateToTutor);
			if ($this->email->send()) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		
	}
	
	public function listTutorChannel_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));		
		$g_channelid = $this->post('channel_id');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($g_channelid != '') {			
			$select_all = $this->db->query("SELECT mct.*, tu.user_name, tu.email, tu.user_callnum, tu.user_gender, tu.user_address FROM tbl_user as tu INNER JOIN master_channel_tutor as mct ON tu.id_user=mct.tutor_id WHERE mct.channel_id = '$g_channelid'")->result_array();
			if (!empty($select_all)) {						
				$rets['status'] 	= true;
				$rets['message'] 	= 'List Tutor';
				$rets['code'] 		= 200;
				$rets['data'] 		= $select_all;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 0;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function listStudentChannel_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));		
		$g_channelid = $this->post('channel_id');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($g_channelid != '') {			
			$select_all = $this->db->query("SELECT mct.*, tu.user_name, tu.email, tu.user_callnum, tu.user_gender, tu.user_address FROM tbl_user as tu INNER JOIN master_channel_student as mct ON tu.id_user=mct.id_user WHERE mct.channel_id = '$g_channelid' AND mct.flag_membership=1")->result_array();
			if (!empty($select_all)) {						
				$rets['status'] 	= true;
				$rets['message'] 	= 'List Student';
				$rets['code'] 		= 200;
				$rets['data'] 		= $select_all;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 0;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function listStudentJoinChannel_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));		
		$g_channelid = $this->post('channel_id');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($g_channelid != '') {			
			$select_all = $this->db->query("SELECT mct.*, tu.user_name, tu.email, tu.user_callnum, tu.user_gender, tu.user_address FROM tbl_user as tu INNER JOIN master_channel_student as mct ON tu.id_user=mct.id_user WHERE mct.channel_id = '$g_channelid' AND mct.flag_membership='0'")->result_array();
			if (!empty($select_all)) {						
				$rets['status'] 	= true;
				$rets['message'] 	= 'List Student';
				$rets['code'] 		= 200;
				$rets['data'] 		= $select_all;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 0;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function reportChannel_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));		
		$channel_id = $this->post('channel_id');
		$type = $this->post('type');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($channel_id != '' && $type != '') {								
			if ($type == "channel") {				
				$list_point = $this->db->query("SELECT lcpt.*, tc.*, tu.user_name FROM log_channel_point_transaction as lcpt INNER JOIN tbl_class as tc ON lcpt.class_id=tc.class_id INNER JOIN tbl_user as tu ON tu.id_user=lcpt.tutor_creator WHERE lcpt.channel_id='$channel_id' ORDER BY lcpt.ptrx_id DESC")->result_array();
				if ($list_point) {
					$res['status'] = true;
					$new_arr = array();
					if(!empty($list_point)){
						foreach ($list_point as $key => $value) {
							$date = date_create($value['created_at']);
					        $datee = date_format($date, 'd/m/y');
					        $hari = date_format($date, 'd');
					        $tahun = date_format($date, 'Y');
					                                                
					        $date = $datee;
					        $sepparator = '/';
					        $parts = explode($sepparator, $date);
					        $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

							$list_point[$key]['participant'] = json_decode($value['participant'],true);
							$list_point[$key]['created_at_point'] = $hari.' '.$bulan.' '.$tahun;
							array_push($new_arr, $list_point[$key]);
						}
					}
					$rets['status'] 	= true;
					$rets['message'] 	= 'Report Channel';
					$rets['code'] 		= 200;
					$rets['data'] 		= $new_arr;
				}
				else
				{
					$rets['status'] = true;
					$rets['code'] = 0;
					$rets['message'] = "Succeed";
					$rets['data'] = null;
					$this->response($rets);
				}
			}
			else
			{
				$list_point = $this->db->query("SELECT lcpt.*, tc.*, tu.user_name FROM log_channel_point_transaction as lcpt INNER JOIN tbl_class as tc ON lcpt.class_id=tc.class_id INNER JOIN tbl_user as tu ON tu.id_user=lcpt.tutor_creator ORDER BY lcpt.ptrx_id DESC")->result_array();
				if ($list_point) {
					$res['status'] = true;
					$new_arr = array();
					if(!empty($list_point)){
						foreach ($list_point as $key => $value) {
							$date = date_create($value['created_at']);
					        $datee = date_format($date, 'd/m/y');
					        $hari = date_format($date, 'd');
					        $tahun = date_format($date, 'Y');
					                                                
					        $date = $datee;
					        $sepparator = '/';
					        $parts = explode($sepparator, $date);
					        $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

							$list_point[$key]['participant'] = json_decode($value['participant'],true);
							$list_point[$key]['created_at_point'] = $hari.' '.$bulan.' '.$tahun;
							array_push($new_arr, $list_point[$key]);
						}
					}
					$res['message'] = 'successful';
					$res['data'] = $new_arr;
				}
				else
				{
					$res['status'] = false;
					$res['message'] = 'failed';
					$res['data'] = '';
				}			
			}
			
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function pointChannel_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));		
		$g_channelid = $this->post('channel_id');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($g_channelid != '') {			
			$select_all = $this->db->query("SELECT * FROM uangpoint_channel WHERE channel_id='$g_channelid'")->row_array();
			if (!empty($select_all)) {						
				$rets['status'] 	= true;
				$rets['message'] 	= 'Point Channel';
				$rets['code'] 		= 200;
				$rets['data'] 		= $select_all;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 0;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function deleteTutor_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$g_channelid = $this->post('channel_id');
		$rtp = $this->post('rtp');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($g_channelid != '') {	
			$delete = $this->db->query("DELETE FROM master_channel_tutor WHERE tutor_id='$rtp'");
			if ($delete) {
				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$g_channelid','Delete tutor channel','$rtp','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil menghapus tutor";				
				$this->response($rets);
			}
			else
			{
				$rets['status'] 	= false;
				$rets['message'] 	= 'Terjadi kesalahan';
				$rets['code'] 		= 0;				
				$this->response($rets);
			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);		
	}

	public function deleteStudent_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$g_channelid = $this->post('channel_id');
		$rtp = $this->post('rtp');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($g_channelid != '') {	
			$delete = $this->db->query("DELETE FROM master_channel_student WHERE id_user='$rtp' AND channel_id='$g_channelid'");
		
			if ($delete) {
				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$g_channelid','Delete student channel','$rtp','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil menghapus student";				
				$this->response($rets);
			}
			else
			{
				$rets['status'] 	= false;
				$rets['message'] 	= 'Terjadi kesalahan';
				$rets['code'] 		= 0;				
				$this->response($rets);
			}			
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);		
	}

	public function getTutorName_get($value='')
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->get('channel_id');
		$term = $this->get('term');
		$page  = $this->get('page');

		if ($v_accesstoken == false) {
			$res = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($channel_id != '') {
			$resCount = 10;

			$offset = ($page - 1) * $resCount;				
			$res['results'] = $this->db->query("SELECT id_user as id, CONCAT(user_name, ' - ', email) as text FROM tbl_user WHERE id_user NOT IN (SELECT tutor_id FROM master_channel_tutor where channel_id = {$channel_id}) AND usertype_id='tutor' AND user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
			$total = count($res['results']);
			if($total < $resCount){
				$res['more'] = 0;
			}else{
				$res['more'] = 1;
			}
		}
		
		$res['status'] = false;
		$res['message'] = "Field incomplete";
		ending:		
		echo json_encode($res);
	}

	public function getTutorNameIn_get($value='')
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->get('channel_id');
		$term = $this->get('term');
		$page  = $this->get('page');

		if ($v_accesstoken == false) {
			$res = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($channel_id != '') {
			$resCount = 10;

			$offset = ($page - 1) * $resCount;				
			$res['results'] = $this->db->query("SELECT id_user as id, CONCAT(user_name, ' - ', email) as text FROM tbl_user WHERE id_user IN (SELECT tutor_id FROM master_channel_tutor where channel_id = {$channel_id}) AND usertype_id='tutor' AND user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
			$total = count($res['results']);
			if($total < $resCount){
				$res['more'] = 0;
			}else{
				$res['more'] = 1;
			}
		}
		
		$res['status'] = false;
		$res['message'] = "Field incomplete";
		ending:		
		echo json_encode($res);
	}

	public function getStudentName_get($value='')
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->get('channel_id');
		$term = $this->get('term');
		$page  = $this->get('page');

		if ($v_accesstoken == false) {
			$res = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($channel_id != '') {
			// $term = $this->input->get('term');
			// $page  = $this->input->get('page');

			$resCount = 10;

			$offset = ($page - 1) * $resCount;
			// SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id='student'
			// $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id = 'student'")->result_array();
			$res['results'] = $this->db->query("SELECT id_user as id, CONCAT(user_name, ' - ', email) as text FROM tbl_user WHERE id_user NOT IN (SELECT id_user FROM master_channel_student where channel_id = {$channel_id}) AND ( usertype_id='student' OR usertype_id = 'student kid' ) AND user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();

			$total = count($res['results']);
			if($total < $resCount){
				$res['more'] = 0;
			}else{
				$res['more'] = 1;
			}
		}
		
		$res['status'] = false;
		$res['message'] = "Field incomplete";
		ending:		
		echo json_encode($res);
	}

	public function getStudentNameIn_get($value='')
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->get('channel_id');
		$term = $this->get('term');
		$page  = $this->get('page');

		if ($v_accesstoken == false) {
			$res = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($channel_id != '') {
			// $term = $this->input->get('term');
			// $page  = $this->input->get('page');

			$resCount = 10;

			$offset = ($page - 1) * $resCount;
			// SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id='student'
			// $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id = 'student'")->result_array();
			$res['results'] = $this->db->query("SELECT id_user as id, CONCAT(user_name, ' - ', email) as text FROM tbl_user WHERE id_user IN (SELECT id_user FROM master_channel_student where channel_id = {$channel_id}) AND ( usertype_id='student' OR usertype_id = 'student kid' ) AND user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
			$total = count($res['results']);
			if($total < $resCount){
				$res['more'] = 0;
			}else{
				$res['more'] = 1;
			}
		}
		
		$res['status'] = false;
		$res['message'] = "Field incomplete";
		ending:		
		echo json_encode($res);
	}

	public function getListNameGroup_get($value='')
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->get('channel_id');		
		$term = $this->get('term');
		$page  = $this->get('page');

		if ($v_accesstoken == false) {
			$res = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($channel_id != '') {
			// $term = $this->input->get('term');
			// $page  = $this->input->get('page');

			$resCount = 10;

			$offset = ($page - 1) * $resCount;
			// SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id='student'
			// $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id = 'student'")->result_array();
			$res['results'] = $this->db->query("SELECT id_user as id, CONCAT(user_name, ' - ', email) as text FROM tbl_user WHERE id_user IN (SELECT id_user FROM master_channel_student where channel_id = {$channel_id} AND status='active') AND (usertype_id='student' OR usertype_id='student kid') AND user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
			$total = count($res['results']);
			if($total < $resCount){
				$res['more'] = 0;
			}else{
				$res['more'] = 1;
			}
		}
		
		$res['status'] = false;
		$res['message'] = "Field incomplete";
		ending:		
		echo json_encode($res);
	}

	public function getListNameGroupNotLike_get($value='')
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->get('channel_id');
		$group_id = $this->get('group_id');
		$term = $this->get('term');
		$page  = $this->get('page');		
		if ($v_accesstoken == false) {
			$res = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($channel_id != '') {
			// $term = $this->input->get('term');
			// $page  = $this->input->get('page');

			$resCount = 10;

			$list_part = $this->db->query("SELECT group_participants FROM master_channel_group WHERE group_id = '$group_id'")->row_array();
			$lar = "";
			if(!empty($list_part)){

				$list_part = json_decode($list_part['group_participants'], TRUE);
				if(isset($list_part)){
					foreach ($list_part as $key => $value) {
						$lar.= $value.",";							
					}
				}

			}
			$lar = rtrim($lar, ','); 
			if($lar == "") {
				$lar = "0";
			}

			$offset = ($page - 1) * $resCount;
			// SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id='student'
			// $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id = 'student'")->result_array();
			$res['results'] = $this->db->query("SELECT id_user as id, CONCAT(user_name, ' - ', email) as text FROM tbl_user WHERE id_user IN (SELECT id_user FROM master_channel_student where channel_id = {$channel_id} && id_user NOT IN ($lar AND status='active') ) AND  (usertype_id='student' OR usertype_id='student kid') AND user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
			$total = count($res['results']);
			if($total < $resCount){
				$res['more'] = 0;
			}else{
				$res['more'] = 1;
			}
		}
		
		$res['status'] = false;
		$res['message'] = "Field incomplete";
		ending:		
		echo json_encode($res);
	}

	public function changeStatusClass_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$pilihan 	 	= $this->post('pilihan');
		$class_id 		= $this->post('class_id');
		$channel_id		= $this->post('channel_id');

		if ($v_accesstoken == false) {
			$json = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($pilihan != '' && $class_id != '' && $channel_id != '') {
			$update = $this->db->simple_query("UPDATE tbl_class SET channel_status='$pilihan' WHERE class_id='$class_id'");		
			if ($update) {
				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Memilih status channel menjadi ','','$myip',now())");
				$json = array("status" => 1,"code" => '200');
			}
			else
			{
				$json = array("status" => 0,"code" => '404');
			}
		}
		else
		{
			$json = array("status" => 0,"code" => '404');
		}

		$json['status'] = false;
		$json['message'] = "Field incomplete";
		ending:
		echo json_encode($json);
	}

	public function listTutor_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->post('channel_id');

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $alltutor = $this->db->query("SELECT mct.*, tu.user_name, tu.email FROM master_channel_tutor as mct INNER JOIN tbl_user as tu ON mct.tutor_id=tu.id_user WHERE mct.channel_id='$channel_id'")->result_array();
	        if(!empty($alltutor)){
				// foreach ($list_point as $key => $value) {
		        // echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line("selectsubjecttutor").'</option>'; 
		        // foreach ($alltutor as $row => $v) {                                            
		        //     echo '<option value="'.$v['tutor_id'].'">'.$v['user_name'].' - '.$v['email'].'</option>';
		        // }
		        $rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "List Tutor";
				$rets['data'] = $alltutor;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Tidak ada tutor";
				$rets['data'] = null;
				$this->response($rets);	
			}
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;				
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function addTutorChannel_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->post('channel_id');
		$tutor_id = $this->post('tutor_id');

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $add = $this->db->query("INSERT INTO master_channel_tutor (channel_id, tutor_id) VALUES ('$channel_id','$tutor_id')");

			if ($add) {
				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Tambah tutor channel','$tutor_id','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil menambah tutor";				
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Gagal menambah tutor";
				$this->response($rets);	
			}	        
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function addStudentChannel_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->post('channel_id');
		$id_user = $this->post('id_user');

		if ($v_accesstoken == false) {
			$rets['status'] 	= false;
			$rets['code'] 		= -400;
			$rets['message'] 	= "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $add = $this->db->query("INSERT INTO master_channel_student (channel_id, id_user, flag_membership) VALUES ('$channel_id','$id_user', '1')");

			if ($add) {
				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Delete student channel','$id_user','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil menambah Student";
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Gagal menambah Student";
				$this->response($rets);	
			}	        
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function changeTutorClassChannel_post()
    {
    	$v_accesstoken = $this->verify_access_token($this->get('access_token'));
        $class_id       = $this->post('class_id');
        $tutor_select   = $this->post('tutor_select');
        $channel_id     = $this->post('channel_id');

        if ($v_accesstoken == false) {
			$rets['status'] 	= false;
			$rets['code'] 		= -400;
			$rets['message'] 	= "invalid token.";
			goto ending;
		}

		if ($class_id != '' && $tutor_select != '' && $channel_id != '') {
	        $cekclass       = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id' AND channel_id='$channel_id'")->row_array();
	        if (empty($cekclass)) {	            
	            $rets['status'] = false;
					$rets['code'] = -400;
					$rets['message'] = "Terjadi kesalahan system saat mengubah data, harap coba lagi";
					$this->response($rets);
	        }
	        else
	        {
	            $updttr   = $this->db->simple_query("UPDATE tbl_class SET tutor_id='$tutor_select' WHERE class_id='$class_id' AND channel_id='$channel_id'");
	            if ($updttr) {	                
	                $rets['status'] = true;
					$rets['code'] = 200;
					$rets['message'] = "Tutor berhasil terubah";
					$this->response($rets);
	            }
	            else
	            {                	                
	                $rets['status'] = false;
					$rets['code'] = -300;
					$rets['message'] = "Gagal mengubah data, harap coba lagi";
					$this->response($rets);
	            }
	        }
	    }
	    else
	    {
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;
			$this->response($rets);
	    }

	    $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
        $this->response($rets);	
    }

    public function channel_addStudentToGroup_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));
		$channel_id 		= $this->post('channel_id');
		$group_name 		= $this->post('group_name');		
		$group_participants = json_encode($this->post('group_participants'));

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($group_name != '' && $channel_id != '' && $group_participants != '') {
			$addGroup 	= $this->db->query("INSERT INTO master_channel_group (channel_id,group_name, group_participants) VALUES ('$channel_id','$group_name','$group_participants')");
			if($addGroup)
			{
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil menambah Group";
				$this->response($rets);
			}
			else
			{
				$rets['status'] = false;
				$rets['code'] = -300;
				$rets['message'] = "Terjadi Kesalahan, harap coba lagi";
				$this->response($rets);
			}
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function listProgramFromProgramId_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->post('channel_id');
		$program_id = $this->post('program_id');

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $allprogram = $this->db->query("SELECT * FROM master_channel_program WHERE channel_id='{$channel_id}' AND program_id = '{$program_id}'")->row_array();
	        if(!empty($allprogram)){
	        	
		        $rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "List program";
				$rets['data'] = $allprogram;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Tidak ada program";
				$rets['data'] = null;
				$this->response($rets);	
			}
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;				
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function update_programById_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));
		$channel_id 	= $this->post('channel_id');
		$program_id 	= $this->post('program_id');
		$program_name 	= $this->post('program_name');
		$group_id 		= $this->post('group_id');		

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $allprogram = $this->db->query("UPDATE master_channel_program SET program_name = '{$program_name}', group_id = '{$group_id}' WHERE channel_id = '{$channel_id}' AND program_id = '{$program_id}'");
				        	
	        if($allprogram){
	        	
		        $rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Success update program";				
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Failed update program";				
				$this->response($rets);	
			}
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;				
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function listGroup_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->post('channel_id');

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $allGroup = $this->db->query("SELECT * FROM master_channel_group WHERE channel_id='{$channel_id}'")->result_array();
	        if(!empty($allGroup)){
	        	foreach ($allGroup as $key => $value) {
	        		$participant 	= $value['group_participants'];	        			     	        		
                    $listParti 		= json_decode($participant,true);
                    $data 			= "";
                    foreach ($listParti as $keyy => $v) {
                    	$getName 	= $this->db->query("SELECT user_name, email FROM tbl_user WHERE id_user='$v'")->row_array();
	        			$data 		= $data.'* '.$getName['user_name'].' - '.$getName['email'].'<br> ';
                    }
                    $allGroup[$key]['participant'] = $data;
                    $allGroup[$key]['group_participants'] = json_decode($value['group_participants']);
	        	}
		        $rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "List Group";
				$rets['data'] = $allGroup;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Tidak ada Group";
				$rets['data'] = null;
				$this->response($rets);	
			}
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;				
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function listGroupById_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->post('channel_id');
		$group_id	= $this->post('group_id');

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $allGroup = $this->db->query("SELECT * FROM master_channel_group WHERE channel_id='{$channel_id}' AND group_id='{$group_id}'")->row_array();
	        if(!empty($allGroup)){	 

	        	$new_array = array();       	
        		$participant 	= $allGroup['group_participants'];
                $listParti 		= json_decode($participant,true);
                $data_username  = array();
                $data_email  	= array();
                $data 			= "";
                foreach ($listParti as $keyy => $v) {
                	$getName 	= $this->db->query("SELECT user_name, email FROM tbl_user WHERE id_user='$v'")->row_array();
        			$data 		= $data.'* '.$getName['user_name'].' - '.$getName['email'].'<br> ';        			
        			array_push($new_array, array("id_user" => $v, "nama" => $getName['user_name'], "email" => $getName['email']));
                }
                $allGroup['participant'] 	= $data;
                $allGroup['new_arr'] 		= $new_array;
                $allGroup['group_participants'] = $listParti;

		        $rets['status'] = true;
				$rets['code'] 	= 200;
				$rets['message']= "List Group";
				$rets['data'] 	= $allGroup;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Tidak ada Group";
				$rets['data'] = null;
				$this->response($rets);	
			}
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;				
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function updateGroupName_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));
		$channel_id 	= $this->post('channel_id');
		$group_id 		= $this->post('group_id');
		$group_name 	= $this->post('group_name');

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $allGroup = $this->db->query("SELECT * FROM master_channel_group WHERE channel_id='{$channel_id}' AND group_id='{$group_id}'")->result_array();
	        if(!empty($allGroup)){
	        	
	        	$this->db->query("UPDATE master_channel_group SET group_name = '$group_name' WHERE group_id = '{$group_id}' AND channel_id = '{$channel_id}'");

		        $rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Success Update Name Group";				
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Tidak ada Group";				
				$this->response($rets);	
			}
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;				
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function updateGroupParticipants_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));
		$channel_id 	= $this->post('channel_id');
		$group_id 		= $this->post('group_id');
		$group_participants 	= json_encode($this->post('group_participants'));

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $allGroup = $this->db->query("SELECT * FROM master_channel_group WHERE channel_id='{$channel_id}' AND group_id='{$group_id}'")->result_array();
	        if(!empty($allGroup)){
	        	
	        	$this->db->query("UPDATE master_channel_group SET group_participants = '$group_participants' WHERE group_id = '{$group_id}' AND channel_id = '{$channel_id}'");

		        $rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Success Update Group Participants";				
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Tidak ada Group";				
				$this->response($rets);	
			}
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;				
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function deleteGroup_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$g_channelid = $this->post('channel_id');
		$rtp = $this->post('rtp');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($g_channelid != '') {	
			$delete = $this->db->query("DELETE FROM master_channel_group WHERE group_id='$rtp'");
			if ($delete) {
				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$g_channelid','Delete Group','$rtp','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil menghapus group";				
				$this->response($rets);
			}
			else
			{
				$rets['status'] 	= false;
				$rets['message'] 	= 'Terjadi kesalahan';
				$rets['code'] 		= 0;				
				$this->response($rets);
			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);		
	}

	public function channel_saveProgram_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));
		$channel_id 	= $this->post('channel_id');		
		$program_name 	= $this->post('program_name');
		$sesi 			= $this->post('sesi');
		$group_id 		= json_decode($this->post('group_id'));
		$schedule 		= $this->post('schedule');		
		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {
			$valid_until = date('Y-m-d');
			$participant = array();			
			foreach ($group_id as $key => $value) {				
				$a 	= $this->db->query("SELECT group_participants FROM master_channel_group WHERE group_id = '$value'")->row_array()['group_participants'];
				$a 	= json_decode($a);	
				// $match = 1;			
				foreach ($a as $keya => $vl) {
					if (!in_array($vl, $participant)) {		
						array_push($participant, array("id_user" => $vl, "valid_until" => ''));				
					}					
				}
				// if (in_array($a, $participant)) {
				// 	echo "ada ";
				// }
				// else
				// {
				// 	echo "tidak ada";
				// 	$participant = array_merge($participant,$a);
				// }				
			}
			$participant 	= json_encode($participant);
			// print_r($participant);
			// return false;
	        $add = $this->db->query("INSERT INTO master_channel_program (channel_id, program_name, sesi, participants, schedule,status) VALUES ('$channel_id','$program_name','$sesi','$participant','$schedule','0')");

			if ($add) {
				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Tambah Program','','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil menambah program";				
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Gagal menambah program";
				$this->response($rets);	
			}	        
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function listProgram_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->post('channel_id');

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $allProgram = $this->db->query("SELECT * FROM master_channel_program  WHERE channel_id='{$channel_id}'")->result_array();
	        if(!empty($allProgram)){
	        	foreach ($allProgram as $key => $value) {
	        		$participant 	= $value['schedule'];	
	        		$group_id 		= $value['group_id'];
                    $listParti 		= json_decode($participant,true);
                    $listGroup 		= json_decode($group_id,true);
                    $data 			= "";
                    $countGroup 	= count($listGroup);
                    if (is_array($participant)) {
	                    foreach ($listParti as $keyy => $v) {
	                    	$seconds            = $v['eventDurasi'];
	                        $hours              = floor($seconds / 3600);
	                        $mins               = floor($seconds / 60 % 60);
	                        $secs               = floor($seconds % 60);
	                        $durationrequested  = sprintf('%02d:%02d', $hours, $mins);
	                    	// $getName 	= $this->db->query("SELECT user_name, email FROM tbl_user WHERE id_user='$v'")->row_array();
		        			$data 		= $data.'* Tanggal : '.$v['eventDate'].', Waktu : '.$v['eventTime'].', Durasi : '.$durationrequested.' Hours<br> ';
	                    }
	                    $allProgram[$key]['schedule'] = $data;
	                }
	                if (is_array($listGroup)) {
	                	$group_name = '';
	                	foreach ($listGroup as $keya => $v) {
	                		$getGroupName 	= $this->db->query("SELECT group_name FROM master_channel_group WHERE group_id = '$v'")->row_array()['group_name'];
	                		if ($keya+1 == $countGroup) {
	                			$group_name 	= $group_name.$getGroupName;	
	                		}	   
	                		else{
	                			$group_name 	= $group_name.$getGroupName.', ';
	                		}             		
	                	}
	                	$allProgram[$key]['group_name'] = $group_name;
	                }
	                else
	                {
	                	$getGroupName 	= $this->db->query("SELECT group_name FROM master_channel_group WHERE group_id = '$group_id'")->row_array()['group_name'];
	                	$allProgram[$key]['group_name'] = $getGroupName;
	                }
	        	}
		        $rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "List Program";
				$rets['data'] = $allProgram;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Tidak ada Program";
				$rets['data'] = null;
				$this->response($rets);	
			}
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;				
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function deleteProgram_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$g_channelid = $this->post('channel_id');
		$rtp = $this->post('rtp');

		if ($v_accesstoken == false) {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		if($g_channelid != '') {	
			$delete = $this->db->query("DELETE FROM master_channel_program WHERE program_id='$rtp'");
			if ($delete) {
				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$g_channelid','Delete Program','$rtp','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil menghapus program";				
				$this->response($rets);
			}
			else
			{
				$rets['status'] 	= false;
				$rets['message'] 	= 'Terjadi kesalahan';
				$rets['code'] 		= 0;				
				$this->response($rets);
			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);		
	}

	public function channel_getTentang_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->post('channel_id');
		
		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {

			$get 	= $this->db->query("SELECT * FROM master_channel WHERE channel_id =  '$channel_id'")->row_array();

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Details Channel";
			$rets['data'] = $get;
			$this->response($rets);
		}	
		else
		{
			$rets['status'] = true;
			$rets['code'] = 300;
			$rets['message'] = "Error";
			$rets['data'] = null;
			$this->response($rets);	
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function channel_saveTentang_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id 			= $this->post('channel_id');		
		$channel_name 			= $this->post('channel_name');
		$channel_email 			= $this->post('channel_email');
		$channel_description 	= $this->post('channel_description');
		$channel_about 			= $this->post('channel_about');
		$channel_country_code 	= $this->post('channel_country_code');
		$channel_callnum 		= $this->post('channel_callnum');		
		$channel_address 		= $this->post('channel_address');
		$channel_link 			= $this->post('channel_link');
		
		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $cek = $this->db->query("SELECT * FROM master_channel WHERE channel_id='$channel_id'")->row_array();

			if ($cek) {
				$add = $this->db->simple_query("UPDATE master_channel SET channel_name='$channel_name',channel_description='$channel_description',channel_about='$channel_about',channel_country_code='$channel_country_code',channel_callnum='$channel_callnum',channel_email='$channel_email',channel_address='$channel_address',channel_link='$channel_link' WHERE channel_id='$channel_id'");
				if ($add) {
					$myip = $_SERVER['REMOTE_ADDR'];
					$plogc_id = $this->Rumus->getLogChnTrxID();
					$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Update data channel','','$myip',now())");
					
					$rets['status'] = true;
					$rets['code'] = 200;
					$rets['message'] = "Berhasil mengubah channel";				
					$this->response($rets);	
				}
				else
				{
					$rets['status'] = true;
					$rets['code'] = -300;
					$rets['message'] = "Terjadi kesalahan mengubah data";
					$this->response($rets);
				}
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Channel Tidak Ditemukan";
				$this->response($rets);	
			}	        
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function countryCode_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
		
		$allcode = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
		if (empty($allcode)) {
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;
			$rets['data'] = null;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Sukses";
			$rets['data'] = $allcode;
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function channel_updateLogo_post()
	{			
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = substr($v_accesstoken['id_user'], -3);

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}

		$filename 			= $_FILES['file']['name'];
		$fileSize 			= $_FILES['file']['size'];
		$fileTmpname 		= $_FILES['file']['tmp_name'];
		$logo_new	=$this->input->post('logo_new');
		
		$folder 			= './aset/_temp_images/';
		$sessionuserlogo 	= $channel_id;
		$sessioniduser		= $channel_id."_l".$this->Rumus->RandomString(15);
		$file_size			= $fileSize;

		$max_size			= 2000000;
		$file_name			= $sessioniduser.'.jpg';
		if($fileSize >= 2000000) { //2 MB (size is also in bytes)
			$rets['status'] 	= false;
			$rets['message'] 	= 'File Melebihi size';
			$rets['code'] 		= -304;
			$this->response($rets);
		}
		else
		{
			file_put_contents("aset/_temp_images/".$file_name,file_get_contents($logo_new));
			// move_uploaded_file($fileTmpname, $folder.$file_name);


			$updatelogo = $this->db->query("UPDATE master_channel SET channel_logo='".base64_encode('channel/'.$file_name)."' WHERE channel_id='$sessionuserlogo'");

			if ($updatelogo) {

				$config['image_library'] = 'gd2';
				$config['source_image']	= $folder.$file_name;
				$config['width']	= 512;
				$config['maintain_ratio'] = FALSE;
				$config['height']	= 512;
				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$sessionuserlogo','Update logo channel',NULL,'$myip',now())");

				$this->image_lib->initialize($config); 

				$this->image_lib->resize();
				$this->Rumus->sendToCDN('channel', $file_name, null, 'aset/_temp_images/'.$file_name );

				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil menganti Logo channel";				
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = -300;
				$rets['message'] = "Terjadi kesalahan mengubah data";
				$this->response($rets);
			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function channel_updateBanner_post()
	{			
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = substr($v_accesstoken['id_user'], -3);

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}

		$filename 			= $_FILES['file']['name'];
		$fileSize 			= $_FILES['file']['size'];
		$fileTmpname 		= $_FILES['file']['tmp_name'];
		
		$folder 			= './aset/_temp_images/';
		$sessionuserlogo 	= $channel_id;
		$sessioniduser		= $channel_id."_l".$this->Rumus->RandomString(15);
		$file_size			= $fileSize;

		$max_size			= 2000000;
		$file_name			= $sessioniduser.'.jpg';
		if($fileSize >= 2000000) { //2 MB (size is also in bytes)
			$rets['status'] 	= false;
			$rets['message'] 	= 'File Melebihi size';
			$rets['code'] 		= -304;
			$this->response($rets);
		}
		else
		{
			move_uploaded_file($fileTmpname, $folder.$file_name);


			$updatelogo = $this->db->query("UPDATE master_channel SET channel_banner='".base64_encode('channel/'.$file_name)."' WHERE channel_id='$sessionuserlogo'");

			if ($updatelogo) {

				$config['image_library'] = 'gd2';
				$config['source_image']	= $folder.$file_name;
				$config['width']	= 1268;
				$config['maintain_ratio'] = FALSE;
				$config['height']	= 720;
				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$sessionuserlogo','Update banner channel',NULL,'$myip',now())");

				$this->image_lib->initialize($config); 

				$this->image_lib->resize();
				$this->Rumus->sendToCDN('channel', $file_name, null, 'aset/_temp_images/'.$file_name );

				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil menganti banner channel";				
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = -300;
				$rets['message'] = "Terjadi kesalahan mengubah data";
				$this->response($rets);
			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function channel_listModifyClass_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->post('channel_id');
		$program_id = $this->post('program_id');

		if ($v_accesstoken == false) {
			$rets['status']  = false;
			$rets['code'] 	 = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
        
		if ($channel_id != '') {		
	        $allProgram = $this->db->query("SELECT * FROM master_channel_program WHERE channel_id='{$channel_id}' AND program_id='$program_id'")->result_array();
	        if(!empty($allProgram)){
	        	foreach ($allProgram as $key => $value) {
	        		$participant 		= $value['schedule'];

	        		// $group_id 			= json_decode($value['group_id']);
	        		// $gp 				= "";
	        		// $gp_name 			= "";
	        		// $group_participants = '';
	        		// if (is_array($group_id)) {
	        		// 	$count = count($group_id);
	        		// 	foreach ($group_id as $ki => $vl) {	        				
	        		// 		$get 				= $this->db->query("SELECT * FROM master_channel_group WHERE group_id = '$vl'")->row_array();
	        		// 		$gp_nm 				= $get['group_name'];
	        		// 		$gp_pr 				= $get['group_participants'];
	        		// 		$group_participants = $group_participants.$gp_pr;
	        		// 		if($count == $ki+1){
	        		// 			$gp_name = $gp_name.$gp_nm;
	        		// 		}
	        		// 		else
	        		// 		{
	        		// 			$gp_name = $gp_name.$gp_nm.' ,';	
	        		// 		}

	        		// 	}
	        		// }
	        		// else
	        		// {	        			
	        		// 	$get 				= $this->db->query("SELECT * FROM master_channel_group WHERE group_id = '$group_id'")->row_array();
	        		// 	$gp_nm 				= $get['group_name'];
	        		// 	$gp_name 			= $gp_name.$gp_nm;
	        		// 	$gp_pr 				= $get['group_participants'];
	        		// 	$group_participants = $group_participants.$gp_pr;
	        			
	        		// }	        			        		

	        		// $allProgram[$key]['group_name'] = $gp_name;

	        		// $listGroupParti 	= json_decode($group_participants,true);
                    $data 				= "";
                    if ($participant != null) {     
                    	$listParti 			= json_decode($participant,true);               	
	                    foreach ($listParti as $keyy => $v) {
	                    	$seconds            = $v['eventDurasi'];
	                        $hours              = floor($seconds / 3600);
	                        $mins               = floor($seconds / 60 % 60);
	                        $secs               = floor($seconds % 60);
	                        $durationrequested  = sprintf('%02d:%02d', $hours, $mins);
	                        $schedule_tanggal 	= $v['eventDate'];
	                        $schedule_waktu 	= $v['eventTime'];                        
	                        $allProgram[$key]['listParti'][$keyy]['schedule_tanggal'] = $schedule_tanggal;
	                        $allProgram[$key]['listParti'][$keyy]['schedule_waktu']   = $schedule_waktu;
	                        $allProgram[$key]['listParti'][$keyy]['schedule_durasi']  = $durationrequested.' Jam';
	                        $allProgram[$key]['listParti'][$keyy]['eventDurasi']  = $v['eventDurasi'];
	                        $allProgram[$key]['listParti'][$keyy]['schedule_tutorid']  = $v['tutor_id'];
	                        $allProgram[$key]['listParti'][$keyy]['schedule_status']  = $v['status'];
	                    }
	                }
                    // foreach ($listGroupParti as $keyyy => $list) {
                   	// 	$getName 		= $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$list'")->row_array()['user_name'];
                   	// 	$allProgram[$key]['listGroupParti'][$keyyy]['id_user'] = $list;
                   	// 	$allProgram[$key]['listGroupParti'][$keyyy]['user_name'] = $getName;
                   	// } 
                   	                   
	        	}
		        $rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "List Modify Class";
				$rets['data'] = $allProgram;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = 300;
				$rets['message'] = "Tidak ada Program";
				$rets['data'] = null;
				$this->response($rets);	
			}
	    }
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;				
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function channel_listMapel_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->post('channel_id');

		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}        
		$country_code = $this->db->query("SELECT channel_country_code FROM master_channel WHERE channel_id='$channel_id'")->row_array()['channel_country_code'];
		$nicename = $this->db->query("SELECT nicename FROM master_country WHERE phonecode = '$country_code'")->row_array()['nicename'];
		if ($channel_id != '') {	
			if ($nicename == "Singapore") {
				$where = "AND curriculum = '$nicename' OR curriculum = 'Internasional'";

				$allmapel = $this->db->query("SELECT * FROM `master_jenjang` WHERE curriculum LIKE '%International%' OR curriculum LIKE '%Singapore%'")->result_array();
				if (!empty($allmapel)) {
					foreach ($allmapel as $key => $vl) {
						$jenjangnamelevel = array();
						$jenjangid = json_decode($vl['jenjang_id'],true);
						if ($jenjangid != NULL) {
							// if (is_array($jenjangid)) {
								$allmapell = $this->db->query("SELECT * FROM master_subject WHERE jenjang_id LIKE '%$jenjangid%'")->row_array();
								$jenjangnamelevel[] = $vl['jenjang_name']." ".$vl['jenjang_level'];
								$allmapel[$key]['subject_id'] = $allmapell['subject_id'];
								$allmapel[$key]['subject_name'] = $allmapell['subject_name'];
								$allmapel[$key]['indonesia'] = $allmapell['indonesia'];
								$allmapel[$key]['english'] = $allmapell['english'];
								$allmapel[$key]['iconweb'] = $allmapell['iconweb'];
								$allmapel[$key]['icon'] = $allmapell['icon'];
							// }
						}
						$jenjangnamelevel_c = implode(", ",$jenjangnamelevel);
	                    $allmapel[$key]['jenjangnamelevel'] = $jenjangnamelevel_c;
					}	
				}
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "List Mapel";
				$rets['data'] = $allmapel;
				$this->response($rets);			
			}
			else
			{		
				$where = '';
		        $allmapel = $this->db->query("SELECT * FROM master_subject")->result_array();
		        if(!empty($allmapel)){
					foreach ($allmapel as $row => $v) {
	                    $jenjangnamelevel = array();
	                    $jenjangid = json_decode($v['jenjang_id'], TRUE);
	                    if ($jenjangid != NULL) {
	                        if(is_array($jenjangid)){
	                            foreach ($jenjangid as $key => $value) {                                    
	                                $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value' $where ")->row_array();
	                                if ($selectnew['jenjang_level']=='0') {
	                                    $jenjangnamelevel[] = $selectnew['jenjang_name'];
	                                }
	                                else{
	                                    $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];
	                                }
	                            }
	                        }else{
	                            $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid' $where ")->row_array();
	                            if ($selectnew['jenjang_level']=='0') {
	                                $jenjangnamelevel[] = $selectnew['jenjang_name'];
	                            }
	                            else{
	                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];
	                            }                                                         
	                        }                                       
	                    
	                        $jenjangnamelevel_c = implode(", ",$jenjangnamelevel);
	                        $allmapel[$row]['jenjangnamelevel'] = $jenjangnamelevel_c;
	                    }
	                }

			        $rets['status'] = true;
					$rets['code'] = 200;
					$rets['message'] = "List Mapel";
					$rets['data'] = $allmapel;
					$this->response($rets);
				}
			}
		}		
	    else
		{
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;				
			$this->response($rets);
		}

        $rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}

	public function channel_createLaterClass_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$data_arrlater = $this->post('data_arrlater');		
		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
		}
		if(empty($data_arrlater))
		{
			$rets['status'] = false;
			$rets['code'] = -405;
			$rets['message'] = "data kosong";	
		}
		else
		{
			foreach ($data_arrlater as $keyo => $arraydata) { 
				$program_id = $arraydata['program_id'];
				$tanggal 	= $arraydata['tanggal'];
				$waktu 		= $arraydata['waktu'];
				$waktuu 	= $arraydata['waktuu'];
				$durasi 	= $arraydata['durasi'];
				$channel_id = $arraydata['channel_id'];
				$user_utc 	= $arraydata['user_utc'];

				if ($channel_id != '') {		
			        $cek = $this->db->query("SELECT * FROM master_channel_program WHERE program_id='$program_id'")->row_array();

					if (!empty($cek)) {
						$pizza 		= $cek['schedule'];
						$json_a 	= json_decode($pizza,true);

						foreach ($json_a as $key => $data) {
							if ($tanggal == $data['eventDate'] && $waktuu == $data['eventTime'] && $durasi == $data['eventDurasi']) {
								$json_a[$key]['status'] = "0";
							}
						}
						$participant = $this->db->escape_str(json_encode($json_a));
						$updateLaterClass = $this->db->simple_query("UPDATE master_channel_program SET schedule='$participant'  WHERE program_id='{$program_id}'");

						$myip = $_SERVER['REMOTE_ADDR'];
						$plogc_id = $this->Rumus->getLogChnTrxID();
						$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Update Later Class','','$myip',now())");
						
						$rets['status'] = true;
						$rets['code'] = 200;
						$rets['message'] = "Berhasil update later class";					
					}
					else
					{
						$rets['status'] = true;
						$rets['code'] = 300;
						$rets['message'] = "Gagal update";					
					}	        
			    }
			    else
				{
					$rets['status'] 	= false;
					$rets['message'] 	= 'Terjadi kesalahan';
					$rets['code'] 		= 0;				
				}
			}
		}
		$this->response($rets);	
	}

	public function channel_changeTutorModify_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$program_id = $this->post('program_id');
		$channel_id = $this->post('channel_id');
		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
		$eventDate 		= $this->post('eventDate');
		$eventTime 		= $this->post('eventTime');
		$eventDurasi 	= $this->post('eventDurasi');

		$getProgram 		= $this->db->query("SELECT * FROM master_channel_program WHERE program_id='$program_id' AND channel_id='$channel_id'")->row_array();

		$schedule 			= $getProgram['schedule'];
		$json_schedule 		= json_decode($schedule,true);		
		$stat_upd 			= 0;
		foreach ($json_schedule as $key => $value) {
			if ($eventDate == $value['eventDate'] && $eventTime == $value['eventTime'] && $eventDurasi == $value['eventDurasi']) {
				$json_schedule[$key]['tutor_id'] 	= "";
				$json_schedule[$key]['subject_id'] 	= "";
				$json_schedule[$key]['desc'] 		= "";
				$json_schedule[$key]['status'] 		= 0;
				$json_schedule[$key]['expired']		= "";
				$stat_upd = 1;
			}
		}

		if ($stat_upd == 1) {
			$schedule_now 		= $this->db->escape_str(json_encode($json_schedule));
			$updateDataProgram 	= $this->db->simple_query("UPDATE master_channel_program SET schedule='$schedule_now' WHERE program_id='{$program_id}'");	

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Sukses";		
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "gagal";		
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);	
	}
	
	public function bcu_email($value='')
	{
		$a = "<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
				            <table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
			                    <tbody>
			                        <tr>        
			                            <td style='padding-left:10px;padding-right:10px'>
			                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:700px' width='100%'>
			                                    <tbody>
			                                      	<tr>
			                                          	<td style='text-align:center;padding-top:3%'>
			                                              	<a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
			                                                  	<img src='".$banner_channel."' style='display:block;margin:0;border:0;max-width:700px' width='100%'>
			                                              	</a>
			                                          	</td>
			                                      	</tr>

			                                      	<tr>
			                                          	<td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
			                                              	<table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
			                                                  	<tbody>
			                                                      	<tr>
			                                                            <td style='padding-right:5%' width='72%'>
			                                                                <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
			                                                                      Dear ".$usernameSend.",
			                                                                </h1>
			                                                                <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
			                                                                    Anda mendapat permintaan kelas dari Channel <b id='nama_channel'>".$namechannel."</b><br>  
			                                                                </p>                   
			                                                                <div style='border-bottom:2px solid #eeeeee;'>&nbsp;</div>
				                                                                <br><br>
				                                                                <br><br>
				                                                                <div class='row col-md-12' style='color: black;'>
				                                                                    <div class='col-md-3'>
				                                                                        <img class='img-rounded' id='logo_channel' onerror='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=' style='height: 120px; width: 120px; background-color: #008080;'>
				                                                                    </div>
				                                                                    <div class='col-md-4' style='color: black;'>
				                                                                        <label>Nama Program</label>
				                                                                        <label>Waktu Permintaan</label><br>
				                                                                        <label>Mata Pelajaran</label><br>
				                                                                        <label>Topik</label><br>
				                                                                        <label>Durasi Kelas</label><br>
				                                                                    </div>
				                                                                    <div class='col-md-5' style='padding: 0;'>
				                                                                        <b>: </b><label id='nama_program'>".$programname."</label><br>
				                                                                        <b>: </b><label id='waktu_permintaan'>".$tanggal." ".$waktu."</label><br>
				                                                                        <b>: </b><label id='mata_pelajaran'>".$mapel_select."</label><br>
				                                                                        <b>: </b><label id='topik'>".$enterDesc."</label><br>
				                                                                        <b>: </b><label id='durasi_kelas'></label>".$durasiemail."<br>
				                                                                    </div>
				                                                                </div>
				                                                            </div>
			                                                                </center>
			                                                                </p>                                                    
			                                                            </td>
			                                                      	</tr>                                                          
			                                                      	<tr>
			                                                          	<td style='background-color:#ffffff'>                                                                      
			                                                              	<table border='0' cellpadding='0' cellspacing='0' style='width:100%'>
			                                                                  	<tbody>
			                                                                    	<tr>
			                                                                        	<td style='padding-right:5%; text-align:left;' width='72%'>
			                                                                              	<center>
			                                                                                  	<h2>
			                                                                                      	<p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
			                                                                                          	Silakan klik tombol LOGIN untuk masuk ke ".$channel_name." untuk Menyetujui / tidak<br><br>  
			                                                                                      	</p>
			                                                                                      	<div style='width:300px; height:60px;'>
			                                                                                          	<a href='".BASE_URL()."' style='color:white; cursor:pointer; width:300px; height:100px; border-radius:5px; padding:15px; text-decoration:none; border:0px; background-color:#19B5FE; color:white; font-size:16px;'>
			                                                                                              	LOGIN
			                                                                                          	</a>                                                
			                                                                                      	</div>
			                                                                                  	</h2>
			                                                                              	</center><br>
			                                                                        	</td>                                                   
			                                                                    	</tr>
			                                                                	</tbody>
			                                                              	</table>                                                                                                                         
			                                                              	<tbody>                                                                          
			                                                                  	<tr>
			                                                                      	<td style='padding-bottom:20px;line-height:20px'>
			                                                                          	<div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
			                                                                      	</td>
			                                                                  	</tr>
			                                                                  	<tr>
			                                                                      	<td align='center' style='padding-right:5%' width='100%'>
			                                                                            <p style='color:gray;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
			                                                                            Diharapkan jangan membalas email ini.<br>
			                                                                            Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
			                                                                            <br>Terima kasih,                                           
			                                                                            <br>
			                                                                              Tim ".$channel_name."
			                                                                            </p>                                                        
			                                                                      	</td>                                                   
			                                                                  	</tr>
			                                                              	</tbody>                                               
			                                                          	</td>
			                                                      	</tr>
			                                                  	</tbody>
			                                              	</table>
			                                          	</td>
			                                      	</tr>
			                                      
			                                      	<tr>
			                                          	<td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
			                                              	<table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
			                                                  
			                                              	</tbody></table>
			                                              	<table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
			                                                  
			                                              	</tbody></table>
			                                          	</td>
			                                     	</tr>
			                                  	</tbody>
			                                </table>
			                            </td>
			                        </tr>
				                </tbody>
				            </table>
				        </div>";
	}

	public function channel_createNowClass_post()
	{
		$data_arr 		= $this->post('data_arr');		
		$channelid 		= $this->post('channel_id');
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));		
		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";			
		}
		if(empty($data_arr))
		{
			$rets['status'] = false;
			$rets['code'] = -405;
			$rets['message'] = "data kosong";	
		}
		else
		{
			if ($channelid == '46') {
				$banner_channel = "https://cdn.classmiles.com/sccontent/banner/Email_Header_rlt.png";
			}
			else if ($channelid == '52') {
				$banner_channel = "https://cdn.classmiles.com/sccontent/banner/Email_Header_fisipro.png";
			}
			else
			{
				$banner_channel = "https://cdn.classmiles.com/sccontent/maillogo_classmiles.png";
			}

			foreach ($data_arr as $keyo => $arraydata) {
				$tanggal 		= $arraydata['tanggal'];			
				$channel_id 	= $arraydata['channel_id'];
				$program_id 	= $arraydata['program_id'];
				$tanggal 		= $arraydata['tanggal'];
				$waktu 			= $arraydata['waktu'];
				$waktuu 		= $arraydata['waktuu'];
				$durasi 		= $arraydata['durasi'];
				$tutor_select 	= $arraydata['tutor_select'];
				$mapel_select 	= $arraydata['mapel_select'];
				$enterDesc 		= $arraydata['enterDesc'];
				$user_utc 		= $arraydata['user_utc'];

				$namechannel 	= $this->db->query("SELECT channel_name FROM master_channel WHERE channel_id='$channel_id'")->row_array()['channel_name'];
				       
				if ($channel_id != '') {		
			        $cek = $this->db->query("SELECT * FROM master_channel_program WHERE program_id='$program_id'")->row_array();

					if (!empty($cek)) {												

						//PROSES EMAIL KE TUTOR
						// $config2 = Array(
					 //        'protocol' => 'smtp',
					 //        'smtp_host' => 'ssl://smtp.googlemail.com',
					 //        'smtp_port' => 465,
					 //        'smtp_user' =>'info@classmiles.com',
					 //        'smtp_pass' => 'kerjadiCLASSMILES', 
					 //        'mailtype' => 'html',   
					 //        'charset' => 'iso-8859-1'
					 //    );
						// $programname 	= $cek['program_name'];
						// $dataTutor 		= $this->db->query("SELECT * FROM tbl_user WHERE id_user='$tutor_select'")->row_array();
						// $emailSend 		= $dataTutor['email'];
						// $usernameSend   = $dataTutor['user_name'];
						// $H 				= floor($durasi / 3600);
						// $i 				= ($durasi / 60) % 60;
						// $s 				= $durasi % 60;
						// $durasiemail 	= sprintf("%02d:%02d:%02d", $H, $i, $s);

						// $getChannelData = $this->db->query("SELECT * FROM master_channel WHERE channel_id = '$channel_id'")->row_array();
						// $channel_name 	= $getChannelData['channel_name'];
						// $selectgetsubject 	= $this->db->query("SELECT * FROM master_subject WHERE subject_id = '$mapel_select'")->row_array();
						// $subject_name 	= $selectgetsubject['subject_name'];

						// $this->load->library('email', $config2);
						// $this->email->initialize($config2);
						// $this->email->set_mailtype("html");
						// $this->email->set_newline("\r\n");
						// $this->email->from('info@classmiles.com', 'Info '.$channel_name);
						// $this->email->to($emailSend);
						// $this->email->subject($channel_name.' - Request Class From Channel '.$namechannel);
						// $templateToStudent = 
						// "
						
						// ";
										
						// $this->email->message($templateToStudent);
						// if ($this->email->send()) 
						// {
							//PROSES CHANGE STATUS MASTER_CHANNEL_PROGRAM
							$pizza 		= $cek['schedule'];
							$json_ag 	= json_decode($pizza,true);
							foreach ($json_ag as $keyo => $datas) {											
								// echo $tanggal." - ".$datas['eventDate']." ";
								// echo $waktuu." - ".$datas['eventTime']." ";
								// echo $durasi." - ".$datas['eventDurasi']." ";
								if ($tanggal == $datas['eventDate'] && $waktuu == $datas['eventTime'] && $durasi == $datas['eventDurasi']) {
									$json_ag[$keyo]['tutor_id'] 	= $tutor_select;
									$json_ag[$keyo]['subject_id'] 	= $mapel_select;
									$json_ag[$keyo]['desc'] 		= $enterDesc;
									$json_ag[$keyo]['status'] 		= "2";
								}							
							}							
							$schedule_a = $this->db->escape_str(json_encode($json_ag));
							$updateNowwClass = $this->db->simple_query("UPDATE master_channel_program SET schedule='$schedule_a' WHERE program_id='{$program_id}'");
							$upd_status = 0;
							if ($updateNowwClass) {
								$get_sc 	= $this->db->query("SELECT schedule FROM master_channel_program WHERE program_id='$program_id'")->row_array()['schedule'];
								$json_sc 	= json_decode($get_sc,true);
								foreach ($json_sc as $keyp => $datass) {
									if ($tanggal == $datass['eventDate'] && $waktuu == $datass['eventTime'] && $durasi == $datass['eventDurasi']) {
										if ($datass['status'] == 2 || $datass['status'] == 1) {
											$upd_status = 1;
										}
									}
								}		
								$updateNowwClass = $this->db->simple_query("UPDATE master_channel_program SET status='$upd_status' WHERE program_id='{$program_id}'");
							}
							$myip = $_SERVER['REMOTE_ADDR'];
							$plogc_id = $this->Rumus->getLogChnTrxID();
							$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Update Now Class','','$myip',now())");
							
							//// FCM NOTIF ????
							$json_notif = "";
				            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$tutor_select}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
				            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
								$json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "33","title" => "Classmiles", "text" => "", "payload" => "")));
								$json_notif['data']['data']['payload'][] = array("student_name" => $namechannel, "indonesia" => $selectgetsubject['indonesia'], "english" =>$selectgetsubject['english']);
				            }
				            if($json_notif != ""){
				                $headers = array(
				                    'Authorization: key=' . FIREBASE_API_KEY,
				                    'Content-Type: application/json'
				                    );
				                // Open connection
				                $ch = curl_init();

				                // Set the url, number of POST vars, POST data
				                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
				                curl_setopt($ch, CURLOPT_POST, true);
				                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

				                // Execute post
				                $result = curl_exec($ch);
				                curl_close($ch);
				                // echo $result;
				            }
							//// FCM NOTIF ????

							// $rets['res'] = $result;
							$rets['status'] = true;
							$rets['code'] = 200;
							$rets['message'] = "Berhasil menyimpan dan request ke tutors";
							$rets['json']	= $json_notif;
							$rets['API'] = FIREBASE_API_KEY;
							
						// }
						// else
						// {										
						// 	$rets['status'] = false;
						// 	$rets['code'] = '-304';
						// 	$rets['message'] = "gagal mengirim email";
						// }				
					}
					else
					{
						$rets['status'] = true;
						$rets['code'] = 300;
						$rets['message'] = "Gagal update";					
					}	        
			    }
			    else
				{
					$rets['status'] 	= false;
					$rets['message'] 	= 'Terjadi kesalahan';
					$rets['code'] 		= 0;				
				}	       	 
			}	
		}
		$this->response($rets);	
	}	

	public function channel_createClass_post()
	{
		$rets = array();
		$tutor_id = $this->post('id_tutor');
		$tanggal = $this->post('tanggal');
		$time = $this->post('time');
		$durasi = $this->post('durasi');
		$template = $this->post('template');
		$pelajaran = $this->post('pelajaran');
		$topikpelajaran = $this->post('topikpelajaran');
		$channel_id = $this->post('channel_id');
		// $participantt = $this->post('participant');
		$st_time = $tanggal." ".$time;

		$user_utc = $this->post('user_utc');

		if ($this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			$namesubject = $this->db->query("SELECT * FROM master_subject WHERE subject_id='{$pelajaran}'")->row_array()['subject_name'];

			$server_utc = $this->Rumus->getGMTOffset();
			$interval = $user_utc - $server_utc;
			$date_requested = DateTime::createFromFormat ('Y-m-d H:i:s', $tanggal." ".$time );
			$date_requested->modify("-".$interval ." minutes");

			$unix_requested = $date_requested->getTimestamp();
			$unix_now = time();
			if($unix_requested < ($unix_now + 3600) ){
				$rets['status'] = false;
				$rets['code'] = '402';
				$rets['message'] = "Maaf, Lewat Batas.";
				$this->response($rets);
			}

			$date_requested = $date_requested->format('Y-m-d H:i:s');

			$fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
			$fn_time->modify("+$durasi second");
			$fn_time = $fn_time->format('Y-m-d H:i:s');

			// CHECK JIKA WAKTU TELAH TERISI
			$cjwtt = $this->db->query("SELECT * FROM tbl_class WHERE ( ( '$date_requested' <= start_time AND '$fn_time' > start_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) OR ( '$date_requested' >= start_time AND '$date_requested' < finish_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) ) AND tutor_id=$tutor_id ")->result_array();
			if(!empty($cjwtt)){
				$rets['status'] = false;
				$rets['code'] = '403';
				$rets['message'] = "Maaf, Bentrok.";
				$this->response($rets);
			}

			$participant['visible'] = "private";
			$participant['participant'] = array();

			$exist = $this->db->query("SELECT point_id, active_point FROM uangpoint_channel WHERE channel_id='{$channel_id}' ")->row_array();
			if(!empty($exist)){
				$point = $exist['active_point'];
				$point_id = $exist['point_id'];
				$harga_multicast15menit = 1;
				// $total_person = count($participant['participant']);
				// $price = $harga_multicast15menit * $total_person * ( ($durasi / 60) / 15 );

				// if($point >= $price){
					$topikpelajaran = $this->db->escape_str($topikpelajaran);
					$participant = json_encode($participant);
					$this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,duration,participant,class_type,template_type,channel_id,channel_status) VALUES('{$pelajaran}','{$namesubject}','{$topikpelajaran}','{$tutor_id}','{$date_requested}','{$fn_time}','{$durasi}','{$participant}','multicast_channel_paid','{$template}','{$channel_id}','0')");

					$class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

					// $new_point = $point-$price;
					// $this->db->simple_query("UPDATE uangpoint_channel SET active_point=$new_point WHERE point_id='$point_id'");
					// $trx_id = $this->Rumus->getChnPointTrxID();
					// $this->db->simple_query("INSERT INTO log_channel_point_transaction VALUES('$trx_id','$channel_id','$tutor_id','$class_id','$point','$price','{$new_point}',now())");
					
					$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$tutor_id'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						// echo $hash_id;
						$this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$class_id','$tutor_id','TRUE',0)");
					}	

					$rets['status'] = true;
					$rets['code'] = '200';
					$rets['message'] = "sukses";					
					$this->response($rets);	
				// }else{
				// 	$rets['status'] = false;
				// 	$rets['code'] = '-101';
				// 	$rets['message'] = "Point tidak mencukupi";
				// 	$this->response($rets);
				// }
			}
			$rets['status'] = false;
			$rets['code'] = '-100';
			$rets['message'] = "Tidak memiliki point data";
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);		
	}

	public function dashboardChannel_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$channel_id = $this->post('channel_id');
		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
		
		$total_program = $this->db->query("SELECT count(*) as total_program FROM master_channel_program WHERE channel_id='$channel_id'")->row_array()['total_program'];
		$total_group = $this->db->query("SELECT count(*) as total_group FROM master_channel_group WHERE channel_id='$channel_id'")->row_array()['total_group'];
		$total_student = $this->db->query("SELECT count(*) as total_student FROM master_channel_student WHERE channel_id='$channel_id'")->row_array()['total_student'];
		$total_tutor = $this->db->query("SELECT count(*) as total_tutor FROM master_channel_tutor WHERE channel_id='$channel_id'")->row_array()['total_tutor'];
		$total_class = $this->db->query("SELECT count(*) as total_class FROM tbl_user as tu INNER JOIN tbl_class as tc ON tu.id_user=tc.tutor_id WHERE tc.channel_id='$channel_id' AND ( tc.start_time>=now() OR ( tc.start_time<now() AND tc.finish_time>now() ) )")->row_array()['total_class'];

		$rets['status'] = true;
		$rets['code'] = 200;
		$rets['message'] = "Sukses";
		$rets['total_program'] = $total_program;
		$rets['total_group'] = $total_group;
		$rets['total_student'] = $total_student;
		$rets['total_tutor'] = $total_tutor;
		$rets['total_class'] = $total_class;
		$this->response($rets);

		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function getFeaturedClass_post()
	{
		$user_utc 			= '420';
		$channel_id 		= $this->post('channel_id');
		$device 			= $this->post('device');
		$server_utc         = $this->Rumus->getGMTOffset();
        $interval           = $user_utc - $server_utc;
        $dt 				= new DateTime();       
        $todayInClient		= $dt->format('Y-m-d H:i:s');

        if ($device == "web") {
        	$limit = "8";
        }else{
        	$limit = "4";
        }

		$data 	= $this->db->query("SELECT tc.class_id, tc.name, tc.description, tc.start_time, tc.finish_time, tc.class_type, tc.participant, tc.channel_id, tu.user_name, tu.user_image, ms.subject_name, ms.jenjang_id FROM tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN master_subject as ms ON tc.subject_id=ms.subject_id WHERE tc.finish_time >= '$todayInClient' AND tc.channel_id='$channel_id' AND ( tc.tutor_id!=174000 AND tc.tutor_id!=450000 AND tc.tutor_id!=1053000) ORDER BY tc.start_time ASC LIMIT $limit")->result_array();
		if (empty($data)) {
			$rets['status'] = false;
			$rets['code'] 	= 404;
			$rets['message'] = "Not found";
		}
		else
		{
			foreach ($data as $row => $va) {
				$statusall = null; 	
				$jenjangnamelevel = array();
				$jenjangid = json_decode($va['jenjang_id'], TRUE);
				if ($jenjangid != NULL) {
					if(is_array($jenjangid)){
						$listclass = "20,15,16,17,18,19,1,2,3,4,5,9,12,6,10,13,7,11,14,8";
						
						foreach ($jenjangid as $key => $value) {
							$cats = explode(",", $listclass);
								foreach($cats as $cat) {		    
								    if ($cat == $value) {
								    	$statusall = 'Semua Kelas';
								    }
								    else
								    {
								    	$statusall = null;
								    }
								}						
								$jenjang_id[] = $value;
								if ($jenjang_id == $value) {																				
									$data[$row]['jenjang_idd'] = $value;
								}								
							$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$value'")->row_array();

							// $data[$row]['jenjang_level'] = $getAllSubject['jenjang_level'];										
							if ($getAllSubject['jenjang_level']=="0") {
								$jenjangnamelevel[] = $getAllSubject['jenjang_name'];
							}
							else{
								$jenjangnamelevel[] = $getAllSubject['jenjang_name'].' '.$getAllSubject['jenjang_level'];	
							}
							
							$data[$row]['jenjangnamelevel'] = implode(", ", $jenjangnamelevel);
						}
					}
					else
					{
						$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$jenjangid'")->row_array();
						$jenjangnamelevel[] = $getAllSubject['jenjang_name'].' - '.$getAllSubject['jenjang_level'];	
						$data[$row]['jenjangnamelevel'] = implode(", ", $jenjangnamelevel);			
					}
				}

	            $start_time         = DateTime::createFromFormat ('Y-m-d H:i:s',$va['start_time']);
	            $start_time->modify("+".$interval ." minutes");
	            $start_time         = $start_time->format('Y-m-d H:i:s');
	            $finish_time        = DateTime::createFromFormat ('Y-m-d H:i:s',$va['finish_time']);
	            $finish_time->modify("+".$interval ." minutes");
	            $finish_time        = $finish_time->format('Y-m-d H:i:s');
	            $date          		= date_create($start_time);
	            $datefinish         = date_create($finish_time);
	            $hari          		= date_format($date, 'l');
	            $tanggal            = date_format($date, 'd');
	            $bulan 				= date_format($date, 'F');
	            $tahun 		        = date_format($date, 'Y');
	            $waktu         		= date_format($date, 'H:i');
	            $waktufinish        = date_format($datefinish, 'H:i');
	            $ch 				= curl_init();

				curl_setopt($ch, CURLOPT_URL, 'https://classmiles.com/Tools/get_c/'.$va['class_id']);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$result 			= curl_exec($ch);
				curl_close($ch);
				$data[$row]['status_all'] = $statusall;
				$link 				= BASE_URL()."class?c=".trim($result);

	            $data[$row]['firstdate'] 	= $tanggal.' '.$bulan.' '.$tahun; 
	            $data[$row]['firsttime'] 	= $waktu;
	            $data[$row]['finishtime'] 	= $waktufinish;
	            $data[$row]['user_image'] 	= CDN_URL.USER_IMAGE_CDN_URL.$va['user_image'];
	            $data[$row]['link'] 		= $link;
	            if ($va['class_type'] == "multicast_paid") {
	            	$harga 	= $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id='$va[class_id]'")->row_array()['harga_cm'];
	            	$data[$row]['harga'] = $harga;
	            }
	            else
	            {
	            	$data[$row]['harga'] = 'Free';
	            }		     
	            $decode = json_decode($va['participant'],true);
	            $data[$row]['participant'] = $decode;
				if(isset($decode['participant'])){
					$length = count($decode['participant']);
					$data[$row]['participant_count'] = $length;
				}
				else
				{
					$data[$row]['participant_count'] = 0;
				}       
        	}
			$rets['status'] = true;
			$rets['code'] 	= 200;
			$rets['message'] = $data;

		}
		$this->response($rets);
	}

	public function tutorList_post()
	{
		$channel_id = $this->post('channel_id');
		
		$listTutor = $this->db->query("SELECT mct.*, tu.user_name, tu.id_user, tu.user_age, tu.user_image FROM master_channel_tutor as mct INNER JOIN tbl_user as tu ON mct.tutor_id=tu.id_user WHERE mct.channel_id='$channel_id'")->result_array();
		$year_experience = 0;
		$last_education = null;
		$nama_lembaga = null;
		$edu_score = 0;
		if (empty($listTutor)) {
			$rets['status'] 	= false;
			$rets['message'] 	= 'Terjadi kesalahan';
			$rets['code'] 		= 0;
			$rets['data'] = null;
			$this->response($rets);
		}
		else
		{
			// foreach ($listTutor as $keya => $v) {						
			// 	$data  = $this->db->query("SELECT * FROM tbl_user as tu LEFT JOIN tbl_profile_tutor as tpt ON tu.id_user=tpt.tutor_id WHERE tu.id_user='".$v['tutor_id']."' AND tu.usertype_id='tutor'")->row_array();

			// 	$data['education_background'] = unserialize($data['education_background']);
			// 	foreach ($data['education_background'] as $key => $value) {
			// 		$a = $this->db->query("SELECT jenjanglevel_score FROM master_jenjanglevel WHERE jenjanglevel_name='".$value['educational_level']."'")->row_array();
			// 		if(!empty($a)){
			// 			$a = $a['jenjanglevel_score'];
			// 		}else{
			// 			$a = 0;
			// 		}
			// 		if($a > $edu_score){
			// 			$edu_score = $a;
			// 			$data['nama_lembaga'] = $value['educational_institution'];
			// 			$last_education = $value['educational_level'];
			// 		}
			// 	}
			// 	if($edu_score >=4 ){
			// 		$listTutor[$keya]['last_education'] = $last_education;
			// 	}else{
			// 		$listTutor[$keya]['last_education'] = $last_education;					
			// 	}
			// 	$listTutor[$keya]['self_description'] = $data['self_description'];
			// }

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Sukses";
			$rets['data'] = $listTutor;
			$this->response($rets);
		}
		
		$this->response($rets);
	}

	public function Channel_UpdateProgram_post()
	{
		$v_accesstoken = $this->verify_access_token($this->get('access_token'));
		$program_id = $this->post('program_id');
		$channel_id = $this->post('channel_id');
		if ($v_accesstoken == false) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "invalid token.";
			goto ending;
		}
		$eventDateBefore 	= $this->post('eventDateBefore');
		$eventTimeBefore 	= $this->post('eventTimeBefore');
		$eventDurasiBefore 	= $this->post('eventDurasiBefore');
		$eventDateAfter 	= $this->post('eventDateAfter');
		$eventTimeAfter 	= $this->post('eventTimeAfter');
		$eventDurasiAfter 	= $this->post('eventDurasiAfter');

		$getProgram 		= $this->db->query("SELECT * FROM master_channel_program WHERE program_id='$program_id' AND channel_id='$channel_id'")->row_array();

		$schedule 			= $getProgram['schedule'];
		$json_schedule 		= json_decode($schedule,true);		
		$stat_upd 			= 0;
		foreach ($json_schedule as $key => $value) {
			if ($eventDateBefore == $value['eventDate'] && $eventTimeBefore == $value['eventTime'] && $eventDurasiBefore == $value['eventDurasi']) {				
				$json_schedule[$key]['eventDate'] 	= $eventDateAfter;
				$json_schedule[$key]['eventTime'] 	= $eventTimeAfter;
				$json_schedule[$key]['eventDurasi'] = $eventDurasiAfter;
				$stat_upd = 1;
			}
		}

		if ($stat_upd == 1) {
			$schedule_now 		= $this->db->escape_str(json_encode($json_schedule));
			$updateDataProgram 	= $this->db->simple_query("UPDATE master_channel_program SET schedule='$schedule_now' WHERE program_id='{$program_id}'");	

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Sukses";		
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "gagal";		
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function requestClassTutorBCU_post()
	{
		$tutor_id 		= $this->post('tutor_id');
		$cekRequest 	= $this->db->query("SELECT * FROM master_channel_program WHERE schedule LIKE '%\"$tutor_id\"%'")->result_array();
		if (!empty($cekRequest)) {
			$datat = array();
			foreach ($cekRequest as $keya => $valuee) {
				$program_id 		= $valuee['program_id'];
				$channel_id 		= $valuee['channel_id'];
				$sesi 				= $valuee['sesi'];
				$program_name 		= $valuee['program_name'];
				$group_id 			= $valuee['group_id'];
				$schedule 			= json_decode($valuee['schedule'],true);
				$subject_id 		= "";			
				$datat['program_id'] 	= $program_id;
				$datat['program_name'] 	= $program_name;
				$datat['channel_id']	= $channel_id;	
				foreach ($schedule as $keyo => $vo) {
					$eventDate		= $vo['eventDate'];
					$eventTime 		= $vo['eventTime'];
					$eventDurasi 	= $vo['eventDurasi'];
					$eventtutor_id 	= $vo['tutor_id'];
					$eventdesc 		= $vo['desc'];
					$subject_id 	= $vo['subject_id'];
					$eventstatus 	= $vo['status'];							

					if ($tutor_id == $eventtutor_id && $eventstatus == 2) {
						$datat['eventDate'] 	= $eventDate;
						$datat['eventTime'] 	= $eventTime;
						$datat['eventDurasi'] 	= $eventDurasi;
						$datat['tutor_id'] 	= $eventtutor_id;
						$datat['topik'] 		= $eventdesc;
						$datat['subject_id']	= $subject_id;
						$datat['eventstatus'] 	= $eventstatus;	
						break;
					}	
					else
					{						
						$datat['eventDate'] 	= null;
						$datat['eventTime'] 	= null;
						$datat['eventDurasi'] 	= null;
						$datat['tutor_id'] 		= null;
						$datat['topik'] 		= null;
						$datat['subject_id']	= null;
						$datat['eventstatus'] 	= null;	
					}
				}				

				$dataChannel 	= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$channel_id'")->row_array();

				$allmapel 		= $this->db->query("SELECT * FROM master_subject WHERE subject_id='$subject_id'")->result_array();
		        if(!empty($allmapel)){
					foreach ($allmapel as $row => $v) {
	                    $jenjangnamelevel = array();
	                    $jenjangid = json_decode($v['jenjang_id'], TRUE);
	                    if ($jenjangid != NULL) {
	                        if(is_array($jenjangid)){
	                            foreach ($jenjangid as $key => $value) {                                    
	                                $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
	                                if ($selectnew['jenjang_level']=='0') {
	                                    $jenjangnamelevel[] = $selectnew['jenjang_name'];
	                                }
	                                else{
	                                    $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
	                                } 
	                            }   
	                        }else{
	                            $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
	                            if ($selectnew['jenjang_level']=='0') {
	                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
	                            }
	                            else{
	                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
	                            }                                                         
	                        }                                       
	                    
	                        $jenjangnamelevel_c = implode(", ",$jenjangnamelevel);
	                        $datat['jenjangnamelevel'] = $jenjangnamelevel_c;
	                        $datat['subject_name'] 	= $v['subject_name'];
	                    }
	                }
	            }

				$datat['channel_name'] 	= $dataChannel['channel_name'];
				$datat['channel_logo'] 	= $dataChannel['channel_logo'];
				$datat['schedule']		= $schedule;

				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Sukses";
				$rets['data'] = $datat;
				
			}	
			$this->response($rets);			
		}
		else
		{
			$rets['status'] 	= false;
			$rets['code'] 		= -300;
			$rets['message'] 	= "Tidak ada data";
			$rets['data']		= null;
			$this->response($rets);
		}
	}

	public function requestClassTutor_post()
	{
		$tutor_id 		= $this->post('tutor_id');
		$cekRequest 	= $this->db->query("SELECT * FROM master_channel_program WHERE schedule LIKE '%\"$tutor_id\"%'")->result_array();
		if (!empty($cekRequest)) {
			$datat = array();
			$no = 1;
			foreach ($cekRequest as $keya => $valuee) {
				$program_id 		= $valuee['program_id'];
				$channel_id 		= $valuee['channel_id'];
				$sesi 				= $valuee['sesi'];
				$program_name 		= $valuee['program_name'];
				$group_id 			= $valuee['group_id'];
				$schedule 			= json_decode($valuee['schedule'],true);
				$subject_id 		= "";			
				$datat['program_id'] 	= $program_id;
				$datat['program_name'] 	= $program_name;
				$datat['channel_id']	= $channel_id;
					
				foreach ($schedule as $keyo => $vo) {
					$eventDate		= $vo['eventDate'];
					$eventTime 		= $vo['eventTime'];
					$eventDurasi 	= $vo['eventDurasi'];
					$eventtutor_id 	= $vo['tutor_id'];
					$eventdesc 		= $vo['desc'];
					$subject_id 	= $vo['subject_id'];
					$eventstatus 	= $vo['status'];							

					if ($tutor_id == $eventtutor_id && $eventstatus == 2) {
						$no = 0;
						$datat['eventDate'] 	= $eventDate;
						$datat['eventTime'] 	= $eventTime;
						$datat['eventDurasi'] 	= $eventDurasi;
						$datat['tutor_id'] 		= $eventtutor_id;
						$datat['topik'] 		= $eventdesc;
						$datat['subject_id']	= $subject_id;
						$datat['eventstatus'] 	= $eventstatus;
						break;
					}	
				}	
												
			}	
			if ($no == 0) {
				$dataChannel 	= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$channel_id'")->row_array();

				$allmapel 		= $this->db->query("SELECT * FROM master_subject WHERE subject_id='$subject_id'")->result_array();
		        if(!empty($allmapel)){
					foreach ($allmapel as $row => $v) {
	                    $jenjangnamelevel = array();
	                    $jenjangid = json_decode($v['jenjang_id'], TRUE);
	                    if ($jenjangid != NULL) {
	                        if(is_array($jenjangid)){
	                            foreach ($jenjangid as $key => $value) {                                    
	                                $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
	                                if ($selectnew['jenjang_level']=='0') {
	                                    $jenjangnamelevel[] = $selectnew['jenjang_name'];
	                                }
	                                else{
	                                    $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
	                                } 
	                            }   
	                        }else{
	                            $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
	                            if ($selectnew['jenjang_level']=='0') {
	                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
	                            }
	                            else{
	                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
	                            }                                                         
	                        }                                       
	                    
	                        $jenjangnamelevel_c = implode(", ",$jenjangnamelevel);
	                        $datat['jenjangnamelevel'] = $jenjangnamelevel_c;
	                        $datat['subject_name'] 	= $v['subject_name'];
	                    }
	                }
	            }

				$datat['channel_name'] 	= $dataChannel['channel_name'];
				$datat['channel_logo'] 	= $dataChannel['channel_logo'];	

				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Sukses";
				$rets['data'] = $datat;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = false;
				$rets['code'] = -100;
				$rets['message'] = "Failed";
				$rets['data'] = null;
				$this->response($rets);
			}
						
		}
		else
		{
			$rets['status'] 	= false;
			$rets['code'] 		= -300;
			$rets['message'] 	= "Tidak ada data";
			$rets['data']		= null;
			$this->response($rets);
		}
	}

	public function requestClassTutorByChannel_post()
	{
		$tutor_id 		= $this->post('tutor_id');
		$cekRequest 	= $this->db->query("SELECT * FROM master_channel_program WHERE schedule LIKE '%\"$tutor_id\"%'")->result_array();
		if (!empty($cekRequest)) {
			foreach ($cekRequest as $keya => $valuee) {
				$program_id 		= $valuee['program_id'];
				$channel_id 		= $valuee['channel_id'];
				$sesi 				= $valuee['sesi'];
				$group_id 			= $valuee['group_id'];
				$datat['program_id']= $valuee['program_id'];
				$datat['channel_id']= $valuee['channel_id'];
				$datat['sesi'] 		= $valuee['sesi'];
				$datat['group_id'] 	= $valuee['group_id'];
				$datat['program_name'] = $valuee['program_name'];
				$schedule 			= json_decode($valuee['schedule'],true);
				$subject_id 		= "";			
				foreach ($schedule as $keyo => $vo) {
					$eventDate		= $vo['eventDate'];
					$eventTime 		= $vo['eventTime'];
					$eventDurasi 	= $vo['eventDurasi'];
					$eventtutor_id 	= $vo['tutor_id'];
					$eventdesc 		= $vo['desc'];
					$subject_id 	= $vo['subject_id'];
					$eventstatus 	= $vo['status'];

					$allmapel 		= $this->db->query("SELECT * FROM master_subject WHERE subject_id='$subject_id'")->result_array();
			        if(!empty($allmapel)){
						foreach ($allmapel as $row => $v) {
		                    $jenjangnamelevel = array();
		                    $jenjangid = json_decode($v['jenjang_id'], TRUE);
		                    if ($jenjangid != NULL) {
		                        if(is_array($jenjangid)){
		                            foreach ($jenjangid as $key => $value) {                                    
		                                $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
		                                if ($selectnew['jenjang_level']=='0') {
		                                    $jenjangnamelevel[] = $selectnew['jenjang_name'];
		                                }
		                                else{
		                                    $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
		                                } 
		                            }   
		                        }else{
		                            $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
		                            if ($selectnew['jenjang_level']=='0') {
		                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
		                            }
		                            else{
		                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
		                            }                                                         
		                        }                                       
		                    
		                        $jenjangnamelevel_c = implode(", ",$jenjangnamelevel);
		                        $schedule[$keyo]['jenjangnamelevel'] = $jenjangnamelevel_c;
		                        $schedule[$keyo]['subject_name'] = $v['subject_name'];		                        
		                    }
		                }
		            }
				}				

				$dataChannel 	= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$channel_id'")->row_array();				

				$datat['channel_name'] 	= $dataChannel['channel_name'];
				$datat['channel_logo'] 	= $dataChannel['channel_logo'];
				$datat['schedule']		= $schedule;

				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Sukses";
				$rets['data'] = $datat;
				
			}	
			$this->response($rets);			
		}
		else
		{
			$rets['status'] 	= false;
			$rets['code'] 		= -300;
			$rets['message'] 	= "Tidak ada data";
			$rets['data']		= null;
			$this->response($rets);
		}
	}

	public function accept_RequestFromTutor_post()
	{		
		$program_id = $this->post('program_id');
		$channel_id = $this->post('channel_id');		
		$eventDate 	= $this->post('eventDate');
		$eventTime 	= $this->post('eventTime');
		$eventTimee = $this->post('eventTime').':00';
		$eventDurasi= $this->post('eventDurasi');
		$subject_id	= $this->post('subject_id');
		$tutor_id	= $this->post('tutor_id');
		$user_utc   = $this->post('user_utc');
		$topikpelajaran = "";
		$getProgram = $this->db->query("SELECT * FROM master_channel_program WHERE program_id='$program_id' AND channel_id='$channel_id'")->row_array();		
		if (!empty($getProgram)) {	
			$server_utc = $this->Rumus->getGMTOffset();
			$interval = $user_utc - $server_utc;
			$date_requested = DateTime::createFromFormat ('Y-m-d H:i:s', $eventDate." ".$eventTimee );
			$date_requested->modify("-".$interval ." minutes");

			$unix_requested = $date_requested->getTimestamp();
			$unix_now = time();

			$schedule 			= $getProgram['schedule'];
			$json_schedule 		= json_decode($schedule,true);		
			$stat_upd 			= 0;			
			foreach ($json_schedule as $key => $value) {
				if ($eventDate == $value['eventDate'] && $eventTime == $value['eventTime'] && $eventDurasi == $value['eventDurasi']) {
					$json_schedule[$key]['status'] 	= "1";
					$stat_upd = 1;
				}
			}

			if ($stat_upd == 1) {
				$schedule_now 		= $this->db->escape_str(json_encode($json_schedule));
				$updateDataProgram 	= $this->db->simple_query("UPDATE master_channel_program SET schedule='$schedule_now' WHERE program_id='{$program_id}'");	
				$schedule_new 		= $this->db->query("SELECT schedule FROM master_channel_program WHERE program_id='$program_id' AND channel_id='$channel_id'")->row_array()['schedule'];
				$json_schedule_new 	= json_decode($schedule_new,true);
				$updt_statusnya 	= 1;
				foreach ($json_schedule_new as $keyy => $v) {	
					if ($eventDate == $v['eventDate'] && $eventTime == $v['eventTime'] && $eventDurasi == $v['eventDurasi']) {
						$topikpelajaran = $this->db->escape_str($v['desc']);				
						if ($v['status'] != "1") {
							$updt_statusnya = 0;
						}
					}
				}

				// if ($updt_statusnya == 1) {
				$this->db->query("UPDATE master_channel_program SET status='$updt_statusnya' WHERE program_id='{$program_id}'");
				// }

				//PROSES ADD CLASS
				$namesubject = $this->db->query("SELECT * FROM master_subject WHERE subject_id='{$subject_id}'")->row_array()['subject_name'];
				

				$date_requested = $date_requested->format('Y-m-d H:i:s');
				$fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
				$fn_time->modify("+$eventDurasi second");
				$fn_time = $fn_time->format('Y-m-d H:i:s');

				// CHECK JIKA WAKTU TELAH TERISI
				$cjwtt = $this->db->query("SELECT * FROM tbl_class WHERE ( ( '$date_requested' <= start_time AND '$fn_time' > start_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) OR ( '$date_requested' >= start_time AND '$date_requested' < finish_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) ) AND tutor_id=$tutor_id ")->result_array();

				if(!empty($cjwtt)){
					$scheduleee 		= $this->db->query("SELECT schedule FROM master_channel_program WHERE program_id='$program_id' AND channel_id='$channel_id'")->row_array()['schedule'];
					$json_scheduleee 	= json_decode($scheduleee,true);
					foreach ($json_scheduleee as $key => $value) {
						if ($eventDate == $value['eventDate'] && $eventTime == $value['eventTime'] && $eventDurasi == $value['eventDurasi']) {
							$json_scheduleee[$key]['tutor_id'] 		= "";
							$json_scheduleee[$key]['subject_id'] 	= "";
							$json_scheduleee[$key]['desc'] 			= "";
							$json_scheduleee[$key]['status'] 		= "0";
						}
					}
					$scheduleeee 		= $this->db->escape_str(json_encode($json_scheduleee));
					$updateDataProgram 	= $this->db->simple_query("UPDATE master_channel_program SET schedule='$scheduleeee' WHERE program_id='{$program_id}'");	
					$rets['status'] = false;
					$rets['code'] = '403';
					$rets['message'] = "Maaf, Bentrok.";
					$this->response($rets);
				}

				$participant['visible'] = "private";
				$participant['participant'] = array();

				$exist = $this->db->query("SELECT point_id, active_point FROM uangpoint_channel WHERE channel_id='{$channel_id}' ")->row_array();
				if(!empty($exist)){
					$point 			= $exist['active_point'];
					$point_id 		= $exist['point_id'];
					$harga_multicast15menit = 1;

					// $getgroupid = $this->db->query("SELECT group_id FROM master_channel_program WHERE program_id='$program_id'")->row_array()['group_id'];
					// $grouplist 	= $this->db->query("SELECT group_participants FROM master_channel_group WHERE group_id='$getgroupid' OR group_id LIKE '%\"$getgroupid\"%'")->row_array()['group_participants'];
					// $json_g 	= json_decode($grouplist,true);
					// foreach ($json_g as $k => $vaa) {						
					// 	array_push($participant['participant'], array("id_user" => $vaa));
					// }

					$getgroupid 	= $this->db->query("SELECT participants FROM master_channel_program WHERE program_id='$program_id'")->row_array()['participants'];
				    $gp_id        	= json_decode($getgroupid,true);
				    foreach ($gp_id as $key => $value) {

				        // $grouplist  = $this->db->query("SELECT group_participants FROM master_channel_group WHERE group_id='$value' OR group_id LIKE '%\"$value\"%'")->row_array()['group_participants'];
				        // $json_g   = json_decode($grouplist,true);
				        // foreach ($json_g as $k => $vaa) {           
				            array_push($participant['participant'], array("id_user" => $value['id_user']));
				        // }
				    }
					
					$participant 	= json_encode($participant);                                               
					$this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,duration,participant,class_type,template_type,channel_id,channel_status) VALUES('{$subject_id}','{$namesubject}','{$topikpelajaran}','{$tutor_id}','{$date_requested}','{$fn_time}','{$eventDurasi}','{$participant}','multicast_channel_paid','all_featured','{$channel_id}','1')");
					$class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

					//INPUT CLASS ID 
					$schedulee 			= $this->db->query("SELECT schedule FROM master_channel_program WHERE program_id='$program_id' AND channel_id='$channel_id'")->row_array()['schedule'];
					$json_schedulee 	= json_decode($schedulee,true);
					$updt_statusnya 	= 1;
					foreach ($json_schedulee as $keyy => $v) {	
						if ($eventDate == $v['eventDate'] && $eventTime == $v['eventTime'] && $eventDurasi == $v['eventDurasi']) {
							$json_schedulee[$keyy]['class_id'] 	= $class_id;
						}
					}
					//END INPUT CLASSS ID

					// if ($updt_statusnya == 1) {
					$schedule_noww 		= $this->db->escape_str(json_encode($json_schedulee));
					$updateDataProgram 	= $this->db->simple_query("UPDATE master_channel_program SET schedule='$schedule_noww' WHERE program_id='{$program_id}'");
					// }

					$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$tutor_id'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						// echo $hash_id;
						$this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$class_id','$tutor_id','TRUE',0)");
					}

					$rets['status'] = true;
					$rets['code'] = 200;
					$rets['message'] = "Sukses";		
					$this->response($rets);
				}
				else
				{
					$rets['status'] = false;
					$rets['code'] = '-100';
					$rets['message'] = "Tidak memiliki point data";
					$this->response($rets);
				}				
			}
			else
			{
				$rets['status'] = false;
				$rets['code'] = -400;
				$rets['message'] = "gagal";		
				$this->response($rets);
			}
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -404;
			$rets['message'] = "Mohon maaf, tidak ditemukan program tersebut";		
			$this->response($rets);
		}
				
		$this->response($rets);
	}

	public function reject_RequestFromTutor_post()
	{		
		$program_id = $this->post('program_id');
		$channel_id = $this->post('channel_id');
		$tutor_id 	= $this->post('tutor_id');		
		$eventDate 	= $this->post('eventDate');
		$eventTime 	= $this->post('eventTime');
		$eventDurasi= $this->post('eventDurasi');

		$getProgram 		= $this->db->query("SELECT * FROM master_channel_program WHERE program_id='$program_id' AND channel_id='$channel_id'")->row_array();

		if (!empty($getProgram)) {					
			$schedule 			= $getProgram['schedule'];
			$json_schedule 		= json_decode($schedule,true);		
			$stat_upd 			= 0;			
			foreach ($json_schedule as $key => $value) {
				if ($value['tutor_id'] == $tutor_id && $value['status'] == "2") {					
					if ($eventDate == $value['eventDate'] && $eventTime == $value['eventTime'] && $eventDurasi == $value['eventDurasi']) {				
						$json_schedule[$key]['status'] 	= "0";					
						$stat_upd = 1;
					}	
				}			
			}

			if ($stat_upd == 1) {
				$schedule_now 		= $this->db->escape_str(json_encode($json_schedule));
				$updateDataProgram 	= $this->db->simple_query("UPDATE master_channel_program SET schedule='$schedule_now' WHERE program_id='{$program_id}'");					
				
				$this->db->query("UPDATE master_channel_program SET status='0' WHERE program_id='{$program_id}'");				

				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Sukses";		
				$this->response($rets);
			}
			else
			{
				$rets['status'] = false;
				$rets['code'] = -400;
				$rets['message'] = "gagal";		
				$this->response($rets);
			}
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -404;
			$rets['message'] = "Mohon maaf, tidak ditemukan program tersebut";		
			$this->response($rets);
		}
				
		$this->response($rets);
	}

	public function Channel_joindaftarMurid_post()
	{
		$channel_id 	= $this->post('channel_id');
		$id_user 		= $this->post('id_user');
		$user_type 		= $this->post('user_type');

		$ckChannel 		= $this->db->query("SELECT * FROM master_channel_student WHERE channel_id='$channel_id' AND id_user = '$id_user'")->row_array();
		if (empty($ckChannel)) {
			$this->db->query("INSERT INTO master_channel_student (channel_id, id_user, status) VALUES ('$channel_id','$id_user','inactive')");

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Sukses bergabung channel";		
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;
			$rets['message'] = "gagal";		
			$this->response($rets);
		}
	}

	public function Channel_daftarMurid_post()
	{
		$channel_id 	= $this->post('channel_id');
		$first_name 	= $this->post('first_name');
		$last_name 		= $this->post('last_name');
		$user_name 		= $first_name." ".$last_name;
		$email 			= $this->post('email');
		$kata_sandi 	= password_hash($this->post('kata_sandi'),PASSWORD_DEFAULT);
		$tanggal_lahir 	= $this->post('tanggal_lahir');
		$bulan_lahir 	= $this->post('bulan_lahir');
		$tahun_lahir 	= $this->post('tahun_lahir');
		$birthdate 		= $tahun_lahir."-".$bulan_lahir."-".$tanggal_lahir;
		$jenis_kelamin 	= $this->post('jenis_kelamin');
		$kode_area 		= $this->post('kode_area');
		$nama_negara 	= $this->post('nama_negara');
		$no_hape 		= $this->post('no_hape');
		$user_callnum 	= $kode_area."".$no_hape;
		$type 			= $this->post('type');
		$linkweb 		= $this->post('linkweb');
		if($type == "siswa")
		{
			$jenjang_id 	= $this->post('jenjang_id');	
		}
		else
		{
			$jenjang_id 	= "15";
		}
		
		list($year,$month,$day) = explode("-",$birthdate);
	    $year_diff  = date("Y") - $year;
	    $month_diff = date("m") - $month;
	    $day_diff   = date("d") - $day;
	    if ($month_diff < 0) $year_diff--;
	        elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
	    $umur 			= $year_diff;

		$cek 			= $this->db->query("SELECT * FROM tbl_user WHERE email='$email'")->row_array();
		if (empty($cek)) {

			$id_user 	= $this->Rumus->gen_id_user();
			$res = $this->db->simple_query("INSERT INTO tbl_user (id_user, user_name, first_name, last_name, email, password, user_birthdate, user_nationality,  user_countrycode, user_callnum, user_gender, usertype_id, user_image, st_tour, reg_by, user_age) VALUES ('$id_user','$user_name','$first_name','$last_name','$email','$kata_sandi','$birthdate','$nama_negara','$kode_area','$no_hape','$jenis_kelamin','student','".base64_encode('user/empty.jpg')."','0','Channel','$umur')");
			if ($res) {				
				$r_key = $this->M_login->randomKey();

				//TAMBAH NOTIF
				$notif_message = json_encode( array("code" => 33));
				$link = BASE_URL()."master/resendEmaill";
				$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','$link','0')");

				$simpan = $this->db->simple_query("INSERT INTO gen_auth (random_key, id_user) VALUES ('$r_key','$id_user')");
				$simpan_profile_student = $this->db->simple_query("INSERT INTO tbl_profile_student (student_id, jenjang_id) VALUES ('$id_user','$jenjang_id')");

				//ADD STUDENT TO CHANNEL
				$ck_student = $this->db->query("SELECT * FROM master_channel_student WHERE id_user = '$id_user' AND channel_id ='$channel_id'")->row_array();
				if (empty($ck_student)) {
					$this->db->query("INSERT INTO master_channel_student (channel_id, id_user) VALUES ('$channel_id','$id_user') ");
				}

				if ($channel_id == '46') {
					$banner_channel = "https://cdn.classmiles.com/sccontent/banner/Email_Header_rlt.png";
				}
				else if ($channel_id == '52') {
					$banner_channel = "https://cdn.classmiles.com/sccontent/banner/Email_Header_fisipro.png";
				}
				else
				{
					$banner_channel = "https://cdn.classmiles.com/sccontent/maillogo_classmiles.png";
				}

				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' =>'info@classmiles.com',
					'smtp_pass' => 'kerjadiCLASSMILES',	
					'mailtype' => 'html',
					'charset' => 'iso-8859-1'
				);

				$getChannelData 	= $this->db->query("SELECT * FROM master_channel WHERE channel_id = '$channel_id'")->row_array();
				$channel_name 		= $getChannelData['channel_name'];
				$this->load->library('email', $config);
				$this->email->initialize($config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('info@classmiles.com', 'Info '.$channel_name);
				$this->email->to($email);
				$this->email->subject($channel_name.' - Account Activation');
				$templateverifikasi = 
				"
				<div style='height:100%;margin:0;padding:0;width:100%;background-color:#fafafa'>
				    <center>
				        <table align='center' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' style='border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fafafa'>
				            <tbody>
				                <tr>
				                    <td align='center' valign='top' style='height:100%;margin:0;padding:10px;width:100%;border-top:0'>
				                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse:collapse;border:0;max-width:600px!important'>
				                            <tbody>
				                                <tr>
				                                    <td valign='top' style='background:#ffffff none no-repeat center/cover;background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:0'>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top'>
				                                                        <table align='left' width='100%' border='0' cellpadding='0' cellspacing='0' style='min-width:100%;border-collapse:collapse'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='text-align:center'>
				                                                                        <img align='center' alt='' src='".$banner_channel."' width='100%' style='max-width:700px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none'>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top' style='padding-top:9px'>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
				                                                                        <p style='margin:10px 0;padding:0;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
				                                                                            Hello ".$user_name.",<br><br>                                  
				                                                                            Thank you for signing up at ".$channel_name.".
				                                                                            <br><br>
				                                                                            Kindly click below link to activate your account at ".$linkweb.". Please do not reply this email.<br>
				                                                                            <br>
				                                                                            <b><i>Enjoy the new experience on learning and share it with others!</i></b>
				                                                                            <br>
				                                                                        </p>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse; margin-top: 15px;' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' class='m_2872471405169675079mcnTextContent' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <em>Thank you,</em><br>
				                                                                        <em>".$channel_name." Team</em>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse; margin-top: 15px;' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' class='m_2872471405169675079mcnTextContent' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <hr>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                    </td>
				                                </tr>                                				                                
				                                <tr>
				                                    <td valign='top' style='background:#ffffff none no-repeat center/cover;background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:2px solid #eaeaea;padding-top:0;padding-bottom:9px'>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td style='padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px' valign='top' align='center'>
				                                                        <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate!important;border-radius:3px;background-color:#20b172'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td align='center' valign='middle' style='font-family:Arial;font-size:16px;padding:15px'>
				                                                                        <a title='Verifikasi' href='".$linkweb."first/verifikasi/".$r_key."' style='font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color:#ffffff;display:block' target='_blank' >
				                                                                            Activate/Aktifasi
				                                                                        </a>
				                                                                    </td>                                                                    
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate!important;border-radius:3px;'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td align='center' valign='middle' style='font-family:Arial;font-size:16px;padding:15px'>
				                                                                        OR
				                                                                    </td>                                                                    
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate!important;border-radius:3px;'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td align='center' valign='middle' style='font-family:Arial;font-size:12px;padding:15px'>
				                                                                        <a href='".$linkweb."first/verifikasi/".$r_key."' target='_blank'>
				                                                                            ".$linkweb."first/verifikasi/".$r_key."
				                                                                        </a>
				                                                                    </td>                                                                    
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                    </td>
				                                </tr>
				                                <tr>
				                                    <td valign='top' style='background:#fafafa none no-repeat center/cover;background-color:#fafafa;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px'>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top' style='padding-top:9px'>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <em>Copyright &copy; 2018 ".$channel_name.", All rights reserved.</em><br>                                                                    
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                    </td>
				                                </tr>
				                            </tbody>
				                        </table>
				                    </td>
				                </tr>
				            </tbody>
				        </table>
				    </center>
				</div>
				";
				/**/				
				$this->email->message($templateverifikasi);

				if ($this->email->send()) 
				{
					$rets['status'] = true;
					$rets['code'] = 200;
					$rets['message'] = "Pendaftaran Sukses";		
					$this->response($rets);
				}
				else
				{
					$rets['status'] = false;
					$rets['code'] = -101;
					$rets['message'] = "Gagal Mengirim email ";
					$this->response($rets);		
				}
			}
			else
			{
				$rets['status'] = false;
				$rets['code'] = -100;
				$rets['message'] = "Pendaftaran gagal";
				$this->response($rets);
			}
		}
		else
		{			
			$rets['status'] = false;
			$rets['code'] 	= -102;
			$rets['message']= "Siswa tersebut sudah terdaftar!!!";
			$rets['user_type'] = $cek['usertype_id'];
			$rets['id_user'] = $cek['id_user'];
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function Channel_joindaftarTutor_post()
	{
		$channel_id 	= $this->post('channel_id');
		$tutor_id 		= $this->post('id_user');
		$user_type 		= $this->post('user_type');

		$ckChannel 		= $this->db->query("SELECT * FROM master_channel_tutor WHERE channel_id='$channel_id' AND tutor_id = '$tutor_id'")->row_array();
		if (empty($ckChannel)) {
			$this->db->query("INSERT INTO master_channel_tutor (channel_id, tutor_id, status) VALUES ('$channel_id','$tutor_id','inactive')");

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Sukses bergabung channel";		
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;
			$rets['message'] = "gagal";		
			$this->response($rets);
		}
	}

	public function Channel_daftarTutor_post()
	{
		$channel_id 	= $this->post('channel_id');
		$first_name 	= $this->post('first_name');
		$last_name 		= $this->post('last_name');
		$user_name 		= $first_name." ".$last_name;
		$email 			= $this->post('email');
		$kata_sandi 	= password_hash($this->post('kata_sandi'),PASSWORD_DEFAULT);
		$tanggal_lahir 	= $this->post('tanggal_lahir');
		$bulan_lahir 	= $this->post('bulan_lahir');
		$tahun_lahir 	= $this->post('tahun_lahir');
		$birthdate 		= $tahun_lahir."-".$bulan_lahir."-".$tanggal_lahir;
		$jenis_kelamin 	= $this->post('jenis_kelamin');
		$kode_area 		= $this->post('kode_area');
		$nama_negara 	= $this->post('nama_negara');
		$no_hape 		= $this->post('no_hape');
		$user_callnum 	= $kode_area."".$no_hape;
		$type 			= $this->post('type');
		$linkweb 		= $this->post('linkweb');
		if($type == "siswa")
		{
			$jenjang_id 	= $this->post('jenjang_id');	
		}
		else
		{
			$jenjang_id 	= "15";
		}
		
		list($year,$month,$day) = explode("-",$birthdate);
	    $year_diff  = date("Y") - $year;
	    $month_diff = date("m") - $month;
	    $day_diff   = date("d") - $day;
	    if ($month_diff < 0) $year_diff--;
	        elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
	    $umur 			= $year_diff;

		$cek 			= $this->db->query("SELECT * FROM tbl_user WHERE email='$email'")->row_array();
		if (empty($cek)) {
			$id_user 	= $this->Rumus->gen_id_user();
			$res = $this->db->simple_query("INSERT INTO tbl_user (id_user, user_name, first_name, last_name, email, password, user_birthdate, user_nationality,  user_countrycode, user_callnum, user_gender, usertype_id, user_image, st_tour, reg_by, user_age) VALUES ('$id_user','$user_name','$first_name','$last_name','$email','$kata_sandi','$birthdate','$nama_negara','$kode_area','$no_hape','$jenis_kelamin','tutor','".base64_encode('user/empty.jpg')."','0','Channel','$umur')");
			if ($res) {				
				$r_key = $this->M_login->randomKey();

				//TAMBAH NOTIF
				$notif_message = json_encode( array("code" => 33));
				$link = BASE_URL()."master/resendEmaill";
				$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','$link','0')");

				$simpan = $this->db->simple_query("INSERT INTO gen_auth (random_key, id_user) VALUES ('$r_key','$id_user')");				
				$simpan_profile_tutor = $this->db->simple_query("INSERT INTO tbl_profile_tutor (tutor_id) VALUES ('$id_user')");
				
				//ADD TUTOR TO CHANNEL
				$ck_student = $this->db->query("SELECT * FROM master_channel_tutor WHERE tutor_id = '$id_user' AND channel_id ='$channel_id'")->row_array();
				if (empty($ck_student)) {
					$this->db->query("INSERT INTO master_channel_tutor (channel_id, tutor_id, status) VALUES ('$channel_id','$id_user','inactive') ");
				}

				if ($channel_id == '46') {
					$banner_channel = "https://cdn.classmiles.com/sccontent/banner/Email_Header_rlt.png";
				}
				else if ($channel_id == '52') {
					$banner_channel = "https://cdn.classmiles.com/sccontent/banner/Email_Header_fisipro.png";
				}
				else
				{
					$banner_channel = "https://cdn.classmiles.com/sccontent/maillogo_classmiles.png";
				}

				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' =>'info@classmiles.com',
					'smtp_pass' => 'kerjadiCLASSMILES',	
					'mailtype' => 'html',
					'charset' => 'iso-8859-1'
				);

				$getChannelData 	= $this->db->query("SELECT * FROM master_channel WHERE channel_id = '$channel_id'")->row_array();
				$channel_name 		= $getChannelData['channel_name'];

				$this->load->library('email', $config);
				$this->email->initialize($config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('info@classmiles.com', 'Info '.$channel_name);
				$this->email->to($email);
				$this->email->subject($channel_name.' - Account Activation');
				$templateverifikasi = 
				"
				<div style='height:100%;margin:0;padding:0;width:100%;background-color:#fafafa'>
				    <center>
				        <table align='center' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' style='border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fafafa'>
				            <tbody>
				                <tr>
				                    <td align='center' valign='top' style='height:100%;margin:0;padding:10px;width:100%;border-top:0'>
				                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse:collapse;border:0;max-width:600px!important'>
				                            <tbody>
				                                <tr>
				                                    <td valign='top' style='background:#ffffff none no-repeat center/cover;background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:0'>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top'>
				                                                        <table align='left' width='100%' border='0' cellpadding='0' cellspacing='0' style='min-width:100%;border-collapse:collapse'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='text-align:center'>
				                                                                        <img align='center' alt='' src='".$banner_channel."' width='100%' style='max-width:700px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none'>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top' style='padding-top:9px'>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
				                                                                        <p style='margin:10px 0;padding:0;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
				                                                                            Hello ".$user_name.",<br><br>                                  
				                                                                            Thank you for signing up at ".$channel_name.".
				                                                                            <br><br>
				                                                                            Kindly click below link to activate your account at ".$linkweb.". Please do not reply this email.<br>
				                                                                            <br>
				                                                                            <b><i>Enjoy the new experience on learning and share it with others!</i></b>
				                                                                            <br>
				                                                                        </p>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse; margin-top: 15px;' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' class='m_2872471405169675079mcnTextContent' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <em>Thank you,</em><br>
				                                                                        <em>".$channel_name." Team</em>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse; margin-top: 15px;' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' class='m_2872471405169675079mcnTextContent' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <hr>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                    </td>
				                                </tr>				                                
				                                <tr>
				                                    <td valign='top' style='background:#ffffff none no-repeat center/cover;background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:2px solid #eaeaea;padding-top:0;padding-bottom:9px'>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td style='padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px' valign='top' align='center'>
				                                                        <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate!important;border-radius:3px;background-color:#20b172'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td align='center' valign='middle' style='font-family:Arial;font-size:16px;padding:15px'>
				                                                                        <a title='Verifikasi' href='".$linkweb."first/verifikasiTutor/".$r_key."' style='font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color:#ffffff;display:block' target='_blank' >
				                                                                            Activate/Aktifasi
				                                                                        </a>
				                                                                    </td>                                                                    
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate!important;border-radius:3px;'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td align='center' valign='middle' style='font-family:Arial;font-size:16px;padding:15px'>
				                                                                        OR
				                                                                    </td>                                                                    
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate!important;border-radius:3px;'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td align='center' valign='middle' style='font-family:Arial;font-size:12px;padding:15px'>
				                                                                        <a href='".$linkweb."first/verifikasiTutor/".$r_key."' target='_blank'>
				                                                                            ".$linkweb."first/verifikasiTutor/".$r_key."
				                                                                        </a>
				                                                                    </td>                                                                    
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                    </td>
				                                </tr>
				                                <tr>
				                                    <td valign='top' style='background:#fafafa none no-repeat center/cover;background-color:#fafafa;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px'>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top' style='padding-top:9px'>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <em>Copyright &copy; 2018 ".$channel_name.", All rights reserved.</em><br>                                                                    
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                    </td>
				                                </tr>
				                            </tbody>
				                        </table>
				                    </td>
				                </tr>
				            </tbody>
				        </table>
				    </center>
				</div>
				";
				/**/				
				$this->email->message($templateverifikasi);

				if ($this->email->send()) 
				{
					$rets['status'] = true;
					$rets['code'] = 200;
					$rets['message'] = "Pendaftaran Sukses";		
					$this->response($rets);
				}
				else
				{
					$rets['status'] = false;
					$rets['code'] = -101;
					$rets['message'] = "Gagal Mengirim email ";
					$this->response($rets);		
				}
			}
			else
			{
				$rets['status'] = false;
				$rets['code'] = -100;
				$rets['message'] = "Pendaftaran gagal";
				$this->response($rets);
			}
		}
		else
		{			
			$rets['status'] = false;
			$rets['code'] 	= -102;
			$rets['message']= "Tutor tersebut sudah terdaftar!!!";
			$rets['user_type'] = $cek['usertype_id'];
			$rets['id_user'] = $cek['id_user'];
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function ListJenjang_post()
	{	
		$curriculum 	= $this->post('curriculum');		
		if ($curriculum == '') {
			$list_jenjang 	= $this->db->query("SELECT jenjang_id as id , CONCAT(jenjang_name,' ',jenjang_level) as text FROM master_jenjang WHERE jenjang_name NOT LIKE 'UMUM' and jenjang_name NOT LIKE 'TK' and jenjang_name NOT LIKE 'SD' order by jenjang_level")->result_array();					
		}
		else
		{
			$list_jenjang 	= $this->db->query("SELECT jenjang_id as id , CONCAT(jenjang_name,' ',jenjang_level) as text FROM master_jenjang WHERE jenjang_name NOT LIKE 'UMUM' and jenjang_name NOT LIKE 'TK' AND curriculum = '$curriculum' order by jenjang_level")->result_array();				

		}
		if (empty($list_jenjang)) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "failed";
			$rets['list_jenjang'] = null;		
			$this->response($rets);
		}
		else
		{
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Sukses";
			$rets['list_jenjang'] = $list_jenjang;		
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function ListCountry_post()
	{		
		$list_country = $this->db->query("SELECT * FROM master_country")->result_array();
		if (empty($list_country)) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "failed";
			$rets['list_country'] = null;		
			$this->response($rets);
		}
		else
		{
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Sukses";
			$rets['list_country'] = $list_country;		
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function saveLocation_post()
	{	
		$lat = $this->post('lat');
		$lng = $this->post('lng');
		$save = $this->db->query("INSERT INTO log_locations (lat, lng) VALUES ('$lat','$lng')");
		if (empty($save)) {
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "failed";
			$rets['lat'] = null;
			$rets['lng'] = null;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Sukses";
			$rets['lat'] = $lat;
			$rets['lng'] = $lng;
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function channel_addPackage_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	

		$channel_id 	= $this->post('channel_id');
		$kuota 			= $this->post('kuota');
		$start_date 	= $this->post('start_date').'-01';
		$end_date 		= $this->post('end_date');
		$end_date		= date("Y-m-t", strtotime($end_date));
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$cekPackage = $this->db->query("SELECT * FROM master_channel_package WHERE ( ( '$start_date' <= start_date AND '$end_date' > start_date AND ( '$end_date' <= end_date OR '$end_date' > end_date )  ) OR ( '$start_date' >= start_date AND '$start_date' < end_date AND ( '$end_date' <= end_date OR '$end_date' > end_date )  ) ) AND channel_id = '$channel_id' AND (status_active='1' OR status_active='0')")->row_array();	
		if (empty($cekPackage)) {
			$insrt = $this->db->query("INSERT INTO master_channel_package (channel_id, kuota, start_date, end_date) VALUES ('$channel_id', '$kuota' , '$start_date' , '$end_date')");

			$myip = $_SERVER['REMOTE_ADDR'];
			$plogc_id = $this->Rumus->getLogChnTrxID();
			$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Add package','','$myip',now())");
			
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Berhasil menambah package";
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;
			$rets['message'] = "Package already and active";
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function change_join_student_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	

		$channel_id 	= $this->post('channel_id');
		$id_user 		= $this->post('id_user');
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$cekActin = $this->db->query("SELECT * FROM `master_channel_student` WHERE channel_id='$channel_id' AND id_user='$id_user'")->row_array();	
		if (!empty($cekActin)) {
			if ($cekActin['status'] == 'inactive') {
				$insrt = $this->db->query("UPDATE master_channel_student SET flag_membership = '1' WHERE channel_id='$channel_id' AND id_user='$id_user'");

				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Approve Student $id_user','','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil active student";
				$this->response($rets);
			}
			else
			{
				$insrt = $this->db->query("UPDATE master_channel_student SET flag_membership = '-1' WHERE channel_id='$channel_id' AND id_user='$id_user'");

				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Reject Student $id_user','','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil inactive student";
				$this->response($rets);
			}
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;
			$rets['message'] = "Not found";
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function change_actIn_student_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	

		$channel_id 	= $this->post('channel_id');
		$id_user 		= $this->post('id_user');
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$cekActin = $this->db->query("SELECT * FROM `master_channel_student` WHERE channel_id='$channel_id' AND id_user='$id_user'")->row_array();	
		if (!empty($cekActin)) {
			if ($cekActin['status'] == 'inactive') {
				$insrt = $this->db->query("UPDATE master_channel_student SET status = 'active' WHERE channel_id='$channel_id' AND id_user='$id_user'");

				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Active Student $id_user','','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil active student";
				$this->response($rets);
			}
			else
			{
				$insrt = $this->db->query("UPDATE master_channel_student SET status = 'inactive' WHERE channel_id='$channel_id' AND id_user='$id_user'");

				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','InActive Student $id_user','','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil inactive student";
				$this->response($rets);
			}
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;
			$rets['message'] = "Not found";
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function change_actIn_tutor_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	

		$channel_id 	= $this->post('channel_id');
		$tutor_id 		= $this->post('tutor_id');
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$cekActin = $this->db->query("SELECT * FROM `master_channel_tutor` WHERE channel_id='$channel_id' AND tutor_id='$tutor_id'")->row_array();	
		if (!empty($cekActin)) {
			if ($cekActin['status'] == 'inactive') {
				$insrt = $this->db->query("UPDATE master_channel_tutor SET status = 'active' WHERE channel_id='$channel_id' AND tutor_id='$tutor_id'");

				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Active tutor $tutor_id','','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil active tutor";
				$this->response($rets);
			}
			else
			{
				$insrt = $this->db->query("UPDATE master_channel_tutor SET status = 'inactive' WHERE channel_id='$channel_id' AND tutor_id='$tutor_id'");

				$myip = $_SERVER['REMOTE_ADDR'];
				$plogc_id = $this->Rumus->getLogChnTrxID();
				$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','InActive tutor $tutor_id','','$myip',now())");
				
				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = "Berhasil inactive tutor";
				$this->response($rets);
			}
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;
			$rets['message'] = "Not found";
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function channel_joinClass_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	
		$id_user 	= $this->post('id_user');
		$class_id 	= $this->post('class_id');
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$qq = $this->db->query("SELECT tc.*, tcp.harga, tcp.harga_cm FROM tbl_class as tc LEFT JOIN tbl_class_price as tcp ON tc.class_id=tcp.class_id WHERE tc.class_id='$class_id' AND tc.participant NOT LIKE '%\"$id_user\":\"$id_user\"%'")->row_array();

		if (!empty($qq)) {
			$participant_arr = json_decode($qq['participant'],true);
			if(!array_key_exists('participant', $participant_arr)){				
				$participant_arr['participant'] = array();
			}

			array_push($participant_arr['participant'], array("id_user" => $id_user));
			// echo json_encode($participant_arr);
			$participant_arr = json_encode($participant_arr);
			$this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
			
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Anda berhasil mengikuti kelas";
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}	

	public function change_actIn_package_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	

		$channel_id 	= $this->post('channel_id');
		$pid 			= $this->post('pid');
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$date = date('Y-m-d');

		$data = $this->db->query("SELECT * FROM master_channel_package WHERE status_active=0 AND channel_id='$channel_id' AND pid='$pid'")->row_array();
		if (!empty($data)) {
						
			$kuota 		= $data['kuota'];
			// LIST TOTAL ACTIVE MEMBER ON CHANNEL_TUTOR AND CHANNEL_STUDENT
			$total_active_tutor = $this->db->query("SELECT COUNT(*) FROM master_channel_tutor WHERE channel_id={$channel_id} AND status='active' ")->row_array()['COUNT(*)'];
			$total_active_student = $this->db->query("SELECT COUNT(*) FROM master_channel_student WHERE channel_id={$channel_id} AND status='active' ")->row_array()['COUNT(*)'];

			$interval = $kuota - ( intval($total_active_student) + intval($total_active_tutor) );
			if($interval > 0){
				$unactived_tutor = $this->db->query("SELECT * FROM master_channel_tutor WHERE channel_id={$channel_id} AND status='inactive' ORDER BY created_at ASC limit 0,{$interval} ")->result_array();
				if(!empty($unactived_tutor)){
					$interval -= count($unactived_tutor);
					$to_update = "";
					foreach ($unactived_tutor as $k => $v) {
						$to_update .= "tutor_id=".$v['tutor_id']." OR ";
					}
					$to_update = rtrim($to_update, " OR ");
					$this->db->simple_query("UPDATE master_channel_tutor SET status='active' WHERE $to_update ");
				}
				if($interval > 0){
					$unactived_student = $this->db->query("SELECT * FROM master_channel_student WHERE channel_id={$channel_id} AND status='inactive' ORDER BY created_at ASC limit 0,{$interval} ")->result_array();
					if(!empty($unactived_student)){
						$interval -= count($unactived_student);
						$to_update = "";
						foreach ($unactived_student as $k => $v) {
							$to_update .= "id_user=".$v['id_user']." OR ";
						}
						$to_update = rtrim($to_update, " OR ");
						$this->db->simple_query("UPDATE master_channel_student SET status='active' WHERE $to_update ");
					}
				}
				
			}else if($interval < 0){
				$interval_sementara = $interval*-1;
				$actived_student = $this->db->query("SELECT * FROM master_channel_student WHERE channel_id={$channel_id} AND status='active' ORDER BY created_at DESC limit 0,{$interval_sementara} ")->result_array();
				if(!empty($actived_student)){
					$interval += count($actived_student);
					$to_update = "";
					foreach ($actived_student as $k => $v) {
						$to_update .= "id_user=".$v['id_user']." OR ";
					}
					$to_update = rtrim($to_update, " OR ");
					$this->db->simple_query("UPDATE master_channel_student SET status='inactive' WHERE $to_update ");
				}
				if($interval < 0){
					$interval_sementara = $interval*-1;
					$actived_tutor = $this->db->query("SELECT * FROM master_channel_tutor WHERE channel_id={$channel_id} AND status='active' ORDER BY created_at DESC limit 0,{$interval_sementara} ")->result_array();
					if(!empty($actived_tutor)){
						$interval += count($actived_tutor);
						$to_update = "";
						foreach ($actived_tutor as $k => $v) {
							$to_update .= "tutor_id=".$v['tutor_id']." OR ";
						}
						$to_update = rtrim($to_update, " OR ");
						$this->db->simple_query("UPDATE master_channel_tutor SET status='inactive' WHERE $to_update ");
					}
				}
			}		

			$insrt = $this->db->query("UPDATE master_channel_package SET status_active = 1 WHERE channel_id='$channel_id' AND pid='$pid'");

			$myip = $_SERVER['REMOTE_ADDR'];
			$plogc_id = $this->Rumus->getLogChnTrxID();
			$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Active package channel $channel_id','','$myip',now())");
			
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Berhasil active package";
			$this->response($rets);			
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;
			$rets['message'] = "Not found";
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function channel_unjoinClass_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	
		$id_user 	= $this->post('id_user');
		$class_id 	= $this->post('class_id');
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$qq = $this->db->query("SELECT tc.*, tcp.harga, tcp.harga_cm FROM tbl_class as tc LEFT JOIN tbl_class_price as tcp ON tc.class_id=tcp.class_id WHERE tc.class_id='$class_id' AND tc.participant NOT LIKE '%\"$id_user\":\"$id_user\"%'")->row_array();

		if (!empty($qq)) {
			$participant_arr = json_decode($qq['participant'],true);
			if(!array_key_exists('participant', $participant_arr)){				
				$participant_arr['participant'] = array();
			}
			$participant_new['visible'] = "followers";
			$participant_new['participant'] = array();

			foreach ($participant_arr['participant'] as $key => $value) {
				if ($value['id_user'] != $id_user) {
					array_push($participant_new['participant'], array("id_user" => $value['id_user']));		
				}
			}
			
			// unset($participant_arr['participant'], array("id_user" => $id_user));
			// array_push($participant_arr['participant'], array("id_user" => $id_user));
			// echo json_encode($participant_arr);
			$participant_new = json_encode($participant_new);
			$this->db->simple_query("UPDATE tbl_class SET participant='$participant_new' WHERE class_id='$class_id'");
			
			$rets['status'] 	= true;
			$rets['code'] 		= 200;
			$rets['message'] 	= "Anda berhasil mengikuti kelas";
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function listNationality_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));			
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$qq = $this->db->query("SELECT * FROM master_country ORDER BY id ASC")->result_array();

		if (!empty($qq)) {			
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['data'] = $qq;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['data'] = null;
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function detail_student_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	
		$id_user 	= $this->post('id_user');		
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$qq = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$id_user'")->row_array();

		if (!empty($qq)) {			
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['data'] = $qq;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['data'] = null;
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function success_upgrade_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	
		$id_user 	= $this->post('id_user');
		$channel_id 	= $this->post('channel_id');
		$member_select 	= $this->post('member_select');
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$qq = $this->db->query("SELECT * FROM master_channel_student WHERE id_user='$id_user' AND channel_id='$channel_id'")->row_array();

		if (!empty($qq)) {	
			$insrt = $this->db->query("UPDATE master_channel_student SET member_ship = '$member_select' WHERE channel_id='$channel_id' AND id_user='$id_user'");

			$myip = $_SERVER['REMOTE_ADDR'];
			$plogc_id = $this->Rumus->getLogChnTrxID();
			$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Change membership $id_user to $member_select','','$myip',now())");	

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'Sucess upgrade to $member_select';
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = 'Failed upgrade';
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function DetailClass_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));		
		$channel_id 	= $this->post('channel_id');
		$class_id 		= $this->post('class_id');
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$qq = $this->db->query("SELECT tu.user_name, tu.user_image, tpt.self_description, tc.* FROM tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN tbl_profile_tutor as tpt ON tpt.tutor_id=tu.id_user WHERE tc.class_id = '$class_id'")->row_array();

		if (!empty($qq)) {				
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'Sucess upgrade to $member_select';
			$rets['data'] = $qq;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = 'Failed upgrade';
			$rets['data'] = null;
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function list_PricePackage_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));		
		$channel_id 	= $this->post('channel_id');		
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$qq = $this->db->query("SELECT mcp.program_id, mcp.program_name, tpp.* FROM tbl_price_program as tpp INNER JOIN master_channel_program as mcp ON mcp.program_id=tpp.program_id  WHERE tpp.channel_id = '$channel_id'")->result_array();

		if (!empty($qq)) {
			foreach ($qq as $key => $value) {
				$expired_start 	= $value['expired_start'];
				$expired_start 	= date("d/m/Y", strtotime($expired_start));
				
				$expired_finish = $value['expired_finish'];
				$expired_finish = date("d/m/Y", strtotime($expired_finish));

				$qq[$key]['expired_start'] = $expired_start;
				$qq[$key]['expired_finish'] = $expired_finish;
			}				

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'List Package Price';
			$rets['data'] = $qq;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -300;
			$rets['message'] = 'Empty';
			$rets['data'] = null;
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function listdata_Program_post()
	{
		// $v_accesstoken 	= $this->verify_access_token($this->get('access_token'));
		// if ($v_accesstoken == "") {
		// 	$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
		// 	goto ending;
		// }
		$id_user = $this->post('id_user');
		$type = $this->post('type') != null ? "WHERE type='".$this->post('type')."' " : "";
		$jenjang_id = $this->post('jenjang_id') != null ? "jenjang_id LIKE '%\"".$this->post('jenjang_id')."\"%' " : '';


		$anak = $this->db->query("SELECT * FROM tbl_profile_kid WHERE parent_id='$id_user'")->result_array();
            $q_anak = "";
            if(!empty($anak)){
                foreach ($anak as $key => $value) {
                	$jd = $value['jenjang_id'];                    	
                	$q_anak.= "OR jenjang_id LIKE '%\"$jd\"%' ";
                }
            }

        if ($jenjang_id == "") {
			$qq = $this->db->query("SELECT tpp.* , mc.channel_name  from tbl_price_program as tpp INNER JOIN master_channel as mc on tpp.channel_id = mc.channel_id $type and tpp.status_display ='1'")->result_array();
		} else {
			$qq = $this->db->query("SELECT tpp.* , mc.channel_name  from tbl_price_program as tpp INNER JOIN master_channel as mc on tpp.channel_id = mc.channel_id WHERE $jenjang_id $q_anak and tpp.status_display ='1'")->result_array();
		}
		

		if (!empty($qq)) {				
			
			foreach ($qq as $key => $value) {
				$qq[$key]["bidang"] = json_decode($value['bidang'],true);
				$qq[$key]["fasilitas"] = json_decode($value['fasilitas'],true);
				$qq[$key]["jenjang_id"] = json_decode($value['jenjang_id'],true);
			}
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "List Package Price";
			$rets['data'] = $qq;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -300;
			$rets['message'] = "Empty";
			$rets['data'] = null;
			$this->response($rets);
		}
		
		// $rets['status'] = false;
		// $rets['message'] = "Field incomplete";
		// ending:
		$this->response($rets);
	}

	public function save_PricePackage_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));		
		$channel_id 	= $this->post('channel_id');		
		$name 			= $this->post('name');
		$description 	= $this->post('description');
		$price 			= $this->post('price');
		$jenjang_id 	= json_encode($this->post('jenjang_id'),true);
		$bidang 		= json_encode($this->post('bidang'),true);
		$fasilitas 		= json_encode($this->post('fasilitas'),true);
		$type 			= $this->post('type');
		$program_id 	= $this->post('program_id');
		$image_new 		= $this->post('image_new');
		$image_new_android 	= $this->post('image_new_android');
		$billing_type 	= json_encode($this->post('billing_type'));
		$participant_eligibility 	= $this->post('participant_eligibility');
		$a 	= $this->post('expired_start');
		$b = $this->post('expired_finish');
		$expired_start = date("Y-m-d", strtotime($a));
		$expired_finish = date("Y-m-d", strtotime($b));

		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$qq = $this->db->query("INSERT INTO tbl_price_program (program_id, channel_id, name, description, type, jenjang_id, bidang, fasilitas, price, poster, poster_mobile, status, expired_start, expired_finish, billing_type, participants_eligibility) VALUES ('$program_id','$channel_id','$name','$description','$type','$jenjang_id','$bidang','$fasilitas','$price','','','0','$expired_start','$expired_finish','$billing_type','$participant_eligibility')");

		if ($qq) {

			$parti 			= $this->db->query("SELECT participants FROM master_channel_program WHERE program_id = '$program_id'")->row_array()['participants'];
			$participants 	= json_decode($parti,true);			
			foreach ($participants as $key => $value) {
				$id_user 			= $value['id_user'];
				$participants[$key] = array("id_user" => $id_user, "valid_until" => $expired_finish);	
			}
			$participants = json_encode($participants);
			$this->db->query("UPDATE master_channel_program SET participants = '$participants' WHERE program_id = '$program_id'");

			$list_id 	= $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
			$basename   = $this->Rumus->RandomString();
    		$img_name   = $basename.'.jpg';
    		file_put_contents("aset/_temp_images/".$img_name,file_get_contents($image_new));    		
            $updatephoto = $this->db->query("UPDATE tbl_price_program SET poster='https://cdn.classmiles.com/usercontent/".base64_encode('banner/'.$img_name)."' WHERE list_id='$list_id'");
            $this->Rumus->sendToCDN('banner', $img_name, null, 'aset/_temp_images/'.$img_name );			
            
            $basename_andro   = $this->Rumus->RandomString();
    		$img_name_andro   = $basename_andro.'.jpg';
    		file_put_contents("aset/_temp_images/".$img_name_andro,file_get_contents($image_new_android));
    		$updatephoto = $this->db->query("UPDATE tbl_price_program SET poster_mobile='https://cdn.classmiles.com/usercontent/".base64_encode('banner/'.$img_name_andro)."' WHERE list_id='$list_id'");
    		$this->Rumus->sendToCDN('banner', $img_name_andro, null, 'aset/_temp_images/'.$img_name_andro );

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'Success add package';	
			// $rets['link'] = CDN_URL.USER_IMAGE_CDN_URL.base64_encode('banner/'.$img_name);
			// $rets['link_android'] = CDN_URL.USER_IMAGE_CDN_URL.base64_encode('banner/'.$img_name_andro);			
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -300;
			$rets['message'] = 'Empty';			
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function update_PricePackage_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));		
		$list_id 		= $this->post('list_id');
		$name 			= $this->post('name');
		$description 	= $this->post('description');
		$price 			= $this->post('price');
		$program_id 	= $this->post('program_id');
		$type 			= $this->post('type');
		$jenjang_id 	= $this->post('jenjang_id');
		$bidang 		= $this->post('bidang');
		$fasilitas 		= $this->post('fasilitas');
		$imageData 		= $this->post('imageData');
		$imageDataAndro = $this->post('imageDataAndro');
		$expired_start 	= $this->post('expired_start');
		$expired_finish = $this->post('expired_finish');

		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$qq = $this->db->query("SELECT * FROM tbl_price_program WHERE list_id = '$list_id'")->row_array();
		
		if (!empty($qq)) {		

			$this->db->query("UPDATE tbl_price_program SET name='$name', description='$description', price='$price', program_id = '$program_id' WHERE list_id = '$list_id'");		
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'Success update package';
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -300;
			$rets['message'] = 'Empty';
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function delete_PricePackage_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));		
		$list_id 		= $this->post('link_id');					
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$qq = $this->db->simple_query("DELETE FROM tbl_price_program WHERE list_id = '$list_id'");

		if ($qq) {				
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'Success delete package';
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -300;
			$rets['message'] = 'Empty';
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function addSessionFromModifyClass_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	

		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$channel_id 	= $this->post('channel_id');
		$date 			= $this->post('date');
		$time 			= $this->post('time');
		$duration 		= $this->post('duration');
		$program_id 	= $this->post('program_id');
		$user_utc 		= $this->post('user_utc');
		$schedule 		= json_encode($this->post('schedule'));
		
		$st_time 		= $date.' '.$time.':00';
		$fn_time 		= DateTime::createFromFormat('Y-m-d H:i:s', $st_time);
		$fn_time->modify("+$duration second");
		$fn_time 		= $fn_time->format('Y-m-d H:i:s');
	
		$getSchedule 	= $this->db->query("SELECT schedule FROM master_channel_program WHERE channel_id = '{$channel_id}' AND program_id = '{$program_id}' ")->row_array()['schedule'];		
		$getSesi 		= $this->db->query("SELECT sesi FROM master_channel_program WHERE channel_id = '{$channel_id}' AND program_id = '{$program_id}' ")->row_array()['sesi'];
		$sesi 			= $getSesi+1;		
        $data 			= "";
        $save 			= 1;        
        if ($getSchedule === null) {
        				
        	$listParti 		= json_decode($getSchedule,true);
	        foreach ($listParti as $keyy => $v) {
	        	$seconds            = $v['eventDurasi'];
	            $hours              = floor($seconds / 3600);
	            $mins               = floor($seconds / 60 % 60);
	            $secs               = floor($seconds % 60);
	            $durationrequested  = sprintf('%02d:%02d', $hours, $mins);
	            $schedule_tanggal 	= $v['eventDate'];
	            $schedule_waktu 	= $v['eventTime'];

	            $starttime_dt 		= $schedule_tanggal.' '.$schedule_waktu.':00';
	            $endtime_dt 		= DateTime::createFromFormat('Y-m-d H:i:s', $starttime_dt);
				$endtime_dt->modify("+$seconds second");
				$endtime_dt 		= $endtime_dt->format('Y-m-d H:i:s');
				// ( ( '$date_requested' <= start_time AND '$fn_time' > start_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) OR ( '$date_requested' >= start_time AND '$date_requested' < finish_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) )
				if ( ( $st_time <= $starttime_dt && $fn_time > $starttime_dt && ( $fn_time <= $endtime_dt || $fn_time > $endtime_dt ) ) || ( $st_time >= $starttime_dt && $st_time < $endtime_dt && ( $fn_time <= $endtime_dt || $fn_time > $endtime_dt) ) ) {
					$fk = 'ada kelas';
					$save = 0;
					break;
				}
				
				// $allProgram['listParti'][$keyy]['starttime_dt'] = $starttime_dt;
	   //          $allProgram['listParti'][$keyy]['endtime_dt'] 	= $endtime_dt;
	            
	        }
	    }

        if ($save == 1) {
        	$updtSchedule = $this->db->query("UPDATE master_channel_program SET schedule = '$schedule', sesi = '$sesi' WHERE channel_id = '$channel_id' AND program_id = '$program_id' ");

        	if ($updtSchedule) {
        		$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = 'Success';
				$this->response($rets);
        	}
        	else
        	{
        		$rets['status'] = false;
				$rets['code'] 	= -100;
				$rets['message'] = "Failed Update";
				$this->response($rets);
        	}
        }
        else
        {
        	$rets['status'] = false;
			$rets['code'] 	= -105;
			$rets['message'] = "Sesi Bentrok";
			$this->response($rets);
        }

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function deleteSessionFromModifyClass_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	

		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$channel_id 	= $this->post('channel_id');
		$program_id 	= $this->post('program_id');		
		$schedule 		= json_encode($this->post('schedule'));
		$sesi 			= $this->db->query("SELECT sesi FROM master_channel_program WHERE channel_id = '$channel_id' AND program_id = '$program_id' ")->row_array()['sesi'];
		
		$sesi_now 		= $sesi-1;
		$delSchedule = $this->db->query("UPDATE master_channel_program SET schedule = '$schedule', sesi = '$sesi_now' WHERE channel_id = '$channel_id' AND program_id = '$program_id' ");

        if ($delSchedule) {
        
    		$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'successfully Delete';			
			$this->response($rets);

        }
        else
        {
        	$rets['status'] = false;
			$rets['code'] 	= -105;
			$rets['message'] = "Failed Delete";
			$this->response($rets);
        }

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function getDetailDataTutorFromCh_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));	

		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

		$tutor_id 		= $this->post('tutor_id');
		$subject_id 	= $this->post('subject_id');

		$Tutor 		= $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
		$Get 		= $this->db->query("SELECT * FROM master_subject WHERE subject_id='$subject_id'")->row_array();
		$jenjangnamelevel = array();
		$jenjangid = json_decode($Get['jenjang_id'], TRUE);
		if ($jenjangid != NULL) {
			if(is_array($jenjangid)){
				foreach ($jenjangid as $keyy => $valuee) {									
					$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$valuee'")->row_array();	
					$jenjangnamelevel[] = $selectnew['jenjang_name'].' '.$selectnew['jenjang_level']; 					
				}	
			}else{
				$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();	
				$jenjangnamelevel[] = $selectnew['jenjang_name'].' '.$selectnew['jenjang_level']; 				
			}										
		}
		
        if ($Get) {        	
    		$rets['status'] = true;
			$rets['code'] = 200;
			$rets['Tutor'] = $Tutor;
			$rets['Jenjang_namelevel'] = implode(", ",$jenjangnamelevel);
			$rets['Subject_name'] = $Get['subject_name'];
			$this->response($rets);

        }
        else
        {
        	$rets['status'] = false;
			$rets['code'] 	= -105;
			$rets['message'] = "Failed Delete";
			$this->response($rets);
        }

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}	

	public function cekClass_post()
	{
		$date_requested = $this->post('date_requested');
		$fn_time 		= $this->post('fn_time');
		$tutor_id 		= $this->post('tutor_id');
		
		$qq = $this->db->query("SELECT * FROM tbl_class WHERE ( ( '$date_requested' <= start_time AND '$fn_time' > start_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) OR ( '$date_requested' >= start_time AND '$date_requested' < finish_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) ) AND tutor_id=$tutor_id ")->row_array();

		if (empty($qq)) {				
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'Class empty';			
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -300;
			$rets['message'] = 'Class already';			
			$this->response($rets);
		}
	}

	public function getDetailsRequest_post()
	{
		$tutor_id 		= $this->post('tutor_id');
		$subject_id 	= $this->post('subject_id');		

		$getTutor 		= $this->db->query("SELECT * FROM tbl_user WHERE id_user = '$tutor_id'")->row_array();
		$getSubject 	= $this->db->query("SELECT * FROM master_subject WHERE subject_id='$subject_id'")->row_array();

		$jenjangnamelevel = array();
		$jenjangid = json_decode($getSubject['jenjang_id'], TRUE);
		if ($jenjangid != NULL) {
			if(is_array($jenjangid)){
				foreach ($jenjangid as $keyy => $valuee) {									
					$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$valuee'")->row_array();	
					$jenjangnamelevel[] = $selectnew['jenjang_name'].' '.$selectnew['jenjang_level']; 					
				}	
			}else{
				$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();	
				$jenjangnamelevel[] = $selectnew['jenjang_name'].' '.$selectnew['jenjang_level']; 				
			}										
		}

		if (!empty($getSubject)) {				
			$rets['status'] 			= true;
			$rets['code'] 				= 200;
			$rets['Jenjang_namelevel'] 	= implode(", ",$jenjangnamelevel);
			$rets['Subject_name'] 		= $getSubject['subject_name'];
			$rets['Tutor_name'] 		= $getTutor['user_name'];
			$rets['Tutor_image'] 		= $getTutor['user_image'];
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -300;
			$rets['message'] = 'Empty';			
			$this->response($rets);
		}
	}

	public function getDetailsApprovePrivate_post()
	{
		$subject_id 	= $this->post('subject_id');		
		$id_user 		= $this->post('id_requester');

		$getUser 		= $this->db->query("SELECT * FROM tbl_user WHERE id_user = '$id_user'")->row_array();
		$getSubject 	= $this->db->query("SELECT * FROM master_subject WHERE subject_id = '$subject_id'")->row_array();

		$jenjangnamelevel = array();
		$jenjangid = json_decode($getSubject['jenjang_id'], TRUE);
		if ($jenjangid != NULL) {
			if(is_array($jenjangid)){
				foreach ($jenjangid as $keyy => $valuee) {									
					$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$valuee'")->row_array();	
					$jenjangnamelevel[] = $selectnew['jenjang_name'].' '.$selectnew['jenjang_level']; 					
				}	
			}else{
				$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();	
				$jenjangnamelevel[] = $selectnew['jenjang_name'].' '.$selectnew['jenjang_level']; 				
			}										
		}

		if (!empty($getSubject)) {				
			$rets['status'] 			= true;
			$rets['code'] 				= 200;
			$rets['Jenjang_namelevel'] 	= implode(", ",$jenjangnamelevel);
			$rets['Subject_name'] 		= $getSubject['subject_name'];
			$rets['Tutor_name'] 		= $getUser['user_name'];
			$rets['Tutor_image'] 		= $getUser['user_image'];
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -300;
			$rets['message'] = 'Empty';			
			$this->response($rets);
		}
	}

	public function createClassPrivateFromChannel_post()
	{
		$id_user 		= $this->post('id_user');		
		$tutor_id 		= $this->post('tutor_id');
		$subject_id 	= $this->post('subject_id');
		$subject_name 	= $this->post('name');
		$topic 			= $this->post('topic');
		$start_time 	= $this->post('start_time');
		$finish_time 	= $this->post('finish_time');
		$participant 	= $this->post('participant');
		$channel_id 	= $this->post('channel_id');

		$createClass 	= $this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,participant,class_type,template_type,channel_id,channel_status) VALUES('{$subject_id}','{$subject_name}','{$topic}','{$tutor_id}','{$start_time}','{$finish_time}','{$participant}','private','all_featured','$channel_id','1')");			

		if ($createClass) {
			$class_id           = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

			$rets['status'] 			= true;
			$rets['code'] 				= 200;
			$rets['class_id'] 			= $class_id;			
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -300;
			$rets['message'] = 'Empty';			
			$this->response($rets);
		}
	}

	public function createClassGroupFromChannel_post()
	{
		$id_user 		= $this->post('id_user');		
		$tutor_id 		= $this->post('tutor_id');
		$subject_id 	= $this->post('subject_id');
		$subject_name 	= $this->post('name');
		$topic 			= $this->post('topic');
		$start_time 	= $this->post('start_time');
		$finish_time 	= $this->post('finish_time');
		$participant 	= $this->post('participant');
		$channel_id 	= $this->post('channel_id');

		$createClass 	= $this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,participant,class_type,template_type,channel_id,channel_status) VALUES('{$subject_id}','{$subject_name}','{$topic}','{$tutor_id}','{$start_time}','{$finish_time}','{$participant}','group','all_featured','$channel_id','1')");			

		if ($createClass) {
			$class_id           = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

			$rets['status'] 			= true;
			$rets['code'] 				= 200;
			$rets['class_id'] 			= $class_id;			
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -300;
			$rets['message'] = 'Empty';			
			$this->response($rets);
		}
	}

	public function tps_me_post($session_id='')
	{
		// $v_accesstoken 	= $this->verify_access_token($this->post('access_token'));	

		// if ($v_accesstoken == "") {
		// 	$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
		// 	goto ending;
		// }

		$id_user 	= $this->post('id_user');
		$linkback 	= $this->post('linkback');		
		$checkstudentonline = $this->db->query("SELECT * from temp_user_online where id_user = '".$id_user."' ")->row_array();
	    if (!empty($checkstudentonline)) {	        
			$rets['status'] = false;
			$rets['code'] 	= -405;
			$rets['message'] = "Sorry, your now in class";
			$this->response($rets);
	    } 
	    else {
			$DB2 = $this->load->database('db2', TRUE); // SAY SEA SERVER CLASSPLAY DB
			$DB_SG = $this->load->database('db_sg', TRUE); // SAY SEA SERVER CLASSPLAY DB

			$cm_continent = "id";
			$iduser = $this->post('id_user');
			// $andro 	= $this->input->get('p');
			$andro = $_GET['p'];
			if ($iduser == null) {				
				$iduser = $this->post('id_user');
			}			

			$db_res = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
			$room_active_janus = $this->db->query("SELECT * FROM temp_active_room WHERE class_id='$session_id'")->row_array();
			if(!empty($room_active_janus)){
				$cm_continent = $room_active_janus['cm_continent'];
			}else{
				// TAKE ALL PARTICIPANT IN THAT CLASS
				$visible = json_decode($db_res['participant'],true);
				$incoming_kuota = 0;
				$local_region = 0;
				$interlocal_region = 0;
				$chosen_janus = "";

				$country_code = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='".$db_res['tutor_id']."'")->row_array()['user_countrycode'];
				if($country_code == "+62" || $country_code == "62"){
					$local_region++;
				}else{
					$interlocal_region++;
				}
				$incoming_kuota++;

				foreach ($visible['participant'] as $k => $value) {
					$id_user = $value['id_user'];
					$data_ccode_utype = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='$id_user'")->row_array();
					$country_code = $data_ccode_utype['user_countrycode'];
					$usertype_id = $data_ccode_utype['usertype_id'];
					if($usertype_id != "student kid"){
						if($country_code == "+62" || $country_code == "62"){
							$local_region++;
						}else{
							$interlocal_region++;
						}
					}
					$incoming_kuota++;
				}

				$ttl_active_kuota = $this->db->query("SELECT janus, SUM(kuota) as kuota FROM temp_active_room GROUP BY janus")->result_array();
				if($local_region > $interlocal_region){
					$cm_continent = "id";
					// PRESENTASE TERBESAR INDONESIA
					// $list_janus = array("c3.classmiles.com" => 0,"c1.classmiles.com" => 0,"c2.classmiles.com" => 0);
					// $list_janus = array("c4.classmiles.com" => 0, "c3.classmiles.com" => 0);
					$list_janus = array("c1.classmiles.com" => 0);
					foreach ($ttl_active_kuota as $key => $value) {
						if(in_array($value['janus'], $list_janus)){
							$list_janus[$value['janus']] = $value['kuota'];
						}
					}
					foreach ($list_janus as $key => $value) {
						if($value < 200 && ($value+$incoming_kuota) <= 200){
							$chosen_janus = $key;
							break;
						}
					}
					if($chosen_janus == "")
						$chosen_janus = "c1.classmiles.com"; //JANUS SAMPAH
				}else{
					$cm_continent = "sg";
					// $list_janus = array("c102.classmiles.com" => 0);
					$list_janus = array("c1.classmiles.com" => 0);
					foreach ($ttl_active_kuota as $key => $value) {
						if(in_array($value['janus'], $list_janus)){
							$list_janus[$value['janus']] = $value['kuota'];
						}
					}
					foreach ($list_janus as $key => $value) {
						if($value < 200 && ($value+$incoming_kuota) <= 200){
							$chosen_janus = $key;
							break;
						}
					}
					if($chosen_janus == "")
						// $chosen_janus = "c102.classmiles.com"; //JANUS SAMPAH
						$chosen_janus = "c1.classmiles.com"; //JANUS SAMPAH
				}
				$this->db->simple_query("INSERT INTO temp_active_room VALUES(NULL,'$cm_continent','$chosen_janus','$session_id',$incoming_kuota,now());");
				$this->janus_server = $chosen_janus;
			}
			if($cm_continent == "id"){
				$student = $DB2->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
				if(!empty($student)){
					$hash_id = $student['session_id'];
				}else{
					$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());					
					$DB2->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
				}
				$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

				$qr = "";
				$qr_data = "";
				$setter = "";
				foreach ($data_diri as $key => $value) {
					if($key == "username" && $value == NULL){

					}else{
						$qr.= $key.", ";
						$qr_data.= "'$value', ";
						$setter.= "$key='$value', ";
					}
				}
				$qr = rtrim($qr, ", ");
				$qr_data = rtrim($qr_data, ", ");
				$setter = rtrim($setter, ", ");
				$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
				if(!$insert_tbl_user) {
					$DB2->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
				}

				$classplay_exist = $DB2->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
				if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
					$qc = "";
					$qc_data = "";
					$setter = "";
					foreach ($db_res as $key => $value) {
						$qc.= $key.", ";
						$qc_data.= "'".$this->db->escape_str($value)."', ";
						$setter.= "$key='$value', ";
					}
					$qc = rtrim($qc, ", ");
					$qc_data = rtrim($qc_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
					if(!$insert_tbl_user){
						$DB2->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
					}
				}
				
			}else if($cm_continent == 'sg') {
				$student = $DB_SG->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
				if(!empty($student)){
					$hash_id = $student['session_id'];
				}else{
					$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
					echo $hash_id;
					$DB_SG->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
				}
				$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

				$qr = "";
				$qr_data = "";
				$setter = "";
				foreach ($data_diri as $key => $value) {
					if($key == "username" && $value == NULL){

					}else{
						$qr.= $key.", ";
						$qr_data.= "'$value', ";
						$setter.= "$key='$value', ";
					}
				}
				$qr = rtrim($qr, ", ");
				$qr_data = rtrim($qr_data, ", ");
				$setter = rtrim($setter, ", ");
				$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
				if(!$insert_tbl_user) {
					$DB_SG->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
				}

				$classplay_exist = $DB_SG->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
				if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0'){
					$qc = "";
					$qc_data = "";
					$setter = "";
					foreach ($db_res as $key => $value) {
						$qc.= $key.", ";
						$qc_data.= "'$value', ";
						$setter.= "$key='$value', ";
					}
					$qc = rtrim($qc, ", ");
					$qc_data = rtrim($qc_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
					if(!$insert_tbl_user){
						$DB_SG->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
					}
				}
			}

			$load = $this->lang->line('logsuccess');					
			$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$session_id'")->row_array();
			$action = "$load ".$dpt_subject_name['name'];
			$log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$iduser')");
			// $this->session->set_userdata('p',$andro);
			// header('Location: https://classmiles.com/classroom/broadcast_wd?access_session='.$hash_id);
			// echo "<script>alert('halo ini di master');</script>";
			// if ($templateweb == "whiteboard_digital") {
			// 	// header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_wd?access_session='.$hash_id.'&p='.$andro);
			// 	$rets['status'] 			= true;
			// 	$rets['code'] 				= 200;
			// 	$rets['link'] 				= 'https://'.$cm_continent.'.classmiles.com/classroom/broadcast_wd?access_session='.$hash_id.'&p='.$andro;			
			// 	$this->response($rets);
			// }
			// else if ($templateweb == "whiteboard_videoboard") {
			// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_wv?access_session='.$hash_id.'&p='.$andro);
			// }
			// else if ($templateweb == "whiteboard_no")
			// {
			// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast?access_session='.$hash_id.'&p='.$andro);
			// }
			// else if ($templateweb == "all_featured")
			// {				
			// header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_all?access_session='.$hash_id.'&p='.$andro);
			$rets['status'] 			= true;
			$rets['code'] 				= 200;
			$rets['link'] 				= 'https://'.$cm_continent.'.classmiles.com/classroom/broadcast_all?access_session='.$hash_id.'&p='.$andro.'&back='.$linkback;			
			$this->response($rets);
			// }

			exit();
		}

		// $rets['status'] = false;
		// $rets['message'] = "Field incomplete";
		// ending:
		// $this->response($rets);
	}

	public function savedProfileStudent_post()
	{
		$v_accesstoken 	= $this->verify_access_token($this->get('access_token'));			
		if ($v_accesstoken == "") {
			$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
			goto ending;
		}

        $id_user 		= $this->post('id_user');
        $user_name 		= $this->post('first_name').' '.$this->post('last_name');
        $first_name 	= $this->post('first_name');
        $last_name 		= $this->post('last_name');
        $user_gender 	= $this->post('user_gender');
        $user_nationality= $this->post('user_nationality');
        $user_birthdate = $this->post('user_birthdate');
        $user_age 		= $this->post('user_age');
        $user_countrycode= $this->post('user_countrycode');
        $user_address 	= $this->post('user_address');
        $image_new 		= $this->post('image_new');

		$cekUser = $this->db->query("SELECT * FROM tbl_user WHERE id_user = '$id_user'")->row_array();

		if (!empty($cekUser)) {

			$basename           = $id_user.$this->Rumus->RandomString();
    		$img_name      		= $basename.'.jpg';
    		
    		file_put_contents("aset/_temp_images/".$img_name,file_get_contents($image_new));
            $updatephoto = $this->db->query("UPDATE tbl_user SET user_image='".base64_encode('user/'.$img_name)."' WHERE id_user='$id_user'");
           	
            $this->Rumus->sendToCDN('user', $img_name, null, 'aset/_temp_images/'.$img_name );

			$updt = $this->db->simple_query("UPDATE tbl_user SET user_name = '$user_name', first_name = '$first_name', last_name = '$last_name', user_gender = '$user_gender', user_nationality = '$user_nationality', user_birthdate = '$user_birthdate', user_age = '$user_age', user_countrycode = '$user_countrycode', user_address = '$user_address' WHERE id_user='$id_user'");

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'Success update profile';
			$rets['link'] = CDN_URL.USER_IMAGE_CDN_URL.base64_encode('user/'.$img_name);
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = 'Failed update profile';
			$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function face_detection_post()
	{
		$image = $this->post('image');		

		if(!empty($image))
		{		
			 $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://classmiles.com/engine/face_detection');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, "image=$image");

	        // Execute post
	        $result = curl_exec($ch);
	        $face = json_decode($result);
	        curl_close($ch);

        	$res['status'] = true;
			$res['face'] = $face->{'is_there_face'};
			$res['code'] = 200;
			$this->response($res);
        }
        else
        {
			$res['status'] = false;
			$res['message'] = 'Failed';
			$res['code'] = -200;
			$this->response($res);
		}
	}

	public function update_photo_post($value='')
	{
		$datas = $this->input->post();
		$image_new = $this->input->post('image_new');

		$id		= $this->session->userdata('id_user');
		$sessionuserimage 	= $this->session->userdata('user_image');
		
		// $id 				= $datas['id_user'];
        $basename           = $id.$this->Rumus->RandomString();
    	$img_name      		= $basename.'.jpg';
    	$bukti 				= isset($datas["image"]) ? $datas["image"] : "";
    	// $this->db->simple_query("INSERT INTO test(a) VALUES('{$bukti}')");

		// $gambar_base64 = $DATA_DARI_POST['bukti_tr'];
    	$processadd = $this->db->query("SELECT *FROM tbl_user where id_user = '{$id}'")->row_array();
    	if ($processadd) {
            // $image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'               
                         
            //Write to disk
            file_put_contents("aset/_temp_images/".$img_name,file_get_contents($image_new));
            $updatephoto = $this->db->query("UPDATE tbl_user SET user_image='".base64_encode('user/'.$img_name)."' WHERE id_user='$id'");
           
            $this->Rumus->sendToCDN('user', $img_name, null, 'aset/_temp_images/'.$img_name );
            $this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block'); 
			$this->session->set_flashdata('mes_message','Berhasil mengupload file');
			$this->session->set_userdata('code','4044');
            $cek_id_user_kids = $this->session->userdata('id_user_kids');
			if ($parent_id == null) {
				$this->session->set_userdata('user_image', CDN_URL.USER_IMAGE_CDN_URL.base64_encode('user/'.$img_name));
			}
			else{
				$this->session->set_userdata('user_image_kids', CDN_URL.USER_IMAGE_CDN_URL.base64_encode('user/'.$img_name));	
			}
            if ($this->session->userdata('usertype_id') == "student") {							
				redirect('About');
			}
			else
			{
				redirect('tutor/about');
			}
            return null;    		
		}
		else
		{
			$res['status'] = false;
			$res['messag'] = 'Gagal bro';			
			echo json_encode($res);
			return null;
			$this->response($res);
		}	
	}

	// START SYSTEM //
	// ===================================================================================== //
	private function create_access_token($data='')
	{
		$token= array(
			"exp" => time() + ( 60 * 60 ),
			"iss" => "air classmiles jwt v1",
			"iat" => time(),
			"data" => $data
		);
		$jwt = JWT::encode($token, SECRET_KEY);
		return $jwt;
	}
	private function verify_access_token($access_token='')
	{
		try{
			$dc = JWT::decode($access_token, SECRET_KEY, array('HS256'));
		}catch(Exception $e){
			return false;
		}

		if(isset($dc->status) && $dc->status == false){
			return -1;
		}else{
			return json_decode(json_encode($dc->data),true);
		}
	}
	public function listmentor_post()
	{		
		$get_datamentor = $this->db->query("SELECT * FROM tbl_user")->row_array();
		// echo $get_datamentor['nama_mentor'];
		// return false;
		// $list_country = $this->db->query("SELECT * FROM master_country")->result_array();
		if (empty($get_datamentor)) {
			$rets = array();
			$rets['status'] = false;
			$rets['code'] = -400;
			$rets['message'] = "failed";
			$rets['data_mentor'] = null;		
			// $this->response($rets);
		}
		else
		{
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Sukses";
			$rets['data_mentor'] = $get_datamentor;		
			$this->response($rets);
			// echo $rets['message'];
			// echo $rets['message'];

		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		// $this->response($rets);
	}

	
}