<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classroom extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        // $this->load_lang($this->session->userdata('lang'));        
        $this->load->database();
		$this->load->helper(array('url'));       
		// $this->load->model('Master_model'); 
	 	$this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
		// Check for any mobile device.
		if ($detect->isMobile()){
			define('MOBILE', 'mobile/');
		}else{
			define('MOBILE', '');
		}
	}	

	public function index()
	{
		$type = $this->session->userdata("type");
		$data['type'] = $type;
		$this->load->view(MOBILE.'classroom_config/header',$data);
		if ($type == "broadcast") {
			$this->load->view(MOBILE.'classroom_view/broadcast/broadcast');
		}
		else if ($type == "broadcast_wv")
		{
			$this->load->view(MOBILE.'classroom_view/broadcast/broadcast_wv');
		}
		else if ($type == "broadcast_wd")
		{
			$this->load->view(MOBILE.'classroom_view/broadcast/broadcast_wd');
		}
    	
	}

	// public function index()
	// {
	// 	$dataa['type'] = "broadcast";
	// 	$this->load->view(MOBILE.'classroom_config/header',$dataa);
 //    	$this->load->view(MOBILE.'classroom_view/broadcast/broadcast');
	// }

	// public function wb()
	// {
	// 	$this->load->view(MOBILE.'classroom_config/header');
 //    	$this->load->view(MOBILE.'classroom_view/test');
	// }


	//------------------------- GET SESSION ALL ---------------------------//
	
	////////// BROADCAST //////////
	public function broadcast()
	{
		$data['inisession'] = $_GET['access_session'];
		$data['type'] = "broadcast";
		$this->load->view(MOBILE.'classroom_config/headerr');
		$this->load->view(MOBILE.'classroom_config/session_broadcast',$data);
	}

	public function broadcast_wv()
	{
		$data['inisession'] = $_GET['access_session'];
		$data['type'] = "broadcast_wv";
		$this->load->view(MOBILE.'classroom_config/headerr');
		$this->load->view(MOBILE.'classroom_config/session_broadcast',$data);
	}

	public function broadcast_wd()
	{
		$data['inisession'] = $_GET['access_session'];
		$data['type'] = "broadcast_wd";
		$this->load->view(MOBILE.'classroom_config/headerr');
		$this->load->view(MOBILE.'classroom_config/session_broadcast',$data);
	}
	////////// END BROADCAST //////////

	////////// PRIVATE //////////
	public function privateclass()
	{	
		// echo $_GET['access_session'];
		$data['class'] = $_GET['access_session'];
		$data['type'] = "private";
		$this->load->view(MOBILE.'classroom_config/headerr');
		$this->load->view(MOBILE.'classroom_config/session_private',$data);		
	}

	public function privateclas_wv()
	{	
		// echo $_GET['access_session'];
		$data['class'] = $_GET['access_session'];
		$data['type'] = "private_wv";
		$this->load->view(MOBILE.'classroom_config/headerr');
		$this->load->view(MOBILE.'classroom_config/session_private',$data);		
	}

	public function privateclas_wd()
	{	
		// echo $_GET['access_session'];
		$data['class'] = $_GET['access_session'];
		$data['type'] = "private_wd";
		$this->load->view(MOBILE.'classroom_config/headerr');
		$this->load->view(MOBILE.'classroom_config/session_private',$data);		
	}
	////////// END PRIVATE //////////

	////////// GROUP //////////
	public function groupclass()
	{	
		// echo $_GET['access_session']; 
		$data['class'] = $_GET['access_session'];
		$data['type'] = "group";
		$this->load->view(MOBILE.'classroom_config/headerr');
		$this->load->view(MOBILE.'classroom_config/session_group',$data);		
	}

	public function groupclass_wv()
	{	
		// echo $_GET['access_session']; 
		$data['class'] = $_GET['access_session'];
		$data['type'] = "group_wv";
		$this->load->view(MOBILE.'classroom_config/headerr');
		$this->load->view(MOBILE.'classroom_config/session_group',$data);		
	}

	public function groupclass_wd()
	{	
		// echo $_GET['access_session']; 
		$data['class'] = $_GET['access_session'];
		$data['type'] = "group_wd";
		$this->load->view(MOBILE.'classroom_config/headerr');
		$this->load->view(MOBILE.'classroom_config/session_group',$data);		
	}
	////////// END GROUP //////////

	//------------------------END GET SESSION---------------------------//

	//--------------------------- GO CLASS ---------------------------//

	public function roomtester()
	{
		$data['access_session'] = $_GET['access_session'];
		$dataa['type'] = "roomtester";
		$this->load->view(MOBILE.'classroom_config/header', $dataa);
		$this->load->view(MOBILE.'classroom_config/session',$data);
	}
	
	public function room_tester()
	{
		$type = $this->session->userdata("roomtester");
		$data['type'] = $type;
		$this->load->view(MOBILE.'classroom_config/header',$data);
		$this->load->view(MOBILE.'classroom_view/private/roomtester');    	
	}

	public function private_room()
	{
		$typep = $this->session->userdata("type_p");
		$data['type'] = $typep;
		$this->load->view(MOBILE.'classroom_config/header',$data);
		if ($typep == "private") {
    		$this->load->view(MOBILE.'classroom_view/private/private');
    	}
    	else if ($typep == "private_wv") {
    		$this->load->view(MOBILE.'classroom_view/private/private_wv');
    	}
    	else if ($typep == "private_wd") {
    		$this->load->view(MOBILE.'classroom_view/private/private_wd');
    	}
	}

	public function group_room()
	{
		$typeg = $this->session->userdata("type_g");
		$data['type'] = $typeg;
		$this->load->view(MOBILE.'classroom_config/header',$data);
		if ($typeg == "group") {
			$this->load->view(MOBILE.'classroom_view/group/group');
		}
		else if($typeg == "group_wv")
		{
			$this->load->view(MOBILE.'classroom_view/group/group_wv');
		}
		else if($typeg == "group_wd")
		{
			$this->load->view(MOBILE.'classroom_view/group/group_wd');
		}
    	
	}

	public function callsupport()
	{	
		// echo $_GET['access_session'];
		$data['class'] = $_GET['access_session'];
		$data['type'] = "callsupport";
		$this->load->view(MOBILE.'classroom_config/headerr');
		$this->load->view(MOBILE.'classroom_config/session_support',$data);		
	}

	public function call_support()
	{		
		$data['type'] = 'callsupport';
		$this->load->view(MOBILE.'classroom_config/header',$data);		
    	$this->load->view(MOBILE.'classroom_view/support/support');    	
	}

	//------------------------ END GO CLASS ---------------------------//





	
	// public function session()
	// {			
	// 	$data['session'] = $_GET['access_session'];
	// 	$this->load->view(MOBILE.'classroom_config/header');
	// 	$this->load->view(MOBILE.'classroom_config/session',$data);		
	// }

	public function processing()
	{
		$data['inisession'] = $_GET['access_session'];
		$dataa['type'] = "broadcast";
		$this->load->view(MOBILE.'classroom_config/header', $dataa);
		$this->load->view(MOBILE.'classroom_config/session',$data);
	}	

	

	public function whiteboard_class()
	{
		$data['type'] = "whiteboard";
		$this->load->view(MOBILE.'classroom_config/header', $data);
    	$this->load->view(MOBILE.'classroom_view/whiteboard_only');
	}	

	public function getdata()
	{
		$data = $this->input->post('data');
		$this->load->view(MOBILE.'classroom_config/header');
		$this->load->view(MOBILE.'classroom_view/classroom', $data);
	}

	public function chat()
	{
		$this->load->view(MOBILE."classroom_view/textroomtest");
	}


	public function janus()
	{
		
		$this->load->view(MOBILE."classroom_view/videoroom");
	}

	public function process()
	{	
		// echo $_GET['access_session'];
		$data['class'] = $_GET['access_session'];
		$this->load->view(MOBILE.'classroom_config/header');
		$this->load->view(MOBILE.'classroom_config/class',$data);		
	}

	public function room()
	{
		$this->load->view(MOBILE.'classroom_config/header_t');
    	$this->load->view(MOBILE.'classroom_view/testt');
	}

	

	///////// ANDROID /////////////
	public function room_class()
	{
		$data['type'] = "private_android";
		$this->load->view(MOBILE.'classroom_config/header',$data);
    	$this->load->view(MOBILE.'classroom_view/private/private_android');
	}

	public function broadcastwv_android()
	{
		$data['type'] = "broadcastwv_android";
		$this->load->view(MOBILE.'classroom_config/header',$data);
    	$this->load->view(MOBILE.'classroom_view/broadcast/broadcastwv_android');
	}

	public function broadcastwd_android()
	{
		$data['type'] = "broadcastwd_android";
		$this->load->view(MOBILE.'classroom_config/header',$data);
    	$this->load->view(MOBILE.'classroom_view/broadcast/broadcastwd_android');
	}

	public function broadcast_android()
	{
		$data['type'] = "broadcast_android";
		$this->load->view(MOBILE.'classroom_config/header',$data);
    	$this->load->view(MOBILE.'classroom_view/broadcast/broadcast_android');
	}

	public function privatewd_android()
	{
		$data['type'] = "privatewd_android";
		$this->load->view(MOBILE.'classroom_config/header',$data);
    	$this->load->view(MOBILE.'classroom_view/private/privatewd_android');
	}

	public function privatewv_android()
	{
		$data['type'] = "privatewv_android";
		$this->load->view(MOBILE.'classroom_config/header',$data);
    	$this->load->view(MOBILE.'classroom_view/private/privatewv_android');
	}

	public function private_android()
	{
		$data['type'] = "private_android";
		$this->load->view(MOBILE.'classroom_config/header',$data);
    	$this->load->view(MOBILE.'classroom_view/private/private_android');
	}

	public function group_android()
	{
		$data['type'] = "group_android";
		$this->load->view(MOBILE.'classroom_config/header',$data);
    	$this->load->view(MOBILE.'classroom_view/group/group_android');
	}

	public function groupwd_android()
	{
		$data['type'] = "groupwd_android";
		$this->load->view(MOBILE.'classroom_config/header',$data);
    	$this->load->view(MOBILE.'classroom_view/group/groupwd_android');
	}

	public function groupwv_android()
	{
		$data['type'] = "groupwv_android";
		$this->load->view(MOBILE.'classroom_config/header',$data);
    	$this->load->view(MOBILE.'classroom_view/group/groupwv_android');
	}
}