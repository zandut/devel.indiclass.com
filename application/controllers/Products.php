<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller
{
    function  __construct(){
        parent::__construct();
        $this->load->library('paypal_lib');
        $this->load->model('product');
    }
    
    function index(){
        $data = array();
        
        // Get products data from database
        $data['products'] = $this->product->getRows();
        
        // Pass products data to view
        $this->load->view('products/index', $data);
    }
    
    function buy($id){
        // Set variables for paypal form
        $returnURL = base_url().'Paypal/success'; //payment success url
        $cancelURL = base_url().'Paypal/cancel'; //payment cancel url
        $notifyURL = base_url().'Paypal/ipn'; //ipn url
        
        // Get product data
        $product = $this->product->getRows($id);
        
        // Get current user ID from session
        $userID = $this->session->userdata('id_user');
        
        // Add fields to paypal form
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('item_name', $product['name']);
        $this->paypal_lib->add_field('custom', $userID);
        $this->paypal_lib->add_field('item_number',  $product['id']);
        $this->paypal_lib->add_field('amount',  $product['price']);
        
        // Load paypal form
        $this->paypal_lib->paypal_auto_form();
    }

    function buy_package($id){
        // Set variables for paypal form
        $returnURL = base_url().'Paypal/success'; //payment success url
        $cancelURL = base_url().'Paypal/cancel'; //payment cancel url
        $notifyURL = base_url().'Paypal/ipn'; //ipn url
        
        // Get product data
        $product = $this->product->getPackage($id);
        
        // Get current user ID from session
        $userID = $this->session->userdata('id_user');
        $member = $this->session->userdata('member_ship');
        if ($member == 'basic') {
            $pricepackage = $product['price_package_basic'];
        }
        else
        {
            $pricepackage = $product['price_package_premium'];
        }
        // Add fields to paypal form
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('item_name', $product['name_package']);
        $this->paypal_lib->add_field('custom', $userID);
        $this->paypal_lib->add_field('item_number',  $product['id_package']);
        $this->paypal_lib->add_field('amount',  $pricepackage);
        
          
        // Load paypal form
        $this->paypal_lib->paypal_auto_form();
    }

}