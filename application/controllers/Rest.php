<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH."/libraries/REST_Controller.php";
/*require APPPATH."/libraries/jwt/JWT.php";
require APPPATH."/libraries/jwt/BeforeValidException.php";
require APPPATH."/libraries/jwt/ExpiredException.php";
require APPPATH."/libraries/jwt/SignatureInvalidException.php";*/

use \Firebase\JWT\JWT;

class Rest extends REST_Controller {

	public $sesi = array('nama' => null,'username' => null,'priv_level' => null);
	public $janus_server = "c2.classmiles.com";
	public $DB1 = null;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		// $this->load->database('default');
		// $this->DB1 = $this->load->database('db2', TRUE);
	}
	public function index_get($value='')
	{
		$tr = $this->Rumus->RandomString();
		$res = array('error' => array('code' => 400, 'message' => 'Invalid Request', 'transaction' => $tr));
		// echo json_encode($res);
		$this->response($res);
	}
	public function index_post($value='')
	{
		$tr = $this->Rumus->RandomString();
		$res = array('error' => array('code' => 400, 'message' => 'Invalid Request', 'transaction' => $tr));
		// echo json_encode($res);
		$this->response($res);
	}
	public function bad_alias_get($value='')
	{
		$tr = $this->Rumus->RandomString();
		$res = array('error' => array('code' => 406, 'message' => 'Bad Alias', 'transaction' => $tr));
		// echo json_encode($res);
		$this->response($res);
	}
	public function bad_alias_post($value='')
	{
		$tr = $this->Rumus->RandomString();
		$res = array('error' => array('code' => 406, 'message' => 'Bad Alias', 'transaction' => $tr));
		// echo json_encode($res);
		$this->response($res);
	}
	public function main_get()
	{
		$tr = $this->Rumus->RandomString();
		$res = array();
		$res['status'] = true;
		$res['response'] = array('code' => 200, 'message' => 'OAuthServer','info' => 'Classmiles Auth Server', 'transaction' => $tr);
		$this->response($res);
	}
	public function main_post()
	{
		$tr = $this->Rumus->RandomString();
		$res = array();
		$res['status'] = true;
		$res['response'] = array('code' => 200, 'message' => 'OAuthServer','info' => 'Classmiles Auth Server', 'transaction' => $tr);
		$this->response($res);
	}
	public function get_token_post()
	{
		$res = array();
		$res['method'] = "get_token";
		$res['transaction'] = null;
		$res['response'] = array();
		$time = time();

		if($this->post('session') != ''){
			$session_key = $this->post('session');
			$user_utc = $this->post('user_utc');
			$server_utc = $this->Rumus->getGMTOffset();
			$interval   = $user_utc - $server_utc;

			// PREPARE FOR ADD TOKEN TO JANUS
			$send = array();
			$send['janus'] = "add_token";
			$send['token'] = sha1($session_key);

			$send['transaction'] = $this->Rumus->RandomString();
			$send['admin_secret'] = "admin556677";

			$res['transaction'] = $send['transaction'];

			$ari = json_encode($send);
			// CHECK EXISTANCE SESSION IN DB

			$db_res = $this->db->query("SELECT tu.id_user, tu.user_name, tu.first_name, tu.email, tu.usertype_id, tps.class_id, tc.participant, tc.break_state,tc.break_pos, tc.subject_id, tc.name, tc.description, tc.tutor_id, tps.session_id, DATE_ADD(tc.start_time, INTERVAL $interval minute) as start_time, DATE_ADD(tc.finish_time, INTERVAL $interval minute) as finish_time, tu.st_tour, tc.template_type, tc.class_type FROM tbl_user as tu INNER JOIN ( tbl_participant_session as tps INNER JOIN tbl_class as tc ON tps.class_id=tc.class_id) ON tu.id_user=tps.id_user WHERE tps.session_id='".$session_key."'")->row_array();

			if(empty($db_res)){
				$res['session_parsed'] = $session_key;
				$res['response'] = array('token' => null);
				$res['error'] = array('code' => 404, 'message' => 'session invalid');
				$this->session->sess_destroy();
				$this->response($res);
				return null;
			}

			$id_tutor = $db_res['tutor_id'];
			$getnamatutor = $this->db->query("SELECT first_name FROM tbl_user WHERE id_user='$id_tutor'")->row_array();
			$namatutor = $getnamatutor['first_name'];
			$class_id = $db_res['class_id'];
			$subject_id = $db_res['subject_id'];
			// CHECK AVAILABILITY TOKEN
			$token_exist = $this->db->query("SELECT destroyed_at FROM log_token WHERE session_id='".$session_key."'")->row_array();

			if(!empty($token_exist) && $token_exist['destroyed_at'] != '0000-00-00 00:00:00'){
				$res['session_parsed'] = $session_key;
				$res['response'] = array('token' => null);
				$res['error'] = array('code' => 404, 'message' => 'token invalid');
				$this->session->sess_destroy();
				$this->response($res);
				return null;
			}
			// LOAD BALANCING

			$room_active_janus = $this->db->query("SELECT * FROM temp_active_room WHERE class_id='$class_id'")->row_array();
			if(!empty($room_active_janus)){
				$this->janus_server = $room_active_janus['janus'];
			}else{
				// TAKE ALL PARTICIPANT IN THAT CLASS
				$visible = json_decode($db_res['participant'],true);
				$incoming_kuota = 0;
				$local_region = 0;
				$interlocal_region = 0;
				$chosen_janus = "";

				$country_code = $this->db->query("SELECT user_countrycode FROM tbl_user WHERE id_user='$id_tutor'")->row_array()['user_countrycode'];
				if($country_code == "+62" || $country_code == "62"){
					$local_region++;
				}else{
					$interlocal_region++;
				}
				$incoming_kuota++;

				if($visible['visible'] == "followers"){
					foreach ($visible['participant'] as $k => $value) {
						$id_user = $value['id_user'];
						$country_code = $this->db->query("SELECT user_countrycode FROM tbl_user WHERE id_user='$id_user'")->row_array()['user_countrycode'];
						if($country_code == "+62" || $country_code == "62"){
							$local_region++;
						}else{
							$interlocal_region++;
						}
						$incoming_kuota++;
					}
				}else if($visible['visible'] == "private" || $visible['visible'] == "group"){
					foreach ($visible['participant'] as $k => $value) {
						$id_user = $value['id_user'];
						$country_code = $this->db->query("SELECT user_countrycode FROM tbl_user WHERE id_user='$id_user'")->row_array()['user_countrycode'];
						if($country_code == "+62" || $country_code == "62"){
							$local_region++;
						}else{
							$interlocal_region++;
						}
						$incoming_kuota++;
					}
				}
				$ttl_active_kuota = $this->db->query("SELECT janus, SUM(kuota) as kuota FROM temp_active_room GROUP BY janus")->result_array();
				if($local_region > $interlocal_region){
					// PRESENTASE TERBESAR INDONESIA
					$cm_continent = 'id';
					$list_janus = array("c4.classmiles.com" => 0,"c3.classmiles.com" => 0);
					foreach ($ttl_active_kuota as $key => $value) {
						if(in_array($value['janus'], $list_janus)){
							$list_janus[$value['janus']] = $value['kuota'];
						}
					}
					foreach ($list_janus as $key => $value) {
						if($value < 200 && ($value+$incoming_kuota) <= 200){
							$chosen_janus = $key;
							break;
						}
					}
					if($chosen_janus == "")
						$chosen_janus = "c4.classmiles.com"; //JANUS SAMPAH
				}else{
					$cm_continent = 'sg';
					$list_janus = array("c102.classmiles.com" => 0);
					foreach ($ttl_active_kuota as $key => $value) {
						if(in_array($value['janus'], $list_janus)){
							$list_janus[$value['janus']] = $value['kuota'];
						}
					}
					foreach ($list_janus as $key => $value) {
						if($value < 200 && ($value+$incoming_kuota) <= 200){
							$chosen_janus = $key;
							break;
						}
					}
					if($chosen_janus == "")
						$chosen_janus = "c102.classmiles.com"; //JANUS SAMPAH
				}
				$this->db->simple_query("INSERT INTO temp_active_room VALUES(NULL,'$cm_continent','$chosen_janus','$class_id',$incoming_kuota,now());");
				$this->janus_server = $chosen_janus;
			}
			// $this->janus_server  = "c2.classmiles.com";
			if(empty($token_exist)){
				// echo $ari;
				$this->session->set_userdata('global_session',$session_key);
				// TO JANUS
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://".$this->janus_server.":7889/admin");
				// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				// curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_POST,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$ari);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_TIMEOUT_MS,4000); 

				$curl_rets = curl_exec($ch);

				//if(curl_errno($ch))
				//{
				//   echo 'Curl error: ' . curl_error($ch);
				//}

				if(empty($curl_rets)){
					$res['response'] = array('token' => null);
					$res['error'] = array('code' => 503, 'message' => 'core janus server is offline');	
					$this->response($res);
					return null;
				}else{
					$cret = json_decode($curl_rets,true);
					if($cret['janus'] == "error"){
						$res['error'] = array('code' => $cret['error']['code'], 'message' => $cret['error']['reason']);
						$this->response($res);
						return null;
					}else{
						// HERE HAVE TO BE INTEGRATE SESSION PARSED WITH TOKEN TO DB 
						$this->db->simple_query("INSERT INTO log_token(token,session_id,ip) VALUES('".$send['token']."','".$session_key."','".$_SERVER['REMOTE_ADDR']."')");
					}
				}
			}
			

			// CHECK TIME REQUESTED
			$time_logs = $this->db->query("SELECT * FROM log_time WHERE session_id='$session_key'")->row_array();

			if(empty($time_logs)){
				$this->db->simple_query("INSERT INTO log_time(session_id,interval_time,current_update,last_update) VALUES('$session_key',0,now(),now())");
			}else{
				$last_update = strtotime($time_logs['current_update']);
				$current_time = $time_logs['current_update'];
				$now = strtotime(date('Y-m-d H:i:s'));
				$interval = $now-$last_update;

				// INTERVAL MORE THAN 25
				if($interval >=25){
					$res['response'] = array('token' => null);
					$res['error'] = array('code' => 504, 'message' => 'session has timeout.');
					$this->db->simple_query("UPDATE log_time SET interval_time=0, current_update=now(), last_update=now() WHERE session_id='$session_key'");
					$this->db->simple_query("UPDATE tbl_participant_session SET total_ptime=total_ptime+$interval WHERE session_id='$session_key'");
					$this->response($res);
					return null;
				}
				$this->db->simple_query("UPDATE log_time SET interval_time=$interval, current_update=now(), last_update='$current_time' WHERE session_id='$session_key'");
				$this->db->simple_query("UPDATE tbl_participant_session SET total_ptime=total_ptime+$interval WHERE session_id='$session_key'");
			}
		}else{
			$res['error'] = array('code' => 400, 'message' => 'parameters incomplete');
			$this->response($res);
			return null;
		}

		// RAISE HAND PROCCESS
		$rhand_state = $this->db->query("SELECT *FROM log_rhand WHERE session_id='$session_key' AND status=0")->row_array();
		
		if(!empty($rhand_state)){
			$rhand_state = true;
		}else{
			$rhand_state = false;
		}

		if($db_res['id_user'] == $id_tutor){
			$res['response'] = array('token' => $send['token'],'janus' => $this->janus_server,'id_user' => $db_res['id_user'], 'usertype' => $db_res['usertype_id'], 'display_name' => $db_res['user_name'], 'first_name' => $db_res['first_name'], 'display_email' => $db_res['email'], 'class_id' => $db_res['class_id'],'id_tutor' => $id_tutor, 'class_name' => $db_res['name'],'class_description' => $db_res['description'], 'start_time' => $db_res['start_time'], 'finish_time' => $db_res['finish_time'],'end_time' => explode(' ', $db_res['finish_time'])[1] , 'st_tour' => $db_res['st_tour'], 'template_type' => $db_res['template_type'], 'class_type' => $db_res['class_type'], 'nama_tutor' => $namatutor);

			// STUDENT RAISE HAND
			$data_raisehand = $this->db->query("SELECT tlr.*,tps.class_id, tps.id_user FROM `log_rhand` as tlr INNER JOIN tbl_participant_session as tps ON tlr.session_id=tps.session_id 
				WHERE tlr.status=0 AND tps.class_id='".$db_res['class_id']."'")->result_array();

			if(!empty($data_raisehand)){
				$res['student_rhand'] = array();
				foreach ($data_raisehand as $key => $value) {
					$student_rhand = $this->db->query("SELECT tu.id_user, tu.user_name, tu.email, tps.session_id, tps.class_id FROM tbl_user as tu INNER JOIN tbl_participant_session as tps ON tps.id_user=tu.id_user WHERE tps.class_id='".$db_res['class_id']."' AND tu.id_user='".$value['id_user']."' ")->row_array();
					$res['student_rhand'][$key] = $student_rhand;					
				}
			}
		}else{
			$rhand_statee = $this->db->query("SELECT *FROM log_rhand WHERE session_id='$session_key' ORDER BY uid DESC LIMIT 1")->row_array();
			$status_rhand = $rhand_statee['status'];
			$res['response'] = array('token' => $send['token'],'janus' => $this->janus_server,'id_user' => $db_res['id_user'], 'usertype' => $db_res['usertype_id'], 'display_name' => $db_res['user_name'], 'first_name' => $db_res['first_name'], 'display_email' => $db_res['email'], 'class_id' => $db_res['class_id'],'id_tutor' => $id_tutor, 'class_name' => $db_res['name'],'class_description' => $db_res['description'], 'start_time' => $db_res['start_time'], 'finish_time' => $db_res['finish_time'],'end_time' => explode(' ', $db_res['finish_time'])[1] , 'st_tour' => $db_res['st_tour'], 'template_type' => $db_res['template_type'], 'class_type' => $db_res['class_type'], 'raise_hand' => $status_rhand, 'nama_tutor' => $namatutor);
		}

		// ROOM STATE
		$source = "";
		$pos = 0;
		// $db_res['break_state'] = 1;
		if($db_res['break_state'] == 1){
			$pos = $db_res['break_pos'];
		}
		$res['room_state'] = array("break_state" => $db_res['break_state'],"play_pos" => $pos);
		
		$res['error'] = array('code' => 200, 'message' => 'success');

		$this->response($res);

		

	}

	public function get_token_for_continent_post()
	{
		$DB_UTAMA = $this->load->database('db_utama', TRUE);
		$res = array();
		$res['method'] = "get_token";
		$res['transaction'] = null;
		$res['response'] = array();
		$time = time();

		if($this->post('session') != ''){
			$session_key = $this->post('session');
			$class_id = $this->post('class_id');
			$janus = $this->post('janus');
			$user_utc = $this->post('user_utc');
			$server_utc = $this->Rumus->getGMTOffset();
			$interval   = $user_utc - $server_utc;

			// PREPARE FOR ADD TOKEN TO JANUS
			$send = array();
			$send['janus'] = "add_token";
			$send['token'] = sha1($session_key);

			$send['transaction'] = $this->Rumus->RandomString();
			$send['admin_secret'] = "admin556677";

			$res['transaction'] = $send['transaction'];

			$ari = json_encode($send);
			
			// CHECK AVAILABILITY TOKEN
			$token_exist = $this->db->query("SELECT destroyed_at FROM log_token WHERE session_id='".$session_key."'")->row_array();

			if(!empty($token_exist) && $token_exist['destroyed_at'] != '0000-00-00 00:00:00'){
				$res['session_parsed'] = $session_key;
				$res['response'] = array('token' => null);
				$res['error'] = array('code' => 404, 'message' => 'token invalid');
				$this->session->sess_destroy();
				$this->response($res);
				return null;
			}

			// $this->janus_server  = "c2.classmiles.com";
			if(empty($token_exist)){
				// echo $ari;
				$this->session->set_userdata('global_session',$session_key);
				// TO JANUS
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://".$janus.":7889/admin");
				// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				// curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_POST,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$ari);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_TIMEOUT_MS,4000); 

				$curl_rets = curl_exec($ch);

				//if(curl_errno($ch))
				//{
				//   echo 'Curl error: ' . curl_error($ch);
				//}

				if(empty($curl_rets)){
					$res['response'] = array('token' => null);
					$res['error'] = array('code' => 503, 'message' => 'core janus server is offline');	
					$this->response($res);
					return null;
				}else{
					$cret = json_decode($curl_rets,true);
					if($cret['janus'] == "error"){
						$res['error'] = array('code' => $cret['error']['code'], 'message' => $cret['error']['reason']);
						$this->response($res);
						return null;
					}else{
						// HERE HAVE TO BE INTEGRATE SESSION PARSED WITH TOKEN TO DB 
						$this->db->simple_query("INSERT INTO log_token(token,session_id,ip) VALUES('".$send['token']."','".$session_key."','".$_SERVER['REMOTE_ADDR']."')");
					}
				}
			}
			

			// CHECK TIME REQUESTED
			$time_logs = $this->db->query("SELECT * FROM log_time WHERE session_id='$session_key'")->row_array();

			if(empty($time_logs)){
				$this->db->simple_query("INSERT INTO log_time(session_id,interval_time,current_update,last_update) VALUES('$session_key',0,now(),now())");
			}else{
				$last_update = strtotime($time_logs['current_update']);
				$current_time = $time_logs['current_update'];
				$now = strtotime(date('Y-m-d H:i:s'));
				$interval = $now-$last_update;

				// INTERVAL MORE THAN 25
				if($interval >=25){
					$res['response'] = array('token' => null);
					$res['error'] = array('code' => 504, 'message' => 'session has timeout.');
					$this->db->simple_query("UPDATE log_time SET interval_time=0, current_update=now(), last_update=now() WHERE session_id='$session_key'");
					$this->db->simple_query("UPDATE tbl_participant_session SET total_ptime=total_ptime+$interval WHERE session_id='$session_key'");
					$this->response($res);
					return null;
				}
				$this->db->simple_query("UPDATE log_time SET interval_time=$interval, current_update=now(), last_update='$current_time' WHERE session_id='$session_key'");
				$this->db->simple_query("UPDATE tbl_participant_session SET total_ptime=total_ptime+$interval WHERE session_id='$session_key'");
			}
		}else{
			$res['error'] = array('code' => 400, 'message' => 'parameters incomplete');
			$this->response($res);
			return null;
		}

		// RAISE HAND PROCCESS
		$rhand_state = $this->db->query("SELECT *FROM log_rhand WHERE session_id='$session_key' AND status=0")->row_array();
		
		if(!empty($rhand_state)){
			$rhand_state = true;
		}else{
			$rhand_state = false;
		}

		if($db_res['id_user'] == $id_tutor){
			$res['response'] = array('token' => $send['token']);

			// STUDENT RAISE HAND
			$data_raisehand = $this->db->query("SELECT tlr.*,tps.class_id, tps.session_id, tps.id_user FROM `log_rhand` as tlr INNER JOIN tbl_participant_session as tps ON tlr.session_id=tps.session_id 
				WHERE tlr.status=0 AND tps.class_id='$class_id'")->result_array();

			if(!empty($data_raisehand)){
				$res['student_rhand'] = array();
				foreach ($data_raisehand as $key => $value) {
					$student_rhand = $this->db->query("SELECT id_user, user_name, email FROM tbl_user WHERE id_user='".$value['id_user']."' ")->row_array();
					$res['student_rhand'][$key] = array_merge($student_rhand, array("session_id" => $value['session_id'], "class_id" => $class_id));
				}
			}
		}else{
			$rhand_statee = $this->db->query("SELECT *FROM log_rhand WHERE session_id='$session_key' ORDER BY uid DESC LIMIT 1")->row_array();
			$status_rhand = $rhand_statee['status'];
			$res['response'] = array('token' => $send['token'], 'raise_hand' => $status_rhand);
		}

		// ROOM STATE
		$source = "";
		$pos = 0;
		// $db_res['break_state'] = 1;
		$db_res = $DB_UTAMA->query("SELECT * FROM tbl_class WHERE class_id='$class_id'")->row_array();
		if($db_res['break_state'] == 1){
			$pos = $db_res['break_pos'];
		}
		$res['room_state'] = array("break_state" => $db_res['break_state'],"play_pos" => $pos);
		
		$res['error'] = array('code' => 200, 'message' => 'success');

		$this->response($res);

		

	}

	public function get_static_data_post()
	{
		$DB_UTAMA = $this->load->database('db_utama', TRUE);
		$res = array();
		$res['method'] = "get_token";
		$res['transaction'] = null;
		$res['response'] = array();
		$time = time();

		if($this->post('session') != ''){
			// echo "masuk bro";
			// return false;
			$session_key = $this->post('session');
			$user_utc = $this->post('user_utc');
			$server_utc = $this->Rumus->getGMTOffset();
			$interval   = $user_utc - $server_utc;

			// PREPARE FOR ADD TOKEN TO JANUS
			//   DYNAMIC DATA //
			/*$send = array();
			$send['janus'] = "add_token";
			$send['token'] = sha1($session_key);

			$send['transaction'] = $this->Rumus->RandomString();
			$send['admin_secret'] = "admin556677";

			$res['transaction'] = $send['transaction'];

			$ari = json_encode($send);*/
			// CHECK EXISTANCE SESSION IN DB

			$db_res = $DB_UTAMA->query("SELECT tu.id_user, tu.user_name, tu.first_name, tu.email, tu.usertype_id, tps.class_id, tc.participant, tc.break_state,tc.break_pos, tc.subject_id, tc.name, tc.description, tc.tutor_id, tps.session_id, DATE_ADD(tc.start_time, INTERVAL $interval minute) as start_time, DATE_ADD(tc.finish_time, INTERVAL $interval minute) as finish_time, tu.st_tour, tc.template_type, tc.class_type FROM tbl_user as tu INNER JOIN ( tbl_participant_session as tps INNER JOIN tbl_class as tc ON tps.class_id=tc.class_id) ON tu.id_user=tps.id_user WHERE tps.session_id='".$session_key."'")->row_array();

			if(empty($db_res)){
				$res['session_parsed'] = $session_key;
				$res['response'] = array('token' => null);
				$res['error'] = array('code' => 404, 'message' => 'session invalid');
				$this->session->sess_destroy();
				$this->response($res);
				return null;
			}

			$id_tutor = $db_res['tutor_id'];
			$getnamatutor = $DB_UTAMA->query("SELECT first_name FROM tbl_user WHERE id_user='$id_tutor'")->row_array();
			$namatutor = $getnamatutor['first_name'];
			$class_id = $db_res['class_id'];
			$subject_id = $db_res['subject_id'];

			// LOAD BALANCING

			$room_active_janus = $DB_UTAMA->query("SELECT * FROM temp_active_room WHERE class_id='$class_id'")->row_array();
			if(!empty($room_active_janus)){
				$this->janus_server = $room_active_janus['janus'];
				$cm_continent = $room_active_janus['cm_continent'];
			}

			// CHECK AVAILABILITY TOKEN
			// DYNAMIC DATA //
			/*$token_exist = $this->db->query("SELECT destroyed_at FROM log_token WHERE session_id='".$session_key."'")->row_array();

			if(!empty($token_exist) && $token_exist['destroyed_at'] != '0000-00-00 00:00:00'){
				$res['session_parsed'] = $session_key;
				$res['response'] = array('token' => null);
				$res['error'] = array('code' => 404, 'message' => 'token invalid');
				$this->session->sess_destroy();
				$this->response($res);
				return null;
			}

			if(empty($token_exist)){
				// echo $ari;
				$this->session->set_userdata('global_session',$session_key);
				// TO JANUS
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://".$this->janus_server.":7889/admin");
				// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				// curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_POST,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$ari);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_TIMEOUT_MS,4000); 

				$curl_rets = curl_exec($ch);

				//if(curl_errno($ch))
				//{
				//   echo 'Curl error: ' . curl_error($ch);
				//}

				if(empty($curl_rets)){
					$res['response'] = array('token' => null);
					$res['error'] = array('code' => 503, 'message' => 'core janus server is offline');	
					$this->response($res);
					return null;
				}else{
					$cret = json_decode($curl_rets,true);
					if($cret['janus'] == "error"){
						$res['error'] = array('code' => $cret['error']['code'], 'message' => $cret['error']['reason']);
						$this->response($res);
						return null;
					}else{
						// HERE HAVE TO BE INTEGRATE SESSION PARSED WITH TOKEN TO DB 
						$this->db->simple_query("INSERT INTO log_token(token,session_id,ip) VALUES('".$send['token']."','".$session_key."','".$_SERVER['REMOTE_ADDR']."')");
					}
				}
			}
			*/
			

			// CHECK TIME REQUESTED
			// DYNAMIC DATA //
			/*$time_logs = $this->db->query("SELECT * FROM log_time WHERE session_id='$session_key'")->row_array();

			if(empty($time_logs)){
				$this->db->simple_query("INSERT INTO log_time(session_id,interval_time,current_update,last_update) VALUES('$session_key',0,now(),now())");
			}else{
				$last_update = strtotime($time_logs['current_update']);
				$current_time = $time_logs['current_update'];
				$now = strtotime(date('Y-m-d H:i:s'));
				$interval = $now-$last_update;

				// INTERVAL MORE THAN 25
				if($interval >=25){
					$res['response'] = array('token' => null);
					$res['error'] = array('code' => 504, 'message' => 'session has timeout.');
					$this->db->simple_query("UPDATE log_time SET interval_time=0, current_update=now(), last_update=now() WHERE session_id='$session_key'");
					$this->db->simple_query("UPDATE tbl_participant_session SET total_ptime=total_ptime+$interval WHERE session_id='$session_key'");
					$this->response($res);
					return null;
				}
				$this->db->simple_query("UPDATE log_time SET interval_time=$interval, current_update=now(), last_update='$current_time' WHERE session_id='$session_key'");
				$this->db->simple_query("UPDATE tbl_participant_session SET total_ptime=total_ptime+$interval WHERE session_id='$session_key'");
			}*/
		}else{
			$res['error'] = array('code' => 400, 'message' => 'parameters incomplete');
			$this->response($res);
			return null;
		}

		// RAISE HAND PROCCESS
		// DYNAMIC DATA //
		/*
		$rhand_state = $this->db->query("SELECT *FROM log_rhand WHERE session_id='$session_key' AND status=0")->row_array();
		
		if(!empty($rhand_state)){
			$rhand_state = true;
		}else{
			$rhand_state = false;
		}
		*/

		if($db_res['id_user'] == $id_tutor){
			$res['response'] = array('janus' => $this->janus_server,'id_user' => $db_res['id_user'], 'usertype' => $db_res['usertype_id'], 'display_name' => $db_res['user_name'], 'first_name' => $db_res['first_name'], 'display_email' => $db_res['email'], 'class_id' => $db_res['class_id'],'id_tutor' => $id_tutor, 'class_name' => $db_res['name'],'class_description' => $db_res['description'], 'start_time' => $db_res['start_time'], 'finish_time' => $db_res['finish_time'],'end_time' => explode(' ', $db_res['finish_time'])[1] , 'st_tour' => $db_res['st_tour'], 'template_type' => $db_res['template_type'], 'class_type' => $db_res['class_type'], 'nama_tutor' => $namatutor);

			// STUDENT RAISE HAND
			// DYNAMIC DATA //
			/*$data_raisehand = $this->db->query("SELECT tlr.*,tps.class_id, tps.id_user FROM `log_rhand` as tlr INNER JOIN tbl_participant_session as tps ON tlr.session_id=tps.session_id 
				WHERE tlr.status=0 AND tps.class_id='".$db_res['class_id']."'")->result_array();

			if(!empty($data_raisehand)){
				$res['student_rhand'] = array();
				foreach ($data_raisehand as $key => $value) {
					$student_rhand = $this->db->query("SELECT tu.id_user, tu.user_name, tu.email, tps.session_id, tps.class_id FROM tbl_user as tu INNER JOIN tbl_participant_session as tps ON tps.id_user=tu.id_user WHERE tps.class_id='".$db_res['class_id']."' AND tu.id_user='".$value['id_user']."' ")->row_array();
					$res['student_rhand'][$key] = $student_rhand;					
				}
			}*/
		}else{
			// DYNAMIC DATA //
			/*$rhand_statee = $this->db->query("SELECT *FROM log_rhand WHERE session_id='$session_key' ORDER BY uid DESC LIMIT 1")->row_array();
			$status_rhand = $rhand_statee['status'];*/
			$res['response'] = array('janus' => $this->janus_server,'id_user' => $db_res['id_user'], 'usertype' => $db_res['usertype_id'], 'display_name' => $db_res['user_name'], 'first_name' => $db_res['first_name'], 'display_email' => $db_res['email'], 'class_id' => $db_res['class_id'],'id_tutor' => $id_tutor, 'class_name' => $db_res['name'],'class_description' => $db_res['description'], 'start_time' => $db_res['start_time'], 'finish_time' => $db_res['finish_time'],'end_time' => explode(' ', $db_res['finish_time'])[1] , 'st_tour' => $db_res['st_tour'], 'template_type' => $db_res['template_type'], 'class_type' => $db_res['class_type'], 'nama_tutor' => $namatutor);
		}

		// ROOM STATE
		// DYNAMIC DATA //
		/*$pos = 0;
		// $db_res['break_state'] = 1;
		if($db_res['break_state'] == 1){
			$pos = $db_res['break_pos'];
		}
		$res['room_state'] = array("break_state" => $db_res['break_state'],"play_pos" => $pos);*/
		
		$res['error'] = array('code' => 200, 'message' => 'success');

		$this->response($res);

	}
	public function face_exist_post($value='')
	{
		$rets = array();
		$res['method'] = "face_exist";
		$res['data'] = array();

		$image = $this->post('image');
		$source = $this->post('source');

		if ($this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                // $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

            $req = array("requests" => array( array( "image" => array(), "features" => array( array("type" => "FACE_DETECTION", "maxResults" => 20) ) )));

           	if($image != null) {
           		$image = str_replace(' ','+',$image); //replacing ' ' with '+'
           		$req['requests'][0]['image']['content'] = $image;
           	}
           	if($source != null) {
           		$req['requests'][0]['image']['source']['imageUri'] = $source;
           	}
           	$req = json_encode($req);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://vision.googleapis.com/v1/images:annotate?key=". VISION_API_KEY);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
			// curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
			// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$req);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000);
			$curl_rets = curl_exec($ch);

			$res = json_decode($curl_rets, TRUE);
			// echo count($res['responses'][0]['faceAnnotations']);
			// print_r($curl_rets);
			if(isset($res['responses'][0]['faceAnnotations']) && count($res['responses'][0]['faceAnnotations']) == 1) {
				$rets['status'] = true;
				$rets['code'] = 1;
				$rets['message'] = "face existed";
				$this->response($rets);
			}else{
				if(isset($res['responses'][0]['error']['message'])) {
					$rets['status'] = false;
					$rets['code'] = -100;
					$rets['message'] = $res['responses'][0]['error']['message'];
					$this->response($rets);
				}else if(isset($res['error'])) {
					$rets['status'] = false;
					$rets['code'] = -100;
					$rets['message'] = $res['error']['message'];
					$this->response($rets);
				}else{
					$rets['status'] = false;
					$rets['code'] = -1;
					$rets['message'] = "face not existed";
					$this->response($rets);
				}
			}
		}

		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}


	public function destroy_token_post($value='')
	{
		$res = array();
		$res['method'] = "destroy_token";
		$res['response'] = array();
		$time = time();

		if($this->post('token') != ''){

			// PREPARE FOR SEND REMOVE_TOKEN TO JANUS
			$send = array();
			$send['janus'] = "remove_token";
			$send['transaction'] = $this->Rumus->RandomString();
			$send['token'] = $this->post('token');
			$send['admin_secret'] = "admin556677";

			$res['transaction'] = $send['transaction'];

			$ari = json_encode($send);

			// TO JANUS
			/*
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://c1.classmiles.com:7889/admin");
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch,CURLOPT_POST,1);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$ari);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch,CURLOPT_TIMEOUT_MS,4000); 

			$curl_rets = curl_exec($ch);

			if(empty($curl_rets)){
				$res['response'] = array('token' => null);
				$res['error'] = array('code' => 503, 'message' => 'core janus server is offline');
			}else{
				$cret = json_decode($curl_rets,true);
				if($cret['janus'] == "error"){
					$res['error'] = array('code' => $cret['error']['code'], 'message' => $cret['error']['reason']);	
				}else{
			*/
					$res['error'] = array('code' => 200, 'message' => 'success');	
			/*
					$this->session->sess_destroy('global_token');
					$this->db->simple_query("UPDATE tbl_log_token SET destroyed_at=now() WHERE token='".$this->post('token')."' AND hash_id='".$this->session->userdata('global_session')."'");
				}
			}
			*/
		}else{
			$res['error'] = array('code' => 400, 'message' => 'parameters incomplete');
		}
		$this->response($res);
	}

	public function raise_hand_get()
	{
		$session = $this->get('session');
		$res['method'] = "raise_hand";
		$res['transaction'] = $this->Rumus->RandomString();

		$db_res = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE tps.session_id='".$session."'")->row_array();

		if(empty($db_res)){
			$res['session_parsed'] = $session;
			$res['status'] = false;
			$res['message'] = "Invalid session";
			$this->response($res);
			return null;
		}

		$unrespond_rhand = $this->db->query("SELECT *FROM log_rhand WHERE session_id='".$session."' AND status=0")->row_array();

		if(empty($unrespond_rhand)){
			$this->db->simple_query("INSERT INTO log_rhand(session_id) VALUES('".$session."')");
		}
		$res['status'] = true;
		$res['message'] = 'Raise hand was sent to tutor';

		$this->response($res);

	}

	public function audio_mute_get()
	{
		$session = $this->get('session');
		$res['method'] = "audio_mute";
		$res['transaction'] = $this->Rumus->RandomString();

		$db_res = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE tps.session_id='".$session."'")->row_array();

		if(empty($db_res)){
			$res['session_parsed'] = $session;
			$res['status'] = false;
			$res['message'] = "Invalid session";
			$this->response($res);
			return null;
		}

		$unrespond_rhand = $this->db->query("SELECT *FROM log_audio WHERE session_id='".$session."' AND status=0")->row_array();

		if(empty($unrespond_rhand)){
			$this->db->simple_query("INSERT INTO log_audio(session_id) VALUES('".$session."')");
		}
		$res['status'] = true;
		$res['message'] = 'Audio List was sent to tutor';

		$this->response($res);

	}

	public function rec_chat_post()
	{
		$session = $this->get('session');
		$res['method'] = "rec_chat";
		$res['transaction'] = $this->Rumus->RandomString();

		if($this->post('class_id') != '' && $this->post('chat_arr') != ''){
			$class_id = htmlentities($this->post('class_id'));
			foreach ($this->post('chat_arr') as $key => $value) {
				$this->db->simple_query("INSERT INTO rec_chat(class_id,text,sender_name, datetime) VALUES('{$class_id}','".$value['text']."','".$value['sender_name']."','".$value['datetime']."')");
			}
			$res['status'] = true;
			$res['message'] = 'chat saved';
		}else{
			$res['status'] = false;
			$res['message'] = 'field incomplete';
		}

		$this->response($res);

	}
	public function raise_hand_destroy_get()
	{
		$session = $this->get('session');
		$res['method'] = "raise_hand_destroy";
		$res['transaction'] = $this->Rumus->RandomString();

		$db_res = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE tps.session_id='".$session."'")->row_array();

		if(empty($db_res)){
			$res['session_parsed'] = $session;
			$res['status'] = false;
			$res['message'] = "Invalid session";
			$this->response($res);
		}

		$unrespond_rhand = $this->db->query("SELECT *FROM log_rhand WHERE session_id='".$session."'")->row_array();
		if(!empty($unrespond_rhand)){
			$this->db->simple_query("UPDATE log_rhand SET status=1 WHERE session_id='".$session."'");
		}
		$res['status'] = true;
		$res['message'] = 'Raise hand successfully destroyed.';

		$this->response($res);
	}

	public function raise_hand_call_get()
	{
		$session = $this->get('session');
		$res['method'] = "raise_hand_destroy";
		$res['transaction'] = $this->Rumus->RandomString();

		$db_res = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE tps.session_id='".$session."'")->row_array();

		if(empty($db_res)){
			$res['session_parsed'] = $session;
			$res['status'] = false;
			$res['message'] = "Invalid session";
			$this->response($res);
		}

		$unrespond_rhand = $this->db->query("SELECT *FROM log_rhand WHERE session_id='".$session."'")->row_array();
		if(!empty($unrespond_rhand)){
			$this->db->simple_query("UPDATE log_rhand SET status=2 WHERE session_id='".$session."'");
		}
		$res['status'] = true;
		$res['message'] = 'Raise hand successfully destroyed.';

		$this->response($res);
	}

	public function break_get()
	{
		$res['method'] = "break";
		$res['transaction'] = $this->Rumus->RandomString();

		$session = htmlentities($this->get('session'));
		$class_id = $this->db->query("SELECT class_id FROM tbl_participant_session WHERE session_id='{$session}'")->row_array()['class_id'];
		$this->db->simple_query("UPDATE tbl_class SET break_state=1 WHERE class_id='{$class_id}'");

		$res['status'] = true;
		$res['message'] = 'Successfully break class.';

		$this->response($res);

	}
	public function unbreak_get()
	{
		$res['method'] = "unbreak";
		$res['transaction'] = $this->Rumus->RandomString();

		$session = htmlentities($this->get('session'));
		$class_id = $this->db->query("SELECT class_id FROM tbl_participant_session WHERE session_id='{$session}'")->row_array()['class_id'];
		$this->db->simple_query("UPDATE tbl_class SET break_state=0 WHERE class_id='{$class_id}'");

		$res['status'] = true;
		$res['message'] = 'Successfully unbreak class.';

		$this->response($res);
	}
	public function list_video_get()
	{
		$res['method'] = "list_video";
		$res['transaction'] = $this->Rumus->RandomString();

		$session = htmlentities($this->get('session'));
		$class_id = $this->db->query("SELECT class_id FROM tbl_participant_session WHERE session_id='{$session}'")->row_array()['class_id'];
		$list = $this->db->query("SELECT url_commercial as video_source, play_pos FROM tbl_commercial WHERE class_id='{$class_id}'")->result_array();

		$res['status'] = true;
		$res['list'] = $list;

		$this->response($res);
	}
	public function check_room_post()
	{
		$class_id = $this->post('class_id');
		$session = $this->post('session');

		$db_res = $this->db->query("SELECT * FROM  tbl_participant_session as tps INNER JOIN tbl_class as tc ON tps.class_id=tc.class_id WHERE tps.session_id='{$session}' AND tps.class_id='{$class_id}' AND tc.break_pos!=0")->row_array();

		if(empty($db_res)){
			$res= array('code' => 2, 'message' => 'session invalid');
			$this->response($res);
			return null;
		}
		$res = array('code' => 1, 'message' => 'success');
		$this->response($res);
		return null;

	}
	public function sshare_polling_post()
	{
		/*
		1. tutor_id

		2. id_user

		3. class_id

		4. active = 0 / 1

		5. presentation_id

		*/
		$res['method'] = "sshare_polling";
		$res['transaction'] = $this->Rumus->RandomString();

		$session = $this->post('session');

		// VALIDATE SESSION AND GET CLASS_ID
		$alldata = $this->db->query("SELECT tps.class_id, tps.id_user, tc.tutor_id, lot.token FROM `tbl_participant_session` as tps INNER JOIN tbl_class as tc ON tps.class_id=tc.class_id INNER JOIN log_token as lot ON tps.session_id=lot.session_id WHERE tps.session_id='{$session}'")->row_array();
		if(!empty($alldata)){
			$res['status'] = true;
			$class_id = $alldata['class_id'];
			$tutor_id = $alldata['tutor_id'];
			$id_user = $alldata['id_user'];
			$token = $alldata['token'];

			$res['class_id'] = $class_id;
			$res['tutor_id'] = $tutor_id;
			$res['id_user'] = $id_user;
			$res['token'] = $token;

			// CHECK SCREEN SHARE
			$dscreen = $this->db->query("SELECT * FROM tbl_sshare WHERE class_id='{$class_id}'")->row_array();
			if(!empty($dscreen)){
				$res['active'] = $dscreen['active'];
				$res['presentation_id'] = $dscreen['sshare_id'];
			}else{
				$res['active'] = 0;
				$res['presentation_id'] = '0';
			}

			$this->response($res);
		}
		$res['session_parsed'] = $session;
		$res['status'] = false;
		$res['message'] = "Invalid session";
		$this->response($res);
	}
	public function sshare_activate_post()
	{
		$res['method'] = "sshare_activate";
		$res['transaction'] = $this->Rumus->RandomString();

		$session = $this->post('session');
		$presentation_id = $this->post('presentation_id');

		// VALIDATE SESSION AND GET CLASS_ID
		$dtutor = $this->db->query("SELECT tps.class_id FROM `tbl_participant_session` as tps INNER JOIN tbl_class as tc ON tps.class_id=tc.class_id AND tps.id_user=tc.tutor_id WHERE tps.session_id='{$session}'")->row_array();
		if(!empty($dtutor)){
			$class_id = $dtutor['class_id'];
			// CHECK SCREEN SHARE
			$dscreen = $this->db->query("SELECT * FROM tbl_sshare WHERE class_id='{$class_id}'")->row_array();
			if(!empty($dscreen)){
				$sshare_id = $dscreen['sshare_id'];
				$this->db->simple_query("UPDATE tbl_sshare SET active=1 WHERE sshare_id='{$sshare_id}'");
			}else{
				$this->db->simple_query("INSERT INTO tbl_sshare(sshare_id,class_id, active) VALUES('{$presentation_id}','{$class_id}',1)");
				$sshare_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
			}
			$res['status'] = true;
			$res['message'] = 'Screen Share Activated.';

			$this->response($res);
		}
		$res['session_parsed'] = $session;
		$res['status'] = false;
		$res['message'] = "Invalid session";
		$this->response($res);
	}
	public function sshare_deactivate_post()
	{
		$res['method'] = "sshare_deactivate";
		$res['transaction'] = $this->Rumus->RandomString();

		$session = $this->post('session');

		// VALIDATE SESSION AND GET CLASS_ID
		$dtutor = $this->db->query("SELECT tps.class_id FROM `tbl_participant_session` as tps INNER JOIN tbl_class as tc ON tps.class_id=tc.class_id AND tps.id_user=tc.tutor_id WHERE tps.session_id='{$session}'")->row_array();
		if(!empty($dtutor)){
			$class_id = $dtutor['class_id'];
			// CHECK SCREEN SHARE
			$dscreen = $this->db->query("SELECT * FROM tbl_sshare WHERE class_id='{$class_id}'")->row_array();
			if(!empty($dscreen)){
				$sshare_id = $dscreen['sshare_id'];
				// $this->db->simple_query("UPDATE tbl_sshare SET active=0 WHERE sshare_id='{$sshare_id}'");
				$this->db->simple_query("DELETE FROM tbl_sshare WHERE sshare_id='{$sshare_id}'");
			}
			$res['status'] = true;
			$res['message'] = 'Screen Share Deactivated.';

			$this->response($res);
		}
		$res['session_parsed'] = $session;
		$res['status'] = false;
		$res['message'] = "Invalid session";
		$this->response($res);
	}
	public function turner_stuner_get($value='')
	{
		$res = array();
		$res['method'] = "turner_stuner";
		$res['transaction'] = $this->Rumus->RandomString();
		$res['response'] = array();

		if($this->get('session')){
			$session_key = $this->get('session');

			// CHECK EXISTANCE SESSION IN DB

			$db_res = $this->db->query("SELECT tu.id_user, tu.user_name, tu.email, tu.usertype_id, tps.class_id, tc.break_state,tc.break_pos, tc.name, tc.tutor_id, tps.session_id, cc.maxbitrate, cc.audiocodec, cc.videocodec, cc.resolution FROM tbl_user as tu INNER JOIN ( tbl_participant_session as tps INNER JOIN (tbl_class as tc LEFT JOIN cfg_class as cc ON tc.class_id=cc.class_id ) ON tps.class_id=tc.class_id) ON tu.id_user=tps.id_user WHERE tps.session_id='".$session_key."'")->row_array();

			if(empty($db_res)){
				$res['session_parsed'] = $session_key;
				$res['error'] = array('code' => 404, 'message' => 'session invalid');
				$this->response($res);
				return null;
			}

			$list_turn = array();	
			$list_stun = array();
			$list_turn[] = array("name" => "server1.1", "type" => "turn", "url" => "turn:m1.classmiles.com", "credential" => "654d0a86969ae6bebea6a03a4dbe26a1", "username" => "4644f4b08e09b891fe9ed98027b21541");
			$list_stun[] = array("name" => "server2.1", "type" => "stun", "url" => "stun:m1.classmiles.com", "credential" => "654d0a86969ae6bebea6a03a4dbe26a1", "username" => "4644f4b08e09b891fe9ed98027b21541");
			/*$list_turn[] = array("name" => "server1.1", "type" => "turn", "url" => "turn:m1.classmiles.com", "credential" => "tyh4g92chiwugahrj7hqwt8978sdiguf", "username" => "c1397542364");
			$list_stun[] = array("name" => "server2.1", "type" => "stun", "url" => "stun:m1.classmiles.com", "credential" => "tyh4g92chiwugahrj7hqwt8978sdiguf", "username" => "c1397542364");*/
			$res['response']['list_turn'] = $list_turn;
			$res['response']['list_stun'] = $list_stun;
			$res['response']['class_id'] = $db_res['class_id'];
			$res['response']['maxbitrate'] = $db_res['maxbitrate'] != null ? $db_res['maxbitrate'] : "256000";
			// $res['response']['maxbitrate'] = $db_res['maxbitrate'] != null ? $db_res['maxbitrate'] : "128000";
			$res['response']['audiocodec'] = $db_res['audiocodec'] != null ? $db_res['audiocodec'] : "opus";
			$res['response']['videocodec'] = $db_res['videocodec'] != null ? $db_res['videocodec'] : "vp9";
			$res['response']['resolution'] = $db_res['resolution'] != null ? $db_res['resolution'] : "hires";
			
		}
		$res['error'] = array('code' => 200, 'message' => 'success');
		$this->response($res);

	}






	/*

	==============================================================================
	ACCESS FOR ANDROID
	==============================================================================
	*/




	public function update_class_notif_get()
	{
		$ret['status'] = true;
		$ret['message'] = 'Successful';
		$id_user = htmlentities($this->get('id_user'));
		$q = $this->db->simple_query("UPDATE log_class_notif SET read_status=1 WHERE type='missed' && read_status = 0 && id_user='{$id_user}'");
		$this->response($ret);

	}
	public function update_fcm_post()
	{
		$id_user = htmlentities($this->post('id_user'));
		$fcm_token = htmlentities($this->post('fcm_token'));
		$time = date('Y-m-d H:i:s');
		if ($id_user != '' && $fcm_token != '' && $this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

			$q = $this->db->simple_query("INSERT INTO master_fcm VALUES('{$fcm_token}','{$id_user}','{$time}')");
			if(!$q){
				$q =$this->db->simple_query("UPDATE master_fcm SET fcm_token='{$fcm_token}', update_time='{$time}' WHERE id_user='{$id_user}'");
				$s = $this->db->affected_rows();
				if($s<1 || !$q){
					$ret['status'] = false;
					$ret['message'] = 'Error Occured!';
					$this->response($ret);
				}
			}
			$ret['status'] = true;
			$ret['message'] = 'Successful';
			$this->response($ret);
		}
		$ret['status'] = false;
		$ret['message'] = "Field incomplete.";
		ending:
		$this->response($ret);
		


	}

	public function select_fcm_post()
	{
		
		$id_user = htmlentities($this->post('id_user'));
		$q = $this->db->query("SELECT * FROM master_fcm WHERE id_user = '{$id_user}'")->row_array();
		if (!empty($q)) {
			$baru = $q['fcm_token'];
			if ($baru == 'kosong'.$id_user) {
				$rets['data'] = $baru;
				$rets['status'] = true;
				$rets['message'] = "Data found.";
				$this->response($rets);
			}
			$rets['data'] = $baru;
			$rets['status'] = false;
			$rets['message'] = "Has login.";
			$this->response($rets);
		}
		$rets['status'] = true;
		$rets['message'] = "Go login.";
		$this->response($rets);
	}

	public function login_post()
	{
		$rets['method'] = "login";

		if($this->post('email') != '' && $this->post('password') != ''){
			$un = htmlentities($this->post('email'));
			$ps = htmlentities($this->post('password'));


			$data = $this->db->query("SELECT * FROM tbl_user WHERE email =  '$un'")->row_array();
			// print_r($un+"|"+$ps);
			if(!empty($data) && password_verify($ps, $data['password']) ){
				if ($data['usertype_id'] == 'student') {
					$data = $this->db->query("SELECT * FROM tbl_user AS tu INNER JOIN tbl_profile_student AS tps WHERE tu.email =  '$un' AND tps.student_id = tu.id_user")->row_array();
					$rets['response']['status'] = true;
					$rets['response']['message'] = "Succeed student";
					$rets['response']['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'],"jenjang_id" => $data['jenjang_id'] );
					$access_token = $this->create_access_token($data);
					$rets['response']['access_token'] = $access_token;
					$this->response($rets);
				}
				if ($data['usertype_id'] == 'tutor') {
					$data = $this->db->query("SELECT * FROM tbl_user AS tu INNER JOIN tbl_profile_tutor AS tps WHERE tu.email =  '$un' AND tps.tutor_id = tu.id_user")->row_array();
					$data['education_background'] = unserialize($data['education_background']);
					$data['teaching_experience'] = unserialize($data['teaching_experience']);
					$rets['response']['status'] = true;
					$rets['response']['message'] = "Succeed tutor";
					$rets['response']['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'] );
					$access_token = $this->create_access_token($data);
					$rets['response']['access_token'] = $access_token;
					$this->response($rets);
				}
				if ($data['usertype_id'] == 'moderator') {
					$data = $this->db->query("SELECT * FROM tbl_user WHERE email='$un'")->row_array();
					$rets['response']['status'] = true;
					$rets['response']['message'] = "Succeed Operator";
					$rets['response']['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'] );
					$access_token = $this->create_access_token($data);
					$rets['response']['access_token'] = $access_token;
					$this->response($rets);
				} else {
					$rets['response']['status'] = true;
					$rets['response']['message'] = "Succeed akhir";
					$rets['response']['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'] );
					$this->response($rets);
				}
			}
			$rets['response']['status'] = false;
			$rets['response']['message'] = "Wrong email or password.";
			$this->response($rets);
		}
		$rets['response']['status'] = false;
		$rets['response']['message'] = "Field incomplete.";
		$this->response($rets);
		
	}

	public function googleAuthGetJWT_post()
	{
		$data = $this->post();
		$access_token = $this->create_access_token($data);
		$this->response(array("access_token" => $access_token));
	}

	public function logingoogle_post()
	{
		if($this->post('email') != ''){
			$un = htmlentities($this->post('email'));


			$data = $this->db->query("SELECT * 
				FROM tbl_user 
				WHERE email =  '$un'")->row_array();

			if(!empty($data)){
				if ($data['usertype_id'] == 'student') {
					$data = $this->db->query("SELECT * FROM tbl_user AS tu INNER JOIN tbl_profile_student AS tps WHERE tu.email =  '$un' AND tps.student_id = tu.id_user")->row_array();
					$rets['response']['status'] = true;
					$rets['response']['message'] = "Succeed student";
					$rets['response']['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'],"jenjang_id" => $data['jenjang_id'] );
					$access_token = $this->create_access_token($data);
					$rets['response']['access_token'] = $access_token;
					$this->response($rets);
				}
				if ($data['usertype_id'] == 'tutor') {
					$data = $this->db->query("SELECT * FROM tbl_user AS tu INNER JOIN tbl_profile_tutor AS tps WHERE tu.email =  '$un' AND tps.tutor_id = tu.id_user")->row_array();
					$data['education_background'] = unserialize($data['education_background']);
					$data['teaching_experience'] = unserialize($data['teaching_experience']);
					$rets['response']['status'] = true;
					$rets['response']['message'] = "Succeed tutor";
					$rets['response']['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'] );
					$access_token = $this->create_access_token($data);
					$rets['response']['access_token'] = $access_token;
					$this->response($rets);
				} else {
					$rets['response']['status'] = true;
					$rets['response']['message'] = "Succeed akhir";
					$rets['response']['data'] = array("id_user" => $data['id_user'], "user_name" => $data['user_name'], "user_image" => $data['user_image'], "email" => $data['email'], "usertype_id" => $data['usertype_id'], "status" => $data['status'] );
					$this->response($rets);
				}
			}
			$rets['response']['status'] = false;
			$rets['response']['message'] = "Wrong email or not registered";
			$this->response($rets);
		}
		$rets['response']['status'] = false;
		$rets['response']['message'] = "Field incomplete";
		ending:
		$this->response($rets);
		
	}

	public function descstudent_post()
	{

		if($this->post('id_user') != '' && $this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

			$un = htmlentities($this->post('id_user'));


			$data = $this->db->query("SELECT * 
				FROM tbl_user AS tu
				LEFT JOIN ( tbl_profile_student AS tps INNER JOIN master_jenjang as mj on tps.jenjang_id = mj.jenjang_id ) ON tps.student_id=tu.id_user LEFT JOIN master_provinsi as mp on tu.id_provinsi = mp.provinsi_id INNER JOIN master_country as mc on tu.user_nationality = mc.nicename LEFT JOIN master_school as ms on tps.school_id = ms.school_id
				WHERE tu.id_user =  '$un'")->row_array();

			if(!empty($data)){
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$data['user_image'] = $data['user_image'];
				$rets['data'] = $data;
				$this->response($rets);
			}
			$rets['status'] = false;
			$rets['message'] = "Wrong id user";
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
		
	}

	public function descstudentupdate_post()
	{

		if($this->post('id_user') != '' && $this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			$un = htmlentities($this->post('id_user'));


			$data = $this->db->query("SELECT * 
				FROM tbl_user WHERE id_user =  '$un'")->row_array();

			if(!empty($data)){
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$data['user_image'] = $data['user_image'];
				$rets['data'] = $data;
				$this->response($rets);
			}
			$rets['status'] = false;
			$rets['message'] = "Wrong id user";
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
		
	}
	public function getSchoolDLL_post($value='')
	{
		$type = $this->post('type') != null ? " && type='".substr($this->post('type'), 0,3)."' " : "";

		if ($this->post('provinsi_id') !== null && $this->post('kabupaten_id') === null && $this->post('kecamatan_id') === null) {
			$provinsi_id 	= $this->post('provinsi_id');
			$result = $this->db->query("SELECT kabupaten FROM `master_school` WHERE provinsi='$provinsi_id' $type GROUP BY kabupaten")->result_array();
		}
		else if($this->post('kabupaten_id') !== null && $this->post('kecamatan_id') === null){
			$provinsi_id = $this->post('provinsi_id');
			$kabupaten_id = $this->post('kabupaten_id');
			$result = $this->db->query("SELECT kecamatan FROM `master_school` WHERE provinsi='$provinsi_id' && kabupaten='$kabupaten_id' $type GROUP BY kecamatan;")->result_array();
		}
		else if($this->post('provinsi_id') !== null && $this->post('kabupaten_id') !== null && $this->post('kecamatan_id') !== null){
			$provinsi_id = $this->post('provinsi_id');
			$kabupaten_id = $this->post('kabupaten_id');
			$kecamatan_id = $this->post('kecamatan_id');
			$result = $this->db->query("SELECT * FROM `master_school` WHERE provinsi='$provinsi_id' && kabupaten='$kabupaten_id' && kecamatan='$kecamatan_id' $type;")->result_array();
		}
		else
		{
			$result = $this->db->query("SELECT provinsi FROM `master_school` GROUP BY provinsi")->result_array();
			// $result = $this->db->query("SELECT * FROM master_provinsi")->result_array();
		}

		$res['status'] = true;
		$res['message'] = 'Successful';
		$res['code'] = 200;
		$res['data'] = $result;	
		$this->response($res);	
	}

	public function subject_post() {
		if($this->post('jenjang_id') != ''){
			$un = htmlentities($this->post('jenjang_id'));

			$data = $this->db->query("SELECT *FROM master_subject WHERE jenjang_id = '$un' order by subject_id ASC")->result_array();

			if(!empty($data)){
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = $data;
				$this->response($rets);
			} else {
				$rets['status'] = false;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		$this->response($rets);
	}

	public function allSubject_get()
	{
		if($this->get('id_user') != "" && $this->get('access_token') != ''){

			$pg = $this->get('pg') != null ? $this->get('pg') : 1;
			$rq = $this->get('rq') != null ? $this->get('rq') : 100000;
			$forwat = $this->get('for') != null ? $this->get('for') : "tutor";
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

			$id_user = $this->get('id_user');

			$new_array = array();
			$new_array_counter = 0;
			$jenjang_id = $this->db->query("SELECT jenjang_id FROM tbl_profile_student WHERE student_id='$id_user'")->row_array()['jenjang_id'];
			if ($jenjang_id == "") {
				$jenjang_id = $this->db->query("SELECT jenjang_id FROM tbl_profile_kid WHERE kid_id='$id_user'")->row_array()['jenjang_id'];
			}
			$pure_query = $this->db->query("SELECT *FROM master_subject WHERE jenjang_id like '%\"$jenjang_id\"%' OR jenjang_id = '$jenjang_id'");
			$totalPureRecords = $pure_query->num_rows();
			$pure_query->free_result();

			$real_query = $this->db->query("SELECT *FROM master_subject WHERE jenjang_id like '%\"$jenjang_id\"%' OR jenjang_id = '$jenjang_id' LIMIT ".((intval($pg)-1)*$rq).",".intval($rq));
			$rs 		= $real_query->num_rows();
			$allsub 	= $real_query->result_array();
			if(!empty($allsub)){
				if ($forwat == "tutor") {
					foreach ($allsub as $row => $v) {
						$alltutor = $this->db->query("SELECT ts.usertype_id, ts.user_name, ts.user_image, mc.nicename as country, tb.* FROM tbl_booking as tb  INNER JOIN tbl_user as ts ON tb.id_user=ts.id_user INNER JOIN master_country as mc ON mc.nicename=ts.user_nationality WHERE ts.usertype_id='tutor' AND tb.subject_id='$v[subject_id]'")->result_array();
						
						// $new_array[$row]['tutors'] = array();
						if(!empty($alltutor)){
							
							$new_array[$new_array_counter] = $allsub[$row];
							$new_array[$new_array_counter]['tutors'] = 1;
							$new_array_counter++;
						}
					}
					// foreach ($allsub as $row => $v) {
					// 	if( $row >= (($pg*$rq) - $rq) && $row < ($pg*$rq) ){
					// 		$alltutor = $this->db->query("SELECT ts.usertype_id, ts.user_name, ts.user_image, mc.nicename as country, tb.* FROM tbl_booking as tb  INNER JOIN tbl_user as ts ON tb.id_user=ts.id_user INNER JOIN master_country as mc ON mc.nicename=ts.user_nationality WHERE ts.usertype_id='tutor' AND tb.subject_id='$v[subject_id]'")->result_array();
					// 		$new_array[$new_array_counter] = $allsub[$row];
					// 		$new_array[$new_array_counter]['tutors'] = array();

					// 		if(!empty($alltutor)){
					// 			foreach ($alltutor as $row_tutor => $va) { 
					// 				$tutor_id = $va['id_user'];
					// 				$mybooking = $this->db->query("SELECT *FROM tbl_booking WHERE id_user='$id_user'")->result_array();
					// 				$flag = 0;
					// 				if(!empty($mybooking)){
					// 					foreach ($mybooking as $rowbook => $valbook) {
					// 						if ($tutor_id==$valbook['tutor_id'] && $va['subject_id']==$valbook['subject_id']) {
					// 							$flag = 1;
					// 						}
					// 					}
					// 				}
					// 				$alltutor[$row_tutor]['following'] = $flag;
					// 				$new_array[$new_array_counter]['tutors'][$row_tutor]['tutor_id'] = $tutor_id;
					// 				$new_array[$new_array_counter]['tutors'][$row_tutor]['tutor_name'] = $va['user_name'];
					// 				$new_array[$new_array_counter]['tutors'][$row_tutor]['tutor_image'] = $va['user_image'];
					// 				$new_array[$new_array_counter]['tutors'][$row_tutor]['tutor_country'] = $va['country'];
					// 				$new_array[$new_array_counter]['tutors'][$row_tutor]['following'] = $flag;

					// 			}
					// 		}
					// 		$new_array_counter++;
					// 	}
					// }
				} else {
					$new_array = $allsub;
				}
				// $this->response($new_array);
			}
			$rets['status']			= true;
			$rets['message']		= "Succeed";
			$rets['pg']				= $pg;
			$rets['rq']				= $rq;
			$rets['rs'] 			= $rs;
			$rets['recordsTotal'] 	= $totalPureRecords;
			// $rets['more'] = $pg*$rq >= count($allsub) ? false : true;
			$rets['data'] = $new_array;
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
        ending:
		$this->response($rets);
		
	}

	public function tutorSubjectOne_get() {
		if($this->get('id_user') != "" && $this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			$id_user = $this->get('id_user');
			$subject_id = $this->get('subject_id');
			$alltutor = $this->db->query("SELECT ts.usertype_id, ts.user_name, ts.user_image, mc.nicename as country, tb.* FROM tbl_booking as tb  INNER JOIN tbl_user as ts ON tb.id_user=ts.id_user INNER JOIN master_country as mc ON mc.nicename=ts.user_nationality WHERE ts.usertype_id='tutor' AND tb.subject_id='$subject_id'")->result_array();
			
			$new_array = array();
			if(!empty($alltutor)){
				foreach ($alltutor as $row_tutor => $va) { 
					$tutor_id = $va['id_user'];
					$mybooking = $this->db->query("SELECT *FROM tbl_booking WHERE id_user='$id_user'")->result_array();
					$flag = 0;
					if(!empty($mybooking)){
						foreach ($mybooking as $rowbook => $valbook) {
							if ($tutor_id==$valbook['tutor_id'] && $va['subject_id']==$valbook['subject_id']) {
								$flag = 1;
							}
						}
					}
					$new_array[$row_tutor]['tutor_id'] = $tutor_id;
					$new_array[$row_tutor]['tutor_name'] = $va['user_name'];
					$new_array[$row_tutor]['tutor_image'] = $va['user_image'];
					$new_array[$row_tutor]['tutor_country'] = $va['country'];
					$new_array[$row_tutor]['tutor_verified'] = $va['status'];
					$new_array[$row_tutor]['following'] = $flag;

				}

				$rets['status'] = true;
				$rets['message'] = "Succeed ";
				$rets['data'] = $new_array;
				$this->response($rets);
			}
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete ";
		ending:
		$this->response($rets);
	}

	public function allSubject_bytutor_get()
	{
		if($this->get('id_user') != "" && $this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			$id_user = $this->get('id_user');

			$new_array = array();
			$jenjang_id = $this->db->query("SELECT jenjang_id FROM tbl_profile_student WHERE student_id='$id_user'")->row_array()['jenjang_id'];
			if ($jenjang_id == "") {
				$jenjang_id = $this->db->query("SELECT jenjang_id FROM tbl_profile_kid WHERE kid_id='$id_user'")->row_array()['jenjang_id'];
			}
			$alltutor = $this->db->query("SELECT tb.id_user as tutor_id, tu.user_name as tutor_name, tu.user_image as tutor_image, mc.nicename as tutor_country FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id=ms.subject_id INNER JOIN (tbl_user as tu INNER JOIN master_country as mc ON mc.nicename = tu.user_nationality) ON tb.id_user=tu.id_user WHERE (ms.jenjang_id like '%\"$jenjang_id\"%' OR ms.jenjang_id = '$jenjang_id') AND tb.tutor_id=0 GROUP BY tb.id_user")->result_array();
			if(!empty($alltutor)){
			}
			$rets['status'] = true;
			$rets['message'] = "Success";
			$rets['data'] = $alltutor;
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete ";
		ending:
		$this->response($rets);
		
	}

	public function schedule_post()
	{

		/*$now = date('N');
		$nextMon = date('Y-m-d', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d')))); 
		$oneWeek = date('Y-m-d', strtotime('+6 day', strtotime($nextMon)));

		if($this->post('id_user') != ''){
			$ids = $this->post('id_user');
			$res = $this->db->query("SELECT tc.class_id, tc.participant, tc.subject_id,tc.name as subject_name, tc.description, tc.tutor_id, tu.user_name, tu.user_image, LEFT(tc.start_time,10) as date, SUBSTR(tc.start_time,12,5) as time, SUBSTR(tc.finish_time,12,5) as endtime FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN tbl_booking as tb ON tc.tutor_id=tb.tutor_id where tb.subject_id=tc.subject_id AND tb.id_user=$ids AND start_time>='$nextMon' AND finish_time<='$oneWeek' order by start_time ASC")->result_array();
			if(!empty($res)){
				$rets['status'] = true;
				$rets['message'] = "Succeed";
                foreach ($res as $key => $value) {
					$res[$key]['participant'] = json_decode($value['participant'],true);
				}
				$rets['data'] = $res;
				$this->response($rets);
			}else{
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete".$this->post('id_user');
		$this->response($rets);*/

		$date = $this->post('date');
		$user_utc = $this->post('user_utc');
		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";

		$now = date('N');
		$server_utc = $this->Rumus->getGMTOffset();
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		$interval   = $user_utc - $server_utc;

		/*$nextMon = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
		$nextMon->modify("+$interval minute");*/
		// $todayInClient = $date." 00:00:00";
		// $enddayInClient = $date." 23:59:59";
		$todayInClient = date($date.' 00:00:00', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
		$enddayInClient = date('Y-m-d 23:59:59', strtotime('+6 day', strtotime($todayInClient)));

		$nextMon = DateTime::createFromFormat('Y-m-d H:i:s',$todayInClient);
		$nextMon->modify("-$interval minute");
		$nextMon = $nextMon->format('Y-m-d H:i:s');

		$oneWeek = DateTime::createFromFormat('Y-m-d H:i:s',$enddayInClient);
		$oneWeek->modify("-$interval minute");
		$oneWeek = $oneWeek->format('Y-m-d H:i:s');

		if($this->post('id_user') != ''){
			$ids = $this->post('id_user');
			$res = $this->db->query("SELECT tc.class_id,tc.participant, tc.subject_id,tc.name as subject_name, tc.description, tc.tutor_id, tu.user_name, tu.first_name, tc.template_type, tc.class_type, tu.user_image, tcp.harga, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN tbl_booking as tb ON tc.tutor_id=tb.tutor_id LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id where tb.subject_id=tc.subject_id AND tb.id_user=$ids AND start_time>='$nextMon' AND finish_time<='$oneWeek' order by start_time ASC")->result_array();
			if(!empty($res)){
				$rets['status'] = true;
				$rets['message'] = "Succeed";
                foreach ($res as $key => $value) {
					$res[$key]['participant'] = json_decode($value['participant'],true);
				}
				$rets['data'] = $res;
				$this->response($rets);
			}else{
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete".$this->post('id_user');
		$this->response($rets);
	}

	public function scheduletutor_post()
	{

		$now = date('N');
		$nextMon = date('Y-m-d', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d')))); 
		$oneWeek = date('Y-m-d', strtotime('+13 day', strtotime($nextMon)));

		if($this->post('id_user') != ''){
			$ids = $this->post('id_user');
			$res = $this->db->query("SELECT tc.class_id, tc.subject_id, tc.name as subject_name, mj.jenjang_level, mj.jenjang_name, tc.description, tc.tutor_id, tu.user_name, tu.user_image, tc.start_time, tc.finish_time,LEFT(tc.start_time,10) as date, SUBSTR(tc.start_time,12,5) as time ,SUBSTR(tc.finish_time,12,5) as endtime  FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tu.id_user=tc.tutor_id INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id) ON tc.subject_id=ms.subject_id WHERE tc.tutor_id='$ids' AND start_time>='$nextMon' AND start_time<='$oneWeek' ORDER BY SUBSTRING(start_time,12) ASC, SUBSTRING(start_time,1,10) ASC")->result_array();
			if(!empty($res)){
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = $res;
				$this->response($rets);
			}else{
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete".$this->post('id_user');
		$this->response($rets);
	}

	public function scheduleone_post()
	{		
		$date = $this->post('date');
		$user_utc = $this->post('user_utc');
		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";
		$retok = $this->verify_access_token($this->get('access_token'));

		$now = date('N');
		// $thisBeginDay = date($date.' 00:00:00', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
		// $oneDayAfter = date($date.' 23:59:59', strtotime('+'.$now.' day', strtotime(date('Y-m-d'))));
		$server_utc = $this->Rumus->getGMTOffset();
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		$interval   = $user_utc - $server_utc;

		/*$thisBeginDay = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
		$thisBeginDay->modify("+$interval minute");*/
		$todayInClient = $date." 00:00:00";
		$enddayInClient = $date." 23:59:59";

		$thisBeginDay = DateTime::createFromFormat('Y-m-d H:i:s',$todayInClient);
		$thisBeginDay->modify("-$interval minute");
		$thisBeginDay = $thisBeginDay->format('Y-m-d H:i:s');

		$thisEndDay = DateTime::createFromFormat('Y-m-d H:i:s',$enddayInClient);
		$thisEndDay->modify("-$interval minute");
		$thisEndDay = $thisEndDay->format('Y-m-d H:i:s');

		$oneDayAfter = DateTime::createFromFormat('Y-m-d H:i:s',$thisEndDay);
		$oneDayAfter->modify("+1 day");
		$oneDayAfter = $oneDayAfter->format('Y-m-d H:i:s');

		$oneDayBefore = DateTime::createFromFormat('Y-m-d H:i:s',$thisBeginDay);
		$oneDayBefore->modify("-1 day");
		$oneDayBefore = $oneDayBefore->format('Y-m-d H:i:s');

		if($this->post('id_user') != ''){
			$ids = $this->post('id_user');

			if($retok == false){
				$rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
				goto ending;
			}

			// QUERY BY FOLLOW
			$res = $this->db->query("SELECT tc.class_id,tc.participant, tc.max_participants, tc.subject_id, tc.channel_status, SUBSTR(DATE_ADD(tc.last_order, INTERVAL $interval minute),12,5) as time_order, LEFT(DATE_ADD(tc.last_order, INTERVAL $interval minute),10) as date_order, tc.name as subject_name, ms.english, tc.description, tc.tutor_id, tu.user_name, tu.first_name, tc.template_type, tc.class_type, mc.nicename as country, tu.user_image, tcp.harga, tcp.harga_cm, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN master_country as mc ON mc.nicename = tu.user_nationality INNER JOIN tbl_booking as tb ON tc.tutor_id=tb.tutor_id AND tc.subject_id=tb.subject_id AND tb.id_user=$ids LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id INNER JOIN master_subject as ms ON tc.subject_id=ms.subject_id WHERE  ( ( start_time>='$thisBeginDay' AND start_time<'$thisEndDay' AND finish_time<='$oneDayAfter' ) || ( start_time>='$oneDayBefore' AND finish_time>'$thisBeginDay' AND finish_time<='$thisEndDay' ) )  order by start_time ASC")->result_array();

			$res2 = $this->db->query("SELECT tc.class_id,tc.participant, tc.max_participants, tc.subject_id, tc.channel_status, SUBSTR(DATE_ADD(tc.last_order, INTERVAL $interval minute),12,5) as time_order, LEFT(DATE_ADD(tc.last_order, INTERVAL $interval minute),10) as date_order, tc.name as subject_name, ms.english, tc.description, tc.tutor_id, tu.user_name, tu.first_name, tc.template_type, tc.class_type, mc.nicename as country, tu.user_image, tcp.harga, tcp.harga_cm, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN master_country as mc ON mc.nicename = tu.user_nationality LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id INNER JOIN master_subject as ms ON tc.subject_id=ms.subject_id WHERE tc.participant LIKE '%\"id_user\":\"$ids\"%' AND $ids NOT IN (SELECT tb.id_user FROM tbl_booking as tb WHERE tb.tutor_id=tc.tutor_id AND tb.subject_id=tc.subject_id AND tb.id_user=$ids)  AND ( ( start_time>='$thisBeginDay' AND start_time<'$thisEndDay' AND finish_time<='$oneDayAfter' ) || ( start_time>='$oneDayBefore' AND finish_time>'$thisBeginDay' AND finish_time<='$thisEndDay' ) )  order by start_time ASC")->result_array();

			$get = $this->db->query("SELECT trg.*, DATE_ADD(trg.date_requested, INTERVAL $interval minute) as date_requested_utc, mc.nicename as country, tu.user_name, tu.user_image, ms.subject_name FROM tbl_request as trg INNER JOIN tbl_user as tu ON trg.tutor_id=tu.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id INNER JOIN master_country as mc ON mc.nicename = tu.user_nationality WHERE trg.id_user_requester = '$ids' AND date_requested like '$date%' ORDER BY datetime DESC")->result_array();

			$getg = $this->db->query("SELECT trg.*, DATE_ADD(trg.date_requested, INTERVAL $interval minute) as date_requested_utc, mc.nicename as country, tu.user_name, tu.user_image, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu ON trg.tutor_id=tu.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id INNER JOIN master_country as mc ON mc.nicename = tu.user_nationality WHERE trg.id_user_requester = '$ids' AND date_requested like '$date%' ORDER BY datetime DESC")->result_array();

			// ( ) AND
			 // INNER JOIN tbl_booking as tb ON tc.tutor_id=tb.tutor_id AND tc.subject_id=tb.subject_id AND tb.id_user=$ids
			// return null;
			if(!empty($res) || !empty($res2) || !empty($get) || !empty($getg)){
				$rets['status'] 	= true;
				$rets['message'] 	= "Succeed";
				$new_arr 			= array();
				if(!empty($res)){
					foreach ($res as $key => $value) {
						$res[$key]['participant'] = json_decode($value['participant'],true);
						array_push($new_arr, $res[$key]);
					}
				}
                
				if(!empty($res2)){
					foreach ($res2 as $key => $value) {
						$res2[$key]['participant'] = json_decode($value['participant'],true);
						array_push($new_arr, $res2[$key]);
					}
				}
				function date_compare($a, $b)
				{
				    $t1 = strtotime($a['date']." ".$a['time']);
				    $t2 = strtotime($b['date']." ".$b['time']);
				    return $t1 - $t2;
				}
				usort($new_arr, 'date_compare');

				foreach ($getg as $key => $value) {
					$getg[$key]['id_friends'] = json_decode($value['id_friends'], true);
				}

				$rets['data'] = $new_arr;
				$rets['data_private'] = $get;
				$rets['data_group'] = $getg;
				$this->response($rets);
			}else{
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$rets['data_private'] = null;
				$rets['data_group'] = null;
				$this->response($rets);
			}
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete".$this->post('id_user');
		ending:
		$this->response($rets);
	}

	public function scheduleonetutor_post()
	{		
		$date = $this->post('date');
		$now = date('N');
		$user_utc = $this->post('user_utc');
		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";
		$retok = $this->verify_access_token($this->get('access_token'));

		$now = date('N');
		// $nextMon = date($date.' 00:00:00', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
		// $oneWeek = date($date.' 23:59:59', strtotime('+'.$now.' day', strtotime(date('Y-m-d'))));
		$server_utc = $this->Rumus->getGMTOffset();
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		$interval   = $user_utc - $server_utc;


		/*$nextMon = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
		$nextMon->modify("+$interval minute");*/
		$todayInClient = $date." 00:00:00";
		$enddayInClient = $date." 23:59:59";

		$thisBeginDay = DateTime::createFromFormat('Y-m-d H:i:s',$todayInClient);
		$thisBeginDay->modify("-$interval minute");
		$thisBeginDay = $thisBeginDay->format('Y-m-d H:i:s');

		$thisEndDay = DateTime::createFromFormat('Y-m-d H:i:s',$enddayInClient);
		$thisEndDay->modify("-$interval minute");
		$thisEndDay = $thisEndDay->format('Y-m-d H:i:s');

		$oneDayAfter = DateTime::createFromFormat('Y-m-d H:i:s',$thisEndDay);
		$oneDayAfter->modify("+1 day");
		$oneDayAfter = $oneDayAfter->format('Y-m-d H:i:s');

		$oneDayBefore = DateTime::createFromFormat('Y-m-d H:i:s',$thisBeginDay);
		$oneDayBefore->modify("-1 day");
		$oneDayBefore = $oneDayBefore->format('Y-m-d H:i:s');


		if($this->post('id_user') != ''){
			$ids = $this->post('id_user');

			if($retok == false){
				$rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
				goto ending;
			}

			// $res = $this->db->query("SELECT ms.*, tc.class_id, tc.participant,tc.subject_id, tc.name as subject_name, mj.jenjang_level, mj.jenjang_name, tc.description, tc.tutor_id, tu.user_name, tu.user_image, tc.template_type, tc.class_type, tc.channel_id, DATE_ADD(tc.start_time, INTERVAL $interval minute) as start_time, DATE_ADD(tc.finish_time, INTERVAL $interval minute) as finish_time, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tu.id_user=tc.tutor_id INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id) ON tc.subject_id=ms.subject_id WHERE tc.tutor_id='$ids' AND ( ( start_time>='$thisBeginDay' AND start_time<'$thisEndDay' AND finish_time<='$oneDayAfter' ) || ( start_time>='$oneDayBefore' AND finish_time>'$thisBeginDay' AND finish_time<='$thisEndDay' ) )  ORDER BY start_time ASC")->result_array();
			$res = $this->db->query("SELECT ms.*, tc.class_id, tc.participant,tc.subject_id, tc.name as subject_name, tc.description, tc.tutor_id, tu.user_name, tu.user_image, tc.template_type, tc.class_type, tc.channel_id, DATE_ADD(tc.start_time, INTERVAL $interval minute) as start_time, DATE_ADD(tc.finish_time, INTERVAL $interval minute) as finish_time, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tu.id_user=tc.tutor_id INNER JOIN master_subject as ms ON tc.subject_id=ms.subject_id WHERE tc.tutor_id='$ids' AND ( ( start_time>='$thisBeginDay' AND start_time<'$thisEndDay' AND finish_time<='$oneDayAfter' ) || ( start_time>='$oneDayBefore' AND finish_time>'$thisBeginDay' AND finish_time<='$thisEndDay' ) )  ORDER BY start_time ASC")->result_array();

			if(!empty($res)){
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				foreach ($res as $key => $value) {		
					$class_type 	= $value['class_type'];					
					$jenjangid 		= json_decode($value['jenjang_id'], TRUE);
					if ($jenjangid != NULL) {
						if(is_array($jenjangid)){
							foreach ($jenjangid as $keyy => $v) {								
								$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$v'")->row_array();
								$res[$key]['jenjang_name'] = $getAllSubject['jenjang_name'];
								$res[$key]['jenjang_level'] = $getAllSubject['jenjang_level'];												
							}
						}
						else
						{
							$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$jenjangid'")->row_array();
							$res[$key]['jenjang_name'] = $getAllSubject['jenjang_name'];
							$res[$key]['jenjang_level'] = $getAllSubject['jenjang_level'];				
						}
					}			
					$decode = json_decode($value['participant'],true);
					if(isset($decode['participant'])){
						$length = count($decode['participant']);
						$res[$key]['participant_count'] = $length;
					}
					else
					{
						$res[$key]['participant_count'] = 0;
					}
					
					if ($class_type == 'private') {
						$participant  = json_decode($value['participant'],true);
						foreach ($participant['participant'] as $keya => $aa) {
							$iduser = $aa['id_user'];
							$getDataUser 	= 	$this->db->query("SELECT user_name, user_image, usertype_id FROM tbl_user WHERE id_user='$iduser'")->row_array();
							$res[$key]['user_image'] = $getDataUser['user_image'];
							if ($getDataUser['usertype_id'] == 'student kid') {
								$getJenjang 	= $this->db->query("SELECT mj.jenjang_name, mj.jenjang_level FROM tbl_profile_kid as tpk INNER JOIN master_jenjang as mj ON tpk.jenjang_id=mj.jenjang_id WHERE tpk.kid_id='$iduser'")->row_array();
								$jjname 		= $getJenjang['jenjang_name'];
								$jjlevel 		= $getJenjang['jenjang_level'];
							}
							else
							{
								$getJenjang 	= $this->db->query("SELECT mj.jenjang_name, mj.jenjang_level FROM tbl_profile_student as tps INNER JOIN master_jenjang as mj ON tps.jenjang_id=mj.jenjang_id WHERE tps.student_id='$iduser'")->row_array();
								$jjname 		= $getJenjang['jenjang_name'];
								$jjlevel 		= $getJenjang['jenjang_level'];
							}
							$res[$key]['jenjang_name'] 	= $jjname;
							$res[$key]['jenjang_level'] = $jjlevel;
							$res[$key]['usertype_id'] 	= $getDataUser['usertype_id'];
						}
					}
					else if($class_type == 'multicast' || $class_type == 'multicast_channel_paid'){
						$countJenjang = count($jenjangid);
						if ($countJenjang >= 1) {
							$res[$key]['jenjang_name'] 	= $this->lang->line('multigrade');
							$res[$key]['jenjang_level'] = 0;
						}
					}

					$res[$key]['participant'] = json_decode($value['participant'],true);					
				}
				$rets['data'] = $res;
				$this->response($rets);
			}else{
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete".$this->post('id_user');
		ending:
		$this->response($rets);
	}

	public function followtutor_post()
	{
		
		if($this->post('subject_id') != '' && $this->post('tutor_id') != '' && $this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
			if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
				$rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
				$rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
				goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
			}


			$ids = $this->post('id_user'); // TULISAN ASLI
			// $ids = $retok['id_user']; // INI YANG GUA BIKIN BARU, JADI INFORMASI USER (TERMASUK ID USER) ADA DI VARIABLE $retok

			$isb = $this->post('subject_id');
			$idt = $this->post('tutor_id');

			$res = $this->db->simple_query("INSERT INTO tbl_booking (id_user, subject_id, tutor_id) VALUES('$ids','$isb','$idt')");
			if($res){
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = "Successfully selecting tutor and subjects";
				$this->response($rets);
			}else{
				$rets['status'] = false;
				$rets['message'] = "Failed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete ".$this->post('id_user');
		ending:
		$this->response($rets);
	}

	public function tutor_desc_get()
	{
		$jenjang_id = $this->get('jenjang_id');
		$tutor_id = $this->get('tutor_id');
		$id_student = $this->get('id_user');
		$year_experience = 0;
		$last_education = null;
		$last_name = null;
		$edu_score = 0;
		if($tutor_id != '' && $this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

			$data  = $this->db->query("SELECT * FROM tbl_user as tu LEFT JOIN tbl_profile_tutor as tpt ON tu.id_user=tpt.tutor_id WHERE tu.id_user='".$tutor_id."' AND tu.usertype_id='tutor'")->row_array();
			$subcribe = $this->db->query("SELECT count(tutor_id) as follower FROM tbl_booking WHERE tutor_id = '".$tutor_id."'")->row_array();
			$class = $this->db->query("SELECT count(class_id) as classall FROM `tbl_class` WHERE tutor_id = '".$tutor_id."'")->row_array();
			if(!empty($data)){
				if($data['education_background'] != ''){
					$data['education_background'] = unserialize($data['education_background']);
					$deb = $data['education_background'];
					foreach ($deb as $key => $value) {
						$a = $this->db->query("SELECT jenjanglevel_score FROM master_jenjanglevel WHERE jenjanglevel_name='".$value['educational_level']."'")->row_array();
						if(!empty($a)){
							$a = $a['jenjanglevel_score'];
						}else{
							$a = 0;
						}
						if($a > $edu_score){
							$edu_score = $a;
							$last_education = $value['educational_level'];
							$last_name = $value['educational_institution'];
						}
					}
					if($edu_score >=4 ){
						$data['last_education'] = $last_education.' - '.$last_name;
					}else{
						$data['last_education'] = null;
					}
				}
				if($data['teaching_experience'] != ''){
					$data['teaching_experience'] = unserialize($data['teaching_experience']);
					$dte = $data['teaching_experience'];
					foreach ($dte as $key => $value) {
						$a = $value['year_experience2']-$value['year_experience1'];
						$year_experience+=$a;
					}
				}
				$data['year_experience'] = $year_experience;
				// $competency = $this->db->query("SELECT tb.id_user, tb.subject_id ,ms.*, mj.jenjang_name, mj.jenjang_level, mj.jenjang_id FROM `tbl_booking` as tb INNER JOIN (master_subject as ms INNER JOIN master_jenjang as  mj ON ms.jenjang_id=mj.jenjang_id) ON tb.subject_id=ms.subject_id WHERE tb.id_user=".$tutor_id." ORDER BY ms.subject_id")->result_array();
				$competency	   = $this->db->query("SELECT * FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id=ms.subject_id WHERE tb.id_user='$tutor_id' AND (ms.jenjang_id LIKE '%\"$jenjang_id\"%' OR ms.jenjang_id='$jenjang_id')")->result_array();
				$data['competency'] = [];
				foreach ($competency as $key => $value) {
					$jenjang_name = array();
					$jenjang_level = array();
					$jenjangid = json_decode($value['jenjang_id'], TRUE);
					if ($jenjangid != NULL) {
						if(is_array($jenjangid)){
							foreach ($jenjangid as $keye => $v) {								
								if ($jenjang_id==$v) {
									$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$v'")->row_array();	
									$jenjang_name[] = $getAllSubject['jenjang_name'];	
									$jenjang_level[] = $getAllSubject['jenjang_level'];
								}
																															
								$data['competency'][$key]['jenjang_name'] = implode(", ", $jenjang_name);
								$data['competency'][$key]['jenjang_level'] = implode(", ", $jenjang_level);
							}
						}
						else
						{
							$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$jenjangid'")->row_array();
							$jenjang_name[] = $getAllSubject['jenjang_name'];	
							$jenjang_level[] = $getAllSubject['jenjang_level'];
							$data['competency'][$key]['jenjang_name'] = implode(", ", $jenjang_name);
							$data['competency'][$key]['jenjang_level'] = implode(", ", $jenjang_level);		
						}
					}

					$asd = $this->db->query("SELECT * from tbl_booking where subject_id =".$value['subject_id']." AND tutor_id =".$tutor_id." AND id_user = ".$id_student."")->row_array();
					if (!empty($asd)) {
						$data['competency'][$key]['follow'] = 1;
					} else {

						$data['competency'][$key]['follow'] = 0;
					}
					$data['competency'][$key]['subject_id'] = $value['subject_id'];
					$data['competency'][$key]['subject_name'] = $value['subject_name'];
					$data['competency'][$key]['jenjang_id'] = $value['jenjang_id'];					
					$data['competency'][$key]['id_lang'] = $value['indonesia'];
					$data['competency'][$key]['en_lang'] = $value['english'];
				}
				$data['follower'] = $subcribe['follower'];
				$data['allclass'] = $class['classall'];

				$rets['status'] = true;
				$rets['message'] = "Tutor found";
				$rets['data'] = $data;
				$this->response($rets); 
			} else { 
				$rets['status'] = true;
				$rets['message'] = "Tutor not found";
				$rets['data'] = null;
				$this->response($rets);
			}

		} 
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);

	}

	public function tutordescription_get()
	{
		$tutor_id = $this->get('tutor_id');
		$year_experience = 0;
		$last_education = null;
		$edu_score = 0;
		if($tutor_id != '' && $this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			// $data  = $this->db->query("SELECT * FROM tbl_user as tu LEFT JOIN tbl_profile_tutor as tpt ON tu.id_user=tpt.tutor_id WHERE tu.id_user='".$tutor_id."' AND tu.usertype_id='tutor'")->row_array();
			$data = $this->db->query("SELECT tu.*, tpt.*, mprov.provinsi_name, mkab.kabupaten_name, mkec.kecamatan_name , mdes.desa_name FROM tbl_user as tu LEFT JOIN tbl_profile_tutor as tpt ON tu.id_user=tpt.tutor_id LEFT JOIN  master_provinsi as mprov  ON tu.id_provinsi=mprov.provinsi_id LEFT JOIN master_kabupaten as mkab ON tu.id_kabupaten=mkab.kabupaten_id LEFT JOIN master_kecamatan as mkec ON tu.id_kecamatan=mkec.kecamatan_id LEFT JOIN master_desa as mdes ON tu.id_desa=mdes.desa_id  WHERE tu.id_user='$tutor_id' AND tu.usertype_id='tutor'")->row_array();
			if(!empty($data)){
				$data['user_image'] = $data['user_image'];
				if($data['education_background'] != ''){
					$data['education_background'] = unserialize($data['education_background']);
					$deb = $data['education_background'];
					foreach ($deb as $key => $value) {
						$a = $this->db->query("SELECT jenjanglevel_score FROM master_jenjanglevel WHERE jenjanglevel_name='".$value['educational_level']."'")->row_array();
						if(!empty($a)){
							$a = $a['jenjanglevel_score'];
						}else{
							$a = 0;
						}
						if($a > $edu_score){
							$edu_score = $a;
							$last_education = $value['educational_level'];
						}
					}
					if($edu_score >=4 ){
						$data['last_education'] = $last_education;
					}else{
						$data['last_education'] = null;
					}
				}else{
					$data['education_background'] = array();
				}
				if($data['teaching_experience'] != ''){
					$data['teaching_experience'] = unserialize($data['teaching_experience']);
					$dte = $data['teaching_experience'];
					foreach ($dte as $key => $value) {
						$a = $value['year_experience2']-$value['year_experience1'];
						$year_experience+=$a;
					}
				}else{
					$data['teaching_experience'] = array();
				}
				$data['year_experience'] = $year_experience;
				$competency = $this->db->query("SELECT tb.id_user, tb.subject_id ,ms.*, mj.jenjang_name, mj.jenjang_level FROM `tbl_booking` as tb INNER JOIN (master_subject as ms INNER JOIN master_jenjang as  mj ON ms.jenjang_id=mj.jenjang_id) ON tb.subject_id=ms.subject_id WHERE tb.id_user=".$tutor_id." ORDER BY ms.subject_id")->result_array();
				$data['competency'] = [];
				foreach ($competency as $key => $value) {
					$data['competency'][$key]['subject_name'] = $value['subject_name'];
					$data['competency'][$key]['jenjang_name'] = $value['jenjang_name'];
					$data['competency'][$key]['jenjang_level'] = $value['jenjang_level'];
					$data['competency'][$key]['id_lang'] = $value['indonesia'];
					$data['competency'][$key]['en_lang'] = $value['english'];
				}
				$datas = array();
				$datas = [$data];
				$rets['status'] = true;
				$rets['message'] = "Tutor found";
				$rets['data'] = $datas;
				$this->response($rets); 
			} else { 
				$rets['status'] = true;
				$rets['message'] = "Tutor not found";
				$rets['data'] = null;
				$this->response($rets);
			}

		} 
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);

	}
	public function student_updateProfile_post(){
		// $id_user = $this->db->str_escape($this->post('id_user'));
		// $user_birthdate = $this->db->str_escape($this->post('user_birthdate'));
		// $user_birthplace = $this->db->str_escape($this->post('user_birthplace'));
		// $user_gender = $this->db->str_escape($this->post('user_gender'));
		// $user_religion = $this->db->str_escape($this->post('user_religion'));
		// $user_address = $this->db->str_escape($this->post('user_address'));

		$id_user = $this->post('id_user');
		$user_birthdate = $this->post('user_birthdate');
		$user_birthplace = $this->post('user_birthplace');
		$user_gender = $this->post('user_gender');
		$user_religion = $this->post('user_religion');
		$user_address = $this->post('user_address');
		$user_first = $this->post('first_name');
		$user_last = $this->post('last_name');
		$nama_negara	= $this->input->post('nama_negara');
		$countrycode 	= $this->input->post('countrycode');
		$countrycode    = str_replace(" ","+",$countrycode);
		$callnum 		= $this->input->post('callnum');

		$school_id		= $this->post('school_id');
		$jenjang_id		= $this->post('jenjang_id');

		$img = $this->post('user_image');
		$nameimg = $id_user+'.jpg';

		$umur = $this->post('user_birthdate');
		$skrg = date_diff(date_create($umur), date_create('today'))->y;

		if($id_user !== '' && $this->get('access_token') != ''){

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			$updates = $this->db->simple_query("UPDATE tbl_user SET user_name ='{$user_first} {$user_last}', first_name = '{$user_first}', last_name = '{$user_last}', user_age='{$skrg}',user_birthplace='{$user_birthplace}', user_birthdate='{$user_birthdate}', user_gender='{$user_gender}', user_religion='{$user_religion}', user_address='{$user_address}', user_nationality = '{$nama_negara}', user_countrycode = '{$countrycode}', user_callnum = '{$callnum}' WHERE id_user='{$id_user}'");
			$this->db->simple_query("UPDATE tbl_profile_student set school_id='$school_id', jenjang_id = $jenjang_id WHERE student_id = '$id_user'");
	        
			if($updates){
				// $image = 'data:image/jpeg;base64,' .str_replace(' ','+',$img); //replacing ' ' with '+'				
				// //Write to disk
				// file_put_contents("https://c1.classmiles.com/classmiles/aset/img/user/".$nameimg,file_get_contents($image));
				$rets['status'] = true;
				$rets['message'] = "Successfully updated.";
				$this->response($rets);
			}else{
				$rets['status'] = false;
				$rets['message'] = "updating failed.";
				$this->response($rets);
			}
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function test_get($session_key='')
	{
		// echo $this->DB1;
		/*$send['janus'] = "add_token";
			$send['token'] = sha1($session_key);
			$send['transaction'] = $this->Rumus->RandomString();
			$send['admin_secret'] = "admin556677";
		$ari = json_encode($send);
		// print_r($ari);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://".$this->janus_server.":7889/admin");
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$ari);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,4000); 

		$curl_rets = curl_exec($ch);
		// if(curl_errno($ch)){
    		// echo 'Curl error: ' . curl_error($ch);
		// }

		$this->response($curl_rets);*/
	}

	public function enter_room_post() {

		$iduser = $this->post('id_user');
		$value = $this->post('email').time();
		$class_id = $this->post('room');

		$sha = sha1($value);

		if($iduser != '' && $value != '' && $class_id != '' && $this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

			$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$iduser'")->result_array();
			if(!empty($student)){

				$rets['data'] = $student;
				$rets['status'] = true;
				$rets['message'] = "Data found";
				$this->response($rets);

			}else{
					// $hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
					// echo $hash_id;
				$save = $this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$sha','$class_id','$iduser','TRUE',0)");
				if ($save) {

					$student2 = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$iduser'")->result_array();

					if(!empty($student2)){

						$rets['data'] = $student2;
						$rets['status'] = true;
						$rets['message'] = "Data found";
						$this->response($rets);

					}

				} else {

					$rets['data'] = $student;
					$rets['status'] = false;
					$rets['message'] = "Failed to save."."INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$sha','$class_id','$iduser','TRUE',0)";
					$this->response($rets);

				}


			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function subjecttutor_post()
	{
		if($this->post('id_user') != ''){
			$ids = $this->post('id_user');
			$res = $this->db->query("SELECT * FROM master_subject ORDER BY subject_id ASC")->result_array();

			if(!empty($res)){
				foreach ($res as $key => $value) {
					$booked = $this->db->query("SELECT id_user as booked FROM tbl_booking WHERE subject_id='".$value['subject_id']."' AND id_user='".$ids."' ")->row_array();
					if($booked['booked'] == NULL){
						$res[$key]['booked'] = 0;
					}else{
						$res[$key]['booked'] = 1;
					}

					$jenjangname = array();
					$jenjanglevel = array();
					$jenjangid = json_decode($value['jenjang_id'], TRUE);
					if ($jenjangid != NULL) {
						if(is_array($jenjangid)){
							foreach ($jenjangid as $keyy => $valuee) {									
								$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$valuee'")->row_array();	
								$jenjangname[] = $selectnew['jenjang_name']; 
								$jenjanglevel[] = $selectnew['jenjang_level']; 
							}	
						}else{
							$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();	
							$jenjangname[] = $selectnew['jenjang_name']; 
							$jenjanglevel[] = $selectnew['jenjang_level']; 
						}										
					}
					$res[$key]['jenjang_name'] = implode(", ",$jenjangname);
					$res[$key]['jenjang_level'] = implode(", ",$jenjanglevel);
				}
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = $res;
				$this->response($rets);
			}else{
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = null;
				$this->response($rets);
			}
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		$this->response($rets);
	}

	// public function subjecttutor_post()
	// {
	// 	if($this->post('id_user') != ''){
	// 		$ids = $this->post('id_user');
	// 		$res = $this->db->query("SELECT ms.subject_id, ms.jenjang_id, ms.icon, tb.id_user as booked, ms.subject_name, ms.indonesia, ms.english, mj. * FROM master_subject AS ms  INNER JOIN master_jenjang AS mj ON ms.jenjang_id = ms.jenjang_id LEFT JOIN tbl_booking as tb ON ms.subject_id=tb.subject_id AND tb.id_user='{$ids}' WHERE mj.jenjang_id = ms.jenjang_id")->result_array();
	// 		if(!empty($res)){
	// 			foreach ($res as $key => $value) {
	// 				if($value['booked'] == NULL){
	// 					$res[$key]['booked'] = 0;
	// 				}else{
	// 					$res[$key]['booked'] = 1;
	// 				}
	// 			}
	// 			$rets['status'] = true;
	// 			$rets['message'] = "Succeed";
	// 			$rets['data'] = $res;
	// 			$this->response($rets);
	// 		}else{
	// 			$rets['status'] = true;
	// 			$rets['message'] = "Succeed";
	// 			$rets['data'] = null;
	// 			$this->response($rets);
	// 		}
	// 	}
		
	// 	$rets['status'] = false;
	// 	$rets['message'] = "Field incomplete";
	// 	$this->response($rets);
	// }

	public function subdiscussion_post()
	{
		$now = date('N');
		$nextMon = date('Y-m-d', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d')))); 
		$start_date = date('Y-m-d', strtotime('+7 day', strtotime($nextMon)));
		if($this->post('id_user') != ''){
			$tutor_id = $this->post('id_user');

			$data = $this->db->query("SELECT tb.*, ms.*, mj.jenjang_name, mj.jenjang_level FROM tbl_booking as tb INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id)  ON tb.subject_id=ms.subject_id WHERE id_user='{$tutor_id}'")->result_array();
			$result = array();
			foreach ($data as $key => $value) {
				$sid = $value['subject_id'];
				$subd = $this->db->query("SELECT * FROM log_subdiscussion WHERE tutor_id='{$tutor_id}' AND subject_id='{$sid}' AND fdate_week='{$start_date}'")->row_array();
				$result[$key]['subject_id'] = $sid;
				$result[$key]['subject_name'] = $value['subject_name']." - ".$value['jenjang_level']." ".$value['jenjang_name'];
				$result[$key]['icon'] = $value['icon'];
				$result[$key]['jenjang_level'] = $value['jenjang_level'];
				$result[$key]['jenjang_name'] = $value['jenjang_name'];
				$result[$key]['indonesia'] = $value['indonesia'];
				$result[$key]['english'] = $value['english'];
					
				$result[$key]['date'] = $start_date;
				if(!empty($subd)){
					$result[$key]['isi'] = $subd['subdiscussion'];	
				}else{
					$result[$key]['isi'] = "";	
				}
			}
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$rets['data'] = $result;
				$this->response($rets);
		}
		
		$rets['status'] = false;
		$rets['message'] = "Field incomplete".$this->post('id_user');
		$this->response($rets);
	}

	public function savesub_post() 
	{
		if($this->post('id_user') != '')
		{
			$tutor_id = $this->post('id_user');
			$subject_id = $this->input->post('subject_id');
			$subdiscussion = $this->input->post('subdiscussion');
			$fdate = $this->input->post('fdate');

			$data = $this->db->query("SELECT * FROM log_subdiscussion WHERE subject_id='{$subject_id}' AND tutor_id='{$tutor_id}' AND fdate_week='{$fdate}'")->row_array();
			if(!empty($data)){
				$sid = $data['subdiscussion_id'];
				$this->db->simple_query("UPDATE log_subdiscussion SET subdiscussion='".$subdiscussion."' WHERE subdiscussion_id='{$sid}'");
			}else{
				$this->db->simple_query("INSERT INTO log_subdiscussion(subject_id, tutor_id, fdate_week, subdiscussion) VALUES('{$subject_id}','{$tutor_id}','{$fdate}','".$subdiscussion."')");
			}
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		$this->response($rets);
	}

	public function tutorextraclass_post()
	{
		
		if($this->post('id_user') != ''&& $this->get('access_token') != ''){

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

			$id_user = $this->post('id_user');
			$user_utc = $this->post('user_utc');
			$server_utc = $this->Rumus->getGMTOffset();
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
			$interval   = $user_utc - $server_utc;

			$data = $this->db->query("SELECT trg.*, DATE_ADD(trg.date_requested, INTERVAL $interval MINUTE) as valid_date_requested, ms.subject_name, tu.user_name, tu.user_image, mj.*, mjk.jenjang_id as jenjang_idk, mjk.jenjang_name as jenjang_namek, mjk.jenjang_level as jenjang_levelk FROM tbl_request_grup as trg LEFT JOIN (tbl_user as tu LEFT JOIN (tbl_profile_student as tps LEFT JOIN master_jenjang as mj ON tps.jenjang_id=mj.jenjang_id) ON tu.id_user=tps.student_id LEFT JOIN (tbl_profile_kid as tpk LEFT JOIN master_jenjang as mjk ON tpk.jenjang_id=mjk.jenjang_id) ON tpk.kid_id=tu.id_user) ON trg.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON trg.subject_id=ms.subject_id WHERE trg.tutor_id='{$id_user}' AND trg.approve=0 AND trg.id_friends NOT LIKE '%\"status\":0%' ORDER BY trg.request_id DESC")->result_array();

			// $data = $this->db->query("SELECT tr.*, DATE_ADD(tr.date_requested, INTERVAL $interval MINUTE) as valid_date_requested, ms.subject_name, tu.user_name,mj.jenjang_name,mj.jenjang_level, tu.user_image FROM tbl_request_grup as tr INNER JOIN (tbl_user as tu INNER JOIN (tbl_profile_student as tps INNER JOIN master_jenjang as mj ON tps.jenjang_id=mj.jenjang_id) ON tu.id_user=tps.student_id) ON tr.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON tr.subject_id=ms.subject_id  WHERE tr.tutor_id='{$id_user}' AND tr.approve=0 AND tr.id_friends NOT LIKE '%\"status\":0%'")->result_array();

			// $datas = $this->db->query("SELECT tr.*, DATE_ADD(tr.date_requested, INTERVAL $interval MINUTE) as valid_date_requested, ms.subject_name, tu.user_name, tu.user_image  FROM tbl_request as tr LEFT JOIN (tbl_user as tu LEFT JOIN (tbl_profile_student as tps LEFT JOIN master_jenjang as mj ON tps.jenjang_id=mj.jenjang_id) ON tu.id_user=tps.student_id LEFT JOIN tbl_profile_kid as tpk ON tpk.kid_id=tu.id_user) ON tr.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON tr.subject_id=ms.subject_id WHERE tr.tutor_id='{$id_user}' AND tr.approve=0 ORDER BY tr.request_id DESC")->result_array();

			$datas = $this->db->query("SELECT tr.*, DATE_ADD(tr.date_requested, INTERVAL $interval MINUTE) as valid_date_requested, ms.subject_name, tu.user_name, tu.user_image, mj.*, mjk.jenjang_id as jenjang_idk, mjk.jenjang_name as jenjang_namek, mjk.jenjang_level as jenjang_levelk FROM tbl_request as tr LEFT JOIN (tbl_user as tu LEFT JOIN (tbl_profile_student as tps LEFT JOIN master_jenjang as mj ON tps.jenjang_id=mj.jenjang_id) ON tu.id_user=tps.student_id LEFT JOIN (tbl_profile_kid as tpk LEFT JOIN master_jenjang as mjk ON tpk.jenjang_id=mjk.jenjang_id) ON tpk.kid_id=tu.id_user) ON tr.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON tr.subject_id=ms.subject_id WHERE tr.tutor_id='{$id_user}' AND tr.approve=0 ORDER BY tr.request_id DESC")->result_array();

			// $datas = $this->db->query("SELECT tr.*, DATE_ADD(tr.date_requested, INTERVAL $interval MINUTE) as valid_date_requested, ms.subject_name, tu.user_name,mj.jenjang_name,mj.jenjang_level, tu.user_image FROM tbl_request as tr INNER JOIN (tbl_user as tu INNER JOIN (tbl_profile_student as tps INNER JOIN master_jenjang as mj ON tps.jenjang_id=mj.jenjang_id) ON tu.id_user=tps.student_id) ON tr.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON tr.subject_id=ms.subject_id  WHERE tr.tutor_id='{$id_user}' AND tr.approve=0")->result_array();
			foreach ($data as $key => $v) {
				$data[$key]['id_friends'] = json_decode($v['id_friends'],true);
				$subjectid = $v['subject_id'];
				$tutorid = $v['tutor_id'];
				$getjenjang = $this->db->query("SELECT ms.subject_name, mj.jenjang_level, mj.jenjang_name FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.subject_id='$subjectid'")->row_array();

				$getprice = $this->db->query("SELECT harga FROM tbl_service_price WHERE class_type='group' AND tutor_id='$tutorid' AND subject_id='$subjectid'")->row_array();
				$hargakahir = ($v['duration_requested']/900)*$getprice['harga'];
			    // $data[$key]['subject_name'] = $getjenjang['subject_name'];
			    // $data[$key]['jenjang_level'] = $getjenjang['jenjang_level'];
			    // $data[$key]['jenjang_name'] = $getjenjang['jenjang_name'];
			    $data[$key]['harga'] = $hargakahir;
			}

			foreach ($datas as $key => $v) {
				$tutorid = $v['tutor_id'];
				$sujectid = $v['subject_id'];
				$getjenjang = $this->db->query("SELECT ms.subject_name, mj.jenjang_level, mj.jenjang_name FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.subject_id='$sujectid'")->row_array();

				$priceprivate = $this->db->query("SELECT harga FROM tbl_service_price WHERE tutor_id='$tutorid' AND class_type='private' AND subject_id='$sujectid'")->row_array()['harga'];
				
			    // $datas[$key]['subject_name'] = $getjenjang['subject_name'];
			    // $datas[$key]['jenjang_level'] = $getjenjang['jenjang_level'];
			    // $datas[$key]['jenjang_name'] = $getjenjang['jenjang_name'];
			    $datas[$key]['harga'] = $priceprivate;
			}
			$rets['status'] = true;
			$rets['message'] = "Succeed";
			$rets['data_group'] = $data;
			$rets['data'] = $datas;
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function guadestroytoken_post()
	{
		$send = array();
		$send['janus'] = "remove_token";
		$send['transaction'] = $this->Rumus->RandomString();

		$send['admin_secret'] = "admin556677";
		$send['token'] = $this->post('token');
		$ari = json_encode($send);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://".$this->janus_server.":7889/admin");
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$ari);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_TIMEOUT_MS,4000); 

		$curl_rets = curl_exec($ch);

		$rets['status'] = true;
		$rets['message'] = "Sukses bro";
		$this->response($rets);
	}

	public function get_chat_post()
	{
		$rets = array();
		$dateawal = $this->post('dateawal');
		$dateakhir = $this->post('dateakhir');
		$class_id = $this->post('class_id');

		$dataget = $this->db->query("SELECT * FROM rec_chat WHERE (DATETIME >= '$dateawal' AND DATETIME <= '$dateakhir') AND class_id = '$class_id' ORDER BY datetime ASC")->result_array();

		$result = array();
		foreach ($dataget as $key => $value) {			
			$result[$key]['chat_id'] = $value['chat_id'];
			$result[$key]['class_id'] = $value['class_id'];
			$result[$key]['text'] = $value['text'];
			$result[$key]['sender_name'] = $value['sender_name'];
			$result[$key]['status'] = $value['status'];
			$result[$key]['datetime'] = $value['datetime'];							
		}

		$rets['status'] = true;
		$rets['message'] = "Succeed";
		$rets['method'] = "GET CHAT";
		$rets['data'] = $result;
		$this->response($rets);
	}

	public function get_announcement_post()
	{
		$rets = array();
		$id_user = $this->post('id_user');
		$date = date("Y-m-d");
		if ($id_user != "" && $this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                // $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

			$dataget = $this->db->query("SELECT * FROM tbl_notif WHERE id_user = '$id_user' and timestamp like '$date%' ORDER BY notif_id DESC limit 1 ")->row_array();
			$datagets = $this->db->query("SELECT text_announ FROM tbl_announcement ORDER BY id DESC limit 1 ")->row_array();
			$datagetz = $this->db->query("SELECT * FROM tbl_version_app where name = 'student' ")->row_array();

			$text = "";
			if (!empty($dataget)) {
				$text = substr($dataget['notif'],0,1);
				if ($text == "{") {
					$text = "";
				} else {
					$text = $dataget['notif'];
				}
				$dataget['timestamp'] = substr($dataget['timestamp'],0,10);
			} else {
				$dataget['timestamp'] = "";
			}
			$dataget['notif'] = $text;
			$dataget['text_announ'] = $datagets['text_announ'];
			$dataget['version'] = $datagetz['version'];
			$dataget['force'] = $datagetz['force'];

			$rets['status'] = true;
			$rets['message'] = "Succeed";
			$rets['method'] = "GET ANNOUNCEMENT";
			$rets['data'] = $dataget;
			$this->response($rets);	
		} 

			$datagetz = $this->db->query("SELECT * FROM tbl_version_app where name = 'student' ")->row_array();
			$dataget['version'] = $datagetz['version'];
			$dataget['force'] = $datagetz['force'];
			
			$rets['status'] = false;
			$rets['message'] = "Field incomplete";
			$rets['method'] = "GET ANNOUNCEMENT";
			$rets['data'] = $dataget;
			ending:
			$this->response($rets);	
		
	}

	public function get_announcementtutor_post()
	{
		$rets = array();
		$id_user = $this->post('id_user');
		$date = date("Y-m-d");
		if ($id_user != "" && $this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
		
			$dataget = $this->db->query("SELECT * FROM tbl_notif WHERE id_user = '$id_user' and timestamp like '$date%' ORDER BY notif_id DESC limit 1 ")->row_array();
			$datagets = $this->db->query("SELECT text_announ FROM tbl_announcement ORDER BY id DESC limit 1 ")->row_array();
			$datagetz = $this->db->query("SELECT * FROM tbl_version_app where name ='tutor'")->row_array();

			$text = "";
			if (!empty($dataget)) {
				$text = substr($dataget['notif'],0,1);
				if ($text == "{") {
					$text = "";
				} else {
					$text = $dataget['notif'];
				}
				$dataget['timestamp'] = substr($dataget['timestamp'],0,10);
			} else {
				$dataget['timestamp'] = "";
			}
			$dataget['notif'] = $text;
			$dataget['text_announ'] = $datagets['text_announ'];
			$dataget['version'] = $datagetz['version'];
			$dataget['force'] = $datagetz['force'];

			$rets['status'] = true;
			$rets['message'] = "Succeed";
			$rets['method'] = "GET ANNOUNCEMENT";
			$rets['data'] = $dataget;
			$rets['asd'] = $dataget;
			$this->response($rets);	
		} else {
			$datagetz = $this->db->query("SELECT * FROM tbl_version_app where name = 'tutor' ")->row_array();
			$dataget['version'] = $datagetz['version'];
			$dataget['force'] = $datagetz['force'];
			
			$rets['status'] = false;
			$rets['message'] = "Field incomplete";
			$rets['method'] = "GET ANNOUNCEMENT";
			$rets['data'] = $dataget;
			ending:
			$this->response($rets);	
		}
	}

	public function up_tour_post()
	{
		$sttour = $this->post('st_tour');
		$iduser = $this->post('iduser');
		$res['method'] = "up_tour";		

		$dbres = $this->db->simple_query("UPDATE tbl_user SET st_tour='1' WHERE id_user='$iduser'");

		if ($dbres) {
			$res['status'] = true;
			$res['message'] = 'sukses';
			$res['code'] = '200';	
		}		

		$this->response($res);
	}

	public function getnameuser_post()
	{
		$rets = array();		

		if ($this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			$dataget = $this->db->query("SELECT id_user,user_name,email FROM tbl_user WHERE usertype_id='student'")->result_array();

			$result = array();
			foreach ($dataget as $key => $value) {			
				$result[$key]['id_user'] = $value['id_user'];			
				$result[$key]['user_name'] = $value['user_name'];
				$result[$key]['email'] = $value['email'];
			}

			$rets['status'] = true;
			$rets['message'] = "Succeed";
			$rets['method'] = "GET NAME";
			$rets['response'] = $result;
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}

	public function ceksaldo_post()
	{
		$rets = array();
		$iduser = $this->post('id_user');		
	    
	    $getbalance = $this->db->query("SELECT active_balance FROM uangsaku WHERE id_user='$iduser'")->row_array();
	    // $hasil = number_format($getbalance['active_balance'], 0, ".", ".");
	    if (empty($getbalance)) {
	    	$rets['status'] = false;
			$rets['message'] = "failed";		
			$rets['balance'] = 0;
			$this->response($rets);
	    }
		$result = array();
		$result = $getbalance['active_balance'];

		$rets['status'] = true;
		$rets['message'] = "Succeed";		
		$rets['balance'] = $result;
		$this->response($rets);
	}	
	public function isEnoughBalance_post()
	{
		$rets = array();
		$iduser = $this->post('id_user');
		$gh = $this->post('gh');
		$base64 = strtr($gh, '-_.', '+/=');
		$gh = $this->encryption->decrypt($base64);
	   	if ($iduser != '' && $this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

	    	$getbalance = $this->db->query("SELECT active_balance FROM uangsaku WHERE id_user='$iduser'")->row_array();
		    // $hasil = number_format($getbalance['active_balance'], 0, ".", ".");
		    if (empty($getbalance)) {
		    	$rets['status'] = false;
				$rets['message'] = "not enough balance";
				$this->response($rets);
		    }
			$balance = $getbalance['active_balance'];
			if($balance >= $gh){
				$rets['status'] = true;
				$rets['message'] = "enough balance";
				$this->response($rets);
			}else{
				$rets['status'] = false;
				$rets['message'] = "not enough balance";
				$this->response($rets);
			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		ending:
		$this->response($rets);
	}

	public function requestprivate_get() {

		$rets = array();

		$subject_id = $this->get('subject_id');
		$tutor_id = $this->get('tutor_id');
		$id_user = $this->get('id_user');
		$start_time = $this->get('start_time');
		$date = $this->get('date');
		$user_utc = $this->get('user_utc');
		$duration = $this->get('duration');
		$topic = $this->get('topic');
		$topic = $this->db->escape_str($topic);

		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		if ($this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

			$fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm WHERE id_user={$tutor_id}")->row_array();
			if(!empty($fcm_token)){
				$fcm_token = $fcm_token['fcm_token'];
			}

			$server_utc = $this->Rumus->getGMTOffset();
			$interval = $user_utc - $server_utc;
			$date_requested = DateTime::createFromFormat('Y-m-d H:i', $date." ".$start_time );
			$date_requested->modify("-".$interval ." minutes");

			$unix_requested = $date_requested->getTimestamp();
			$unix_now = time();
			if($unix_requested < ($unix_now + 3600) ){
				$rets['status'] = false;
				$rets['code'] = '402';
				$rets['message'] = "Maaf, Lewat Batas.";
				$this->response($rets);
			}

			$date_requested = $date_requested->format('Y-m-d H:i:s');

			$id_requester = $id_user;
			$student_name = $retok['user_name'];
			$request_id = $this->Rumus->get_new_request_id('private');
			$qr = $this->db->simple_query("INSERT INTO tbl_request(request_id,id_user_requester,tutor_id,subject_id,topic,date_requested,duration_requested,user_utc) VALUES('$request_id','{$id_requester}','{$tutor_id}','{$subject_id}','{$topic}','{$date_requested}','{$duration}','$user_utc')");
			
			$lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request')->row('request_id');
			$getdataclass 	= $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='private' AND tr.request_id='$lastrequestid'")->row_array();

            $emailtutor         = $this->db->query("SELECT email FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['email'];
            $nametutor          = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
			$iduserrequest		= $getdataclass['id_user_requester'];
			$namauser 			= $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
            $durasi             = $getdataclass['duration_requested'];
            $hargaakhir         = ($durasi/900)*$getdataclass['harga_cm'];
			$hargakelas         = number_format($hargaakhir, 0, ".", ".");
            $server_utcc        = $this->Rumus->getGMTOffset();
            $intervall          = $user_utc - $server_utcc;
            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclass['date_requested']);
            $tanggal->modify("+".$intervall ." minutes");
            $tanggal            = $tanggal->format('Y-m-d H:i:s');
            $datelimit          = date_create($tanggal);
            $dateelimit         = date_format($datelimit, 'd/m/y');
            $harilimit          = date_format($datelimit, 'd');
            $hari               = date_format($datelimit, 'l');
            $tahunlimit         = date_format($datelimit, 'Y');
            $waktulimit         = date_format($datelimit, 'H:i');   
            $datelimit          = $dateelimit;
            $sepparatorlimit    = '/';
            $partslimit         = explode($sepparatorlimit, $datelimit);
            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
            $seconds 			= $getdataclass['duration_requested'];
		    $hours 				= floor($seconds / 3600);
		    $mins 				= floor($seconds / 60 % 60);
		    $secs 				= floor($seconds % 60);
		    $durationrequested 	= sprintf('%02d:%02d', $hours, $mins);

            //LOG DEMAND
            $myip = $_SERVER['REMOTE_ADDR'];
            $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$lastrequestid','student request private class','$iduserrequest','private','$myip','101')");
            //END LOG DEMAND


			if($qr){
				// Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_requestprivate','content' => array("email" => $retok['email'], "namauser" => $namauser, "hargakelas" => $hargakelas, "getdataclass" => $getdataclass, "harilimit" => $harilimit, "hari" => $hari, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "nametutor" => $nametutor,  "durationrequested" => $durationrequested, "emailtutor" => $emailtutor  ) )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;

                //TAMBAH KERANJANG                    
                $this->db->query("INSERT INTO tbl_keranjang (request_id,id_user,price) VALUES ('$lastrequestid','$iduserrequest','$hargaakhir')");
                //END KERANJANG
	            
				//NOTIF STUDENT
				$notif_message = json_encode( array("code" => 115));					
				$this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$iduserrequest','','0')");

				$this->Rumus->notif_extra_class($tutor_id,$fcm_token,$namauser,$subject_id);
				/*$simpan_notif = $this->Rumus->create_notif($notif='Siswa '.$student_name.' telah melakukan request ke anda. Silahkan cek di persetujuan kelas tambahan.',$notif_mobile='Siswa '.$student_name.' telah melakukan request ke anda. Silahkan cek di persetujuan kelas tambahan.',$notif_type='single',$id_user=$tutor_id,$link='https://classmiles.com/tutor/approval_ondemand');*/
				
				$rets['status'] = true;
				$rets['message'] = "Request success";
				$this->response($rets);
			}else{
				$rets['status'] = false;
				$rets['message'] = "error occured";
				$this->response($rets);
			}
		}
		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}

	public function requestgroup_get() {
		$rets = array();

		$id_requester = $this->get('id_user');
		$tutor_id = $this->get('tutor_id');
		$subject_id = $this->get('subject_id');
		$start_time = $this->get('start_time');
		$date = $this->get('date');
		$user_utc = $this->get('user_utc');
		$duration = $this->get('duration');
		$student_name = $this->get('username');
		$metod = $this->get('metod');
		$id_friends = $this->get('id_friends');
		$topic = $this->get('topic');
		$topic = $this->db->escape_str($topic);

		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		if ($this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	        }

			$fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm WHERE id_user={$tutor_id}")->row_array();
			if(!empty($fcm_token)){
				$fcm_token = $fcm_token['fcm_token'];
			}

			$server_utc = $this->Rumus->getGMTOffset();
			$interval = $user_utc - $server_utc;
			$date_requested = DateTime::createFromFormat ('Y-m-d H:i', $date." ".$start_time );
			$date_requested->modify("-".$interval ." minutes");

			$unix_requested = $date_requested->getTimestamp();
			$unix_now = time();
			if($unix_requested < ($unix_now + 3600) ){
				$rets['status'] = false;
				$rets['code'] = '402';
				$rets['message'] = "Maaf, Lewat Batas.";
				$this->response($rets);
			}
			
			$date_requested = $date_requested->format('Y-m-d H:i:s');

			// $date = $data['date'];
			$participant['id_friends'] = [];
				
			$ids = explode(",", $id_friends);
			$idemails = explode(",", $id_friends);  
            $kotaktd = ""; 
			foreach($ids as $key => $id) {
				$participant['id_friends'][$key]['id_user'] = $id;
				$participant['id_friends'][$key]['status'] = 0;
			}
			array_push($participant['id_friends'], array('id_user' => $id_requester, 'status' => 1));

			$price = $this->db->query("SELECT harga_cm FROM tbl_service_price WHERE tutor_id='$tutor_id' AND subject_id='$subject_id' AND class_type='group'")->row_array()['harga_cm'];
			// $price = ($price/(count($participant['id_friends'])));
			$price = ($duration/900)*$price;

			$participant = json_encode($participant);
			// $idfriends = {$id_friends};      
			$request_id = $this->Rumus->get_new_request_id('group');
			$qr = $this->db->simple_query("INSERT INTO tbl_request_grup(request_id,id_user_requester,tutor_id,subject_id,topic,id_friends,method_pembayaran,harga_kelas,date_requested,duration_requested,user_utc) VALUES('$request_id','{$id_requester}','{$tutor_id}','{$subject_id}','{$topic}','{$participant}','{$metod}','$price','{$date_requested}','{$duration}','{$user_utc}')");

			if($qr){
				$lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request_grup')->row('request_id');
				$getdataclassgroup  = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request_grup as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='group' AND tr.request_id='$lastrequestid'")->row_array();
				$hargakelas         = ($duration/900)*$getdataclassgroup['harga_cm'];
                $hargaakhir         = number_format($hargakelas, 0, ".", ".");
                $toTutor            = 0;

				$ids = explode(",", $id_friends);

				$ALL_getdatafriend = array();
				foreach($ids as $id) { 
                    $showdatafriend = $this->db->query("SELECT email,user_name FROM tbl_user WHERE id_user='$id'")->row_array();
                    $kotaktd = $kotaktd."<tr><td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$showdatafriend['user_name']."</td><td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$showdatafriend['email']."</td></tr>";
                }


				foreach($ids as $id) {	
					$iduserrequest      = $getdataclassgroup['id_user_requester'];
                    $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
                    $getdatafriend      = $this->db->query("SELECT id_user,email,user_name FROM tbl_user WHERE id_user='$id'")->row_array();
                    $server_utcc        = $this->Rumus->getGMTOffset();
                    $intervall          = $user_utc - $server_utcc;
                    $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclassgroup['date_requested']);
                    $tanggal->modify("+".$intervall ." minutes");
                    $tanggal            = $tanggal->format('Y-m-d H:i:s');
                    $datelimit          = date_create($tanggal);
                    $dateelimit         = date_format($datelimit, 'd/m/y');
                    $harilimit          = date_format($datelimit, 'd');
                    $hari               = date_format($datelimit, 'l');
                    $tahunlimit         = date_format($datelimit, 'Y');
                    $waktulimit         = date_format($datelimit, 'H:i');   
                    $datelimit          = $dateelimit;
                    $sepparatorlimit    = '/';
                    $partslimit         = explode($sepparatorlimit, $datelimit);
                    $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                    $seconds            = $getdataclassgroup['duration_requested'];
                    $hours              = floor($seconds / 3600);
                    $mins               = floor($seconds / 60 % 60);
                    $secs               = floor($seconds % 60);
                    $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

                    //LOG DEMAND
                    $myip               = $_SERVER['REMOTE_ADDR'];
                    $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$lastrequestid','student request group class','$iduserrequest','group','$myip','102')");
                    //END LOG DEMAND

                    //TAMBAH KERANJANG
                    $a = $this->db->query("SELECT * FROM tbl_keranjang WHERE request_grup_id='$lastrequestid'")->row_array();                    
                    if (empty($a)) {
                    	$this->db->query("INSERT INTO tbl_keranjang (request_grup_id,id_user,price) VALUES ('$lastrequestid','$iduserrequest','$hargakelas')");	
                    }

                    $ALL_getdatafriend[] = $getdatafriend;
				}
				// Open connection
		        $ch = curl_init();

		        // Set the url, number of POST vars, POST data
		        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
		        curl_setopt($ch, CURLOPT_POST, true);
		        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_requestgroup','content' => array("sender_email" => $retok['email'], "getdatafriend" => $ALL_getdatafriend, "getdataclassgroup" => $getdataclassgroup, "lastrequestid" => $lastrequestid, "harilimit" => $harilimit, "hari" => $hari, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "durationrequested" => $durationrequested, "kotaktd" => $kotaktd, "namauser" => $retok['user_name'], "hargaakhir" => $hargaakhir ) )));

		        // Execute post
		        $result = curl_exec($ch);
		        curl_close($ch);
		        // echo $result;

		        $simpan_notif = $this->Rumus->create_notif($student_name.' telah mengajak anda untuk ikut Group Class. Silahkan cek di menu Extra Class untuk melihat detail pelajaran dan biaya, atau anda bisa klik link disini.',$id.' telah mengajak anda untuk ikut Group Class. Silahkan cek di menu Extra Class untuk melihat detail pelajaran dan biaya, atau anda bisa klik link disini.',$notif_type='single',$id_user=$id,$link='https://classmiles.com/Dataondemand');

				$this->Rumus->notif_extra_class($tutor_id,$fcm_token,$student_name,$subject_id);
				
				// "Fikri telah mengajak anda untuk ikut group class. Silahkan cek di menu Extra class untuk melihat detail pelajaran dan biaya, atau bisa anda klik disini.";
				// $simpan_notif = $this->Rumus->create_notif($notif='',$notif_mobile='',$notif_type='single',$id_user=$tutor_id,$link='https://classmiles.com/tutor/approval_ondemand');
				$rets['status'] = true;
				$rets['message'] = "Request success";
				$this->response($rets);
			}else{
				$rets['status'] = false;
				$rets['message'] = "error occured";
				$this->response($rets);
			}
		}
		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}

	public function getdatarequest_post()
	{
		$rets = array();
		$request_id = $this->post('request_id');		
	    
	    $getdata = $this->db->query("SELECT * FROM tbl_request_grup WHERE request_id='$request_id'")->row_array();	    

		$result = array();		

		$rets['status'] = true;
		$rets['message'] = "Succeed";		
		$rets['response'] = $getdata;
		$this->response($rets);
	}	

	public function approve_ondemandgroup_post($answer='a')
	{
		$rets 		= array();
		$idterima 	= $this->post('id_user');
		$request_id = $this->post('request_id');
		$answer 	= $this->post('answer');
		if ($this->get('access_token') != '') {
			if ($this->get('access_token') != "no") {
				$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
	            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
	                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
	                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
			    }	
			}
			$select = $this->db->query("SELECT * FROM tbl_request_grup WHERE request_id='$request_id' AND id_friends like '%\"$idterima\"%'")->row_array();

			// $id = [];
			// foreach($select['id_friends'] as $p => $o) {
			// 	$id[$o]=$select['id_friends'][$p]['id_user'];
			// 	// $simpan_notif = $this->Rumus->create_notif($notif='Permintaan anda telah di setujui oleh tutor. Kelas akan dimulai pada '.$date_requested.'.',$notif_mobile='Permintaan anda telah di setujui oleh tutor. Kelas akan dimulai pada '.$date_requested.'.',$notif_type='single',$id_user=$id_user_requester,$link='https://classmiles.com/first/');
			// }

			// foreach ($json_a['id_friends'] as $key => $value) {
			// 	$id = $value['id_user'][$key];
			// 	// $simpan_notif = $this->Rumus->create_notif($notif='hai bro',$notif_mobile='haibro',$notif_type='single',$id_user=$json_a['id_user'],$link='');
			
			// 	$rets['status'] = true;
			// 	$rets['message'] = "success";
			// 	$rets['data'] = $select;
			// 	$rets['did'] = $id;
			// 	}
			// $this->response($rets);
			
			if (!empty($select)) {
				# code...
				if ($answer == "t") {
					$this->db->simple_query("UPDATE tbl_request_grup SET approve=-1 WHERE request_id='{$request_id}'");
					// DI SINI KIRIM NOTIF KE SEMUA	

					$pizza  = $select['id_friends'];
			        $json_a = json_decode($pizza,true);
			        $json_a = $json_a['id_friends'];
					foreach ($json_a as $key => $item) {
						$to_send = $item['id_user'];
						$simpan_notif = $this->Rumus->create_notif('Permintaan anda ditolak, silahkan cari kelas lagi. ','Permintaan anda ditolak, silahkan cari kelas lagi. ','single',$to_send,'https://classmiles.com/Dataondemand');
					}         							

	                //LOG DEMAND
		            $myip               = $_SERVER['REMOTE_ADDR'];
		            $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','student invitation reject group class','$idterima','private','$myip','132')");
		            //END LOG DEMAND

					$rets['status'] = true;
					$rets['message'] = "Successfully Rejected";
					$this->response($rets);
				}
				else
				{
					$pizza  = $select['id_friends'];
		            $json_a = json_decode($pizza,true);

		            foreach($json_a['id_friends'] as $p => $o) {
		            	if ($idterima == $o['id_user']) {
		            		# code...
		            		$json_a['id_friends'][$p]['status'] = 1;
		            	} 
		            }
					$participant = $this->db->escape_str(json_encode($json_a));
					// print_r($participant);
					$approvegroup = $this->db->simple_query("UPDATE tbl_request_grup SET id_friends='$participant'  WHERE request_id='{$request_id}'");

					$isStillUnaccepted = $this->db->query("SELECT * FROM tbl_request_grup WHERE request_id='$request_id' AND id_friends like '%\"status\":0%'")->row_array();

					//LOG DEMAND
	                $myip               = $_SERVER['REMOTE_ADDR'];
	                $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','student invitation approve group class','$idterima','group','$myip','131')");
	                //END LOG DEMAND

					if(empty($isStillUnaccepted)){
						// DI SINI NOTIF KE TUTOR
						$pizza  = $select['id_friends'];
				        $json_a = json_decode($pizza,true);
				        $json_a = $json_a['id_friends'];
						foreach ($json_a as $key => $item) {
							$to_send = $item['id_user'];
							$simpan_notif = $this->Rumus->create_notif('Anda mendapatkan permintaan Grup Kelas, mohon segera cek dan respon.','Anda mendapatkan permintaan Grup Kelas, mohon segera cek dan respon.','single',$to_send,'https://classmiles.com/tutor/approval_ondemand_group');
						}  
					}
				}
			}
			if ($approvegroup) {
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$this->response($rets);
			}
			else
			{
				$rets['status'] = false;
				$rets['message'] = "failed";
				$this->response($rets);
			}
		}
		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}

	public function subjectadd_post()
	{
		$rets = array();
		$id_tutor = $this->post('id_tutor');		
		$subject_id = $this->post('subject_id');
		$harga = $this->post('harga');
		$hargacm = $this->post('harga');
		$hargagroup = $this->post('hargagroup');
		$hargacmgroup = $this->post('hargagroup');
		// if ($harga > 25000) {
		// 	$hargacm = $harga + ($harga * 0.2);
		// }
		// else if ($harga <= 25000) {
		// 	$hargacm = $harga + 5000;
		// }
		// if ($hargagroup > 25000) {
		// 	$hargacmgroup = $hargagroup + ($hargagroup * 0.2);
		// }
		// else if ($hargagroup <= 25000) {
		// 	$hargacmgroup = $hargagroup + 5000;
		// }
	    
	    $cekdata = $this->db->query("SELECT * FROM tbl_service_price WHERE tutor_id='{$id_tutor}' AND subject_id='{$subject_id}' AND class_type='private'")->row_array();
	    if (empty($cekdata)) {
	    	
	    	$getdata = $this->db->simple_query("INSERT INTO tbl_service_price (tutor_id,subject_id,class_type,harga,harga_cm) VALUES('{$id_tutor}','{$subject_id}','private','{$harga}','{$hargacm}')");		
	    	
	    }
	    else
	    {
	    	$uid = $cekdata['uid'] ;
	    	$updatedata = $this->db->simple_query("UPDATE tbl_service_price SET harga='{$harga}', harga_cm='{$hargacm}', class_type ='private' WHERE uid='{$uid}' ");
	    	
	    }	  

		$cekdata = $this->db->query("SELECT * FROM tbl_service_price WHERE tutor_id='{$id_tutor}' AND subject_id='{$subject_id}' AND class_type='group'")->row_array();
	    if (empty($cekdata)) {
	    	
	    	$getdata = $this->db->simple_query("INSERT INTO tbl_service_price (tutor_id,subject_id,class_type,harga,harga_cm) VALUES('{$id_tutor}','{$subject_id}','group','{$hargagroup}','{$hargacmgroup}')");		
	    	
	    }
	    else
	    {
	    	$uid = $cekdata['uid'] ;
	    	$updatedata = $this->db->simple_query("UPDATE tbl_service_price SET harga='{$hargagroup}', harga_cm='$hargacmgroup', class_type ='group' WHERE uid='{$uid}' ");
	    	
	    }	


		$rets['status'] = true;
		$rets['code'] = '300';
		$rets['message'] = "Succeed";	
		$this->response($rets);

	}

	public function createclass_privatechannel_post()
	{
		$rets = array();
		$tutor_id = $this->post('id_tutor');
		$tanggal = $this->post('tanggal');
		$time = $this->post('time');
		$durasi = $this->post('durasi');
		$template = $this->post('template');
		$pelajaran = $this->post('pelajaran');
		$topikpelajaran = $this->post('topikpelajaran');
		$channel_id = $this->post('channel_id');
		// $participantt = $this->post('participant');
		$st_time = $tanggal." ".$time;

		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";
		$user_utc = $this->post('user_utc');
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}

		if ($this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			$namesubject = $this->db->query("SELECT * FROM master_subject WHERE subject_id='{$pelajaran}'")->row_array()['subject_name'];

			$server_utc = $this->Rumus->getGMTOffset();
			$interval = $user_utc - $server_utc;
			$date_requested = DateTime::createFromFormat ('Y-m-d H:i:s', $tanggal." ".$time );
			$date_requested->modify("-".$interval ." minutes");

			$unix_requested = $date_requested->getTimestamp();
			$unix_now = time();
			if($unix_requested < ($unix_now + 3600) ){
				$rets['status'] = false;
				$rets['code'] = '402';
				$rets['message'] = "Maaf, Lewat Batas.";
				$this->response($rets);
			}

			$date_requested = $date_requested->format('Y-m-d H:i:s');

			$fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
			$fn_time->modify("+$durasi second");
			$fn_time = $fn_time->format('Y-m-d H:i:s');

			// CHECK JIKA WAKTU TELAH TERISI
			$cjwtt = $this->db->query("SELECT * FROM tbl_class WHERE ( ( '$date_requested' <= start_time AND '$fn_time' > start_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) OR ( '$date_requested' >= start_time AND '$date_requested' < finish_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) ) AND tutor_id=$tutor_id ")->result_array();
			if(!empty($cjwtt)){
				$rets['status'] = false;
				$rets['code'] = '403';
				$rets['message'] = "Maaf, Bentrok.";
				$this->response($rets);
			}

			$participant['visible'] = "private";
			$participant['participant'] = array();

			// foreach($participantt as $key => $id) {
			// 	$participant['participant'][$key]['id_user'] = $id;
			// }
			$participant = json_encode($participant);

			$exist = $this->db->query("SELECT point_id, active_point FROM uangpoint_channel WHERE channel_id='{$channel_id}' ")->row_array();
			if(!empty($exist)){
				$point = $exist['active_point'];
				$point_id = $exist['point_id'];
				// $harga_private15menit = 45;
				// $price = $harga_private15menit * ( ($durasi / 60) / 15 );

				// if($point >= $price){
					$topikpelajaran = $this->db->escape_str($topikpelajaran);
					$this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,participant,class_type,template_type,channel_id) VALUES('{$pelajaran}','{$namesubject}','{$topikpelajaran}','{$tutor_id}','{$date_requested}','{$fn_time}','{$participant}','private_channel','{$template}','{$channel_id}')");

					$class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

					// $new_point = $point-$price;
					// $this->db->simple_query("UPDATE uangpoint_channel SET active_point=$new_point WHERE point_id='$point_id'");
					// $trx_id = $this->Rumus->getChnPointTrxID();
					// $this->db->simple_query("INSERT INTO log_channel_point_transaction VALUES('$trx_id','$channel_id','$tutor_id','$class_id','$point','$price','{$new_point}',now())");

					$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$tutor_id'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						// echo $hash_id;
						$this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$class_id','$tutor_id','TRUE',0)");
					}	


					$rets['status'] = true;
					$rets['code'] = '200';
					$rets['message'] = "sukses";					
					$this->response($rets);	
				// }else{
				// 	$rets['status'] = false;
				// 	$rets['code'] = '-101';
				// 	$rets['message'] = "Point tidak mencukupi";
				// 	$this->response($rets);
				// }
				
			}
			$rets['status'] = false;
			$rets['code'] = '-100';
			$rets['message'] = "Tidak memiliki point data";
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}

	public function createclass_groupchannel_post()
	{
		$rets = array();
		$tutor_id = $this->post('id_tutor');
		$tanggal = $this->post('tanggal');
		$time = $this->post('time');
		$durasi = $this->post('durasi');
		$template = $this->post('template');
		$pelajaran = $this->post('pelajaran');
		$topikpelajaran = $this->post('topikpelajaran');
		$channel_id = $this->post('channel_id');
		// $participantt = $this->post('participant');
		$st_time = $tanggal." ".$time;

		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";
		$user_utc = $this->post('user_utc');
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}

		if ($this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			$namesubject = $this->db->query("SELECT * FROM master_subject WHERE subject_id='{$pelajaran}'")->row_array()['subject_name'];

			$server_utc = $this->Rumus->getGMTOffset();
			$interval = $user_utc - $server_utc;
			$date_requested = DateTime::createFromFormat ('Y-m-d H:i:s', $tanggal." ".$time );
			$date_requested->modify("-".$interval ." minutes");

			$unix_requested = $date_requested->getTimestamp();
			$unix_now = time();
			if($unix_requested < ($unix_now + 3600) ){
				$rets['status'] = false;
				$rets['code'] = '402';
				$rets['message'] = "Maaf, Lewat Batas.";
				$this->response($rets);
			}

			$date_requested = $date_requested->format('Y-m-d H:i:s');

			$fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
			$fn_time->modify("+$durasi second");
			$fn_time = $fn_time->format('Y-m-d H:i:s');

			// CHECK JIKA WAKTU TELAH TERISI
			$cjwtt = $this->db->query("SELECT * FROM tbl_class WHERE ( ( '$date_requested' <= start_time AND '$fn_time' > start_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) OR ( '$date_requested' >= start_time AND '$date_requested' < finish_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) ) AND tutor_id=$tutor_id ")->result_array();
			if(!empty($cjwtt)){
				$rets['status'] = false;
				$rets['code'] = '403';
				$rets['message'] = "Maaf, Bentrok.";
				$this->response($rets);
			}
			// $nowp1 = strtotime($date_requested) + 3600;
			// $fn_timee = date('Y-m-d H:i:s',$nowp1);

			// echo $time+"<br>";
			// // echo $interval+"\n";
			// // echo $date_requested;
			// return null;
			$participant['visible'] = "private";
			$participant['participant'] = array();
			// // $ids = explode(",", $participantt);		
			// foreach($participantt as $key => $id) {
			// 	$participant['participant'][$key]['id_user'] = $id;
			// }
			$exist = $this->db->query("SELECT point_id, active_point FROM uangpoint_channel WHERE channel_id='{$channel_id}' ")->row_array();
			if(!empty($exist)){
				$point = $exist['active_point'];
				$point_id = $exist['point_id'];
				// $harga_group15menit = 20;
				// $total_person = count($participant['participant']);
				// $price = $harga_group15menit * $total_person * ( ($durasi / 60) / 15 );

				// if($point >= $price){
					$topikpelajaran = $this->db->escape_str($topikpelajaran);
					$participant = json_encode($participant);
					$this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,participant,class_type,template_type,channel_id) VALUES('{$pelajaran}','{$namesubject}','{$topikpelajaran}','{$tutor_id}','{$date_requested}','{$fn_time}','{$participant}','group_channel','{$template}','{$channel_id}')");

					$class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

					// $new_point = $point-$price;
					// $this->db->simple_query("UPDATE uangpoint_channel SET active_point=$new_point WHERE point_id='$point_id'");
					// $trx_id = $this->Rumus->getChnPointTrxID();
					// $this->db->simple_query("INSERT INTO log_channel_point_transaction VALUES('$trx_id','$channel_id','$tutor_id','$class_id','$point','$price','{$new_point}',now())");

					$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$tutor_id'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						// echo $hash_id;
						$this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$class_id','$tutor_id','TRUE',0)");
					}	


					$rets['status'] = true;
					$rets['code'] = '200';
					$rets['message'] = "sukses";					
					$this->response($rets);
				// }else{
				// 	$rets['status'] = false;
				// 	$rets['code'] = '-101';
				// 	$rets['message'] = "Point tidak mencukupi";
				// 	$this->response($rets);
				// }
			}
			$rets['status'] = false;
			$rets['code'] = '-100';
			$rets['message'] = "Tidak memiliki point data";
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
			
	}

	public function createclass_multicastchannel_post()
	{
		$rets = array();
		$tutor_id = $this->post('id_tutor');
		$tanggal = $this->post('tanggal');
		$time = $this->post('time');
		$durasi = $this->post('durasi');
		$template = $this->post('template');
		$pelajaran = $this->post('pelajaran');
		$topikpelajaran = $this->post('topikpelajaran');
		$channel_id = $this->post('channel_id');
		// $participantt = $this->post('participant');
		$st_time = $tanggal." ".$time;

		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";
		$user_utc = $this->post('user_utc');
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}

		if ($this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			$namesubject = $this->db->query("SELECT * FROM master_subject WHERE subject_id='{$pelajaran}'")->row_array()['subject_name'];

			$server_utc = $this->Rumus->getGMTOffset();
			$interval = $user_utc - $server_utc;
			$date_requested = DateTime::createFromFormat ('Y-m-d H:i:s', $tanggal." ".$time );
			$date_requested->modify("-".$interval ." minutes");

			$unix_requested = $date_requested->getTimestamp();
			$unix_now = time();
			if($unix_requested < ($unix_now + 3600) ){
				$rets['status'] = false;
				$rets['code'] = '402';
				$rets['message'] = "Maaf, Lewat Batas.";
				$this->response($rets);
			}

			$date_requested = $date_requested->format('Y-m-d H:i:s');

			$fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
			$fn_time->modify("+$durasi second");
			$fn_time = $fn_time->format('Y-m-d H:i:s');

			// CHECK JIKA WAKTU TELAH TERISI
			$cjwtt = $this->db->query("SELECT * FROM tbl_class WHERE ( ( '$date_requested' <= start_time AND '$fn_time' > start_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) OR ( '$date_requested' >= start_time AND '$date_requested' < finish_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) ) AND tutor_id=$tutor_id ")->result_array();
			if(!empty($cjwtt)){
				$rets['status'] = false;
				$rets['code'] = '403';
				$rets['message'] = "Maaf, Bentrok.";
				$this->response($rets);
			}
			// $nowp1 = strtotime($date_requested) + 3600;
			// $fn_timee = date('Y-m-d H:i:s',$nowp1);

			// echo $time+"<br>";
			// // echo $interval+"\n";
			// // echo $date_requested;
			// return null;
			$participant['visible'] = "private";
			$participant['participant'] = array();
			// $ids = explode(",", $participantt);		
			// foreach($participantt as $key => $id) {
			// 	$participant['participant'][$key]['id_user'] = $id;
			// }
			$exist = $this->db->query("SELECT point_id, active_point FROM uangpoint_channel WHERE channel_id='{$channel_id}' ")->row_array();
			if(!empty($exist)){
				$point = $exist['active_point'];
				$point_id = $exist['point_id'];
				$harga_multicast15menit = 1;
				// $total_person = count($participant['participant']);
				// $price = $harga_multicast15menit * $total_person * ( ($durasi / 60) / 15 );

				// if($point >= $price){
					$topikpelajaran = $this->db->escape_str($topikpelajaran);
					$participant = json_encode($participant);
					$this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,duration,participant,class_type,template_type,channel_id,channel_status) VALUES('{$pelajaran}','{$namesubject}','{$topikpelajaran}','{$tutor_id}','{$date_requested}','{$fn_time}','{$durasi}','{$participant}','multicast_channel_paid','{$template}','{$channel_id}','0')");

					$class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

					// $new_point = $point-$price;
					// $this->db->simple_query("UPDATE uangpoint_channel SET active_point=$new_point WHERE point_id='$point_id'");
					// $trx_id = $this->Rumus->getChnPointTrxID();
					// $this->db->simple_query("INSERT INTO log_channel_point_transaction VALUES('$trx_id','$channel_id','$tutor_id','$class_id','$point','$price','{$new_point}',now())");
					
					$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$tutor_id'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						// echo $hash_id;
						$this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$class_id','$tutor_id','TRUE',0)");
					}	

					$rets['status'] = true;
					$rets['code'] = '200';
					$rets['message'] = "sukses";					
					$this->response($rets);	
				// }else{
				// 	$rets['status'] = false;
				// 	$rets['code'] = '-101';
				// 	$rets['message'] = "Point tidak mencukupi";
				// 	$this->response($rets);
				// }
			}
			$rets['status'] = false;
			$rets['code'] = '-100';
			$rets['message'] = "Tidak memiliki point data";
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
		
	}

	public function modifyDateTimeFromAdmin_post()
	{
		$class_id 		= $this->post('class_id');
		$date_start 	= $this->post('date_start');
		$date_finish 	= $this->post('date_finish');
		$user_device 	= $this->post('user_device');
		$user_utc 		= $this->post('user_utc');

		$server_utc 	= $this->Rumus->getGMTOffset();
		$interval 		= $user_utc - $server_utc;

		$start_time 	= DateTime::createFromFormat ('Y-m-d H:i:s', $date_start );
		$start_time->modify("-".$interval ." minutes");
		$start_time 	= $start_time->format('Y-m-d H:i:s');

		$finish_time 	= DateTime::createFromFormat ('Y-m-d H:i:s', $date_finish );
		$finish_time->modify("-".$interval ." minutes");
		$finish_time 	= $finish_time->format('Y-m-d H:i:s');

		$tutor_id 		= $this->db->query("SELECT tutor_id FROM tbl_class WHERE class_id='$class_id'")->row_array()['tutor_id'];
		// CHECK JIKA WAKTU TELAH TERISI
		$cjwtt = $this->db->query("SELECT * FROM tbl_class WHERE ( ( '$start_time' <= start_time AND '$finish_time' > start_time AND ( '$finish_time' <= finish_time OR '$finish_time' > finish_time )  ) OR ( '$start_time' >= start_time AND '$start_time' < finish_time AND ( '$finish_time' <= finish_time OR '$finish_time' > finish_time )  ) ) AND tutor_id=$tutor_id AND class_id != $class_id")->result_array();
		$validate_from_tbl_req = $this->db->query("SELECT * FROM tbl_request WHERE ( ( '$start_time' <= date_requested AND '$finish_time' > date_requested AND ( '$finish_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$finish_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$start_time' >= date_requested AND '$start_time' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$finish_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$finish_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND tutor_id=$tutor_id AND approve=2 ")->result_array();
		$validate_from_tbl_req_grup = $this->db->query("SELECT * FROM tbl_request_grup WHERE ( ( '$start_time' <= date_requested AND '$finish_time' > date_requested AND ( '$finish_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$finish_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$start_time' >= date_requested AND '$start_time' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$finish_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$finish_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND tutor_id=$tutor_id AND approve=2 ")->result_array();
		if(!empty($cjwtt) || !empty($validate_from_tbl_req) || !empty($validate_from_tbl_req_grup) ){
			$rets['status'] = false;
			$rets['code'] = '403';
			$rets['adsf'] = "SELECT * FROM tbl_class WHERE ( ( '$start_time' <= start_time AND '$finish_time' > start_time AND ( '$finish_time' <= finish_time OR '$finish_time' > finish_time )  ) OR ( '$start_time' >= start_time AND '$start_time' < finish_time AND ( '$finish_time' <= finish_time OR '$finish_time' > finish_time )  ) ) AND tutor_id=$tutor_id ";
			$rets['message'] = "Maaf, Waktu tersebut sudah terisi.";
			$this->response($rets);
		}

		$updtDate 		= $this->db->query("UPDATE tbl_class SET start_time='$start_time', finish_time='$finish_time' WHERE class_id='$class_id'");
		if ($updtDate) {
			$rets['status'] = true;
			$rets['code'] = '200';
			$rets['message'] = "Success Update Class";					
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = '-100';
			$rets['message'] = "Failed Update Class";					
			$this->response($rets);
		}

	}

	public function createclass_free_post()
	{

		$rets = array();
		$tutor_id = $this->post('id_tutor');
		$kuota_participants = $this->post('kuota_participants');
		$tanggal = $this->post('tanggal');
		$time = $this->post('time');
		$durasi = $this->post('durasi');
		$template = $this->post('template');
		$pelajaran = $this->post('pelajaran');
		$topikpelajaran = $this->post('topikpelajaran');
		$channel_id = $this->post('channel_id');
		$st_time = $tanggal." ".$time;
		$status = $this->post('status');
		$total = null;
		$valid = true;
		
		$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
            $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
            $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
        }
		if ($status == 2) {
			$cektotal = $this->db->query("SELECT count(*) as total from tbl_class where tutor_id=$tutor_id")->row_array();
			$total = $cektotal['total'];

			if ($total >= 5) {
				$valid = false;
				$rets['status'] = true;
				$rets['code'] = '405';
				$rets['message'] = "Maaf, Sebelum mendapat persetujuan dari pihak kami, anda hanya diperbolehkan membuat kelas sebanyak 5 kali.";
				$this->response($rets);
			}
		}

		if($valid) {
			$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";
			$user_utc = $this->post('user_utc');
			if($user_device == 'mobile'){
				$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
			}

			$namesubject = $this->db->query("SELECT * FROM master_subject WHERE subject_id='{$pelajaran}'")->row_array()['subject_name'];

			$server_utc = $this->Rumus->getGMTOffset();
			$interval = $user_utc - $server_utc;
			$date_requested = DateTime::createFromFormat ('Y-m-d H:i:s', $tanggal." ".$time );
			$date_requested->modify("-".$interval ." minutes");

			$unix_requested = $date_requested->getTimestamp();
			$unix_now = time();
			if($unix_requested < ($unix_now + 3600) ){
				$rets['status'] = false;
				$rets['code'] = '402';
				$rets['message'] = "Maaf, Lewat Batas.";
				$this->response($rets);
			}

			$date_requested = $date_requested->format('Y-m-d H:i:s');

			$fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
			$fn_time->modify("+$durasi second");
			$fn_time = $fn_time->format('Y-m-d H:i:s');

			// CHECK JIKA WAKTU TELAH TERISI
			$cjwtt = $this->db->query("SELECT * FROM tbl_class WHERE ( ( '$date_requested' <= start_time AND '$fn_time' > start_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) OR ( '$date_requested' >= start_time AND '$date_requested' < finish_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) ) AND tutor_id=$tutor_id ")->result_array();
			$validate_from_tbl_req = $this->db->query("SELECT * FROM tbl_request WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND tutor_id=$tutor_id AND approve=2 ")->result_array();
			$validate_from_tbl_req_grup = $this->db->query("SELECT * FROM tbl_request_grup WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND tutor_id=$tutor_id AND approve=2 ")->result_array();
			if(!empty($cjwtt) || !empty($validate_from_tbl_req) || !empty($validate_from_tbl_req_grup) ){
				$rets['status'] = false;
				$rets['code'] = '403';
				$rets['message'] = "Maaf, Bentrok.";
				$this->response($rets);
			}
			$participant = json_encode(array("visible" => "followers"));

			//GET MAX PARTICIPANTS
			$multi_vol 	= $this->db->query("SELECT * FROM tbl_multicast_volume WHERE idv='$kuota_participants'")->row_array();
			$max_participants = $multi_vol['volume'];
			$this->db->query("UPDATE tbl_multicast_volume SET status = 100 WHERE idv='$kuota_participants'");

			//ESCAPE
			$topikpelajaran = $this->db->escape_str($topikpelajaran); // HASIL NYA Sa\'ad Bin Abi Waqash
			$this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,participant,max_participants,class_type,template_type,channel_id) VALUES('{$pelajaran}','{$namesubject}','{$topikpelajaran}','{$tutor_id}','{$date_requested}','{$fn_time}','{$participant}','{$max_participants}','multicast','{$template}','{$channel_id}')");
			$class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
			$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$tutor_id'")->row_array();
			if(!empty($student)){
				$hash_id = $student['session_id'];
			}else{
				$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
				// echo $hash_id;
				$this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$class_id','$tutor_id','TRUE',0)");
			}

			$tutor_name = $retok['user_name'];
			////------------ BUAT NGIRIM EVENT FIREBASE --------------//
			$data_followers = $this->db->query("SELECT id_user FROM tbl_booking WHERE tutor_id='$tutor_id' AND subject_id='$pelajaran'")->result_array();
			foreach ($data_followers as $key => $value) {
				$ids = $value['id_user'];

				$json_notif = "";
	            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$ids}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
	            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
	                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "10","title" => "Classmiles", "class_id" => "$class_id", "tutor_name" => "$tutor_name")));
	            }
	            if($json_notif != ""){
	                $headers = array(
	                    'Authorization: key=' . FIREBASE_API_KEY,
	                    'Content-Type: application/json'
	                    );
	                // Open connection
	                $ch = curl_init();

	                // Set the url, number of POST vars, POST data
	                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
	                curl_setopt($ch, CURLOPT_POST, true);
	                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

	                // Execute post
	                $result = curl_exec($ch);
	                curl_close($ch);
	                // echo $result;
	            }
			}
            ////------------ END -------------------------------------//


			$rets['status'] = true;
			$rets['code'] = '200';
			$rets['message'] = "Success Create Class";					
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
		
	}

	public function createclass_paid_post()
	{
		$rets = array();
		$tutor_id = $this->post('id_tutor');
		$kuota_participants = $this->post('kuota_participants');
		$tanggal = $this->post('tanggal');
		$time = $this->post('time');
		$durasi = $this->post('durasi');
		$tipekelas = $this->post('tipekelas');
		$template = $this->post('template');
		$pelajaran = $this->post('pelajaran');
		$topikpelajaran = $this->post('topikpelajaran');
		$channel_id = $this->post('channel_id');
		$harga = $this->post('harga');
		$harga_cm = $this->post('harga');		

		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";
		$user_utc = $this->post('user_utc');
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		if ($this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			$namesubject = $this->db->query("SELECT * FROM master_subject WHERE subject_id='{$pelajaran}'")->row_array()['subject_name'];
			
			// $st_time = date('Y-m-d H:i:s',$time);
			$st_time = $tanggal." ".$time;
			
			$server_utc = $this->Rumus->getGMTOffset();
			$interval = $user_utc - $server_utc;
			$date_requested = DateTime::createFromFormat ('Y-m-d H:i:s', $tanggal." ".$time );
			$date_requested->modify("-".$interval ." minutes");

			$unix_requested = $date_requested->getTimestamp();
			$unix_now = time();
			if($unix_requested < ($unix_now + 3600) ){
				$rets['status'] = false;
				$rets['code'] = '402';
				$rets['message'] = "Maaf, Lewat Batas.";
				$this->response($rets);
			}

			$date_requested = $date_requested->format('Y-m-d H:i:s');

			$fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
			$fn_time->modify("+$durasi second");
			$fn_time = $fn_time->format('Y-m-d H:i:s');

			$last_order = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
			$last_order->modify("-3600 second");
			$last_order = $last_order->format('Y-m-d H:i:s');

			// CHECK JIKA WAKTU TELAH TERISI
			$cjwtt = $this->db->query("SELECT * FROM tbl_class WHERE ( ( '$date_requested' <= start_time AND '$fn_time' > start_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) OR ( '$date_requested' >= start_time AND '$date_requested' < finish_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) ) AND tutor_id=$tutor_id ")->result_array();
			$validate_from_tbl_req = $this->db->query("SELECT * FROM tbl_request WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND tutor_id=$tutor_id AND approve=2 ")->result_array();
			$validate_from_tbl_req_grup = $this->db->query("SELECT * FROM tbl_request_grup WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND tutor_id=$tutor_id AND approve=2 ")->result_array();
			if(!empty($cjwtt) || !empty($validate_from_tbl_req) || !empty($validate_from_tbl_req_grup) ){
				$rets['status'] = false;
				$rets['code'] = '403';
				$rets['message'] = "Maaf, Bentrok.";
				$this->response($rets);
			}

			//GET MAX PARTICIPANTS
			$multi_vol 	= $this->db->query("SELECT * FROM tbl_multicast_volume WHERE idv='$kuota_participants'")->row_array();
			$max_participants = $multi_vol['volume'];
			$this->db->query("UPDATE tbl_multicast_volume SET status = 100 WHERE idv='$kuota_participants'");

			$topikpelajaran = $this->db->escape_str($topikpelajaran);
			$participant = json_encode(array("visible" => "followers"));
			$this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,last_order,participant,max_participants,class_type,template_type,channel_id) VALUES('{$pelajaran}','{$namesubject}','{$topikpelajaran}','{$tutor_id}','{$date_requested}','{$fn_time}','$last_order','{$participant}','{$max_participants}','multicast_paid','{$template}','{$channel_id}')");
			
			// if ($harga > 25000) {
			// 	$harga_cm = $harga + ($harga*0.2);
			// }
			// if ($harga <= 5000) {
			// 	$harga_cm = 5000;
			// }
			// $fee1 = $harga*20/100 >= 25000 ? $harga*20/100 : 5000;
			// $harga = $harga + $fee1;
			if ($harga > 75000) {
				$persen = $harga * 0.2;
				$harga_cm = $harga + $persen;
			}
			else
			{
				$harga_cm = $harga + 5000;
			}

			$class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
			$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$tutor_id'")->row_array();
			if(!empty($student)){
				$hash_id = $student['session_id'];
			}else{
				$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
				// echo $hash_id;
				$this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$class_id','$tutor_id','TRUE',0)");
			}	
			$this->db->simple_query("INSERT INTO tbl_class_price (class_id,harga,harga_cm) VALUES ('$class_id','$harga','$harga_cm')");

			$tutor_name = $retok['user_name'];
			////------------ BUAT NGIRIM EVENT FIREBASE --------------//
			$data_followers = $this->db->query("SELECT id_user FROM tbl_booking WHERE tutor_id='$tutor_id' AND subject_id='$pelajaran'")->result_array();
			foreach ($data_followers as $key => $value) {
				$ids = $value['id_user'];

				$json_notif = "";
	            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$ids}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
	            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
	                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "10","title" => "Classmiles", "class_id" => "$class_id", "tutor_name" => "$tutor_name")));
	            }
	            if($json_notif != ""){
	                $headers = array(
	                    'Authorization: key=' . FIREBASE_API_KEY,
	                    'Content-Type: application/json'
	                    );
	                // Open connection
	                $ch = curl_init();

	                // Set the url, number of POST vars, POST data
	                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
	                curl_setopt($ch, CURLOPT_POST, true);
	                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

	                // Execute post
	                $result = curl_exec($ch);
	                curl_close($ch);
	                // echo $result;
	            }
			}
            ////------------ END -------------------------------------//

			$rets['status'] = true;
			$rets['code'] = '200';
			$rets['message'] = "sukses";					
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
		
	}

	public function set_language_post()
	{
		$rets = array();
		$ceklanguage = $this->post('ceklanguage');	
		$lang = $this->post('lang');
		
		// if ($ceklanguage == 1) {
		// 	$this->session->set_userdata('lang', $this->session->userdata('lang'));
		// 	$this->session->set_userdata('ceklanguage', $ceklanguage);
		// 	$rets['code'] = '200';
		// 	$rets['message'] = "sukses";
		// }
		// else
		// {
			$this->session->set_userdata('get_location', $lang);
			if ($lang == "ID") {
				$lang = "indonesia";
			}
			else
			{
				$lang = "english";
			}			
			// if ($lang != $this->session->userdata('lang')) {
				
			// 	if ($lang == "ID") {
			// 		$lang = "indonesia";
			// 	}
			// 	else
			// 	{
			// 		$lang = "english";
			// 	}
				
			// }	
			$this->session->set_userdata('lang', $lang);
			// $this->session->set_userdata('ceklanguage', 1);

			$rets['code'] = '200';
			$rets['message'] = "sukses";	
		// }			
		$this->response($rets);
	}

	public function set_myip_post()
	{
		$rets = array();
		$ip = $this->post('ip');
				
		$this->session->set_userdata('myip', $ip);
		$rets['code'] = '200';
		$rets['message'] = $ip;			
		$this->response($rets);
	}

	public function joinMulitcast_post(){
		$rets = array();
		$id_user 		= $this->post('id_user');
		$class_id 		= $this->post('class_id');
		$user_utc 		= $this->post('user_utc');
		$id_ortu 		= $this->post('id_ortu');
		// echo '1';
		// return false;
		if ($this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
            // $id_user_emak = $retok['id_user'];            

			$q = $this->db->query("SELECT tc.*, tcp.harga FROM tbl_class as tc LEFT JOIN tbl_class_price as tcp ON tc.class_id=tcp.class_id WHERE tc.class_id='$class_id' AND tc.participant NOT LIKE '%\"id_user\":\"$id_user\"%'")->row_array();
			
			if(!empty($q)){
				$class_type = $q['class_type'];

				// AND tc.class_type='multicast_paid'
				if($class_type == 'multicast'){
					$participant_arr = json_decode($q['participant'],true);
					if(!array_key_exists('participant', $participant_arr)){
						$participant_arr['participant'] = array();
					}
					$ada = 0;					
					foreach ($participant_arr['participant'] as $key => $value) {
						if ($value['id_user'] == $id_ortu) {
							$ada = 1;
						}
					}

					if ($ada == 1) {
						array_push($participant_arr['participant'], array("id_user" => $id_user));							
					}
					else
					{
						if ($id_user == $id_ortu) {
							array_push($participant_arr['participant'], array("id_user" => $id_user));	
						}
						else
						{
							array_push($participant_arr['participant'], array("id_user" => $id_user));
							array_push($participant_arr['participant'], array("id_user" => $id_ortu));
						}
					}
					// echo json_encode($participant_arr);
					$participant_arr = json_encode($participant_arr);
					$this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
					$cekrating = $this->db->query("SELECT * FROM tbl_class_rating WHERE class_id='$class_id' and id_user='$id_ortu'")->row_array();
					if (empty($cekrating)) {
						$this->db->simple_query("INSERT INTO tbl_class_rating(class_id, id_user, class_ack) VALUES('$class_id','$id_ortu',-1)");
					}

					////------------ BUAT NGIRIM EVENT FIREBASE --------------//
		            $json_notif = "";
		            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$id_user}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
		            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
		                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "10","title" => "Classmiles", "text" => "")));
		            }
		            if($json_notif != ""){
		                $headers = array(
		                    'Authorization: key=' . FIREBASE_API_KEY,
		                    'Content-Type: application/json'
		                    );
		                // Open connection
		                $ch = curl_init();

		                // Set the url, number of POST vars, POST data
		                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		                curl_setopt($ch, CURLOPT_POST, true);
		                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

		                // Execute post
		                $result = curl_exec($ch);
		                curl_close($ch);
		                // echo $result;
		            }
		            ////------------ END -------------------------------------//
					
					$rets['status'] = true;
					$rets['message'] = "Succeed";
					$rets['method'] = "joinMulticast";				
					$this->response($rets);
				} else {	
					$price = $q['harga'];
					
					$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
					$datenow 			= date("Y-m-d h:i:s");
					$server_utc 		= $this->Rumus->getGMTOffset();
					$interval 			= $user_utc - $server_utc;
					$datenow 			= DateTime::createFromFormat ('Y-m-d H:i:s',$datenow);		
					$datenow->modify("-".$interval ." minutes");
					$datenow 			= $datenow->format('Y-m-d H:i:s');
					$expired			= $q['start_time'];
					$namee 				= $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$id_user'")->row_array();

					$cekexpired			= $this->db->query("SELECT * FROM tbl_class WHERE '$datenow' <= start_time AND class_id='$class_id'")->row_array();
					// echo "SELECT * FROM tbl_class WHERE '$datenow' <= start_time AND class_id='$class_id'";
					// return false;
					if (empty($cekexpired)) {
						$this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message', "Expired");
						redirect('/');
					}
					else
					{	
						$cekinvoice = $this->db->query("SELECT * FROM tbl_order_detail as tod INNER JOIN tbl_order as tor ON tod.order_id=tor.order_id INNER JOIN tbl_invoice as ti ON ti.order_id=tod.order_id WHERE tod.class_id='$class_id' AND tor.buyer_id='$id_user'")->row_array();
						$slct = $this->db->query("SELECT * FROM tbl_keranjang as tk INNER JOIN tbl_request_multicast as trm ON tk.request_multicast_id=trm.request_id WHERE tk.id_user='$id_user' AND trm.id_requester='$id_user'")->row_array();
	
						if (empty($slct) && empty($cekinvoice)) {
							$request_id = $this->Rumus->get_new_request_id('multicast');
							$add 			= $this->db->query("INSERT INTO tbl_request_multicast (request_id,class_id,id_requester,date_expired,status) VALUES ('$request_id','$class_id','$id_user','$expired','2')");
							$lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request_multicast')->row('request_id');

							$hargaakhir 		= $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id='$class_id'")->row_array()['harga_cm'];
							$this->db->query("INSERT INTO tbl_keranjang (request_multicast_id,id_user,price) VALUES ('$lastrequestid','$id_user','$hargaakhir')");

							////------------ BUAT NGIRIM EVENT FIREBASE --------------//
				            $json_notif = "";
				            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$id_user_emak}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
				            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
				                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "30","title" => "Classmiles", "text" => "")));
				            }
				            if($json_notif != ""){
				                $headers = array(
				                    'Authorization: key=' . FIREBASE_API_KEY,
				                    'Content-Type: application/json'
				                    );
				                // Open connection
				                $ch = curl_init();

				                // Set the url, number of POST vars, POST data
				                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
				                curl_setopt($ch, CURLOPT_POST, true);
				                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

				                // Execute post
				                $result = curl_exec($ch);
				                curl_close($ch);
				                // echo $result;
				            }
				            ////------------ END -------------------------------------//

							$idadmin = $this->db->query("SELECT id_user, user_name FROM  tbl_user WHERE usertype_id ='admin'")->row_array();
							$adminid = $idadmin['id_user'];				
							$adminname = $idadmin['user_name'];	

							//NOTIF STUDENT
							$notif_message = json_encode( array("code" => 110));					
							$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','https://classmiles.com/Transaction/multicast_join/".$class_id."?t=user&user_utc=".$this->session->userdata('user_utc')."&view=detail','0')");

							//NOTIF ADMIN
							$notif_message = json_encode( array("code" => 111, "tutor_name" => $adminname));					
							$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','5','https://classmiles.com/admin/PaymentMulticast','0')");

							$rets['status'] = true;
							$rets['message'] = "Pembelian anda berhasil";
							$rets['method'] = "joinMulticast";				
							$this->response($rets);	

						} else {
							if ($slct['status'] == 2) {
								$rets['status'] = false;
								$rets['message'] = "Pembelian Anda belum dibayar";
								$rets['method'] = "joinMulticast";
									
								$this->response($rets);
							}
							else
							{
								$rets['status'] = false;
								$rets['message'] = "Pembelian Anda gagal";
								$rets['method'] = "joinMulticast";
									
								$this->response($rets);
							}			
						}
					}					
				}
				
				
			} else {		
				$rets['status'] = false;
				$rets['message'] = "Pembelian Anda gagal";
				$rets['method'] = "joinMulticast";
					
				$this->response($rets);
			}
		}
		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}

	public function aksi_login_post()
	{
		$rets = array();
		// $iduser = $this->post('id_user');

		// // $get = $this->db->query("SELECT trg.*, tu_tutor.user_name as tutor_name, tu_requester.user_name as requester_name, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu_tutor ON trg.tutor_id=tu_tutor.id_user INNER JOIN tbl_user as tu_requester ON trg.id_user_requester=tu_requester.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.id_friends LIKE '%\"$iduser\",\"status\":0%' && approve=0 ORDER BY datetime DESC")->result_array();
		// $get = $this->db->query("SELECT trg.*, tu_tutor.user_name as tutor_name, tu_requester.user_name as requester_name, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu_tutor ON trg.tutor_id=tu_tutor.id_user INNER JOIN tbl_user as tu_requester ON trg.id_user_requester=tu_requester.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.id_friends LIKE '%\"$iduser\"%' ORDER BY trg.datetime ASC")->result_array();


		// if (empty($get)) {
		// 	$rets['status'] = false;
		// 	$rets['code'] = '300';
		// 	$rets['message'] = "gagal";
		// 	$rets['response'] = $get;
		// 	$this->response($rets);
		// }
		// foreach ($get as $key => $value) {
		// 	$get[$key]['id_friends'] = json_decode($value['id_friends'], true);
		// }
		// $rets['status'] = true;
		// $rets['code'] = '200';
		// $rets['response'] = $get;					
		// $this->response($rets);

		$email = $this->post('email');
		$kata_sandi = $this->post('kata_sandi');
		$cokies = $this->post('bapuks');
		$userutc = $this->post('user_utc');
		$this->session->set_userdata('user_utc', $userutc);

		// echo "<script>alert($cokies);</script>";
		$where = array(
			'email' => $email,
			'kata_sandi' => $kata_sandi
		);

		$cek = $this->M_login->cek_login("tbl_user",$where);
		if ($cek == 0) {
			$load = $this->lang->line('wrongpassword');
			// $this->session->set_flashdata('mes_alert','danger');
			// $this->session->set_flashdata('mes_display','block');
			// $this->session->set_flashdata('mes_message', $load);
			// $this->session->set_userdata('message','wrongpassword');
			//echo '<script>alert("Maaf User Tersebut Tidak ada");</script>';
			// redirect('/login');
			
			$rets['status'] = false;
			$rets['code'] = 104;
			$rets['message'] = $load;
			$rets['link'] = 'web';
			$this->response($rets);
			// return false;
		}
		if ($this->session->userdata('usertype_id') == 'student') {
			if ($this->session->userdata('status')==0) {
				$rets['status'] = false;
				$rets['code'] = 102;
				$rets['message'] = $load;
				$rets['link'] = 'Subjects';
				$this->response($rets);
			}
            else if ($this->session->userdata('status')==1) {
				$rets['status'] = true;
				$rets['code'] = 100;
				$rets['message'] = 'berhasil';
				$rets['link'] = 'Login';
				$this->response($rets);
			}
		}
		else if($this->session->userdata('usertype_id') == 'tutor'){
			if ($this->session->userdata('status')==3) {
				$load = $this->lang->line('completeprofile');
				$rets['mes_alert'] ='warning';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				// $this->session->set_userdata('message', 'completeprofile');
				$this->session->set_flashdata('rets',$rets);
				// $this->session->set_userdata('inggris');
				$rets['status'] = true;
				$rets['code'] = 203;
				$rets['message'] = $load;
				$rets['link'] = '/tutor/approval';
				$this->response($rets);
				// redirect('');
			}else if($this->session->userdata('status')==2){
				$load = $this->lang->line('requestsent');
				$rets['mes_alert'] ='info';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				// $this->session->set_userdata('message', 'requestsent');
				$this->session->set_flashdata('rets',$rets);
				// $this->session->set_userdata('inggris');

				$rets['status'] = true;
				$rets['code'] = 204;
				$rets['message'] = $load;
				$rets['link'] = '/tutor';
				$this->response($rets);
			}else if($this->session->userdata('status')==1){
				$load = $this->lang->line('requestsent');
				$rets['mes_alert'] ='info';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				// $this->session->set_userdata('message', 'requestsent');
				$this->session->set_flashdata('rets',$rets);
				// $this->session->set_userdata('inggris');

				$rets['status'] = true;
				$rets['code'] = 200;
				$rets['message'] = $load;
				$rets['link'] = '/tutor';
				$this->response($rets);
			}else if($this->session->userdata('status')==0){
				$load = $this->lang->line('checkemail');
				$rets['mes_alert'] ='warning';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				// $this->session->set_userdata('message', 'checkemail');
				$this->session->set_flashdata('rets',$rets);

				$rets['status'] = true;
				$rets['code'] = 205;
				$rets['message'] = $load;
				$rets['link'] = '/tutor';
				$this->response($rets);
			}
		}
		else if($this->session->userdata('usertype_id') == 'admin'){
			// $this->session->set_userdata('inggris');
			$rets['status'] = true;
			$rets['code'] = 300;
			$rets['message'] = 'berhasil';
			$rets['link'] = '/admin';
			$this->response($rets);
			// redirect("/admin");
		}
		else if($this->session->userdata('usertype_id') == 'user channel'){
			$load = $this->lang->line('completeprofile');
			$rets['mes_alert'] ='success';
			$rets['mes_display'] ='block';
			$rets['mes_message'] = 'blablabla';

			$rets['status'] = true;
			$rets['code'] = 400;
			$rets['message'] = $load;
			$rets['link'] = '/Channel';
			$this->response($rets);
			// redirect('/Channel');
		}
		else{
			$rets['status'] = true;
			$rets['code'] = 500;
			$rets['message'] = $load;
			$rets['link'] = '/first/moderator';
			$this->response($rets);
			// redirect("/first/moderator");
		}
	}

	public function clearsession_post(){
		$this->session->unset_userdata('code'); 
		$this->session->unset_userdata('update_jenjang'); 
		$this->session->unset_userdata('tutorinclass'); 
		$rets['status'] = true;		
		$rets['message'] = 'berhasil';		
		$this->response($rets);
	}

	public function createsession_post(){
		$this->session->set_userdata('code','102'); 
		$rets['status'] = true;		
		$rets['message'] = 'berhasil set';		
		$this->response($rets);
	}

	public function get_dataprivate_post()
	{
		$rets = array();
		$iduser = $this->post('id_user');
		if ($this->get('access_token') != '') {
			$user_utc = $this->post('user_utc');
			$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";

			$server_utc = $this->Rumus->getGMTOffset();
			if($user_device == 'mobile'){
				$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
			}
			$interval   = $user_utc - $server_utc;

			// $db_res = $this->db->query("SELECT tu.id_user, tu.user_name, tu.first_name, tu.email, tu.usertype_id, tps.class_id, tc.participant, tc.break_state,tc.break_pos, tc.subject_id, tc.name, tc.description, tc.tutor_id, tps.session_id, DATE_ADD(tc.start_time, INTERVAL $interval minute) as start_time, DATE_ADD(tc.finish_time, INTERVAL $interval minute) as finish_time, tu.st_tour, tc.template_type, tc.class_type FROM tbl_user as tu INNER JOIN ( tbl_participant_session as tps INNER JOIN tbl_class as tc ON tps.class_id=tc.class_id) ON tu.id_user=tps.id_user WHERE tps.session_id='".$session_key."'")->row_array();

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }

			// $get = $this->db->query("SELECT trg.*, tu_tutor.user_name as tutor_name, tu_requester.user_name as requester_name, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu_tutor ON trg.tutor_id=tu_tutor.id_user INNER JOIN tbl_user as tu_requester ON trg.id_user_requester=tu_requester.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.id_friends LIKE '%\"$iduser\",\"status\":0%' && approve=0 ORDER BY datetime DESC")->result_array();

			// $get = $this->db->query("SELECT tr.*, tu.user_name, ms.subject_name FROM tbl_request as tr INNER JOIN tbl_user as tu ON tr.tutor_id=tu.id_user INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tr.id_user_requester = '$iduser' ORDER BY datetime DESC")->result_array();
			$get = $this->db->query("SELECT trg.*, DATE_ADD(trg.date_requested, INTERVAL $interval minute) as date_requested_utc, mc.nicename as country, tu.user_name, tu.user_image, ms.subject_name FROM tbl_request as trg INNER JOIN tbl_user as tu ON trg.tutor_id=tu.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id INNER JOIN master_country as mc ON mc.nicename = tu.user_nationality WHERE trg.id_user_requester = '$iduser' ORDER BY datetime DESC")->result_array();		

			if (empty($get)) {
				$rets['status'] = false;
				$rets['code'] = '300';
				$rets['message'] = "gagal";
				$rets['response'] = $get;
				$this->response($rets);
			}
			// foreach ($get as $key => $value) {
			// 	$get[$key]['id_friends'] = json_decode($value['id_friends'], true);
			// }
			$rets['status'] = true;
			$rets['code'] = '200';
			$rets['response'] = $get;					
			$this->response($rets);	
		}
		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}

	public function get_datagroup_post()
	{
		$rets = array();
		$iduser = $this->post('id_user');
		if ($this->get('access_token') != '') {
			$user_utc = $this->post('user_utc');
			$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";

			$server_utc = $this->Rumus->getGMTOffset();
			if($user_device == 'mobile'){
				$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
			}
			$interval   = $user_utc - $server_utc;

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }	
			// $get = $this->db->query("SELECT trg.*, tu_tutor.user_name as tutor_name, tu_requester.user_name as requester_name, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu_tutor ON trg.tutor_id=tu_tutor.id_user INNER JOIN tbl_user as tu_requester ON trg.id_user_requester=tu_requester.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.id_friends LIKE '%\"$iduser\",\"status\":0%' && approve=0 ORDER BY datetime DESC")->result_array();
			$get = $this->db->query("SELECT trg.*, DATE_ADD(trg.date_requested, INTERVAL $interval minute) as date_requested_utc, mc.nicename as country, tu.user_name, tu.user_image, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu ON trg.tutor_id=tu.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id INNER JOIN master_country as mc ON mc.nicename = tu.user_nationality WHERE trg.id_user_requester = '$iduser' ORDER BY datetime DESC")->result_array();			

			if (empty($get)) {
				$rets['status'] = false;
				$rets['code'] = '300';
				$rets['message'] = "gagal";
				$rets['response'] = $get;
				$this->response($rets);
			}
			foreach ($get as $key => $value) {
				$get[$key]['id_friends'] = json_decode($value['id_friends'], true);
			}
			$rets['status'] = true;
			$rets['code'] = '200';
			$rets['response'] = $get;					
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}

	public function get_dataprivateclass_post()
	{
		$rets = array();
		$user = $this->post('id_user');
		if ($this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }	
			$get = $this->db->query("SELECT tr.*, tu.user_name, ms.subject_name FROM tbl_request as tr INNER JOIN tbl_user as tu ON tr.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tr.id_user_requester = '$user' ORDER BY datetime DESC")->result_array();

			if (empty($get)) {
				$rets['status'] = false;
				$rets['code'] = '300';
				$rets['message'] = "gagal";
				$rets['response'] = $get;
				$this->response($rets);
			}

			foreach ($get as $key => $value) {
				// $hasilkredit = number_format($v['harga_kelas'], 0, ".", ".");
                                     	                 
                $date = date_create($value['date_requested']);
                $datee = date_format($date, 'd/m/y');
                $hari = date_format($date, 'd');
                $tahun = date_format($date, 'Y');
                                                        
                $date = $datee;
                $sepparator = '/';
                $parts = explode($sepparator, $date);
                $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));
                $tutorname = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$value[tutor_id]'")->row_array();
                $get[$key]['user_name_tutor'] = $tutorname['user_name'];
				$get[$key]['date_requested'] = $hari.' '. $bulan. ' '.$tahun;
                $get[$key]['duration_requested'] = sprintf('%02d',$value['duration_requested']/60);   
                $get[$key]['time_requested'] = substr($value['date_requested'], 11);   
				$rets['status'] = true;
			}
			$rets['code'] = '200';
			$rets['response'] = $get;					
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}

	public function get_datagroupclass_post()
	{
		$rets = array();
		$user = $this->post('id_user');
		if ($this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }	
			$get = $this->db->query("SELECT trg.*, tu.user_name, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu ON trg.tutor_id=tu.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.id_user_requester = '$user' ORDER BY datetime DESC")->result_array();

			if (empty($get)) {
				$rets['status'] = false;
				$rets['code'] = '300';
				$rets['message'] = "gagal";
				$rets['response'] = $get;
				$this->response($rets);
			}

			foreach ($get as $key => $value) {
				// $hasilkredit = number_format($v['harga_kelas'], 0, ".", ".");
                                                      
                $date = date_create($value['date_requested']);
                $datee = date_format($date, 'd/m/y');
                $hari = date_format($date, 'd');
                $tahun = date_format($date, 'Y');
                                                        
                $date = $datee;
                $sepparator = '/';
                $parts = explode($sepparator, $date);
                $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

				$json_a = json_decode($value['id_friends'],true);
				// $par_get_nama = "";
				foreach ($json_a['id_friends'] as $k => $v) {
					$idf = $v['id_user'];
					// $par_get_nama.= " id_user='$idf' ||";
					$the_name = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$idf'")->row_array()['user_name'];
					$json_a['id_friends'][$k]['user_name'] = $the_name;
				}
				// $par_get_nama = rtrim($par_get_nama,'||');
				// $new_json_a = array("id_friends" => array());

				// $get[$key]['id_friends'][$key]['user_name'] = $aa['user_name'];   
				$get[$key]['date_requested'] = $hari.' '. $bulan. ' '.$tahun;
                $get[$key]['duration_requested'] = sprintf('%02d',$value['duration_requested']/60);   
                $get[$key]['time_requested'] = substr($value['date_requested'], 11);   
                $get[$key]['harga_kelas'] = number_format($value['harga_kelas'], 0, ".", ".");                                   
				// $get[$key]['id_friends'] = json_decode($value['id_friends'], true);
				$get[$key]['id_friends'] = $json_a;
				$rets['status'] = true;
			}
			$rets['code'] = '200';
			$rets['response'] = $get;					
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}

	public function get_friendinvitation_post()
	{
		$rets = array();
		$iduser = $this->post('id_user');
		$date = $this->post('date');
		if ($this->get('access_token') != '') {
			$user_utc = $this->post('user_utc');
			$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";

			$server_utc = $this->Rumus->getGMTOffset();
			if($user_device == 'mobile'){
				$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
			}
			$interval   = $user_utc - $server_utc;
			
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }	
			// $get = $this->db->query("SELECT trg.*, tu_tutor.user_name as tutor_name, tu_requester.user_name as requester_name, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu_tutor ON trg.tutor_id=tu_tutor.id_user INNER JOIN tbl_user as tu_requester ON trg.id_user_requester=tu_requester.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.id_friends LIKE '%\"$iduser\",\"status\":0%' && approve=0 ORDER BY datetime DESC")->result_array();
			$get = $this->db->query("SELECT trg.*, mc.nicename as country, tu_tutor.user_name, tu_tutor.user_image, ms.subject_name, tu_tutor.user_name as tutor_name, tu_requester.user_name as requester_name, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu_tutor ON trg.tutor_id=tu_tutor.id_user INNER JOIN master_country as mc ON mc.nicename=tu_tutor.user_nationality INNER JOIN tbl_user as tu_requester ON trg.id_user_requester=tu_requester.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.id_friends LIKE '%".$iduser."%' AND trg.id_user_requester not like '%".$iduser."%' ORDER BY trg.datetime DESC")->result_array();

			if (empty($get)) {
				$rets['status'] = false;
				$rets['code'] = '300';
				$rets['message'] = "gagal";
				$rets['response'] = $get;
				$this->response($rets);
			}
			foreach ($get as $key => $value) {
				$datereq 	= $value['date_requested'];
				          
	            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$datereq);
	            $tanggal->modify("+".$interval ." minutes");
	            $tanggal            = $tanggal->format('Y-m-d H:i:s');

				$get[$key]['id_friends'] = json_decode($value['id_friends'], true);
				$get[$key]['date_requested_utc'] = $tanggal;
				$we = $get[$key]['id_friends']['id_friends'];
				foreach($we as $p => $vas)
				{
				    $s = $vas['id_user'];
				    $aa = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$s'")->row_array();
				    $get[$key]['id_friends']['id_friends'][$p]['nama'] = $aa['user_name'];
				} 
			}
			$rets['status'] = true;
			$rets['code'] = '200';
			$rets['response'] = $get;					
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = 'Field incomplete';
		ending:
		$this->response($rets);
	}

	public function get_classdesc_post()
	{
		$rets = array();
		$class_id = $this->post('class_id');
		$date = $this->post('date');	
		// $this->response($date);
		// return false;	
		$user_utc = $this->post('user_utc');
		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";

		$server_utc = $this->Rumus->getGMTOffset();
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		$interval   = $user_utc - $server_utc;
		$todayInClient = $date." 00:00:00";
		$enddayInClient = $date." 23:59:59";

		$thisBeginDay = DateTime::createFromFormat('Y-m-d H:i:s',$todayInClient);
		$thisBeginDay->modify("-$interval minute");
		$thisBeginDay = $thisBeginDay->format('Y-m-d H:i:s');

		$thisEndDay = DateTime::createFromFormat('Y-m-d H:i:s',$enddayInClient);
		$thisEndDay->modify("-$interval minute");
		$thisEndDay = $thisEndDay->format('Y-m-d H:i:s');

		$oneDayAfter = DateTime::createFromFormat('Y-m-d H:i:s',$thisEndDay);
		$oneDayAfter->modify("+1 day");
		$oneDayAfter = $oneDayAfter->format('Y-m-d H:i:s');

		$oneDayBefore = DateTime::createFromFormat('Y-m-d H:i:s',$thisBeginDay);
		$oneDayBefore->modify("-1 day");
		$oneDayBefore = $oneDayBefore->format('Y-m-d H:i:s');

		// $get = $this->db->query("SELECT trg.*, tu_tutor.user_name as tutor_name, tu_requester.user_name as requester_name, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu_tutor ON trg.tutor_id=tu_tutor.id_user INNER JOIN tbl_user as tu_requester ON trg.id_user_requester=tu_requester.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.id_friends LIKE '%\"$iduser\",\"status\":0%' && approve=0 ORDER BY datetime DESC")->result_array();
		$get = $this->db->query("SELECT tc.subject_id, tc.name, tc.description, tc.participant, tc.tutor_id, tc.start_time, tc.finish_time, tc.channel_id, tc.class_type, tc.template_type, tu.user_name, tu.email, tu.user_gender, tu.user_age, tu.user_image, tc.channel_status, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user WHERE tc.class_id='$class_id'")->result_array();			

		if (empty($get)) {
			$rets['status'] = false;
			$rets['code'] = '300';
			$rets['message'] = "gagal";
			$rets['data'] = $get;
			$this->response($rets);
		}
		foreach ($get as $key => $value) {
			if ($value['class_type'] == "multicast_channel_paid") {
				$channelid 							= $value['channel_id'];
				$getdata 							= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$channelid'")->row_array();
				$get[$key]['channel_name'] 			= $getdata['channel_name'];	
				$get[$key]['channel_description'] 	= $getdata['channel_description'];
				$get[$key]['channel_email'] 		= $getdata['channel_email'];
				$get[$key]['channel_number'] 		= $getdata['channel_country_code'].' '.$getdata['channel_callnum'];

				$listProgram = $this->db->query("SELECT * FROM master_channel_program")->result_array();

            	$program_id = "";
			    foreach ($listProgram as $keyo => $vl) {        
			        $schedule = $vl['schedule'];
			        $json_schedule      = json_decode($schedule,true);
			        $co = 0;
			        foreach ($json_schedule as $keya => $v) {            
			            if($v['class_id'] == $class_id){
			                $co = 1;
			                break;
			            }   
			        }
			        if ($co == 1) {					            
			            $program_id = $vl['program_id'];
			            break;
			        }
			    }
			    $getProgramData = $this->db->query("SELECT * FROM tbl_price_program WHERE program_id = '$program_id'")->row_array();
			    $get[$key]['program_list_id'] 		= $getProgramData['list_id'];
			    $get[$key]['program_price'] 		= $getProgramData['price'];
			    $get[$key]['program_name'] 			= $getProgramData['name'];
			    $get[$key]['program_description'] 	= $getProgramData['description'];
			    $get[$key]['program_type'] 			= $getProgramData['type'];
			    $get[$key]['program_poster'] 		= $getProgramData['poster'];
			    $get[$key]['program_poster_mobile'] = $getProgramData['poster_mobile'];
			    $get[$key]["program_bidang"] 		= json_decode($getProgramData['bidang'],true);
				$get[$key]["program_fasilitas"] 	= json_decode($getProgramData['fasilitas'],true);
				$get[$key]["program_jenjang_id"] 	= json_decode($getProgramData['jenjang_id'],true);
			}
			$subjectid = $value['subject_id'];
			$getjenjangg = $this->db->query("SELECT jenjang_id FROM master_subject WHERE subject_id='$subjectid'")->row_array()['jenjang_id'];
			$jenjangnamelevel = array();
			$jenjang_id = array();
			$jenjangid = json_decode($getjenjangg, TRUE);
			$myjenjang = $this->session->userdata('jenjang_id');
			$statusall = null;
			if ($jenjangid != NULL) {
				if(is_array($jenjangid)){
					$listclass = "20,15,16,17,18,19,1,2,3,4,5,9,12,6,10,13,7,11,14,8";
					foreach ($jenjangid as $keyy => $valuee) {	
						$cats = explode(",", $listclass);
						foreach($cats as $cat) {		    
						    if ($cat == $valuee) {
						    	$statusall = 'Semua Kelas';
						    }
						    else
						    {
						    	$statusall = null;
						    }
						}						
						$jenjang_id[] = $valuee;
						if ($myjenjang == $valuee) {																				
							$get[$key]['jenjang_idd'] = $valuee;
						}
						$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$valuee'")->row_array();								
						// $data[$row]['jenjang_level'] = $getAllSubject['jenjang_level'];		
						if ($getAllSubject['jenjang_level']=="0") {
							$jenjangnamelevel[] = $getAllSubject['jenjang_name'];							
						}
						else{
							$jenjangnamelevel[] = $getAllSubject['jenjang_name'].' '.$getAllSubject['jenjang_level'];								
						}
						$get[$key]['jenjangnamelevel'] = implode(", ", $jenjangnamelevel);
						$get[$key]['jenjang_id'] = implode(", ", $jenjang_id);
					}
				}
				else
				{

					$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$jenjangid'")->row_array();
					// $data[$row]['jenjang_name'] = $getAllSubject['jenjang_name'];
					// $data[$row]['jenjang_level'] = $getAllSubject['jenjang_level'];
					if ($getAllSubject['jenjang_level']=="0") {
						$jenjangnamelevel[] = $getAllSubject['jenjang_name'];							
					}
					else{
						$jenjangnamelevel[] = $getAllSubject['jenjang_name'].' '.$getAllSubject['jenjang_level'];								
					}
					$jenjang_id[] = $jenjangid;
					$get[$key]['jenjangnamelevel'] = implode(", ", $jenjangnamelevel);			
					$get[$key]['jenjang_id'] = implode(", ", $jenjang_id);
				}
			}
			// $get[$key]['jenjang_idd'] = $getjenjangg;
			$get['participant'] = json_decode($value['participant'],true);
			$get[$key]['status_all'] = $statusall;
			$decode = json_decode($value['participant'],true);
			if(isset($decode['participant'])){
				$length = count($decode['participant']);
				$get[$key]['participant_count'] = $length;
			}
			else
			{
				$get[$key]['participant_count'] = 0;
			} 
		}
		$rets['status'] = true;
		$rets['code'] = '200';
		$rets['data'] = $get;	
		$rets['message'] = "sukses";	
		$rets['utc'] = $user_utc;
		$this->response($rets);
	}


	public function get_classdesc_android_post()
	{
		$rets = array();
		$class_id = $this->post('class_id');
		$date = $this->post('date');	
		// $this->response($date);
		// return false;	
		$user_utc = $this->post('user_utc');
		$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";

		$server_utc = $this->Rumus->getGMTOffset();
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		$interval   = $user_utc - $server_utc;
		$todayInClient = $date." 00:00:00";
		$enddayInClient = $date." 23:59:59";

		$thisBeginDay = DateTime::createFromFormat('Y-m-d H:i:s',$todayInClient);
		$thisBeginDay->modify("-$interval minute");
		$thisBeginDay = $thisBeginDay->format('Y-m-d H:i:s');

		$thisEndDay = DateTime::createFromFormat('Y-m-d H:i:s',$enddayInClient);
		$thisEndDay->modify("-$interval minute");
		$thisEndDay = $thisEndDay->format('Y-m-d H:i:s');

		$oneDayAfter = DateTime::createFromFormat('Y-m-d H:i:s',$thisEndDay);
		$oneDayAfter->modify("+1 day");
		$oneDayAfter = $oneDayAfter->format('Y-m-d H:i:s');

		$oneDayBefore = DateTime::createFromFormat('Y-m-d H:i:s',$thisBeginDay);
		$oneDayBefore->modify("-1 day");
		$oneDayBefore = $oneDayBefore->format('Y-m-d H:i:s');

		// $get = $this->db->query("SELECT trg.*, tu_tutor.user_name as tutor_name, tu_requester.user_name as requester_name, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu_tutor ON trg.tutor_id=tu_tutor.id_user INNER JOIN tbl_user as tu_requester ON trg.id_user_requester=tu_requester.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.id_friends LIKE '%\"$iduser\",\"status\":0%' && approve=0 ORDER BY datetime DESC")->result_array();
		$get = $this->db->query("SELECT tc.subject_id, tc.name, tc.description, tc.participant, tc.channel_id, tc.tutor_id, tc.start_time, tc.finish_time, tc.class_type, tc.template_type, tu.user_name, tu.email, tu.user_gender, tu.user_age, tc.channel_status, mc.nicename as country, tu.user_image, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN master_country as mc ON mc.nicename = tu.user_nationality WHERE tc.class_id='$class_id'")->row_array();
		$harga = $this->db->query("SELECT *from tbl_class_price WHERE class_id ='".$class_id."' ")->row_array();
		
		if (empty($harga)) {
			$get['harga'] = "0";
		} else {
			$get['harga'] = $harga['harga_cm'];
		}

		if (empty($get)) {
			$rets['status'] = false;
			$rets['code'] = '300';
			$rets['message'] = "gagal";
			$rets['data'] = $get;
			$this->response($rets);
		}
		if ($get['class_type'] == "multicast_channel_paid") {
			$channelid 							= $get['channel_id'];
			$getdata 							= $this->db->query("SELECT * FROM master_channel WHERE channel_id='$channelid'")->row_array();
			$get['channel_name'] 				= $getdata['channel_name'];	
			$get['channel_description'] 		= $getdata['channel_description'];
			$get['channel_email'] 				= $getdata['channel_email'];
			$get['channel_number'] 				= $getdata['channel_country_code'].' '.$getdata['channel_callnum'];
		}
		$get['participant'] = json_decode($get['participant'],true);
		$rets['status'] = true;
		$rets['code'] = '200';
		$rets['data'] = $get;	
		$rets['message'] = "sukses";	
		$this->response($rets);
	}

	public function getMyAvailabilityTime_get()
	{

		$id_user = $this->get('id_user');
		$start_date = $this->get('start_date');
		$end_date = $this->get('end_date');
		$user_utc = $this->get('user_utc');
		
		$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);

		$server_utc = $this->Rumus->getGMTOffset();
		$interval = $user_utc - $server_utc;
		$start_date_asked = DateTime::createFromFormat('Y-m-d H:i:s', $start_date." 00:00:00" );
		$start_date_asked->modify("-".$interval ." minutes");
		$start_date_asked = $start_date_asked->format('Y-m-d H:i:s');

		$end_date_asked = DateTime::createFromFormat('Y-m-d H:i:s', $end_date." 23:59:59" );
		$end_date_asked->modify("-".$interval ." minutes");
		$end_date_asked = $end_date_asked->format('Y-m-d H:i:s');



		$res_q = $this->db->query("SELECT DATE_ADD(start_time, INTERVAL $interval MINUTE) as start_time, DATE_ADD(finish_time, INTERVAL $interval MINUTE) as finish_time FROM tbl_class WHERE tutor_id=$id_user AND start_time>='$start_date_asked' AND finish_time<='$end_date_asked' ")->result_array();

		$res['method'] = "getMyAvailabilityTime";
		$res['status'] = true;
		$res['response'] = $res_q;
		$this->response($res);
	}
	public function getTutorClasses_get($value='')
	{
		$tutor_id = $this->get('tutor_id');
		$start_date = $this->get('start_date');
		$end_date = $this->get('end_date');
		$user_utc = $this->get('user_utc');

		$user_device = $this->get('user_device') != '' ? $this->get('user_device') : "mobile";

		// $thisBeginDay = date($date.' 00:00:00', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
		// $oneDayAfter = date($date.' 23:59:59', strtotime('+'.$now.' day', strtotime(date('Y-m-d'))));
		

		$server_utc = $this->Rumus->getGMTOffset();
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}		
		$interval = $user_utc - $server_utc;
		$start_date_asked = DateTime::createFromFormat('Y-m-d H:i:s', $start_date." 00:00:00");
		$start_date_asked->modify("+".$interval ." minutes");
		$start_date_asked = $start_date_asked->format('Y-m-d H:i:s');

		$end_date_asked = DateTime::createFromFormat('Y-m-d H:i:s', $end_date." 23:59:59" );
		$end_date_asked->modify("-".$interval ." minutes");
		$end_date_asked = $end_date_asked->format('Y-m-d H:i:s');

		$list_classes = $this->db->query("SELECT tc.class_id,tc.participant, tc.subject_id,tc.name as subject_name, tc.description, tc.tutor_id, tu.user_name, tu.first_name, tc.template_type, tc.class_type, tu.user_image, tcp.harga, DATE_ADD(start_time, INTERVAL $interval MINUTE) as start_time , DATE_ADD(finish_time, INTERVAL $interval MINUTE) as finish_time, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM tbl_class as tc LEFT JOIN tbl_class_price as tcp ON tc.class_id=tcp.class_id INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user WHERE tc.tutor_id='$tutor_id' AND tc.start_time >='$start_date_asked' AND tc.finish_time<='$end_date_asked'")->result_array();
		if (!empty($list_classes)) {
			foreach ($list_classes as $key => $value) {
				$subjectid = $value['subject_id'];
				$getjenjangg = $this->db->query("SELECT jenjang_id FROM master_subject WHERE subject_id='$subjectid'")->row_array()['jenjang_id'];
				$jenjangnamelevel = array();
				$statusall = null;
				$jenjangid = json_decode($getjenjangg, TRUE);
				if ($jenjangid != NULL) {
					if(is_array($jenjangid)){
						$listclass = "20,15,16,17,18,19,1,2,3,4,5,9,12,6,10,13,7,11,14,8";
							
						foreach ($jenjangid as $keyy => $valuee) {
							$cats = explode(",", $listclass);
								foreach($cats as $cat) {		    
								    if ($cat == $value) {
								    	$statusall = 'Semua Kelas';
								    }
								    else
								    {
								    	$statusall = null;
								    }
								}						
								$jenjang_id[] = $valuee;
								if ($jenjang_id == $valuee) {																				
									$data[$row]['jenjang_idd'] = $valuee;
								}									
							$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$valuee'")->row_array();								
							// $data[$row]['jenjang_level'] = $getAllSubject['jenjang_level'];		
							if ($getAllSubject['jenjang_level']=="0") {
								$jenjangnamelevel[] = $getAllSubject['jenjang_name'];							
							}
							else{
								$jenjangnamelevel[] = $getAllSubject['jenjang_name'].' '.$getAllSubject['jenjang_level'];								
							}
							$list_classes[$key]['jenjangnamelevel'] = implode(", ", $jenjangnamelevel);
						}
					}
					else
					{
						$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$jenjangid'")->row_array();
						// $data[$row]['jenjang_name'] = $getAllSubject['jenjang_name'];
						// $data[$row]['jenjang_level'] = $getAllSubject['jenjang_level'];
						if ($getAllSubject['jenjang_level']=="0") {
							$jenjangnamelevel[] = $getAllSubject['jenjang_name'];							
						}
						else{
							$jenjangnamelevel[] = $getAllSubject['jenjang_name'].' '.$getAllSubject['jenjang_level'];								
						}	
						$list_classes[$key]['jenjangnamelevel'] = implode(", ", $jenjangnamelevel);			
					}
				}
				
				$get[$key]['status_all'] = $statusall;
			}

			$res['method'] = "getTutorClasses";
			$res['status'] = true;
			$res['response'] = $list_classes;
		}
		else
		{
			$res['method'] = "getTutorClasses";
			$res['status'] = false;
			$res['response'] = null;
		}
		
		$this->response($res);
	}
	public function getChannelClasses_get($value='')
	{
		$channel_id = $this->get('channel_id');
		$start_date = $this->get('start_date');
		$end_date = $this->get('end_date');
		$user_utc = $this->get('user_utc');

		$user_device = $this->get('user_device') != '' ? $this->get('user_device') : "mobile";

		// $thisBeginDay = date($date.' 00:00:00', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
		// $oneDayAfter = date($date.' 23:59:59', strtotime('+'.$now.' day', strtotime(date('Y-m-d'))));
		
		$server_utc = $this->Rumus->getGMTOffset();
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		$interval = $user_utc - $server_utc;
		$start_date_asked = DateTime::createFromFormat('Y-m-d H:i:s', $start_date." 00:00:00" );
		$start_date_asked->modify("-".$interval ." minutes");
		$start_date_asked = $start_date_asked->format('Y-m-d H:i:s');

		$end_date_asked = DateTime::createFromFormat('Y-m-d H:i:s', $end_date." 23:59:59" );
		$end_date_asked->modify("-".$interval ." minutes");
		$end_date_asked = $end_date_asked->format('Y-m-d H:i:s');

		$list_classes = $this->db->query("SELECT tc.class_id,tc.participant, tc.subject_id,tc.name as subject_name, tc.description, tc.tutor_id, tu.user_name, tu.first_name, tc.template_type, tc.class_type, tu.user_image, tcp.harga, DATE_ADD(start_time, INTERVAL $interval MINUTE) as start_time , DATE_ADD(finish_time, INTERVAL $interval MINUTE) as finish_time, LEFT(DATE_ADD(tc.start_time, INTERVAL $interval minute),10) as date, LEFT(DATE_ADD(tc.finish_time, INTERVAL $interval minute),10) as enddate, SUBSTR(DATE_ADD(tc.start_time, INTERVAL $interval minute),12,5) as time, SUBSTR(DATE_ADD(tc.finish_time, INTERVAL $interval minute),12,5) as endtime FROM tbl_class as tc LEFT JOIN tbl_class_price as tcp ON tc.class_id=tcp.class_id INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user WHERE tc.channel_id='$channel_id' AND tc.start_time >='$start_date_asked' AND tc.finish_time<='$end_date_asked' ORDER BY tc.start_time DESC")->result_array();

		if (!empty($list_classes)) {
			$res['method'] = "getChannelClasses";
			$res['status'] = true;
			$res['code'] = 200;
			$res['response'] = $list_classes;
		}
		else
		{
			$res['method'] = "getChannelClasses";
			$res['status'] = true;
			$res['code'] = 404;
			$res['response'] = null;
		}
		
		$this->response($res);
	}

	function getReportingPoint_post($value='')
	{
		$channel_id = $this->post('channel_id');
		$type = $this->post('type');

		if ($type == "channel") {				
			$list_point = $this->db->query("SELECT lcpt.*, tc.*, tu.user_name FROM log_channel_point_transaction as lcpt INNER JOIN tbl_class as tc ON lcpt.class_id=tc.class_id INNER JOIN tbl_user as tu ON tu.id_user=lcpt.tutor_creator WHERE lcpt.channel_id='$channel_id' ORDER BY lcpt.ptrx_id DESC")->result_array();
			if ($list_point) {
				$res['status'] = true;
				$new_arr = array();
				if(!empty($list_point)){
					foreach ($list_point as $key => $value) {
						$date = date_create($value['created_at']);
				        $datee = date_format($date, 'd/m/y');
				        $hari = date_format($date, 'd');
				        $tahun = date_format($date, 'Y');
				                                                
				        $date = $datee;
				        $sepparator = '/';
				        $parts = explode($sepparator, $date);
				        $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

						$list_point[$key]['participant'] = json_decode($value['participant'],true);
						$list_point[$key]['created_at_point'] = $hari.' '.$bulan.' '.$tahun;
						array_push($new_arr, $list_point[$key]);
					}
				}
				$res['message'] = 'successful';
				$res['data'] = $new_arr;
			}
			else
			{
				$res['status'] = false;
				$res['message'] = 'failed';
				$res['data'] = '';
			}			
		}
		else
		{
			$list_point = $this->db->query("SELECT lcpt.*, tc.*, tu.user_name FROM log_channel_point_transaction as lcpt INNER JOIN tbl_class as tc ON lcpt.class_id=tc.class_id INNER JOIN tbl_user as tu ON tu.id_user=lcpt.tutor_creator ORDER BY lcpt.ptrx_id DESC")->result_array();
			if ($list_point) {
				$res['status'] = true;
				$new_arr = array();
				if(!empty($list_point)){
					foreach ($list_point as $key => $value) {
						$date = date_create($value['created_at']);
				        $datee = date_format($date, 'd/m/y');
				        $hari = date_format($date, 'd');
				        $tahun = date_format($date, 'Y');
				                                                
				        $date = $datee;
				        $sepparator = '/';
				        $parts = explode($sepparator, $date);
				        $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

						$list_point[$key]['participant'] = json_decode($value['participant'],true);
						$list_point[$key]['created_at_point'] = $hari.' '.$bulan.' '.$tahun;
						array_push($new_arr, $list_point[$key]);
					}
				}
				$res['message'] = 'successful';
				$res['data'] = $new_arr;
			}
			else
			{
				$res['status'] = false;
				$res['message'] = 'failed';
				$res['data'] = '';
			}			
		}
		$this->response($res);
	}

	function getListPointChannel_post($value=''){
		$getlist = $this->db->query("SELECT mc.*, uc.* FROM master_channel as mc INNER JOIN uangpoint_channel as uc ON mc.channel_id=uc.channel_id")->result_array();
		if (!empty($getlist)) {
			$res['status'] = true;
			$res['message'] = 'success';
			$res['data'] = $getlist;
		}
		else
		{
			$res['status'] = false;
			$res['message'] = 'failed';
			$res['data'] = '';
		}
		$this->response($res);
	}
	function getListKuotaChannel_post($value=''){
		$getlist = $this->db->query("SELECT mc.*, mcp.* FROM master_channel as mc INNER JOIN master_channel_package as mcp ON mc.channel_id=mcp.channel_id")->result_array();
		if (!empty($getlist)) {
			$res['status'] = true;
			$res['message'] = 'success';
			$res['data'] = $getlist;
		}
		else
		{
			$res['status'] = false;
			$res['message'] = 'failed';
			$res['data'] = '';
		}
		$this->response($res);
	}

	function topupPointChannel_post($value=''){
		$channel_id = $this->post('channel_id');
		$amountpoint = $this->post('amountpoint');

		$getpoint = $this->db->query("SELECT * FROM uangpoint_channel WHERE channel_id='$channel_id'")->row_array();
		if (!empty($getpoint)) {
			$pointid  = $getpoint['point_id'];
			$pointnow = $getpoint['active_point'];
			$topupnow = $pointnow + $amountpoint;
			
			$updatepoint = $this->db->query("UPDATE uangpoint_channel SET active_point='$topupnow' WHERE point_id='$pointid'");
			if ($updatepoint) {
				$res['status'] = true;
				$res['message'] = 'success';
				$res['code'] = 200;
			}
			else
			{
				$res['status'] = false;
				$res['message'] = 'failed';
				$res['data'] = 300;
			}
		}
		$this->response($res);
	}

	function ChannelDetails_post($value=''){
		$channel_link = $this->post('channel_link');
		$get_details = $this->db->query("SELECT * FROM master_channel WHERE channel_link='$channel_link'")->row_array();
		if (!empty($get_details)) {
			$res['status'] = true;
			$res['message'] = 'success';
			$res['code'] = 200;
			$res['data'] = $get_details;
		}
		else
		{
			$res['status'] = false;
			$res['message'] = 'failed';
			$res['code'] = 300;
			$res['data'] = "";
		}
		$this->response($res);
	}

	function getMyChannel_post(){

		$id_user = $this->post('id_user');
		if ($this->get('access_token') != '' && $id_user != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	        }
			$get_details = $this->db->query("SELECT mc.*, mct.channel_id as id, mc.channel_name as text FROM master_channel_tutor as mct INNER JOIN master_channel as mc ON mct.channel_id=mc.channel_id WHERE mct.tutor_id ='$id_user'")->result_array();
			if (!empty($get_details)) {
				$res['status'] = true;
				$res['message'] = 'success';
				$res['code'] = 200;
				$res['data'] = $get_details;
				$this->response($res);
			}
			else
			{
				$res['status'] = false;
				$res['message'] = 'failed';
				$res['code'] = 300;
				$res['data'] = "";
				$this->response($res);
			}
		}
		$res['status'] = false;
		$res['message'] = 'Field incomplete';
		ending:
		$this->response($res);
	}

	function getStudentChannel_post(){

		$channel_id = $this->post('channel_id');
		if ($this->get('access_token') != '' && $channel_id != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	        }
			$get_details = $this->db->query("SELECT id_user, user_name, email FROM tbl_user WHERE id_user IN (SELECT id_user FROM master_channel_student where channel_id = {$channel_id}) AND usertype_id='student' ORDER BY user_name ASC")->result_array();
			if (!empty($get_details)) {
				$res['status'] = true;
				$res['message'] = 'success';
				$res['code'] = 200;
				$res['data'] = $get_details;
				$this->response($res);
			}
			else
			{
				$res['status'] = false;
				$res['message'] = 'failed';
				$res['code'] = 300;
				$res['data'] = "";	
				$this->response($res);
			}

		}

		$res['status'] = false;
		$res['message'] = 'Field incomplete';
		ending:
		$this->response($res);
		
	}

	function channel_ListClass_post(){

		$tutorid = $this->post('tutor_id');
		if ($this->get('access_token') != '' && $tutorid != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			$get_list = $this->db->query("SELECT tc.*, mc.channel_name, mc.channel_logo FROM tbl_class as tc INNER JOIN master_channel as mc ON tc.channel_id=mc.channel_id WHERE tc.tutor_id='$tutorid' AND tc.channel_id != 0 AND ( tc.start_time>=now() OR ( tc.start_time<now() AND tc.finish_time>now() ) )")->result_array();
			if (!empty($get_list)) {
				$res['status'] = true;
				$res['message'] = 'success';
				$res['code'] = 200;
				foreach ($get_list as $key => $value) {
					$get_list[$key]['participant'] = json_decode($value['participant'],true);
				}
				$res['data'] = $get_list;
				$this->response($res);
			}
			else
			{
				$res['status'] = false;
				$res['message'] = 'failed';
				$res['code'] = 300;
				$res['data'] = "";
				$this->response($res);
			}
		}
		$res['status'] = false;
		$res['message'] = 'Field incomplete';
		ending:
		$this->response($res);
		
	}

	function channel_addStudentClass_post()
	{
		$participantt 	= $this->post('idtage');
		$class_id 		= $this->post('class_id');
		$channel_id 	= $this->post('channel_id');

		$getparticipant = $this->db->query("SELECT class_type,participant FROM tbl_class WHERE class_id='$class_id'")->row_array();

		// foreach ($participantt as $key => $value) {
		// 	echo $value;
		// }
		if ($getparticipant['class_type'] == "private_channel") {
			$decode = json_decode($getparticipant['participant'],true);
			$length = count($decode['participant']);			
			if ($length == 1) {
				$res['status'] = false;
				$res['message'] = 'Maaf, kelas private maksimal 1 siswa dalam 1 kelas.';
				$res['code'] = 405;
				$res['data'] = "";
			}
			else
			{
				$participant_arr = json_decode($getparticipant['participant'],true);
				if(!array_key_exists('participant', $participant_arr)){
					$participant_arr['participant'] = array();
				}
				// foreach ($participantt as $key => $id) {
					array_push($participant_arr['participant'], array("id_user" => $participantt));
				// }
				$participant_arr = json_encode($participant_arr);
				$update = $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
				if ($update) {
					$res['status'] = false;
					$res['message'] = 'success';
					$res['code'] = 200;
					$res['data'] = "";
				}
				else
				{
					$res['status'] = false;
					$res['message'] = 'failed';
					$res['code'] = 404;
					$res['data'] = "";
				}
			}
		}
		else if ($getparticipant['class_type'] == "group_channel") {
			$decode = json_decode($getparticipant['participant'],true);
			$length = count($decode['participant']);
			
			if ($length >= 4) {
				$res['status'] = false;
				$res['message'] = 'Maaf, kelas private maksimal 4 siswa dalam 1 kelas.';
				$res['code'] = 406;
				$res['data'] = "";
			}
			else
			{
				$participant_arr = json_decode($getparticipant['participant'],true);
				if(!array_key_exists('participant', $participant_arr)){
					$participant_arr['participant'] = array();
				}
				// foreach ($participantt as $key => $id) {
					array_push($participant_arr['participant'], array("id_user" => $participantt));
				// }
				$participant_arr = json_encode($participant_arr);
				$update = $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
				if ($update) {
					$res['status'] = false;
					$res['message'] = 'success';
					$res['code'] = 200;
					$res['data'] = "";
				}
				else
				{
					$res['status'] = false;
					$res['message'] = 'failed';
					$res['code'] = 404;
					$res['data'] = "";
				}
			}
		}
		else
		{
			$participant_arr = json_decode($getparticipant['participant'],true);
			if(!array_key_exists('participant', $participant_arr)){
				$participant_arr['participant'] = array();
			}
			// foreach ($participantt as $key => $id) {
				array_push($participant_arr['participant'], array("id_user" => $participantt));
			// }
			$participant_arr = json_encode($participant_arr);
			$update = $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
			// print_r($participant_arr);
			// return false;
			if ($update) {
				$res['status'] = false;
				$res['message'] = 'success';
				$res['code'] = 200;
				$res['data'] = "";
			}
			else
			{
				$res['status'] = false;
				$res['message'] = 'failed';
				$res['code'] = 404;
				$res['data'] = "";
			}
		}
		$this->response($res);
	}

	function channel_Showlist_post()
	{
		$class_id = $this->post('class_id');
		// $channel_id = $this->post('channel_id');
		// $this->session->set_userdata('idchannel', $channel_id);
		// $this->session->set_userdata('idclass', $class_id);

		$getparticipant = $this->db->query("SELECT tbl_class.participant FROM tbl_class WHERE class_id='$class_id'")->row_array();
		if (!empty($getparticipant)) {
			// $json = array("status" => 1,"data" => json_decode($getparticipant['participant']));
			$res['status'] = true;
			$res['message'] = 'failed';
			$res['code'] = 200;
			$res['data'] = json_decode($getparticipant['participant']);
		}
		else
		{
			$res['status'] = false;
			$res['message'] = 'failed';
			$res['code'] = 300;
			$res['data'] = "";
		}
		$this->response($res);
	}

	function channel_UpdateParticipant_post()
	{
		$class_id = $this->post('class_id');
		$participantt = $this->post('participant');
		$participant['visible'] = "private";
		$participant['participant'] = array();

		if ($this->get('access_token') != '' && $class_id != ''){
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
			// $participant['participant']['id_user'] = $participantt;
			foreach($participantt as $key => $id) {
				$participant['participant'][$key]['id_user'] = $id;
			}
			$participant = json_encode($participant);
			// print_r($participant);
			// return false;
			
			$update = $this->db->simple_query("UPDATE tbl_class SET participant='$participant' WHERE class_id='$class_id'");		
			if ($update) {
				$res['status'] = true;
				$res['message'] = 'Success';
				$res['code'] = 200;
				$this->response($res);
			}
			
			else
			{
				$res['status'] = false;
				$res['message'] = 'failed';
				$res['code'] = 300;
				$this->response($res);
			}
		}

		$res['status'] = false;
		$res['message'] = 'Field incomplete';
		ending:
		$this->response($res);
	}

	function activityChannel_post()
	{

		$gdataactivity = $this->db->query("SELECT lc.*, mc.channel_name, mc.channel_email FROM master_channel as mc INNER JOIN log_channel as lc ON mc.channel_id=lc.channel_id ORDER BY created_at ASC")->result_array();
		
		if (!empty($gdataactivity)) {
			$res['status'] = true;
			$res['message'] = 'Success';
			foreach ($gdataactivity as $key => $value) {
				$date = date_create($value['created_at']);
		        $datee = date_format($date, 'd/m/y');
		        $hari = date_format($date, 'd');
		        $tahun = date_format($date, 'Y');
		        $hour = date_format($date, 'H');
		        $minute = date_format($date, 'i');
		        $secods = date_format($date, 's');
		                                                
		        $date = $datee;
		        $sepparator = '/';
		        $parts = explode($sepparator, $date);
		        $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

				$getname = $this->db->query("SELECT email,user_name,id_user FROM tbl_user WHERE id_user='$value[data]'")->result_array();
				if ($value['data'] != null) {					
					$gdataactivity[$key]['profile'] = $getname;
				}
				else
				{
					$gdataactivity[$key]['profile'] = null;
				}
				$gdataactivity[$key]['created_at_point'] = $hari.' '.$bulan.' '.$tahun;
				$gdataactivity[$key]['time_created_at'] = $hour.':'.$minute.':'.$secods;
				$gdataactivity[$key]['data'] = $value['data'];
			}
			$res['data'] = $gdataactivity;
		}
		
		else
		{
			$res['status'] = false;
			$res['message'] = 'failed';
			$res['code'] = 300;
		}
		$this->response($res);
	}

	function VerifyTransaction_post(){
		$trx_id = $this->post('trx_id');
		$trf_amount = $this->post('trf_amount');
		$berita = $this->post('berita');
		$description = $this->post('description');
		$buyer_norek = $this->post('buyer_norek');
		$buyer_name = $this->post('buyer_name');
		$id_user_requester = $this->post('id_user_requester');
		$status = $this->post('status');

		$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
        }
		$insertverify = $this->db->query("INSERT INTO log_op_verification (trx_id, trf_amount, berita, description, buyer_norek, buyer_name, created_at) VALUES ('$trx_id','$trf_amount','$berita','$description','$buyer_norek','$buyer_norek',now())");

		if ($insertverify) {
			$upd 	= $this->db->query("UPDATE log_trf_confirmation SET accepted='1' WHERE trx_id='$trx_id'");			
    		if ($upd) {
	    		$this->db->query("UPDATE log_transaction SET flag='1' WHERE trx_id='$trx_id'");
	    	}	

	    	if ($status == "private" || $status == "group" || $status == "multicast") {
	    		$this->db->query("UPDATE tbl_order SET order_status=1 WHERE order_id='$trx_id'");
	    		$res['status'] = true;
				$res['message'] = 'success verify';
				$res['code'] = 200;			
				$this->response($res);
	    	}
	    	else
	    	{
		    	$getdatatransaction = $this->db->query("SELECT lt.*, tu.email, tu.user_name FROM log_transaction as lt INNER JOIN tbl_user as tu ON lt.id_user=tu.id_user WHERE trx_id='$trx_id'")->row_array();
		    	$nominal		= $getdatatransaction['credit'];
		    	$hasilconvert 	= number_format($nominal, 0, ".", ".");
		    	$email_tujuan	= $getdatatransaction['email'];
		    	$username 		= $getdatatransaction['user_name'];

	               
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_indiclass_VerifyTransaction','content' => array("username" => "$username", "trx_id" => "$trx_id", "hasilconvert" => "$hasilconvert", "email_tujuan" => "$email_tujuan") )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
	            

				$res['status'] = true;
				$res['message'] = 'success verify';
				$res['code'] = 200;			
				$this->response($res);
				
	    	}	
			
		}
		else
		{
			$res['status'] = false;
			$res['message'] = 'failed';
			$res['code'] = 300;			
			$this->response($res);
		}

		$res['status'] = false;
		$res['message'] = 'Field incomplete';
		ending:
		$this->response($res);
	}

	public function GetDataVerify_post(){
		$trx_id = $this->post('trx_id');
		$status = $this->post('status');
		$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
        }

        if ($status == "private" || $status == "group") {
        	$get = $this->db->query("SELECT * FROM tbl_order WHERE order_id='$trx_id'")->row_array();
	        if (!empty($get)) {
	        	$credit = number_format($get['total_price'], 0, ".", ".");
				$res = array("status" => true, "code" => 200, "message" => $credit);
				$this->response($res);	
	        }
	        else
			{
				$res['status'] = false;
				$res['message'] = 'failed';
				$res['code'] = 300;			
				$this->response($res);
			}
        }
        else
        {
        	$get = $this->db->query("SELECT * FROM log_transaction WHERE trx_id='$trx_id'")->row_array();
	        if (!empty($get)) {
	        	$credit = number_format($get['credit'], 0, ".", ".");
				$res = array("status" => true, "code" => 200, "message" => $credit);
				$this->response($res);	
	        }
	        else
			{
				$res['status'] = false;
				$res['message'] = 'failed';
				$res['code'] = 300;			
				$this->response($res);
			}
        }
        
        $res['status'] = false;
		$res['message'] = 'Field incomplete';

		ending:
		$this->response($res);
	}

	public function my_ended_class_post(){
		$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
        }

		$at_least_one = 0;
  		$id_userr = $this->post('id_user');
  		$startdate = $this->post('startdate');
        $now = date('Y-m-d H:i:s');
        $query_sche = $this->db->query("SELECT tc.class_id, tc.participant, tc.subject_id, tc.name, tc.description, tc.tutor_id, tu.first_name, tu.user_image, tc.start_time, tc.finish_time FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user where start_time >='$startdate' AND finish_time <= '$now' && break_pos=0 && break_state = 0 ORDER BY finish_time DESC")->result_array();
        $all_schedule = array();

		$rets['status'] = true;
		$rets['message'] = "Succeed";
		$new_arr = array();
		if(!empty($query_sche)){
			foreach ($query_sche as $key => $value) {
				$query_sche[$key]['participant'] = json_decode($value['participant'],true);
				array_push($new_arr, $query_sche[$key]);
			}
			$rets['data'] = $new_arr;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['message'] = "failed";
			$rets['data'] = null;
			$this->response($rets);
		}
        
        $res['status'] = false;
		$res['message'] = 'Field incomplete';
		ending:
		$this->response($res);
	}

	public function get_tutorprice_post(){
		$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
        }

        $id_user = $this->post('id_user');
        $select_all = $this->db->query("SELECT tb.*, ms.* FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id WHERE tb.id_user = '$id_user' AND tb.tutor_id='0' order by ms.subject_id ASC")->result_array();
        if (!empty($select_all)) {
        	foreach ($select_all as $key => $v) {
        		$jenjang_name = array();
        		$jenjang_level = array();
                $jenjangid = json_decode($v['jenjang_id'], TRUE);
                if ($jenjangid != NULL) {
                    if(is_array($jenjangid)){
                        foreach ($jenjangid as $keyy => $value) {                                    
                            $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
                            $jenjang_name[] = $selectnew['jenjang_name'];
                            $jenjang_level[] = $selectnew['jenjang_level'];                                                     
                        }   
                    }else{
                        $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
                        $jenjang_name[] = $selectnew['jenjang_name'];
                       	$jenjang_level[] = $selectnew['jenjang_level'];                                                
                    }                                       
                }

				$data = $this->db->query("SELECT ( SELECT harga FROM tbl_service_price WHERE subject_id='".$v['subject_id']."' AND tutor_id='$id_user' AND class_type='private' ) as private_harga, ( SELECT harga FROM tbl_service_price WHERE subject_id='".$v['subject_id']."' AND tutor_id='$id_user' AND class_type='group' ) as group_harga ")->row_array();
				$select_all[$key]['private_harga'] = $data['private_harga'];
				$select_all[$key]['group_harga'] = $data['group_harga'];
				$select_all[$key]['jenjang_name'] = implode(", ",$jenjang_name);
				$select_all[$key]['jenjang_level'] = implode(", ",$jenjang_level);
			}
			$rets['status'] = true;
			$rets['message'] = "success";
			$rets['data'] = $select_all;
			$this->response($rets);
        }

        $res['status'] = false;
		$res['message'] = 'Field incomplete';
		ending:
		$this->response($res);
	}
	public function submit_addt_info_post()
	{
		$res['method'] = "submit_addt_info";
		$res['transaction'] = $this->Rumus->RandomString();

		$lat = $this->post('lat');
		$lng = $this->post('lng');
		$manufacture = $this->post('manufacture');
		$model = $this->post('model');
		$api_android = $this->post('api_android');
		$mac_addrs1 = $this->post('mac_addrs1');
		$mac_addrs2 = $this->post('mac_addrs2');

		$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
            // $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
        }

		if($lat != '' && $lng != '' && $manufacture != '' && $model != '' && $api_android != ''){
			$id_user = $retok['id_user'];

			if(!$this->db->simple_query("INSERT INTO tbl_user_addt_info(id_user, lat, lng, manufacture, model, api_android, macaddrs1, macaddrs2) VALUES($id_user, $lat, $lng, '$manufacture', '$model', '$api_android', '$mac_addrs1', '$mac_addrs2')")){
				$error = $this->db->error(); // Has keys 'code' and 'message'
				$res['status'] = false;
				$res['message'] = $error['message'];
				$this->response($res);
			}

			$res['status'] = true;
			$res['message'] = 'Successful.';

			$this->response($res);
		}

		$res['status'] = false;
		$res['message'] = "Field incomplete";
		ending:
		$this->response($res);
	}

	public function iamonline_post(){
		$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
        }

        $id_user = $this->post('id_user');
        $select_all = $this->db->query("SELECT * from temp_user_online where id_user = '".$id_user."' ")->row_array();
        if (!empty($select_all)) {
	        $res['status'] = false;
			$res['message'] = 'You in class';
			$res['code'] = 300;	
			$this->response($res);

        } else {
	        $res['status'] = true;
			$res['message'] = 'Welcome to class';
			$res['code'] = 200;	
			$this->response($res);
        }

        $res['status'] = false;
		$res['message'] = 'Field incomplete';
		$res['code'] = -300;	
		ending:
		$this->response($res);
	}

	public function getProvinsiDLL_post()
	{
		if (isset($_POST['provinsi_id'])) {
			$provinsi_id 	= $this->post('provinsi_id');
			$result = $this->db->query("SELECT * FROM master_kabupaten WHERE provinsi_id='$provinsi_id' ")->result_array();
		}
		else if(isset($_POST['kabupaten_id'])){
			$kabupaten_id 	= $this->post('kabupaten_id');
			$result = $this->db->query("SELECT * FROM master_kecamatan WHERE kabupaten_id='$kabupaten_id' ")->result_array();
		}
		else if(isset($_POST['kecamatan_id'])){
			$kecamatan_id 	= $this->post('kecamatan_id');
			$result = $this->db->query("SELECT * FROM master_desa WHERE kecamatan_id='$kecamatan_id' ")->result_array();
		}
		else
		{
			$result = $this->db->query("SELECT * FROM master_provinsi")->result_array();
		}
		$res['status'] = true;
		$res['message'] = 'Field complete';
		$res['code'] = 200;
		$res['data'] = $result;	
		$this->response($res);	
	}

	public function getSchoolMaster_post()
	{
		
		$jenjang 	= $this->post('jenjang');
		$result = $this->db->query("SELECT * FROM master_school where school_name like '{$jenjang}%'")->result_array();
		
		$res['status'] = true;
		$res['message'] = 'Field complete';
		$res['code'] = 200;
		$res['data'] = $result;	
		$this->response($res);	
	}

	public function tutor_approval_android_post()
	{

		$field_param = $this->post('field_param');
		$value_param = $this->post('value_param');
		$type = $this->post('type');
		$new_param = "";

		// print_r($this->post());
		// return false;

		if($field_param != '' && $value_param != '' && $type != ''){
			$retok = $this->verify_access_token($this->get('access_token'));
			if ($this->get('access_token') != '') {
				$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
	            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
	                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
	                // $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	            }
	        }

	        $id_user = $retok['id_user'];

			if(!is_array($field_param)){
				$rets = array("a"=>$field_param, "status" => false, "code" => 2, "message" => "False. parameter should be in array.");
				goto ending;
			}
			if($type == 'form_account') {
				$user_image_indexkey = -1;
				foreach ($field_param as $key => $value) {
					if($value != 'user_image'){
						$v_value = $this->db->escape_str($value_param[$key]);
						$new_param .= "$value='$v_value', ";
					}
					if($value == 'user_image'){
						$user_image_indexkey = $key;
					}
				}
				if($user_image_indexkey != -1){
					// $image = 'data:image/jpeg;base64,' .str_replace(' ','+',$value_param[$user_image_indexkey]);
					// file_put_contents("aset/img/user/".$id_user.".jpg",file_get_contents($image));

					// $config['image_library'] = 'gd2';
					// $config['source_image']	= "aset/img/user/".$id_user.".jpg";
					// $config['width']	= 250;
					// $config['maintain_ratio'] = FALSE;
					// $config['height']	= 250;

					// $this->image_lib->initialize($config); 

					// $this->image_lib->resize();

		            $image = 'data:image/jpeg;base64,' .str_replace(' ','+',$value_param[$user_image_indexkey]); //replacing ' ' with '+'               
		                         
		            //Write to disk
		            file_put_contents("aset/_temp_images/".$id_user.".jpg",file_get_contents($image));
		            
		            $this->Rumus->sendToCDN('user', $id_user.".jpg", null, 'aset/_temp_images/'.$id_user.".jpg" );
					$new_param .= "user_image='".base64_encode('user/'.$id_user.".jpg")."', ";
					// $new_param .= "user_image='".$id_user.".jpg', ";
					// $this->session->set_userdata('user_image', $id_user.".jpg");
				}
				$new_param = rtrim($new_param,', ');
				
				if(!$this->db->simple_query("UPDATE tbl_user SET $new_param WHERE id_user=$id_user")){
					$rets = array("a"=>"UPDATE tbl_user SET $new_param WHERE id_user=$id_user", 
						"status" => false, "code" => -1, "message" => "an error occurred.");
					goto ending;
				}
				$rets = array("status" => true, "code" => 0, "message" => "Successfully.");

			}else if($type == 'form_profile') {
				$edu_array = array();
				$exp_array = array();
				$new_param_insert = ", ";
				$new_value_insert = ", ";
				// print_r($this->post());
				// return false;
				foreach ($field_param as $key => $value) {
					if($value == 'educational_level' || $value == 'education_background'){
						if(is_array($value_param[$key])){
							// foreach ($value_param[$key] as $k => $v) {
							// 	$edu_array[$k]['educational_level'] = $value[$key]['educational_level'][$k];
							// 	$edu_array[$k]['educational_competence'] = $value[$key]['educational_competence'][$k];
							// 	$edu_array[$k]['educational_institution'] = $value[$key]['educational_institution'][$k];
							// 	$edu_array[$k]['institution_address'] = $value[$key]['institution_address'][$k];
							// 	$edu_array[$k]['graduation_year'] = $value[$key]['graduation_year'][$k];
							// }
							$edu_array = $value_param[$key];
						}
						$edu_array = serialize($edu_array);
						$v_value = $this->db->escape_str($edu_array);
						$new_param .= "$value='$v_value', ";
						$new_param_insert .= $value.", ";
						$new_value_insert .= "'$v_value', ";
						
					} else if($value == 'description_experience' || $value == 'teaching_experience'){
						if(is_array($value_param[$key])){
							/*foreach ($value_param[$key]['description_experience'] as $k => $v) {
								$exp_array[$k]['description_experience'] = $form_profile['description_experience'][$k];
								$exp_array[$k]['year_experience1'] = $form_profile['year_experience1'][$k];
								$exp_array[$k]['year_experience2'] = $form_profile['year_experience2'][$k];
								$exp_array[$k]['place_experience'] = $form_profile['place_experience'][$k];
								$exp_array[$k]['information_experience'] = $form_profile['information_experience'][$k];
							}*/
							$exp_array = $value_param[$key];
						}
						$exp_array = serialize($exp_array);
						$v_value = $this->db->escape_str($exp_array);
						$new_param .= "$value='$v_value', ";
						$new_param_insert .= $value.", ";
						$new_value_insert .= "'$v_value', ";
					} else {
						$v_value = $this->db->escape_str($value_param[$key]);
						$new_param .= "$value='$v_value', ";
						$new_param_insert .= $value.", ";
						$new_value_insert .= "'$v_value', ";
					}
					$new_param = rtrim($new_param,', ');
					$new_param_insert = rtrim($new_param_insert,', ');
					$new_value_insert = rtrim($new_value_insert,', ');
				}
				$check_pexist = $this->db->query("SELECT * FROM tbl_profile_tutor WHERE tutor_id='".$id_user."'")->row_array();
				if(!empty($check_pexist)){
					$this->db->simple_query("UPDATE tbl_profile_tutor SET $new_param WHERE tutor_id='".$id_user."'");
				}else{
					$this->db->simple_query("INSERT INTO tbl_profile_tutor(tutor_id".$new_param_insert.") VALUES('$id_user'".$new_value_insert.")");
				}
				$rets = array("status" => true, "code" => 0, "message" => "Successfully.");
			}
		}else{
			$rets = array("status" => false, "code" => 500, "message" => "field incomplete.");
			goto ending;
		}


		ending:
		$this->response($rets);
	}
	public function tutor_submit_approval_android_post()
	{
		if($this->get('access_token') != ''){
			$retok = $this->verify_access_token($this->get('access_token'));
			if ($this->get('access_token') != '') {
				$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
	            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
	                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
	                // $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	            }
	        }

	        $id_user = $retok['id_user'];
	        $user_tutor = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$id_user'")->row_array();
			$user_nametutor = $user_tutor['user_name'];
			$user_email 	= $user_tutor['email'];

			$emailkedua = "hermawan@meetaza.com";
			$emailketiga = "support@classmiles.com";
			
			// Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_indiclass_tutor_submit_approval_android','content' => array("user_nametutor" => "$user_nametutor", "user_email" => "$user_email", "emailkedua" => "$emailkedua" , "emailketiga" => "$emailketiga") )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;
			

			$this->db->simple_query("UPDATE tbl_user SET status=2 WHERE id_user='$id_user' && usertype_id='tutor'");
			$rets = array("status" => true, "code" => 0, "message" => "Successful.");
		}else{
			$rets = array("status" => false, "code" => 500, "message" => "field incomplete.");
			goto ending;
		}


		ending:
		$this->response($rets);
	}

	public function checkReferral_post()
	{
		$referral 		= $this->post('codereferral');
		$dateuser 		= $this->post('dateuser');
		$type 	= $this->post('type');
		$user_device 	= $this->post('user_device');
		$user_utc 		= $this->post('user_utc');
		$server_utc 	= $this->Rumus->getGMTOffset();

		if($user_device == 'mobile'){
			$user_utc 	= $this->Rumus->getGMTOffset('minute',$user_utc);
		}
		$interval   	= $user_utc - $server_utc;
		$thisyourday 	= DateTime::createFromFormat('Y-m-d H:i:s',$dateuser);
		$thisyourday->modify("+$interval minute");
		$thisyourday 	= $thisyourday->format('Y-m-d H:i:s');
		if ($type=="register") {
			$check 			= $this->db->query("SELECT * FROM master_referral WHERE referral_code='$referral' AND end_datetime >= '$thisyourday'")->row_array();
			if (!empty($check)) {
			$res['status'] = true;
			$res['message'] = 'Referral found';
			$res['code'] = 200;
			$res['data'] = $check['referral_id'];		
			}
			else
			{
				$res['status'] = false;
				$res['message'] = 'Referral not found';
				$res['code'] = 300;
				$res['data'] = null;	
			}
		}	
		else
		{
			$res['status'] = false;
			$res['message'] = 'Referral not found';
			$res['code'] = 300;
			$res['data'] = null;	
		}	
		$this->response($res);		
	}

	public function daftarAnak_post()
	{
		$parent_id 		= $this->post('id_user');
		$username 		= $this->post('username');
		$first_name		= $this->post('first_name');
		$last_name 		= $this->post('last_name');
		$password 		= $this->post('password');
		$user_birthdate	= $this->post('user_birthdate');
		$user_birthplace= $this->post('user_birthplace');
		$user_gender	= $this->post('user_gender');

		$school_id		= $this->post('school_id');
		$school_name	= $this->post('school_name');
		$school_address	= $this->post('school_address');
		$jenjang_id		= $this->post('jenjang_id');

		$images 		= $this->post('image');

        $basename           = $username.$this->Rumus->RandomString();
    	$img_name      		= $basename.'.jpg';
    	$bukti 				= isset($images) ? $images : "";

		$user_name = $first_name." ".$last_name;
		$id_user = $this->Rumus->gen_id_user();
		$password = password_hash($password, PASSWORD_DEFAULT);
		$usertype_id = 'student kid';
		$tokens = $this->get('access_token');
		$skrg = date_diff(date_create($user_birthdate), date_create('today'))->y;

		if ($this->get('access_token') != '') {
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $res['token'] = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                // $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
        }

        if ($images != "") {

	        $image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'               
	                     
	        //Write to disk
	        file_put_contents("aset/_temp_images/".$img_name,file_get_contents($image));
	        
	        $this->Rumus->sendToCDN('user', $img_name, null, 'aset/_temp_images/'.$img_name );
        }

		if($this->db->simple_query("INSERT INTO tbl_user(id_user, username, user_name, first_name, last_name, password, user_birthplace, user_birthdate, user_age, user_image, user_gender, usertype_id, status) VALUES($id_user, '$username', '$user_name', '$first_name', '$last_name', '$password', '$user_birthplace', '$user_birthdate', '$skrg', '".base64_encode('user/'.$img_name)."', '$user_gender', '$usertype_id', 1)")){

			$this->db->simple_query("INSERT INTO tbl_profile_kid(kid_id, school_id, school_name, school_address, jenjang_id, parent_id) VALUES($id_user, '$school_id', '$school_name', '$school_address', $jenjang_id, $parent_id)");
	        $res['status'] = true;
			$res['message'] = 'Success create anak';
			$res['code'] = 200;	
			$this->response($res);
		}else{
			$error = $this->db->error();
			if(strpos($error['message'], 'username') !== false){
				
		        $res['status'] = false;
				$res['message'] = 'username already exist!';
				$res['code'] = 300;	
				$this->response($res);
			} else {
				$res['status'] = false;
				$res['message'] = $error;
				$res['code'] = 301;	
				$this->response($res);
			}
		}
		

        $res['status'] = false;
		$res['message'] = 'Fields incomplete '.$username;
		$res['code'] = -300;	
		ending:
		$this->response($res);
	}

	public function editAnak_post()
	{
		$parent_id 		= $this->post('id_user');
		$username 		= $this->post('username');
		$first_name		= $this->post('first_name');
		$last_name 		= $this->post('last_name');
		$user_birthdate	= $this->post('user_birthdate');
		$user_birthplace= $this->post('user_birthplace');
		$user_gender	= $this->post('user_gender');

		$school_id		= $this->post('school_id');
		$school_name	= $this->post('school_name');
		$school_address	= $this->post('school_address');
		$jenjang_id		= $this->post('jenjang_id');

		$images 		= $this->post('image');

        $basename           = $username.$this->Rumus->RandomString();
    	$img_name      		= $basename.'.jpg';
    	$bukti 				= isset($images) ? $images : "";

		$user_name = $first_name." ".$last_name;
		$id_user = $this->post('id_anak');
		$tokens = $this->get('access_token');
		$skrg = date_diff(date_create($user_birthdate), date_create('today'))->y;

		if ($this->get('access_token') != '') {
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $res['token'] = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                // $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
            }
        }

		if ($images != "") {

	        $image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'               
	                     
	        //Write to disk
	        file_put_contents("aset/_temp_images/".$img_name,file_get_contents($image));
	        
	        $this->Rumus->sendToCDN('user', $img_name, null, 'aset/_temp_images/'.$img_name );

	        $this->db->simple_query("UPDATE tbl_user set user_image = '".base64_encode('user/'.$img_name)."' WHERE id_user = '$id_user' ");
        }

		if($this->db->simple_query("UPDATE tbl_user set user_name='{$user_name}', first_name = '$first_name', last_name = '$last_name', user_birthplace = '$user_birthplace', user_birthdate = '$user_birthdate', user_age = '$skrg', user_gender = '$user_gender' WHERE id_user = '$id_user' ")){

			$this->db->simple_query("UPDATE tbl_profile_kid set school_id='$school_id', school_name = '$school_name', school_address = '$school_address', jenjang_id = $jenjang_id WHERE kid_id = '$id_user'");
			$imgs = $this->db->query("SELECT user_image from tbl_user WHERE id_user = '$id_user' ")->row_array()['user_image'];
	        $res['status'] = true;
	        $res['img'] = $imgs;
			$res['message'] = 'Success edit anak ';
			$res['code'] = 200;	
			$this->response($res);
		}else{
			$error = $this->db->error();
			if(strpos($error['message'], 'username') !== false){
				
		        $res['status'] = false;
				$res['message'] = 'username already exist! ';
				$res['code'] = 300;	
				$this->response($res);
			} else {
				$res['status'] = false;
				$res['message'] = $error;
				$res['code'] = 301;	
				$this->response($res);
			}
		}
		

        $res['status'] = false;
		$res['message'] = 'Fields incomplete '.$username;
		$res['code'] = -300;	
		ending:
		$this->response($res);
	}

	public function myListKids_post()
	{
		$parent_id 		= $this->post('parent_id');
		
		if ($this->get('access_token') != '') {
			
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
	        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
	            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
	            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	        }
			// SELECT tu.*, tpk.* FROM tbl_profile_kid as tpk INNER JOIN tbl_user as tu ON tpk.kid_id=tu.id_user WHERE tpk.parent_id='$parent_id'
	        $select_all = $this->db->query("SELECT tu.*, tpk.*, mj.*, ms.*, mst.school_id as school_id_temp, mst.school_name as school_name_temp, mst.school_address as school_address_temp, mst.provinsi_id as provinsi_temp, mst.kabupaten_id as kabupaten_temp, mst.kecamatan_id as kecamatan_temp FROM tbl_user AS tu LEFT JOIN ( tbl_profile_kid AS tpk INNER JOIN master_jenjang as mj on tpk.jenjang_id = mj.jenjang_id ) ON tpk.kid_id=tu.id_user LEFT JOIN master_school as ms on tpk.school_id = ms.school_id LEFT JOIN master_school_temp as mst on tpk.school_id = mst.school_id WHERE tpk.parent_id='$parent_id'")->result_array();
	        if (!empty($select_all)) {
	        	
				$rets['status'] = true;
				$rets['message'] = "success";
				$rets['data'] = $select_all;
				$this->response($rets);
	        }
	
		}

        $res['status'] = false;
		$res['message'] = 'Field incomplete';
		ending:
		$this->response($res);
	}
	public function detailKid_post()
	{
		$kid_id 		= $this->post('kid_id');
		
		$parent_id 		= $this->post('parent_id');
		
		if ($this->get('access_token') != '') {
			
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
	        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
	            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
	            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	        }
			// SELECT tu.*, tpk.* FROM tbl_profile_kid as tpk INNER JOIN tbl_user as tu ON tpk.kid_id=tu.id_user WHERE tpk.parent_id='$parent_id'
	        $select_all = $this->db->query("SELECT tu.*, tpk.*, mj.*, ms.*, mst.school_id as school_id_temp, mst.school_name as school_name_temp, mst.school_address as school_address_temp, mst.provinsi_id as provinsi_temp, mst.kabupaten_id as kabupaten_temp, mst.kecamatan_id as kecamatan_temp FROM tbl_user AS tu LEFT JOIN ( tbl_profile_kid AS tpk INNER JOIN master_jenjang as mj on tpk.jenjang_id = mj.jenjang_id ) ON tpk.kid_id=tu.id_user LEFT JOIN master_school as ms on tpk.school_id = ms.school_id LEFT JOIN master_school_temp as mst on tpk.school_id = mst.school_id WHERE tpk.parent_id='$parent_id' and tpk.kid_id='$kid_id'")->result_array();
	        if (!empty($select_all)) {
	        	
				$rets['status'] = true;
				$rets['message'] = "success";
				$rets['data'] = $select_all;
				$this->response($rets);
	        }
	
		}

        $res['status'] = false;
		$res['message'] = 'Field incomplete';
		ending:
		$this->response($res);
	}
	public function listParentKids_post()
	{		
		$kid_id 	= $this->post('kid_id');		
		$ckList 	= $this->db->query("SELECT * FROM tbl_profile_kid WHERE kid_id = '$kid_id' AND is_master_parent != '1'")->result_array();
		if (!empty($ckList)) {
			$datas = array();
			foreach ($ckList as $key => $value) {
				$parent_id		= $value['parent_id'];
				$getParent 		= $this->db->query("SELECT email,user_name,user_image FROM tbl_user WHERE id_user = '$parent_id'")->row_array();

				$email 			= $getParent['email'];
				$user_name 		= $getParent['user_name'];
				$user_image 	= $getParent['user_image'];
				$datas[$key]   = $getParent;

			}

			

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['data'] = $datas;
			$this->response($rets);
		}	
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;
			$rets['data'] = null;
			$this->response($rets);	
		}
	} 

	public function updateMySchool_post()
	{
		$id_user 		= $this->post('id_user');
		$school_id 		= $this->post('school_id');
		
		if ($this->get('access_token') != '') {
			
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
	        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
	            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
	            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	        }
			$update = $this->db->simple_query("UPDATE tbl_profile_student SET school_id = '{$school_id}' WHERE student_id='".$id_user."'");

	        $res['status'] = true;
			$res['message'] = 'Successfully '.$update." ".$school_id;
			$this->response($res);
		}

        $res['status'] = false;
		$res['message'] = 'Field incomplete ';
		ending:
		$this->response($res);
	}

	public function addSchool_post($value='')
	{
		$id_user 		= $this->post('id_user');
		$school_name 	= $this->post('school_name');
		$school_address = $this->post('school_address');
		$provinsi_id 	= $this->post('provinsi_id');
		$kabupaten_id 	= $this->post('kabupaten_id');
		$kecamatan_id 	= $this->post('kecamatan_id');
		$school_id 		= $this->Rumus->gen_id_school();
		if ($this->get('access_token') != '') {
			
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
	        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
	            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
	            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	        }
			
			$add = $this->db->simple_query("INSERT INTO master_school_temp(school_id, school_name, school_address, provinsi_id, kabupaten_id, kecamatan_id) VALUES('{$school_id}','{$school_name}','{$school_address}','{$provinsi_id}','{$kabupaten_id}','{$kecamatan_id}')");
			$update = $this->db->simple_query("UPDATE tbl_profile_student SET school_id = '{$school_id}' WHERE student_id='".$id_user."'");
	        $res['status'] = true;
	        $res['data'] = $school_id;
			$res['message'] = 'Successfully '.$school_id;
			$this->response($res);
		}
        $res['status'] = false;
		$res['message'] = 'Field incomplete ';
		ending:
		$this->response($res);
	}

	public function addSchoolKids_post($value='')
	{
		$id_user 		= $this->post('id_user');
		$school_name 	= $this->post('school_name');
		$school_address = $this->post('school_address');
		$provinsi_id 	= $this->post('provinsi_id');
		$kabupaten_id 	= $this->post('kabupaten_id');
		$kecamatan_id 	= $this->post('kecamatan_id');
		$school_id 		= $this->Rumus->gen_id_school();
		if ($this->get('access_token') != '') {
			
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
	        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
	            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
	            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	        }
			
			$add = $this->db->simple_query("INSERT INTO master_school_temp(school_id, school_name, school_address, provinsi_id, kabupaten_id, kecamatan_id) VALUES('{$school_id}','{$school_name}','{$school_address}','{$provinsi_id}','{$kabupaten_id}','{$kecamatan_id}')");
	        $res['status'] = true;
			$res['message'] = 'Successfully '.$school_id;
			$res['school_id'] = $school_id;
			$this->response($res);
		}
        $res['status'] = false;
		$res['message'] = 'Field incomplete ';
		ending:
		$this->response($res);
	}

	public function listMyChart_post()
	{
		$id_user 		= $this->post('id_user');
		if ($this->get('access_token') != '') {
			
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
	        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
	            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
	            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	        }
	        $anak               = $this->db->query("SELECT * FROM tbl_profile_kid WHERE parent_id='$id_user'")->result_array();
            $q_anak = "";
            if(!empty($anak)){
                foreach ($anak as $key => $value) {
                    $q_anak.= " OR tk.id_user='".$value['kid_id']."' ";
                }
            }
			$getdatakeranjang   = $this->db->query("SELECT tk.*, tclass.class_id, tr.approve as tr_approve, trg.approve as trg_approve, trm.status as trm_approve, trp.status as trp_approve, tr.subject_id as tr_subject_id, trg.subject_id as trg_subject_id, tclass.subject_id as trm_subject_id,  tr.tutor_id as tr_tutor_id, trg.tutor_id as trg_tutor_id, tclass.tutor_id as trm_tutor_id, tr.topic as tr_topic, trg.topic as trg_topic, tclass.description as trm_topic, tpp.description as trp_topic, tr.date_requested as tr_date_requested, trg.date_requested as trg_date_requested, trm.created_at as trm_date_requested, trp.created_at as trp_date_requested FROM tbl_keranjang as tk LEFT JOIN tbl_request as tr ON tk.request_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON tk.request_grup_id=trg.request_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tclass ON trm.class_id=tclass.class_id) ON tk.request_multicast_id=trm.request_id LEFT JOIN (tbl_request_program as trp INNER JOIN tbl_price_program as tpp ON trp.program_id=tpp.list_id) ON tk.request_program_id=trp.request_id WHERE tk.id_user='$id_user' $q_anak")->result_array();

			foreach ($getdatakeranjang as $row => $v) {
				$subject_id     = $v['tr_subject_id'] != null ? $v['tr_subject_id'] : ( $v['trg_subject_id'] != null ? $v['trg_subject_id'] : $v['trm_subject_id'] );
				$tutor_id       = $v['tr_tutor_id'] != null ? $v['tr_tutor_id'] : ( $v['trg_tutor_id'] != null ? $v['trg_tutor_id'] : $v['trm_tutor_id'] );
				$status         = $v['tr_approve'] != null ? $v['tr_approve'] : ( $v['trg_approve'] != null ? $v['trg_approve'] : ( $v['trm_approve'] ? $v['trm_approve'] != null : $v['trp_approve']));
				$topic          = $v['tr_topic'] != null ? $v['tr_topic'] : ( $v['trg_topic'] != null ? $v['trg_topic'] : ($v['trm_topic'] ? $v['trm_topic'] != null : $v['trp_topic']  ));
				$date_requested = $v['tr_date_requested'] != null ? $v['tr_date_requested'] : ( $v['trg_date_requested'] != null ? $v['trg_date_requested'] : ( $v['trm_date_requested'] != null ? $v['trm_date_requested'] : $v['trp_date_requested']));

				$request_program_id = $v['request_program_id'];
				$subject_name   = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
				$data_tutor     = $this->db->query("SELECT user_name,first_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
				$program_id 	= $this->db->query("SELECT program_id FROM tbl_request_program WHERE request_id='$request_program_id'")->row_array()['program_id'];
        		$program_name 	= $this->db->query("SELECT name FROM tbl_price_program WHERE list_id = '$program_id'")->row_array()['name'];
        		$program_poster = $this->db->query("SELECT poster FROM tbl_price_program WHERE list_id = '$program_id'")->row_array()['poster'];
				$tutor_name     = $data_tutor['first_name'] != null ? $data_tutor['first_name'] : $program_name;
				$tutor_image    = $data_tutor['user_image'] != null ? $data_tutor['user_image'] : $program_poster;
				$request_id     = $v['request_id'];
				$request_grup_id= $v['request_grup_id'];
				$harga          = $v['price'];
				$hargaprice     = number_format($harga, 0, ".", ".");
				$getdatakeranjang[$row]['subject_id'] = $subject_id;
				$getdatakeranjang[$row]['subject_name'] = $subject_name;
				$getdatakeranjang[$row]['topic'] = $topic;
				$getdatakeranjang[$row]['status'] = $status;
				$getdatakeranjang[$row]['date_requested'] = $date_requested;
				$getdatakeranjang[$row]['tutor_name'] = $tutor_name;
				$getdatakeranjang[$row]['tutor_image'] = $tutor_image;
				$getdatakeranjang[$row]['harga'] = $harga;
			}

			$res['status'] = true;
			$res['message'] = 'Successfully ';
			$res['data'] =  $getdatakeranjang;
			$this->response($res);
		}

		$res['status'] = false;
		$res['message'] = 'Field incomplete ';
		ending:
		$this->response($res);

	}

	public function submitOrder_post()
	{
		$order_id = $this->Rumus->gen_order_id();
        $UnixCode = $this->Rumus->RandomUnixCode();        
        $total_price = 0;
        $buyer_id = "";
        $hargatotal = 0;
        $orderid = $this->post('order_id');
        $ids = explode(",", $orderid);
        $tuc = $this->post('utc');
        $utc = $this->Rumus->getGMTOffset('minute',$tuc);

        $date_expired = "";
        if(!empty($ids)) {
        	$server_utc         = $this->Rumus->getGMTOffset();
            $intervall          = $utc - $server_utc;
            $datenow            = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
            $datenow->modify("+3600 seconds");
            $datenow            = strtotime($datenow->format('Y-m-d H:i:s'));
            
            $datenow            = date('Y-m-d H:i:s', $datenow);
            $date_expired       = $datenow;

            foreach($ids as $krnj_id => $id) {
                $v    = $this->db->query("SELECT tk.*, tr.date_requested as start_time_private, trg.date_requested as start_time_grup, tc.start_time as start_time_multicast, trp.created_at as start_time_program FROM tbl_keranjang as tk LEFT JOIN tbl_request as tr ON tk.request_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON tk.request_grup_id=trg.request_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON trm.class_id=tc.class_id ) ON tk.request_multicast_id=trm.request_id LEFT JOIN tbl_request_program as trp ON tk.request_program_id=trp.request_id WHERE krnj_id='$id'")->row_array();
                $price          = $v['price'];

                $start_time     = $v['start_time_private'] != null ? $v['start_time_private'] : ( $v['start_time_grup'] != null ? $v['start_time_grup'] : ( $v['start_time_multicast'] != null ? $v['start_time_multicast'] : $v['start_time_program'] ));

                if(strtotime($start_time) <= $datenow){
                    $datenow = strtotime($start_time);
                }

                if ($buyer_id == "") {                    
                    $buyer_id   = $buyer_id+$v['id_user'];
                }                
                // $product_id     = $g_keranjang['request_id'];
                $product_id     = $v['request_id'] != null ? $v['request_id'] : ( $v['request_grup_id'] != null ? $v['request_grup_id'] : ( $v['request_multicast_id'] != null ? $v['request_multicast_id'] : $v['request_program_id']));
                $type_product   = $v['request_id'] != null ? 'private' : ( $v['request_grup_id'] != null ? 'group': ( $v['request_multicast_id'] != null ? 'multicast' : 'program' ));
                if ($type_product=='multicast') {
                    $product_idclass = $this->db->query("SELECT class_id FROM tbl_request_multicast WHERE request_id='$product_id'")->row_array()['class_id'];
                    $product_properties = $this->db->query("SELECT description FROM tbl_class WHERE class_id='$product_idclass'")->row_array()['description'];

                    $get_pricetutor = $this->db->query("SELECT harga FROM tbl_class_price WHERE class_id='$product_idclass'")->row_array()['harga'];
                    $total_price    = $total_price+$price;                
                    $save_to_tbl_order_detail = $this->db->query("INSERT INTO tbl_order_detail (order_id,class_id,product_id,product_properties,total_amt,init_price,discount,final_price,final_pricetutor,TYPE) VALUES ('$order_id','$product_idclass','$product_id','$product_properties','','$price','0','$price','$get_pricetutor','$type_product')");  
                }
                else if($type_product == 'program'){
                    $list_id            = $this->db->query("SELECT program_id FROM tbl_request_program WHERE request_id = '$product_id'")->row_array()['program_id'];
                    $product_properties = $this->db->query("SELECT name FROM tbl_price_program WHERE list_id = '$list_id'")->row_array()['name'];

                    $total_price    = $total_price+$price; 
                    $save_to_tbl_order_detail = $this->db->query("INSERT INTO tbl_order_detail (order_id,product_id,product_properties,total_amt,init_price,discount,final_price,final_pricetutor,TYPE) VALUES ('$order_id','$product_id','$product_properties','','$price','0','$price','$price','$type_product')");
                }
                else
                {
                    if ($type_product == "private") {
                        $duration_requested = $this->db->query("SELECT duration_requested FROM tbl_request WHERE request_id='$product_id'")->row_array()['duration_requested'];
                        $subject_id = $this->db->query("SELECT subject_id FROM tbl_request WHERE request_id='$product_id'")->row_array()['subject_id'];
                        $tutorid = $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id='$product_id'")->row_array()['tutor_id'];
                    }
                    else
                    {
                        $duration_requested = $this->db->query("SELECT duration_requested FROM tbl_request_grup WHERE request_id='$product_id'")->row_array()['duration_requested'];
                        $subject_id = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$product_id'")->row_array()['subject_id'];
                        $tutorid = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$product_id'")->row_array()['tutor_id'];
                    }
                    $get_pricetutor = $this->db->query("SELECT harga FROM tbl_service_price where tutor_id='$tutorid' AND subject_id='$subject_id' AND class_type='$type_product'")->row_array()['harga'];

                    $total_pricetutor = ($duration_requested/900)*$get_pricetutor;
                    $get_product = $this->db->query("SELECT tr.topic as tr_topic, trg.topic as trg_topic FROM tbl_request as tr LEFT JOIN tbl_request_grup as trg ON tr.request_id=trg.request_id where tr.request_id='$product_id' OR trg.request_id='$product_id'")->row_array();                    
                    $product_properties = $get_product['tr_topic'] != null ? $get_product['tr_topic'] : $get_product['trg_topic'];

                    $total_price    = $total_price+$price;                
                    $save_to_tbl_order_detail = $this->db->query("INSERT INTO tbl_order_detail (order_id,product_id,product_properties,total_amt,init_price,discount,final_price,final_pricetutor,TYPE) VALUES ('$order_id','$product_id','$product_properties','','$price','0','$price','$total_pricetutor','$type_product')");  
                }        
                $this->db->query("DELETE FROM tbl_keranjang WHERE krnj_id='$id'");
            }
            // $datenow-= 300;
            // $datenow = date('Y-m-d H:i:s', $datenow);
        }
                
        $hargatotal    			= $total_price+$UnixCode;
        $invoice_id             = $this->Rumus->gen_invoice_id($order_id);
        $nowdate                = date("Y-m-d H:i:s");

        $save_to_tbl_order      = $this->db->query("INSERT INTO tbl_order (order_id,total_price,unix_code,buyer_id,order_status, payment_due, created_at) VALUES ('$order_id','$hargatotal','$UnixCode','$buyer_id','0','$date_expired','$nowdate')");         
        if ($save_to_tbl_order) {
            $save_to_tbl_invoice    = $this->db->query("INSERT INTO tbl_invoice (invoice_id,order_id,payment_type,total_price,underpayment) VALUES ('$invoice_id','$order_id',null,'$hargatotal','$hargatotal')");

            ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
            $json_notif = "";
            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$buyer_id}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "40","title" => "Classmiles", "text" => "")));
            }
            if($json_notif != ""){
                $headers = array(
                    'Authorization: key=' . FIREBASE_API_KEY,
                    'Content-Type: application/json'
                    );
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
            }
            ////------------ END -------------------------------------//
            
            if ($save_to_tbl_invoice) {

				$order = $this->db->query("SELECT * FROM tbl_order as tor INNER JOIN tbl_invoice as ti ON tor.order_id=ti.order_id WHERE tor.order_id='$order_id'")->row_array();
            	$invoice_id         = $order['invoice_id'];
                $datetrans          = $order['last_update']; 
                $UnixCode           = $order['unix_code'];
                $hargakelas         = $order['total_price']-$UnixCode;
                $order['hargakelas']= $hargakelas;
                    
        		$getdataorder = $this->db->query("SELECT tod.*, trm.class_id as class_id, tc.name as subject_name, tr.subject_id as tr_subject_id, tr.tutor_id as tr_tutor_id, tr.approve as tr_approve, tr.date_requested as tr_date_requested, tr.topic as tr_topic, trg.subject_id as trg_subject_id, trg.tutor_id as trg_tutor_id, trg.approve as trg_approve, trg.date_requested as trg_date_requested, trg.topic as trg_topic, tc.subject_id as trm_subject_id, tc.tutor_id as trm_tutor_id, trm.status as trm_approve, tc.start_time as trm_date_requested, tc.description as trm_topic, tpp.description as trp_topic, trp.status as trp_approve, trp.created_at as trp_date_requested FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id LEFT JOIN (tbl_request_program as trp INNER JOIN tbl_price_program as tpp ON trp.program_id=tpp.list_id) ON tod.product_id=trp.program_id  WHERE tod.order_id='$order_id'")->result_array();

        		$server_utcc        = $this->Rumus->getGMTOffset();
                $intervall          = $utc - $server_utcc;
	            $no = 1;
	            $topic_limit    = 10;  
	            foreach ($getdataorder as $row => $v) {
	                $startimeclass  = $v['tr_date_requested'] != null ? $v['tr_date_requested'] : ( $v['trg_date_requested'] != null ? $v['trg_date_requested'] : ( $v['trm_date_requested'] != null ? $v['trm_date_requested'] : $v['trp_date_requested']));
	                $subject_id     = $v['tr_subject_id'] != null ? $v['tr_subject_id'] : ( $v['trg_subject_id'] != null ? $v['trg_subject_id'] : ( $v['trm_subject_id'] != null ? $v['trm_subject_id'] : ''));
	                $topic          = $v['tr_topic'] != null ? $v['tr_topic'] : ( $v['trg_topic'] != null ? $v['trg_topic'] : ( $v['trm_topic'] != null ? $v['trm_topic'] : substr($v['trp_topic'], 0, $topic_limit)));
	                $tutor_id       = $v['tr_tutor_id'] != null ? $v['tr_tutor_id'] : ( $v['trg_tutor_id'] != null ? $v['trg_tutor_id'] : ( $v['trm_tutor_id'] != null ? $v['trm_tutor_id'] : ''));

	                if ($subject_id == '') {
	                	$subject_name 	= "";
	                	$tutor_name 	= "";
	                	$tutor_image 	= "";
	                }
	                else
	                {
		                $subject_name       = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
		                $tutor_name         = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
		                $tutor_image        = $this->db->query("SELECT user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_image'];
		                $waktumulai         = DateTime::createFromFormat ('Y-m-d H:i:s',$startimeclass);
		                $waktumulai->modify("+".$intervall ." minutes");
		                $waktumulai         = $waktumulai->format('d-m-Y H:i:s'); 
	                }
	                                  
	                $priceperclass      = $v['init_price'];  
	                $priceperclass      = number_format($priceperclass, 0, ".", ".");

	                $subject_id     = $v['tr_subject_id'] != null ? $v['tr_subject_id'] : ( $v['trg_subject_id'] != null ? $v['trg_subject_id'] : ($v['trm_subject_id'] != null ? $v['trm_subject_id'] : ''));
                    $tutor_id       = $v['tr_tutor_id'] != null ? $v['tr_tutor_id'] : ( $v['trg_tutor_id'] != null ? $v['trg_tutor_id'] : ( $v['trm_tutor_id'] != null ? $v['trm_tutor_id'] : ''));
                    $status         = $v['tr_approve'] != null ? $v['tr_approve'] : ( $v['trg_approve'] != null ? $v['trg_approve'] : ($v['trm_approve'] != null ? $v['trm_approve'] : $v['trp_approve']));

                    $datasub   = $this->db->query("SELECT ms.subject_name, mj.jenjang_name, mj.jenjang_level FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.subject_id='$subject_id'")->row_array();
                    // $subject_name   = $datasub['subject_name'];
                    if ($datasub['jenjang_level'] != 0) {
                    	$jenjang_data   = $datasub['jenjang_name'].' '.$datasub['jenjang_level'];
                    } else {
                    	$jenjang_data   = $datasub['jenjang_name'];
                    }
                    $data_tutor     = $this->db->query("SELECT first_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
                    $tutor_name     = $data_tutor['first_name'];
                    $tutor_image    = $data_tutor['user_image'];

                    $getdataorder[$row]['topic'] 			= $topic;
                    $getdataorder[$row]['subject_name'] 	= $subject_name;
                    $getdataorder[$row]['jenjang_names'] 	= $jenjang_data;
                    $getdataorder[$row]['tutor_name'] 		= $tutor_name;
                    $getdataorder[$row]['tutor_image'] 		= $tutor_image;
	            }

	            $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$date_expired);
                $tanggaltransaksi->modify("+".$intervall ." minutes");
                $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');
                $order['payment_due'] = $tanggaltransaksi;
                $order['details'] 	  = $getdataorder;           
				$res['status']        = true;
				$res['message'] 	  = 'Successfully SubmitOrder';
				$res['data'] 		  =  $order;
				$this->response($res);
            }
            else
            {
                $res['status'] = false;
				$res['message'] = 'Failed Save invoice';
				$this->response($res);
            }
        }
        else
        {
            $res['status'] = false;
			$res['message'] = 'Failed Save Order';
			$this->response($res);
        }
        $res['status'] = false;
		$res['message'] = 'Failed No';
		$this->response($res);
	}

	public function detailPayment_get()
	{
		$order_id = $this->get('order_id');

        $tuc = $this->get('utc');
        $utc = $this->Rumus->getGMTOffset('minute',$tuc);

        $user_utc           = $utc;
        $server_utcc        = $this->Rumus->getGMTOffset();
        $intervall          = $user_utc - $server_utcc;
		if (!empty($order_id)) {
		
	        // $type   = $this->db->query("SELECT type FROM tbl_order_detail WHERE order_id='$order_id'")->row_array()['type'];
	        // if ($type == "private") {
	        //     $getdataorder = $this->db->query("SELECT ti.invoice_id, tu.user_name, tr.tutor_id, tr.subject_id, tr.date_requested, ms.subject_name, tor.*, tod.class_id, tod.product_id, tod.product_properties, tod.type FROM tbl_order as tor INNER JOIN tbl_order_detail as tod ON tor.order_id=tod.order_id INNER JOIN tbl_request as tr ON tr.request_id=tod.product_id INNER JOIN tbl_user as tu ON tu.id_user=tr.tutor_id INNER JOIN tbl_invoice as ti ON ti.order_id=tor.order_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tor.order_id='$order_id'")->result_array();
	        // }
	        // else if ($type == "group"){
	        //     $getdataorder = $this->db->query("SELECT ti.invoice_id, tu.user_name, trg.tutor_id, trg.subject_id, trg.date_requested, ms.subject_name, tor.*, tod.class_id, tod.product_id, tod.product_properties, tod.type FROM tbl_order as tor INNER JOIN tbl_order_detail as tod ON tor.order_id=tod.order_id INNER JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id INNER JOIN tbl_user as tu ON tu.id_user=trg.tutor_id INNER JOIN tbl_invoice as ti ON ti.order_id=tor.order_id INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE tor.order_id='$order_id'")->result_array();
	        // }
	        // else
	        // {
	        //     $getdataorder = $this->db->query("SELECT ti.invoice_id, tu.user_name, tc.name, tc.description, tc.start_time, tor.*, tod.class_id, tod.product_id, tod.product_properties, tod.type FROM tbl_order as tor INNER JOIN tbl_order_detail as tod ON tor.order_id=tod.order_id LEFT JOIN tbl_class as tc ON tc.class_id=tod.class_id INNER JOIN tbl_user as tu ON tu.id_user=tc.tutor_id INNER JOIN tbl_invoice as ti ON ti.order_id=tor.order_id WHERE tor.order_id='$order_id'")->result_array();
	        // }

	        // foreach ($getdataorder as $row => $v) {
	        //     $UnixCode           = $v['unix_code'];
	        //     $hargakelas         = $v['total_price']-$UnixCode;
	        //     $hasiljumlah        = number_format($v['total_price'], 0, ".", ".");

	        //     $user_utc           = $this->session->userdata('user_utc');
	        //     $server_utcc        = $this->Rumus->getGMTOffset();
	        //     $intervall          = $user_utc - $server_utcc;

	        //     if ($type == "multicast") {
	        //         $waktumulai         = DateTime::createFromFormat ('Y-m-d H:i:s',$v['start_time']);
	        //         $waktumulai->modify("+".$intervall ." minutes");
	        //         $waktumulai         = $waktumulai->format('Y-m-d H:i:s');
	        //     }
	        //     else if ($type == "private") {
	        //         $waktumulai         = DateTime::createFromFormat ('Y-m-d H:i:s',$v['date_requested']);
	        //         $waktumulai->modify("+".$intervall ." minutes");
	        //         $waktumulai         = $waktumulai->format('Y-m-d H:i:s');
	        //     }
	        //     else if ($type == "group") {
	        //         $waktumulai         = DateTime::createFromFormat ('Y-m-d H:i:s',$v['date_requested']);
	        //         $waktumulai->modify("+".$intervall ." minutes");
	        //         $waktumulai         = $waktumulai->format('Y-m-d H:i:s');
	        //     }

	        //     $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$v['last_update']);
	        //     $tanggaltransaksi->modify("+".$intervall ." minutes");
	        //     $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');

	        //     $dateee             = $tanggaltransaksi;
	        //     $datetime           = new DateTime($dateee);
	        //     $datetime->modify('+1 hours');
	        //     $limitdateee        = $datetime->format('Y-m-d H:i:s');

	        //     $datelimit          = date_create($limitdateee);
	        //     $dateelimit         = date_format($datelimit, 'd/m/y');
	        //     $harilimit          = date_format($datelimit, 'd');
	        //     $hari               = date_format($datelimit, 'l');
	        //     $tahunlimit         = date_format($datelimit, 'Y');
	        //     $waktulimit         = date_format($datelimit, 'H:s');
	                                                    
	        //     $datelimit          = $dateelimit;
	        //     $sepparatorlimit    = '/';
	        //     $partslimit         = explode($sepparatorlimit, $datelimit);
	        //     $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

	        //     $getdataorder[$row]['tanggaltransaksi'] = $tanggaltransaksi;
	        //     $getdataorder[$row]['expireddate']	 	= $hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit. ' Pukul '.$waktulimit;
	        //     $getdataorder[$row]['hargakelas'] 		= $hargakelas;
	        // }
         $getdataorder = $this->db->query("SELECT tod.*, trm.class_id as class_id, tc.name as subject_name, tr.subject_id as tr_subject_id, tr.tutor_id as tr_tutor_id, tr.approve as tr_approve, tr.date_requested as tr_date_requested, tr.topic as tr_topic, trg.subject_id as trg_subject_id, trg.tutor_id as trg_tutor_id, trg.approve as trg_approve, trg.date_requested as trg_date_requested, trg.topic as trg_topic, tc.subject_id as trm_subject_id, tc.tutor_id as trm_tutor_id, trm.status as trm_approve, tc.start_time as trm_date_requested, tc.description as trm_topic FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id  WHERE tod.order_id='$order_id'")->result_array();

		            $no = 1;
		            foreach ($getdataorder as $row => $v) {
		                $startimeclass  = $v['tr_date_requested'] != null ? $v['tr_date_requested'] : ( $v['trg_date_requested'] != null ? $v['trg_date_requested'] : $v['trm_date_requested']);
		                $subject_id     = $v['tr_subject_id'] != null ? $v['tr_subject_id'] : ( $v['trg_subject_id'] != null ? $v['trg_subject_id'] : $v['trm_subject_id']);
		                $topic          = $v['tr_topic'] != null ? $v['tr_topic'] : ( $v['trg_topic'] != null ? $v['trg_topic'] : $v['trm_topic']);
		                $tutor_id       = $v['tr_tutor_id'] != null ? $v['tr_tutor_id'] : ( $v['trg_tutor_id'] != null ? $v['trg_tutor_id'] : $v['trm_tutor_id']);

		                $subject_name       = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
		                $tutor_name         = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
		                $tutor_image        = $this->db->query("SELECT user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_image'];
		                $waktumulai         = DateTime::createFromFormat ('Y-m-d H:i:s',$startimeclass);
		                $waktumulai->modify("+".$intervall ." minutes");
		                $waktumulai         = $waktumulai->format('d-m-Y H:i:s'); 
		                $priceperclass      = $v['init_price'];  
		                $priceperclass      = number_format($priceperclass, 0, ".", ".");

		                $subject_id     = $v['tr_subject_id'] != null ? $v['tr_subject_id'] : ( $v['trg_subject_id'] != null ? $v['trg_subject_id'] : $v['trm_subject_id']);
	                    $tutor_id       = $v['tr_tutor_id'] != null ? $v['tr_tutor_id'] : ( $v['trg_tutor_id'] != null ? $v['trg_tutor_id'] : $v['trm_tutor_id']);
	                    $status         = $v['tr_approve'] != null ? $v['tr_approve'] : ( $v['trg_approve'] != null ? $v['trg_approve'] : $v['trm_approve']);

	                    $datasub   = $this->db->query("SELECT ms.subject_name, mj.jenjang_name, mj.jenjang_level FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.subject_id='$subject_id'")->row_array();
	                    $subject_name   = $datasub['subject_name'];
	                    if ($datasub['jenjang_level'] != 0) {
	                    	$jenjang_data   = $datasub['jenjang_name'].' '.$datasub['jenjang_level'];
	                    } else {
	                    	$jenjang_data   = $datasub['jenjang_name'];
	                    }
	                    $data_tutor     = $this->db->query("SELECT first_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
	                    $tutor_name     = $data_tutor['first_name'];
	                    $tutor_image    = $data_tutor['user_image'];

	                    $getdataorder[$row]['topic'] 			= $topic;
	                    $getdataorder[$row]['subject_name'] 	= $subject_name;
	                    $getdataorder[$row]['jenjang_name'] 	= $jenjang_data;
	                    $getdataorder[$row]['tutor_name'] 		= $tutor_name;
	                    $getdataorder[$row]['tutor_image'] 		= $tutor_image;
		            }

			$res['status'] = true;
			$res['message'] = 'Successfully ';
			$res['data'] =  $getdataorder;
			$this->response($res);
		}
		else
		{
			$res['status'] = false;
			$res['message'] = 'failed ';
			$res['data'] =  null;
			$this->response($res);
		}
	}

	public function ConfirmPayment_post()
	{
		$id 				= $this->post('id_user');
    	$tokenid 			= $this->post('orderid');
    	$bank 				= $this->post("bank");    	
    	$frombank 			= $this->post("frombank"); 
    	$nominalsave 		= $this->post("nominalsave");
    	$namapemilikbank 	= $this->post("namapemilikbank");
    	$metodepembayaran 	= $this->post("metodepembayaran");
    	$buktibayar 		= $id.'_'.$tokenid.'.jpg';
        $type 				= 'private';
        $user_utc 			= $this->post("user_utc");
    	$bukti 				= $this->post("bukti_tr");
    	$where = array(
    		'order_id' => $tokenid,
    		'id_user' => $id, 
    		'bank_id' => $bank,
    		'frombank' => $frombank,
    		'jumlah' => $nominalsave,
    		'nama_tr' => $namapemilikbank,
    		'metod_tr' => $metodepembayaran,
    		'bukti_tr' => $buktibayar,
            'type_confirm' => $type
    	);
    	$kirimdata = $this->Process_model->tradd("log_trf_confirmation",$where);
    	// print_r ($this->post());
    	if ($kirimdata == 0) {
    		$res['status'] = false;
			$res['message'] = 'Confrim Failed ';
			$this->response($res);
    	}
    	else if ($kirimdata == 1)
    	{    		
	  //   	$folder 	= './aset/img/buktipembayaran/';
			// $file_size	= $_FILES['buktibayar']['size'];
			// $max_size	= 2000000;
			// $file_name	= $id.'_'.$tokenid.'.jpg';

			// if($file_size > $max_size){											
			// 	// echo "<script>alert('Ukuran file melebihi batas maximum');</script>";
   //              $res['status'] = false;
			// 	$res['message'] = 'File terlalu besar';
			// 	$this->response($res);
			// }
			// else
			// {
				if ($bukti != '') {
					
					$image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'	


						 
					//Write to disk
					file_put_contents("aset/img/buktipembayaran/".$buktibayar,file_get_contents($image));                 

                    $detail_banktujuan  = $this->db->query("SELECT bank_name FROM master_bank WHERE bank_id='$bank'")->row_array()['bank_name'];
                    $getdetail          = $this->db->query("SELECT * FROM tbl_order WHERE order_id='$tokenid'")->row_array();
                    $detail_hargatotal  = $getdetail['total_price'];
                    $detail_hargatotal  = number_format($detail_hargatotal, 0, ".", ".");
                    $detail_nominalsave = number_format($nominalsave, 0, ".", ".");
                    $detail_tanggaltr   = $getdetail['last_update'];
                    $detail_buyerid     = $getdetail['buyer_id'];
                    $detail_buyername   = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$detail_buyerid'")->row_array()['user_name'];
                    $detail_invoice     = $this->db->query("SELECT invoice_id FROM tbl_invoice WHERE order_id='$tokenid'")->row_array()['invoice_id'];

                    //BATAS TANGGAL PEMBAYARAN
                    $dateee             = $detail_tanggaltr;
                   // $user_utcc          = $this->session->userdata('user_utc');
                    $server_utcc        = $this->Rumus->getGMTOffset();
                    $intervall          = $user_utc - $server_utcc;
                    $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$dateee);
                    $tanggaltransaksi->modify("+".$intervall ." minutes");
                    $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');

                    $datetime           = new DateTime($tanggaltransaksi);
                    $datetime->modify('+1 hours');
                    $limitdateee        = $datetime->format('Y-m-d H:i:s');

                    $akhirtanggal       = date_create($limitdateee);
                    $dateelimit         = date_format($akhirtanggal, 'd/m/y');
                    $hariakhir          = date_format($akhirtanggal, 'd');
                    $hariakhir          = date_format($akhirtanggal, 'l');
                    $tahunakhir         = date_format($akhirtanggal, 'Y');
                    $waktuakhir         = date_format($akhirtanggal, 'H:s');
                                                            
                    $dateakhir          = $dateelimit;
                    $sepparatorakhir    = '/';
                    $partsakhir         = explode($sepparatorakhir, $dateakhir);
                    $bulanakhir         = date("F", mktime(0, 0, 0, $partsakhir[1], $partsakhir[2], $partsakhir[0]));

                    $get_detailorder    = $this->db->query("SELECT * FROM tbl_order_detail WHERE order_id='$tokenid'")->result_array();

                    foreach ($get_detailorder as $resultdata) {
                        $request_id     = $resultdata['product_id'];

                        if ($type == "private") {
                            $tutor_id   = $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['tutor_id'];
                            $subject_id = $this->db->query("SELECT subject_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['subject_id'];
                            $getdataclass  = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array(); 

                            $iduserrequest      = $getdataclass['id_user_requester'];  
                            $daterequesttt      = $getdataclass['date_requested'];  
                            $durationrequest    = $getdataclass['duration_requested'];     
                        }
                        else if($type == "group")
                        {
                            $tutor_id   = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['tutor_id'];
                            $subject_id = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['subject_id'];
                            $getdataclass   = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request_grup as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array(); 

                            $iduserrequest      = $getdataclass['id_user_requester']; 
                            $daterequesttt      = $getdataclass['date_requested'];   
                            $durationrequest    = $getdataclass['duration_requested'];     
                        } 
                        else
                        {                                
                            $getdataclass   = $this->db->query("SELECT tu.user_name, ms.subject_name, tc.description as topic, tc.start_time, tc.finish_time, trm.created_at as date_requested, trm.id_requester as id_user_requester FROM tbl_class as tc INNER JOIN tbl_user as tu on tu.id_user=tc.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=tc.subject_id INNER JOIN tbl_request_multicast as trm ON trm.class_id=tc.class_id WHERE tc.class_id='$request_id'")->row_array();  
                            $iduserrequest  = $getdataclass['id_user_requester']; 
                            $daterequesttt  = $getdataclass['date_requested']; 
                            $duratioon      = strtotime($getdataclass['start_time']) - strtotime($getdataclass['finish_time']);
                            $durationrequest= $duratioon;    
                        }
                         
                        $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
                        // $durasi             = $getdataclass['duration_requested'];
                        $server_utcc        = $this->Rumus->getGMTOffset();
                        $intervall          = $user_utc - $server_utcc;
                        $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$daterequesttt);
                        $tanggal->modify("+".$intervall ." minutes");
                        $tanggal            = $tanggal->format('Y-m-d H:i:s');
                        $datelimit          = date_create($tanggal);
                        $dateelimit         = date_format($datelimit, 'd/m/y');
                        $harilimit          = date_format($datelimit, 'd');
                        $hari               = date_format($datelimit, 'l');
                        $tahunlimit         = date_format($datelimit, 'Y');
                        $waktulimit         = date_format($datelimit, 'H:i');   
                        $datelimit          = $dateelimit;
                        $sepparatorlimit    = '/';
                        $partslimit         = explode($sepparatorlimit, $datelimit);
                        $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                        $seconds            = $durationrequest;
                        $hours              = floor($seconds / 3600);
                        $mins               = floor($seconds / 60 % 60);
                        $secs               = floor($seconds % 60);
                        $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

                        $kotakdetail = 
                        "<tr>
                            <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['user_name']."</td>
                            <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['subject_name']."</td>
                            <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['topic']."</td>
                            <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit."</td>
                            <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td> 
                        </tr>";   
                    }                     


                    // Open connection
	                $ch = curl_init();

	                // Set the url, number of POST vars, POST data
	                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	                curl_setopt($ch, CURLOPT_POST, true);
	                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_confirm','content' => array("detail_buyername" => $detail_buyername, "detail_banktujuan" => $detail_banktujuan, "detail_hargatotal" => $detail_hargatotal, "detail_nominalsave" => $detail_nominalsave, "frombank" => $frombank, "namapemilikbank" => $namapemilikbank, "metodepembayaran" => $metodepembayaran, "hariakhir" => $hariakhir, "hari" => $hari, "bulanakhir" => $bulanakhir, "tahunakhir" => $tahunakhir, "waktuakhir" => $waktuakhir, "detail_invoice" => $detail_invoice, "kotakdetail" => $kotakdetail) )));

	                // Execute post
	                $result = curl_exec($ch);
	                curl_close($ch);
	                // echo $result;
	                
	                $res['status'] = true;
					$res['message'] = 'Success';
					$this->response($res);
						}

			// }
		}
		else
		{
			$res['status'] = false;
			$res['message'] = 'Gagal';
			$this->response($res);
		}
	}

	public function myInvoice_post()
	{
		$myid               = $this->post('id_user');           
		$anak               = $this->db->query("SELECT * FROM tbl_profile_kid WHERE parent_id='$myid'")->result_array();
		$utc 				= $this->post('utc');
		$user_utc 			= $this->Rumus->getGMTOffset('minute', $utc);
		$q_anak = "";
		if ($this->get('access_token') != '') {
			
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
	        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
	            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
	            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
		    }

			if(!empty($anak)){
			    foreach ($anak as $key => $value) {
			        $q_anak.= " OR tor.buyer_id='".$value['kid_id']."' ";
			    }
			}
			    
			$getdatainvoice   = $this->db->query("SELECT * FROM tbl_invoice as ti INNER JOIN tbl_order as tor ON ti.order_id=tor.order_id WHERE tor.buyer_id='$myid' $q_anak ORDER BY ti.created_at DESC")->result_array();
			if (empty($getdatainvoice)) {
				$res['status'] = false;
				$res['message'] = 'failed empty';
				$res['data'] = null;
				$this->response($res);
			}
			else
			{
				foreach ($getdatainvoice as $row => $v) {
	                $invoice_id     = $v['invoice_id'];
	                // $product_id     = $v['product_id'];
	                $order_id       = $v['order_id'];
	                $order_status   = $v['order_status'];

	                $get_orderdetail = $this->db->query("SELECT tod.*, tr.subject_id as tr_subject_id, tr.tutor_id as tr_tutor_id, tr.approve as tr_approve, tr.topic as tr_topic, trg.subject_id as trg_subject_id, trg.tutor_id as trg_tutor_id, trg.approve as trg_approve, trg.topic as trg_topic, tc.subject_id as trm_subject_id, tc.tutor_id as trm_tutor_id, trm.status as trm_approve, tc.description as trm_topic, tpp.name as program_name, tpp.description as trp_topic, trp.status as trp_approve, tpp.channel_id as trp_channel_id FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id LEFT JOIN (tbl_request_program as trp INNER JOIN tbl_price_program as tpp ON trp.program_id=tpp.list_id ) ON trp.request_id=tod.product_id WHERE tod.order_id='$order_id'")->result_array(); 
	                
	                $harga          = $v['total_price'];
	                $hargaprice     = number_format($harga, 0, ".", ".");                

	                $status = null;
	                foreach ($get_orderdetail as $key => $value) {
	                    // $subject_id     = $value['tr_subject_id'] != null ? $value['tr_subject_id'] : ( $value['trg_subject_id'] != null ? $value['trg_subject_id'] : $value['trm_subject_id']);
	                    // $tutor_id       = $value['tr_tutor_id'] != null ? $value['tr_tutor_id'] : ( $value['trg_tutor_id'] != null ? $value['trg_tutor_id'] : $value['trm_tutor_id']);
	                    // $status         = $value['tr_approve'] != null ? $value['tr_approve'] : ( $value['trg_approve'] != null ? $value['trg_approve'] : $value['trm_approve']);
	                    $subject_id     = $value['tr_subject_id'] != null ? $value['tr_subject_id'] : ( $value['trg_subject_id'] != null ? $value['trg_subject_id'] : ($value['trm_subject_id'] != null ? $value['trm_subject_id'] : ''));
                        $tutor_id       = $value['tr_tutor_id'] != null ? $value['tr_tutor_id'] : ( $value['trg_tutor_id'] != null ? $value['trg_tutor_id'] : ( $value['trm_tutor_id'] != null ? $value['trm_tutor_id'] : ''));
                        $status         = $value['tr_approve'] != null ? $value['tr_approve'] : ( $value['trg_approve'] != null ? $value['trg_approve'] : ( $value['trm_approve'] != null ? $value['trm_approve'] : $value['trp_approve']));
                        $topic          = $value['tr_topic'] != null ? $value['tr_topic'] : ( $value['trg_topic'] != null ? $value['trg_topic'] : ($value['trm_topic'] != null ? $value['trm_topic'] : $value['trp_topic']));

                        if ($subject_id != '') {
		                    $datasub   = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array();
		                    $subject_name   = $datasub['subject_name'];
		                    // if ($datasub['jenjang_level'] != 0) {
		                    // 	$jenjang_data   = $datasub['jenjang_name'].' '.$datasub['jenjang_level'];
		                    // } else {
		                    // 	$jenjang_data   = $datasub['jenjang_name'];
		                    // }
		                }else
		                {
		                	$subject_name  = $value['program_name'];
                            $jenjang_data  = '';
		                }
		                if ($tutor_id != '') {
		                    $data_tutor     = $this->db->query("SELECT first_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
		                    $tutor_name     = $data_tutor['first_name'];
		                    $tutor_image    = $data_tutor['user_image'];
		                }
		                else
		                {
		                	$channel_id     = $value['trp_channel_id'];
                            $data_channel   = $this->db->query("SELECT channel_name,channel_logo FROM master_channel WHERE channel_id='$channel_id'")->row_array();
                            $tutor_name     = $data_channel['channel_name'];
                            $tutor_image    = $data_channel['channel_logo'];
		                }

	                    $get_orderdetail[$key]['subject_name'] 		= $subject_name;
	                    $get_orderdetail[$key]['jenjang_name'] 		= null;
	                    $get_orderdetail[$key]['tutor_name'] 		= $tutor_name;
	                    $get_orderdetail[$key]['tutor_image'] 		= $tutor_image;
	                }

	                $cekKonfirmasi 		= $this->db->query("SELECT * FROM log_trf_confirmation WHERE trx_id='$order_id'")->row_array();
	                if (empty($cekKonfirmasi)) {
	                	$getdatainvoice[$row]['trf_confirmation'] 	= true;
	                }
	                else
	                {
	                	$getdatainvoice[$row]['trf_confirmation'] 	= false;
	                }
		            $UnixCode           = $v['unix_code'];
		            $hargakelas         = $v['total_price']-$UnixCode;               
                    $server_utcc        = $this->Rumus->getGMTOffset();
                    $intervall          = $user_utc - $server_utcc;

	                $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$v['payment_due']);
		            $tanggaltransaksi->modify("+".$intervall ." minutes");
		            $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');

	                $tanggalcreate   = DateTime::createFromFormat ('Y-m-d H:i:s',$v['created_at']);
		            $tanggalcreate->modify("+".$intervall ." minutes");
		            $tanggalcreate   = $tanggalcreate->format('Y-m-d H:i:s');

		            $dateee             = $tanggaltransaksi;
		            $datetime           = new DateTime($dateee);
		            $datetime->modify('+1 hours');
		            $limitdateee        = $datetime->format('Y-m-d H:i:s');

		            $datelimit          = date_create($limitdateee);
		            $dateelimit         = date_format($datelimit, 'd/m/y');
		            $harilimit          = date_format($datelimit, 'd');
		            $hari               = date_format($datelimit, 'l');
		            $tahunlimit         = date_format($datelimit, 'Y');
		            $waktulimit         = date_format($datelimit, 'H:s');
		                                                    
		            $datelimit          = $dateelimit;
		            $sepparatorlimit    = '/';
		            $partslimit         = explode($sepparatorlimit, $datelimit);
		            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

		            $getdatainvoice[$row]['tanggaltransaksi'] 	= $tanggalcreate;
		            $getdatainvoice[$row]['expireddate']	 	= $tanggaltransaksi;
	                $getdatainvoice[$row]['order_detail'] 		= $get_orderdetail;
			        $getdatainvoice[$row]['hargakelas'] 		= $hargakelas;
	            }
			}
			$res['status'] = true;
			$res['message'] = 'Successfully';
			$res['data'] = $getdatainvoice;
			$this->response($res);
		}

		$res['status'] = false;
		$res['message'] = 'Field incomplete ';
		ending:
		$this->response($res);
	}

	public function report_teaching_post()
	{
		$myid               = $this->post('id_user');           
		if ($this->get('access_token') != '') {
			
			$retok = $this->verify_access_token($this->get('access_token'));
	        if($retok == false){
	            $res = array("status" => false, "code" => -400, "message" => "invalid token."); 
	            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	            goto ending;
		    }

			$getreport   = $this->db->query("SELECT tcd.order_id, tcd.final_price, tcd.final_pricetutor, tcp.harga, tc.* FROM tbl_class as tc LEFT JOIN tbl_order_detail as tcd on tc.class_id=tcd.class_id LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id WHERE tc.tutor_id='$myid' AND tc.class_type!='multicast_channel_paid' AND tc.class_type!='multicast' AND tc.class_type!='private_channel' AND tc.class_type!='group_channel' ORDER by start_time DESC")->result_array();
			if (empty($getreport)) {
				$res['status'] = false;
				$res['message'] = 'failed empty';
				$res['data'] = null;
				$this->response($res);
			}
			else
			{
				$user_utc		= $this->Rumus->getGMTOffset('minute',$this->post('utc'));
				$server_utcc    = $this->Rumus->getGMTOffset();				
				$intervall      = $user_utc - $server_utcc;
				$active_balance	= $this->db->query("SELECT active_balance FROM uangsaku WHERE id_user='$myid'")->row_array()['active_balance'];
				$totalkelas		= $this->db->query("SELECT count(*) as total FROM tbl_class as tc LEFT JOIN tbl_order_detail as tcd on tc.class_id=tcd.class_id LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id WHERE tc.tutor_id='$myid' AND tc.class_type!='multicast_channel_paid' AND tc.class_type!='multicast' AND tc.class_type!='private_channel' AND tc.class_type!='group_channel'")->row_array();
				$totalpending	= $this->db->query("SELECT * FROM tbl_class as tc LEFT JOIN tbl_order_detail as tcd on tc.class_id=tcd.class_id LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id WHERE tc.tutor_id='$myid' AND tc.class_type!='multicast_channel_paid' AND tc.class_type!='multicast' AND tc.class_type!='private_channel' AND tc.class_type!='group_channel'")->result_array();
        		$countpending   = 0;
        		foreach ($totalpending as $v) {
        			$order_id 	= $v['order_id'];
        			$d 			= $this->db->query("SELECT order_status FROM tbl_order where order_id='$order_id'")->row_array();
        			if (!empty($d)) {
        				$status = $d['order_status'];
        				if ($status != 1) {
        					$countpending = $countpending+1;
        				}                        				
        			}
        			else
        			{
        				$countpending = $countpending+1;
        			}
        		}
				foreach ($getreport as $row => $v) {	
					$datetime       = DateTime::createFromFormat ('Y-m-d H:i:s',$v['start_time']);
		            $datetime->modify("+".$intervall ." minutes");
		            $datetime       = $datetime->format('l, d M Y H:i');
		            $subject_id		= $v['subject_id'];
					$data_subject 	= $this->db->query("SELECT * FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.subject_id='$subject_id'")->row_array();
					$duration 		= strtotime($v['finish_time']) - strtotime($v['start_time']);
					$convertduration= gmdate("H:i", $duration). ' hour';
					$price          = $v['final_pricetutor'] != null ? $v['final_pricetutor'] : $v['harga'];
					if($v['class_type'] == 'private'){ $type='Private'; }else if($v['class_type'] == 'group'){ $type='Group'; } else if($v['class_type'] == 'multicast_paid'){ $type='Multicast Paid'; } else { $type='-';}
					$datepaid 		= $this->db->query("SELECT approval_time FROM tbl_order WHERE order_id='$v[order_id]'")->row_array()['approval_time'];
					if (!empty($datepaid)) {												
			            $tanggal        = DateTime::createFromFormat ('Y-m-d H:i:s',$datepaid);
			            $tanggal->modify("+".$intervall ." minutes");
			            $tanggal        = $tanggal->format('Y-m-d H:i:s');
		        	}
		        	else
		        	{
		        		$tanggal 	= null;
		        	}

		            $getreportr[$row]['datetime'] 	= $datetime;
		            $getreportr[$row]['subject'] 	= $data_subject['subject_name'];
		            $getreportr[$row]['subject_icon'] 	= $data_subject['icon'];
		            $getreportr[$row]['topic'] = $v['description'];
		            if ($data_subject['jenjang_level'] == "0") {
		            	$getreportr[$row]['jenjang']	= $data_subject['jenjang_name'];
		            } else {
		            	$getreportr[$row]['jenjang']	= $data_subject['jenjang_name'].' '.$data_subject['jenjang_level'];
		            }
		            $getreportr[$row]['duration'] 	= $convertduration;
		            $getreportr[$row]['price'] 		= $price;
		            $getreportr[$row]['type'] 		= $type;
		            $getreportr[$row]['date_paid'] 	= $tanggal;
	            }
			}
			$res['status'] = true;
			$res['message'] = 'Successfully';
			$res['uangtutor'] = $active_balance;
			$res['totalkelas'] = $totalkelas['total'];
			$res['totalpending'] = $countpending;
			$res['data'] = $getreportr;			
			$this->response($res);
		}

		$res['status'] = false;
		$res['message'] = 'Field incomplete ';
		ending:
		$this->response($res);
	}

	public function changePassword_post()
	{
		$id_user 	= $this->post('id_user');
		$oldpass 	= $this->post('oldpass');
		$newpass 	= $this->post('newpass');
		$type 		= $this->post('type');
		$retok 	 	= $this->verify_access_token($this->get('access_token'));
		$user_name 	= $retok['user_name'];
		$email 	 	= $retok['email'];

        if($retok == false){
            $res = array("status" => false, "code" => -400, "message" => "invalid token."); 
            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
            // goto ending;
	    }

		$cek_pass = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$id_user'")->row_array();
		if ($type == "") {
			if (empty($cek_pass) || !password_verify($oldpass, $cek_pass['password'])) {
				//kembalikan password lama tidak ada
				$res['status'] = false;
				$res['message'] = 'Old Password didnt match';
				$this->response($res);
			} else {
				goto cp;
			}
		} else {
			cp:
			$passbaru = password_hash($newpass,PASSWORD_DEFAULT);
			$update_w_pass = $this->db->simple_query("UPDATE tbl_user SET password='$passbaru' WHERE id_user='$id_user'");

			if ($update_w_pass) {

				if ($type == "Anak") {
					$nameanak  	= $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$id_user'")->row_array()['user_name'];
					$isi 		= "Mohon diperhatikan bahwa kita menerima pemberitahuan perubahan password pada akun Anak Anda : <strong>".$nameanak."</strong>.";
					$isi_ing	= "Kindly be informed that we received notification of password update on your Kid's account : <strong>".$nameanak."</strong>.";
				}
				else
				{
					$isi 	= "Mohon diperhatikan bahwa kita menerima pemberitahuan perubahan password pada akun Anda :";
					$isi_ing	= "Kindly be informed that we received notification of password change on your account.";
				}
				// Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_changePassword','content' => array("email" => "$email", "user_name" => "$user_name", "isi" => "$isi", "isi_ing" => "$isi_ing") )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
                $res['status'] = true;
				$res['message'] = 'Password has been changed';
				$this->response($res);
				

			}else{
				//kembalian gagal update password
				$res['status'] = false;
				$res['message'] = 'Failed to update password';
				$this->response($res);
			}
		}

		
	}

	public function submitMethodPayment_post()
    {
        $id_user = $this->post('id_user');
        $order_id = $this->post('order_id');
        $payment_type = $this->post('payment_type');
        $bank_id = $this->post('bank_id');
        $nama_akun = $this->post('nama_akun');
        $nomer_rekening = $this->post('nomer_rekening');

        $cekrek = $this->db->query("SELECT * FROM tbl_rekening WHERE id_user='$id_user' AND nomer_rekening='$nomer_rekening' AND bank_id='$bank_id'")->row_array();
        if (empty($cekrek)) {
            $bank_name = $this->db->query("SELECT bank_name FROM master_bank WHERE bank_id='$bank_id'")->row_array()['bank_name'];
            $addbank = $this->db->query("INSERT INTO tbl_rekening (id_user,nama_akun,nomer_rekening,bank_id,nama_bank) VALUES ('$id_user','$nama_akun','$nomer_rekening','$bank_id','$bank_name')");    
            $lastidrek  = $this->db->select('id_rekening')->order_by('id_rekening','desc')->limit(1)->get('tbl_rekening')->row('id_rekening');        
            if ($addbank) {
                $updatemethod = $this->db->query("UPDATE tbl_invoice SET payment_type='$payment_type', id_rekening='$lastidrek' WHERE order_id='$order_id'");
                $json = array("status" => 1,"message" => "sukses" ,"statuz" => true);
				$this->response($json);
            }
            else
            {
                $json = array("status" => 0,"message" => "failed","statuz" => false);
				$this->response($json);
            }
        }
        else
        {
            $idrek = $cekrek['id_rekening'];
            $updatemethod = $this->db->query("UPDATE tbl_invoice SET payment_type='$payment_type', id_rekening='$idrek' WHERE order_id='$order_id'");
            if ($updatemethod) {                
                $json = array("status" => 1,"message" => "sukses" ,"statuz" => true);
				$this->response($json);
            }
            else
            {
                $json = array("status" => 0,"message" => "failed","statuz" => false);
				$this->response($json);
            }            
        }
        $json = array("status" => 0,"message" => "failed","statuz" => false);
		$this->response($json);
    }
	
	public function getComplaintCount_post()
	{
		$id_user = $this->post('id_user');
    	if ($this->get('access_token') != '') {

			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
            if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
                $rets = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
                $rets['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
                goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
	        }
    		$ttlcomplain = $this->db->query("SELECT count(*) as total FROM tbl_class_resolution WHERE resolution_status=0 AND id_user='$id_user'")->row_array()['total'];
	    	if (!empty($ttlcomplain)) 
	        {
	        	$res['status'] = true;
				$res['message'] = 'Data';
				$res['count'] = $ttlcomplain;
				$this->response($res);
	        }
	        else
	        {
				$res['status'] = false;
				$res['message'] = 'Zero';
				$this->response($res);
			}
		}
		
		$res['status'] = false;
		$res['message'] = 'Field incomplete ';
		ending:
		$this->response($res);

	}

	public function tps_me_r_post()
	{
		$session_id = $this->post('class_id');
		$id_user = $this->post('id_user');
		$iduser = $this->post('id_user');
		
			$DB2 = $this->load->database('db2', TRUE); // SAY SEA SERVER CLASSPLAY DB
				// $DB_SG = $this->load->database('db_sg', TRUE); // SAY SEA SERVER CLASSPLAY DB

				$cm_continent = "id";
				// $iduser = $this->session->userdata('id_user');
				// $andro 	= $this->input->get('p');
				$andro = 'android';
				$ch = "indiclass";
				if ($andro != 'android') {
					$linkback = BASE_URL().'MyClass';
				}
				else
				{
					$linkback = '';
				}

				if ($iduser == null) {				
					$iduser = $this->input->get('id_user');
				}
				$templateweb = $this->input->get('t');			

				$db_res = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$room_active_janus = $this->db->query("SELECT * FROM temp_active_room WHERE class_id='$session_id'")->row_array();
				if(!empty($room_active_janus)){
					$cm_continent = $room_active_janus['cm_continent'];
				}else{
					// TAKE ALL PARTICIPANT IN THAT CLASS
					$visible = json_decode($db_res['participant'],true);
					$incoming_kuota = 0;
					$local_region = 0;
					$interlocal_region = 0;
					$chosen_janus = "";

					$country_code = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='".$db_res['tutor_id']."'")->row_array()['user_countrycode'];
					if($country_code == "+62" || $country_code == "62"){
						$local_region++;
					}else{
						$interlocal_region++;
					}
					$incoming_kuota++;

					foreach ($visible['participant'] as $k => $value) {
						$id_user = $value['id_user'];
						$data_ccode_utype = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='$id_user'")->row_array();
						$country_code = $data_ccode_utype['user_countrycode'];
						$usertype_id = $data_ccode_utype['usertype_id'];
						if($usertype_id != "student kid"){
							if($country_code == "+62" || $country_code == "62"){
								$local_region++;
							}else{
								$interlocal_region++;
							}
						}
						$incoming_kuota++;
					}

					$ttl_active_kuota = $this->db->query("SELECT janus, SUM(kuota) as kuota FROM temp_active_room GROUP BY janus")->result_array();
					if($local_region > $interlocal_region){
						$cm_continent = "id";
						// PRESENTASE TERBESAR INDONESIA
						// $list_janus = array("c3.classmiles.com" => 0,"c1.classmiles.com" => 0,"c2.classmiles.com" => 0);
						// $list_janus = array("c4.classmiles.com" => 0, "c3.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 200 && ($value+$incoming_kuota) <= 200){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}else{
						$cm_continent = "id";
						// $list_janus = array("c102.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 200 && ($value+$incoming_kuota) <= 200){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}
					$this->db->simple_query("INSERT INTO temp_active_room VALUES(NULL,'$cm_continent','$chosen_janus','$session_id',$incoming_kuota,now());");
					$this->janus_server = $chosen_janus;
				}

				if($cm_continent == "id"){
					$student = $DB2->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();					
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						// echo $hash_id;
						$DB2->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
					}
					$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

					$qr = "";
					$qr_data = "";
					$setter = "";
					foreach ($data_diri as $key => $value) {
						if($key == "username" && $value == NULL){

						}else{
							$qr.= $key.", ";
							$qr_data.= "'$value', ";
							$setter.= "$key='$value', ";
						}
					}
					$qr = rtrim($qr, ", ");
					$qr_data = rtrim($qr_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
					if(!$insert_tbl_user) {
						$DB2->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
					}

					$classplay_exist = $DB2->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
					if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
						$qc = "";
						$qc_data = "";
						$setter = "";
						foreach ($db_res as $key => $value) {
							$qc.= $key.", ";
							$qc_data.= "'".$this->db->escape_str($value)."', ";
							$setter.= "$key='$value', ";
						}
						$qc = rtrim($qc, ", ");
						$qc_data = rtrim($qc_data, ", ");
						$setter = rtrim($setter, ", ");
						$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
						if(!$insert_tbl_user){
							$DB2->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
						}
					}
					
				}

				$load = $this->lang->line('logsuccess');					
				$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$action = "$load ".$dpt_subject_name['name'];
				$log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$iduser')");
			// $this->session->set_userdata('p',$andro);
			// header('Location: https://classmiles.com/classroom/broadcast_wd?access_session='.$hash_id);
			// echo "<script>alert('halo ini di master');</script>";
			$res['status'] = true;
			$res['message'] = 'Zero';
			$res['cm'] = $cm_continent;
			$res['access_session'] = $hash_id;
			$this->response($res);
				// header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_all?access_session='.$hash_id.'&p='.$andro);
			
	}

	public function startQuiz_post($value='')
	{
		$bsid 		= $this->post('bsid');
		$id_user 	= $this->post('id_user');
		$time_start = $this->post('time_start');
		$chk 		= $this->db->query("SELECT * FROM master_banksoal WHERE bsid='$bsid'")->row_array();
		if (empty($chk)) {
			$res['status'] 		= true;
			$res['message'] 	= 'Error';
			$res['data'] 		= null;	
		}
		else
		{
			$subject_id 		= $chk['subject_id'];
			$duration 			= $chk['duration'];
			$list_soal 			= json_decode($chk['list_soal'],true);
			shuffle($list_soal);
			$jumlah_soal 		= count($list_soal);
			$participant = [];
				
			foreach($list_soal as $key => $va) {
				$answer_system = $this->db->query("SELECT answer_value FROM master_banksoal_archive WHERE soal_uid = '$va' ")->row_array()['answer_value'];
				$participant[$key]['soal_uid'] = (int)$va;
				$participant[$key]['answer_student'] = null;
				$participant[$key]['answer_system'] = $answer_system;
			}
			$answer_question 	= json_encode($participant);			
			$chk['answer'] 		= $answer_question;
			$chk['jumlah_soal'] = $jumlah_soal;
			$res['duration'] 	= $duration;

			//CHECKIN IF READY
			$cekQuizUser 		= $this->db->query("SELECT * FROM tbl_quiz WHERE id_user='$id_user' AND bsid = '$bsid' AND status = '2'")->row_array();
			if ($cekQuizUser['status'] == 2) {
				$ck_timestart 		= $cekQuizUser['time_start'];				
				$quiz_id 			= $cekQuizUser['quiz_id'];				
				$quizTbl 			= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
				if ($quizTbl['time_start'] == "0000-00-00 00:00:00") {
					$this->db->query("UPDATE tbl_quiz SET time_start = '$time_start' WHERE quiz_id = '$quiz_id'");
				}
				$answer_questionnn	= json_decode($quizTbl['answer_question'],true);

				$res['status'] 		= true;
				$res['message'] 	= 'Data';
				$res['quiz_id']		= $quiz_id;
				$res['jumlah_soal'] = $jumlah_soal;
				$res['time_start']  = $quizTbl['time_start'];

				$sisa_soal 			= 0;
				$belum_terjawab 	= 0;
				foreach ($answer_questionnn as $keyaw => $qa) {
					if ($qa['answer_student'] == null) {
						$belum_terjawab = $belum_terjawab+1;						
					}
				}
				$sisa_soal = $jumlah_soal-$belum_terjawab;
				$res['sisa_soal'] 	= $belum_terjawab;
				$res['belum_terjawab'] = $belum_terjawab;
				$aksiFinish = 1;
				foreach ($answer_questionnn as $keyaa => $qw) {
					if($qw['answer_student'] == null){
						$soal_uid   = $qw['soal_uid'];
						$getDetail 	= $this->db->query("SELECT * FROM master_banksoal_archive WHERE soal_uid = '$soal_uid'")->row_array();
						$options = json_decode($getDetail['options'],true);

						$res['getDetail'] = $getDetail;
						$res['getDetail']['options'] = $options;
						$aksiFinish = 0;
						break;
					}
				}		
				$finish 	 = false;
				$total_benar = 0;
				$total_salah = 0;
				if ($aksiFinish == 1) {
					$getCheckQuiz 	= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
					$answer_student	= json_decode($getCheckQuiz['answer_question'],true);
					foreach ($answer_student as $ka => $as) {
						if ($as['answer_student'] != null) {
							if ($as['answer_student'] == $as['answer_system']) {
								$total_benar = $total_benar+1;
							}
							else
							{
								$total_salah = $total_salah+1;	
							}
						}
					}

					$score = $total_benar / $jumlah_soal * 100; 
					$this->db->query("UPDATE tbl_quiz SET correct_answer = '$total_benar', 	wrong_answer = '$total_salah', status = '2' WHERE quiz_id = '$quiz_id'");
					$this->db->query("UPDATE tbl_quiz SET status = 1 WHERE quiz_id = '$quiz_id'");
					$res['total_benar']	= $total_benar;
					$res['total_salah'] = $total_salah;
					$res['score'] 		= $score;
					$finish 			= true;
				}
				$res['finish'] 		= $finish;	
			}
			else
			{
				$ckak = $this->db->query("SELECT count(*) as jml FROM tbl_quiz as tq WHERE tq.status = '2' AND tq.id_user='$id_user' AND bsid='$bsid'")->row_array();
				if ($ckak['jml'] < 1) {									
					// if (empty($cekQuizUser)) {
					$addQuiz 			= $this->db->query("INSERT INTO tbl_quiz (bsid,id_user,subject_id,time_start,duration,answer_question,status) VALUES ('$bsid','$id_user','$subject_id','$time_start','$duration','$answer_question','2')");				
					if ($addQuiz) {
						$quiz_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
						$quizTbl 			= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
						$answer_questionnn	= json_decode($quizTbl['answer_question'],true);

						$res['status'] 		= true;
						$res['message'] 	= 'Data';
						$res['quiz_id']		= $quiz_id;
						$res['jumlah_soal'] = $jumlah_soal;
						$res['time_start'] = $time_start;

						$sisa_soal 			= 0;
						$belum_terjawab 	= 0;
						foreach ($answer_questionnn as $keyaw => $qa) {
							if ($qa['answer_student'] == null) {
								$belum_terjawab = $belum_terjawab+1;						
							}
						}
						$sisa_soal = $jumlah_soal-$belum_terjawab;
						$res['sisa_soal'] 	= $sisa_soal;
						$res['belum_terjawab'] = $belum_terjawab;
						$aksiFinish = 1;
						foreach ($answer_questionnn as $keyaa => $qw) {
							if($qw['answer_student'] == null){
								$soal_uid   = $qw['soal_uid'];
								$getDetail 	= $this->db->query("SELECT * FROM master_banksoal_archive WHERE soal_uid = '$soal_uid'")->row_array();
								$options = json_decode($getDetail['options'],true);

								$res['getDetail'][$keyaa] = $getDetail;
								$res['getDetail'][$keyaa]['options'] = $options;
								// $res['getDetail'] = $getDetail;
								$aksiFinish = 0;
								break;
							}							
						}	
						$finish 	 = false;
						$total_benar = 0;
						$total_salah = 0;
						if ($aksiFinish == 1) {
							$getCheckQuiz 	= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
							$answer_student	= json_decode($getCheckQuiz['answer_question'],true);
							foreach ($answer_student as $ka => $as) {
								if ($as['answer_student'] != null) {
									if ($as['answer_student'] == $as['answer_system']) {
										$total_benar = $total_benar+1;
									}
									else
									{
										$total_salah = $total_salah+1;	
									}
								}
							}

							$score = $total_benar / $jumlah_soal * 100; 
							$this->db->query("UPDATE tbl_quiz SET correct_answer = '$total_benar', 	wrong_answer = '$total_salah', status = '1' WHERE quiz_id = '$quiz_id'");
							$res['total_benar']	= $total_benar;
							$res['total_salah'] = $total_salah;
							$res['score'] 		= $score;
							$finish 			= true;
						}
						$res['finish'] 		= $finish;
						$this->response($res);
					}	
					else
					{
						$res['status'] 	= false;
						$res['message'] = 'No Data';
						$res['data'] 	= null;
						$this->response($res);	
					}
				}			
				else
				{
					$quiz_id 			= $cekQuizUser['quiz_id'];
					$quizTbl 			= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
					if ($quizTbl['time_start'] == "0000-00-00 00:00:00") {
						$this->db->query("UPDATE tbl_quiz SET time_start = '$time_start' WHERE quiz_id = '$quiz_id'");
					}
					$answer_questionnn	= json_decode($quizTbl['answer_question'],true);

					$res['status'] 		= true;
					$res['message'] 	= 'Data';
					$res['quiz_id']		= $quiz_id;
					$res['jumlah_soal'] = $jumlah_soal;
					$res['time_start']  = $quizTbl['time_start'];

					$sisa_soal 			= 0;
					$belum_terjawab 	= 0;
					foreach ($answer_questionnn as $keyaw => $qa) {
						if ($qa['answer_student'] == null) {
							$belum_terjawab = $belum_terjawab+1;						
						}
					}
					$sisa_soal = $jumlah_soal-$belum_terjawab;
					$res['sisa_soal'] 	= $belum_terjawab;
					$res['belum_terjawab'] = $belum_terjawab;
					$aksiFinish = 1;
					foreach ($answer_questionnn as $keyaa => $qw) {
						if($qw['answer_student'] == null){
							$soal_uid   = $qw['soal_uid'];
							$getDetail 	= $this->db->query("SELECT * FROM master_banksoal_archive WHERE soal_uid = '$soal_uid'")->row_array();
							$options = json_decode($getDetail['options'],true);

							$res['getDetail'] = $getDetail;
							$res['getDetail']['options'] = $options;
							$aksiFinish = 0;
							break;
						}
					}		
					$finish 	 = false;
					$total_benar = 0;
					$total_salah = 0;
					if ($aksiFinish == 1) {
						$getCheckQuiz 	= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
						$answer_student	= json_decode($getCheckQuiz['answer_question'],true);
						foreach ($answer_student as $ka => $as) {
							if ($as['answer_student'] != null) {
								if ($as['answer_student'] == $as['answer_system']) {
									$total_benar = $total_benar+1;
								}
								else
								{
									$total_salah = $total_salah+1;	
								}
							}
						}

						$score = $total_benar / $jumlah_soal * 100; 
						$this->db->query("UPDATE tbl_quiz SET correct_answer = '$total_benar', 	wrong_answer = '$total_salah', status = '2' WHERE quiz_id = '$quiz_id'");
						$this->db->query("UPDATE tbl_quiz SET status = 1 WHERE quiz_id = '$quiz_id'");
						$res['total_benar']	= $total_benar;
						$res['total_salah'] = $total_salah;
						$res['score'] 		= $score;
						$finish 			= true;
					}
					$res['finish'] 		= $finish;					
				}
			}
			$this->response($res);
		}
	}

	public function startQuizAndroid_post($value='')
	{
		$bsid 		= $this->post('bsid');
		$id_user 	= $this->post('id_user');
		$time_start = $this->post('time_start');
		$chk 		= $this->db->query("SELECT * FROM master_banksoal WHERE bsid='$bsid'")->row_array();
		if (empty($chk)) {
			$res['status'] 		= true;
			$res['message'] 	= 'Error';
			$res['data'] 		= null;	
		}
		else
		{
			$subject_id 		= $chk['subject_id'];
			$duration 			= $chk['duration'];
			$list_soal 			= json_decode($chk['list_soal'],true);
			shuffle($list_soal);
			$jumlah_soal 		= count($list_soal);
			$participant = [];
				
			foreach($list_soal as $key => $va) {
				$answer_system = $this->db->query("SELECT answer_value FROM master_banksoal_archive WHERE soal_uid = '$va' ")->row_array()['answer_value'];
				$participant[$key]['soal_uid'] = (int)$va;
				$participant[$key]['answer_student'] = null;
				$participant[$key]['answer_system'] = $answer_system;
			}
			$answer_question 	= json_encode($participant);			
			$chk['answer'] 		= $answer_question;
			$chk['jumlah_soal'] = $jumlah_soal;
			$res['duration'] 	= $duration;

			//CHECKIN IF READY
			$cekQuizUser 		= $this->db->query("SELECT * FROM tbl_quiz WHERE id_user='$id_user' AND bsid = '$bsid' AND status = '2'")->row_array();
			if ($cekQuizUser['status'] == 2) {
				$ck_timestart 		= $cekQuizUser['time_start'];				
				$quiz_id 			= $cekQuizUser['quiz_id'];				
				$quizTbl 			= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
				if ($quizTbl['time_start'] == "0000-00-00 00:00:00") {
					$this->db->query("UPDATE tbl_quiz SET time_start = '$time_start' WHERE quiz_id = '$quiz_id'");
				}
				$answer_questionnn	= json_decode($quizTbl['answer_question'],true);

				$res['status'] 		= true;
				$res['message'] 	= 'Data';
				$res['quiz_id']		= $quiz_id;
				$res['jumlah_soal'] = $jumlah_soal;
				$res['time_start']  = $quizTbl['time_start'];

				$sisa_soal 			= 0;
				$belum_terjawab 	= 0;
				foreach ($answer_questionnn as $keyaw => $qa) {
					if ($qa['answer_student'] == null) {
						$belum_terjawab = $belum_terjawab+1;						
					}
				}
				$sisa_soal = $jumlah_soal-$belum_terjawab;
				$res['sisa_soal'] 	= $belum_terjawab;
				$res['belum_terjawab'] = $belum_terjawab;
				$aksiFinish = 1;
				foreach ($answer_questionnn as $keyaa => $qw) {
					// if($qw['answer_student'] == null){
						$soal_uid   = $qw['soal_uid'];
						$getDetail 	= $this->db->query("SELECT * FROM master_banksoal_archive WHERE soal_uid = '$soal_uid'")->row_array();
						$options = json_decode($getDetail['options'],true);

						$res['getDetail'][$keyaa] = $getDetail;
						$res['getDetail'][$keyaa]['options'] = $options;
						$aksiFinish = 0;
						// break;
					// }
				}		
				$finish 	 = false;
				$total_benar = 0;
				$total_salah = 0;
				if ($aksiFinish == 1) {
					$getCheckQuiz 	= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
					$answer_student	= json_decode($getCheckQuiz['answer_question'],true);
					foreach ($answer_student as $ka => $as) {
						// if ($as['answer_student'] != null) {
							if ($as['answer_student'] == $as['answer_system']) {
								$total_benar = $total_benar+1;
							}
							else
							{
								$total_salah = $total_salah+1;	
							}
						// }
					}

					$score = $total_benar / $jumlah_soal * 100; 
					$this->db->query("UPDATE tbl_quiz SET correct_answer = '$total_benar', 	wrong_answer = '$total_salah', status = '2' WHERE quiz_id = '$quiz_id'");
					$this->db->query("UPDATE tbl_quiz SET status = 1 WHERE quiz_id = '$quiz_id'");
					$res['total_benar']	= $total_benar;
					$res['total_salah'] = $total_salah;
					$res['score'] 		= $score;
					$finish 			= true;
				}
				$res['finish'] 		= $finish;	
			}
			else
			{
				$ckak = $this->db->query("SELECT count(*) as jml FROM tbl_quiz as tq WHERE tq.status = '2' AND tq.id_user='$id_user' AND bsid='$bsid'")->row_array();
				if ($ckak['jml'] < 1) {									
					// if (empty($cekQuizUser)) {
					$addQuiz 			= $this->db->query("INSERT INTO tbl_quiz (bsid,id_user,subject_id,time_start,duration,answer_question,status) VALUES ('$bsid','$id_user','$subject_id','$time_start','$duration','$answer_question','2')");				
					if ($addQuiz) {
						$quiz_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
						$quizTbl 			= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
						$answer_questionnn	= json_decode($quizTbl['answer_question'],true);

						$res['status'] 		= true;
						$res['message'] 	= 'Data';
						$res['quiz_id']		= $quiz_id;
						$res['jumlah_soal'] = $jumlah_soal;
						$res['time_start'] = $time_start;

						$sisa_soal 			= 0;
						$belum_terjawab 	= 0;
						foreach ($answer_questionnn as $keyaw => $qa) {
							if ($qa['answer_student'] == null) {
								$belum_terjawab = $belum_terjawab+1;						
							}
						}
						$sisa_soal = $jumlah_soal-$belum_terjawab;
						$res['sisa_soal'] 	= $sisa_soal;
						$res['belum_terjawab'] = $belum_terjawab;
						$aksiFinish = 1;
						foreach ($answer_questionnn as $keyaa => $qw) {
							// if($qw['answer_student'] == null){
								$soal_uid   = $qw['soal_uid'];
								$getDetail 	= $this->db->query("SELECT * FROM master_banksoal_archive WHERE soal_uid = '$soal_uid'")->row_array();
								$options = json_decode($getDetail['options'],true);

								$res['getDetail'][$keyaa] = $getDetail;
								$res['getDetail'][$keyaa]['options'] = $options;
								$aksiFinish = 0;
								// break;
							// }							
						}	
						$finish 	 = false;
						$total_benar = 0;
						$total_salah = 0;
						if ($aksiFinish == 1) {
							$getCheckQuiz 	= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
							$answer_student	= json_decode($getCheckQuiz['answer_question'],true);
							foreach ($answer_student as $ka => $as) {
								if ($as['answer_student'] != null) {
									if ($as['answer_student'] == $as['answer_system']) {
										$total_benar = $total_benar+1;
									}
									else
									{
										$total_salah = $total_salah+1;	
									}
								}
							}

							$score = 0; 
							$this->db->query("UPDATE tbl_quiz SET correct_answer = '$total_benar', 	wrong_answer = '$total_salah', status = '1' WHERE quiz_id = '$quiz_id'");
							$res['total_benar']	= $total_benar;
							$res['total_salah'] = $total_salah;
							$res['score'] 		= 0;
							$finish 			= true;
						}
						$res['finish'] 		= $finish;
						$this->response($res);
					}	
					else
					{
						$res['status'] 	= false;
						$res['message'] = 'No Data';
						$res['data'] 	= null;
						$this->response($res);	
					}
				}			
				else
				{
					$quiz_id 			= $cekQuizUser['quiz_id'];
					$quizTbl 			= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
					if ($quizTbl['time_start'] == "0000-00-00 00:00:00") {
						$this->db->query("UPDATE tbl_quiz SET time_start = '$time_start' WHERE quiz_id = '$quiz_id'");
					}
					$answer_questionnn	= json_decode($quizTbl['answer_question'],true);

					$res['status'] 		= true;
					$res['message'] 	= 'Data';
					$res['quiz_id']		= $quiz_id;
					$res['jumlah_soal'] = $jumlah_soal;
					$res['time_start']  = $quizTbl['time_start'];

					$sisa_soal 			= 0;
					$belum_terjawab 	= 0;
					foreach ($answer_questionnn as $keyaw => $qa) {
						if ($qa['answer_student'] == null) {
							$belum_terjawab = $belum_terjawab+1;						
						}
					}
					$sisa_soal = $jumlah_soal-$belum_terjawab;
					$res['sisa_soal'] 	= $belum_terjawab;
					$res['belum_terjawab'] = $belum_terjawab;
					$aksiFinish = 1;
					foreach ($answer_questionnn as $keyaa => $qw) {
						// if($qw['answer_student'] == null){
							$soal_uid   = $qw['soal_uid'];
							$getDetail 	= $this->db->query("SELECT * FROM master_banksoal_archive WHERE soal_uid = '$soal_uid'")->row_array();
							$options = json_decode($getDetail['options'],true);

							$res['getDetail'][$keyaa] = $getDetail;
							$res['getDetail'][$keyaa]['options'] = $options;
							$aksiFinish = 0;
							// break;
						// }
					}		
					$finish 	 = false;
					$total_benar = 0;
					$total_salah = 0;
					if ($aksiFinish == 1) {
						$getCheckQuiz 	= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
						$answer_student	= json_decode($getCheckQuiz['answer_question'],true);
						foreach ($answer_student as $ka => $as) {
							if ($as['answer_student'] != null) {
								if ($as['answer_student'] == $as['answer_system']) {
									$total_benar = $total_benar+1;
								}
								else
								{
									$total_salah = $total_salah+1;	
								}
							}
						}

						$score = $total_benar / $jumlah_soal * 100; 
						$this->db->query("UPDATE tbl_quiz SET correct_answer = '$total_benar', 	wrong_answer = '$total_salah', status = '2' WHERE quiz_id = '$quiz_id'");
						$this->db->query("UPDATE tbl_quiz SET status = 1 WHERE quiz_id = '$quiz_id'");
						$res['total_benar']	= $total_benar;
						$res['total_salah'] = $total_salah;
						$res['score'] 		= $score;
						$finish 			= true;
					}
					$res['finish'] 		= $finish;					
				}
			}
			$this->response($res);
		}
	}

	public function saveQuizFromStudent_post($value='')
	{
		$bsid			= $this->post('bsid');
		$quiz_id		= $this->post('quiz_id');
		$id_user 		= $this->post('id_user');
		$soal_uid   	= $this->post('soal_uid');
		$answer_select 	= $this->db->escape_str($this->post('answer_select'));
		$cekbsid 		= $this->db->query("SELECT * FROM master_banksoal WHERE bsid = '$bsid'")->row_array();
		if (empty($cekbsid)) {
			$rets['status'] 	= false;
			$rets['code'] 		= -401;
			$rets['message'] 	= 'Error Empty';
			$this->response($rets);			
		}
		else
		{
			$cekQuizUser 		= $this->db->query("SELECT * FROM tbl_quiz WHERE quiz_id = '$quiz_id'")->row_array();
			if (empty($cekQuizUser)) {
				$rets['status'] 	= false;
				$rets['code'] 		= -402;
				$rets['message'] 	= 'Error Empty Quiz';
				$this->response($rets);
			}	
			else
			{
				$answer_question	= json_decode($cekQuizUser['answer_question'],true); 	
				$stat_upd = 0;
				foreach ($answer_question as $key => $value) {								
					if ($value['soal_uid'] == $soal_uid) {
						$answer_question[$key]['answer_student'] = $answer_select;
						$stat_upd = 1;
					}
				}

				if ($stat_upd == 1) {
					$answer_question 		= $this->db->escape_str(json_encode($answer_question));					
					$updateDataProgram 		= $this->db->simple_query("UPDATE tbl_quiz SET answer_question = '$answer_question' WHERE quiz_id='{$quiz_id}'");

					$rets['status'] = true;
					$rets['code'] 	= 200;
					$rets['message']= "Sukses";
					$this->response($rets);
				}
				else
				{
					$rets['status'] = false;
					$rets['code'] = -400;
					$rets['message'] = "gagal";
					$this->response($rets);
				}
			}
		}

	}

	public function dataQuiz_post($value='')
	{
		# code..
		$myjenjang 	= $this->post('jenjangid');
		$user_utc 	= $this->post('user_utc');
		$user_utc 	= $this->Rumus->getGMTOffset('minute',$user_utc);

		$server_utc     = $this->Rumus->getGMTOffset();            
        $intervall      = $user_utc - $server_utc;
        $datenow        = date('Y-m-d H:i:s');

		$waktu_sekarang = DateTime::createFromFormat ('Y-m-d H:i:s',$datenow);
        $waktu_sekarang->modify("+".$intervall ." minutes");
        $waktu_sekarang = $waktu_sekarang->format('Y-m-d H:i:s');

		$resultdata = $this->db->query("SELECT mbs.*, ms.subject_name, ms.jenjang_id, mj.jenjang_name, mj.jenjang_level FROM master_subject as ms INNER JOIN master_banksoal as mbs ON ms.subject_id=mbs.subject_id INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id where ms.jenjang_id='$myjenjang' AND mbs.list_soal != '[]' AND ('$waktu_sekarang' >= mbs.validity) AND ( '$waktu_sekarang' <= mbs.invalidity)")->result_array();
		if (!empty($resultdata)) 
        {

        	foreach ($resultdata as $key => $value)
        	{
				$resultdata[$key]['list_soal'] = json_decode($value['list_soal'],true);
        	}

        	$res['status'] = true;
			$res['message'] = "Data ";
			$res['data'] = $resultdata;
			$this->response($res);
        }
        else
        {
			$res['status'] = false;
			$res['message'] = "Zero";
			$this->response($res);
		}
	}

	public function listProgramPackage_post($value='')
	{	
		$channel_id = $this->post('channel_id');
		$resultdata = $this->db->query("SELECT mbs.*, ms.subject_name, ms.jenjang_id, mj.jenjang_name, mj.jenjang_level FROM master_subject as ms INNER JOIN master_banksoal as mbs ON ms.subject_id=mbs.subject_id INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id where ms.jenjang_id='$myjenjang'")->result_array();
		if (!empty($resultdata)) 
        {

        	foreach ($resultdata as $key => $value)
        		{
					$resultdata[$key]['list_soal'] = json_decode($value['list_soal'],true);
        		}
        	$res['status'] = true;
			$res['message'] = 'Data';
			$res['data'] = $resultdata;
			$this->response($res);
        }
        else
        {
			$res['status'] = false;
			$res['message'] = 'Zero';
			$this->response($res);
		}
	}
	public function listdata_Program_post()
	{
		// $v_accesstoken 	= $this->verify_access_token($this->get('access_token'));
		// if ($v_accesstoken == "") {
		// 	$rets = array("status" => false, "code" => -400, "message" => "invalid token.");
		// 	goto ending;
		// }
		$id_user = $this->post('id_user');
		$type = $this->post('type');
		$jenjang_id = $this->post('jenjang_id');


		$anak = $this->db->query("SELECT * FROM tbl_profile_kid WHERE parent_id='$id_user'")->result_array();
            $q_anak = "";
            if(!empty($anak)){
                foreach ($anak as $key => $value) {
                	$jd = $value['jenjang_id'];                    	
                	$q_anak = "OR jenjang_id LIKE '%\"$jd\"%' and tpp.status_display ='1' ";
                }
            }

   
		$qq = $this->db->query("SELECT tpp.* , mc.channel_name  from tbl_price_program as tpp INNER JOIN master_channel as mc on tpp.channel_id = mc.channel_id WHERE tpp.status_display ='1' and jenjang_id like '%$jenjang_id%' $q_anak ")->result_array();
		

		if (!empty($qq)) {				
			
			foreach ($qq as $key => $value) {
				$qq[$key]["bidang"] = json_decode($value['bidang'],true);
				$qq[$key]["fasilitas"] = json_decode($value['fasilitas'],true);
				$qq[$key]["jenjang_id"] = json_decode($value['jenjang_id'],true);
			}
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "List Package Price";
			$rets['data'] = $qq;
			$this->response($rets);
		}
		else
		{
			$rets['status'] = false;
			$rets['code'] = -300;
			$rets['message'] = "Empty";
			$rets['data'] = null;
			$this->response($rets);
		}
		
		// $rets['status'] = false;
		// $rets['message'] = "Field incomplete";
		// ending:
		$this->response($rets);
	}

	public function dtl_ProgramPackage_post($value='')
	{	
		$list_id = $this->post('list_id');
		$resultdata = $this->db->query("SELECT * FROM tbl_price_program WHERE list_id = '$list_id'")->row_array();
		if (!empty($resultdata)) 
        {        

        	$res['status'] = true;
			$res['message'] = 'Data';
			$res['data'] = $resultdata;
			$this->response($res);
        }
        else
        {
			$res['status'] = false;
			$res['message'] = 'Zero';
			$this->response($res);
		}
	}

	public function requestProgramPackage_post($value='')
	{	
		$request_id 	= $this->Rumus->get_new_request_id('program');

		$list_id 		= $this->post('list_id');
		$id_requester 	= $this->post('id_requester');
		$billing_type 	= $this->post('billing_type');
		$billing_options= $this->post('billing_options');
		$user_utc 		= $this->session->userdata('user_utc');
		
		$qr = $this->db->query("INSERT INTO tbl_request_program (request_id,program_id,id_requester,user_utc,status) VALUES ('$request_id','$list_id','$id_requester','$user_utc','2')");

		if($qr)
		{	

			$lastrequestid  	= $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request_program')->row('request_id');
			$tbl_price_program 	= $this->db->query("SELECT * FROM tbl_price_program WHERE list_id = '$list_id'")->row_array();
			$harga 				= $tbl_price_program['price'];
			//LOG DEMAND
            $myip = $_SERVER['REMOTE_ADDR'];
            $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$lastrequestid','student request package','$id_requester','program package','$myip','808')");
            //END LOG DEMAND

            //TAMBAH KERANJANG                    
            // $this->db->query("INSERT INTO tbl_keranjang (request_program_id,id_user,price) VALUES ('$lastrequestid','$id_requester','$harga')");
            //END KERANJANG

            // CREATE INVOICE
            $iptInvoice = $this->Process_model->createInvoice($lastrequestid,'program',$billing_type,$billing_options);

            if ($iptInvoice == 1) {
            	$res['status'] = true;
				$res['message'] = 'Success';
				$res['code'] = 200;
				$this->response($res);
            }
            else
            {
            	$res['status'] = false;
				$res['message'] = 'Failed';
				$res['code'] = -200;
				$this->response($res);
            }
         //    $order_id 		= $this->Rumus->gen_order_id();
	        // $UnixCode 		= $this->Rumus->RandomUnixCode();  
	        // $user_utc 		= $this->session->userdata('user_utc');
	        // if ($user_utc == "") {
	        // 	$user_utc = "420";
	        // }
	        // $total_price 	= 0;
	        // $buyer_id 		= "";
	        // $hargatotal 	= 0;
	        // $date_expired 	= "";
	        // $server_utc     = $this->Rumus->getGMTOffset();
         //    $intervall      = $user_utc - $server_utc;
         //    $datenow        = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
         //    $datenow->modify("+3600 seconds");
         //    $datenow        = strtotime($datenow->format('Y-m-d H:i:s'));
            
         //    $datenow        = date('Y-m-d H:i:s', $datenow);
         //    $date_expired   = $datenow;

         //    $data               = $this->db->query("SELECT * FROM tbl_request_program WHERE request_id='$request_id'")->row_array();
         //    if ($buyer_id == "") {                    
         //        $buyer_id   = $buyer_id+$v['id_requester'];
         //    }
         //    $list_id            = $this->db->query("SELECT program_id FROM tbl_request_program WHERE request_id = '$request_id'")->row_array()['program_id'];
         //    $product_properties = $this->db->query("SELECT name FROM tbl_price_program WHERE list_id = '$list_id'")->row_array()['name'];

         //    $total_price    = $total_price+$price; 
         //    $save_to_tbl_order_detail = $this->db->query("INSERT INTO tbl_order_detail (order_id,product_id,product_properties,total_amt,init_price,discount,final_price,final_pricetutor,TYPE) VALUES ('$order_id','$product_id','$product_properties','','$price','0','$price','$price','$type_product')");

        	
        }
        else
        {
			$res['status'] = false;
			$res['message'] = 'Failed';
			$res['code'] = -200;
			$this->response($res);
		}
	}

	public function changeDataFromStudent_post($value='')
	{
		$id_user 		= $this->post('id_user');
		$jenjang_old 	= $this->post('jenjang_old');
		$jenjang_new 	= $this->post('jenjang_new');
		$school_id_new 	= $this->post('school_id');
		
		$qr = $this->db->query("SELECT * FROM tbl_profile_student WHERE student_id = '$id_user' AND jenjang_id = '$jenjang_old'")->row_array();

		if(!empty($qr))
		{	
			if ($school_id_new == "" && $jenjang_new == 15 ){
 				$updt = $this->db->query("UPDATE tbl_profile_student SET jenjang_id = '$jenjang_new', school_id = 'null' , flag =1 WHERE student_id = '$id_user'");
 				$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_new."'")->row_array()['jenjang_name'];
				$this->session->set_userdata('jenjang_type',$get_jenjangtype);
 				$this->session->set_userdata('jenjang_id',$jenjang_new);
 				$this->session->set_userdata('update_jenjang','1');
 				$this->session->set_userdata('flag','1');
 				$this->session->set_userdata('code_cek','0');

 				$res['status'] 	= true;
				$res['message'] = 'Success Change Umum';
				$res['code'] 	= 200;
				$this->response($res);
 			}
 			else{
				$school_id_old = $qr['school_id'];			
				//PROSES DELETE FOLLOW TUTOR
				$ck_booking = $this->db->query("SELECT * FROM tbl_booking as tb LEFT JOIN master_subject as ms ON tb.subject_id=ms.subject_id WHERE id_user = '$id_user' AND ( ms.jenjang_id ='$jenjang_old' OR ms.jenjang_id LIKE '%\"$jenjang_old\"%')")->result_array();

				foreach ($ck_booking as $key => $value) {
					$uid 	= $value['uid'];
					$this->db->query("DELETE FROM tbl_booking WHERE uid='$uid'");
				}	 			

	 			if ($school_id_old != $school_id_new) {
	 				$updt = $this->db->query("UPDATE tbl_profile_student SET jenjang_id = '$jenjang_new', school_id = '$school_id_new', flag =1 WHERE student_id = '$id_user'");
	 				$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_new."'")->row_array()['jenjang_name'];
					$this->session->set_userdata('jenjang_type',$get_jenjangtype);
	 				$this->session->set_userdata('jenjang_id',$jenjang_new);
	 				$this->session->set_userdata('flag','1');
	 				$this->session->set_userdata('update_jenjang','1');
	 				$this->session->set_userdata('code_cek','0');

	 				$res['status'] 	= true;
					$res['message'] = 'Success Change Data School';
					$res['code'] 	= 200;
					$this->response($res);
	 			}else{
	 				$updt = $this->db->query("UPDATE tbl_profile_student SET jenjang_id = '$jenjang_new' , flag =1 WHERE student_id = '$id_user'");
	 				$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_new."'")->row_array()['jenjang_name'];
					$this->session->set_userdata('jenjang_type',$get_jenjangtype);
	 				$this->session->set_userdata('jenjang_id',$jenjang_new);
	 				$this->session->set_userdata('update_jenjang','1');
	 				$this->session->set_userdata('flag','1');
	 				$this->session->set_userdata('code_cek','0');

	 				$res['status'] 	= true;
					$res['message'] = 'Success Change School Only Jenjang ';
					$res['code'] 	= 200;
					$this->response($res);
	 			}	
 			}	        	
        }
        else
        {
			$res['status'] = false;
			$res['message'] = 'Failed';
			$res['code'] = -200;
			$this->response($res);
			$this->session->set_userdata('update_jenjang','0');
		}
	}
	public function changeDataFromKid_post($value='')
	{			
		$id_user 			= $this->post('id_user');
		$jenjang_old 		= $this->post('jenjang_old');
		$jenjang_new 		= $this->post('jenjang_new');
		$school_id_new 		= $this->post('school_id');
		$school_name_new 	= $this->post('school_name');
		$school_address_new = $this->post('school_address');

		
		$qr = $this->db->query("SELECT * FROM tbl_profile_kid WHERE kid_id = '$id_user' AND jenjang_id = '$jenjang_old'")->row_array();

		if(!empty($qr))
		{	
			
			$school_id_old = $qr['school_id'];			
			//PROSES DELETE FOLLOW TUTOR
			$ck_booking = $this->db->query("SELECT * FROM tbl_booking as tb LEFT JOIN master_subject as ms ON tb.subject_id=ms.subject_id WHERE id_user = '$id_user' AND ( ms.jenjang_id ='$jenjang_old' OR ms.jenjang_id LIKE '%\"$jenjang_old\"%')")->result_array();

			foreach ($ck_booking as $key => $value) {
				$uid 	= $value['uid'];
				$this->db->query("DELETE FROM tbl_booking WHERE uid='$uid'");
			}	 			

 			if ($school_id_old != $school_id_new) {
 				$updt = $this->db->query("UPDATE tbl_profile_kid SET jenjang_id = '$jenjang_new', school_id = '$school_id_new', flag =1, school_name ='$school_name_new', school_address='$school_address_new' WHERE kid_id = '$id_user'");
 				$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_new."'")->row_array()['jenjang_name'];
				$this->session->set_userdata('jenjang_type',$get_jenjangtype);
 				$this->session->set_userdata('jenjang_id_kids',$jenjang_new);
 				$this->session->set_userdata('flag','1');
 				$this->session->set_userdata('update_jenjang','1');
 				$this->session->set_userdata('code_cek','0');

 				$res['status'] 	= true;
				$res['message'] = 'Success Change Data School';
				$res['code'] 	= 200;
				$this->response($res);
 			}else{
 				$updt = $this->db->query("UPDATE tbl_profile_student SET jenjang_id = '$jenjang_new' , flag =1 WHERE student_id = '$id_user'");
 				$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_new."'")->row_array()['jenjang_name'];
				$this->session->set_userdata('jenjang_type',$get_jenjangtype);
 				$this->session->set_userdata('jenjang_id_kids',$jenjang_new);
 				$this->session->set_userdata('update_jenjang','1');
 				$this->session->set_userdata('flag','1');
 				$this->session->set_userdata('code_cek','0');

 				$res['status'] 	= true;
				$res['message'] = 'Success Change School Only Jenjang ';
				$res['code'] 	= 200;
				$this->response($res);
 			}	
 				        	
        }
        else
        {
			$res['status'] = false;
			$res['message'] = 'Failed';
			$res['code'] = -200;
			$this->response($res);
			$this->session->set_userdata('update_jenjang','0');
		}
	}

	public function actInTutorFromAdmin_post($value='')
	{			
		$tutor_id 		= $this->post('tutor_id');
		$flag 			= $this->post('flag');
		
		$qr = $this->db->query("SELECT * FROM tbl_profile_tutor WHERE tutor_id = '$tutor_id'")->row_array();

		if(!empty($qr))
		{		
			$this->db->query("UPDATE tbl_profile_tutor SET flag = '$flag' WHERE tutor_id = '$tutor_id'");

			if ($flag == 1) {
				$message = "Berhasil aktifkan Tutor";
			}
			else
			{
				$message = "Berhasil nonaktifkan Tutor";
			}

        	$res['status'] = true;
			$res['message'] = $message;
			$res['code'] = 200;
			$this->response($res);
        }
        else
        {
			$res['status'] = false;
			$res['message'] = 'Failed';
			$res['code'] = -200;
			$this->response($res);
		}
	}

	public function listRequestApproval_toTutor_post($value='')
	{			
		$tutor_id 	= $this->post('tutor_id');
		$user_utc 	= $this->post('user_utc');

		$qr = $this->db->query("SELECT tu.user_name, tu.user_image, ms.subject_name, ms.jenjang_id, ms.icon, tr.* FROM tbl_request as tr INNER JOIN tbl_user as tu ON tr.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON tr.subject_id=ms.subject_id WHERE tr.approve = 0 AND tr.tutor_id = '$tutor_id' LIMIT 1")->row_array();

		if(!empty($qr))
		{			
			$duration = sprintf('%02d',$qr['duration_requested']/60);		
			$jenjangid = json_decode($qr['jenjang_id'], TRUE);
			if ($jenjangid != NULL) {
				if(is_array($jenjangid)){
					foreach ($jenjangid as $keyy => $v) {								
						$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$v'")->row_array();
						$qr['jenjang_name'] = $getAllSubject['jenjang_name'];
						$qr['jenjang_level'] = $getAllSubject['jenjang_level'];												
					}
				}
				else
				{
					$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$jenjangid'")->row_array();
					$qr['jenjang_name'] = $getAllSubject['jenjang_name'];
					$qr['jenjang_level'] = $getAllSubject['jenjang_level'];				
				}
			}

			$server_utc         = $this->Rumus->getGMTOffset();
            $intervall          = $user_utc - $server_utc;
            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$qr['date_requested']);
            $tanggal->modify("+".$intervall ." minutes");
            $tanggal            = $tanggal->format('Y-m-d H:i:s');

            $qr['date_requested'] = $tanggal;
			$qr['type'] = 'private';
			$qr['duration_requested'] = $duration;
        	$res['status'] = true;
			$res['message'] = 'Success';
			$res['data'] = $qr;
			$res['code'] = 200;
			$this->response($res);
        }
        else
        {
        	$qrg = $this->db->query("SELECT tu.user_name, tu.user_image, ms.subject_name, ms.jenjang_id, ms.icon, trg.* FROM tbl_request_grup as trg INNER JOIN tbl_user as tu ON trg.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON trg.subject_id=ms.subject_id WHERE trg.approve = 0 AND trg.tutor_id = '$tutor_id' LIMIT 1")->row_array();
        	if(!empty($qrg))
			{		
				// $duration = sprintf('%02d',$qrg['duration_requested']/60);
				$jenjangid = json_decode($qrg['jenjang_id'], TRUE);
				if ($jenjangid != NULL) {
					if(is_array($jenjangid)){
						foreach ($jenjangid as $keyy => $v) {								
							$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$v'")->row_array();
							$qrg['jenjang_name'] = $getAllSubject['jenjang_name'];
							$qrg['jenjang_level'] = $getAllSubject['jenjang_level'];												
						}
					}
					else
					{
						$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$jenjangid'")->row_array();
						$qrg['jenjang_name'] = $getAllSubject['jenjang_name'];
						$qrg['jenjang_level'] = $getAllSubject['jenjang_level'];				
					}
				}	

				$qrg['type'] = 'group';
				// $qrg['duration_requested'] = $duration;
	        	$res['status'] = true;
				$res['message'] = 'Success';
				$res['data'] = $qrg;
				$res['code'] = 200;
				$this->response($res);
	        }
	        else
	        {
				$res['status'] = false;
				$res['message'] = 'Failed';
				$res['data'] = null;
				$res['code'] = -200;
				$this->response($res);
			}
		}
	}

	public function listOrderRequest_post()
	{			
		$id_user 	= $this->post('id_user');
		$user_utc 	= $this->post('user_utc');
		$anak   	= $this->db->query("SELECT * FROM tbl_profile_kid WHERE parent_id='$id_user'")->result_array();
        $q_anak 	= "";
        $q_anakpro 	= "";
        if(!empty($anak)){
            foreach ($anak as $key => $value) {
                $q_anak.= " OR tr.id_user_requester='".$value['kid_id']."' ";
                $q_anakpro.= " OR tr.id_requester='".$value['kid_id']."' ";
            }
        }

        $getDataRequest   = $this->db->query("SELECT tr.date_requested as date_requested, tr.request_id, tr.id_user_requester, tr.tutor_id as tutor_id, tr.subject_id, tr.topic as topic, tr.approve as status FROM tbl_request as tr INNER JOIN tbl_user as tu ON tr.id_user_requester=tu.id_user WHERE ( tr.id_user_requester='$id_user' $q_anak ) AND tr.approve >= 0")->result_array();



        if (!empty($getDataRequest)) {

        	$getDataRequestGroup   		= $this->db->query("SELECT tr.date_requested as date_requested, tr.request_id, tr.id_user_requester, tr.tutor_id as tutor_id, tr.subject_id, tr.topic as topic, tr.approve as status FROM tbl_request_grup as tr INNER JOIN tbl_user as tu ON tr.id_user_requester=tu.id_user WHERE ( tr.id_user_requester='$id_user' $q_anak ) AND tr.approve >= 0")->result_array();
        	$getDataRequestProgram  	= $this->db->query("SELECT tr.created_at as date_requested, tr.* FROM tbl_request_program as tr INNER JOIN tbl_user as tu ON tr.id_requester=tu.id_user WHERE ( tr.id_requester='$id_user' $q_anakpro ) AND tr.status >= 0")->result_array();
        	$getDataRequestMulticast  	= $this->db->query("SELECT tr.created_at as date_requested, tr.request_id, tr.id_requester, tc.tutor_id as tutor_id, tc.subject_id, tc.description as topic, tr.status FROM tbl_request_multicast as tr INNER JOIN tbl_user as tu ON tr.id_requester=tu.id_user INNER JOIN tbl_class as tc ON tc.class_id=tr.class_id WHERE ( tr.id_requester='$id_user' $q_anakpro ) AND tr.status >= 0")->result_array(); 



        	$getData = array_merge($getDataRequest, $getDataRequestGroup, $getDataRequestProgram, $getDataRequestMulticast);


        	foreach ($getData as $key => $value) {
        		$req_id 			= $value['request_id'];        		
        		$sub_productid  	= substr($req_id,0,1);                        
            	$type           	= $sub_productid == '7' ? 'private' : ( $sub_productid == '8' ? 'group': ( $sub_productid == '9' ? 'multicast' : 'program' ));            	
            	
            	$server_utc         = $this->Rumus->getGMTOffset();
                $intervall          = $user_utc - $server_utc;   
                $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$value['date_requested']);
                $tanggal->modify("+".$intervall ." minutes");
                $tanggal            = $tanggal->format('Y-m-d H:i:s');
                $datelimit          = date_create($tanggal);
                $dateelimit         = date_format($datelimit, 'd/m/y');
                $harilimit          = date_format($datelimit, 'd');
                $hari               = date_format($datelimit, 'l');
                $tahunlimit         = date_format($datelimit, 'Y');
                $waktulimit         = date_format($datelimit, 'H:i');   
                $datelimit          = $dateelimit;
                $sepparatorlimit    = '/';
                $partslimit         = explode($sepparatorlimit, $datelimit);
                $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

                $harga 				= $this->db->query("SELECT final_price FROM tbl_order_detail WHERE product_id = '$req_id'")->row_array()['final_price'];
                if ($type == 'program') {
                	$list_id 		= $value['program_id'];
                	$programData 	= $this->db->query("SELECT * FROM tbl_price_program WHERE list_id = '$list_id'")->row_array();
                	$name 			= $programData['name'];
                	$channel_id 	= $programData['channel_id'];
                	$channel_logo   = $this->db->query("SELECT channel_logo FROM master_channel WHERE channel_id = '$channel_id'")->row_array()['channel_logo'];
                	$getData[$key]['nama_program'] 	= $name;
					$getData[$key]['tutor_image'] 	= $channel_logo;
                }
                else
                {
                	$tutor_id 		= $value['tutor_id'];
                	$subject_id 	= $value['subject_id'];
                	$tutorData 		= $this->db->query("SELECT user_name, user_image FROM tbl_user WHERE id_user = '$tutor_id'")->row_array();
                	$subject_name   = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id = '$subject_id'")->row_array()['subject_name'];
					$getData[$key]['tutor_name'] 	= $tutorData['user_name'];
					$getData[$key]['tutor_image'] 	= $tutorData['user_image'];
					$getData[$key]['subject_name'] 	= $subject_name;
                }

                $getData[$key]['type'] 			= $type;
                $getData[$key]['date_request'] 	= $hari.', '.$harilimit.' '.$bulanlimit.' '.$tahunlimit.' '.$waktulimit;
                // $getData[$key]['date_request'] 	= $date_requested;

                $getData[$key]['harga'] 		= number_format($harga, 0, ".", ".");
        	}
        	$res['status'] = true;
			$res['message'] = 'Success';
			$res['data'] = $getData;
			$res['code'] = 200;
			$this->response($res);
        }
        else
        {
        	$res['status'] = false;
			$res['message'] = 'Failed';
			$res['data'] = null;
			$res['code'] = -200;
			$this->response($res);
        }
	}

	public function showInvoiceToStudent_post()
	{			
		$id_user 			= $this->post('id_user');
		$myid               = $this->post('id_user');          

		$anak               = $this->db->query("SELECT * FROM tbl_profile_kid WHERE parent_id='$myid'")->result_array();
		$utc 				= $this->post('user_utc');
		$user_utc 			= $this->Rumus->getGMTOffset('minute', $utc);

		$q_anak 	= "";
        $q_anakpro 	= "";
        $server_utcc        = $this->Rumus->getGMTOffset();
        $intervall          = $user_utc - $server_utcc;
		if ($this->get('access_token') != '') {
			
			$retok = $this->verify_access_token($this->get('access_token')); // INI UNTUK VERIFIKASI KEASLIAN ACCESS TOKEN NYA
	        if($retok == false){ // JIKA FALSE MAKA TOKEN PALSU
	            $res = array("status" => false, "code" => -400, "message" => "invalid token."); // INI ADA KARENA FUNCTION INI `STATUS` NYA DI LUAR OBJECT `RESPONSE`
	            $res['response'] = array("status" => false, "code" => -400, "message" => "invalid token.");
	            goto ending; // GOTO ENDING ITU LANGSUNG LONCATIN KE AKHIR DARI FUNCTION
		    }
        	if(!empty($anak)){
			    foreach ($anak as $key => $value) {
			        $q_anak.= " OR tor.buyer_id='".$value['kid_id']."' ";
			    }
			}
			    
			$getdatainvoice   = $this->db->query("SELECT * FROM tbl_invoice as ti INNER JOIN tbl_order as tor ON ti.order_id=tor.order_id WHERE (tor.buyer_id='$myid'  $q_anak ) AND tor.order_status=2 AND ti.payment_type is NULL ORDER BY ti.created_at DESC")->result_array();
			
			if (empty($getdatainvoice)) {
				$res['status'] = false;
				$res['message'] = 'failed empty';
				$res['data'] = null;
				$this->response($res);
			}
			else
			{
				foreach ($getdatainvoice as $row => $v) {
	                $invoice_id     = $v['invoice_id'];
	                // $product_id     = $v['product_id'];
	                $order_id       = $v['order_id'];
	                $order_status   = $v['order_status'];

	                $get_orderdetail = $this->db->query("SELECT tod.*, tr.*, trg.*, tr.subject_id as tr_subject_id, tr.tutor_id as tr_tutor_id, tr.approve as tr_approve, tr.topic as tr_topic, tr.date_requested as tr_date_requested, trg.subject_id as trg_subject_id, trg.tutor_id as trg_tutor_id, trg.approve as trg_approve, trg.topic as trg_topic, trg.date_requested as trg_date_requested, tc.subject_id as trm_subject_id, tc.tutor_id as trm_tutor_id, trm.status as trm_approve, tc.description as trm_topic, trp.created_at as trp_date_requested, tpp.name as program_name,  tpp.description as trp_topic, trp.status as trp_approve, tpp.channel_id as trp_channel_id FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id LEFT JOIN (tbl_request_program as trp INNER JOIN tbl_price_program as tpp ON trp.program_id=tpp.list_id ) ON trp.request_id=tod.product_id WHERE tod.order_id='$order_id'")->result_array(); 
	               
	                $harga          = $v['total_price'];
	                $hargaprice     = number_format($harga, 0, ".", ".");                

	                $status = null;
	                foreach ($get_orderdetail as $key => $value) {
	                    // $subject_id     = $value['tr_subject_id'] != null ? $value['tr_subject_id'] : ( $value['trg_subject_id'] != null ? $value['trg_subject_id'] : $value['trm_subject_id']);
	                    // $tutor_id       = $value['tr_tutor_id'] != null ? $value['tr_tutor_id'] : ( $value['trg_tutor_id'] != null ? $value['trg_tutor_id'] : $value['trm_tutor_id']);
	                    // $status         = $value['tr_approve'] != null ? $value['tr_approve'] : ( $value['trg_approve'] != null ? $value['trg_approve'] : $value['trm_approve']);
	                    $subject_id     = $value['tr_subject_id'] != null ? $value['tr_subject_id'] : ( $value['trg_subject_id'] != null ? $value['trg_subject_id'] : ($value['trm_subject_id'] != null ? $value['trm_subject_id'] : ''));
                        $tutor_id       = $value['tr_tutor_id'] != null ? $value['tr_tutor_id'] : ( $value['trg_tutor_id'] != null ? $value['trg_tutor_id'] : ( $value['trm_tutor_id'] != null ? $value['trm_tutor_id'] : ''));
                        $status         = $value['tr_approve'] != null ? $value['tr_approve'] : ( $value['trg_approve'] != null ? $value['trg_approve'] : ( $value['trm_approve'] != null ? $value['trm_approve'] : $value['trp_approve']));
                        $topic          = $value['tr_topic'] != null ? $value['tr_topic'] : ( $value['trg_topic'] != null ? $value['trg_topic'] : ($value['trm_topic'] != null ? $value['trm_topic'] : $value['trp_topic']));

                        if ($subject_id != '') {
		                    $datasub   = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array();
		                    $subject_name   = $datasub['subject_name'];
		                    
		                }else
		                {
		                	$subject_name  = $value['program_name'];
                            $jenjang_data  = '';
		                }
		                if ($tutor_id != '') {
		                    $data_tutor     = $this->db->query("SELECT user_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
		                    $tutor_name     = $data_tutor['user_name'];
		                    $tutor_image    = $data_tutor['user_image'];
		                }
		                else
		                {
		                	$channel_id     = $value['trp_channel_id'];
                            $data_channel   = $this->db->query("SELECT channel_name,channel_logo FROM master_channel WHERE channel_id='$channel_id'")->row_array();
                            $tutor_name     = $data_channel['channel_name'];
                            $tutor_image    = $data_channel['channel_logo'];
		                }
		                if ($value['type'] == 'program') {
		                	$date_requested = $value['trp_date_requested'];
		                }
		                else if ($value['type'] == 'multicast') {
		                	$class_id 		= $value['class_id'];
		                	$trp_date_requested = $this->db->query("SELECT start_time FROM tbl_class WHERE class_id='$class_id'")->row_array()['start_time'];
		                	$date_requested = $trp_date_requested;
		                }
		                else if($value['type'] == 'private')
		                {
 							$date_requested   = $value['tr_date_requested'];
 						}
 						else
 						{
 							$date_requested   = $value['trg_date_requested'];	
 						}

 						if ($value['topic'] == null) {
 							$get_orderdetail[$key]['topic'] = $topic;
 						}
 						$date_requested   = DateTime::createFromFormat ('Y-m-d H:i:s',$date_requested);
		            	$date_requested->modify("+".$intervall ." minutes");
		            	$date_requested   = $date_requested->format('Y-m-d H:i:s');
		            	$get_orderdetail[$key]['date_requested'] 	= $date_requested;
	                    $get_orderdetail[$key]['subject_name'] 		= $subject_name;
	                    $get_orderdetail[$key]['jenjang_name'] 		= null;
	                    $get_orderdetail[$key]['tutor_name'] 		= $tutor_name;
	                    $get_orderdetail[$key]['tutor_image'] 		= $tutor_image;
	                }

		            $UnixCode           = $v['unix_code'];
		            $hargakelas         = $v['total_price']-$UnixCode;               
                    

	                $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$v['payment_due']);
		            $tanggaltransaksi->modify("+".$intervall ." minutes");
		            $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');

	                $tanggalcreate   = DateTime::createFromFormat ('Y-m-d H:i:s',$v['created_at']);
		            $tanggalcreate->modify("+".$intervall ." minutes");
		            $tanggalcreate   = $tanggalcreate->format('Y-m-d H:i:s');

		            $dateee             = $tanggaltransaksi;
		            $datetime           = new DateTime($dateee);
		            $datetime->modify('+1 hours');
		            $limitdateee        = $datetime->format('Y-m-d H:i:s');

		            $datelimit          = date_create($limitdateee);
		            $dateelimit         = date_format($datelimit, 'd/m/y');
		            $harilimit          = date_format($datelimit, 'd');
		            $hari               = date_format($datelimit, 'l');
		            $tahunlimit         = date_format($datelimit, 'Y');
		            $waktulimit         = date_format($datelimit, 'H:s');
		                                                    
		            $datelimit          = $dateelimit;
		            $sepparatorlimit    = '/';
		            $partslimit         = explode($sepparatorlimit, $datelimit);
		            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

		            $getdatainvoice[$row]['tanggaltransaksi'] 	= $tanggalcreate;
		            $getdatainvoice[$row]['expireddate']	 	= $tanggaltransaksi;
	                $getdatainvoice[$row]['order_detail'] 		= $get_orderdetail;
			        $getdatainvoice[$row]['hargakelas'] 		= $hargakelas;
	            }
			}
			$res['status'] = true;
			$res['message'] = 'Successfully';
			$res['data'] = $getdatainvoice;
			$this->response($res);
		}

		$res['status'] = false;
		$res['message'] = 'Field incomplete ';
		ending:
		$this->response($res);
	}
	public function testRandom_get($value='')
	{		
		$re = array(1, 2, 3, 4, 5 ,8, 19, 20);
		shuffle($re);
		print_r($re);
	}

	private function create_access_token($data='')
	{
		$token= array(
			"iss" => "classmiles jwt v1",
			"iat" => time(),
			"data" => $data
		);
		$jwt = JWT::encode($token, SECRET_KEY);
		return $jwt;
	}
	private function verify_access_token($access_token='')
	{
		try{
			$dc = JWT::decode($access_token, SECRET_KEY, array('HS256'));
		}catch(Exception $e){
			return false;
		}

		if(isset($dc->status) && $dc->status == false){
			return -1;
		}else{
			return json_decode(json_encode($dc->data),true);
		}
	}
	public function channel_listChannel_post()
	{
		$getList 	= $this->db->query("SELECT * FROM master_channel WHERE status = 1")->result_array();
        
		if (!empty($getList)) {			

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "List Channel";
			$rets['data'] = $getList;
			$this->response($rets);
		}	
		else
		{
			$rets['status'] = true;
			$rets['code'] = 300;
			$rets['message'] = "Error";
			$rets['data'] = null;
			$this->response($rets);	
		}
	}
	public function channel_descChannel_post()
	{
		$channel_id = $this->post('channel_id');
		$id_user 	= $this->post('id_user');
		
		if ($channel_id != '') {

			$get 	= $this->db->query("SELECT * FROM master_channel WHERE channel_id = '$channel_id'")->row_array();
			$total_program = $this->db->query("SELECT count(*) as total_program FROM master_channel_program WHERE channel_id='$channel_id'")->row_array()['total_program'];
			$total_group = $this->db->query("SELECT count(*) as total_group FROM master_channel_group WHERE channel_id='$channel_id'")->row_array()['total_group'];
			$total_student = $this->db->query("SELECT count(*) as total_student FROM master_channel_student WHERE channel_id='$channel_id'")->row_array()['total_student'];
			$total_tutor = $this->db->query("SELECT count(*) as total_tutor FROM master_channel_tutor WHERE channel_id='$channel_id'")->row_array()['total_tutor'];
			$total_class = $this->db->query("SELECT count(*) as total_class FROM tbl_class WHERE channel_id='$channel_id'")->row_array()['total_class'];
			$qq = $this->db->query("SELECT tpp.* , mc.channel_name  from tbl_price_program as tpp INNER JOIN master_channel as mc on tpp.channel_id = mc.channel_id WHERE mc.channel_id = '$channel_id'")->result_array();
			foreach ($qq as $key => $value) {
				$qq[$key]["bidang"] = json_decode($value['bidang'],true);
				$qq[$key]["fasilitas"] = json_decode($value['fasilitas'],true);
				$qq[$key]["jenjang_id"] = json_decode($value['jenjang_id'],true);
			}
			$ckUser 	= $this->db->query("SELECT * FROM master_channel_student WHERE channel_id = '$channel_id' AND id_user = '$id_user' ")->row_array();
			if (empty($ckUser)) {
				$rets['status_join'] = -1;
			} else {

				$rets['status_join'] = $ckUser['flag_membership'];
			}
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Details Channel";
			$rets['data'] = $get;
			$rets['data_program'] = $qq;
			$rets['total_program'] = $total_program;
			$rets['total_group'] = $total_group;
			$rets['total_student'] = $total_student;
			$rets['total_tutor'] = $total_tutor;
			$rets['total_class'] = $total_class;
			$this->response($rets);
		}	
		else
		{
			$rets['status'] = false;
			$rets['code'] = 300;
			$rets['message'] = "Error";
			$rets['data'] = null;
			$rets['data_program'] = null;
			$rets['total_program'] = null;
			$rets['total_group'] = null;
			$rets['total_student'] = null;
			$rets['total_tutor'] = null;
			$rets['total_class'] = null;
			$this->response($rets);	
		}
	}
	public function channel_joinChannel_post()
	{
		$channel_id = $this->post('channel_id');
		$id_user 	= $this->post('id_user');

		$ckChannel 	= $this->db->query("SELECT * FROM master_channel WHERE channel_id = '$channel_id'")->row_array();
		if (!empty($ckChannel)) {
			$membership_type = $ckChannel['membership_type'];
			
			$ckUser 	= $this->db->query("SELECT * FROM master_channel_student WHERE channel_id = '$channel_id' AND id_user = '$id_user' ")->row_array();
			if (empty($ckUser)) {
				if ($membership_type == 'close') {
					$get 	= $this->db->query("INSERT INTO master_channel_student (channel_id, id_user, status, member_ship, flag_membership) VALUES ('$channel_id','$id_user','inactive','basic','0')");
				}
				else
				{
					$get 	= $this->db->query("INSERT INTO master_channel_student (channel_id, id_user, status, member_ship, flag_membership) VALUES ('$channel_id','$id_user','inactive','basic','1')");
				}
			}
			

			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = "Success Join to Channel";
			$rets['data'] = $get;
			$this->response($rets);
		}	
		else
		{
			$rets['status'] = true;
			$rets['code'] = -100;
			$rets['message'] = "Error";
			$rets['data'] = null;
			$this->response($rets);	
		}
	}

	public function face_detection_post()
	{
		$image = $this->post('image');
		

		if(!empty($image))
		{		
			 $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://classmiles.com/engine/face_detection');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, "image=$image");

	        // Execute post
	        $result = curl_exec($ch);
	        $face = json_decode($result);
	        curl_close($ch);

        	$res['status'] = true;
			$res['face'] = $face->{'is_there_face'};
			$res['code'] = 200;
			$this->response($res);
        }
        else
        {
			$res['status'] = false;
			$res['message'] = 'Failed';
			$res['code'] = -200;
			$this->response($res);
		}
	}	
	
	public function addParticipant_ToGroup_post()
	{
		$class_id   = $this->post('class_id');
		$email 		= $this->post('email');

		if ($email == '') {
			$rets['status'] = false;
			$rets['code'] = -101;
			$rets['message'] = "Email kosong";			
			$this->response($rets);
		}
		else
		{
			$ckEmail 	= $this->db->query("SELECT * FROM tbl_user WHERE email = '$email'")->row_array();
			if (!empty($ckEmail)) {
				$id_user 		= $ckEmail['id_user'];
				$participant 	= $this->db->query("SELECT * FROM tbl_class WHERE class_id = '$class_id' ")->row_array();


				$participant_arr = json_decode($participant['participant'],true);
				$total 			 = count($participant_arr);			
				if ($total > 4) {
					$rets['status'] = false;
					$rets['code'] = -102;
					$rets['message'] = "Maksimal participant";			
					$this->response($rets);	
				}
				else
				{
					if(!array_key_exists('participant', $participant_arr)){
						$participant_arr['participant'] = array();
					}
					$ada = 0;					
					foreach ($participant_arr['participant'] as $key => $value) {
						if ($value['id_user'] == $id_user) {
							$ada = 1;
						}
					}

					if ($ada == 0) {
						array_push($participant_arr['participant'], array("id_user" => $id_user, "st" => '0'));
						$participant_arr = json_encode($participant_arr);
						$this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");

						$rets['status'] = true;
						$rets['code'] 	= 200;
						$rets['message']= "Success Join to class";				
						$this->response($rets);
					}
					else
					{
						$rets['status'] = false;
						$rets['code'] = -101;
						$rets['message'] = "Sudah didalam kelas";			
						$this->response($rets);	
					}	
				}		
			}
			else
			{
				$rets['status'] = true;
				$rets['code'] = -100;
				$rets['message'] = "Error";			
				$this->response($rets);	
			}
		}	
	}

	public function resize_photo_post(){
		$getallimage = $this->db->query("SELECT user_image from tbl_user WHEre created_at < '2018-08-30 23:00:00' and usertype_id='tutor' and user_image != 'dXNlci9lbXB0eS5qcGc='")->result_array(['user_image']);
		if (!empty($getallimage )) {
			foreach ($getallimage as $key => $value) {
				$user_image = $value['user_image'];
				$decodename = base64_decode($user_image);
		    	$img_name  = substr($decodename,5);
		    	print_r($img_name);
		    	$content = file_get_contents(CDN_URL.USER_IMAGE_CDN_URL.$user_image);
		    	$fp = fopen("aset/_temp_images/".$img_name, "w");
				$rets = fwrite($fp, $content);
				fclose($fp);
		    	$config['image_library'] = 'gd2';
				$config['source_image']	= './aset/_temp_images/'.$img_name;
				$config['width']	= 512;
				$config['maintain_ratio'] = FALSE;
				$config['height']	= 512;
				$this->image_lib->initialize($config); 
				if ( ! $this->image_lib->resize())
				{
				        echo $this->image_lib->display_errors();
				}
		    	//Write to disk
		    	print_r("Berhasil");
	            $this->Rumus->sendToCDN('user', $img_name, null, 'aset/_temp_images/'.$img_name );
	            print_r("Berhasil lagi");
			}
		}
		else{
			$rets['status'] = false;
			$rets['code'] = -100;
			$rets['message'] = "Gagal Cuy";
			$this->response($rets);
		}
	}

	public function channel_leavechannel_post()
	{
		
		$channel_id = $this->post('channel_id');
		$id_user 	= $this->post('id_user');

		$ckChannel 	= $this->db->query("SELECT * FROM master_channel WHERE channel_id = '$channel_id'")->row_array();
		if (!empty($ckChannel)) {
			
			$get 	= $this->db->query("DELETE FROM master_channel_student WHERE id_user = '$id_user' AND channel_id = '$channel_id'");
			$rets['message'] = "Leave";
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['data'] = "Successful";
			$this->response($rets);
		}	
		else
		{
			$rets['status'] = true;
			$rets['code'] = -100;
			$rets['message'] = "Error";
			$rets['data'] = null;
			$this->response($rets);	
		}
	}

	public function list_paidmulticast_volume_post()
	{					
		$type 		= $this->post('type');
		$ckList 	= $this->db->query("SELECT * FROM master_paidmulticast_volume WHERE type = '$type'")->result_array();
		if (!empty($ckList)) {
			foreach ($ckList as $key => $value) {
				$price = number_format($value['price'], 0, ".", ".");
				$ckList[$key]['price_cv'] = $price;

			}
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['data'] = $ckList;
			$this->response($rets);
		}	
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;			
			$rets['data'] = null;
			$this->response($rets);	
		}
	}

	public function request_invoice_multicast_post($value='')
	{		
		$id_tutor 		= $this->post('id_tutor');
		$volume 		= $this->post('volume');
		$price 			= $this->post('price');
		$user_utc 		= $this->post('user_utc');
		$type 			= $this->post('type');

		$datenow 		= date("Y-m-d H:i:s");
		$server_utc 	= $this->Rumus->getGMTOffset();
		$interval 		= $user_utc - $server_utc;
		$date_requested = DateTime::createFromFormat('Y-m-d H:i:s', $datenow );
		$date_requested->modify("+".$interval ." minutes");		
		$date_requested = $date_requested->format('Y-m-d H:i:s');
		
		$datelimit      = date_create($date_requested);
	    $dateelimit     = date_format($datelimit, 'd/m/y');
	    $harilimit      = date_format($datelimit, 'd');
	    $hari           = date_format($datelimit, 'l');
	    $tahunlimit     = date_format($datelimit, 'Y');
	    $waktulimit     = date_format($datelimit, 'H:i');   
	    $datelimit      = $dateelimit;
	    $sepparatorlimit= '/';
	    $partslimit     = explode($sepparatorlimit, $datelimit);
	    $bulanlimit     = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

		$getdatatutor 	= $this->db->query("SELECT * FROM tbl_user WHERE id_user = '$id_tutor'")->row_array();
		$emailtutor 	= $getdatatutor['email'];
		$namatutor 		= $getdatatutor['user_name'];

		$insert 		= $this->db->query("INSERT INTO tbl_multicast_volume (id_tutor, volume, price, date_request, type, status, utc) VALUES ('$id_tutor','$volume','$price','$date_requested','$type','0','$user_utc')");
		$idv 			= $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
		if ($insert) {
			
			$price = number_format($price, 0, ".", ".");
			
			// Open connection
	        $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_request_multicast_volume','content' => array("email" => $emailtutor, "namatutor" => $namatutor, "price" => $price, "volume" => $volume, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit ) )));

	        // Execute post
	        $result = curl_exec($ch);
	        curl_close($ch);
	        // echo $result;       

	        if ($type == 'open_multicast') {
		        $delete = $this->db->query("UPDATE tbl_multicast_volume SET status = '1' WHERE idv='$idv'");

		        $getdata  		= $this->db->query("SELECT tmv.*, tu.user_name, tu.email FROM tbl_multicast_volume as tmv INNER JOIN tbl_user as tu ON tmv.id_tutor=tu.id_user WHERE idv = '$idv'")->row_array();

	        	$emailtutor 	= $getdata['email'];
	        	$namatutor 		= $getdata['user_name'];
	        	$price 			= $getdata['price'];
	        	$volume 		= $getdata['volume'];
	        	$emailtutor 	= $getdata['email'];
	        	$user_utc 		= $getdata['utc'];
	        	$datenow 		= $getdata['date_request'];
	        	$price 			= number_format($price, 0, ".", ".");

				$server_utc 	= $this->Rumus->getGMTOffset();
				$interval 		= $user_utc - $server_utc;
				$date_requested = DateTime::createFromFormat('Y-m-d H:i:s', $datenow );
				$date_requested->modify("+".$interval ." minutes");		
				$date_requested = $date_requested->format('Y-m-d H:i:s');
				
				$datelimit      = date_create($date_requested);
			    $dateelimit     = date_format($datelimit, 'd/m/y');
			    $harilimit      = date_format($datelimit, 'd');
			    $hari           = date_format($datelimit, 'l');
			    $tahunlimit     = date_format($datelimit, 'Y');
			    $waktulimit     = date_format($datelimit, 'H:i');   
			    $datelimit      = $dateelimit;
			    $sepparatorlimit= '/';
			    $partslimit     = explode($sepparatorlimit, $datelimit);
			    $bulanlimit     = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

	        	// Open connection
		        $ch = curl_init();

		        // Set the url, number of POST vars, POST data
		        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
		        curl_setopt($ch, CURLOPT_POST, true);
		        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_approve_multicast_volume','content' => array("email" => $emailtutor, "namatutor" => $namatutor, "price" => $price, "volume" => $volume, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit ) )));

		        // Execute post
		        $result = curl_exec($ch);
		        curl_close($ch);
		        // echo $result;
		    }

	        $rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'Sukses';
			$this->response($rets);

	    }
	    else
	    {
	    	$rets['status'] = false;
			$rets['code'] = -100;
			$rets['data'] = null;
			$this->response($rets);	
	    }
	}

	function approveRequestKuota_post()
    {
        // $id_user = $this->input->post('rtp');
        $idv = $this->post('idv');
        $base64 = strtr($idv, '-_.', '+/=');
        $idv = $this->encryption->decrypt($base64);
        $where = array(
            'idv' => $idv
        );

        $approve = $this->Process_model->approveRequestKuota("master_channel", $where);
        if ($approve == 1) {

        	$getdata  		= $this->db->query("SELECT tmv.*, tu.user_name, tu.email FROM tbl_multicast_volume as tmv INNER JOIN tbl_user as tu ON tmv.id_tutor=tu.id_user WHERE idv = '$idv'")->row_array();

        	$emailtutor 	= $getdata['email'];
        	$namatutor 		= $getdata['user_name'];
        	$price 			= $getdata['price'];
        	$volume 		= $getdata['volume'];
        	$emailtutor 	= $getdata['email'];
        	$user_utc 		= $getdata['utc'];
        	$datenow 		= $getdata['date_request'];
        	$price 			= number_format($price, 0, ".", ".");

			$server_utc 	= $this->Rumus->getGMTOffset();
			$interval 		= $user_utc - $server_utc;
			$date_requested = DateTime::createFromFormat('Y-m-d H:i:s', $datenow );
			$date_requested->modify("+".$interval ." minutes");		
			$date_requested = $date_requested->format('Y-m-d H:i:s');
			
			$datelimit      = date_create($date_requested);
		    $dateelimit     = date_format($datelimit, 'd/m/y');
		    $harilimit      = date_format($datelimit, 'd');
		    $hari           = date_format($datelimit, 'l');
		    $tahunlimit     = date_format($datelimit, 'Y');
		    $waktulimit     = date_format($datelimit, 'H:i');   
		    $datelimit      = $dateelimit;
		    $sepparatorlimit= '/';
		    $partslimit     = explode($sepparatorlimit, $datelimit);
		    $bulanlimit     = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

        	// Open connection
	        $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_approve_multicast_volume','content' => array("email" => $emailtutor, "namatutor" => $namatutor, "price" => $price, "volume" => $volume, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit ) )));

	        // Execute post
	        $result = curl_exec($ch);
	        curl_close($ch);
	        // echo $result;

            $res['status'] = true;
            $res['code'] = 200;
            $res['message'] = 'Berhasil Approve Request';            
            $this->response($res);
        }
        else
        {            
            $res['status'] = false;
            $res['code'] = -100;
            $res['message'] = 'Terjadi kesalahan system saat approve request';
            $this->response($res);
        }        
    }

    function rejectRequestKuota_post()
    {
        // $id_user = $this->input->post('rtp');
        $idv = $this->post('idv');
        $base64 = strtr($idv, '-_.', '+/=');
        $idv = $this->encryption->decrypt($base64);
        $where = array(
            'idv' => $idv
        );

        $delete = $this->Process_model->rejectRequestKuota("master_channel", $where);
        if ($delete == 1) {

        	$getdata  		= $this->db->query("SELECT tmv.*, tu.user_name, tu.email FROM tbl_multicast_volume as tmv INNER JOIN tbl_user as tu ON tmv.id_tutor=tu.id_user WHERE idv = '$idv'")->row_array();

        	$emailtutor 	= $getdata['email'];
        	$namatutor 		= $getdata['user_name'];
        	$price 			= $getdata['price'];
        	$volume 		= $getdata['volume'];
        	$emailtutor 	= $getdata['email'];
        	$user_utc 		= $getdata['utc'];
        	$datenow 		= $getdata['date_request'];
        	$price 			= number_format($price, 0, ".", ".");

			$server_utc 	= $this->Rumus->getGMTOffset();
			$interval 		= $user_utc - $server_utc;
			$date_requested = DateTime::createFromFormat('Y-m-d H:i:s', $datenow );
			$date_requested->modify("+".$interval ." minutes");		
			$date_requested = $date_requested->format('Y-m-d H:i:s');
			
			$datelimit      = date_create($date_requested);
		    $dateelimit     = date_format($datelimit, 'd/m/y');
		    $harilimit      = date_format($datelimit, 'd');
		    $hari           = date_format($datelimit, 'l');
		    $tahunlimit     = date_format($datelimit, 'Y');
		    $waktulimit     = date_format($datelimit, 'H:i');   
		    $datelimit      = $dateelimit;
		    $sepparatorlimit= '/';
		    $partslimit     = explode($sepparatorlimit, $datelimit);
		    $bulanlimit     = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

        	// Open connection
	        $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_reject_multicast_volume','content' => array("email" => $emailtutor, "namatutor" => $namatutor, "price" => $price, "volume" => $volume, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit ) )));

	        // Execute post
	        $result = curl_exec($ch);
	        curl_close($ch);
	        // echo $result;

            $res['status'] = true;
            $res['code'] = 200;
            $res['message'] = 'Berhasil Reject Request';            
            $this->response($res);
        }
        else
        {            
            $res['status'] = false;
            $res['code'] = -100;
            $res['message'] = 'Terjadi kesalahan system saat reject request';
            $this->response($res);
        }        
    }

    public function save_multicast_volume_post()
	{					
		$volume 	= $this->post('volume');
		$price 		= $this->post('price');
		$type 		= $this->post('type');

		$ckList 	= $this->db->query("INSERT INTO master_paidmulticast_volume (volume,price,type) VALUES ('$volume','$price','$type')");
		if ($ckList) {
			
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'Add Quota Successfully';
			$this->response($rets);
		}	
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;			
			$rets['message'] = 'Add Quota Failed';
			$this->response($rets);	
		}
	}

	public function delete_multicast_volume_post()
	{							
		$idv 		= $this->post('idv');
        $base64 	= strtr($idv, '-_.', '+/=');
        $idv 		= $this->encryption->decrypt($base64);

		$ckList 	= $this->db->query("DELETE FROM master_paidmulticast_volume WHERE id='$idv'");
		if ($ckList) {
			
			$rets['status'] = true;
			$rets['code'] = 200;
			$rets['message'] = 'Delete Quota Successfully';
			$this->response($rets);
		}	
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;			
			$rets['message'] = 'Delete Quota Failed';
			$this->response($rets);	
		}
	}

	public function checkKidsID_post()
	{							
		$id_user 		= $this->post('id_user');        

		$ckList 	= $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$id_user'")->row_array()['usertype_id'];
		if ($ckList == 'student kid') {			
			$rets['status'] = true;
			$rets['code'] = 200;			
			$this->response($rets);
		}	
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;
			$this->response($rets);	
		}
	}

	public function addParentKids_post()
	{
		$email 		= $this->post('email');
		$kid_id 	= $this->post('kid_id');
		$ckList 	= $this->db->query("SELECT * FROM tbl_user WHERE email = '$email' AND usertype_id = 'student' ")->row_array();
		if (!empty($ckList)) {
			$parent_id		= $ckList['id_user'];
			$getKidID 		= $this->db->query("SELECT * FROM tbl_profile_kid WHERE kid_id = '$kid_id'")->row_array();

			$school_id 		= $getKidID['school_id'];
			$school_name 	= $getKidID['school_name'];
			$school_address = $getKidID['school_address'];
			$jenjang_id 	= $getKidID['jenjang_id'];

			$ckReady 		= $this->db->query("SELECT * FROM tbl_profile_kid WHERE kid_id = '$kid_id' AND parent_id = '$parent_id'")->row_array();
			if (empty($ckReady)) {
				$this->db->query("INSERT INTO tbl_profile_kid (kid_id, school_id, school_name, school_address, jenjang_id, parent_id, is_master_parent, flag) VALUES ('$kid_id','$school_id','$school_name','$school_address','$jenjang_id','$parent_id','0','1') ");

				$rets['status'] = true;
				$rets['code'] = 200;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = false;
				$rets['code'] = -101;	
				$this->response($rets);		
			}			
		}	
		else
		{
			$rets['status'] = false;
			$rets['code'] = -100;	
			$this->response($rets);		
		}
	} 
	
}