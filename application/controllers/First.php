<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class First extends CI_Controller {

	public $sesi = array("lang" => "indonesia");

	public function __construct()
	{
		parent::__construct();
        $this->load_lang($this->session->userdata('lang'));
        $this->load->library('recaptcha');
		$this->session->set_userdata('color','blue');
		$this->load->library('email');
		// $this->load->library('fpdf');
        $this->load->helper(array('Form', 'Cookie', 'String'));
        $isMobile = false;

        $this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
		// Check for any mobile device.
		if ($detect->isMobile()){
			$this->isMobile = true;
			if (base_url() == 'https://devel.indiclass.id') {
				// redirect('https://m.classmiles.com');
			}else{

			}
			define('MOBILE', 'mobile/');

		}else{
			define('MOBILE', '');
		}
	}

	public function index($page = 0)
	{			
		$this->load->view(MOBILE.'home/header');
		$this->load->view(MOBILE.'home/index');
	}

	public function dashboard($value='')
	{		
		if($this->session->userdata('usertype_id') == "tutor"){
			redirect('/tutor');
		}
		if($this->session->userdata('usertype_id') == "admin"){
			redirect('/admin');
		}			

		$new_state = $this->db->query("SELECT status FROM tbl_user where id_user='".$this->session->userdata('id_user')."'")->row_array()['status'];				
		if($this->session->userdata('status') != $new_state ){
			echo "id user = ".$this->session->userdata('id_user');
			echo "<br> wahahaa tidak sama bung <br>";
			// redirect('/');
		}
		else
		{
			$data['sideactive'] = "home";
			$data['page'] = $page;
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'myclass',$data);
		}
		// if($this->session->has_userdata('id_user')){
		// if($this->session_check() == 1){
			// $data['sideactive'] = "home";
			// $data['page'] = $page;
			// $this->load->view(MOBILE.'inc/header');
			// $this->load->view(MOBILE.'myclass',$data);
		// }else{
			// echo MOBILE;

			// $this->session->set_userdata('lang','indonesia');
			// $this->load->view(MOBILE.'home/header');
			// $this->load->view(MOBILE.'home/index');					
		// }		
		
	}

	public function MyClass($page = 0)
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "myclass";
			$data['page'] = $page;
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'myclass',$data);
		}
		else{
			redirect('/');
		}
	}
	
	public function Camera()
	{
		// echo "a";
		// return false;
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'camera/index');
	}

	public function mobile()
	{
		if ($this->isMobile){
			$this->load->view(MOBILE.'home/header');
			$this->load->view(MOBILE.'home/mobile');
		}else{
			redirect('/');
		}
	}

	public function web()
	{
		// $this->load->view(MOBILE.'home/header');
		$this->load->view(MOBILE.'home/web');
	}		

	public function syarat_ketentuan()
	{
		$this->load->view(MOBILE.'home/header');
		$this->load->view(MOBILE.'home/syarat-kententuan');
	}

	public function cobacalendar()
	{		
		$this->load->view(MOBILE.'cobacalendar');
	}

	public function subjects()
	{	
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "subject";
			$data['allsub'] = $this->Master_model->getSubject();
			// $data['getTutor'] = $this->Master_model->
			$data['alltutor'] = $this->Master_model->getMyTutor();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'subjects',$data);
		}
		else{
			redirect('/');
		}

		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}
	public function all_subject()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "all_subject";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'all_subject',$data);			
		}
		else{
			redirect('/');
		}
		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}

	public function ondemand()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "ondemand";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'on_demand',$data);
		}
		else{
			redirect('/');
		}
	}

	public function data_ondemand()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "ondemand";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'data_ondemand',$data);
		}
		else{
			redirect('/');
		}
	}	

	public function classhistory()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "classhistory";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'replay/classhistory',$data);
		}
		else{
			redirect('/');
		}
	}

	public function uangsaku()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "uangsaku";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'uangsaku',$data);
		}
		else{
			redirect('/');
		}
	}

	public function Epocket()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student") {
			$data['sideactive'] = "daftartransaksi";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/epocket', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Accountlist()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student") {
			$data['sideactive'] = "accountlist";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/accountlist', $data);
		}
		else
		{
			redirect('/');
		}
	}
	
	public function editbank()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student") {
			$data['sideactive'] = "accountlist";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/editbank', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Topup()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student") {
			$data['sideactive'] = "topup";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/topup', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Complain()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "complain";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'inc/complain',$data);
		}
		else{
			redirect('/');
		}
	}

	public function Transfer()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student") {
			$data['sideactive'] = "topup";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/transfer', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Cara_topup()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student") {
			$data['sideactive'] = "caratopup";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/caratopup', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Konfirmasi()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student") {
			$data['sideactive'] = "daftartransaksi";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/konfirmasi', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Details()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student") {
			$data['sideactive'] = "daftartransaksi";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/details', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function sukses()
	{
		$data['sideactive'] = "topup";
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'topup/sukses',$data);
	}

	public function failed()
	{
		$data['sideactive'] = "topup";
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'topup/failed',$data);
	}

	public function ondemandpalsu()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "ondemand";
			$this->load->view(MOBILE.'inc/cssjs_load');
			$this->load->view(MOBILE.'ondemand_palsu',$data);
		}
		else{
			redirect('/');
		}
	}
	public function history()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "history";
			$data['dataimage'] = $this->Master_model->ambildataphoto();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'history',$data);
		}
		else{
			redirect('/');
		}
		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}

	public function Reputation()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "reputation";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'review',$data);
		}
		else{
			redirect('/');
		}
	}

	public function KidsRegister()
	{
		if($this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "mykids_registrasion";
			$this->load->view(MOBILE.'inc/header');
			$data['alluserprofile'] = $this->Master_model->getTblUserAndProfile();
			$this->load->view(MOBILE.'kids/register',$data);
		}
		else{
			redirect('/');
		}
	}

	public function profile()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			if ($this->session->flashdata('rets')) {
				$data = $this->session->flashdata('rets');
			}
			$data['sideactive'] = "profile";
			$this->load->view(MOBILE.'inc/header');
			$data['alluserprofile'] = $this->Master_model->getTblUserAndProfile();
			$data['dataimage'] = $this->Master_model->ambildataphoto();		
			$this->load->view(MOBILE.'profile',$data);
		}
		else{
			redirect('/');
		}		
	}
	public function set_profile()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			if ($this->session->flashdata('rets')) {
				$data = $this->session->flashdata('rets');
			}
			$data['sideactive'] = "profile";
			$this->load->view(MOBILE.'inc/header');
			$data['alluserprofile'] = $this->Master_model->getTblUserAndProfile();
			$data['dataimage'] = $this->Master_model->ambildataphoto();		
			$this->load->view(MOBILE.'set_profile',$data);
		}
		else{
			redirect('/');
		}		
	}

	public function about()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student" ){
			$data['sideactive'] = "profile";
			$data['profile'] = $this->Master_model->getTblUserAndProfile();
			$data['dataimage'] = $this->Master_model->ambildataphoto();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'about',$data);
		}
		else{
			redirect('/');
		}
		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}
	public function profile_picture()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student" ){
			$data['sideactive'] = "profile";
			$data['profile'] = $this->Master_model->getTblUserAndProfile();
			$data['dataimage'] = $this->Master_model->ambildataphoto();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'profile_picture',$data);
		}
		else{
			redirect('/');
		}

	}
	public function profile_account()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){

			if ($this->session->flashdata('rets')) {
				$data = $this->session->flashdata('rets');
			}

			$data['sideactive'] = "profile_account";		
			$data['alluser'] = $this->Master_model->getTblUser();
			$data['dataimage'] = $this->Master_model->ambildataphoto();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'profile_account',$data);
		}
		else{
			redirect('/');
		}
		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}
	public function profile_school()
	{
		if($this->session_check() == 1 && ($this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == 'student')){
			if($this->session_check() == 1){
				if ($this->session->flashdata('rets')) {
					$data = $this->session->flashdata('rets');
				}
				$data['sideactive'] = "profile";
				$this->load->view(MOBILE.'inc/header');
				$data['alluserprofile'] = $this->Master_model->getTblUserAndProfile();
				$data['dataimage'] = $this->Master_model->ambildataphoto();		
				$this->load->view(MOBILE.'profile_school',$data);
			}
			else{
				echo "asdfsdf";
			}
		}
		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}
	public function school_kids()
	{
		if($this->session_check() == 1 && ($this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == 'student')){
			if($this->session_check() == 1){
				if ($this->session->flashdata('rets')) {
					$data = $this->session->flashdata('rets');
				}
				$data['sideactive'] = "profile";
				$this->load->view(MOBILE.'inc/header');
				$data['alluserprofile'] = $this->Master_model->getTblUserAndProfile();
				$data['dataimage'] = $this->Master_model->ambildataphoto();		
				$this->load->view(MOBILE.'school_kids',$data);
			}
			else{
				echo "asdfsdf";
			}
		}
		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}

	function ambil_data(){

		$modul=$this->input->post('modul');
		$id=$this->input->post('id');

		if($modul=="kabupaten"){
			echo $this->Master_model->kabupaten($id);
		}
		else if($modul=="kecamatan"){
			echo $this->Master_model->kecamatan($id);
		}
		else if($modul=="kelurahan"){
			echo $this->Master_model->kelurahan($id);
		}
	}

	function getlevels(){		
		$name= $this->input->post('name');
		echo $this->Master_model->levels($name);		
	}

	function getlevelsregister(){		
		$name= $this->input->post('name');
		echo $this->Master_model->levelsregister($name);		
	}
	
	public function profile_forgot()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student") {
			$data['sideactive'] = "profile_forgot";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'profile_forgot',$data);
		}
		else{
			redirect('/');
		}
	}

	public function geo_location()
	{
		$this->load->view('inc/geoplugin_activation');
	}

	public function DetailPayment()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student") {
			$data['sideactive'] = "home";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'order_Transfer',$data);
		}
		else{
			redirect('/');
		}
	}

	public function ConfirmPayment()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student") {
			$data['sideactive'] = "home";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'order_Konfirmasi',$data);
		}
		else{
			redirect('/');
		}
	}

	public function DetailOrder()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid" || $this->session->userdata('usertype_id') != "student") {
			$data['sideactive'] = "home";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'order_Details',$data);
		}
		else{
			redirect('/');
		}
	}
	public function register_email()
	{			
		if ($this->session_check() != 1) {	
			$data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100% width:100%;" background="'.CDN_URL.STATIC_IMAGE_CDN_URL.'blur/img3.jpg"';
			$this->load->view(MOBILE.'inc/header',$data);
			$new['1'] = null;
			if($this->session->flashdata('google_data')){
				$new['google_data'] = $this->session->flashdata('google_data');
			}
			if($this->session->userdata('lang') == 'indonesia'){
				$this->session->set_userdata('lang','indonesia');
			}
			else
			{
				$this->session->set_userdata('lang','english');
			}
			$this->load->view(MOBILE.'register_email',$new);
		}
		else
		{
			redirect('/');
		}
	}

	public function newPassword()
	{	
		$r_link = $this->input->get('randomLink');
		$email_user = $this->input->get('email');
		$cek_valid = $this->db->query("SELECT *FROM gen_link WHERE random_link='$r_link' AND email_user='$email_user'")->row_array();

		if ($cek_valid == null) {
			$load = $this->lang->line('linkexpired');
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',$load);
			redirect('/');
		}else{
			$data['sideactive'] = "profile_account";
		 	$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'newpassword',$data);
		}

			
	}

	// public function login_mobile()
	// {
	// 	if ($this->session_check() != 1) {	
	// 	$data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
	// 	$this->load->view(MOBILE.'inc/header', $data);
	// 	$this->load->view(MOBILE.'login_mb');	
	// 	}else{
	// 		redirect('/');
	// 	}	
	// }

	public function login_web()
	{
		// if ($this->session_check() != 1) {	
		// $data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		// if($this->session->userdata('lang') == 'indonesia'){
		// 		$this->session->set_userdata('lang','indonesia');
		// 	}
		// 	else
		// 	{
		// 		$this->session->set_userdata('lang','english');
		// 	}
		// $this->load->view(MOBILE.'inc/header', $data);
		// $this->load->view(MOBILE.'login_wb');	
		// }else{
			redirect('/');
		// }	
	}

	public function login()
	{
		// if ($this->session_check() != 1) {	
		// 	$data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		// 	if($this->session->userdata('lang') == 'indonesia'){
		// 		$this->session->set_userdata('lang','indonesia');
		// 	}
		// 	else
		// 	{
		// 		$this->session->set_userdata('lang','english');
		// 	}			
		// 	$this->load->view(MOBILE.'inc/header', $data);
		// 	$this->load->view(MOBILE.'login');	
		// }else{
			redirect('/');
		// }	
	}

	public function verifikasi($r_key){
		$detect = new Mobile_Detect();

		// redirect(BASE_URL().'first/verifikasi/'.$r_key);
		$res = $this->M_login->getAuth($r_key);
		if (empty($res)) {
			// $this->load->view(MOBILE.'inc/header');
			// $this->load->view(MOBILE.'s_ver');
			// $this->session->set_userdata("code", "1088");
			// redirect('/');
			$data['data'] = 1;
			$this->load->view(MOBILE.'home/header');
			$this->load->view(MOBILE.'home/s_ver',$data);
		}else{
			$upd = $this->db->simple_query("UPDATE tbl_user SET status='1' WHERE id_user='$res[id_user]'");
			if ($upd) {
				$del = $this->db->simple_query("DELETE FROM gen_auth WHERE id_user='$res[id_user]'");
				// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,status) VALUES ('Yeay verifikasi telah berhasil, silahkan pilih mata pelajaran dan guru untuk memulai kelas private.','general','$userid','https://beta.dev.meetaza.com/classmiles/first/subjects','0')");

				// $simpan_notif = $this->Rumus->create_notif($notif='Yeay verifikasi telah berhasil, silahkan pilih mata pelajaran dan guru untuk memulai kelas private.',$notif_mobile='Yeay verifikasi telah berhasil, silahkan pilih mata pelajaran dan guru untuk memulai kelas private.',$notif_type='single',$id_user=$userid,$link='https://classmiles.com/first/subjects');

				//TAMBAH NOTIF
				$notif_message = json_encode( array("code" => 34));
				$link = BASE_URL()."/Subjects";
				$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$res[id_user]','$link','0')");
				
				$data['data'] = 900;
				$this->load->view(MOBILE.'home/header');
				$this->load->view(MOBILE.'home/s_ver',$data);
			}
		}
	}

	public function verifikasiTutor($r_key){
		$detect = new Mobile_Detect();

		// Check for any mobile device.
		if ($detect->isMobile()){
			// redirect('https://m.classmiles.com/first/verifikasi/'.$r_key);
			$res = $this->M_login->getAuth($r_key);
			if (empty($res)) {
				$data['data'] = 901;
				$this->load->view(MOBILE.'home/header');
				$this->load->view(MOBILE.'home/s_ver',$data);				
			}else{
				$upd = $this->db->simple_query("UPDATE tbl_user SET status='3' WHERE id_user='$res[id_user]'");
				$userid = $res['id_user'];
				if ($upd) {
					$del = $this->db->simple_query("DELETE FROM gen_auth WHERE id_user='$res[id_user]'");
					// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,status) VALUES ('Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.','complete profile','$userid','https://beta.dev.meetaza.com/classmiles/tutor/approval','0')");

					//NOTIF
					$notif_message = json_encode( array("code" => 43));	
					$link = BASE_URL()."/tutor/approval";
					$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$userid','$link','0')");

					// $simpan_notif = $this->Rumus->create_notif($notif='Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.',$notif_mobile='Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.',$notif_type='single',$id_user=$userid,$link='https://classmiles.com/tutor/approval');
					$data['data'] = 901;
					$this->load->view(MOBILE.'home/header');
					$this->load->view(MOBILE.'home/s_ver',$data);		
				}
			}
		}
		else{
			$res = $this->M_login->getAuth($r_key);
			if (empty($res)) {				
				$this->session->set_userdata("code", "108");
				redirect('/');
			}else{						
				$upd = $this->db->simple_query("UPDATE tbl_user SET status='3' WHERE id_user='$res[id_user]'");
				$userid = $res['id_user'];
				if ($upd) {
					$del = $this->db->simple_query("DELETE FROM gen_auth WHERE id_user='$res[id_user]'");
					// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,status) VALUES ('Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.','complete profile','$userid','https://beta.dev.meetaza.com/classmiles/tutor/approval','0')");

					//NOTIF
					$notif_message = json_encode( array("code" => 43));	
					$link = "https://classmiles.com/tutor/approval";
					$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','$link','0')");

					// $simpan_notif = $this->Rumus->create_notif($notif='Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.',$notif_mobile='Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.',$notif_type='single',$id_user=$userid,$link='https://classmiles.com/tutor/approval');
					$this->session->set_userdata("code", "110");
					$load = $this->lang->line('activationsuccess');
					$this->session->set_flashdata('mes_alert','success');
					$this->session->set_flashdata('mes_display','block');
					$this->session->set_flashdata('mes_message',$load);						
					redirect('/');
				}
			}

			// }
		}
	}

	function gen_session(){

			$digit = 32;
			$karakter = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			srand((double)microtime()*1000000);
			$i = 0;
			$pass = "";
			while ($i <= $digit-1)
			{
			$num = rand() % 32;
			$tmp = substr($karakter,$num,1);
			$pass = $pass.$tmp;
			$i++;
			}
			echo $pass;

		// 	$where = array(
		// 			'random_key' => $pass
		// 		);

		// $cek = $this->M_login->saveRandKey("gen_auth", $where);
		// $this->session->set_flashdata('cek', $cek);	


	}
	
	public function register()
	{		
		if(isset($_GET['email']) || isset($_GET['type']))
		{
			if (!isset($_GET['type'])) {
				$email 	= $_GET['email'];
				$data['typeregister'] = "";
			}
			else if (!isset($_GET['email'])) {
				$typee 	= $_GET['type'];
				$data['typeregister'] = $typee;
			}
			else
			{
				$email 	= $_GET['email'];
				$typee 	= $_GET['type'];
				$data['typeregister'] = $typee;
			}

			$dataJenjangname = $this->Master_model->getDataJenjangname();
			if($this->session->flashdata('form_cache') != null){
				$data['form_cache'] = $this->session->flashdata('form_cache');
			}
			$data['jenjang_name'] = $dataJenjangname;
			$data['levels']=$this->Master_model->jenjangnameregister();
			$this->load->view(MOBILE.'inc/header', $data);
			$data['email'] = $email;
			
			$this->load->view(MOBILE.'register_thirdparty',$data);	
		}
		else
		{
			$dataJenjangname = $this->Master_model->getDataJenjangname();
			if($this->session->flashdata('form_cache') != null){
				$data['form_cache'] = $this->session->flashdata('form_cache');
			}
			$data['jenjang_name'] = $dataJenjangname;
			$data['levels']=$this->Master_model->jenjangnameregister();
			$this->load->view(MOBILE.'inc/header', $data);
			$this->load->view(MOBILE.'register');
		}

	}
	public function register_mobile()
	{		
		// $data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		$dataJenjangname = $this->Master_model->getDataJenjangname();


		if($this->session->flashdata('form_cache') != null){
			$data['form_cache'] = $this->session->flashdata('form_cache');
		}
		$data['jenjang_name'] = $dataJenjangname;
		$data['levels']=$this->Master_model->jenjangnameregister();
		// if($this->session->userdata('lang') == 'indonesia'){
		// 	$this->session->set_userdata('lang','indonesia');
		// }
		// else
		// {
		// 	$this->session->set_userdata('lang','english');
		// }

		$this->load->view(MOBILE.'inc/header', $data);
		$this->load->view(MOBILE.'register_m');

	}

	public function tutor_register(){
		// $data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'register_tutor');
	}

	public function forgot()
	{
		$data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		if($this->session->userdata('lang') == 'indonesia'){
			$this->session->set_userdata('lang','indonesia');
		}
		else
		{
			$this->session->set_userdata('lang','english');
		}
		$this->load->view(MOBILE.'inc/header', $data);
		$this->load->view(MOBILE.'forgot.php');
	}

	public function resendEmail()
	{
		$data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		$this->load->view(MOBILE.'inc/header', $data);
		$this->load->view(MOBILE.'resendEmail.php');
	}	

	public function session_check()
	{
		if($this->session->userdata('id_user') != ''){
			$new_state = $this->db->query("SELECT status FROM tbl_user where id_user='".$this->session->userdata('id_user')."'")->row_array()['status'];
			if($this->session->userdata('status') != $new_state ){
				// $this->session->sess_destroy();
				redirect('/');
			}
			// $this->sesi['user_name'] = $this->session->userdata('user_name');
			// $this->sesi['username'] = $this->session->userdata('username');
			// $this->sesi['priv_level'] = $this->session->userdata('priv_level');
			if($this->session->userdata('usertype_id') != "student" && $this->session->userdata('usertype_id') != "student kid"){
				redirect('/');
			}
			// if($this->session->userdata('usertype_id') != "student kid"){
			// 	redirect('/');
			// }
			return 1;
		}else{
			return 0;
		}
	}

	public function validationCheck(){

	}

	public function load_lang($lang = '')
	{
		$this->lang->load('umum',$lang);
	}
	public function set_lang($value='')
	{	
		if($value == ""){
			$this->session->set_userdata('lang','indonesia');
		}else{
			$this->session->set_userdata('lang',$value);
		}		
		
		return null;
	}

	public function mails(){
		$this->load->view(MOBILE.'mail_layout_reg');
	}
	public function createSession(){
		//echo $this->M_login->createSess($this->session->userdata('id_user'), $this->session->userdata('email')); 
	}

	public function getTutorProfile($id_user='')
	{
		$data = $this->db->query("SELECT tu.*, tpt.self_description, tpt.education_background, tpt.teaching_experience FROM tbl_user as tu INNER JOIN tbl_profile_tutor as tpt ON tu.id_user=tpt.tutor_id WHERE tu.id_user='".$id_user."'")->row_array();
		if($data['education_background'] != ''){
			$data['education_background'] = unserialize($data['education_background']);
		}
		if($data['teaching_experience'] != ''){
			$data['teaching_experience'] = unserialize($data['teaching_experience']);
		}
		echo json_encode($data);
		return null;
	}

	public function approveTheTutor()
	{
		$id_user 	= $this->input->get('id_user');
		$text 		= $this->input->get('text');
		$data 		= $this->db->simple_query("UPDATE tbl_user SET status=1 WHERE id_user='".$id_user."'");
		// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,status) VALUES ('Yeay, anda telah di terima sebagai guru di classmiles. Pilih mata pelajaran dan mulai membuat kelas anda.','Approval Tutor','$id_user','','0')");
		if ($data) {
			$upd 		= $this->db->simple_query("UPDATE tbl_profile_tutor SET conclusion='".$text."' WHERE tutor_id='".$id_user."'");
			$selectname	= $this->db->query("SELECT * FROM tbl_user WHERE id_user='$id_user'")->row_array();
			$usernamee	= $selectname['user_name'];
			$emailll	= $selectname['email'];
			
			// Open connection
	        $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_approveTutor','content' => array("emailll" => "$emailll", "usernamee" => "$usernamee") )));

	        // Execute post
	        $result = curl_exec($ch);
	        curl_close($ch);
	        // echo $result;

	        // SEND TO HERMAWAN MEETAZA THE OWNER
			$emailkedua = "hermawan@meetaza.com";
			$ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_approveTutor_CC','content' => array("emailll" => "$emailll", "usernamee" => "$usernamee", "emailkedua" => "$emailkedua") )));

	        // Execute post
	        $result = curl_exec($ch);
	        curl_close($ch);
	        // echo $result;

	        if ($result) {
              	$load = $this->lang->line('approvesuccess');
				$rets['mes_alert'] ='success';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				// $this->session->set_userdata('message', 'tutorregistered');
				$this->session->set_flashdata('rets',$rets);			
				$simpan_notif = $this->Rumus->create_notif($notif='Yeay, anda telah di terima sebagai guru di classmiles. Pilih mata pelajaran dan mulai membuat kelas anda.',$notif_mobile='Yeay, anda telah di terima sebagai guru di classmiles. Pilih mata pelajaran dan mulai membuat kelas.',$notif_type='single',$id_user=$id_user,$link='https://classmiles.com/tutor/choose');
				
				redirect('admin/tutor_approval');
            }else{
                  redirect('admin/tutor_approval');
            }
		}
		else
		{
			
			redirect('admin/tutor_approval');
		}	
		return null;

	}
	public function declineTheTutor()
	{
		$id_user 		= $this->input->get("id_user");
		$text 			= $this->input->get('text');
		$data 			= $this->db->simple_query("UPDATE tbl_user SET status=-1 WHERE id_user='".$id_user."'");
		$upd 			= $this->db->query("UPDATE tbl_profile_tutor SET conclusion='".$text."' WHERE tutor_id='".$id_user."'");
		$simpan_notif 	= $this->Rumus->create_notif($notif='Maaf permintaan anda tidak diterima, anda belum memenuhi syarat sebagai tutor di Classmiles.',$notif_mobile='Maaf permintaan anda tidak diterima, anda belum memenuhi syarat sebagai tutor di Classmiles.',$notif_type='single',$id_user=$id_user,$link='');
		return null;
	}
	public function test_m()
	{
		$a = $this->Master_model->getAllKelurahan('1702041002','1702041');
		print_r($a);
	}

	public function expicture()
	{
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'ex_picture');
	}

	public function imgcrop()
	{
		// $this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'imgselect/index');
	}

	public function post_profile($act = ''){
    	 //print_R($post);die;
		// echo $act;
		// return null;
    	 switch($act) {
    	  case 'save' :
    		$this->Master_model->saveAvatarTmp();
    	  break;
    	  default:
    		$this->Master_model->changeAvatar();
    		
    	 }
	}

	public function test()
	{			
		$this->load->view(MOBILE."classroom_config/header");
		$this->load->view(MOBILE."classroom_view/test");		
	}
	public function test2()
	{	
		$this->load->view(MOBILE."inc/header");
		// // print_r($this->session->userdata('form_cache'));
		$this->load->view(MOBILE."inc/test2");
		// $folder = $this->Rumus->classDrive_listFolders('32');
		// $listfile = $this->Rumus->classDrive_listImages('32','Live Stream malming/');	
		// echo json_encode($listfile);
	}

	public function test3()
	{	
		// print_r($this->session->userdata('form_cache'));
		$data['sideactive'] = "home";
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE."inc/test3", $data);
		
	}

	public function me()
	{	
		// print_r($this->session->userdata('form_cache'));
		$data['sideactive'] = "home";
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE."inc/me");
		
	}

	public function test4()
	{		
		$user 	= $this->db->query("SELECT * FROM tbl_user WHERE id_user = '9316000'")->row_array();
		print_r($user);
		// $this->load->view(MOBILE.'inc/header');
		// $this->load->view(MOBILE."inc/test4");
		// $this->load->view(MOBILE."inc/footer");
		// print_r($this->session->userdata('form_cache'));
		// $data['sideactive'] = "home";
		// $request_id = "71";
		// $type = "private";
		// $this->Process_model->reject_request($request_id, 'private');
		// $data['data'] = "0";
		// $data['allsub'] = $this->Master_model->getSubject();
		// $this->load->view(MOBILE.'home/header');
		// $this->load->view(MOBILE."login_channel");
		
	}

	public function s_ver()
	{
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'s_ver');
	}

    public function charge()
    {
    	// print_r('<pre>');
    	// print_r($this->input->post());
    	// print_r('</pre>');
    	// return null;

    	// $mobile = $this->input->post('mobile');
  //   	echo $mobile;
  //   	return false;
    	$transaction_details = array(
		  'order_id' => rand(),
		  'gross_amount' => '', // no decimal allowed for creditcard
		);

		// Optional
		$customer_details = array(
		  'first_name'    => $this->session->userdata('user_name'),
		  'email'         => $this->session->userdata('email'),
		  'phone'         => $this->session->userdata('no_hp')
		  // 'billing_address'  => $billing_address,
		  // 'shipping_address' => $shipping_address
		);

		// Fill transaction details
		$transaction = array(
		  'transaction_details' => $transaction_details,
		  'customer_details' => $customer_details,
		);
		$snapToken = $this->midtrans->getSnapToken($transaction);
		error_log($snapToken);
		$data['snap'] = $snapToken;
    	$this->load->view(MOBILE.'topup/test', $data);
    }

    public function classroom()
    {
    	// $this->load->view(MOBILE.'inc/header');
    	// $this->load->view(MOBILE.'classroom_view/classroom');
    	$this->load->view(MOBILE."classroom_config/header");
		$this->load->view(MOBILE."classroom_view/test");
    }

    public function testclassroom()
    {
    	$this->load->view(MOBILE.'inc/header');
    	$this->load->view(MOBILE.'classroom/main');
    }

    public function testclassroomm()
    {
    	$this->load->view(MOBILE.'inc/header');
    	$this->load->view(MOBILE.'classroom/test2');
    }

    public function load(){
    	$this->load->view(MOBILE.'classroom/test3');
    }

    public function android()
    {	
    	$this->load->view(MOBILE."classroom_config/header");
    	$this->load->view(MOBILE."classroom_view/classandro");
    }

    public function fbtest()
    {	
    	$this->load->view(MOBILE."inc/header");
    	$this->load->view(MOBILE."classroom/fbtest");
    }

    public function testsubject()
    {
    	if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "subject";
			$data['allsub'] = $this->Master_model->getSubject();
			// $data['getTutor'] = $this->Master_model->
			$data['alltutor'] = $this->Master_model->getMyTutor();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'new_subject',$data);
		}
		else{
			redirect('/');
		}
    }

    public function player()
    {
    	$this->load->view(MOBILE."inc/player");
    }


   function add(){
     // retrieve the album and add to the data array
       $data['log_trf_confirmation'] = $this->Process_model->getnorek();
       $this->load->view(MOBILE.'topup/konfirmasi, $data');
      }

      function getnorekk(){
      	$trfc = $this->input->post('trfc_id');
      	$norek = $this->getnorek($trfc);
      	$data = "<option value=''>--Pilih--</option>";
      	foreach ($norek as $mp) {

      		$data = "<option value='$mp[norekid]'>$mp[norekno]</option>/n";
      	}
      	echo $data;

      }     


      function addn(){
     // retrieve the album and add to the data array
       $data['log_trf_confirmation'] = $this->Process_model->getnorek();
       $this->load->view(MOBILE.'topup/konfirmasi, $data');
      }

      function getnorekkn(){
      	$trfc = $this->input->post('trfc_idn');
      	$norek = $this->getnorek($trfcn);
      	$data = "<option value=''>--Pilih--</option>";
      	foreach ($norekn as $mp) {

      		$data = "<option value='$mp[norekidn]'>$mp[noreknon]</option>/n";
      	}
      	echo $data;

      }     


       function getid(){
     // retrieve the album and add to the data array
       $data['log_trf_confirmation'] = $this->Process_model->getnorek();
       $this->load->view(MOBILE.'topup/konfirmasi, $data');
      }

      function getidn(){
      	$trfc = $this->input->post('trfc_idn');
      	$norek = $this->getnorek($trfcn);
      	$data = "<option value=''>--Pilih--</option>";
      	foreach ($idn as $mp) {

      		$data = "<option value='$mp[idn]'>$mp[idnn]</option>/n";
      	}
      	echo $data;

      }     

	//LEARN ANGULAR
	public function directive()
	{
		$this->load->view(MOBILE.'inc/directive');		
	}


	//Paypal
	public function do_purchase(){
		$email = $this->session->userdata('email');
		$nominal = $this->input->post('nominalsave');
		$config['business']					= $email;
		$config['cpp_header_image']			= '';
		$config['return']					= base_url().'first/sukses';
		$config['cancel_return']			= base_url().'first/failed';
		$config['notify_url']				= 'process_payment.php';
		$config['production']				= TRUE;
		$config['invoice']					= '';

		$this->load->library('paypal',$config);
		$this->paypal->add('Topup Saldo Classmiles',$nominal);

		$this->paypal->pay();

	}
	public function notify_payment(){
		$received_data = print_r($this->input->post(),TRUE);
		echo"<pre>".$received_data."<pre>";

	}
	public function cancel_payment(){

	}

	public function cobapdf(){
	   // define('FPDF_FONTPATH',$this->config->item('fonts_path'));
        $this->load->library('fpdf');
   $this->fpdf->FPDF("L","cm","A4");
   // kita set marginnya dimulai dari kiri, atas, kanan. jika tidak diset, defaultnya 1 cm
   $this->fpdf->SetMargins(1,1,1);
   /* AliasNbPages() merupakan fungsi untuk menampilkan total halaman
   di footer, nanti kita akan membuat page number dengan format : number page / total page
   */
   $this->fpdf->AliasNbPages();
   // AddPage merupakan fungsi untuk membuat halaman baru
	$this->fpdf->AddPage();
  // Setting Font : String Family, String Style, Font size
  	$this->fpdf->SetFont('Times','B',12);
  	$this->fpdf->Cell(30,0.7,'Laporan Produk dhani Jasa Jawa Barat',0,'C','C');
  	$this->fpdf->Ln();
	$this->fpdf->Cell(30,0.7,'Periode Bulan Agustus',0,'C','C');
 	$this->fpdf->Line(1,3.5,30,3.5);
 	$this->fpdf->Output("laporan.pdf","I");
	}
}
