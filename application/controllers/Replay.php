<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Replay extends CI_Controller {

	public $sesi = array("lang" => "indonesia");

	public function __construct()
	{
		parent::__construct();
        // $this->load_lang($this->session->userdata('lang'));
		$this->session->set_userdata('color','blue'); 
		$this->load->library('email');
	 	$this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
		// Check for any mobile device.
		if ($detect->isMobile()){
			define('MOBILE', 'mobile/');
		}else{
			define('MOBILE', '');
		}
	}
	public function session_check()
	{
		if($this->session->has_userdata('id_user')){
			$new_state = $this->db->query("SELECT status FROM tbl_user where id_user='".$this->session->userdata('id_user')."'")->row_array()['status'];
			if($this->session->userdata('status') != $new_state ){
				$this->session->sess_destroy();
				redirect('/');
			}
			// $this->sesi['user_name'] = $this->session->userdata('user_name');
			// $this->sesi['username'] = $this->session->userdata('username');
			// $this->sesi['priv_level'] = $this->session->userdata('priv_level');
			if($this->session->userdata('usertype_id') != "student"){
				redirect('/tutor');
			}
			return 1;
		}else{
			return 0;
		}
	}
	public function classreplay()
    {
    	if($this->input->get('c') != ''){
    		$this->ViewHistory($this->input->get('c'));
    		return null;
    	}
    	$data['sideactive'] = "classhistory";
    	// $data['class'] = $this->Rumus->my_ended_class();
    	$this->load->view(MOBILE."inc/header",$data);
    	$this->load->view(MOBILE."replay/classhistory");
    }

    public function viewhistory($class_id='')
    {
    	// $class_id = $_GET['c'];
    	$data['sideactive'] = "classhistory";
    	$data['class'] = $this->Rumus->get_class_info($class_id);
    	$this->load->view(MOBILE."inc/header");
    	$this->load->view(MOBILE."replay/viewhistory",$data);
    }

    
}