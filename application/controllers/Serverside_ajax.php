<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Serverside_ajax extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function tutorChooseSubject($given_id_user='')
	{
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
			)
		{
			$dt = $_POST;
			
			$dt['col-display'] = array(
				'CAST(@rownum as UNSIGNED)',
				'ms.subject_name',
				// 'ms.jenjang_id',
				// 'ms.jenjang_level',
				// "ms.status"
				);
			$field_sorter = array(
				'CAST(@rownum as UNSIGNED)',
				'ms.subject_name',
				// 'ms.jenjang_id',
				// 'ms.jenjang_level',
				// "ms.status"
				);

			$sql  = "SELECT @rownum:=@rownum+1 seq, ms.*, tb.uid, tb.id_user, tb.tutor_id, tb.status FROM master_subject ms LEFT JOIN tbl_booking tb ON ms.subject_id=tb.subject_id and tb.id_user='$given_id_user' , (SELECT @rownum:=0) r  ";
			$sql1 = $sql;
			$data = $this->db->query($sql);
			$rowCount = $data->num_rows();
			$data->free_result();
        // pengkondisian aksi seperti next, search dan limit
			$columnd = $dt['col-display'];
			$count_c = count($columnd);
        // search
			$search = $dt['search']['value'];
	        /**
	         * Search Global
	         * pencarian global pada pojok kanan atas
	         */
	        $where = '';
	        if ($search != '') {   
	        	for ($i=0; $i < $count_c ; $i++) {
	        		$where .= $columnd[$i] .' LIKE "%'. $search .'%"';

	        		if ($i < $count_c - 1) {
	        			$where .= ' OR ';
	        		}
	        	}
	        }
	        if($where != ''){
	        	$where = '( '.$where.' )';
	        }
	        
	        /**
	         * Search Individual Kolom
	         * pencarian dibawah kolom
	         */
	        /*for ($i=0; $i < $count_c; $i++) { 
	        	$searchCol = $dt['columns'][$i]['search']['value'];
	        	if ($searchCol != '') {
	        		if($where != ''){
	        			$where .= ' AND '. $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';	
	        		}else{
	        			$where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
	        		}
	        		
	        		// break;
	        	}
	        }*/
	        /**
	         * pengecekan Form pencarian
	         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
	         */
	        if ($where != '') {
	        	$sql .= " WHERE " . $where; // . ($profile_where != "" ? " AND (".$profile_where.")" : "")
	        	/*if($perso_status != ''){
	        		$sql .= " AND ".$perso_status;
	        	} */

	        }else{
	        	// $sql .= " WHERE ". $perso_status . ($profile_where != "" ? " AND (".$profile_where.")" : "");
	        }
	        
        // sorting
	        $sql .= " ORDER BY {$field_sorter[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
	        
	        // $sql = $this->db->escape_str($sql);
	        $list = $this->db->query($sql);
	        $rowCount2 = $list->num_rows();
	        $list->free_result();
        // limit
	        $start  = $dt['start'];
	        $length = $dt['length'];
	        if($length == -1){
	        	$length = $rowCount;
	        }
	        $sql .= " LIMIT {$start}, {$length}";
	        $list = $this->db->query($sql);
	        /**
	         * convert to json
	         */
	        $option['draw']            = $dt['draw'];
	        $option['recordsTotal']    = $rowCount;
        // if($sql != $sql1){
	        $option['recordsFiltered'] = $rowCount2;
        // }else{
        	// $option['recordsFiltered'] = $rowCount;
        // }
	        
	        $option['data'] = array();
	        foreach ($list->result_array() as $row) {

	        	$jenjanglevel = array();
				$jenjangid = json_decode($row['jenjang_id'], TRUE);
				if ($jenjangid != NULL) {
					if(is_array($jenjangid)){
						foreach ($jenjangid as $key => $value) {									
							$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();	
							$jenjangname[] = $selectnew['jenjang_name']; 
							// $jenjanglevel[] = $selectnew['jenjang_level'];
							if ($selectnew['jenjang_level']=="0") {
								$jenjanglevel[] = '-';
							}
							else{
								$jenjanglevel[] = $selectnew['jenjang_level'];	
							}
						}	
					}else{
						$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();	
						$jenjangname[] = $selectnew['jenjang_name']; 
						if ($selectnew['jenjang_level']=="0") {
							$jenjanglevel[] = '-';
						}
						else{
							$jenjanglevel[] = $selectnew['jenjang_level'];	
						}
						// $jenjanglevel[] = $selectnew['jenjang_level']; 
					}										
				}

	        	$rows = array();
	        	$rows[] = $row['seq'];
	        	$rows[] = $row['subject_name'];
	        	$rows[] = implode("<br><br> ",$jenjangname);
	        	$rows[] = implode("<br><br> ",$jenjanglevel);
	        	unset($jenjangname);
	        	unset($jenjanglevel);

	        	$rows[] = $row['status'] == "verified" ? "Verified" : ( $row['status'] == "unverified" ? "Unverified" : ( $row['status'] == "requested" ? "Waiting Verification" : "No Choose" ) );
				if($row['id_user'] != null){
					$rows[] = $row['status'] == "verified" ? '<a class="waves-effect c-gray btn btn-default btn-block" id="" href="#"><i class="zmdi zmdi-face-add"></i>Verified</a>' : ( $row['status'] == "unverified" ? '<a class="waves-effect c-white btn btn-success btn-block request_ajax" id="'.$row['subject_id'].'" href="'.base_url('/master/requestsub?id_user='.$row['id_user'].'&subject_id='.$row['subject_id']).'"><i class="zmdi zmdi-face-add"></i>Request Verification</a>' : ( $row['status'] == "requested" ? '<a class="waves-effect c-gray btn btn-default btn-block" href="#"><i class="zmdi zmdi-face-add"></i>Waiting Verification</a>' : "" ) );
					
				}else{
					$rows[] = '<a class="waves-effect bgm-green c-white btn btn-success btn-block choose_ajax" id="'.$row['subject_id'].'" href="'.base_url('/master/choosesub?id_user='.$given_id_user.'&subject_id='.$row['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('ikutisubject').'</a>';											
				}
	        	
	        	$rows[] = '<a class="waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax" id="'.$row['subject_id'].'" href="'.base_url('/master/unchoosesub?id_user='.$row['id_user'].'&subject_id='.$row['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('tidakikutisubject').'</a>';
	        	// $rows[] = $row['created_at'];
	        	/*$rows[] = "<div class='col-md-6'><a href='".base_url()."first/edit_haji/".$row['no_porsi']."' class='btns btn btn-info glyphicon glyphicon-pencil'></a></div><div class='col-md-6'>
	        	<a class='btn btn-danger' href='".base_url()."master/delete_haji/".$row['no_porsi']."' onclick='return delconf();'><i class='glyphicon glyphicon-trash'></i></a></div>";*/
	        	$option['data'][] = $rows;
	        }
	        $option['callback'] = $dt;
        // eksekusi json
	        echo json_encode($option);
	        return null;

	    }
	}

	public function listProgramChannelID($channel_id='')
	{
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
			)
		{
			$dt = $_POST;
			
			$dt['col-display'] = array(
				'CAST(@rownum as UNSIGNED)',
				'ms.subject_name',
				// 'ms.jenjang_id',
				// 'ms.jenjang_level',
				// "ms.status"
				);
			$field_sorter = array(
				'CAST(@rownum as UNSIGNED)',
				'ms.subject_name',
				// 'ms.jenjang_id',
				// 'ms.jenjang_level',
				// "ms.status"
				);

			$sql  = "SELECT @rownum:=@rownum+1 seq, ms.*, tb.uid, tb.id_user, tb.tutor_id, tb.status FROM master_subject ms LEFT JOIN tbl_booking tb ON ms.subject_id=tb.subject_id and tb.id_user='$given_id_user' , (SELECT @rownum:=0) r  ";
			$sql1 = $sql;
			$data = $this->db->query($sql);
			$rowCount = $data->num_rows();
			$data->free_result();
        // pengkondisian aksi seperti next, search dan limit
			$columnd = $dt['col-display'];
			$count_c = count($columnd);
        // search
			$search = $dt['search']['value'];
	        /**
	         * Search Global
	         * pencarian global pada pojok kanan atas
	         */
	        $where = '';
	        if ($search != '') {   
	        	for ($i=0; $i < $count_c ; $i++) {
	        		$where .= $columnd[$i] .' LIKE "%'. $search .'%"';

	        		if ($i < $count_c - 1) {
	        			$where .= ' OR ';
	        		}
	        	}
	        }
	        if($where != ''){
	        	$where = '( '.$where.' )';
	        }
	        
	        /**
	         * Search Individual Kolom
	         * pencarian dibawah kolom
	         */
	        /*for ($i=0; $i < $count_c; $i++) { 
	        	$searchCol = $dt['columns'][$i]['search']['value'];
	        	if ($searchCol != '') {
	        		if($where != ''){
	        			$where .= ' AND '. $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';	
	        		}else{
	        			$where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
	        		}
	        		
	        		// break;
	        	}
	        }*/
	        /**
	         * pengecekan Form pencarian
	         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
	         */
	        if ($where != '') {
	        	$sql .= " WHERE " . $where; // . ($profile_where != "" ? " AND (".$profile_where.")" : "")
	        	/*if($perso_status != ''){
	        		$sql .= " AND ".$perso_status;
	        	} */

	        }else{
	        	// $sql .= " WHERE ". $perso_status . ($profile_where != "" ? " AND (".$profile_where.")" : "");
	        }
	        
        // sorting
	        $sql .= " ORDER BY {$field_sorter[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
	        
	        // $sql = $this->db->escape_str($sql);
	        $list = $this->db->query($sql);
	        $rowCount2 = $list->num_rows();
	        $list->free_result();
        // limit
	        $start  = $dt['start'];
	        $length = $dt['length'];
	        if($length == -1){
	        	$length = $rowCount;
	        }
	        $sql .= " LIMIT {$start}, {$length}";
	        $list = $this->db->query($sql);
	        /**
	         * convert to json
	         */
	        $option['draw']            = $dt['draw'];
	        $option['recordsTotal']    = $rowCount;
        // if($sql != $sql1){
	        $option['recordsFiltered'] = $rowCount2;
        // }else{
        	// $option['recordsFiltered'] = $rowCount;
        // }
	        
	        $option['data'] = array();
	        foreach ($list->result_array() as $row) {

	        	$jenjanglevel = array();
				$jenjangid = json_decode($row['jenjang_id'], TRUE);
				if ($jenjangid != NULL) {
					if(is_array($jenjangid)){
						foreach ($jenjangid as $key => $value) {									
							$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();	
							$jenjangname[] = $selectnew['jenjang_name']; 
							// $jenjanglevel[] = $selectnew['jenjang_level'];
							if ($selectnew['jenjang_level']=="0") {
								$jenjanglevel[] = '-';
							}
							else{
								$jenjanglevel[] = $selectnew['jenjang_level'];	
							}
						}	
					}else{
						$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();	
						$jenjangname[] = $selectnew['jenjang_name']; 
						if ($selectnew['jenjang_level']=="0") {
							$jenjanglevel[] = '-';
						}
						else{
							$jenjanglevel[] = $selectnew['jenjang_level'];	
						}
						// $jenjanglevel[] = $selectnew['jenjang_level']; 
					}										
				}

	        	$rows = array();
	        	$rows[] = $row['seq'];
	        	$rows[] = $row['subject_name'];
	        	$rows[] = implode("<br><br> ",$jenjangname);
	        	$rows[] = implode("<br><br> ",$jenjanglevel);
	        	unset($jenjangname);
	        	unset($jenjanglevel);

	        	$rows[] = $row['status'] == "verified" ? "Verified" : ( $row['status'] == "unverified" ? "Unverified" : ( $row['status'] == "requested" ? "Waiting Verification" : "No Choose" ) );
				if($row['id_user'] != null){
					$rows[] = $row['status'] == "verified" ? '<a class="waves-effect c-gray btn btn-default btn-block" id="" href="#"><i class="zmdi zmdi-face-add"></i>Verified</a>' : ( $row['status'] == "unverified" ? '<a class="waves-effect c-white btn btn-success btn-block request_ajax" id="'.$row['subject_id'].'" href="'.base_url('/master/requestsub?id_user='.$row['id_user'].'&subject_id='.$row['subject_id']).'"><i class="zmdi zmdi-face-add"></i>Request Verification</a>' : ( $row['status'] == "requested" ? '<a class="waves-effect c-gray btn btn-default btn-block" href="#"><i class="zmdi zmdi-face-add"></i>Waiting Verification</a>' : "" ) );
					
				}else{
					$rows[] = '<a class="waves-effect bgm-green c-white btn btn-success btn-block choose_ajax" id="'.$row['subject_id'].'" href="'.base_url('/master/choosesub?id_user='.$given_id_user.'&subject_id='.$row['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('ikutisubject').'</a>';											
				}
	        	
	        	$rows[] = '<a class="waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax" id="'.$row['subject_id'].'" href="'.base_url('/master/unchoosesub?id_user='.$row['id_user'].'&subject_id='.$row['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('tidakikutisubject').'</a>';
	        	// $rows[] = $row['created_at'];
	        	/*$rows[] = "<div class='col-md-6'><a href='".base_url()."first/edit_haji/".$row['no_porsi']."' class='btns btn btn-info glyphicon glyphicon-pencil'></a></div><div class='col-md-6'>
	        	<a class='btn btn-danger' href='".base_url()."master/delete_haji/".$row['no_porsi']."' onclick='return delconf();'><i class='glyphicon glyphicon-trash'></i></a></div>";*/
	        	$option['data'][] = $rows;
	        }
	        $option['callback'] = $dt;
        // eksekusi json
	        echo json_encode($option);
	        return null;

	    }
	}
}