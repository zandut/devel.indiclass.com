<?php
	
	//require file
	//require_once('../../Facebook/FacebookSession.php');
	require_once('../../Facebook/FacebookRequest.php');
	require_once('../../Facebook/FacebookResponse.php');
	require_once('../../Facebook/FacebookSDKException.php');
	require_once('../../Facebook/FacebookRequestException.php');
	require_once('../../Facebook/FacebookRedircetLoginHelper.php');
	require_once('../../Facebook/FacebookAuthorizationException.php');
	require_once('../../Facebook/GraphObject.php');
	require_once('../../Facebook/GraphUser.php');
	require_once('../../Facebook/GraphSessionInfo.php');
	require_once('../../Facebook/Entities/AccesToken.php');
	require_once('../../Facebook/HttpClients/FacebookCurl.php.php');
	require_once('../../Facebook/HttpClients/FacebookHttpable.php');
	require_once('../../Facebook/HttpClients/FacebookCurlHttpClient.php.php');


	//load library class
	use Facebook\FacebookSession;
	use Facebook\FacebookRedircetLoginHelper;
	use Facebook\FacebookRequest;
	use Facebook\FacebookResponse;
	use Facebook\FacebookSDKException;
	use Facebook\FacebookRequestException;
	use Facebook\FacebookAuthorizationException;
	use Facebook\GraphObject;
	use Facebook\GraphUser;
	use Facebook\GraphSessionInfo;
	use Facebook\FacebookHttpable;
	use Facebook\FacebookCurlHttpClient;
	use Facebook\FacebookCurl;

	class Fblogin extends CI_Controller{
		public function __construct(){
			parent::__construct();
		}

		public function index(){
			// cek jika masih login
			if ($this->session->userdata('loggedin')==FALSE) {
				redirect('fblogin/login');

			}

			$this->load->view('fblogin/dashboard', $this->data);
		}

		public function login(){
			//1. app id dan secret key dari facebook
			$app_id = '755278701291006';
			$app_secret = 'e5328ca10cc303ac30853e36ac50eb39';
			$redirect_url='https://classmiles.com/fblogin/login';

			//2. inisialisasi, buat helper object dan dapatkan session
			FacebookSession::setDefaultApplication($app_id, $app_secret);
			$helper = new FacebookRedircetLoginHelper($redirect_url);
			$sess = $helper->getSessionFromRedirect();

			//3. cek validasi akun pengguna
			if ($this->session->userdata('fb_token')) {
				$sess = new FacebookSession($this->session->userdata('fb_token'));

				try {
					$sess->Validate($id, $secret);
				} catch (FacebookAuthorizationException $e) {
					print_r($e);
				}
			}

			$this->data['loggedin'] = FALSE;
			//login url
			$this->data['login_url'] = $helper->getLoginUrl(array('email'));

			//4. jika fb session ada maka buat session pengguna
			if (isset($sess)) {
				$this->session->set_userdata('fb_token', $sess->getToken());
				$request = new FacebookRequest($sess, 'GET', '/me');
				$respponse = $request->execute();
				$graph = $respponse->getGraphObject(GraphUser::classname());
				$sess_data = array(
					'id' => $graph->getId(),
					'name' => $graph->getName(),
					'email' => $graph->getProperty('email'),
					'image' => 'https://graph.facebook.com/'.$graph->getId().'/picture?width=50',
					'loggedin' => TRUE
					);
				$this->session->set_userdata($sess_data);

				redirect('fblogin');
			}
			$this->load->view('fblogin/login', $this->data);
		}

		public function logout(){
			$sess_data = array(
					'id' => NULL,
					'name' => NULL,
					'email' => NULL,
					'image' => NULL,
					'loggedin' => FALSE
					);
			$this->session->unset_userdata($sess_data);

			delete_cookie('ci_session');
			redirect('fblogin/login');
		}
	}

?>