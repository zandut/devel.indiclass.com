<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// $this->load->library('fpdf');
        $this->load->helper(array('Form', 'Cookie', 'String'));
		// Check for any mobile device.
	}

	public function index()
	{
		if($this->session_check() == 1){
			$data['sideactive'] = 'classes_index';
			$this->load->view('inc/header_student');
			$this->load->view('student/index',$data);
			$this->load->view('inc/footer_student');	
		}
		else{
			redirect('/');
		}		
	}

	public function KelasSaya()
	{
		if($this->session_check() == 1){
			$data['sideactive'] = 'classes_index';
			$this->load->view('inc/header_student');
			$this->load->view('student/index',$data);
			$this->load->view('inc/footer_student');	
		}
		else{
			redirect('/');
		}		
	}

	public function Pengajar()
	{
		if($this->session_check() == 1){
			$data['sideactive'] = 'classes_pengajar';
			$this->load->view('inc/header_student');
			$this->load->view('student/pengajar_class',$data);
			$this->load->view('inc/footer_student');
		}
		else{
			redirect('/');
		}
	}

	public function Permintaan()
	{
		if($this->session_check() == 1){
			$data['sideactive'] = 'classes_request';
			$this->load->view('inc/header_student');
			$this->load->view('student/request_class',$data);
			$this->load->view('inc/footer_student');
		}
		else{
			redirect('/');
		}
	}

	public function DataDiri()
	{
		if($this->session_check() == 1){
			$data['sideactive'] = 'classes_datadiri';
			$this->load->view('inc/header_student');
			$this->load->view('student/datadiri_class',$data);
			$this->load->view('inc/footer_student');
		}
		else{
			redirect('/');
		}
	}

	public function session_check()
	{		
		if($this->session->has_userdata('id_user')){		
			return 1;
		}else{
			return 0;
		}
	}

	function Logout(){
		$this->session->sess_destroy();
		redirect('/');
	}

}