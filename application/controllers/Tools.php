<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// require 'aset/BCAParser.php';
// require 'vendor/autoload.php';
// use PHPHtmlParser\Dom;

class Tools extends CI_Controller {
	
	public $Parser;
	public function __construct()
	{
		parent::__construct();
		$params = array('server_key' => 'VT-server-31TBIo8Zf4Pa-I7j4pKj8CID', 'production' => true);
		$this->load->library('veritrans');
		$this->veritrans->config($params);
		$this->load->helper('url');
		$this->load->database('default');
	 	$this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
		// Check for any mobile device.
		if ($detect->isMobile()){
			define('MOBILE', 'mobile/');
		}else{
			define('MOBILE', '');
		}
	}
	public function index()
	{
		echo "";
	}
	public function tps_me_clicker($value='')
	{
		$link = $this->input->get('link');
		$link = base64_decode($link);
		$data['link'] = $link;
		$this->load->view('tps_me_clicker', $data);
	}
	public function begin_()
	{
		
		$now = date('Y-m-d');
		$data = $this->db->query("SELECT class_id, start_time FROM tbl_class WHERE break_state=1 && break_pos=1 && start_time like '{$now}%'")->result_array();
		if(!empty($data)){
			echo "BEGIN :\n";
			echo "There is ".count($data)." class that start today\n";
		}
		foreach($data as $key => $value) {
			$dtnow = strtotime(date('Y-m-d H:i:s'));
			$dtstart = strtotime($value['start_time']);
			$class_id = $value['class_id'];
			if($dtnow-$dtstart >=56){
				$this->db->simple_query("UPDATE tbl_class SET break_state=0, break_pos=2 WHERE class_id='{$class_id}'");
				echo "updating class {$class_id} to break_pos 2\n";
			}
		}
	}
	public function end_()
	{
		
		$nowminone = date('Y-m-d H:i:s',mktime(date('H'), date('i')+1, date('s'), date('m'), date('d'), date('Y')));
		$now = date('Y-m-d H:i:s');

		$data = $this->db->query("SELECT class_id, finish_time, break_state FROM tbl_class WHERE break_pos=2 && finish_time >= '{$now}' && finish_time <= '{$nowminone}'")->result_array();
		if(!empty($data)){
			echo "END :\n";
			echo "There is ".count($data)." class that will be finished within a minute\n";
		}
		foreach($data as $key => $value) {
			if($value['break_state'] == 1){
				// parse to REST stop_break;
			}
			$class_id = $value['class_id'];
			$this->db->simple_query("UPDATE tbl_class SET break_state=1, break_pos=3 WHERE class_id='{$class_id}'");
			echo "updating class {$class_id} to break_pos 3\n";
		}
	}
	public function destroy_class_()
	{
		// PREPARE FOR SEND REMOVE_TOKEN TO JANUS

		$send = array();
		$send['janus'] = "remove_token";
		$send['transaction'] = $this->Rumus->RandomString();

		$send['admin_secret'] = "admin556677";

		$dtnow = date('Y-m-d H:i:s');

		$data = $this->db->query("SELECT class_id FROM tbl_class WHERE finish_time < '{$dtnow}' && break_pos != 0")->result_array();
		foreach($data as $key => $value){
			echo "DESTROY CLASS :\n";
			$class_id = $value['class_id'];
			echo "Class {$class_id} has ended\n";
			$this->db->simple_query("UPDATE tbl_class SET break_pos=0, break_state=0 WHERE class_id='{$class_id}'");
			$alltoken = $this->db->query("SELECT tps.session_id, lt.token FROM tbl_participant_session as tps INNER JOIN log_token as lt ON tps.session_id=lt.session_id WHERE tps.class_id='{$class_id}'")->result_array();
			foreach($alltoken as $k => $v) {
				$token = $v['token'];
				$session_id = $v['session_id'];
				echo "Token {$token} has destroyed\n";
				$send['token'] = $v['token'];
				$ari = json_encode($send);
					// TO JANUS

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://c2.classmiles.com:7889/admin");
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch,CURLOPT_POST,1);
				curl_setopt($ch,CURLOPT_POSTFIELDS,$ari);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch,CURLOPT_TIMEOUT_MS,4000); 

				$curl_rets = curl_exec($ch);

				$this->db->simple_query("UPDATE log_token SET destroyed_at=now() WHERE token='{$token}'");
				$this->db->simple_query("UPDATE log_rhand SET status = -1 WHERE session_id='{$session_id}' AND status=0;");
			}
		}
	}
	public function request_rejector()
	{
		$now = date('Y-m-d H:i:s', time() - 3600);
		echo $now;
		$data = $this->db->query("SELECT * FROM tbl_request WHERE approve=0 && datetime<='$now'")->result_array();
		echo 'private : <pre>';
		print_r($data);
		echo '</pre>';
		foreach ($data as $key => $value) {
			$rid=$value['request_id'];
			$ids = $value['id_user_requester'];
			$this->db->simple_query("UPDATE tbl_request SET approve=-2 WHERE request_id=$rid");

			$this->Process_model->reject_request($rid, 'private', 'unapproved');

			////------------ BUAT NGIRIM EVENT FIREBASE --------------//
            $json_notif = "";
            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$ids}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "31","title" => "Classmiles", "text" => "")));
            }
            if($json_notif != ""){
                $headers = array(
                    'Authorization: key=' . FIREBASE_API_KEY,
                    'Content-Type: application/json'
                    );
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
            }
            ////------------ END -------------------------------------//
		}

		$data = $this->db->query("SELECT * FROM tbl_request_grup WHERE approve=0 && datetime<='$now'")->result_array();
		echo 'group : <pre>';
		print_r($data);
		echo '</pre>';
		foreach ($data as $key => $value) {
			$rid=$value['request_id'];
			$ids = $value['id_user_requester'];
			$this->db->simple_query("UPDATE tbl_request_grup SET approve=-2 WHERE request_id=$rid");
			$this->Process_model->reject_request($rid, 'group', 'unapproved');
			// due_payment

			////------------ BUAT NGIRIM EVENT FIREBASE --------------//
            $json_notif = "";
            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$ids}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "31","title" => "Classmiles", "text" => "")));
            }
            if($json_notif != ""){
                $headers = array(
                    'Authorization: key=' . FIREBASE_API_KEY,
                    'Content-Type: application/json'
                    );
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
            }
            ////------------ END -------------------------------------//
		}

		// UDAH APPROVE TUTOR TAPI BLM CHECKOUT
		
		$data = $this->db->query("SELECT * FROM tbl_request WHERE approve=2 && datetime<='$now'")->result_array();
		echo 'private : <pre>';
		print_r($data);
		echo '</pre>';
		foreach ($data as $key => $value) {
			$rid=$value['request_id'];
			$this->db->simple_query("UPDATE tbl_request SET approve=-3 WHERE request_id=$rid");
			$this->Process_model->reject_request_unpaid($rid, 'private');
		}
		$data = $this->db->query("SELECT * FROM tbl_request_grup WHERE approve=2 && datetime<='$now'")->result_array();
		echo 'group : <pre>';
		print_r($data);
		echo '</pre>';
		foreach ($data as $key => $value) {
			$rid=$value['request_id'];
			$this->db->simple_query("UPDATE tbl_request_grup SET approve=-3 WHERE request_id=$rid");
			$this->Process_model->reject_request_unpaid($rid, 'group');
		}

		// REJECT BY STILL 2 MULTICAST UNTIL TIME REQUESTED PASSED
		$data = $this->db->query("SELECT * FROM tbl_request_multicast WHERE status=2 && date_expired<='$now'")->result_array();
		echo 'multicast : <pre>';
		print_r($data);
		echo '</pre>';
		foreach ($data as $key => $value) {
			$rid=$value['request_id'];
			$this->db->simple_query("UPDATE tbl_request_multicast SET status=-3 WHERE request_id=$rid");
			$this->Process_model->reject_request_unpaid($rid, 'multicast');
		}

		// REJECT BY STILL 2 PROGRAM UNTIL TIME REQUESTED PASSED
		$data = $this->db->query("SELECT * FROM tbl_request_program WHERE status=2 && created_at<='$now'")->result_array();
		echo 'program : <pre>';
		print_r($data);
		echo '</pre>';
		foreach ($data as $key => $value) {
			$rid=$value['request_id'];
			$this->db->simple_query("UPDATE tbl_request_program SET status=-3 WHERE request_id=$rid");
			$this->Process_model->reject_request_unpaid($rid, 'program');
		}

		// REJECT BY NOT PAYING IN DUE
		$now = date('Y-m-d H:i:s');
		$payment_duedate = $this->db->query("SELECT * FROM tbl_order WHERE ( order_status=0 OR order_status=2 OR order_status=-3) AND payment_due<='$now' ")->result_array();
		foreach ($payment_duedate as $k => $v) {
			$order_id = $v['order_id'];
			$data = $this->db->query("SELECT tod.* FROM tbl_order_detail as tod INNER JOIN tbl_order as tord ON tod.order_id=tord.order_id WHERE tord.order_id=$order_id")->result_array();
			foreach ($data as $key => $value) {
				$type = $value['type'];
				$rid = $value['product_id'];
				switch ($type) {
					case 'private':
						$this->db->simple_query("UPDATE tbl_request SET approve=-3 WHERE request_id=$rid");
						break;
					case 'group':
						$this->db->simple_query("UPDATE tbl_request_grup SET approve=-3 WHERE request_id=$rid");
						break;
					case 'multicast':
						$this->db->simple_query("UPDATE tbl_request_multicast SET status=-3 WHERE request_id=$rid");
						break;
					case 'program':
						$this->db->simple_query("UPDATE tbl_request_program SET status=-3 WHERE request_id=$rid");
						break;
					default:
						# code...
						break;
				}
			}
			$this->db->simple_query("UPDATE tbl_order SET order_status=-2 WHERE order_id='$order_id'");
			$this->Process_model->reject_order_unpaid($order_id);
		}
	}
	public function channel_ct_program_expired($value='')
	{
		$now = time();
		$data = $this->db->query("SELECT * FROM master_channel_program WHERE status=0;")->result_array();
		if(!empty($data)){
			foreach ($data as $key => $value) {
				$program_id = $value['program_id'];
				$ars = json_decode($value['schedule'],TRUE);
				if(is_array($ars)){
					foreach ($ars as $k => $v) {
						$status_array = intval($v['status']);
						$expired_array = strtotime($v['expired']);
						if($status_array == 2 && $expired_array <= $now){
							$ars[$k]['status'] = 0;
						}
					}
				}
				$new_schedule = json_encode($ars);
				$this->db->simple_query("UPDATE master_channel_program SET schedule='$new_schedule' WHERE program_id='$program_id'");
			}
		}
	}
	public function morning_checker_notification()
	{
		$fields = array();
		$today = date('Y-m-d');

		// SELECT ALL START TODAY
		$users_arr = array();
		$all_class = $this->db->query("SELECT tc.*, tu.user_name, ms.subject_name FROM tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN master_subject as ms ON tc.subject_id=ms.subject_id where start_time like '{$today}%' ORDER BY start_time ASC")->result_array();
		foreach ($all_class as $i => $kelas) {
			// TAKE ALL PARTICIPANT IN THAT CLASS
			$visible = json_decode($kelas['participant'],true);
			$tutor_id = $kelas['tutor_id'];
			$subject_id = $kelas['subject_id'];
			$slap = array();
			// $slap['tutor_name'] = $kelas['user_name'];
			$slap['subject_name'] = $kelas['subject_name'];
			$slap['time'] = substr($kelas['start_time'], 11,8);

			if($visible['visible'] == "followers"){
				$the_user = $this->db->query("SELECT * FROM tbl_booking where tutor_id='{$tutor_id}' AND subject_id='{$subject_id}'")->result_array();
				foreach ($the_user as $k => $value) {
					$id_user = $value['id_user'];
					if(isset($users_arr[$id_user])){
						array_push($users_arr[$id_user], $slap);
					}else{
						$users_arr[$id_user][0] = $slap;
					}
				}
			}else if($visible['visible'] == "private"){
				foreach ($visible['participant'] as $k => $value) {
					$id_user = $value['id_user'];
					if(isset($users_arr[$id_user])){
						array_push($users_arr[$id_user], $slap);
					}else{
						$users_arr[$id_user][0] = $slap;
					}
				}
			}
		}
		foreach ($users_arr as $key => $value) {
			$tot_class = count($users_arr[$key]);

			if($tot_class == 1){
				$fields['notification'] = array("body" => $users_arr[$key][0]['subject_name']."    ".$users_arr[$key][0]['time'], "title" => "Classmiles", "sound" => "Reminder.getRingtone()");
			}else{
				$fields['data']['data'] = array("image" => "", "title" => "Classmiles", "is_background" => false, "message" => $tot_class." classes","payload"=> $users_arr[$key], "timestamp" => date('Y-m-d H:i:s'));
			}
			$this->Rumus->create_notif('Kamu memiliki '.$tot_class.' kelas untuk hari ini',serialize($fields),'single',$key,BASE_URL);
		}
	}
	
	public function short_checker_notification()
	{	
		$fields = array();
		$today = date('Y-m-d');
		$now = date('Y-m-d H:i:s');
		$nowKurangDuaJam = DateTime::createFromFormat('Y-m-d H:i:s',$now);
		$nowKurangDuaJam->modify('-2 hour');
		$nowKurangDuaJam = $nowKurangDuaJam->format('Y-m-d H:i:s');
		$list_student_send = array();

		$todayinMe = DateTime::createFromFormat('Y-m-d H:i:s',$today." 00:00:00");
		$todayinMe->modify("-1 day");
		$todayinMe = $todayinMe->format('Y-m-d H:i:s');


		//----- NOTIF MISSED CLASS -------//
		// SELECT ALL PASSED AND notif never made
		echo "<h3>missed class</h3><br>";
		$passed_class = $this->db->query("SELECT class_id, subject_id, start_time, finish_time, tutor_id FROM tbl_class as tc where finish_time < '{$now}' && finish_time > '{$nowKurangDuaJam}' && tc.class_id NOT IN (SELECT class_id FROM log_class_notif WHERE type='missed' && class_id=tc.class_id) ORDER BY finish_time ASC")->result_array();
		foreach ($passed_class as $key => $value) {
			$class_id = $value['class_id'];
			echo "processing class ".$class_id."<br>";
			$list_participant = $this->list_participant($class_id);
			echo "list participant :<br>";
			print_r('<pre>');
			print_r($list_participant);
			print_r('</pre>');
			foreach ($list_participant as $id_user) {
				$this->db->simple_query("INSERT INTO log_class_notif(class_id, id_user, type, read_status) VALUES('{$class_id}','{$id_user}','missed',1)");
				$this->db->simple_query("DELETE FROM log_class_notif WHERE type!='missed' AND class_id='{$class_id}' AND id_user='{$id_user}' ");
			}
		}
		//----- end NOTIF MISSED CLASS -------//
		//----- NOTIF ON-GOING CLASS-------//
		echo "<h3>on-going class</h3><br>";
		$going_class = $this->db->query("SELECT class_id, subject_id, start_time, finish_time, tutor_id FROM tbl_class as tc where start_time <= '{$now}' && finish_time > '{$now}' && tc.class_id NOT IN (SELECT class_id FROM log_class_notif WHERE type='on-going' && class_id=tc.class_id) ORDER BY finish_time ASC")->result_array();
		foreach ($going_class as $key => $value) {
			$class_id = $value['class_id'];
			echo "processing class ".$class_id."<br>";
			$list_participant = $this->list_participant($class_id);
			echo "list participant :<br>";
			print_r('<pre>');
			print_r($list_participant);
			print_r('</pre>');
			foreach ($list_participant as $id_user) {
				if(!in_array($id_user, $list_student_send)){
					$list_student_send[] = $id_user;
				}
				$notif = $this->db->query("SELECT * FROM log_class_notif WHERE type='on-going' AND class_id='{$class_id}' AND id_user='{$id_user}'")->row_array();
				if(empty($notif)){
					//CREATE NOTIF
					$this->db->simple_query("INSERT INTO log_class_notif(class_id, id_user, type) VALUES('{$class_id}','{$id_user}','on-going')");
					$this->db->simple_query("DELETE FROM log_class_notif WHERE type!='on-going' AND class_id='{$class_id}' AND id_user='{$id_user}' ");
				}
			}
		}
		//----- end NOTIF ON-GOING CLASS -------//
		//----- NOTIF will start CLASS-------//
		/*echo "<h3>will start class</h3><br>";
		$will_start_glass = $this->db->query("SELECT class_id, subject_id, start_time, finish_time, tutor_id FROM tbl_class as tc where LEFT(start_time,10)='{$today}' && TIME_TO_SEC(TIMEDIFF(start_time, '$now')) > 70 && TIME_TO_SEC(TIMEDIFF(start_time, '$now')) <= 900 && tc.class_id NOT IN (SELECT class_id FROM log_class_notif WHERE type='will start' && class_id=tc.class_id) ORDER BY finish_time ASC")->result_array();
		foreach ($will_start_glass as $key => $value) {
			$class_id = $value['class_id'];
			echo "processing class ".$class_id."<br>";
			$list_participant = $this->list_participant($class_id);
			echo "list participant :<br>";
			print_r('<pre>');
			print_r($list_participant);
			print_r('</pre>');
			foreach ($list_participant as $id_user) {
				if(!in_array($id_user, $list_student_send)){
					$list_student_send[] = $id_user;
				}
				//CREATE NOTIF
				$this->db->simple_query("INSERT INTO log_class_notif(class_id, id_user, type) VALUES('{$class_id}','{$id_user}','will start')");
			}
		}
		foreach ($list_student_send as $k) {
			$will_start_second_class = $this->db->query("SELECT tc.class_id, tc.subject_id, tc.start_time, tc.finish_time FROM log_class_notif as lcn RIGHT JOIN tbl_class as tc ON lcn.class_id=tc.class_id where lcn.id_user='{$k}' && LEFT(tc.start_time,10)='{$today}' && TIME_TO_SEC(TIMEDIFF(tc.start_time, '$now')) > 70 && TIME_TO_SEC(TIMEDIFF(tc.start_time, '$now')) <= 900 && lcn.type='will start' && lcn.read_status=0 ORDER BY finish_time ASC")->result_array();
			echo "processing user ".$k."<br>";
			foreach ($will_start_second_class as $key => $value) {
				//CREATE NOTIF
				$class_id = $value['class_id'];
				$this->db->simple_query("UPDATE log_class_notif SET read_status=1 WHERE type='will start' AND class_id='{$class_id}' AND id_user='{$k}' ");
				$this->db->simple_query("INSERT INTO log_class_notif(class_id, id_user, type) VALUES('{$class_id}','{$k}','will start')");
			}
		}*/
		//----- end NOTIF will start CLASS -------//


		// SEND ALL NOTIF
		

		echo "list all users to_send<br>";
		print_r('<pre>');
		print_r($list_student_send);
		print_r('</pre>');
		foreach ($list_student_send as $key) {
			$json_notif = "";
			$fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$key}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
			if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
				$json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "202","title" => "Classmiles", "text" => "", "payload" => "")));
				$notifications =$this->db->query("SELECT lcn.cnotif_id as notif_id, tc.name, lcn.class_id, lcn.type, tc.subject_id, tc.start_time, TIME_TO_SEC(TIMEDIFF(RIGHT(tc.start_time,8), RIGHT(lcn.created_at,8))) as minutes_left FROM log_class_notif as lcn INNER JOIN tbl_class as tc ON lcn.class_id=tc.class_id WHERE lcn.id_user='{$key}' && lcn.read_status=0 ORDER BY lcn.created_at DESC")->result_array();

				foreach ($notifications as $k => $v) {
					if($v['type'] == "will start"){
						$json_notif['data']['data']['payload'][] = array("notif_id" => $v['notif_id'], "subject_id" => $v['name'], "time" => round($v['minutes_left']/60));
					}else if($v['type'] == "on-going"){
						$json_notif['data']['data']['payload'][] = array("notif_id" => $v['notif_id'], "subject_id" => $v['name'], "time" => "0");
					}else if($v['type'] == "missed"){
						$json_notif['data']['data']['payload'][] = array("notif_id" => $v['notif_id'], "subject_id" => $v['name'], "time" => "-1");
					}
				}
			}
			if($json_notif != ""){
				$headers = array(
					'Authorization: key=' . FIREBASE_API_KEY,
					'Content-Type: application/json'
					);
	        	// Open connection
				$ch = curl_init();

	        	// Set the url, number of POST vars, POST data
				curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

	        	// Execute post
				$result = curl_exec($ch);
				curl_close($ch);
				echo $result;
			}
			echo '<br>';
		}

	}
	public function get_c($value='')
	{
		$c = htmlentities($value);

		// $c = str_pad($c, 10, "0");
		$c = openssl_encrypt ( $c , "AES-128-CBC" , 'Jf13Wx1qc1hXrbqu7rLITpwvPbNb1jOa' , 0 , "0000000000000000");
		echo bin2hex(base64_decode($c));
	}
	public function get_dc($value='')
	{
		$c = htmlentities($value);
		$c = base64_encode(pack("H*", $c));

		$c = openssl_decrypt ( $c , "AES-128-CBC" , 'Jf13Wx1qc1hXrbqu7rLITpwvPbNb1jOa' , 0 , "0000000000000000");
		echo $c;
	}
	public function list_participant($class_id='')
	{
		$list_participant = array();
		$class = $this->db->query("SELECT participant, tutor_id, subject_id  FROM tbl_class WHERE class_id='$class_id'")->row_array();
		$tutor_id = $class['tutor_id'];
		$subject_id = $class['subject_id'];
		$participant = json_decode($class['participant'],true);
		if(isset($participant['participant'])) {
			foreach ($participant['participant'] as $key => $value) {
				$list_participant[] = $value['id_user'];
			}
		}
		/*if($participant['visible'] == "followers"){
			$the_user = $this->db->query("SELECT id_user FROM tbl_booking where tutor_id='{$tutor_id}' AND subject_id='{$subject_id}'")->result_array();
			foreach ($the_user as $k => $value) {
				$list_participant[] = $value['id_user'];
			}
		}else if($participant['visible'] == "private"){
			foreach ($participant['participant'] as $k => $value) {
				$list_participant[] = $value['id_user'];
			}
		}else if($participant['visible'] == "all"){
			$the_user = $this->db->query("SELECT id_user FROM tbl_booking where tutor_id!='0'")->result_array();
			foreach ($the_user as $k => $value) {
				$list_participant[] = $value['id_user'];
			}
		}*/
		return $list_participant;

	}
	public function fulfill_subd($value='')
	{
		$now = 7-(date('N')-1);
		$nextMon = date('Y-m-d', strtotime('+'.$now.' day', strtotime(date('Y-m-d'))));
		$nextMon = DateTime::createFromFormat('Y-m-d',$nextMon);
		$start_date = date('Y-m-d', strtotime($nextMon->format('Y-m-d')));

		// SELECT tb.id_user, tb.subject_id, ms.subject_name, lsd.subdiscussion FROM tbl_booking as tb INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id) ON tb.subject_id=ms.subject_id LEFT JOIN log_subdiscussion as lsd ON tb.subject_id=lsd.subject_id AND tb.id_user=lsd.tutor_id WHERE tb.tutor_id='0' AND lsd.fdate_week='{$start_date}'
		$data = $this->db->query("SELECT tb.id_user, tb.subject_id, ms.subject_name FROM tbl_booking as tb INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id) ON tb.subject_id=ms.subject_id WHERE tb.tutor_id='0'")->result_array();
		echo $start_date;
		$result = array();
		foreach ($data as $key => $value) {
			$tutor_id = $value['id_user'];
			$subject_id = $value['subject_id'];
			$d = $this->db->query("SELECT * FROM log_subdiscussion WHERE fdate_week='{$start_date}' AND subject_id='{$subject_id}' AND tutor_id='{$tutor_id}'")->row_array();
			if(empty($d) || $d['subdiscussion'] == '' || $d['subdiscussion'] == NULL){
				if(!isset($result[$tutor_id])){
					$result[$tutor_id] = "1";
				}
			}
		}
		foreach ($result as $key => $value) {
			$fields['notification'] = array("body" => "Terdapat Sub-Bahasa yang belum kamu isi", "title" => "Classmiles", "sound" => "Reminder.getRingtone()");
			$this->Rumus->create_notif("Terdapat Sub-Bahasa yang belum kamu isi",serialize($fields),'single',$key,BASE_URL);
		}
		return $result;
	}
	public function schedule_engine_($value='')
	{

		// TAKE ALL TIME FROM LOG_AVTIME WHERE SETTED = 0
		// INTO TBL_AVTIME AND UPDATE SETTED = 1
		/*STEPP :
		1. CALL PROCEDURE LogToTblAvtime
		2. GET ALL TUTOR WITHIN ITS SUBJECT_ID BOOKED FROM tbl_booking
		3. CHECK EVERY SUBJECT WHETHER IT S ALREADY EXIST IN tbl_class OR NOT ( TAKE 2 WEEKS LOOPING)
		4. IF NOT EXIST , DO SOME :
		 A. CHECK TUTOR AVAILABILITY TIME FROM TBL_AVTIME EACH DAY FOR 7 DAYS
		 B. IF EXIST , THEN :
		 	- TAKE SUBJECT SUBDISCUSSION PREPARED FROM LOG_SUBDISCUSSION
		 	- USE AS SUBDISCUSSION FOR THAT SUBJECT IN THE FOLLOWING CLASS
		 	- FILL IN tbl_class
		 	- DIVIDE EXISTING AVAILABILITY TIME INTO NEW AVAILABILITY TIME AFTER USAGE OF NEW CLASS ( LITERALLY 1 HOUR )*/
		 	$this->db->query("CALL LogToTblAvtime(@res);");
		 	$res = $this->db->query("SELECT @res")->row_array();
		 	if(!empty($res)){
			// PROCEDURE CALLED SUCCESSFULLY
		 		for ($loops=0; $loops <= 7; $loops+=7) {
		 			echo "ini loop : ".$loops."<br>";
		 			$now = $loops-(date('N')-1);
		 			$nextMon = date('Y-m-d', strtotime('+'.$now.' day', strtotime(date('Y-m-d'))));
		 			$oneWeek = date('Y-m-d', strtotime('+6 day', strtotime($nextMon)));
		 			$nextMon = DateTime::createFromFormat('Y-m-d',$nextMon);
		 			$oneWeek = DateTime::createFromFormat('Y-m-d',$oneWeek);

		 			$timeArray = array();

		 			for($x=0;$x<7;$x++){
		 				$timeArray[$x]["day"] =  date('D, d M', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));
		 				$timeArray[$x]["date"] = date('Y-m-d', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));
		 			}
		 			$list_booking = $this->db->query("SELECT tb.uid, tb.id_user, tb.subject_id, ms.subject_name, ms.jenjang_id, mj.jenjang_name, mj.jenjang_level FROM `tbl_booking` as tb LEFT JOIN ( master_subject as ms LEFT JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id) ON ms.subject_id=tb.subject_id WHERE tutor_id=0")->result_array();
		 			if(!empty($list_booking)){
		 				foreach ($list_booking as $k1 => $v1) {

		 					$jenjang_id = $v1['jenjang_id'];
		 					$subject_id = $v1['subject_id'];
		 					$subject_name = $v1['subject_name'];
		 					$tutor_id = $v1['id_user'];

						// CHECK IF THAT TUTOR WITHIN SUBJECT IS EXIST IN tbl_class FOR THAT WEEK
		 					$cek_weekexist = $this->db->query("SELECT * FROM tbl_class WHERE subject_id={$subject_id} AND tutor_id={$tutor_id} AND start_time>='".$nextMon->format('Y-m-d')." 00:00:00' AND finish_time<='".$oneWeek->format('Y-m-d')." 23:59:59'")->result_array();
		 					if(!empty($cek_weekexist)){
		 						echo "pelajaran ".$subject_id." untuk tutor ".$tutor_id." udah ada mminggu ini, so skip!<br>";
		 						goto toNextPelajaran;
		 					}
		 					echo "pelajaran ".$subject_id." untuk tutor ".$tutor_id." belum ada, so do!<br>";

						// DONT EXIST , CHECK SELF AVAILABILITY TIME 
		 					for($x=0;$x<7;$x++){
		 						$self_empty = $this->db->query("SELECT * FROM tbl_avtime WHERE date='".$timeArray[$x]["date"]."' AND (stop_time-start_time)>=3600 AND tutor_id=".$v1['id_user'])->result_array();
		 						if(!empty($self_empty)){
								////////////////////////////////////////////
								// NOW THIS SHOULD RUN ALGORITHM FOR HISTORICAL LAST WEEK SCHEDULE AND STUDENT EASE
								////////////////////////////////////////////
		 							echo "my empty schedule :\n";
		 							foreach ($self_empty as $k2 => $v2) {

		 								$uid = $v2['uid'];
		 								$db_date = $v2['date'];
		 								$db_starttime = $v2['start_time'];
		 								$db_stoptime = $v2['stop_time'];

									$chosen_starttime = $db_starttime; //  NANTI NYA NGARUH KE KETENTUAN BATAS MULAI JAM PELAJARAN 
									$chosen_stoptime = $chosen_starttime+3600; // STANDAR KELAS 1 JAM DURASI

									$AsTime = strtotime($db_date." 00:00:00");
									$b1 = date('Y-m-d H:i:s',$AsTime+$chosen_starttime);
									$b2 = date('Y-m-d H:i:s',$AsTime+$chosen_stoptime);

									echo $uid." -- ".$db_date." ".$chosen_starttime." ".$chosen_stoptime."<br>";

									$fdate_week = $timeArray[0]['date']; // hari pertama di minggu ini
									$subdiscussion = "";
									$db_subd = $this->db->query("SELECT * FROM log_subdiscussion WHERE tutor_id='{$tutor_id}' AND subject_id='{$subject_id}' AND fdate_week='{$fdate_week}'")->row_array();
									if(!empty($db_subd)){
										$subdiscussion = $db_subd['subdiscussion'];
									}
									
									$participant = json_encode(array("visible" => "followers"));

									echo "this time schedule clear empty"."<br>";

									$this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,participant) VALUES('{$subject_id}','{$subject_name}','{$subdiscussion}','{$tutor_id}','{$b1}','{$b2}','{$participant}')");
									// $id_new = $this->db->query("SELECT MAX(class_id) FROM tbl_class;")->row_array();
									// mkdir("/home/", 0700);
									if($chosen_starttime-$db_starttime > 0){
										// DIVIDE AVAILABILITY TIME EARLIER
										$this->db->simple_query("INSERT INTO tbl_avtime(tutor_id,date,start_time,stop_time) VALUES('{$tutor_id}','{$db_date}','{$db_starttime}','{$chosen_starttime}')");
									}
									if($db_stoptime-$chosen_stoptime > 0){
										// DIVIDE AVAILABILITY TIME LATER
										$this->db->simple_query("INSERT INTO tbl_avtime(tutor_id,date,start_time,stop_time) VALUES('{$tutor_id}','{$db_date}','{$chosen_stoptime}','{$db_stoptime}')");
									}
									$this->db->simple_query("DELETE FROM tbl_avtime WHERE uid={$uid}");

									goto toNextPelajaran;
									toNextTime:
								}
							}else{
								// TUTOR HAS NO EMPTY SCHEDULE WHILE THERE S A SUBJECT THAT DOESNT HAVE CLASS YET THIS WEEK
							}
						}
						toNextPelajaran:
					}
				}
			} // LOOPS CLOSE
		}
	}
	public function push_notif()
	{
		$fields = array();
		$today = date('Y-m-d');

		// SELECT ALL START TODAY
		$users_arr = array();
		$all_notif = $this->db->query("SELECT * FROM tbl_notif WHERE push=0 ORDER BY timestamp ASC")->result_array();
		foreach ($all_notif as $i => $notif) {
			$notif_id = $notif['notif_id'];
			$id_user = $notif['id_user'];

			if($notif['status'] == 1){
				$this->db->simple_query("UPDATE tbl_notif SET push=1 WHERE notif_id='{$notif_id}'");
			}else{
				$fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm WHERE id_user='{$id_user}'")->row_array();
				if(empty($fcm_token)){
					$this->db->simple_query("UPDATE tbl_notif SET push=1 WHERE notif_id='{$notif_id}'");
				}else{
					$fcm_token = $fcm_token['fcm_token'];
					
					$fields = unserialize($notif['notif_mobile']);
					$fields['to'] = $fcm_token;

					$headers = array(
						'Authorization: key=' . FIREBASE_API_KEY,
						'Content-Type: application/json'
						);
		        	// Open connection
					$ch = curl_init();

		        	// Set the url, number of POST vars, POST data
					curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		        	// Execute post
					$result = curl_exec($ch);
					echo $result;
					$this->db->simple_query("UPDATE tbl_notif SET push=1 WHERE notif_id='{$notif_id}'");
				}
			}
		}
	}
	public function check_room_off($class_id='')
	{
		$dtnow = date('Y-m-d H:i:s');
		$info = $this->db->query("SELECT * FROM tbl_class WHERE break_pos=0 && break_state=0 && finish_time < '{$dtnow}' && class_id='$class_id' ")->row_array();
		if(!empty($info)){
			echo 1;
		}else{
			echo 0;
		}
	}
	public function update_log_convert($type='', $status='3', $class_id='')
	{
		$res = $this->db->simple_query("INSERT INTO log_convert(class_id, type, status) VALUES('$class_id','$type','$status')");
		if(!$res){
			$this->db->simple_query("UPDATE log_convert SET status='$status' WHERE class_id='$class_id' && type='$type'");
		}
		echo 1;
	}
	public function get_log_convert($type='', $class_id='')
	{
		$res = $this->db->query("SELECT status FROM log_convert WHERE class_id='$class_id' AND type='$type'")->row_array();
		if(!empty($res)){
			echo $res['status'];
		}else{
			echo -1;
		}
	}
	public function sshare_convert_state_($value='')
	{
		// PREPARE FOR SEND REMOVE_TOKEN TO JANUS

		$send = array();
		$send['transaction'] = $this->Rumus->RandomString();

		$dtnow = date('Y-m-d H:i:s');
		$data = $this->db->query("SELECT tc.class_id, lc.status FROM tbl_class as tc INNER JOIN log_convert as lc ON tc.class_id=lc.class_id WHERE break_pos=0 && break_state=0 && finish_time < '{$dtnow}' && lc.status > 0 limit 1")->row_array();

		if(!empty($data)){
			if($data['status'] == 2){
				/*$this->db->simple_query("UPDATE log_convert SET status = 1 WHERE ");*/
			}else if($data['status'] == 1){

			}
		}
		$data = $this->db->query("SELECT class_id FROM tbl_class WHERE finish_time < '{$dtnow}' && break_pos != 0")->result_array();
		foreach($data as $key => $value){
			echo "DESTROY CLASS :\n";
			$class_id = $value['class_id'];
			echo "Class {$class_id} has ended\n";
			$this->db->simple_query("UPDATE tbl_class SET break_pos=0, break_state=0 WHERE class_id='{$class_id}'");
			$alltoken = $this->db->query("SELECT tps.session_id, lt.token FROM tbl_participant_session as tps INNER JOIN log_token as lt ON tps.session_id=lt.session_id WHERE tps.class_id='{$class_id}'")->result_array();
			foreach($alltoken as $k => $v) {
				$token = $v['token'];
				echo "Token {$token} has destroyed\n";
				$send['token'] = $v['token'];
				$ari = json_encode($send);
					// TO JANUS

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://beta.dev.meetaza.com:7889/admin");
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch,CURLOPT_POST,1);
				curl_setopt($ch,CURLOPT_POSTFIELDS,$ari);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch,CURLOPT_TIMEOUT_MS,4000); 

				$curl_rets = curl_exec($ch);

				$this->db->simple_query("UPDATE log_token SET destroyed_at=now() WHERE token='{$token}'");
			}
		}
	}
	public function video_convert_($value='')
	{
		$dir    = '/home/videos';

		$dtnow = date('Y-m-d H:i:s');
		$info = $this->db->query("SELECT lc.convert_id, tc.class_id, lc.status FROM tbl_class as tc INNER JOIN log_convert as lc ON tc.class_id=lc.class_id WHERE break_pos=0 && break_state=0 && finish_time < '{$dtnow}' && lc.status = 2 && lc.type='video' limit 1")->row_array();
 		// tempat mentahan .mjr dan tempat jadi nya merged .mp4
 		if(!empty($info)){
 			echo "converting all video from class ".$info['class_id']."<br>";
 			$mjr_list_video = array();
     		$source_dir = $dir . DIRECTORY_SEPARATOR . $info['class_id'] . DIRECTORY_SEPARATOR;
     		$dest_dir = $dir . DIRECTORY_SEPARATOR . $info['class_id']."_temp". DIRECTORY_SEPARATOR;
     		$list_all = scandir($source_dir);
     		foreach ($list_all as $k => $v) {
     			if(strpos($v, '-video.mjr')){
     				if(!file_exists($source_dir.str_replace('-video.mjr', ".mp4", $v)) && !file_exists($dest_dir.str_replace('.mjr', ".mp4", $v))){
     					$mjr_list_video[] = array("video" => $v, "audio" => str_replace("-video.mjr", "-audio.mjr", $v));
     				}
     			}
     		}
     		if(count($mjr_list_video) > 0){
     			mkdir($dest_dir,0777);
     		}
     		foreach ($mjr_list_video as $k => $v) {
     			echo "converting ".$v['video']."...   ";
     			shell_exec("/opt/janus/bin/janus-pp-rec ".$source_dir.$v['video']." ".$dest_dir.str_replace(".mjr", ".mp4", $v['video'])." 2>&1")."<br>";

     			echo "done<br>";
     			echo "converting ".$v['audio']."...   ";
     			shell_exec("/opt/janus/bin/janus-pp-rec ".$source_dir.$v['audio']." ".$dest_dir.str_replace(".mjr", ".webm", $v['audio'])." 2>&1")."<br>";
     			echo "done<br>";
     			echo "removing file...   ";
     			echo shell_exec("sudo /home/robith/remover.sh ".$source_dir.$v['video']." 2>&1")."<br>";
     			echo "done<br>";

     			// $exec = "/usr/bin/sudo ";
     			// echo shell_exec($exec." rm -rf ".$source_dir.$v['video']." 2>&1");
     		}
     		echo "All files successfully converted.<br>";
     		$this->db->simple_query("UPDATE log_convert SET status = 1 WHERE convert_id = '".$info['convert_id']."'");
     		return null;
 		}

	}
	public function video_merge_($value='')
	{
		$dir    = '/home/videos';
		$dtnow = date('Y-m-d H:i:s');
		$info = $this->db->query("SELECT lc.convert_id, tc.class_id, lc.status FROM tbl_class as tc INNER JOIN log_convert as lc ON tc.class_id=lc.class_id WHERE break_pos=0 && break_state=0 && finish_time < '{$dtnow}' && lc.status = 1 && lc.type='video' limit 1")->row_array();
 		// tempat mentahan .mjr dan tempat jadi nya merged .mp4
 		if(!empty($info)){
 			echo "merging file from class ".$info['class_id']."<br>";
 			$mjr_list_video = array();
     		$source_dir = $dir . DIRECTORY_SEPARATOR . $info['class_id']."_temp". DIRECTORY_SEPARATOR;
     		$dest_dir = $dir . DIRECTORY_SEPARATOR . $info['class_id'] . DIRECTORY_SEPARATOR;
     		$list_source = scandir($source_dir);
     		$list_dest = scandir($dest_dir);
     		// cek apakah di source dir ada file ,kalo gk ada berarti kelar
     		if($this->is_dir_empty($source_dir)){
     			echo "class ".$info['class_id']." has done, updating db..   ";
     			$this->db->simple_query("UPDATE log_convert SET status = 0 WHERE convert_id='".$info['convert_id']."'");
     			shell_exec("rm -r ".$source_dir);
     			shell_exec("sudo /home/robith/concat.sh ".$dest_dir." > /dev/null 2>&1 &");

     			echo "done";
     			return null;
     		}
     		//  dan cek apakah di dest dir ada file, kalo ada brarti msh di proses, kalo kosong ya mulai proses
     		if(!$this->is_dir_empty($dest_dir)){
     			echo "class ".$info['class_id']." still proceeding<br>";
     			return null;
     		}
     		foreach ($list_source as $k => $v) {
     			if(strpos($v, '-video.mp4')){
     				$mjr_list_video[] = array("video" => $v, "audio" => str_replace("-video.mp4", "-audio.webm", $v));
     				goto proceed;
     			}
     		}
     		if (count($mjr_list_video) == 0) {
     			echo "All files successfully merged.<br>";
     			$this->db->simple_query("UPDATE log_convert SET status = 0 WHERE convert_id = '".$info['convert_id']."'");
     			return null;
     		}
     		proceed:
     		foreach ($mjr_list_video as $k => $v) {
     			echo "merging ".$v['video']." with ".$v['audio']."...   ".filesize($source_dir.$v['video'])." bytes";

     			echo shell_exec("sudo /home/robith/merger.sh ".$source_dir." ".$dest_dir." > /dev/null 2>&1 &");
     			// echo shell_exec("/usr/bin/ffmpeg -i ".$source_dir.$v['video']." -i ".$source_dir.$v['audio']." -y ".$dest_dir.str_replace("-video.mp4", ".mp4", $v['video'])." 2>&1");
     			// unlink($source_dir.$v['video']);
     			// unlink($source_dir.$v['audio']);
     			echo "done<br>";
     			// $exec = "/usr/bin/sudo ";
     			// echo shell_exec($exec." rm -rf ".$source_dir.$v['video']." 2>&1");
     		}
     		
 		}
	}
	public function sshare_convert_($value='')
	{
		$dir    = '/home/ssharing';

		$dtnow = date('Y-m-d H:i:s');
		$info = $this->db->query("SELECT lc.convert_id, tc.class_id, lc.status FROM tbl_class as tc INNER JOIN log_convert as lc ON tc.class_id=lc.class_id WHERE break_pos=0 && break_state=0 && finish_time < '{$dtnow}' && lc.status = 2 && lc.type='screenshare' limit 1")->row_array();
 		if(!empty($info)){
 			echo "converting all screenshare from class ".$info['class_id']."<br>";
 			$mjr_list_video = array();
     		$source_dir = $dir . DIRECTORY_SEPARATOR . $info['class_id'] . DIRECTORY_SEPARATOR;
     		$dest_dir = $dir . DIRECTORY_SEPARATOR . $info['class_id']."_temp". DIRECTORY_SEPARATOR;
     		$list_all = scandir($source_dir);
     		foreach ($list_all as $k => $v) {
     			if(strpos($v, '-video.mjr')){
     				if(!file_exists($source_dir.str_replace('-video.mjr', ".mp4", $v)) && !file_exists($dest_dir.str_replace('.mjr', ".mp4", $v))){
     					$mjr_list_video[] = array("video" => $v);
     				}
     			}
     		}
     		if(count($mjr_list_video) > 0){
     			mkdir($dest_dir,0777);
     		}
     		foreach ($mjr_list_video as $k => $v) {
     			echo "converting ".$v['video']."...   ";
     			shell_exec("/opt/janus/bin/janus-pp-rec ".$source_dir.$v['video']." ".$dest_dir.str_replace(".mjr", ".mp4", $v['video'])." 2>&1")."<br>";
     			echo "done<br>";

     			echo "removing file...   ";
     			echo shell_exec("sudo /home/robith/remover.sh ".$source_dir.$v['video']." 2>&1")."<br>";
     			echo "done<br>";

     			// $exec = "/usr/bin/sudo ";
     			// echo shell_exec($exec." rm -rf ".$source_dir.$v['video']." 2>&1");
     		}
     		echo "All files successfully converted.<br>";
     		$this->db->simple_query("UPDATE log_convert SET status = 1 WHERE convert_id = '".$info['convert_id']."'");
     		return null;
 		}

	}
	public function sshare_merge_($value='')
	{
		$dir    = '/home/ssharing';
		$dtnow = date('Y-m-d H:i:s');
		$info = $this->db->query("SELECT lc.convert_id, tc.class_id, lc.status FROM tbl_class as tc INNER JOIN log_convert as lc ON tc.class_id=lc.class_id WHERE break_pos=0 && break_state=0 && finish_time < '{$dtnow}' && lc.status = 1 && lc.type='screenshare' limit 1")->row_array();
 		if(!empty($info)){
 			echo "merging file from class ".$info['class_id']."<br>";
 			$mjr_list_video = array();
     		$source_dir = $dir . DIRECTORY_SEPARATOR . $info['class_id']."_temp". DIRECTORY_SEPARATOR;
     		$dest_dir = $dir . DIRECTORY_SEPARATOR . $info['class_id'] . DIRECTORY_SEPARATOR;
     		$list_source = scandir($source_dir);
     		$list_dest = scandir($dest_dir);
     		// cek apakah di source dir ada file ,kalo gk ada berarti kelar
     		if($this->is_dir_empty($source_dir)){
     			echo "class ".$info['class_id']." has done, updating db..   ";
     			$this->db->simple_query("UPDATE log_convert SET status = 0 WHERE convert_id='".$info['convert_id']."'");
     			shell_exec("rm -r ".$source_dir);
     			shell_exec("sudo /home/robith/concat.sh ".$dest_dir." > /dev/null 2>&1 &");

     			echo "done";
     			return null;
     		}
     		//  dan cek apakah di dest dir ada file, kalo ada brarti msh di proses, kalo kosong ya mulai proses
     		if(!$this->is_dir_empty($dest_dir)){
     			echo "class ".$info['class_id']." still proceeding<br>";
     			return null;
     		}
     		foreach ($list_source as $k => $v) {
     			if(strpos($v, '-video.mp4')){
     				$mjr_list_video[] = array("video" => $v);
     				goto proceed;
     			}
     		}
     		if (count($mjr_list_video) == 0) {
     			echo "All files successfully merged.<br>";
     			$this->db->simple_query("UPDATE log_convert SET status = 0 WHERE convert_id = '".$info['convert_id']."'");
     			return null;
     		}
     		proceed:
     		foreach ($mjr_list_video as $k => $v) {
     			echo "transalting ".$v['video']."(vp8) into h264...   ".filesize($source_dir.$v['video'])." bytes";

     			echo shell_exec("sudo /home/robith/merger.sh ".$source_dir." ".$dest_dir." > /dev/null 2>&1 &");
     			// echo shell_exec("/usr/bin/ffmpeg -i ".$source_dir.$v['video']." -i ".$source_dir.$v['audio']." -y ".$dest_dir.str_replace("-video.mp4", ".mp4", $v['video'])." 2>&1");
     			// unlink($source_dir.$v['video']);
     			// unlink($source_dir.$v['audio']);
     			echo "done<br>";
     			// $exec = "/usr/bin/sudo ";
     			// echo shell_exec($exec." rm -rf ".$source_dir.$v['video']." 2>&1");
     		}
     		
 		}
	}
	function is_dir_empty($dir) {
		$tfile = count(scandir($dir));
		if($tfile > 2){
			return false;
		}else{
			return true;
		}
	}

	function check_midtrans()
	{
		$data 		= $this->db->query("SELECT * FROM log_transaction WHERE flag='0'")->result_array();
		foreach ($data as $key => $value) {
			// echo "<br><br>Details Data : <br>";
			$order_id 	= $value['trx_id'];
			$getmidtrans = json_encode($this->veritrans->status($order_id));
			// echo $getmidtrans;
			$array = json_decode($getmidtrans,true);
			$status = $array['transaction_status'];
			if ($status == "settlement") {
				
				$this->db->simple_query("UPDATE log_transaction SET flag = 1 WHERE trx_id='$order_id'");
				$this->db->simple_query("UPDATE log_midtrans_transaction SET transaction_status = '$status' WHERE order_id='$order_id'");	
			}
			else
			{

			}
		}
	}

	public function get_utc_($value='')
	{
		$server_utc = $this->Rumus->getGMTOffset();
		echo $server_utc;
	}

	public function clear_token_janus($janus='')
	{
		$list_class = $this->db->query("SELECT class_id FROM temp_active_room WHERE janus='$janus'")->result_array();
		foreach ($list_class as $key => $value) {
			$cid = $value['class_id'];
			$list_session = $this->db->query("SELECT session_id FROM tbl_participant_session WHERE class_id='$cid' ")->result_array();
			$list_delete = "";
			foreach ($list_session as $k => $v) {
				$ssid = $v['session_id'];
				$list_delete.= "'$ssid', ";
			}
			$list_delete = rtrim($list_delete, ', ');
			$this->db->simple_query("DELETE FROM log_token WHERE session_id IN ($list_delete)");
		}
	}
	public function test($session_id='')
	{
		$DB2 = $this->load->database('db2', TRUE); // SAY SEA SERVER CLASSPLAY DB
		$db_res = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
		if(empty($DB2->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array())){
			$qc = "";
			$qc_data = "";
			$setter = "";
			foreach ($db_res as $key => $value) {
				$qc.= $key.", ";
				$qc_data.= "'$value', ";
				$setter.= "$key='$value', ";
			}
			$qc = rtrim($qc, ", ");
			$qc_data = rtrim($qc_data, ", ");
			$setter = rtrim($setter, ", ");
			$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
		}
	}
	public function mandiri_get_mutasi()
	{
		$this->load->library('mandiribot', array('company_id' => 'FD02434', 'username' => 'MEETAZA304', 'password' => 'N4n4N1n1'));
		$this->mandiribot->login();
		echo date('Y-m-d');
		if($this->mandiribot->isLoggedIn){
			echo "true";
			$date_awal = DateTime::createFromFormat('Y-m-d',date('Y-m-d'));
			$date_awal = $date_awal->modify('-1 day');

			$mutasi = $this->mandiribot->getMutasiRekening($date_awal->format('Y-m-d'), date('Y-m-d'));

			$bank_id = 2;
			foreach ($mutasi as $key => $value) {
				$date_field = DateTime::createFromFormat('d/m/Y H:i:s', $value['TanggalDanWaktu']);
				$date_field = $date_field->format('Y-m-d H:i:s');
				$description_field = $value['Deskripsi'];
				$credit_amount = str_replace(',','',$value['Kredit']);
				$balance_after_credit = $value['Saldo'] != null ? str_replace(',','',$value['Saldo']) : 0;
				$full_text = json_encode($value);
				if($this->db->simple_query("INSERT INTO log_mutasi_bank(bank_id, date_field, description_field, credit_amount, balance_after_credit, full_text) VALUES($bank_id, '$date_field', '$description_field', $credit_amount, $balance_after_credit, '$full_text')")){
					$this->Process_model->inbound_transfer($date_field, $description_field, $credit_amount, '1370014152082', 'MEETAZA PRAWIRA MEDI');
				}
			}
			echo '<pre>';
			print_r($mutasi);
			print_r($this->mandiribot->last_output);
			echo '</pre>';
		}else{
			echo "false";
		}
		$this->mandiribot->logout();
		// echo '<pre>';
		// print_r($this->mandiribot->last_output);
		// echo '</pre>';
	}
	public function bca_parser_login_only($value='')
	{
		$this->Parser = new BCAParser('hermawan3040', '159075');
		print_r($this->Parser);
		$this->session->set_userdata('Parsers', $this->Parser);
		// $this->bca_parser_listtr_only($this->Parser);
		// $this->Parser->logout();
	}
	public function bca_parser_logout_only($value='')
	{
		$this->Parser = new BCAParser('hermawan3040', '159075', 'abc');
		$this->Parser->logout();
	}
	public function bca_parser_listtr_only($curl_object='')
	{
		$this->Parser = $this->session->userdata('Parsers');
		print_r($this->Parser);
		$curl_object = $this->Parser;
		$this->Parser = new BCAParser('hermawan3040', '159075', $curl_object);
		$a = $this->Parser->getListTransaksi('2017-11-14', '2017-11-15');
		print_r($a);
	}
	public function bca_parser($from='', $to='')
	{
		if($from != '' && $to != ''){
			$Parser = new BCAParser('hermawan3040', '159075');
			/*print_r('<pre>');
			print_r($Parser);
			print_r('</pre>');*/

			$a = $Parser->getListTransaksi($from, $to);
			// $a = $Parser->getTransaksiCredit('2016-11-10', '2016-12-06');
			// print_r($a);
			// $Html = $Parser->getMutasiRekening('2017-11-28', '2017-12-01');
			/*print_r('<pre>');
			print_r($a);
			print_r('</pre>');*/

			$new_arr = array();
			// $a[0]['flows'];
			foreach ($a as $key => $value) {
				if($value['flows'] == 'CR') {
					// print_r($a[$key]);
					array_push($new_arr, $a[$key]);
					// echo "1-";
				}else{
					// echo "0-";
				}
			}
			print_r('<pre>');
			print_r($new_arr);
			print_r('</pre>');
			$counter_id = false;
			$total = $this->db->query("SELECT * FROM log_mutasi_bca_counter WHERE account_uname='hermawan3040' && date_field='$from'")->row_array();
			if(!empty($total)){
				$total = $total['total'];
				$counter_id = $total['cid'];
			}else{
				$total = 0;
			}
			$total_response = count($new_arr);
			// echo $total_response."-".$total;
			if($total_response > $total){
				foreach ($new_arr as $key => $value) {
					if($key >= $total) {
						$date_field = $value['date'];
						$description_field = $this->db->escape_str(json_encode($value['description']));
						$credit_amount = $value['description'][count($value['description'])-1];
						$credit_amount = str_replace(',', '', $credit_amount);
						$this->db->simple_query("INSERT INTO log_mutasi_bank(bank_id, account_uname, date_field, description_field, credit_amount) VALUES('bca', 'hermawan3040', '$date_field', '$description_field', '$credit_amount') ");
					}
				}
				if(!$counter_id){
					$this->db->simple_query("INSERT INTO log_mutasi_bca_counter(account_uname, date_field, total) VALUES('hermawan3040', '$from', $total_response)");
				}else{
					$this->db->simple_query("UPDATE log_mutasi_bca_counter SET total=$total_response WHERE cid=$counter_id");
				}
			}
			$Parser->logout();
		}
	}
	public function mutasi_processor($value='')
	{
		$data = $this->db->query("SELECT * FROM log_mutasi_bank WHERE status=0")->result_array();

		foreach ($data as $key => $value) {
			$bank_id = $value['bank_id'];
			$date_field = $value['date_field'];
			$description_field = $value['description_field'];
			$credit_amount = $value['credit_amount'];
			$balance_after_credit = $value['balance_after_credit'];

			$matched = false;

			$data_order = $this->db->query("SELECT tord.order_id, tinv.invoice_id, trek.nama_akun, trek.nomer_rekening FROM tbl_order as tord INNER JOIN ( tbl_invoice as tinv INNER JOIN tbl_rekening as trek ON tinv.id_rekening=trek.id_rekening ) ON tord.order_id=tinv.order_id WHERE (tord.order_status=2 || tord.order_status=-3) && tinv.underpayment=$credit_amount ")->result_array();
			$chosen_invoice_id;
			$chosen_order_id;
			foreach ($data_order as $k => $v) {
				$nama_akun = $v['nama_akun'];
				$nomer_rekening = $v['nomer_rekening'];
				if(strpos($description_field, strtoupper($nama_akun)) !== false){
					$matched = true;
					$chosen_invoice_id = $v['invoice_id'];
					$chosen_order_id = $v['order_id'];
					break;
				}else if(strpos($description_field, $nomer_rekening) !== false){
					$matched = true;
					$chosen_invoice_id = $v['invoice_id'];
					$chosen_order_id = $v['order_id'];
					break;
				}
			}
			if($matched){
				// SET LOG MUTASI TO HAVE DONE PROCESSING
				$this->db->simple_query("UPDATE log_mutasi_bank SET status=1 WHERE bank_id='$bank_id' && date_field='$date_field' && description_field='$description_field' && balance_after_credit='$balance_after_credit'");
				// PROCEED THE ORDER ITSELF

				$this->db->simple_query("UPDATE tbl_invoice SET underpayment=0 WHERE invoice_id='$chosen_invoice_id'");
				$o_detail = $this->db->query("SELECT tod.order_id, tod.product_id as rtp, tod.type, tr.id_user_requester as tr_id_user, tr.tutor_id as tr_tutor_id, trg.id_user_requester as trg_id_user, trg.tutor_id as trg_tutor_id, tc.tutor_id as trm_tutor_id, trm.id_requester as trm_id_user, trp.id_requester as trp_id_user, tc.class_id FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id LEFT JOIN (tbl_request_program as trp INNER JOIN tbl_price_program as tpp ON trp.program_id=tpp.list_id) ON trp.request_id=tod.product_id WHERE tod.order_id='$chosen_order_id'")->result_array();
				$newarr = array();
				foreach ($o_detail as $kz => $vz) {
					$tutor_id = $vz['tr_tutor_id'] != null ? $vz['tr_tutor_id'] : ( $vz['trg_tutor_id'] != null ? $vz['trg_tutor_id'] : ($vz['trm_tutor_id'] != null ? $vz['trm_tutor_id'] : ''));
					$id_user  = $vz['tr_id_user'] != null ? $vz['tr_id_user'] : ( $vz['trg_id_user'] != null ? $vz['trg_id_user'] : ( $vz['trm_id_user'] != null ? $vz['trm_id_user'] : $vz['trp_id_user'] ));
					$newarr[] = array("trx_id" => $vz['order_id'], 'type' => $vz['type'], 'id_user' => $id_user, 'tutor_id' => $tutor_id, 'class_id' => $vz['class_id'], 'rtp' => $vz['rtp']);

				}
				// echo json_encode(array("status" => true, "aksi" => 1, "message" => "found", "data" => $newarr));
				foreach ($newarr as $kn => $vn) {
					$trx_id 	= $vn['trx_id'];
                    $id_user 	= $vn['id_user'];
                    $tutor_id 	= $vn['tutor_id'];
                    $class_id 	= $vn['class_id'];
                    $rtp 		= $vn['rtp'];
                    $type 		= $vn['type'];

                    $get_data 	= $this->db->query("SELECT email,user_name FROM tbl_user WHERE id_user='$id_user'")->row_array();
                    $email 		= $get_data['email'];
                    $user_name 	= $get_data['user_name'];
                    $isi 		= 'Terima kasih telah melakukan pembayaran di classmiles, Pembayaran anda telah berhasil kami terima';
                    $isi_ing 	= 'Thank you for making payment in classmiles, our payment has been successfully received';

                    if($type == 'multicast'){
                    	$link = base_url().'Process/ConfirmPaymentMulticast/399';
                    }else if($type == 'private'){
                    	$link = base_url().'Process/ConfirmPaymentPrivate/399';
                    }else if($type == 'group'){
                    	$link = base_url().'Process/ConfirmPaymentGroup/399';
                    }else if($type == 'program'){
                    	$link = base_url().'Process/ConfirmPaymentProgram/399';
                    }
                    $ari = array("trx_id" => $trx_id, "id_user" => $id_user, "tutor_id" => $tutor_id, "class_id" => $class_id, "rtp" => $rtp, "type" => $type);
                    $headr[] = 'Authorization: Basic '.base64_encode('cvuser:adaserangan123');

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $link);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
					curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_HTTPHEADER,$headr);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch,CURLOPT_POST,1);
					curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($ari));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
					curl_setopt($ch,CURLOPT_TIMEOUT_MS,8000); 

					$curl_rets = curl_exec($ch);
					print_r($curl_rets);

					// // Open connection
	    //             $ch = curl_init();

	    //             // Set the url, number of POST vars, POST data
	    //             curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	    //             curl_setopt($ch, CURLOPT_POST, true);
	    //             // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    //             curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    //             curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_paymentreceived','content' => array("email" => "$email", "user_name" => "$user_name", "isi" => "$isi", "isi_ing" => "$isi_ing", "hargakelas" => "$credit_amount") )));

	    //             // Execute post
	    //             $result = curl_exec($ch);
	    //             curl_close($ch);
	    //             // echo $result;
				}

			}else{
				$this->db->simple_query("UPDATE log_mutasi_bank SET status=2 WHERE bank_id=$bank_id && date_field='$date_field' && description_field='$description_field' && balance_after_credit=$balance_after_credit");
			}
		}
	}
	public function crawler($value='')
	{

		$a = $this->db->query("select tpk.kid_id, tu.id_user, tu.user_countrycode from tbl_profile_kid as tpk inner join tbl_user as tu ON tpk.parent_id=tu.id_user")->result_array();

		foreach ($a as $key => $value) {
			$cc = $value['user_countrycode'];
			$kid_id = $value['kid_id'];
			$this->db->simple_query("UPDATE tbl_user SET user_countrycode='$cc' WHERE id_user='$kid_id'");
			echo $cc."-".$kid_id."<br>";
		}
	}

	public function channel_package_activator($value='')
	{
		$date = date('Y-m-d');

		$data = $this->db->query("SELECT * FROM master_channel_package WHERE status_active=0 AND end_date >='$date' AND '$date' >= start_date ")->result_array();
		print_r($data);
		foreach ($data as $key => $value) {
			$channel_id 	= $value['channel_id'];
			$kuota 		= $value['kuota'];
			$pid 		= $value['pid'];
			// LIST TOTAL ACTIVE MEMBER ON CHANNEL_TUTOR AND CHANNEL_STUDENT
			$total_active_tutor = $this->db->query("SELECT COUNT(*) FROM master_channel_tutor WHERE channel_id={$channel_id} AND status='active' ")->row_array()['COUNT(*)'];
			$total_active_student = $this->db->query("SELECT COUNT(*) FROM master_channel_student WHERE channel_id={$channel_id} AND status='active' ")->row_array()['COUNT(*)'];

			$interval = $kuota - ( intval($total_active_student) + intval($total_active_tutor) );
			if($interval > 0){
				$unactived_tutor = $this->db->query("SELECT * FROM master_channel_tutor WHERE channel_id={$channel_id} AND status='inactive' ORDER BY created_at ASC limit 0,{$interval} ")->result_array();
				if(!empty($unactived_tutor)){
					$interval -= count($unactived_tutor);
					$to_update = "";
					foreach ($unactived_tutor as $k => $v) {
						$to_update .= "tutor_id=".$v['tutor_id']." OR ";
					}
					$to_update = rtrim($to_update, " OR ");
					$this->db->simple_query("UPDATE master_channel_tutor SET status='active' WHERE $to_update ");
				}
				if($interval > 0){
					$unactived_student = $this->db->query("SELECT * FROM master_channel_student WHERE channel_id={$channel_id} AND status='inactive' ORDER BY created_at ASC limit 0,{$interval} ")->result_array();
					if(!empty($unactived_student)){
						$interval -= count($unactived_student);
						$to_update = "";
						foreach ($unactived_student as $k => $v) {
							$to_update .= "id_user=".$v['id_user']." OR ";
						}
						$to_update = rtrim($to_update, " OR ");
						$this->db->simple_query("UPDATE master_channel_student SET status='active' WHERE $to_update ");
					}
				}
				
			}else if($interval < 0){
				$interval_sementara = $interval*-1;
				$actived_student = $this->db->query("SELECT * FROM master_channel_student WHERE channel_id={$channel_id} AND status='active' ORDER BY created_at DESC limit 0,{$interval_sementara} ")->result_array();
				if(!empty($actived_student)){
					$interval += count($actived_student);
					$to_update = "";
					foreach ($actived_student as $k => $v) {
						$to_update .= "id_user=".$v['id_user']." OR ";
					}
					$to_update = rtrim($to_update, " OR ");
					$this->db->simple_query("UPDATE master_channel_student SET status='inactive' WHERE $to_update ");
				}
				if($interval < 0){
					$interval_sementara = $interval*-1;
					$actived_tutor = $this->db->query("SELECT * FROM master_channel_tutor WHERE channel_id={$channel_id} AND status='active' ORDER BY created_at DESC limit 0,{$interval_sementara} ")->result_array();
					if(!empty($actived_tutor)){
						$interval += count($actived_tutor);
						$to_update = "";
						foreach ($actived_tutor as $k => $v) {
							$to_update .= "tutor_id=".$v['tutor_id']." OR ";
						}
						$to_update = rtrim($to_update, " OR ");
						$this->db->simple_query("UPDATE master_channel_tutor SET status='inactive' WHERE $to_update ");
					}
				}
			}
			$this->db->simple_query("UPDATE master_channel_package SET status_active=1 WHERE pid=$pid ");
		}
	}
	public function channel_package_deactivator($value='')
	{
		$date = date('Y-m-d');

		$data = $this->db->query("SELECT * FROM master_channel_package WHERE status_active=1 AND end_date <='$date' ")->result_array();
		print_r($data);
		foreach ($data as $key => $value) {
			$channel_id 	= $value['channel_id'];
			$kuota 		= $value['kuota'];
			$pid 		= $value['pid'];
			
			// INACTIVE ALL STUDENT AND TUTORS
			$this->db->simple_query("UPDATE master_channel_student SET status='inactive' WHERE channel_id=$channel_id ");
			$this->db->simple_query("UPDATE master_channel_tutor SET status='inactive' WHERE channel_id=$channel_id ");
			$this->db->simple_query("UPDATE master_channel_package SET status_active=-1 WHERE pid=$pid ");
		}
	}
}
