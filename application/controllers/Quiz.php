<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quiz extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->library('recaptcha');
		$this->session->set_userdata('color','blue'); 
		$this->load->library('email');
        $this->load->helper(array('Form', 'Cookie', 'String'));

        $this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
		// Check for any mobile device.
		if ($detect->isMobile()){
			if (base_url() == 'https://classmiles.com/') {
			}else{

			}
			define('MOBILE', 'mobile/');

		}else{
			define('MOBILE', '');
		}
	}

	public function Quiz()
	{
		if($this->session_check() == 1){
			$data['sideactive'] = "quizside";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'quiz/list_quiz',$data);
		}
		else{
			redirect('/');
		}
	}

	public function QuizLMS($value='')
	{
		if($this->session_check() == 1){
			$data['sideactive'] = "quizcourses";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'quiz/quiz_lms',$data);
		}
		else{
			redirect('/');
		}
	}

	public function Courses()
	{
		if($this->session_check() == 1){
			$data['sideactive'] = "quizcourses";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'quiz/courses',$data);
		}
		else{
			redirect('/');
		}
	}

	public function Start()
	{
		if($this->session_check() == 1){
			$data['sideactive'] = "quizcourses";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'quiz/start_quiz',$data);
		}
		else{
			redirect('/');
		}
	}

	public function session_check()
	{
		if($this->session->has_userdata('id_user')){
			$new_state = $this->db->query("SELECT status FROM tbl_user where id_user='".$this->session->userdata('id_user')."'")->row_array()['status'];
			if($this->session->userdata('status') != $new_state ){
				$this->session->sess_destroy();
				redirect('/');
			}
			// $this->sesi['user_name'] = $this->session->userdata('user_name');
			// $this->sesi['username'] = $this->session->userdata('username');
			// $this->sesi['priv_level'] = $this->session->userdata('priv_level');
			if($this->session->userdata('usertype_id') != "student" AND $this->session->userdata('usertype_id') != "student kid"){
				redirect('/');
			}
			return 1;
		}else{
			return 0;
		}
	}
}
