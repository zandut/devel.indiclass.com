<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
        $params = array('server_key' => 'VT-server-31TBIo8Zf4Pa-I7j4pKj8CID', 'production' => true);
		$this->load->library('veritrans');
		$this->veritrans->config($params);
		$this->load->helper('url');		
    }

    public function index()
    {
    	$this->load->view('veritrans/transaction');
    }

    public function process()
    {
    	$order_id = $this->input->post('order_id');
    	$action = $this->input->post('action');
    	switch ($action) {
		    case 'status':
		        $this->status($order_id);
		        break;
		    case 'approve':
		        $this->approve($order_id);
		        break;
		    case 'expire':
		        $this->expire($order_id);
		        break;
		   	case 'cancel':
		        $this->cancel($order_id);
		        break;
		}

    }

	public function status($order_id)
	{
		echo 'test get status </br>';
		$a = json_encode($this->veritrans->status($order_id));
		echo $a;
		$array = json_decode($a,true);
		echo "<br><br>";
		// echo 'Master Card : '.$array['masked_card'];
		// echo "<br>";
		// echo 'Approval Code : '.$array['approval_code'];
		// echo "<br>";
		// echo 'Bank : '.$array['bank'];
		// echo "<br>";
		echo 'Transaction Time : '.$array['transaction_time'];
		echo "<br>";
		echo 'Amount : '.$array['gross_amount'];
		echo "<br>";
		echo 'Order ID : '.$array['order_id'];
		echo "<br>";
		echo 'Payment Type : '.$array['payment_type'];
		echo "<br>";
		echo 'Signature Key : '.$array['signature_key'];
		echo "<br>";
		echo 'Transaction Status : '.$array['transaction_status'];
		echo "<br>";
		echo 'Fraud Status : '.$array['fraud_status'];
		echo "<br>";
		echo 'Status Message : '.$array['status_message'];
		echo "<br>";
		// $array = json_decode($this->veritrans->status($order_id), true);
	}

	public function handling(){
		// $a = json_encode($data);
		// echo $a;
		// $a = json_decode($this->input->post(),true);
		$order_id = json_decode($this->input->raw_input_stream,TRUE);
		// print_r();
		$a = $order_id['order_id'];
		$this->db->query("INSERT INTO coba VALUES('','$a')");

	}

	public function cancel($order_id)
	{
		echo 'test cancel trx </br>';
		echo $this->veritrans->cancel($order_id);
	}

	public function approve($order_id)
	{
		echo 'test get approve </br>';
		print_r ($this->veritrans->approve($order_id) );
	}

	public function expire($order_id)
	{
		echo 'test get expire </br>';
		print_r ($this->veritrans->expire($order_id) );
	}

	public function multicast_join($class_id = '',$user_utc = '',$child_id = ''){
		if (!isset($_GET['t'])) {
			return 0;
		}
		else
		{
			$t 	= $_GET['t'];	
		}
		if ($t == "kids") {
			$id_user = $_GET['child_id'];
			
			$q = $this->db->query("SELECT tc.*, tcp.harga FROM tbl_class as tc LEFT JOIN tbl_class_price as tcp ON tc.class_id=tcp.class_id WHERE tc.class_id='$class_id' AND tc.participant NOT LIKE '%\"$id_user\":\"$id_user\"%'")->row_array();
			if(!empty($q)){
				$class_type = $q['class_type'];
	            $tutor_id = $q['tutor_id'];
	            $parent_id = $this->session->userdata('id_user');
				// AND tc.class_type='multicast_paid'
				if($class_type == 'multicast' || $class_type == 'multicast_channel_paid'){
					$participant_arr = json_decode($q['participant'],true);
					if(!array_key_exists('participant', $participant_arr)){
						$participant_arr['participant'] = array();
					}
					$ada = 0;					
					foreach ($participant_arr['participant'] as $key => $value) {
						if ($value['id_user'] == $parent_id) {
							$ada = 1;
						}
					}

					if ($ada == 1) {
						array_push($participant_arr['participant'], array("id_user" => $id_user));							
					}
					else
					{
						array_push($participant_arr['participant'], array("id_user" => $id_user));
						array_push($participant_arr['participant'], array("id_user" => $parent_id));
					}
					
					$participant_arr = json_encode($participant_arr);
					$this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
					$cekrating = $this->db->query("SELECT * FROM tbl_class_rating WHERE class_id='$class_id' and id_user='$parent_id'")->row_array();
					if (empty($cekrating)) {
						$this->db->simple_query("INSERT INTO tbl_class_rating(class_id, id_user, class_ack) VALUES('$class_id','$parent_id',-1)");
					}					

					////------------ BUAT NGIRIM EVENT FIREBASE --------------//
		            $json_notif = "";
		            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$id_user}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
		            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
		                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "10","title" => "Classmiles", "text" => "")));
		            }
		            if($json_notif != ""){
		                $headers = array(
		                    'Authorization: key=' . FIREBASE_API_KEY,
		                    'Content-Type: application/json'
		                    );
		                // Open connection
		                $ch = curl_init();

		                // Set the url, number of POST vars, POST data
		                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		                curl_setopt($ch, CURLOPT_POST, true);
		                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

		                // Execute post
		                $result = curl_exec($ch);
		                curl_close($ch);
		                // echo $result;
		            }
		            ////------------ END -------------------------------------//
					
					$namee = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$id_user'")->row_array();
					$this->session->set_flashdata('mes_alert','success');
					$this->session->set_flashdata('mes_display','block');
					$this->session->set_flashdata('mes_message', "<label class='c-yellow'>".$namee['user_name']."</label>, Berhasil mengikuti kelas ".$q['name']." - ".$q['description']);
					redirect('/MyClass');
				}
				else
				{
					$user_utc 			= $this->session->userdata('user_utc');
					$datenow 			= date("Y-m-d h:i:s");
					$checknow 			= date("Y-m-d h:i:s");
					$server_utc 		= $this->Rumus->getGMTOffset();
					$interval 			= $user_utc - $server_utc;
					$datenow 			= DateTime::createFromFormat ('Y-m-d H:i:s',$datenow);		
					$datenow->modify("-".$interval ." minutes");
					$datenow 			= $datenow->format('Y-m-d H:i:s');
					$expired			= $q['start_time'];
					$namee 				= $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$id_user'")->row_array();

					// $cekexpired			= $this->db->query("SELECT * FROM tbl_class WHERE '$datenow' <= start_time AND class_id='$class_id'")->row_array();
					$cekexpired			= $this->db->query("SELECT * FROM tbl_class WHERE '$checknow' <= last_order AND class_id='$class_id'")->row_array();
					if (empty($cekexpired)) {
						$this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message', "Expired");
						redirect('/MyClass');
					}
					else
					{							
						$invoicecheck 	= $this->db->query("SELECT * FROM tbl_order_detail as tod INNER JOIN tbl_request_multicast as trm ON tod.product_id=trm.request_id WHERE tod.class_id='$class_id' AND trm.id_requester='$id_user'")->row_array();
						// echo "SELECT * FROM tbl_order_detail as tod INNER JOIN tbl_order as tor ON tod.order_id=tor.order_id INNER JOIN tbl_invoice as ti ON ti.order_id=tod.order_id WHERE tod.class_id='$class_id' AND tor.buyer_id='$id_user'";
    					// return false;
						if (!empty($invoicecheck)) {
							$od = $invoicecheck['order_id'];
					        $now = date('Y-m-d h:i:s');
							$ckExpired = $this->db->query("SELECT * FROM tbl_order WHERE order_id = '$od' AND payment_due <= '$now' ")->row_array();
							if (empty($ckExpired)) {
								$this->session->set_flashdata('mes_alert','danger');
								$this->session->set_flashdata('mes_display','block');
								$this->session->set_flashdata('mes_message', 'Pembelian kelas ini sudah terdapat di invoice. segera lakukan pembayaran.<br>Details pembayaran dapat di lihat : <a style="color:#00e640;" href="'.BASE_URL().'/DetailOrder?order_id='.$od.'">Disini</a>');
								redirect('/MyClass');
							}
							else
							{
								//MASUKIN KE TBL_REQUEST_MULTICAST
								$request_id 	= $this->Rumus->get_new_request_id('multicast');
								$add 			= $this->db->query("INSERT INTO tbl_request_multicast (request_id,class_id,id_requester,date_expired,status) VALUES ('$request_id','$class_id','$id_user','$expired','2')");
								$lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request_multicast')->row('request_id');

								$hargaakhir 		= $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id='$class_id'")->row_array()['harga_cm'];								

								//MASUK KE INVOICE
			                    $iptInvoice = $this->Process_model->createInvoice($lastrequestid,'multicast');			                    
			                    if ($iptInvoice == 1) {
			                    	$participantt  		= $this->db->query("SELECT participant FROM tbl_class WHERE class_id = '$class_id'")->row_array()['participant'];
			                    	$participant_arr 	= json_decode($participantt,true);					
									if(!array_key_exists('participant', $participant_arr)){						
										$participant_arr['participant'] = array();
									}
									array_push($participant_arr['participant'], array("id_user" => $id_user, "st" => 0));
									// echo json_encode($participant_arr);
									$participant_arr 	= json_encode($participant_arr);
									$this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");

									//SEND NOTIF
									$idadmin = $this->db->query("SELECT id_user, user_name FROM  tbl_user WHERE usertype_id ='admin'")->row_array();
									$adminid = $idadmin['id_user'];				
									$adminname = $idadmin['user_name'];	

									//NOTIF STUDENT
									$notif_message = json_encode( array("code" => 110));					
									$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','https://classmiles.com/Transaction/multicast_join/".$class_id."?t=user&user_utc=".$this->session->userdata('user_utc')."&view=detail','0')");

									//NOTIF ADMIN
									$notif_message = json_encode( array("code" => 111, "tutor_name" => $adminname));					
									$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','5','https://classmiles.com/admin/PaymentMulticast','0')");

									$order_id = $this->db->query("SELECT order_id FROM tbl_order_detail WHERE product_id='$lastrequestid'")->row_array()['order_id'];
									// $this->session->set_flashdata('mes_alert','success');
									// $this->session->set_flashdata('mes_display','block');
									// $this->session->set_flashdata('mes_message', "Pesanan kelas telah ditambahkan ke keranjang, silahkan cek keranjang anda untuk melanjutkan ke pembayaran.");
									redirect('/DetailOrder?order_id='.$order_id);	
								}
								else{
									$this->session->set_flashdata('mes_alert','danger');
									$this->session->set_flashdata('mes_display','block');
									$this->session->set_flashdata('mes_message', 'Pembelian anda gagal');
									redirect('/MyClass');
								}
							}
						}
						else
						{
							if (isset($_GET['view'])) {
					            $lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request_multicast')->row('request_id');
					            $data['trx_id']     = $lastrequestid;
					            $data['classid']    = $class_id;
					            $data['user_utc']   = $user_utc;
					            $data['sideactive'] = "home";
					            $this->load->view('inc/header');
					            $this->load->view('class_detailpay',$data);
					        }
					        else
					        {  
								//MASUKIN KE TBL_REQUEST_MULTICAST
					            $request_id     = $this->Rumus->get_new_request_id('multicast');
					            $add            = $this->db->query("INSERT INTO tbl_request_multicast (request_id,class_id,id_requester,date_expired,status) VALUES ('$request_id','$class_id','$id_user','$expired','2')");
					            $lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request_multicast')->row('request_id');

					            $hargaakhir         = $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id='$class_id'")->row_array()['harga_cm'];                             

					            //MASUK KE INVOICE
					            $iptInvoice = $this->Process_model->createInvoice($lastrequestid,'multicast');                              
					            if ($iptInvoice == 1) {
					            	$participantt  		= $this->db->query("SELECT participant FROM tbl_class WHERE class_id = '$class_id'")->row_array()['participant'];
			                    	$participant_arr 	= json_decode($participantt,true);					
									if(!array_key_exists('participant', $participant_arr)){						
										$participant_arr['participant'] = array();
									}
									array_push($participant_arr['participant'], array("id_user" => $id_user, "st" => 0));
									// echo json_encode($participant_arr);
									$participant_arr 	= json_encode($participant_arr);
									$this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");

									////------------ BUAT NGIRIM EVENT FIREBASE --------------//
						            $json_notif = "";
						            $id_user_emak = $this->session->userdata('id_user');
						            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$id_user_emak}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
						            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
						                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "30","title" => "Classmiles", "text" => "")));
						            }
						            if($json_notif != ""){
						                $headers = array(
						                    'Authorization: key=' . FIREBASE_API_KEY,
						                    'Content-Type: application/json'
						                    );
						                // Open connection
						                $ch = curl_init();

						                // Set the url, number of POST vars, POST data
						                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
						                curl_setopt($ch, CURLOPT_POST, true);
						                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
						                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

						                // Execute post
						                $result = curl_exec($ch);
						                curl_close($ch);
						                // echo $result;
						            }
						            ////------------ END -------------------------------------//
									
									$idadmin = $this->db->query("SELECT id_user, user_name FROM  tbl_user WHERE usertype_id ='admin'")->row_array();
									$adminid = $idadmin['id_user'];				
									$adminname = $idadmin['user_name'];	

									//NOTIF STUDENT
									$notif_message = json_encode( array("code" => 110));					
									$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','https://classmiles.com/Transaction/multicast_join/".$class_id."?t=user&user_utc=".$this->session->userdata('user_utc')."&view=detail','0')");

									//NOTIF ADMIN
									$notif_message = json_encode( array("code" => 111, "tutor_name" => $adminname));					
									$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','5','https://classmiles.com/admin/PaymentMulticast','0')");

									$order_id = $this->db->query("SELECT order_id FROM tbl_order_detail WHERE product_id='$lastrequestid'")->row_array()['order_id'];
					                // $this->session->set_flashdata('mes_alert','success');
					                // $this->session->set_flashdata('mes_display','block');
					                // $this->session->set_flashdata('mes_message', "Pesanan kelas telah ditambahkan ke keranjang, silahkan cek keranjang anda untuk melanjutkan ke pembayaran.");
					                redirect('/DetailOrder?order_id='.$order_id);
								}
								else
								{
									$this->session->set_flashdata('mes_alert','danger');
					                $this->session->set_flashdata('mes_display','block');
					                $this->session->set_flashdata('mes_message', 'Pembelian anda gagal');
					                redirect('/MyClass');
								}
							}
						}
					}
				}				
			}else{		
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message', 'Pembelian anda gagal');
				redirect('Kids/');	
			}
		}
		else if($t == "channel"){
			$id_user = $this->session->userdata('id_user');
			$qq = $this->db->query("SELECT tc.*, tcp.harga, tcp.harga_cm FROM tbl_class as tc LEFT JOIN tbl_class_price as tcp ON tc.class_id=tcp.class_id WHERE tc.class_id='$class_id' AND tc.participant NOT LIKE '%\"$id_user\":\"$id_user\"%'")->row_array();

			$participant_arr = json_decode($qq['participant'],true);
			if(!array_key_exists('participant', $participant_arr)){				
				$participant_arr['participant'] = array();
			}

			array_push($participant_arr['participant'], array("id_user" => $id_user));
			// echo json_encode($participant_arr);
			$participant_arr = json_encode($participant_arr);
			$this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
			
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message', 'Anda berhasil mengikuti kelas Channel Free');
			redirect('/MyClass');
		}
		else
		{	
			$id_user = $this->session->userdata('id_user_kids');
	        if ($id_user == null || $id_user=="") {
	            $id_user  = $this->session->userdata('id_user');
	        }
			$q = $this->db->query("SELECT tc.*, tcp.harga, tcp.harga_cm FROM tbl_class as tc LEFT JOIN tbl_class_price as tcp ON tc.class_id=tcp.class_id WHERE tc.class_id='$class_id' AND tc.participant NOT LIKE '%\"$id_user\":\"$id_user\"%'")->row_array();
			if(!empty($q)){
				$class_type = $q['class_type'];
				$maxLength 	= $q['max_participants'];
	            $tutor_id 	= $q['tutor_id'];
				// AND tc.class_type='multicast_paid'
				if($class_type == 'multicast_channel_paid' || $class_type == "multicast"){					
					$participant_arr 	= json_decode($q['participant'],true);
					$lengthParti 		= count($participant_arr['participant']);
					
					if ($lengthParti < $maxLength) {
						if(!array_key_exists('participant', $participant_arr)){						
							$participant_arr['participant'] = array();
						}
						array_push($participant_arr['participant'], array("id_user" => $id_user));
						// echo json_encode($participant_arr);
						$participant_arr 	= json_encode($participant_arr);
						$this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
						$this->db->simple_query("INSERT INTO tbl_class_rating(class_id, id_user, class_ack) VALUES('$class_id','$id_user',-1)");
						
						$namee = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$id_user'")->row_array();
						$this->session->set_flashdata('mes_alert','success');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message', "<label class='c-yellow'>".$namee['user_name']."</label>, Berhasil mengikuti kelas ".$q['name']." - ".$q['description']);
						redirect('/MyClass');	
					}
					else
					{
						$this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message', "<label class='c-white'>Maaf kelas sudah penuh!!!</label>");
						redirect('/MyClass');
					}								
				}else{	
					// $trxidd 			= $this->Rumus->getTrxIDPurchase();
					$user_utc 			= $this->session->userdata('user_utc');
					$datenow 			= date("Y-m-d h:i:s");
					$checknow 			= date("Y-m-d h:i:s");
					$server_utc 		= $this->Rumus->getGMTOffset();
					$interval 			= $user_utc - $server_utc;
					$datenow 			= DateTime::createFromFormat ('Y-m-d H:i:s',$datenow);		
					$datenow->modify("-".$interval ." minutes");
					$datenow 			= $datenow->format('Y-m-d H:i:s');
					$expired			= $q['start_time'];
					$namee 				= $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$id_user'")->row_array();

					$cekexpired			= $this->db->query("SELECT * FROM tbl_class WHERE '$checknow' <= last_order AND class_id='$class_id'")->row_array();
					// echo "SELECT * FROM tbl_class WHERE '$checknow' <= last_order AND class_id='$class_id'";
					// return false;
					if (empty($cekexpired)) {
						$this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message', "Expired");
						redirect('/MyClass');
					}
					else
					{	
						$invoicecheck 	= $this->db->query("SELECT * FROM tbl_order_detail as tod INNER JOIN tbl_request_multicast as trm ON tod.product_id=trm.request_id WHERE tod.class_id='$class_id' AND trm.id_requester='$id_user'")->row_array();
						// echo "SELECT * FROM tbl_order_detail as tod INNER JOIN tbl_order as tor ON tod.order_id=tor.order_id INNER JOIN tbl_invoice as ti ON ti.order_id=tod.order_id WHERE tod.class_id='$class_id' AND tor.buyer_id='$id_user'";
						// return false;
						if (!empty($invoicecheck)) {
							$od = $invoicecheck['order_id'];
							$now = date('Y-m-d h:i:s');
							$ckExpired = $this->db->query("SELECT * FROM tbl_order WHERE order_id = '$od' AND payment_due <= '$now' ")->row_array();
							if (empty($ckExpired)) {
								$this->session->set_flashdata('mes_alert','danger');
								$this->session->set_flashdata('mes_display','block');
								$this->session->set_flashdata('mes_message', 'Pembelian kelas ini sudah terdapat di invoice. segera lakukan pembayaran.<br>Details pembayaran dapat di lihat : <a style="color:#00e640;" href="'.BASE_URL().'/DetailOrder?order_id='.$od.'">Disini</a>');
								redirect('/MyClass');
							}
							else
							{
								//MASUKIN KE TBL_REQUEST_MULTICAST
								$request_id 	= $this->Rumus->get_new_request_id('multicast');
								$lastorder 		= $this->db->query("SELECT last_order FROM tbl_class WHERE class_id='$class_id'")->row_array()['last_order'];
								$add 			= $this->db->query("INSERT INTO tbl_request_multicast (request_id,class_id,id_requester,date_expired,status) VALUES ('$request_id','$class_id','$id_user','$lastorder','2')");
								$lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request_multicast')->row('request_id');

								$hargaakhir 		= $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id='$class_id'")->row_array()['harga_cm'];								

								//MASUK KE INVOICE
			                    $iptInvoice = $this->Process_model->createInvoice($lastrequestid,'multicast');			                    
			                    if ($iptInvoice == 1) {
			                    	$participantt  		= $this->db->query("SELECT participant FROM tbl_class WHERE class_id = '$class_id'")->row_array()['participant'];
			                    	$participant_arr 	= json_decode($participantt,true);					
									if(!array_key_exists('participant', $participant_arr)){						
										$participant_arr['participant'] = array();
									}
									array_push($participant_arr['participant'], array("id_user" => $id_user, "st" => 0));
									// echo json_encode($participant_arr);
									$participant_arr 	= json_encode($participant_arr);
									$this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");

									//SEND NOTIF
									$idadmin = $this->db->query("SELECT id_user, user_name FROM  tbl_user WHERE usertype_id ='admin'")->row_array();
									$adminid = $idadmin['id_user'];				
									$adminname = $idadmin['user_name'];	

									//NOTIF STUDENT
									$notif_message = json_encode( array("code" => 110));					
									$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','https://classmiles.com/Transaction/multicast_join/".$class_id."?t=user&user_utc=".$this->session->userdata('user_utc')."&view=detail','0')");

									//NOTIF ADMIN
									$notif_message = json_encode( array("code" => 111, "tutor_name" => $adminname));					
									$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','5','https://classmiles.com/admin/PaymentMulticast','0')");

									$order_id = $this->db->query("SELECT order_id FROM tbl_order_detail WHERE product_id='$lastrequestid'")->row_array()['order_id'];
									// $this->session->set_flashdata('mes_alert','success');
									// $this->session->set_flashdata('mes_display','block');
									// $this->session->set_flashdata('mes_message', "Pesanan kelas telah ditambahkan ke keranjang, silahkan cek keranjang anda untuk melanjutkan ke pembayaran.");
									redirect('/DetailOrder?order_id='.$order_id);	
								}
								else{
									$this->session->set_flashdata('mes_alert','danger');
									$this->session->set_flashdata('mes_display','block');
									$this->session->set_flashdata('mes_message', 'Pembelian anda gagal');
									redirect('/MyClass');
								}
							}
						}
						else
						{														
							if (isset($_GET['view'])) {
								$lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request_multicast')->row('request_id');
								$data['trx_id']		= $lastrequestid;
								$data['classid'] 	= $class_id;
								$data['user_utc'] 	= $user_utc;
								$data['sideactive'] = "home";
								$this->load->view('inc/header');
								$this->load->view('class_detailpay',$data);
							}
							else
							{							
								//MASUKIN KE TBL_REQUEST_MULTICAST
								$request_id 	= $this->Rumus->get_new_request_id('multicast');
								$lastorder 		= $this->db->query("SELECT last_order FROM tbl_class WHERE class_id='$class_id'")->row_array()['last_order'];
								$add 			= $this->db->query("INSERT INTO tbl_request_multicast (request_id,class_id,id_requester,date_expired,status) VALUES ('$request_id','$class_id','$id_user','$lastorder','2')");
								$lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request_multicast')->row('request_id');

								$hargaakhir 		= $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id='$class_id'")->row_array()['harga_cm'];								

								//MASUK KE INVOICE
			                    $iptInvoice = $this->Process_model->createInvoice($lastrequestid,'multicast');			                    
			                    if ($iptInvoice == 1) {
			                    	$participantt  		= $this->db->query("SELECT participant FROM tbl_class WHERE class_id = '$class_id'")->row_array()['participant'];
			                    	$participant_arr 	= json_decode($participantt,true);					
									if(!array_key_exists('participant', $participant_arr)){						
										$participant_arr['participant'] = array();
									}
									array_push($participant_arr['participant'], array("id_user" => $id_user, "st" => 0));
									// echo json_encode($participant_arr);
									$participant_arr 	= json_encode($participant_arr);
									$this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");

									//SEND NOTIF
									$idadmin = $this->db->query("SELECT id_user, user_name FROM  tbl_user WHERE usertype_id ='admin'")->row_array();
									$adminid = $idadmin['id_user'];				
									$adminname = $idadmin['user_name'];	

									//NOTIF STUDENT
									$notif_message = json_encode( array("code" => 110));					
									$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','https://classmiles.com/Transaction/multicast_join/".$class_id."?t=user&user_utc=".$this->session->userdata('user_utc')."&view=detail','0')");

									//NOTIF ADMIN
									$notif_message = json_encode( array("code" => 111, "tutor_name" => $adminname));					
									$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','5','https://classmiles.com/admin/PaymentMulticast','0')");

									$order_id = $this->db->query("SELECT order_id FROM tbl_order_detail WHERE product_id='$lastrequestid'")->row_array()['order_id'];
									// $this->session->set_flashdata('mes_alert','success');
									// $this->session->set_flashdata('mes_display','block');
									// $this->session->set_flashdata('mes_message', "Pesanan kelas telah ditambahkan ke keranjang, silahkan cek keranjang anda untuk melanjutkan ke pembayaran.");
									redirect('/DetailOrder?order_id='.$order_id);	
								}
								else{
									$this->session->set_flashdata('mes_alert','danger');
									$this->session->set_flashdata('mes_display','block');
									$this->session->set_flashdata('mes_message', 'Pembelian anda gagal');
									redirect('/MyClass');
								}						
							}
						}
					}										
				}				
			}else{		
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message', 'Pembelian anda gagal');
				redirect('/MyClass');			
			}
		}
	}

	
}
