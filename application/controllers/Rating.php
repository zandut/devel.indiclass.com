<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rating extends CI_Controller {

	public $sesi = array("lang" => "indonesia");

	public function __construct()
	{
		parent::__construct();
        // $this->load_lang($this->session->userdata('lang'));
        // $this->load->library('recaptcha');
		$this->session->set_userdata('color','blue'); 
		// $this->load->library('email');
		// $this->load->library('fpdf');
        // $this->load->helper(array('Form', 'Cookie', 'String'));
         $this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
		// Check for any mobile device.
		if ($detect->isMobile()){
			define('MOBILE', 'mobile/');
		}else{
			define('MOBILE', '');
		}
	}

	public function polling_unrate()
	{
		$id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : $this->session->userdata('id_user');

		$new_arr = array();
        if($id_user){        	
        	$data = $this->db->query("SELECT tcr.id_user, tcr.class_ack, tc.*, tu.user_name, tu.user_image, ms.subject_name, ms.icon as subject_icon, mj.jenjang_name, mj.jenjang_level FROM tbl_class_rating as tcr INNER JOIN ( tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user LEFT JOIN ( master_subject as ms LEFT JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id ) ON tc.subject_id=ms.subject_id ) ON tcr.class_id=tc.class_id WHERE tc.finish_time <= now() AND ( (tcr.class_ack=1 && tcr.class_rate=0) OR tcr.class_ack=-1) AND tcr.id_user='$id_user'")->result_array();
			foreach ($data as $key => $value) {
				$user_utc = $this->input->post('user_utc');        
		        if ($user_utc != null) {
					$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		        } else {
		            $user_utc = $this->session->userdata('user_utc');
		        }
				$server_utcc        = $this->Rumus->getGMTOffset();
                $interval          	= $user_utc - $server_utcc;
                $start_time   		= DateTime::createFromFormat ('Y-m-d H:i:s',$value['start_time']);
                $start_time->modify("+".$interval ." minutes");
                $end_time   		= DateTime::createFromFormat ('Y-m-d H:i:s',$value['finish_time']);
                $end_time->modify("+".$interval ." minutes");

                
				$hari 				= $start_time->format('d F Y H:i');
                $tanggal 			= $start_time->format('Y-m-d H:i:s');
                $tanggals 			= $end_time->format('Y-m-d H:i:s');

				$new_arr[$key] 		= $data[$key];
				$new_arr[$key]['user_image']  = CDN_URL.USER_IMAGE_CDN_URL.$value['user_image'];
				$new_arr[$key]['starttime']	 = $tanggal;
				$new_arr[$key]['date_class'] = $hari;
				$new_arr[$key]['start_time'] 	= $tanggal;
				$new_arr[$key]['finish_time'] 	= $tanggals;
				$new_arr[$key]['user_utc'] = $user_utc;
			}
		}
		echo json_encode($new_arr);
		return false;
	}

	public function polling_rated($value='')
	{
		$id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : $this->session->userdata('id_user');
		
		$new_arr = array();
        if($id_user){        	
        	$data = $this->db->query("SELECT tcr.*, tc.*, tu.user_name, tu.user_image, ms.subject_name, ms.icon as subject_icon, mj.jenjang_name, mj.jenjang_level FROM tbl_class_rating as tcr INNER JOIN ( tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user LEFT JOIN ( master_subject as ms LEFT JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id ) ON tc.subject_id=ms.subject_id ) ON tcr.class_id=tc.class_id WHERE tcr.class_ack=1 && tcr.class_rate!=0 AND tcr.id_user='$id_user'")->result_array();
			foreach ($data as $key => $value) {
				$user_utc = $this->input->post('user_utc');        
		        if ($user_utc==null) {
		            $user_utc = $this->session->userdata('user_utc');
		        }
				$server_utcc        = $this->Rumus->getGMTOffset();
                $interval          = $user_utc - $server_utcc;
                $start_time   		= DateTime::createFromFormat ('Y-m-d H:i:s',$value['start_time']);
                $start_time->modify("+".$interval ." minutes");
                
				$hari 				= $start_time->format('d F Y H:i');
                $tanggal 			= $start_time->format('Y-m-d H:i:s');

				$new_arr[$key] 		= $data[$key];
				$new_arr[$key]['user_image']  = CDN_URL.USER_IMAGE_CDN_URL.$value['user_image'];
				$new_arr[$key]['starttime']	 = $tanggal;
				$new_arr[$key]['date_class'] = $hari;
			}
		}
		echo json_encode($new_arr);
		return false;
	}
	public function polling_resolution($usertype='')
	{
		$id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : $this->session->userdata('id_user');
		$uwhere = $usertype == 'tutor' ? " tc.tutor_id='$id_user' " : " tcr.id_user='$id_user' ";

		$new_arr = array();
        if($id_user){        	
        	$data = $this->db->query("SELECT tcr.class_id, tcr.id_user, tcr.resolution_status, tcr.last_update, tcr.created_at, tc.start_time, tc.description, tu.user_name, tu.user_image, ms.subject_name, ms.icon as subject_icon, mj.jenjang_name, mj.jenjang_level FROM tbl_class_resolution as tcr INNER JOIN ( tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user LEFT JOIN ( master_subject as ms LEFT JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id ) ON tc.subject_id=ms.subject_id ) ON tcr.class_id=tc.class_id WHERE $uwhere ")->result_array();
			foreach ($data as $key => $value) {
				$user_utc = $this->input->post('user_utc');        
		        if ($user_utc==null) {
		            $user_utc = $this->session->userdata('user_utc');
		        } 
				$server_utcc        = $this->Rumus->getGMTOffset();
                $interval          = $user_utc - $server_utcc;
                $last_update   		= DateTime::createFromFormat ('Y-m-d H:i:s',$value['last_update']);
                $last_update->modify("+".$interval ." minutes");
                
				$hari 				= $last_update->format('d F Y H:i:s');
                $tanggal 			= $last_update->format('Y-m-d H:i:s');

				$new_arr[$key] 		= $data[$key];
				$new_arr[$key]['user_image']  = CDN_URL.USER_IMAGE_CDN_URL.$value['user_image'];
				// $new_arr[$key]['resolution_text'] = json_decode($value['resolution_text'], TRUE);
				$new_arr[$key]['last_update'] = $hari;
			}
		}
		echo json_encode($new_arr);
		/*echo '<pre>';
		print_r($new_arr);
		echo '</pre>';*/
		return false;
	}
	public function get_resolution($value='')
	{
		$class_id 	= $this->input->post('class_id');
		$id_user 	= $this->input->post('id_user');
		// $uwhere = $usertype == 'tutor' ? " tc.tutor_id='$id_user' " : " tcr.id_user='$id_user' ";

        if($id_user){
        	$data = $this->db->query("SELECT tcr.*, tc.start_time, tc.description, tu.user_name, tu.user_image, ms.subject_name, ms.icon as subject_icon, mj.jenjang_name, mj.jenjang_level FROM tbl_class_resolution as tcr INNER JOIN ( tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user LEFT JOIN ( master_subject as ms LEFT JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id ) ON tc.subject_id=ms.subject_id ) ON tcr.class_id=tc.class_id WHERE tcr.class_id='$class_id' AND tcr.id_user='$id_user' ")->row_array();
			$user_utc = $this->input->post('user_utc');   
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);     
	        if ($user_utc==null) {
	            $user_utc = $this->session->userdata('user_utc');
	        }
			$server_utcc        = $this->Rumus->getGMTOffset();
            $interval           = $user_utc - $server_utcc;
            $last_update   		= DateTime::createFromFormat ('Y-m-d H:i:s',$data['last_update']);
            $last_update->modify("+".$interval ." minutes");
            
			$hari 				= $last_update->format('d F Y H:i:s');
            $tanggal 			= $last_update->format('Y-m-d H:i:s');

			$data['resolution_text'] = json_decode($data['resolution_text'], TRUE);
			foreach ($data['resolution_text'] as $key => $value) {
				$wt = $value['writer_time'] != null ? DateTime::createFromFormat ('Y-m-d H:i:s',$value['writer_time']) : '1990-01-01 00:00:00';
				$wt->modify("+$interval minutes");
				$data['resolution_text'][$key]['writer_time'] = $wt->format('d/m/Y H:i:s');
			}
			$data['last_update'] = $hari;
		}
		echo json_encode($data);
		/*echo '<pre>';
		print_r($new_arr);
		echo '</pre>';*/
		return false;
	}
	public function submit_ack()
	{
		$class_id 	= $this->input->post('class_id');
		$id_user 	= $this->input->post('id_user');
		$class_ack 	= $this->input->post('class_ack');

		$updt 		= $this->db->simple_query("UPDATE tbl_class_rating SET class_ack=1 WHERE class_id='$class_id' AND id_user='$id_user'");		
		if ($updt) {
			$getdata = $this->db->query("SELECT tu.id_user, tu.user_name, tu.user_image FROM tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user WHERE tc.class_id='$class_id'")->row_array();
			$tutor_id 	= $getdata['id_user'];
			$tutor_name = $getdata['user_name'];
			$user_image = $getdata['user_image'];
			$json = array("status" => 1,"message" => "Sukses", "tutor_id" => $tutor_id, "tutor_name" => $tutor_name, "tutor_image" => $user_image);
		}
		else
		{
			$json = array("status" => -1,"message" => "Gagal");
		}
		echo json_encode($json);
	}

	public function reject_ack()
	{
		$class_id 	= $this->input->post('class_id');
		$id_user 	= $this->input->post('id_user');
		$class_ack 	= $this->input->post('class_ack');

		$rjct 		= $this->db->simple_query("UPDATE tbl_class_rating SET class_ack=0 WHERE class_id='$class_id' AND id_user='$id_user'");		
		if ($rjct) {
			$this->db->simple_query("INSERT INTO tbl_class_resolution(class_id, id_user, resolution_text, last_update) VALUES('$class_id','$id_user', '[]', now())");
			$getdata = $this->db->query("SELECT tu.user_name, tc.name, tc.description FROM tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user WHERE tc.class_id='$class_id'")->row_array();
			$user_name 	 = $getdata['user_name'];
			$name 		 = $getdata['name'];
			$description = $getdata['description'];
			$json = array("status" => 1,"message" => "Sukses", "tutor_name" => $user_name, "subject_name" => $name, "description" => $description);
		}
		else
		{
			$json = array("status" => -1,"message" => "Gagal");
		}
		echo json_encode($json);
	}

	public function complain_solved($value='')
	{
		$class_id 	= $this->input->post('class_id');
		$id_user 	= $this->input->post('id_user');

		$solved		= $this->db->simple_query("UPDATE tbl_class_resolution SET resolution_status=1, last_update=now() WHERE class_id='$class_id' AND id_user='$id_user'");		
		if ($solved) {
			$json = array("status" => 1,"message" => "Sukses");
		}
		else
		{
			$json = array("status" => -1,"message" => "Gagal");
		}
		echo json_encode($json);
	}

	public function submit_rate()
	{
		$class_id 		= $this->input->post('class_id');
		$id_user 		= $this->input->post('id_user');
		$class_ack 		= $this->input->post('class_ack');
		$class_rate 	= $this->input->post('class_rate');
		$class_review 	= $this->input->post('class_review');
		$class_review2 	= $this->input->post('class_review2');
		$class_review2	= json_encode($class_review2);		
		// array("gj" => 0, "tmm" => 0, "tm" => 0);

		$updt = $this->db->simple_query("UPDATE tbl_class_rating SET class_rate='$class_rate', class_review='$class_review', class_review2='$class_review2' WHERE class_id='$class_id' AND id_user='$id_user'");
		if($updt) {			
			$json = array("status" => 1,"message" => "Sukses");
		}
		else
		{
			$json = array("status" => -1,"message" => "Gagal");
		}
		echo json_encode($json);
	}
	public function submit_resolution()
	{
		$class_id 		= $this->input->post('class_id');
		$id_user 		= $this->input->post('id_user');
		$writer_id		= $this->input->post('writer_id'); // ISI ID USER YANG NULIS
		$writer_text		= $this->input->post('writer_text');
		// array("gj" => 0, "tmm" => 0, "tm" => 0);

		$data = $this->db->query("SELECT * FROM tbl_class_resolution WHERE class_id='$class_id' AND id_user='$id_user'")->row_array();
		if(!empty($data)){
			$resolution_text = array();
			if($data['resolution_text'] != null) {
				$resolution_text = json_decode($data['resolution_text'], TRUE);
			}
			$writer_info = $this->db->query("SELECT first_name, user_image FROM tbl_user WHERE id_user='$writer_id'")->row_array();
			$last_message = array("writer_id" => $writer_id, "writer_name" => $writer_info['first_name'], "writer_text" => $writer_text, "writer_time" => date('Y-m-d H:i:s'), "writer_image" => CDN_URL.USER_IMAGE_CDN_URL.$writer_info['user_image']);
			$resolution_text[] = $last_message;

			$resolution_text = json_encode($this->db->escape_str($resolution_text));
			$updt = $this->db->simple_query("UPDATE tbl_class_resolution SET resolution_text='$resolution_text', last_update=now() WHERE class_id='$class_id' AND id_user='$id_user'");
			if($updt) {
				$dclass 		= $this->db->query("SELECT tutor_id, name FROM tbl_class WHERE class_id='$class_id'")->row_array();
				$tutor_id		= $dclass['tutor_id'];
				//NOTIF TUTOR
				$notif_message = json_encode( array("code" => 216, "class_name" => $dclass['name'], "student_name" => $this->session->userdata('user_name')));					
				$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$tutor_id', '".base_url()."Tutor/Complain','0')");
				
				$wt = $last_message['writer_time'] != null ? DateTime::createFromFormat ('Y-m-d H:i:s',$last_message['writer_time']) : '1990-01-01 00:00:00';
				$last_message['writer_time'] = $wt->format('d/m/Y H:i:s');
				$json = array("status" => 1,"message" => "Sukses", "data" => $last_message);
			}
			else
			{
				$json = array("status" => -1,"message" => "Gagal");
			}
		}else{
			$json = array("status" => -1,"message" => "Gagal, Data tidak ditemukan");
		}
		echo json_encode($json);
	}

}