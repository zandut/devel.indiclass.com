<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Operator extends CI_Controller {

	public $sesi = array("lang" => "indonesia");	

	public function __construct()
	{
		parent::__construct();
        $this->load_lang($this->session->userdata('lang'));        
        $this->load->database();
		$this->load->helper(array('url'));   
		$this->load->library('email');    
		// $this->load->model('Master_model'); 
	}

	public function index($page = 0)
	{
		if ($this->session_check() == 1) {	
			if($this->session->flashdata('rets')){
				$data['rets'] = $this->session->flashdata('rets');
			}
			$data['sideactive'] = "home";
			$this->load->view('inc/header');
			$this->load->view('operator/index', $data);
			return null;
		}
		redirect('/Operator');
	}

	public function DataTransaction()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "datauangsaku";
			$this->load->view('inc/header');
			$this->load->view('operator/data_uangsaku', $data);
		}
		else
		{
			redirect('/Operator');
		}
	}

	public function ConfrimTransaction()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "confrimuangsaku";
			$this->load->view('inc/header');
			$this->load->view('operator/confrim_uangsaku', $data);
		}
		else
		{
			redirect('/Operator');
		}
	}

	public function ConfrimOnDemand()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "confrimondemand";
			$this->load->view('inc/header');
			$this->load->view('operator/confrim_ondemand', $data);
		}
		else
		{
			redirect('/Operator');
		}
	}

	public function ConfrimMulticast()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "confrimmulticast";
			$this->load->view('inc/header');
			$this->load->view('operator/confrim_multicast', $data);
		}
		else
		{
			redirect('/Operator');
		}
	}

	public function ConfrimPending()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "confrimpending";
			$this->load->view('inc/header');
			$this->load->view('operator/confrim_pending', $data);
		}
		else
		{
			redirect('/Operator');
		}
	}

	public function ListInvoice()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "listinvoice";
			$this->load->view('inc/header');
			$this->load->view('operator/list_invoice', $data);
		}
		else
		{
			redirect('/Operator');
		}
	}
	public function ListPackageInvoice()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "listpackageinvoice";
			$this->load->view('inc/header');
			$this->load->view('operator/list_package_invoice', $data);
		}
		else
		{
			redirect('/Operator');
		}
	}

	public function InvoiceExpired()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "invoiceexpired";
			$this->load->view('inc/header');
			$this->load->view('operator/list_invoice_expired', $data);
		}
		else
		{
			redirect('/Operator');
		}
	}

	public function ConfrimationPayment()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "confrimationpayment";
			$this->load->view('inc/header');
			$this->load->view('operator/confrimation_payment', $data);
		}
		else
		{
			redirect('/Operator');
		}
	}

	public function ListTutor()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "alltutor";
			$data['profile'] = $this->Master_model->getTblUserAndProfileTutor();
			$data['alldatatutor'] = $this->Master_model->getAllTutor();
			$this->load->view('inc/header');
			$this->load->view('operator/alltutor', $data);
		}
		else
		{
			redirect('/Operator');
		}
	}

	public function paymentTutor()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "listpaymenttutor";
			// $data['alllistpayment'] = $this->Master_model->getListTutorPayment();
			$this->load->view('inc/header');
			$this->load->view('operator/payment_tutor', $data);
		}
		else
		{
			redirect('/Operator');
		}
	}


	public function OperatorAction($value='')
	{
		$bank_id = $this->input->post('bi');
		$date_field = $this->input->post('df');
		$description_field = $this->input->post('def');
		$credit_amount = $this->input->post('ca');
		$balance_after_credit = $this->input->post('bac');
		$invoice_id = $this->input->post('invoice_id');
		$aksi = $this->input->post('aksi');
		$ket = $this->input->post('ket');

		// SET LOG MUTASI TO HAVE DONE PROCESSING
		$this->db->simple_query("UPDATE log_mutasi_bank SET status=1 WHERE bank_id='$bank_id' && date_field='$date_field' && description_field='$description_field' && balance_after_credit='$balance_after_credit'");
		// PROCEED THE ORDER ITSELF
		if($aksi == '1'){

			// BAYAR LUNAS
			$data_invoice = $this->db->query("SELECT * FROM tbl_invoice WHERE invoice_id='$invoice_id' && underpayment > 0")->row_array();
			if(!empty($data_invoice)){
				$order_id = $data_invoice['order_id'];

				$underpayment = $data_invoice['underpayment'];
				if($credit_amount > $underpayment){
					$change = $credit_amount-$underpayment;
					$uangsaku_id = $this->db->query("SELECT us.us_id FROM tbl_order as tord INNER JOIN ( tbl_user as tu INNER JOIN uangsaku as us ON tu.id_user=us.id_user ) ON tord.buyer_id=tu.id_user WHERE tord.order_id='$order_id' ")->row_array();
					if(!empty($uangsaku_id)){
						$uangsaku_id = $uangsaku_id['us_id'];
						$this->db->simple_query("UPDATE uangsaku SET active_balance=active_balance+$change WHERE us_id='$uangsaku_id'");
					}
				}

				$this->db->simple_query("UPDATE tbl_invoice SET underpayment=0 WHERE invoice_id='$invoice_id'");
				$o_detail = $this->db->query("SELECT tod.order_id, tod.product_id as rtp, tod.type, tr.id_user_requester as tr_id_user, tr.tutor_id as tr_tutor_id, trg.id_user_requester as trg_id_user, trp.id_requester as trp_id_user, trg.tutor_id as trg_tutor_id, tc.tutor_id as trm_tutor_id, trm.id_requester as trm_id_user, tc.class_id FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id LEFT JOIN  tbl_request_program as trp ON trp.request_id=tod.product_id  WHERE tod.order_id='$order_id'")->result_array();
				$newarr = array();
				foreach ($o_detail as $key => $value) {
					$tutor_id       = $value['tr_tutor_id'] != null ? $value['tr_tutor_id'] : ( $value['trg_tutor_id'] != null ? $value['trg_tutor_id'] : ( $value['trm_tutor_id'] != null ? $value['trm_tutor_id'] : ''));
					$id_user       = $value['tr_id_user'] != null ? $value['tr_id_user'] : ( $value['trg_id_user'] != null ? $value['trg_id_user'] : ( $value['trm_id_user'] != null ? $value['trg_id_user'] : $value['trp_id_user']));
					$newarr[] = array("trx_id" => $value['order_id'], 'type' => $value['type'], 'id_user' => $id_user, 'tutor_id' => $tutor_id, 'class_id' => $value['class_id'], 'rtp' => $value['rtp']);

				}

				$buyer_id 	= $this->db->query("SELECT buyer_id FROM tbl_order WHERE order_id='$order_id'")->row_array()['buyer_id'];
				$getdataa 	= $this->db->query("SELECT user_name, email, usertype_id FROM tbl_user WHERE id_user='$buyer_id'")->row_array();
				$usertype_id= $getdataa['usertype_id'];
				if ($usertype_id == 'student kid') {
					$getParent  = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$buyer_id'")->row_array()['parent_id'];
					$getdata 	= $this->db->query("SELECT user_name, email, usertype_id FROM tbl_user WHERE id_user='$getParent'")->row_array();
					$email 		= $getdata['email'];
	                $user_name 	= $getdata['user_name'];
				}
				else
				{
					$email 		= $getdataa['email'];
	                $user_name 	= $getdataa['user_name'];
                }
                $isi 		= 'Terima kasih telah melakukan pembayaran di classmiles, Pembayaran anda telah berhasil kami terima';
                $isi_ing 	= 'Thank you for making payment in classmiles, our payment has been successfully received';
				// Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_paymentreceived','content' => array("emailuser" => "$email", "usernameuser" => "$user_name", "hargakelas" => "$credit_amount") )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;		

				echo json_encode(array("status" => true, "aksi" => 1, "message" => "found", "data" => $newarr));
				/*foreach ($o_detail as $key => $value) {
					$type = $value['type'];
					if($type == 'multicast'){

					}
				}*/
			}else{
				echo json_encode(array("status" => false, "aksi" => 1, "message" => "data not found"));
			}
		}else if($aksi == '2'){
			// KURANG BAYAR
			$data_invoice = $this->db->query("SELECT * FROM tbl_invoice WHERE invoice_id='$invoice_id' && underpayment > 0")->row_array();
			if(!empty($data_invoice)){
				$order_id = $data_invoice['order_id'];

				$underpayment = $data_invoice['underpayment'];
				if($credit_amount >= $underpayment){
					$change = $credit_amount-$underpayment;
					$uangsaku_id = $this->db->query("SELECT us.us_id FROM tbl_order as tord INNER JOIN ( tbl_user as tu INNER JOIN uangsaku as us ON tu.id_user=us.id_user ) ON tord.buyer_id=tu.id_user WHERE tord.order_id='$order_id' ")->row_array();
					if(!empty($uangsaku_id)){
						$uangsaku_id = $uangsaku_id['us_id'];
						$this->db->simple_query("UPDATE uangsaku SET active_balance=active_balance+$change WHERE us_id='$uangsaku_id'");
					}
					$underpayment = 0;
					$aksi = 1;
				}else{
					$underpayment -= intval($credit_amount);
					$aksi = 2;
				}

				$this->db->simple_query("UPDATE tbl_invoice SET underpayment=0 WHERE invoice_id='$invoice_id'");
				$o_detail = $this->db->query("SELECT tod.order_id, tod.product_id as rtp, tod.type, tr.id_user_requester as tr_id_user, tr.tutor_id as tr_tutor_id, trg.id_user_requester as trg_id_user, trp.id_requester as trp_id_user, trg.tutor_id as trg_tutor_id, tc.tutor_id as trm_tutor_id, trm.id_requester as trm_id_user, tc.class_id FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id LEFT JOIN  tbl_request_program as trp ON trp.request_id=tod.product_id  WHERE tod.order_id='$order_id'")->result_array();
				$newarr = array();
				foreach ($o_detail as $key => $value) {
					$tutor_id       = $value['tr_tutor_id'] != null ? $value['tr_tutor_id'] : ( $value['trg_tutor_id'] != null ? $value['trg_tutor_id'] : $value['trm_tutor_id']);
					$id_user       = $value['tr_id_user'] != null ? $value['tr_id_user'] : ( $value['trg_id_user'] != null ? $value['trg_id_user'] : $value['trm_id_user']);
					$newarr[] = array("trx_id" => $value['order_id'], 'type' => $value['type'], 'id_user' => $id_user, 'tutor_id' => $tutor_id, 'class_id' => $value['class_id'], 'rtp' => $value['rtp']);

				}
				echo json_encode(array("status" => true, "aksi" => $aksi, "message" => "found", "data" => $newarr));
				/*foreach ($o_detail as $key => $value) {
					$type = $value['type'];
					if($type == 'multicast'){

					}
				}*/


			}else{
				echo json_encode(array("status" => false, "aksi" => 2, "message" => "data not found"));
			}
		}else if($aksi == '3'){
			// DITOLAK KARENA BAYAR NYA TELAT JADI UANG DI KEMBALIKAN KE UANGSAKU
			$data_invoice = $this->db->query("SELECT * FROM tbl_invoice WHERE invoice_id='$invoice_id'")->row_array();
			if(!empty($data_invoice)){
				$order_id = $data_invoice['order_id'];

				$change = intval($credit_amount);
				$uangsaku_id = $this->db->query("SELECT us.us_id FROM tbl_order as tord INNER JOIN ( tbl_user as tu INNER JOIN uangsaku as us ON tu.id_user=us.id_user ) ON tord.buyer_id=tu.id_user WHERE tord.order_id='$order_id' ")->row_array();
				if(!empty($uangsaku_id)){
					$uangsaku_id = $uangsaku_id['us_id'];
					$this->db->simple_query("UPDATE uangsaku SET active_balance=active_balance+$change WHERE us_id='$uangsaku_id'");
				}

				$o_detail = $this->db->query("SELECT tod.order_id, tod.product_id as rtp, tod.type, tr.id_user_requester as tr_id_user, tr.tutor_id as tr_tutor_id, trg.id_user_requester as trg_id_user, trp.id_requester as trp_id_user, trg.tutor_id as trg_tutor_id, tc.tutor_id as trm_tutor_id, trm.id_requester as trm_id_user, tc.class_id FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id LEFT JOIN  tbl_request_program as trp ON trp.request_id=tod.product_id  WHERE tod.order_id='$order_id'")->result_array();
				$newarr = array();
				foreach ($o_detail as $key => $value) {
					$tutor_id       = $value['tr_tutor_id'] != null ? $value['tr_tutor_id'] : ( $value['trg_tutor_id'] != null ? $value['trg_tutor_id'] : $value['trm_tutor_id']);
					$id_user       = $value['tr_id_user'] != null ? $value['tr_id_user'] : ( $value['trg_id_user'] != null ? $value['trg_id_user'] : $value['trm_id_user']);
					$newarr[] = array("trx_id" => $value['order_id'], 'type' => $value['type'], 'id_user' => $id_user, 'tutor_id' => $tutor_id, 'class_id' => $value['class_id'], 'rtp' => $value['rtp']);

				}
				$this->Process_model->payment_return($newarr);
				echo json_encode(array("status" => true, "aksi" => $aksi, "message" => "found", "data" => $newarr));
				/*foreach ($o_detail as $key => $value) {
					$type = $value['type'];
					if($type == 'multicast'){

					}
				}*/


			}else{
				echo json_encode(array("status" => false, "aksi" => 3, "message" => "data not found"));
			}
		}
	}

	public function OperatorUnderPayment()
	{
		
	}
	
	public function load_lang($lang = '')
	{
		$this->lang->load('umum',$lang);
	}

	public function session_check()
	{
		if($this->session->has_userdata('id_user')){
			$new_state = $this->db->query("SELECT status FROM tbl_user where id_user='".$this->session->userdata('id_user')."'")->row_array()['status'];
			if($this->session->userdata('status') != $new_state ){
				$this->session->sess_destroy();
				redirect('/login');
			}
			// $this->sesi['user_name'] = $this->session->userdata('user_name');
			// $this->sesi['username'] = $this->session->userdata('username');
			// $this->sesi['priv_level'] = $this->session->userdata('priv_level');
			if($this->session->userdata('usertype_id') != "moderator"){
				redirect('/');
			}
			return 1;
		}else{
			return 0;
		}
	}
	

}
