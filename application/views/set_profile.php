<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">

        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <!-- <div id="alert_complete_data" class="alert alert-info" style="">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->lang->line('alertcompletedata'); ?>
            </div> -->
            <?php if(isset($mes_alert)){ ?>
                <div class="alert alert-<?php echo $mes_alert; ?>" style="display: <?php echo $mes_display; ?>">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?php echo $mes_message; ?>
                </div>
                <?php } ?>
            <div class="block-header">
                <h2><?php echo $this->lang->line('profil'); ?></h2>

                <!-- <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_about'); ?></li>
                        </ol>
                    </li>
                </ul> -->
            </div><!-- akhir block header -->            
            <div class="bs-item z-depth-5" style="margin-top: 5%;">
                <div class="card" id="profile-main">

                    <?php
                        $this->load->view('inc/sideprofile');
                    ?>

                    <div class="pm-body clearfix">
                       
                        <ul class="tab-nav tn-justified" role="tablist">
                        	<?php
                        	$status_user = $this->session->userdata('status_user');
                        	if ($status_user =="umum") {
                        	?>
	                            <li class="active waves-effect"><a href="<?php echo base_url('Profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li> 
                        	<?php
                        	}
                        	else{
							?>
	                            <li class="active waves-effect"><a href="<?php echo base_url('Profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li> 
	                            <li id="tab_school" class="waves-effect"><a href="<?php echo base_url('Profile-School'); ?>"><?php echo $this->lang->line('tab_ps'); ?></a></li>
                        	<?php
                        	}
                        	?>
                            <!-- <li class="waves-effect"><a href="<?php echo base_url('first/profile_forgot'); ?>"><?php echo $this->lang->line('tab_cp'); ?></a></li> -->                                      
                        </ul>   
                        <div class="card-padding card-body">
                            <div class="pmbb-header">
                                <button title="Edit Profile" id="btn_edit_username" class="btn btn-info btn-icon pull-right"><i class="zmdi zmdi-edit"></i></button>
                                <button title="Back to Profile" style="display: none;" id="btn_save_username" class="btn btn-info btn-icon pull-right"><i class="zmdi zmdi-check"></i></button>
                            </div>
                            <blockquote class="m-b-25">
                                <p><?php echo $this->lang->line('tab_account'); ?></p>
                            </blockquote> 
                         	<div class=" modal fade" data-modal-color="blue" id="complete_modal" tabindex="-1" data-backdrop="static" role="dialog">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content" style="margin-top: 75%;" >
                                        <div class="modal-header">
                                            <h4 class="modal-title"><?php echo $this->lang->line('completeprofiletitle');?></h4>
                                        </div>
                                            <center>
                                                <div id='modal_approv' class="modal-body" >
                                                    <div class="alert alert-info" style="display: block; ?>" id="alertcompletedata">
                                                        <?php echo $this->lang->line('alertcompletedata');?>
                                                    </div>
                                                </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="edit_profile" style="display: none;">
								<form action="<?php echo base_url('/master/saveEditProfileStudent') ?>" method="post">                            
	                                <input type="hidden" name="id_user" required class="input-sm form-control fg-input" value="<?php if(isset($alluserprofile)){ echo $alluserprofile['id_user']; } ?>">
	                                <div class="input-group fg-float">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-city"></i></span>
	                                    <div class="fg-line">
	                                        <input id="tempat_lahir" type="text" name="user_birthplace" class="input-sm form-control fg-input" value="<?php if(isset($alluserprofile)){ echo $alluserprofile['user_birthplace']; } ?>">
	                                        <label class="fg-label"><?php echo $this->lang->line('placeofbirth'); ?></label>
	                                    </div>                                    
	                                    <small id="cek_tempat_lahir" style="color: red;"></small>
	                                </div>

	                                <!-- <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-male-female"></i></span>
	                                    <?php 
	                                        //$flag = null;
	                                    	if (isset($alluserprofile)) {
	                                    		$flag = $alluserprofile['user_gender'];
	                                    	}
	                                    ?>
	                                        <label class="fg-label"><small><?php echo $this->lang->line('gender'); ?></small></label>
	                                        <select name="user_gender" class="select2 form-control">
	                                            <option disabled><?php echo $this->lang->line('select_gender'); ?></option>
	                                            <option value="Male" <?php if($flag=="Male"){ echo "selected='true'";} ?> ><?php echo $this->lang->line('male');?></option>
	                                            <option value="Female" <?php if($flag=="Female"){ echo "selected='true'";} ?>><?php echo $this->lang->line('women');?></option>
	                                        </select>                                        
	                                </div> -->


	                                <br/>  
	                                <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-library"></i></span>
	                                    <!-- <div class="select"> -->
	                                     <?php 
	                                        if (isset($alluserprofile)) {
	                                            $flag = $alluserprofile['user_religion'];
	                                        }
	                                    ?>
	                                        <label class="fg-label"><small><?php echo $this->lang->line('religion'); ?></small></label>
	                                        <select name="user_religion" class="select2 form-control" style="width: 100%">
	                                            <option disabled><?php echo $this->lang->line('select_religion'); ?></option>
	                                            <option value="Islam" <?php if($flag=="Islam"){ echo "selected='true'";} ?>>Islam</option>
	                                            <option value="Kristen Protestan" <?php if($flag=="Kristen Protestan"){ echo "selected='true'";} ?>>Kristen Protestan</option>
	                                            <option value="Kristen Katolik" <?php if($flag=="Kristen Katolik"){ echo "selected='true'";} ?>>Kristen Katolik</option>
	                                            <option value="Hindu" <?php if($flag=="Hindu"){ echo "selected='true'";} ?>>Hindu</option>
	                                            <option value="Buddha" <?php if($flag=="Buddha"){ echo "selected='true'";} ?>>Buddha</option>
	                                            <option value="Konghucu" <?php if($flag=="Konghucu"){ echo "selected='true'";} ?>>Konghucu</option>
	                                        </select>
	                                    <!-- </div> -->
	                                </div>

	                                <br> <br>                          
	                            
	                                <div class="input-group fg-float">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-home"></i></span>
	                                    <div class="fg-line">
	                                        <input id="alamat" type="text" name="user_address" class="input-sm form-control fg-input" value="<?php if(isset($alluserprofile)){ echo $alluserprofile['user_address']; } ?>">
	                                        <label class="fg-label"><?php echo $this->lang->line('home_address'); ?></label>
	                                    </div>  
	                                    <small id="cek_alamat" style="color: red;"></small>                                  
	                                </div>

	                                <br/>

	                                                             

	                                <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
	                                    <select id='provinsi_id' class="select2 form-control" name="id_provinsi" data-placeholder="<?php echo $this->lang->line('selectprovinsi'); ?>" style="width: 100%">
	                                        <option value=""></option>
	                                        <?php
	                                            $prv = $this->Master_model->provinsi($alluserprofile['id_provinsi']);
	                                            if($prv != ''){
	                                                echo "<option value='".$prv['provinsi_id']."' selected='true'>".$prv['provinsi_name']."</option>";
	                                                echo "<script>pfp = '".$prv['provinsi_id']."'</script>";
	                                            }
	                                        ?>
	                                    </select>
	                                </div>

	                                <br>

	                                <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
	                                     <select id='kabupaten_id' class="select2 form-control" name="id_kabupaten" data-placeholder="<?php echo $this->lang->line('selectkabupaten'); ?>" style="width: 100%">
	                                        <option value=""></option>
	                                        <?php
	                                            $kbp = $this->Master_model->getAllKabupaten($alluserprofile['id_kabupaten'],$alluserprofile['id_provinsi']);
	                                            if($kbp != ''){
	                                                echo "<option value='".$kbp['kabupaten_id']."' selected='true'>".$kbp['kabupaten_name']."</option>";
	                                                echo "<script>pfka = '".$kbp['kabupaten_id']."'</script>";
	                                            }

	                                        ?>
	                                    </select>
	                                </div>

	                                <br>

	                                <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
	                                    <select id='kecamatan_id' class="select2 form-control" name="id_kecamatan" data-placeholder="<?php echo $this->lang->line('selectkecamatan'); ?>" style="width: 100%">
	                                        <option value=""></option>
	                                        <?php
	                                            $kcm = $this->Master_model->getAllKecamatan($alluserprofile['id_kecamatan'],$alluserprofile['id_kabupaten']);
	                                            if($kcm != ''){
	                                                echo "<option value='".$kcm['kecamatan_id']."' selected='true'>".$kcm['kecamatan_name']."</option>";
	                                                echo "<script>pfkec = '".$kcm['kecamatan_id']."'</script>";
	                                            }

	                                        ?>
	                                    </select>
	                                </div>

	                                <br>

	                                <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
	                                    <select id='desa_id' class="select2 form-control" name="id_desa" data-placeholder="<?php echo $this->lang->line('selectkelurahan'); ?>" style="width: 100%">
	                                        <option value=""></option>
	                                        <?php
	                                            $des = $this->Master_model->getAllKelurahan($alluserprofile['id_desa'],$alluserprofile['id_kecamatan']);
	                                            if($des != ''){
	                                                echo "<option value='".$des['desa_id']."' selected='true'>".$des['desa_name']."</option>";
	                                            }

	                                        ?>
	                                    </select>
	                                </div>                              

	                                <br/>
	    
	                                <div class="input-group fg-float">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-email-open"></i></span>
	                                    <div class="fg-line">
	                                        <input type="text" required id="kodepos" name="kodepos" class="form-control" value="<?php if(isset($alluserprofile)){ echo $alluserprofile['kodepos']; } ?>">
	                                        <label class="fg-label"><?php echo $this->lang->line('postalcode'); ?></label>
	                                    </div>                                    
	                                </div>                           


	                                <br/><br>

	                                <!-- <blockquote class="m-b-25">
	                                    <p><?php echo $this->lang->line('school_jenjang'); ?></p>
	                                </blockquote>     -->                             

	                                
	                                <!-- <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
	                                    <select required id="jenjang_name"  class="select2 form-control" data-placeholder="<?php echo $this->lang->line('select_level'); ?>">
	                                         <option value=""></option>
	                                         <option value="<?php echo $alluserprofile['jenjang_name']; ?>" selected='true'>
	                                         <?php echo $alluserprofile['jenjang_name']; ?></option>
	                                     </select>
	                                   
	                                </div> -->

	                                <!-- <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-graduation-cap"></i></span>
	                                    <select required name="jenjang_id"  class="select2 form-control" id='jenjang_level' data-placeholder="<?php echo $this->lang->line('select_class'); ?>">
	                                            <option value=""></option>
	                                            <?php
	                                                $jen = $this->Master_model->getMasterJenjang($alluserprofile['jenjang_id']);
	                                                if($jen != ''){
	                                                    echo "<option value='".$jen['jenjang_id']."' selected='true'>".$jen['jenjang_name'].' '.$jen['jenjang_level']."</option>";
	                                                }

	                                            ?>
	                                        </select>
	                                  
	                                </div> -->               
	                                              

	                                <div class="card-padding card-body">                                                                           
	                                    <button id="btn_update_profile" type="submit" name="simpanedit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>                    
	                                </div>
	                            </form>                            	
                            </div>
                        </div>                         
                    </div>

                    <div class="pmb-block">
                        <div class="pmbb-header">
                        
                        </div>
                        <div class="pmbb-body p-l-30">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- akhir container -->
</section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
  <script type="text/javascript">
    $('#tempat_lahir').on('keyup',function(){
       if($('#tempat_lahir').val() == ('')){
            $('#cek_tempat_lahir').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');  
        }
        else{
                $('#cek_tempat_lahir').html('').css('color', 'red');
            }
    
    });
    $('#alamat').on('keyup',function(){
        if($('#alamat').val() == ('')){
            $('#cek_alamat').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');  
        }
        else{
                $('#cek_alamat').html('').css('color', 'red');
            }
        
    });
    $('#no_hp_ortu').on('keyup',function(){
        if($('#no_hp_ortu').val() == ('')){
            $('#cek_no_hp_ortu').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');  
        }
        else{
                $('#cek_no_hp_ortu').html('').css('color', 'red');
            }
    });
     $('#desa_id').on('change', function() {
            var cek_hape_ortu = $("#no_hp_ortu").val();
            var cek_tempat_lahir = $("#tempat_lahir").val();
            var cek_alamat = $("#alamat").val();
            
            // if (cek_hape_ortu!=null||cek_tempat_lahir!=null||cek_alamat!=null) {
            //     $("#btn_update_profile").attr('disabled','true');
            // }
            // else
            // {
            //  $("#btn_update_profile").attr('disabled','false');
            // }
     });
        
    </script>

<script>
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
    function validAngka(a)
    {
        if(!/^[0-9.]+$/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
      function validPhone(a)
    {
        if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
</script>
<script type="text/javascript">
        $(document).ready(function(){
            var cek_awal = "<?php echo $this->session->userdata('code_cek');?>";
            if (cek_awal == "889") {
                $(".edit_profile").css('display','block');
                $(".show_profile").css('display','none');
                $("#btn_edit_username").css('display','none');
                $("#btn_save_username").css('display','none');
            }else{
                $(".show_profile").css('display','block');
                $("#btn_edit_username").css('display','block');
            }
            var code_cek = null;
            var status_login = "<?php echo $this->session->userdata('status_user');?>";
            var cekk = setInterval(function(){
                code_cek = "<?php echo $this->session->userdata('code_cek');?>";
                
                cek();
            },500);
            // alert('aasdfsd');
            function cek(){
                if (code_cek == "889") {
                    $("#complete_modal").modal('show');
                    $("#alertcompletedata").css('display','block');
                    
                    code_cek == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>Rest/clearsession',
                        type: 'POST',
                        data: {
                            cek_code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
                else
                {
                    
                }
                // console.warn(code_cek);
                // console.warn(status_login);
            }

            
        });
        jQuery('#btn_edit_username').on('click', function(e){
            $("#btn_save_username").css('display','block');
            $(".edit_profile").css('display','block');
            $(".show_profile").css('display','none');
            $("#btn_edit_username").css('display','none');
            
        });
        jQuery('#btn_save_username').on('click', function(e){
            $("#btn_save_username").css('display','none');
            $(".edit_profile").css('display','none');
            $(".show_profile").css('display','block');
            $("#btn_edit_username").css('display','block');
            
        });
</script>

