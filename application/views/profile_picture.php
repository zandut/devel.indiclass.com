<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">

        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container" style="">
            <?php if(isset($mes_alert)){ ?>
            <div class="alert alert-<?php echo $mes_alert; ?>" style="display: <?php echo $mes_display; ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $mes_message; ?>
            </div>
            <?php } ?>
            
            <div class="block-header">
                <h2><?php echo $this->lang->line('profil'); ?></h2>

                <!-- <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_about'); ?></li>
                        </ol>
                    </li>
                </ul> -->
            </div><!-- akhir block header --> 
            <div class="alert alert-success" style="display: none; ?>" id="alertsuccessupdate">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                Foto akun berhasil diubah
            </div>
            <div class="alert alert-danger" style="display: none; ?>" id="alertsizebesar">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                Upload gagal, Mohon ukuran file tidak melebihi 2 MB.
            </div>
            
            <div id="" class="bs-item z-depth-5" style="margin-top: 5%; display: block;">
                <div class="card" id="profile-main">

                    <?php
                    $this->load->view('inc/sideprofile');
                    ?>           

                    <div id="profil_nonkids" class="pm-body clearfix" style="display: none;">
                        <?php if(isset($mes_alert)){ ?>
                            <div class="alert alert-<?php echo $mes_alert; ?>" style="display: <?php echo $mes_display; ?>">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <?php echo $mes_message; ?>
                            </div>
                        <?php } ?>
                        <ul class="tab-nav tn-justified" role="tablist">
                            <?php
                                $status_user = $this->session->userdata('status_user');
                                if ($status_user =="umum") {
                                ?>
                                    <li class="active waves-effect"><a href="<?php echo base_url('About'); ?>"><?php echo $this->lang->line('tab_about'); ?></a></li>
                                    <li class="waves-effect"><a href="<?php echo base_url('Profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li> 
                                <?php
                                }
                                else{
                                ?>
                                    <li class="active waves-effect"><a href="<?php echo base_url('About'); ?>"><?php echo $this->lang->line('tab_about'); ?></a></li>
                                    <li class="waves-effect"><a href="<?php echo base_url('Profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li> 
                                    <li id="tab_school" class="waves-effect"><a href="<?php echo base_url('Profile-School'); ?>"><?php echo $this->lang->line('tab_ps'); ?></a></li>
                                <?php
                                }
                                ?>
                        </ul>
                        <div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('basic_info'); ?>
                                <button title="Edit Profile" id="btn_edit_username" class="btn btn-info btn-icon pull-right"><i class="zmdi zmdi-edit"></i></button>
                                <button title="Back to Profile" style="display: none;" id="btn_save_username" class="btn btn-info btn-icon pull-right"><i class="zmdi zmdi-check"></i></button>
                                </h2>  
                            </div>
                            <div class="edit_profile" style="display: none;">
                                <form action="<?php echo base_url('/master/saveEditAccountStudent') ?>" method="post">
                                    <div class="col-sm-12" style="margin-bottom: 5%;">

                                        <input name="id_user" type="hidden" value="<?php if ($profile['id_user'] == "") {echo "";} echo $profile['id_user']; ?>">
                                        <label class="fg-label m-l-10"><?php echo $this->lang->line('full_name'); ?> :</label>
                                        <div class="input-group fg-float input-group-addon">
                                            <div class="col-sm-6 m-t-10">
                                                <div class="fg-line">
                                                    <input type="text" name="first_name" required class="input-sm form-control fg-input" value="<?php if ($profile['first_name'] == "") {echo $this->lang->line('nousername');} echo $profile['first_name']; ?>">
                                                    <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                                </div>                                    
                                            </div>
                                            <div class="col-sm-6 m-t-10">
                                                <div class="fg-line">
                                                    <input type="text" name="last_name" required class="input-sm form-control fg-input" value="<?php if ($profile['last_name'] == "") {echo $this->lang->line('nousername');} echo $profile['last_name']; ?>">
                                                    <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                                </div>            
                                            </div>  
                                        </div>                                                                                   
                                        <br>
                                        <label class="fg-label m-l-10"><?php echo $this->lang->line('gender'); ?> :</label>
                                        <br>
                                        <div class="input-group fg-float input-group-addon">
                                            <!-- <div class="select"> -->
                                            <?php 
                                                //$flag = null;
                                                if (isset($profile)) {
                                                    $flag = $profile['user_gender'];
                                                }
                                            ?>
                                            <div class="col-md-3" align="left">                                        
                                                <select name="user_gender" class="select2 form-control">
                                                    <option disabled><?php echo $this->lang->line('select_gender'); ?></option>
                                                    <option value="Male" <?php if($flag=="Male"){ echo "selected='true'";} ?> ><?php echo $this->lang->line('male');?></option>
                                                    <option value="Female" <?php if($flag=="Female"){ echo "selected='true'";} ?>><?php echo $this->lang->line('women');?></option>
                                                </select>                                        
                                            </div>
                                        </div>
                                        <br>
                                        <label class="fg-label m-l-10"><?php echo $this->lang->line('birth'); ?> :</label>
                                        <br>
                                        <div class="input-group fg-float input-group-addon">
                                            <div class="col-sm-4 m-t-10">
                                                <div class="fg-line">
                                                    <input type="text" name="user_birthplace" required class="input-sm form-control fg-input" value="<?php echo $profile['user_birthplace']?>">
                                                    <label class="fg-label"><?php echo $this->lang->line('placeofbirth'); ?></label>
                                                </div>    
                                            </div>
                                            <div class="col-sm-2 m-t-10">
                                                <div class="fg-line">
                                                    <select name="tanggal_lahir" id="" class="select2 form-control" style='width:100%;'>
                                                            <option disabled selected><?php echo $this->lang->line('datebirth'); ?></option>
                                                            <?php 
                                                            if(isset($profile)){
                                                                $tgl = substr($profile['user_birthdate'], 8,2);
                                                                $bln = substr($profile['user_birthdate'], 5,2);
                                                                $thn = substr($profile['user_birthdate'], 0,4);
                                                            }

                                                                for ($date=01; $date <= 31; $date++) {
                                                                    echo '<option value="'.$date.'" ';
                                                                    if(isset($profile) && $tgl == $date){ echo "selected='true'";}
                                                                    echo '>'.$date.'</option>';
                                                                }
                                                            ?>  
                                                        </select>
                                                        <label class="fg-label"><?php echo $this->lang->line('datebirth'); ?></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 m-t-10">
                                                <div class="fg-line">
                                                    <select name="bulan_lahir" class="select2 form-control" style='width:100%;' style="width: 100%;">
                                                            <option disabled selected><?php echo $this->lang->line('monthbirth'); ?></option>
                                                            <option value="01" <?php if(isset($profile) && $bln == "01"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jan'); ?></option>
                                                            <option value="02" <?php if(isset($profile) && $bln == "02"){ echo "selected='true'";} ?>><?php echo $this->lang->line('feb'); ?></option>
                                                            <option value="03" <?php if(isset($profile) && $bln == "03"){ echo "selected='true'";} ?>><?php echo $this->lang->line('mar'); ?></option>
                                                            <option value="04" <?php if(isset($profile) && $bln == "04"){ echo "selected='true'";} ?>><?php echo $this->lang->line('apr'); ?></option>
                                                            <option value="05" <?php if(isset($profile) && $bln == "05"){ echo "selected='true'";} ?>><?php echo $this->lang->line('mei'); ?></option>
                                                            <option value="06" <?php if(isset($profile) && $bln == "06"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jun'); ?></option>
                                                            <option value="07" <?php if(isset($profile) && $bln == "07"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jul'); ?></option>
                                                            <option value="08" <?php if(isset($profile) && $bln == "08"){ echo "selected='true'";} ?>><?php echo $this->lang->line('ags'); ?></option>
                                                            <option value="09" <?php if(isset($profile) && $bln == "09"){ echo "selected='true'";} ?>><?php echo $this->lang->line('sep'); ?></option>
                                                            <option value="10" <?php if(isset($profile) && $bln == "10"){ echo "selected='true'";} ?>><?php echo $this->lang->line('okt'); ?></option>
                                                            <option value="11" <?php if(isset($profile) && $bln == "11"){ echo "selected='true'";} ?>><?php echo $this->lang->line('nov'); ?></option>
                                                            <option value="12" <?php if(isset($profile) && $bln == "12"){ echo "selected='true'";} ?>><?php echo $this->lang->line('des'); ?></option>  
                                                    </select> 
                                                    <label class="fg-label"><?php echo $this->lang->line('monthbirth'); ?></label>
                                                </div>
                                                <!-- <span class="zmdi zmdi-triangle-down form-control-feedback"></span> -->
                                            </div>
                                            <div class="col-sm-2 m-t-10">                                   
                                                <div class="fg-line">
                                                    <select name="tahun_lahir" id="tahun_lahir"  class="select2 form-control" style='width:100%;' style="width: 100%;">
                                                        <option disabled selected><?php echo $this->lang->line('yearbirth'); ?></option>
                                                        <?php 
                                                            for ($i=1960; $i <= 2016; $i++) {                                                                            
                                                                echo '<option value="'.$i.'" ';
                                                                if(isset($profile) && $thn == $i){ echo "selected='true'";}
                                                                echo '>'.$i.'</option>';
                                                            } 
                                                        ?>
                                                    </select>
                                                    <label class="fg-label"><?php echo $this->lang->line('yearbirth'); ?></label>

                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <label class="fg-label m-l-10"><?php echo $this->lang->line('contact'); ?> :</label>
                                        <br>
                                        <br>
                                        
                                        <div class="input-group fg-float m-t-10 input-group-addon">
                                            <div class="col-md-3">                                        
                                                <div class="fg-line">                                           
                                                    <!-- <select required name="kode_area" class="select2 form-control" style='width:100%;'>
                                                        <option disabled selected value=''><?php echo $this->lang->line('countrycode'); ?></option>
                                                        <option value="+62" <?php if($ambildepan == "+62"){ echo "selected='true'";} ?>>Indonesia +62</option>
                                                        <option value="+60" <?php if($ambildepan == "+60"){ echo "selected='true'";} ?>>Malaysia +60</option>
                                                        <option value="+65" <?php if($ambildepan == "+65"){ echo "selected='true'";} ?>>Singapore +65</option>
                                                    </select> -->
                                                    <select required name="kode_area"  class="select2 form-control" style='width:100%;' style="width: 100%;">
                                                    
                                                    <?php
                                                        $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                                        foreach ($allsub as $row => $d) {
                                                            if($d['phonecode'] == $profile['user_countrycode']){
                                                                echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                            }
                                                            if ($d['phonecode'] != $profile['user_countrycode']) {
                                                                echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                            }                                                            
                                                        }
                                                    ?> 
                                                    </select>
                                                    <label class="fg-label"><?php echo $this->lang->line('countrycode'); ?></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="fg-line">
                                                    <input type="text" id="user_callnum" name="user_callnum" minlength="9" maxlength="13" onmousemove="validPhone(this)" onkeyup="validPhone(this)"  onkeyup="validAngka(this)" onkeypress="return hanyaAngka(event)" required class="input-sm form-control fg-input" value="<?php echo $profile['user_callnum'];?>">
                                                    <label class="fg-label"><?php echo $this->lang->line('mobile_phone'); ?></label>
                                                </div>
                                            </div>  
                                            <div class="col-md-5">
                                                <div class="fg-line">
                                                    <input type="email" readonly name="email" style="background-color: #f8f8f8; cursor: pointer;" required class="input-sm form-control fg-input" value="<?php if ($profile['email'] == "") {echo $this->lang->line('noemail');} echo $profile['email']; ?>">
                                                    <label class="fg-label">Email</label>
                                                </div> 
                                            </div>                          
                                        </div>


                                        <br/>
                                        <div class="input-group fg-float input-group-addon">
                                                                               
                                        </div>     
                                        <br>                                                                                

                                        <blockquote class="m-b-10 m-t-10">
                                            <p><?php echo $this->lang->line('tab_cp'); ?></p>
                                        </blockquote>
                                        <br/>
                                        <div class="input-group fg-float">
                                            <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                            <div class="fg-line">
                                                <input type="password" name="oldpass" class="input-sm form-control fg-input">
                                                <label class="fg-label"><?php echo $this->lang->line('old_pass'); ?></label>
                                            </div>                                    
                                        </div>

                                        <br>

                                        <div class="input-group fg-float">
                                            <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                            <div class="fg-line">
                                                <input type="password" name="newpass" id="newpass" class="input-sm form-control fg-input">
                                                <label class="fg-label"><?php echo $this->lang->line('new_pass'); ?></label>
                                            </div>                                    
                                        </div>
                                        
                                        <br>    

                                        <div class="input-group fg-float">
                                            <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                            <div class="fg-line">
                                                <input type="password" name="conpass" id="conpass" class="input-sm form-control fg-input" onChange="checkPasswordMatch();">
                                                <label class="fg-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                            </div>
                                            <small class="help-block" id="divCheckPasswordMatch"></small>                                    
                                        </div> 
                                    </div>                                                    
                                    <div class="card-padding card-body">                                                                            
                                        <button type="submit" name="simpanedit" id="simpanedit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>                                
                                    </div>

                                </form> 
                            </div>
                            
                            <div class="pmbb-body p-l-30 show_profile">
                                <div class="pmbb-view ">
                                    <dl class="dl-horizontal" id="show_username">
                                        <dt><?php echo $this->lang->line('full_name'); ?></dt>
                                        <dd>:
                                            <?php 
                                                if ($profile['user_name'] == "") {
                                                    echo $this->lang->line('nousername');
                                                }
                                                echo $profile['user_name']; 
                                            ?>                                            
                                        </dd>
                                    </dl> 
                                    <dl class="dl-horizontal">
                                        <dt><?php echo $this->lang->line('birth'); ?></dt>                                    
                                        <dd>:
                                            <?php 

                                                $birthdate = date("d M Y", strtotime($profile['user_birthdate']));
                                                if ($profile['user_birthdate'] == "") {
                                                    echo $this->lang->line('nobirthday');
                                                }
                                                echo $profile['user_birthplace'].', '.$birthdate; 
                                            ?>                                            
                                        </dd>
                                    </dl>                         
                                    <dl class="dl-horizontal">
                                        <dt><?php echo $this->lang->line('gender'); ?></dt>                                    
                                        <dd>:
                                            <?php 
                                                if ($profile['user_gender'] == "") {
                                                    echo $this->lang->line('nogender');
                                                }
                                                if ($profile['user_gender'] == "Male"){
                                                    echo $this->lang->line('male');
                                                }
                                                if($profile['user_gender'] == "Female"){
                                                    echo $this->lang->line('women');
                                                }
                                            ?>                                            
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt><?php echo $this->lang->line('home_address'); ?></dt>                                    
                                        <dd>:
                                            <?php 
                                                if ($profile['user_address'] == "") {
                                                    echo $this->lang->line('noaddress');
                                                }
                                                echo $profile['user_address']; 
                                            ?>                                            
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt><?php echo $this->lang->line('email_address'); ?></dt>                      
                                        <dd>:
                                            <?php 
                                                if ($profile['email'] == "") {
                                                    echo $this->lang->line('noemail');
                                                }
                                                echo $profile['email'] 
                                            ?>                                            
                                        </dd>
                                    </dl>  
                                    <dl class="dl-horizontal">
                                        <dt><?php echo $this->lang->line('mobile_phone'); ?></dt>                                        
                                        <dd>:
                                            <?php 
                                                if ($profile['user_callnum'] == "") {
                                                    echo $this->lang->line('nonumber');
                                                }
                                                echo "+".$profile['user_countrycode'].$profile['user_callnum'] 
                                            ?>                                            
                                        </dd>
                                    </dl>                             
                                </div>                            
                            </div>

                        </div> 
                        <br><br><br><br><br><br><br><br>
                    </div>
                    <div id="profil_kids" class="pm-body clearfix" style="display: none;">
                        <ul class="tab-nav tn-justified" role="tablist">
                            <!-- <li class="active waves-effect"><a href="<?php echo base_url('About'); ?>"><?php echo $this->lang->line('tab_about'); ?></a></li> -->
                            <li class="active waves-effect"><a href="<?php echo base_url('About'); ?>"><?php echo $this->lang->line('tab_account'); ?></a></li> 
                            <!-- <li class="waves-effect"><a href="<?php echo base_url('Profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li>  -->
                            <li class="waves-effect"><a href="<?php echo base_url('School-Kids'); ?>"><?php echo $this->lang->line('tab_ps'); ?></a></li>
                            <!-- <li class="waves-effect"><a href="<?php echo base_url('first/profile_forgot'); ?>"><?php echo $this->lang->line('tab_cp'); ?></a></li> -->                                      
                        </ul>
                        <div class="card-padding card-body">
                            <div class="col-sm-12">  
                            <form action="<?php echo base_url('/master/saveEditAccountStudent') ?>" method="post">
                                <input name="id_user" type="hidden" value="<?php if(isset($profile)){ echo $profile['id_user']; } ?>">
                                
                                <div class="input-group fg-float">
                                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                    <div class="col-sm-6 m-t-10">
                                        <div class="fg-line">
                                            <input type="text" name="first_name" required class="input-sm form-control fg-input" value="<?php if(isset($profile)){ echo $profile['first_name']; } ?>">
                                            <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                        </div>    
                                    </div>                                
                                    <div class="col-sm-6 m-t-10">
                                        <div class="fg-line">
                                            <input type="text" name="last_name" required class="input-sm form-control fg-input" value="<?php if(isset($profile)){ echo $profile['last_name']; } ?>">
                                            <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                        </div>                                    
                                    </div>
                                </div>
                                <br>
                                <!-- <label class="fg-label" style="margin-left: 5%;"><small><?php echo $this->lang->line('dateofbirth'); ?>Tanggal Lahir :</small></label> -->
                                <div class="input-group fg-float">
                                    <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                    <div class="col-sm-4 m-t-10">
                                        <div class="fg-line">
                                            <select name="tanggal_lahir" id="tanggal_lahir" class="select2 form-control" style='width:100%;'>
                                                    <option disabled selected><?php echo $this->lang->line('datebirth'); ?></option>
                                                    <?php 
                                                    if(isset($profile)){
                                                        $tgl = substr($profile['user_birthdate'], 8,2);
                                                        $bln = substr($profile['user_birthdate'], 5,2);
                                                        $thn = substr($profile['user_birthdate'], 0,4);
                                                    }

                                                        for ($date=01; $date <= 31; $date++) {
                                                            echo '<option value="'.$date.'" ';
                                                            if(isset($profile) && $tgl == $date){ echo "selected='true'";}
                                                            echo '>'.$date.'</option>';
                                                        }
                                                    ?>  
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 m-t-10">
                                        <div class="fg-line">
                                            <select name="bulan_lahir" class="select2 form-control" style='width:100%;'>
                                                    <option disabled selected><?php echo $this->lang->line('monthbirth'); ?></option>
                                                    <option value="01" <?php if(isset($profile) && $bln == "01"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jan'); ?></option>
                                                    <option value="02" <?php if(isset($profile) && $bln == "02"){ echo "selected='true'";} ?>><?php echo $this->lang->line('feb'); ?></option>
                                                    <option value="03" <?php if(isset($profile) && $bln == "03"){ echo "selected='true'";} ?>><?php echo $this->lang->line('mar'); ?></option>
                                                    <option value="04" <?php if(isset($profile) && $bln == "04"){ echo "selected='true'";} ?>><?php echo $this->lang->line('apr'); ?></option>
                                                    <option value="05" <?php if(isset($profile) && $bln == "05"){ echo "selected='true'";} ?>><?php echo $this->lang->line('mei'); ?></option>
                                                    <option value="06" <?php if(isset($profile) && $bln == "06"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jun'); ?></option>
                                                    <option value="07" <?php if(isset($profile) && $bln == "07"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jul'); ?></option>
                                                    <option value="08" <?php if(isset($profile) && $bln == "08"){ echo "selected='true'";} ?>><?php echo $this->lang->line('ags'); ?></option>
                                                    <option value="09" <?php if(isset($profile) && $bln == "09"){ echo "selected='true'";} ?>><?php echo $this->lang->line('sep'); ?></option>
                                                    <option value="10" <?php if(isset($profile) && $bln == "10"){ echo "selected='true'";} ?>><?php echo $this->lang->line('okt'); ?></option>
                                                    <option value="11" <?php if(isset($profile) && $bln == "11"){ echo "selected='true'";} ?>><?php echo $this->lang->line('nov'); ?></option>
                                                    <option value="12" <?php if(isset($profile) && $bln == "12"){ echo "selected='true'";} ?>><?php echo $this->lang->line('des'); ?></option>  
                                            </select> 
                                        </div>
                                        <!-- <span class="zmdi zmdi-triangle-down form-control-feedback"></span> -->
                                    </div>
                                    <div class="col-sm-4 m-t-10">                                   
                                        <div class="fg-line">
                                            <select name="tahun_lahir" class="select2 form-control" style='width:100%;'>
                                                <option disabled selected><?php echo $this->lang->line('yearbirth'); ?></option>
                                                <?php 
                                                    for ($i=1960; $i <= 2016; $i++) {                                                                            
                                                        echo '<option value="'.$i.'" ';
                                                        if(isset($profile) && $thn == $i){ echo "selected='true'";}
                                                        echo '>'.$i.'</option>';
                                                    } 
                                                ?>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                
                                <br>
                                 <!-- <label class="fg-label" style="margin-left: 5%;"><small><?php echo $this->lang->line('dateofbirth'); ?>Ganti Kata Sandi :</small></label> -->
                                <div class="input-group fg-float">
                                    <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                    <div class="col-sm-6 m-t-10">
                                        <div class="fg-line">
                                            <input type="password" name="newpass" id="newpass" class="input-sm form-control fg-input">
                                            <label class="fg-label"><?php echo $this->lang->line('new_pass'); ?></label>
                                        </div>                                    
                                    </div>
                                    <div class="col-sm-6 m-t-10">
                                        <div class="fg-line">
                                            <input type="password" name="conpass" id="conpass" class="input-sm form-control fg-input" onChange="checkPasswordMatch();">
                                            <label class="fg-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                        </div>
                                    </div>
                                    <small class="help-block" id="divCheckPasswordMatch"></small>                                    
                                </div>                                                                                                               
                                <br><br>
                            </div>                                                    

                            <div class="card-padding card-body">                                                                            
                                <button type="submit" id="simpanedit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>                                
                            </div>

                            </form>

                            <!--Disini Password -->
                            <form>
                                 
                            </form>
                            <!-- Akhirnya disini -->
                        </div>
                    </div>
                </div>
            </div>
            <div id="" class="bs-item z-depth-5" style="margin-top: 5%; display: none;">
                <div class="card" id="profile-main">

                    <?php
                        // $this->load->view('inc/sideprofile');

                        $id_user_kids = $this->session->userdata('id_user_kids');
                        $get_data_anak = $this->db->query("SELECT ts.*, tpk.parent_id, tpk.jenjang_id FROM tbl_user as ts LEFT JOIN tbl_profile_kid as tpk ON ts.id_user=tpk.kid_id WHERE id_user='$id_user_kids'")->row_array();
                        $get_jenjang_anak = $this->db->query("SELECT tpk.parent_id, tpk.kid_id, tpk.jenjang_id, tu.jenjang_id, tu.jenjang_level, tu.jenjang_name from tbl_profile_kid as tpk INNER JOIN master_jenjang as tu where tu.jenjang_id = tpk.jenjang_id and tpk.kid_id='$id_user_kids'")->row_array();                        
                        $username_kids = $this->session->userdata('username_kids');
                        $parent_id  = $this->session->userdata('id_user');
                        $id_user_kids = $this->session->userdata('id_user_kids');
                        $birth_date = $get_data_anak['user_birthdate'];
                        $birthdate = date("d M Y", strtotime($birth_date));
                    ?>           

                    
                </div>
            </div>
        </div><!-- akhir container -->
    </section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
        var id_user_kids = "<?php echo $this->session->userdata('id_user_kids'); ?>";
        var id_user = "<?php echo $this->session->userdata('id_user'); ?>";
        var parent_id = "<?php echo $this->session->userdata('parent_id'); ?>";

        
        if (parent_id == "") {
            $("#profil_kids").css('display','none');
            $("#profil_nonkids").css('display','block');
        }
        else{
            $("#profil_kids").css('display','block');
            $("#profil_nonkids").css('display','none');

        }
</script>
<script type="text/javascript">
        jQuery(document).ready(function(){
                
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        var status_login = "<?php echo $this->session->userdata('status_user');?>";

        var cekk = setInterval(function(){
            code = "<?php echo $this->session->userdata('code');?>";
            cek();
        },500);

        function cek(){

            if (code == "4044") {
                $("#alertsizebesar").css('display','none');
                $("#alertsuccessupdate").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url();?>Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
            else if (code == "4045") {
                $("#alertsuccessupdate").css('display','none');
                $("#alertsizebesar").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url();?>Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
        }

        jQuery('#change-pic').on('click', function(e){
            jQuery('#changePic').show();
            jQuery('#change-pic').hide();
            
        });
        
        jQuery('#photoimg').on('change', function()   
        { 
            jQuery("#preview-avatar-profile").html('');
            jQuery("#preview-avatar-profile").html('Uploading....');
            jQuery("#cropimage").ajaxForm(
            {
            target: '#preview-avatar-profile',
            success:    function() { 
                    jQuery('img#photo').imgAreaSelect({
                    aspectRatio: '1:1',
                    onSelectEnd: getSizes,
                });
                jQuery('#image_name').val(jQuery('#photo').attr('file-name'));
                }
            }).submit();

        });

        });

        jQuery('#btn_edit_username').on('click', function(e){
            $("#btn_save_username").css('display','block');
            $(".edit_profile").css('display','block');
            $(".show_profile").css('display','none');
            $("#btn_edit_username").css('display','none');
            
        });
        jQuery('#btn_save_username').on('click', function(e){
            $("#btn_save_username").css('display','none');
            $(".edit_profile").css('display','none');
            $(".show_profile").css('display','block');
            $("#btn_edit_username").css('display','block');
            
        });
    </script>
