<main class="main">

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>List Package</h1>
            <small>Detail list of Package on your channel!</small>

            <div class="actions">
                <a data-toggle="modal" href="#modalAddPackage"><button class="btn btn-success">+ Add Package</button></a>
            </div>        
        </header>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">
                    <div class="row table-responsive">
                        <div id="tables_listPackage"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" >
                <div class="modal-content" id="modal_konten">
                    <div class="modal-body" align="center">
                        <label id="text_modal">Halloo</label><br>
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal modalAddPackage -->  
        <div class="modal fade show" id="modalAddPackage">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">                        
                        <h4 class="modal-title c-white pull-left">Input Detail Package</h4>
                        <h4 class="modal-title c-white pull-left" style="display: none;">Input Detail Payment</h4>
                        <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    </div>
                    <div class="modal-body m-t-20">
                        <form id="frmdata">

                            <div class="row col-md-12" id="boxOne">                            	
	                            <div class="col-md-12">
	                            	<div class="form-group">
	                                    <label>Package Name</label>
	                                    <input type="text" id="val_packagename" required style="background-color: #ECF0F1; padding: 10px;" class="form-control floating-label" placeholder="Type Here...">
	                                    <i class="form-group__bar"></i>
	                                    <label style="color: red; display: none;" id="alert_name">Please enter package name</label>
	                                </div>	                                
								</div>  
	                            <div class="col-md-12">
	                            	<div class="form-group">
	                                    <label>Package Description</label>                                    
	                                    <textarea class="form-control floating-label" required rows="5" style="background-color: #ECF0F1; padding: 10px;" id="val_packagedescription" placeholder="Type Here..."></textarea>
	                                    <label style="color: red; display: none;" id="alert_description">Please enter package description</label>
	                                </div>
								</div>
								<div class="col-md-6">
	                            	<div class="form-group">
	                                    <label>Choose Type</label>                                    
	                                    <select class="select2 form-control" required id="select_type" style="width: 100%;">                                        
	                                        <option value="SD">SD</option>
	                                        <option value="SMP">SMP</option>
	                                        <option value="SMA - IPA">SMA - IPA</option>
	                                        <option value="SMA - IPS">SMA - IPS</option>
	                                        <option value="SMK">SMK</option>
	                                    </select>
	                                    <label style="color: red; display: none;" id="alert_type">Please choose type</label>
	                                </div>
								</div>
								<div class="col-md-6">
	                            	<div class="form-group">
	                                    <label>Choose Jenjang</label>                                    
	                                    <select class="select2 form-control" required id="select_jenjang" multiple style="width: 100%;">                                        
	                                    </select>
	                                    <label style="color: red; display: none;" id="alert_jenjang">Please choose jenjang</label>
	                                </div>
								</div>
								<div class="col-md-6">
	                            	<div class="form-group">
	                                    <label>Bidang</label>                                    
	                                    <select class="select2 form-control" required name="val_packagebidang[]" id="val_packagebidang" multiple style="width: 100%;">                                        
	                                    </select>
	                                    <label style="color: red; display: none;" id="alert_bidang">Please choose bidang</label>
	                                </div>
								</div>
								<div class="col-md-6">
	                            	<div class="form-group">
	                                    <label>Fasilitas</label>                                    
	                                    <select class="select2 form-control" required name="val_packagefisilitas[]" id="val_packagefisilitas" multiple style="width: 100%;">                                        
	                                    </select>
	                                    <label style="color: red; display: none;" id="alert_fasilitas">Please choose fasilitas</label>
	                                </div>
								</div>
	                            
								<div class="col-md-12">
                                    <br>
                                    <label>Expired Package</label>
                                    <div class="form-group">                                    
                                        <input type="text" id="expired_start" class="form-control  floating-label " placeholder="Select Date Session">
                                        <label style="color: red; display: none;" id="alert_date_expired">Please Select Start Date Expired</label>
                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                    <br>
                                    <label>Finish Expired Package</label>
                                    <div class="form-group">                                    
                                        <input type="text" id="expired_finish" class="form-control date-picker floating-label " placeholder="Select Date Session">
                                        <label style="color: red; display: none;" id="alert_date_expired">Please Select Finish Date Expired</label>
                                    </div>
                                </div>  -->  
	                            <div class="col-md-12">
	                                <br>
	                                <label>Program</label>
	                                <div class="form-group">                                    
	                                    <select class="select2 form-control" required id="select_program" style="width: 100%;">
	                                        <option disabled="disabled" selected="" value="0">Choose Program</option>                            
	                                    </select>
	                                    <label style="color: red; display: none;" id="alert_program">Please choose program</label>
	                                </div>
	                            </div> 	                            	                                                      
	                            <div class="col-md-6">
	                                <br>
	                                <label>Banner Program</label>
	                                <div class="form-group">                                    
	                                    <input type="file" class="form-control" id="poster">  
	                                    <textarea id="image_new" rows="3" class="form-control" style="display: none;"></textarea>
	                                    <label style="color: red; display: none;" id="alert_banner">Please choose banner</label>
	                                </div>
	                            </div> 
	                            <div class="col-md-6">
	                                <br>
	                                <label>Banner Program Android</label>
	                                <div class="form-group">                                    
	                                    <input type="file" class="form-control" id="poster_android">  
	                                    <textarea id="image_new_android" rows="3" class="form-control" style="display: none;"></textarea>
	                                    <label style="color: red; display: none;" id="alert_banner_android">Please choose banner android</label>
	                                </div>
	                            </div>                     
                            </div>

                            <div class="row col-md-12" id="boxTwo" style="display: none;">
                            	<div class="col-md-12">
	                                <br>
	                                <label>Participant Eligibility</label>
	                                <div class="form-group">                                     
	                                    <select class="select2 form-control" required id="participant_eligibility" style="width: 100%;">
	                                        <option disabled="disabled" selected="">Choose</option>
	                                        <option value="Once">Once</option>
	                                        <option value="Periodic">Periodic</option>
	                                    </select>
	                                    <label style="color: red; display: none;" id="alert_participant_eligibility">Please choose Participant Eligibility</label>
	                                </div>
	                            </div> 
                            	<div class="col-md-12">
	                                <label>Package Price</label>
	                                <div class="input-group">                                    
	                                    <span class="input-group-addon">Rp</span>
	                                    <div class="form-group">
	                                        <input type="number" min="1" step="1" class="form-control" required id="val_packageprice" style="background-color: #ECF0F1; padding: 10px;" placeholder="Type Here..." data-mask="000.000.000.000.000,00">
	                                        <i class="form-group__bar"></i>
	                                    </div>
	                                    <label style="color: red; display: none;" id="alert_price">Please enter price</label>
	                                </div>
								</div>								
								<div class="col-md-12" id="boxBillingUtama">
									<div id="boxBillingType1" class="row">
			                            <div class="col-md-10">
			                                <br>
			                                <label>Billing Type</label>
			                                <div class="form-group">                                     
			                                    <select class="select2 form-control" required id="billing_type1" style="width: 100%;">
			                                        <option disabled="disabled" selected="">Choose Type</option>
			                                        <option value="byday">By Day</option>
			                                        <!-- <option value="bycounter">By Session</option> -->
			                                    </select>
			                                    <label style="color: red; display: none;" id="alert_billing_type1">Please choose banner</label>
			                                </div>
			                            </div> 
			                            <!-- <div class="col-md-5">
			                                <br>
			                                <label>Billing Input</label>
			                                <div class="form-group" id="boxValType">                                     
			                                    <select class="select2 form-control" required name="val_byday[]" id="val_byday1" multiple style="width: 100%;">
			                                    </select>
			                                    <label style="color: red; display: none;" id="alert_val_byday1">Please choose banner android</label>
			                                </div>
			                            </div> -->  
			                            <div class="col-md-2 boxAddOrDel"  style="display: none;">
			                            	<br>		                            	
			                            	<a class="form-control btn btn-primary btn-block addBillingType" title="Tambah Billing Type" style="color: white;">+</a>
			                            	<a class="form-control btn btn-primary btn-block deleteBillingType" title="Hapus Billing Type" style="color: white; display: none;">-</a>
			                            </div>
			                            <div class="col-md-12" id="boxSelect">
			                            	
			                            </div>
		                            </div>
		                            <!-- <div id="boxBillingType2" class="row">
		                            </div> -->
	                            </div>
                            </div>
                        </form>
                        <br>
                    </div>
                    <div class="modal-footer">
                    	<button type="button" class="btn btn-success btn-block m-t-15" id="nextPackage">Next</button>
                    	<button type="button" class="btn btn-success btn-block m-t-15" id="prevPackage" style="display: none;">Prev</button>
                        <button type="button" class="btn btn-success btn-block m-t-15" id="savePackage" style="display: none;">Save Package</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade show" id="modalDeletePackage" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Delete Package</b></h3>
                    </div>
                    <div class="modal-body">                        
                        <label class="c-gray f-15">Are you sure you want to delete?</label>                         
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                        <button type="button" data-dismiss="modal" class="btn btn-danger waves-effect" id="proses_hapuspackage" data-id="" rtp="">Yes</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade show" id="modalStatusPackage">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">                        
                        <h4 class="modal-title c-white pull-left">Update Package</h4>
                        <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    </div>
                    <div class="modal-body m-t-20">
                        <form id="frmdata">
                        	<input type="hidden" id="txt_listid" name="">
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Package Name</label>
                                        <input type="text" id="txt_packagename" required style="background-color: #ECF0F1; padding: 10px;" class="form-control floating-label" placeholder="Type Here...">
                                        <i class="form-group__bar"></i>
                                        <label style="color: red; display: none;" id="alert_update_name">Please enter package name</label>
                                    </div>
                                </div>  
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Package Description</label>
                                        <textarea class="form-control floating-label" required rows="5" style="background-color: #ECF0F1; padding: 10px;" id="txt_packagedescription" placeholder="Type Here..."></textarea>
                                        <label style="color: red; display: none;" id="alert_update_description">Please enter package description</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
	                            	<div class="form-group">
	                                    <label>Choose Type</label>                                    
	                                    <select class="select2 form-control" required id="select_update_type" style="width: 100%;">                                        
	                                        <option value="SD">SD</option>
	                                        <option value="SMP">SMP</option>
	                                        <option value="SMA - IPA">SMA - IPA</option>
	                                        <option value="SMA - IPS">SMA - IPS</option>
	                                        <option value="SMK">SMK</option>
	                                    </select>
	                                    <label style="color: red; display: none;" id="alert_update_type">Please choose type</label>
	                                </div>
								</div>
								<div class="col-md-6">
	                            	<div class="form-group">
	                                    <label>Choose Jenjang</label>                                    
	                                    <select class="select2 form-control" required id="select_update_jenjang" multiple style="width: 100%;">                                        
	                                    </select>
	                                    <label style="color: red; display: none;" id="alert_update_jenjang">Please choose jenjang</label>
	                                </div>
								</div>
								<div class="col-md-6">
	                            	<div class="form-group">
	                                    <label>Bidang</label>                                    
	                                    <select class="select2 form-control" required name="val_update_packagebidang[]" id="val_update_packagebidang" multiple style="width: 100%;">
	                                    	<option selected="selected">adasf</option>                             
	                                    </select>
	                                    <label style="color: red; display: none;" id="alert_update_bidang">Please choose bidang</label>
	                                </div>
								</div>
								<div class="col-md-6">
	                            	<div class="form-group">
	                                    <label>Fasilitas</label>                                    
	                                    <select class="select2 form-control" required name="val_update_packagefisilitas[]" id="val_update_packagefisilitas" multiple style="width: 100%;">                                        
	                                    </select>
	                                    <label style="color: red; display: none;" id="alert_update_fasilitas">Please choose fasilitas</label>
	                                </div>
								</div>
                                <div class="col-md-12">
                                    <label>Package Price</label>
                                    <div class="input-group">                                    
                                        <span class="input-group-addon">Rp</span>
                                        <div class="form-group">
                                            <input type="number" min="1" step="1" class="form-control" required id="txt_packageprice" style="background-color: #ECF0F1; padding: 10px;" placeholder="Type Here..." data-mask="000.000.000.000.000,00">
                                            <i class="form-group__bar"></i>
                                        	<label style="color: red; display: none;" id="alert_update_price">Please enter price</label>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <br>
                                    <label>Expired Package</label>
                                    <div class="form-group">                                    
                                        <input type="text" id="expired_update_start" class="form-control floating-label" placeholder="Select Date Session" value="04/10/2018 – 18/10/2018">
                                        <label style="color: red; display: none;" id="alert_update_date_expired">Please Select Start Date Expired</label>
                                    </div>
                                </div>  
                                <div class="col-md-12">
                                    <br>
                                    <label>Program</label>
                                    <div class="form-group">                                    
                                        <select class="select2 form-control" required id="txt_program" style="width: 100%;">
                                            <option disabled="disabled" selected="" value="0">Choose Program</option>                            
                                        </select> 
                                        <label style="color: red; display: none;" id="alert_update_program">Please choose program</label>                                   
                                    </div>
                                </div>
                                <div class="col-md-6">
	                                <br>
	                                <label>Banner Program<br><small style="color: red;">(Jika tidak ada perubahan tidak perlu memilih banner)</small></label>
	                                <div class="form-group">                                    
	                                    <input type="file" class="form-control" id="poster">  
	                                    <textarea id="image_update_new" rows="3" class="form-control" style="display: none;"></textarea>
	                                    <label style="color: red; display: none;" id="alert_update_banner">Please choose banner</label>
	                                </div>
	                            </div> 
	                            <div class="col-md-6">
	                                <br>
	                                <label>Banner Program Android<br><small style="color: red;">(Jika tidak ada perubahan tidak perlu memilih banner)</small></label>
	                                <div class="form-group">                                    
	                                    <input type="file" class="form-control" id="poster_android">  
	                                    <textarea id="image_update_new_android" rows="3" class="form-control" style="display: none;"></textarea>
	                                    <label style="color: red; display: none;" id="alert_update_banner_android">Please choose banner android</label>
	                                </div>
	                            </div>                        
                            </div>                               
                        </form>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button"  class="btn btn-success btn-block m-t-15" id="updatePackage">Update Package</button>                            
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>        
<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";    
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = [];
    var billing_type = [];
    var noInput = 1;
    var InputNo = 1;
    var actv = "";

    $(document).ready(function() {

        $.ajax({
            url: '<?php echo AIR_API;?>list_PricePackage/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response);  
                var code = response['code'];                
                if (code == 200) {
                    for (var i = 0; i < response.data.length; i++) {
                        var list_id     	= response['data'][i]['list_id'];
                        var name        	= response['data'][i]['name'];
                        var description 	= response['data'][i]['description'];
                        var price       	= response['data'][i]['price'];
                        var poster      	= response['data'][i]['poster'];                        
                        var program_id  	= response['data'][i]['program_id'];
                        var program_name	= response['data'][i]['program_name'];
                        var jenjang_id  	= response['data'][i]['jenjang_id'];
                        var bidang    		= response['data'][i]['bidang'];
                        var fasilitas   	= response['data'][i]['fasilitas'];
                        var expired_start 	= response['data'][i]['expired_start'];
                        var expired_finish  = response['data'][i]['expired_finish'];

                        var action = "<button class='edit_package btn waves-effect' style='background-color: #607D8B;' data-name='"+name+"' data-description='"+description+"' data-price='"+price+"' data-list_id='"+list_id+"' data-programid='"+program_id+"' data-jenjangid='"+jenjang_id+"' data-bidang='"+bidang+"' data-fasilitas='"+fasilitas+"' data-expiredstart='"+expired_start+"' data-expiredfinish='"+expired_finish+"'><i class='zmdi zmdi-edit' style='color:#fff;'></i></button> <button class='action_package btn waves-effect' style='background-color: #607D8B;' data-list_id='"+list_id+"'><i class='zmdi zmdi-delete' style='color:#fff;'></i></button>";
                        var image_poster = "<img src='"+poster+"' style='height:30%; width:auto;'/>";
                        dataSet.push([i+1, name, description, price, image_poster, program_name, action],);
                    }

                    $('#tables_listPackage').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="packageList"></table>' );
             
                    $('#packageList').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Package Name"},
                            { "title": "Package Description"},
                            { "title": "Package Price"},
                            { "title": "Package Poster"},
                            { "title": "Group"},
                            { "title": "Action" }
                        ]
                    });  
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>logout';
                }
                else
                {
                    $('#tables_listPackage').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="packageList"></table>' );
             
                    $('#packageList').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Package Name"},
                            { "title": "Package Description"},
                            { "title": "Package Price"},
                            { "title": "Package Poster"},
                            { "title": "Group"},
                            { "title": "Action" }
                        ]
                    }); 
                }
            }
        });             
		
		$(document).on('click', '#nextPackage', function(){
			var name 			= $("#val_packagename").val();
        	var description 	= $("#val_packagedescription").val();
        	var program_id 		= $("#select_program").val();
            var type 			= $("#select_type").val();
            var jenjang_id 		= $("#select_jenjang").val();
            var bidang 			= $("#val_packagebidang").val();
            var fasilitas 		= $("#val_packagefisilitas").val();  
            var imageData 		= $("#image_new").val();
            var imageDataAndro 	= $("#image_new_android").val(); 
            var expired_package = $("#expired_start").val();

            if (name == null || name == "") {
            	billing_type = [];
            	$("#alert_description").css('display','none');
            	$("#alert_type").css('display','none');
            	$("#alert_jenjang").css('display','none');
            	$("#alert_fasilitas").css('display','none');
            	$("#alert_bidang").css('display','none');
            	$("#alert_price").css('display','none');
                $("#alert_date_expired").css('display','none');
                $("#alert_name").css('display','block');
            }
            else if(description == ""){
            	billing_type = [];
            	$("#alert_name").css('display','none');
            	$("#alert_type").css('display','none');
            	$("#alert_jenjang").css('display','none');
            	$("#alert_fasilitas").css('display','none');
            	$("#alert_bidang").css('display','none');
            	$("#alert_price").css('display','none');
                $("#alert_date_expired").css('display','none');
            	$("#alert_description").css('display','block');	
            }
            else if(type == "" || type == null){
            	billing_type = [];
            	$("#alert_name").css('display','none');
            	$("#alert_description").css('display','none');	
            	$("#alert_jenjang").css('display','none');
            	$("#alert_fasilitas").css('display','none');
            	$("#alert_bidang").css('display','none');
            	$("#alert_price").css('display','none');
            	$("#alert_banner_android").css('display','none');
            	$("#alert_banner").css('display','none');
                $("#alert_date_expired").css('display','none');
            	$("#alert_type").css('display','block');
            }
            else if(jenjang_id == "" || jenjang_id == null){
            	billing_type = [];
            	$("#alert_name").css('display','none');
            	$("#alert_description").css('display','none');	
            	$("#alert_type").css('display','none');
            	$("#alert_fasilitas").css('display','none');
            	$("#alert_bidang").css('display','none');
            	$("#alert_price").css('display','none');
            	$("#alert_banner_android").css('display','none');
            	$("#alert_banner").css('display','none');
                $("#alert_date_expired").css('display','none');
            	$("#alert_jenjang").css('display','block');
            }
            else if(bidang == "" || bidang == null){
            	billing_type = [];
            	$("#alert_name").css('display','none');
            	$("#alert_description").css('display','none');	
            	$("#alert_jenjang").css('display','none');
            	$("#alert_type").css('display','none');
            	$("#alert_fasilitas").css('display','none');
            	$("#alert_price").css('display','none');
            	$("#alert_banner_android").css('display','none');
            	$("#alert_banner").css('display','none');
                $("#alert_date_expired").css('display','none');
            	$("#alert_bidang").css('display','block');
            }
            else if(fasilitas == "" || fasilitas == null){
            	billing_type = [];
            	$("#alert_name").css('display','none');
            	$("#alert_description").css('display','none');	
            	$("#alert_type").css('display','none');
            	$("#alert_jenjang").css('display','none');
            	$("#alert_bidang").css('display','none');
            	$("#alert_price").css('display','none');
            	$("#alert_banner_android").css('display','none');
            	$("#alert_banner").css('display','none');
                $("#alert_date_expired").css('display','none');
            	$("#alert_fasilitas").css('display','block');
            }
            else if (program_id == null) {
            	billing_type = [];
                $("#alert_name").css('display','none');
            	$("#alert_description").css('display','none');	
            	$("#alert_type").css('display','none');
            	$("#alert_jenjang").css('display','none');
            	$("#alert_bidang").css('display','none');
            	$("#alert_fasilitas").css('display','none');
            	$("#alert_price").css('display','none');
            	$("#alert_banner").css('display','none');
            	$("#alert_banner_android").css('display','none');
                $("#alert_date_expired").css('display','none');
            	$("#alert_program").css('display','block');
            }
            else if (expired_package == null || expired_package == "") {
            	billing_type = [];
                $("#alert_name").css('display','none');
                $("#alert_description").css('display','none');  
                $("#alert_type").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#alert_bidang").css('display','none');
                $("#alert_fasilitas").css('display','none');
                $("#alert_price").css('display','none');
                $("#alert_banner").css('display','none');
                $("#alert_banner_android").css('display','none');
                $("#alert_program").css('display','none');
                $("#alert_date_expired").css('display','block');
            }
            else if(imageData == "" || imageData == null){
            	billing_type = [];
            	$("#alert_name").css('display','none');
            	$("#alert_description").css('display','none');	
            	$("#alert_type").css('display','none');
            	$("#alert_jenjang").css('display','none');
            	$("#alert_bidang").css('display','none');
            	$("#alert_fasilitas").css('display','none');
            	$("#alert_price").css('display','none');
            	$("#alert_banner_android").css('display','none');
                $("#alert_date_expired").css('display','none');
            	$("#alert_banner").css('display','block');
            }
            else if(imageDataAndro == "" || imageDataAndro == null){
            	billing_type = [];
            	$("#alert_name").css('display','none');
            	$("#alert_description").css('display','none');	
            	$("#alert_type").css('display','none');
            	$("#alert_jenjang").css('display','none');
            	$("#alert_bidang").css('display','none');
            	$("#alert_fasilitas").css('display','none');
            	$("#alert_price").css('display','none');
            	$("#alert_banner").css('display','none');
                $("#alert_date_expired").css('display','none');
            	$("#alert_banner_android").css('display','block');
            }
            else
            {
				$("#boxOne").css('display','none');
				$("#nextPackage").css('display','none');
				$("#boxTwo").css('display','block');
				$("#prevPackage").css('display','block');
				$("#savePackage").css('display','block');
			}
		});

		$(document).on('click', '#prevPackage', function(){
			$("#boxTwo").css('display','none');
			$("#prevPackage").css('display','none');
			$("#savePackage").css('display','none');
			$("#boxOne").css('display','block');
			$("#nextPackage").css('display','block');
			
		});

		$(document).on('click', '.addBillingType', function(){			
			if (InputNo == 1) {
				$(".deleteBillingType").css('display','block');
				InputNo = InputNo+1;
				var boxInput = "<div id='boxx"+InputNo+"' class='col-md-12 row'><div class='col-md-12'>"+
	                "<br>"+
	                "<label>Billing Date</label>"+
	                "<div class='form-group'>"+
	                    "<select class='select2 form-control billing_type' required id='billing_type"+InputNo+"' name='billingtype[]' style='width: 100%;'>"+
	                        "<option disabled='disabled' selected=''>Choose Type</option>"+
	                        "<option value='byday'>By Day</option>"+
	                        "<option value='bycounter'>By Session</option>"+
	                    "</select>"+
	                    "<label style='color: red; display: none;' id='alert_billing_type"+InputNo+"'>Please choose banner</label>"+
	                "</div>"+
	            "</div>"+
	            
	            "</div>";
	            $("#boxBillingType2").append(boxInput);
	            $("#billing_type"+InputNo).select2();
            }       	
		});

		$(document).on('click', '.deleteBillingType', function(){
			if (InputNo == 1) {				
				
			}
			else if (InputNo == 2) {
				$("#boxx"+InputNo).remove();
				$(".deleteBillingType").css('display','none');
				InputNo = InputNo-1;
			}
			else
			{
				$("#boxx"+InputNo).remove();
				InputNo = InputNo-1;
			}
			
		});
		
		$(document).on('click', '.deleteBillingDate', function(){			
			if (noInput == 1) {				
				$(".deleteBillingDate").css('display','none');
				noInput = noInput-1;
			}
			else if (noInput == 2) {
				$("#box"+noInput).remove();
				$(".deleteBillingDate").css('display','none');
				noInput = noInput-1;
			}
			else
			{
				$("#box"+noInput).remove();
				noInput = noInput-1;
			}
			
		});

		$('#participant_eligibility').select2();
        $('#select_type').select2();
        $('#select_update_type').select2();
        $('#select_jenjang').select2();
        $('#select_update_jenjang').select2();
        $('#select_program').select2();
        $('#txt_program').select2();
        $('#val_packagebidang').select2({
          	tags:["",],
          	tokenSeparators: [","],
          	maximumSelectionLength: 10
        });
        $('#val_packagefisilitas').select2({
          	tags:["",],
          	tokenSeparators: [","],
          	maximumSelectionLength: 10
        });
        $('#val_update_packagebidang').select2({
          	tags:["",],
          	tokenSeparators: [","],
          	maximumSelectionLength: 10
        });
        $('#val_update_packagefisilitas').select2({
          	tags:["",],
          	tokenSeparators: [","],
          	maximumSelectionLength: 10
        });
        $("#billing_type1").select2();        
        $('#val_bycounter').select2({
          	tags:["",],
          	tokenSeparators: [","]          	
        });
        
        $('#billing_type1').change(function() {
        	var value = $(this).val();          	
        	if (value == 'byday') {
        		// if (noInput > 1) {
    			actv = 'byday';
    			// $(".boxAddOrDel").css('display','block');
    			if (noInput == 1) {
					$(".deleteBillingDate").css('display','block');
				}    			
				var boxInput = "<div id='box"+noInput+"' class='col-md-12 row'><div class='col-md-1'><br><label></label><div class='form-group'><label><h3>-</h3></label></div></div><div class='col-md-4'>"+
	                "<br>"+
	                "<label>Billing Date</label>"+
	                "<div class='form-group'>"+
	                    "<input type='date' class='form-control' required id='billing_date"+noInput+"' style='width:100%'>"+	                    
	                    "<label style='color: red; display: none;' id='alert_billing_type"+noInput+"'>Please choose banner</label>"+
	                "</div>"+
	            "</div>"+
	            "<div class='col-md-5'>"+
	                "<br>"+
	                "<label>Billing Price</label>"+
	                "<div class='input-group'>"+        
		                "<span class='input-group-addon'>Rp</span>"+ 
		                "<div class='form-group'>"+
	                        "<input type='number' min='1' step='1' class='form-control' required id='billing_price"+noInput+"' style='background-color: #ECF0F1; padding: 10px;' placeholder='Type Here...' data-mask='000.000.000.000.000,00'>"+
	                        "<i class='form-group__bar'></i>"+
	                    	"<label style='color: red; display: none;' id='alert_billing_type"+noInput+"'>Please enter price</label>"+
	                    "</div>"+
                    "</div>"+
	            "</div>"+
	            "<div class='col-md-2'>"+
	            	"<a class='form-control btn btn-default btn-block addBillingDate' title='Tambah Billing Type' style='color: grey;'>+</a>"+
		            "<a class='form-control btn btn-default btn-block deleteBillingDate' title='Hapus Billing Type' style='color: grey; display: none;'>-</a>"+
	            "</div></div>";
	            $("#boxBillingType1").append(boxInput);
        		// }
        		// else
        		// {
        		// 	$("#boxAddOrDel").css('display','none');
        		// }
        	}
        	else
        	{
        		actv = 'bycounter';    
        	}
        });
    
        $(document).on('click', '.addBillingDate', function(){
        	if (actv == 'byday') {
        		// if (noInput > 1) {
        		if (noInput >= 1) {
					$(".deleteBillingDate").css('display','block');
				}
				else if (noInput == 1) {
					$("#noInput"+noInput).css('display','none');
				}
    			noInput = noInput+1;
				var boxInput = "<div id='box"+noInput+"' class='col-md-12 row'><div class='col-md-1'><br><label></label><div class='form-group'><label><h3>-</h3></label></div></div><div class='col-md-4'>"+
	                "<br>"+
	                "<label>Billing Date</label>"+
	                "<div class='form-group'>"+
	                    "<input type='date' class='form-control' required id='billing_date"+noInput+"' style='width:100%'>"+	                    
	                    "<label style='color: red; display: none;' id='alert_billing_date"+noInput+"'>Please choose date</label>"+
	                "</div>"+
	            "</div>"+
	            "<div class='col-md-5'>"+
	                "<br>"+
	                "<label>Billing Price</label>"+
	                "<div class='input-group'>"+        
		                "<span class='input-group-addon'>Rp</span>"+ 
		                "<div class='form-group'>"+
	                        "<input type='number' min='1' step='1' class='form-control' required id='billing_price"+noInput+"' style='background-color: #ECF0F1; padding: 10px;' placeholder='Type Here...' data-mask='000.000.000.000.000,00'>"+
	                        "<i class='form-group__bar'></i>"+
	                    	"<label style='color: red; display: none;' id='alert_billing_price"+noInput+"'>Please enter price</label>"+
	                    "</div>"+
                    "</div>"+
	            "</div>"+
	            "<div class='col-md-2'>"+
	            	// "<a class='form-control btn btn-default btn-block addBillingDate' id='noAdd"+noInput+"' title='Tambah Billing Type' style='color: grey;'>+</a>"+
		            "<a class='form-control btn btn-default btn-block deleteBillingType' title='Hapus Billing Type' style='color: grey; display: none;'>-</a>"+
	            "</div></div>";
	            $("#boxBillingType1").append(boxInput);
        		// }
        		// else
        		// {
        		// 	$("#boxAddOrDel").css('display','none');
        		// }
        	}
        	else
        	{
        		actv = 'bycounter';
        	}
        });

        listprogram();

        function listprogram(pg_id){
        	$("#select_program").empty();
        	$("#txt_program").empty();
	        $.ajax({
	            url: '<?php echo AIR_API;?>listProgram/access_token/'+access_token,
	            type: 'POST',
	            data: {
	                channel_id: channel_id
	            },
	            success: function(response)
	            {	            	
	                var a = JSON.stringify(response); 
	                if (response['code'] == -400) {
	                    window.location.href='<?php echo base_url();?>logout';
	                }

	                for (var i = 0; i < response.data.length; i++) {
	                    var program_id = response['data'][i]['program_id'];
	                    var program_name = response['data'][i]['program_name'];   
	                    if (program_id == pg_id) {                
		                    $("#select_program").append("<option selected value='"+program_id+"'>"+program_name+"</option>");
		                    $("#txt_program").append("<option selected value='"+program_id+"'>"+program_name+"</option>");
	                    }
	                    else
	                    {
	                    	$("#select_program").append("<option value='"+program_id+"'>"+program_name+"</option>");
		                    $("#txt_program").append("<option value='"+program_id+"'>"+program_name+"</option>");
	                    }
	                }                
	            }
	        });
	    }

        listjenjang('-1');

        function listjenjang(jd_id){  
        	$("#select_jenjang").empty();
	        $.ajax({
	            url: '<?php echo AIR_API;?>ListJenjang/access_token/'+access_token,
	            type: 'POST',
	            data: {
	                curriculum: 'Indonesia'
	            },
	            success: function(response)
	            {
	                var a = JSON.stringify(response); 
	                if (response['code'] == -400) {
	                    window.location.href='<?php echo base_url();?>logout';
	                }
	                var listJenjang = response['list_jenjang'];
	                for (var i = 0; i < listJenjang.length; i++) {	                	
	                    var id = listJenjang[i]['id'];
	                    var text = listJenjang[i]['text']; 
	                    $("#select_jenjang").append("<option value='"+id+"'>"+text+"</option>");	                    
			            $("#select_update_jenjang").append("<option value='"+id+"'>"+text+"</option>");

			            if (jd_id != '-1') {
	                    for (var o = 0; o < jd_id.length; o++) {    	                   	 
		                    if (id == jd_id[o]) {
			                    $("#select_jenjang option[value='"+id+"']").attr("selected","selected");
			                    $("#select_update_jenjang option[value='"+id+"']").attr("selected","selected");
		                    }
	                    }
	                    }
	                }                
	            }
	        });
        }

        $(document).on('click', '.action_package', function(){
            var link_id    = $(this).data('list_id');            
            $('#proses_hapuspackage').attr('rtp',link_id);
            $("#modalDeletePackage").modal("show");
        });

        $(document).on('click', '.edit_package', function(){
            $("#body_status").empty();
            $("#select_update_jenjang").empty();
            var list_id    = $(this).data('list_id');
            var name    = $(this).data('name');
            var description    = $(this).data('description');
            var price    = $(this).data('price');            
            var jenjangid    = $(this).data('jenjangid');
            var bidang    = $(this).data('bidang');
            var fasilitas    = $(this).data('fasilitas');
            var program_id = $(this).data('programid');
            var expired_start = $(this).data('expiredstart');
            var expired_finish = $(this).data('expiredfinish');
            $("#txt_listid").val(list_id);
            $("#txt_packagename").val(name);
            $("#txt_packagedescription").val(description);
            $("#txt_packageprice").val(price);            
            $("#modalStatusPackage").modal("show");            
            // $("#expired_update_start").val(expired_start + ' – ' + expired_finish);
            if (bidang != "") {
	            for (var i = 0; i < bidang.length; i++) {
	            	$("#val_update_packagebidang").html("<option value='"+bidang[i]+"' selected>"+bidang[i]+"</option>");
	            }
            }
            if (fasilitas != "") {
	            for (var j = 0; j < fasilitas.length; j++) {
	            	$("#val_update_packagefisilitas").html("<option value='"+fasilitas[j]+"' selected>"+fasilitas[j]+"</option>");
	            }
            }
            listjenjang(jenjangid);
            listprogram(program_id);

        });

        $('#proses_hapuspackage').click(function(e){
            var link_id = $(this).attr('rtp');
            //menggunakan fungsi ajax untuk pengambilan data                        
            $.ajax({
                url :"<?php echo AIR_API;?>delete_PricePackage/access_token/"+access_token,
                type:"post",
                data: {
                    link_id: link_id
                },
                success: function(response){   
                    var code = response['code'];
                    if (code == 200) {

                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully deleted Package");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                        /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully deleted Student data");
                        $('#modalDeleteStudent').modal('hide');
                        setTimeout(function(){
                            window.location.reload();
                        },2000);*/
                    }   
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>logout';
                    }
                    else{
                        $('#modalDeletePackage').modal("hide");
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#text_modal").html("There is an error!!!");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                        /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"There is an error!!!");
                        setTimeout(function(){
                            window.location.reload();
                        },2000);*/
                    }                  
                } 
            });
        });

        function readFile() {
            $("#savePackage").attr('disabled','true');
            if (this.files && this.files[0]) {
                var FR= new FileReader();
                FR.addEventListener("load", function(e) {                    
                    document.getElementById("image_new").value = e.target.result;
                }); 
                FR.readAsDataURL( this.files[0] );
                $("#savePackage").removeAttr('disabled');
            } 
        }

        document.getElementById("poster").addEventListener("change", readFile);

        function readFileAndroid() {
            $("#savePackage").attr('disabled','true');
            if (this.files && this.files[0]) {
                var FR= new FileReader();
                FR.addEventListener("load", function(e) {                    
                    document.getElementById("image_new_android").value = e.target.result;
                }); 
                FR.readAsDataURL( this.files[0] );
                $("#savePackage").removeAttr('disabled');
            } 
        }

        document.getElementById("poster_android").addEventListener("change", readFileAndroid);

        $('#expired_start').daterangepicker({
		    opens: 'left'
			}, function(start, end, label) {
		    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
		});

		$('#expired_update_start').daterangepicker({
		    opens: 'left'
			}, function(start, end, label) {
		    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
		});

        function hilang_spasi(string) {
            return string.split(' ').join('');
        }

        $('#savePackage').click(function(e){
        	var name 			= $("#val_packagename").val();
        	var description 	= $("#val_packagedescription").val();
        	var program_id 		= $("#select_program").val();
            var type 			= $("#select_type").val();
            var jenjang_id 		= $("#select_jenjang").val();
            var bidang 			= $("#val_packagebidang").val();
            var fasilitas 		= $("#val_packagefisilitas").val();  
            var imageData 		= $("#image_new").val();
            var imageDataAndro 	= $("#image_new_android").val(); 
            var expired_package = $("#expired_start").val();
            var price 			= $("#val_packageprice").val();             
            var participant_eligibility = $("#participant_eligibility").val();

            // var expired_finish = $("#expired_finish").val();
            
            if(price == "" || price == null){
            	billing_type = [];
            	$("#alert_name").css('display','none');
            	$("#alert_description").css('display','none');	
            	$("#alert_type").css('display','none');
            	$("#alert_jenjang").css('display','none');
            	$("#alert_bidang").css('display','none');
            	$("#alert_fasilitas").css('display','none');
            	$("#alert_banner_android").css('display','none');
            	$("#alert_banner").css('display','none');
                $("#alert_date_expired").css('display','none');
            	$("#alert_price").css('display','block');
            }                        
            else{
            	for (var i = 1; i <= noInput; i++) {            		
	            	var billingdate = $("#billing_date"+i).val();	            	
	            	var billingprice = $("#billing_price"+i).val();	            	
	            	if (billingdate == null) {
	            		$("#alert_billing_price"+i).css('display','none');
	            		$("#alert_billing_date"+i).css('display','block');
	            		billing_type = [];
	            		break;
	            	}
	            	else if(billingprice == ""){
	            		$("#alert_billing_date"+i).css('display','none');
	            		$("#alert_billing_price"+i).css('display','block');
	            		billing_type = [];
	            		break;
	            	}
	            	else
	            	{
	            		$("#alert_billing_date"+i).css('display','none');
	            		$("#alert_billing_price"+i).css('display','none');
	            		billing_type.push({"type":"byday","options":[{"date":billingdate,"price":billingprice}]});
	            	}
	            } 
	            
            	$("#modalAddPackage").modal("hide");
            	var profil_img 		= imageData.split(";base64,")[1];
            	var expired_start 	= "";
            	var expired_finish 	= "";
            	var a = expired_package.split('-');
	            for (var i = 0; i < a.length; i++) {
	            	if (i == 0) {
		            	expired_start = hilang_spasi(a[i]);
		            }
		            else
		            {
		            	expired_finish = hilang_spasi(a[i]);
		            }
	            }	            
                $.ajax({
                    url : '<?php echo AIR_API;?>save_PricePackage/access_token/'+access_token,
                    type:"post",
                    data: {
                        name : name,
                        description : description,
                        price : price,
                        program_id : program_id,
                        type : type,
                        jenjang_id : jenjang_id,
                        bidang : bidang,
                        fasilitas : fasilitas,
                        channel_id: channel_id,
                        image_new : imageData,
                        image_new_android : imageDataAndro,
                        expired_start : expired_start,
                        expired_finish : expired_finish,
                        billing_type :  billing_type,
                        participant_eligibility : participant_eligibility
                    },
                    success: function(response){          
                        var code = response['code'];
                        if (code == 200) {
                            $('#modalAddPackage').modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#32c787');
                            $("#text_modal").html("Successfully add Package");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                        }
                        else if (code == -400) {
                            window.location.href='<?php echo base_url();?>logout';
                        }
                        else
                        {
                            $('#modalAddPackage').modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#ff3333');
                            $("#text_modal").html("There is an error!!!");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                        }
                        
                    } 
                });				            
            }
        });

        $('#updatePackage').click(function(e){ 
        	var list_id 		= $("#txt_listid").val();
        	var name 			= $("#txt_packagename").val();
        	var description 	= $("#txt_packagedescription").val();
            var price 			= $("#txt_packageprice").val(); 
            var program_id 		= $("#txt_program").val();
            var type 			= $("#select_update_type").val();
            var jenjang_id 		= $("#select_update_jenjang").val();
            var bidang 			= $("#val_update_packagebidang").val();
            var fasilitas 		= $("#val_update_packagefisilitas").val();  
            var imageData 		= $("#image_update_new").val();
            var imageDataAndro 	= $("#image_update_new_android").val(); 
            var expired_package = $("#expired_update_start").val();
            
            // var expired_finish = $("#expired_finish").val();
            if (name == null || name == "") {
            	$("#alert_update_description").css('display','none');
            	$("#alert_update_type").css('display','none');
            	$("#alert_update_jenjang").css('display','none');
            	$("#alert_update_fasilitas").css('display','none');
            	$("#alert_update_bidang").css('display','none');
            	$("#alert_update_price").css('display','none');
                $("#alert_update_date_expired").css('display','none');
                $("#alert_update_name").css('display','block');
            }
            else if(description == ""){
            	$("#alert_update_name").css('display','none');
            	$("#alert_update_type").css('display','none');
            	$("#alert_update_jenjang").css('display','none');
            	$("#alert_update_fasilitas").css('display','none');
            	$("#alert_update_bidang").css('display','none');
            	$("#alert_update_price").css('display','none');
                $("#alert_update_date_expired").css('display','none');
            	$("#alert_update_description").css('display','block');	
            }
            else if(type == "" || type == null){
            	$("#alert_update_name").css('display','none');
            	$("#alert_update_description").css('display','none');	
            	$("#alert_update_jenjang").css('display','none');
            	$("#alert_update_fasilitas").css('display','none');
            	$("#alert_update_bidang").css('display','none');
            	$("#alert_update_price").css('display','none');
            	$("#alert_banner_android").css('display','none');
            	$("#alert_banner").css('display','none');
                $("#alert_update_date_expired").css('display','none');
            	$("#alert_update_type").css('display','block');
            }
            else if(jenjang_id == "" || jenjang_id == null){
            	$("#alert_update_name").css('display','none');
            	$("#alert_update_description").css('display','none');	
            	$("#alert_update_type").css('display','none');
            	$("#alert_update_fasilitas").css('display','none');
            	$("#alert_update_bidang").css('display','none');
            	$("#alert_update_price").css('display','none');
            	$("#alert_banner_android").css('display','none');
            	$("#alert_banner").css('display','none');
                $("#alert_update_date_expired").css('display','none');
            	$("#alert_update_jenjang").css('display','block');
            }
            else if(bidang == "" || bidang == null){
            	$("#alert_update_name").css('display','none');
            	$("#alert_update_description").css('display','none');	
            	$("#alert_update_jenjang").css('display','none');
            	$("#alert_update_type").css('display','none');
            	$("#alert_update_fasilitas").css('display','none');
            	$("#alert_update_price").css('display','none');
            	$("#alert_banner_android").css('display','none');
            	$("#alert_banner").css('display','none');
                $("#alert_update_date_expired").css('display','none');
            	$("#alert_update_bidang").css('display','block');
            }
            else if(fasilitas == "" || fasilitas == null){
            	$("#alert_update_name").css('display','none');
            	$("#alert_update_description").css('display','none');	
            	$("#alert_update_type").css('display','none');
            	$("#alert_update_jenjang").css('display','none');
            	$("#alert_update_bidang").css('display','none');
            	$("#alert_update_price").css('display','none');
            	$("#alert_banner_android").css('display','none');
            	$("#alert_banner").css('display','none');
                $("#alert_update_date_expired").css('display','none');
            	$("#alert_update_fasilitas").css('display','block');
            }
            else if(price == "" || price == null){
            	$("#alert_update_name").css('display','none');
            	$("#alert_update_description").css('display','none');	
            	$("#alert_update_type").css('display','none');
            	$("#alert_update_jenjang").css('display','none');
            	$("#alert_update_bidang").css('display','none');
            	$("#alert_update_fasilitas").css('display','none');
            	$("#alert_banner_android").css('display','none');
            	$("#alert_banner").css('display','none');
                $("#alert_update_date_expired").css('display','none');
            	$("#alert_update_price").css('display','block');
            }
            else if (program_id == null) {
                $("#alert_update_name").css('display','none');
            	$("#alert_update_description").css('display','none');	
            	$("#alert_update_type").css('display','none');
            	$("#alert_update_jenjang").css('display','none');
            	$("#alert_update_bidang").css('display','none');
            	$("#alert_update_fasilitas").css('display','none');
            	$("#alert_update_price").css('display','none');
            	$("#alert_banner").css('display','none');
            	$("#alert_banner_android").css('display','none');
                $("#alert_update_date_expired").css('display','none');
            	$("#alert_program").css('display','block');
            }
            else if (expired_package == null || expired_package == "") {
                $("#alert_update_name").css('display','none');
                $("#alert_update_description").css('display','none');  
                $("#alert_update_type").css('display','none');
                $("#alert_update_jenjang").css('display','none');
                $("#alert_update_bidang").css('display','none');
                $("#alert_update_fasilitas").css('display','none');
                $("#alert_update_price").css('display','none');
                $("#alert_banner").css('display','none');
                $("#alert_banner_android").css('display','none');
                $("#alert_program").css('display','none');
                $("#alert_update_date_expired").css('display','block');
            }
            else{  
            	var expired_start 	= "";
            	var expired_finish 	= "";
            	var a = expired_package.split('-');
	            for (var i = 0; i < a.length; i++) {
	            	if (i == 0) {
		            	expired_start = hilang_spasi(a[i]);
		            }
		            else
		            {
		            	expired_finish = hilang_spasi(a[i]);
		            }
	            }
	            return false;          	
                $.ajax({
                    url : '<?php echo AIR_API;?>update_PricePackage/access_token/'+access_token,
                    type:"post",
                    data: {
                        list_id : list_id,
                        name : name,
                        description : description,
                        price : price,
                        program_id : program,
                        type : type,
                        jenjang_id : jenjang_id,
                        bidang : bidang,
                        fasilitas : fasilitas,
                        imageData : imageData,
                        imageDataAndro : imageDataAndro,
                        expired_start : expired_start,
                        expired_finish : expired_finish
                    },
                    success: function(response){          
                        var code = response['code'];
                        if (code == 200) {
                            $('#modalStatusPackage').modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#32c787');
                            $("#text_modal").html("Successfully Update Package");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                        }
                        else if (code == -400) {
                            window.location.href='<?php echo base_url();?>logout';
                        }
                        else
                        {
                            $('#modalAddPackage').modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#cf000f');
                            $("#text_modal").html("There is an error!!!");
                            $("#button_ok").click( function(){
                                location.reload();
                            });			                            
                        }
                        
                    } 
                });			            
            }
        });

    });

</script>
