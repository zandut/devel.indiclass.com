<main class="main">

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>List Group</h1>
            <small>Details of the group list in your channel</small>

            <div class="actions">
                <a data-toggle="modal" href="#modalAddGroup"><button class="btn btn-success">+ Add Group</button></a>
            </div>
        </header>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">
                    <div class="row table-responsive">
                        <div id="tables_listGroup"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" >
                <div class="modal-content" id="modal_konten">
                    <div class="modal-body" align="center">
                        <label id="text_modal">Halloo</label><br>
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal modalAddTutor -->  
        <div class="modal fade show" id="modalAddGroup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">                        
                        <h4 class="modal-title c-white pull-left">Add Group</h4>
                        <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    </div>
                    <div class="modal-body m-t-20">
                        <form id="frmdata">
                            <div class="col-md-12" style="padding: 0;">
                                <div class="col-md-12">
                                    <div class="form-group form-group--float">
                                        <input type="text" class="form-control" id="group_name">
                                        <label>Name group</label>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Participants Group</label><br>                                    
                                    <div class="col-md-12 m-t-10">                                          
                                        <select required name="taggroup[]" id="taggroup" multiple class="select2 taggroup" style="width: 100%;">                                                
                                        </select>                                           
                                    </div>
                                </div>                                
                            </div>
                        </form>
                        <br>
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-success waves-effect btn-block p-10" style="margin-top: 2%;" id="saveGroup">Save</button>                            
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade show" id="modalDeleteGroup" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Delete Group</b></h3>
                    </div>
                    <div class="modal-body">                        
                        <label class="c-gray f-15">Are you sure you want to delete?</label>                         
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-danger waves-effect" id="proses_hapusgroup" data-id="" rtp="">Yes</button>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>      

<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = [];
    var user_utc = new Date().getTimezoneOffset();
        user_utc = 1 * user_utc;

    $(document).ready(function() {

        $('#select_listtutor').select2();

        $.ajax({
            url: '<?php echo AIR_API;?>listGroup/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id,
                user_utc: user_utc
            },
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                  
                if (code == 200) {
                    for (var i = 0; i < response.data.length; i++) {
                        var group_id        = response['data'][i]['group_id'];
                        var group_name      = response['data'][i]['group_name'];
                        var Participant     = response['data'][i]['participant'];

                        dataSet.push([i+1, group_name, Participant, "<a href='<?php echo BASE_URL();?>Channel/EditGroup?idg="+group_id+"'<button class='editGroup btn waves-effect' style='background-color: #F89406;' data-groupid='"+group_id+"' title='Edit Group'><i class='zmdi zmdi-edit zmdi-hc-fw' style='color:#fff;'></i></button></a> <button class='deleteGroup btn waves-effect' style='background-color: #D91E18;' data-groupid='"+group_id+"' title='Delete Group'><i class='zmdi zmdi-delete zmdi-hc-fw' style='color:#fff;'></i></button>"]);
                    }                        

                    $('#tables_listGroup').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="listGroup"></table>' );
             
                    $('#listGroup').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Name Group"},
                            { "title": "Participant"},
                            { "title": "Action"},                            
                        ]
                    });
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>logout';
                }
                else
                {
                    $('#tables_listGroup').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="listGroup"></table>' );
             
                    $('#listGroup').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Name Group"},
                            { "title": "Participant"},
                            { "title": "Action"},                            
                        ]
                    });
                }
            }
        }); 
        
        $(document).on('click', '.deleteGroup', function(){
            var groupid    = $(this).data('groupid');
            $('#proses_hapusgroup').attr('rtp',groupid);
            $("#modalDeleteGroup").modal("show");
        });

        $("select.select2").select2({
            delay: 2000,                            
            ajax: {
                dataType: 'json',
                type: 'GET',
                url: '<?php echo AIR_API;?>getListNameGroup/access_token/'+access_token+'?channel_id='+channel_id,
                data: function (params) {
                    return {
                      term: params.term,
                      page: params.page || 1
                    };
                },
                processResults: function(data){
                    return {
                        results: data.results,
                        pagination: {
                            more: data.more
                        }                       
                    };
                }                   
            }
        });   

        $(document).on("click", "#saveGroup", function(){
            $('#modalAddGroup').modal('hide');
            var group_name          = $("#group_name").val();
            var group_participants  = $("#taggroup").val();
            $.ajax({
                url : '<?php echo AIR_API;?>channel_addStudentToGroup/access_token/'+access_token,
                type:"post",
                data: {
                    group_name: group_name,
                    group_participants: group_participants,
                    channel_id: channel_id
                },
                success: function(response){             
                    var code = response['code'];
                    if (code == 200) {
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully Added Group");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                        /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Successfully Added Group');
                        setTimeout(function(){                            
                            location.reload();
                        },2000);*/
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>logout';
                    }
                    else
                    {
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#text_modal").html("Please complete the data !!!");
                        $("#button_ok").click( function(){
                            $("#modalAddGroup").modal('show');
                        });
                        /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','There is an error!!!');
                        setTimeout(function(){
                            location.reload();
                        },2000);*/
                    }
                }
            });
        });

        $(document).on("click", "#proses_hapusgroup", function(){   
            $('#modalDeleteGroup').modal('hide');
            var group_id = $(this).attr('rtp');
            $.ajax({
                url :"<?php echo AIR_API;?>deleteGroup/access_token/"+access_token,
                type:"post",
                data: {
                    rtp: group_id,
                    channel_id: channel_id
                },
                success: function(response){   
                    var code = response['code'];
                    if (code == 200) {
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully Removing Group");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                        /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Successfully Added Group');
                        setTimeout(function(){                            
                            location.reload();
                        },2000);*/
                    }   
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>logout';
                    }
                    else{                        
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#text_modal").html("There is an error!!!");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                        /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','There is an error!!!');
                        setTimeout(function(){
                            location.reload();
                        },2000);*/
                    }                  
                } 
            });
        });
    });
</script>  