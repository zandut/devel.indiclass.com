<main class="main">

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>Point Report</h1>
            <small>Details of point usage on your channel</small>
        </header>

        <div class="row quick-stats">
            <div class="col-sm-6 col-md-3">
                <div class="quick-stats__item bg-light-blue">
                    <div class="quick-stats__info">
                        <h2 id="chart_totalpoint"></h2>
                        <small>Total Point Channel</small>
                    </div>                        
                </div>
            </div>           
        </div>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">
                    <div class="row table-responsive">
                        <div id="tables_reportPoint"></div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>   

<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = [];

    $(document).ready(function() {
        $.ajax({
            url: '<?php echo AIR_API;?>reportChannel/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id,
                type: 'channel'
            },
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                
                if (code == 200) {                     
                    for (var i = 0; i < response.data.length; i++) {
                        var name                = response['data'][i]['name'];
                        var description         = response['data'][i]['description'];
                        var user_name           = response['data'][i]['user_name'];
                        var start_time          = response['data'][i]['start_time'];
                        start_time = moment(start_time).format('HH:ss');
                        var finish_time         = response['data'][i]['finish_time'];
                        finish_time = moment(finish_time).format('HH:ss');
                        var duration            = response['data'][i]['duration'];
                        var participant_listt   = response['data'][i]['participant']['participant'];
                        var count_student       = participant_listt.length;
                        var class_type          = response['data'][i]['class_type'];
                        var last_point          = response['data'][i]['last_point'];
                        var amount              = response['data'][i]['amount'];
                        var new_point           = response['data'][i]['new_point'];
                        var created_at          = response['data'][i]['created_at'];    
                        var created_at_point    = response['data'][i]['created_at_point'];    
                       
                        var hours = Math.floor(duration / 3600);
                        var min = Math.floor((duration - (hours*3600)) / 60);
                        var seconds = Math.floor(duration % 60);

                        var value = "";
                        if(min > 0) {
                            if(min == 1) {
                               value = " Menit ";
                            } else {
                               value = " Menit " + value;
                            }
                            value = min + value;
                        }

                        if(hours > 0) {
                            if(hours == 1) {
                               value = " Jam " + value;
                            } else {
                               value = " Jam " + value;
                            }
                            value = hours + value;
                        }    
                        
                        if (class_type == "multicast_channel_paid") {
                            class_type = "Multicast";
                        }
                        else if (class_type == "private_channel") {
                            class_type = "Private";
                        }
                        else
                        {
                            class_type = "Group";
                        }

                        dataSet.push([i+1, name, description, user_name, created_at_point, start_time, finish_time, duration, count_student, class_type, last_point, amount, new_point]);
                    }                        

                    $('#tables_reportPoint').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="reportList"></table>' );
             
                    $('#reportList').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Class Name"},
                            { "title": "Description"},
                            { "title": "Tutor"},
                            { "title": "Date"},
                            { "title": "Start Time"},
                            { "title": "End Time"},
                            { "title": "Duration"},
                            { "title": "Number of participants"},
                            { "title": "Type Class"},
                            { "title": "Starting Point"},
                            { "title": "Point Used"},
                            { "title": "Final Point" }                            
                        ]
                    });
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>logout';
                }
                else
                {
                    $('#tables_reportPoint').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="reportList"></table>' );
             
                    $('#reportList').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Class Name"},
                            { "title": "Description"},
                            { "title": "Tutor"},
                            { "title": "Date"},
                            { "title": "Start Time"},
                            { "title": "End Time"},
                            { "title": "Duration"},
                            { "title": "Number of participants"},
                            { "title": "Type Class"},
                            { "title": "Starting Point"},
                            { "title": "Point Used"},
                            { "title": "Final Point" }
                        ]
                    });
                }
            }
        }); 
        

        $.ajax({
            url: '<?php echo AIR_API;?>pointChannel/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                
                if (code == 200) {
                    var active_point = response['data']['active_point'];
                    $("#chart_totalpoint").text(active_point);
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>logout';
                }
            }
        });

    });
</script>