<main class="main">

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>Program List</h1>
            <small>Detail list program in Your Channel</small>

            <div class="actions">
                <a href="<?php echo base_url();?>Channel/addProgram"><button class="btn btn-success">+ Add Program</button></a>
            </div>
        </header>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">    
                    <div class="row table-responsive">                
                        <div id="tables_listGroup"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal modalAddTutor -->  
        <div class="modal fade show" id="modalAddProgram">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">                        
                        <h4 class="modal-title c-white pull-left">Add Program</h4>
                        <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    </div>
                    <div class="modal-body m-t-20">
                        
                        <br>
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-success waves-effect btn-block p-10" style="margin-top: 2%;" id="saveGroup">Save</button>                            
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade show" id="modalDeleteProgram" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Delete Program</b></h3>
                    </div>
                    <div class="modal-body">                        
                        <label class="c-gray f-15">Are You sure want to delete ?</label>                         
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-danger waves-effect" id="proses_hapusprogram" data-id="" rtp="">Yes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" >
                <div class="modal-content" id="modal_konten" style="background-color: #ff1a1a" >
                    <div class="modal-body" align="center">
                        <label id="text_modal">Halloo</label><br>
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>      

<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = [];
    var user_utc = new Date().getTimezoneOffset();
        user_utc = 1 * user_utc;
    var maxSchedule = 3;

    $(document).ready(function() {

        $('#select_listtutor').select2();

        $.ajax({
            url: '<?php echo AIR_API;?>listProgram/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id,
                user_utc: user_utc
            },
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                  
                if (code == 200) {
                    for (var i = 0; i < response.data.length; i++) {
                        var program_id      = response['data'][i]['program_id'];
                        var program_name    = response['data'][i]['program_name'];
                        var sesi            = response['data'][i]['sesi'];
                        var group_name      = response['data'][i]['group_name'];                        
                        var schedule        = response['data'][i]['schedule'];                        

                        dataSet.push([i+1, program_name, group_name, sesi, "<a href='<?php echo base_url();?>Channel/modifyClass?pid="+program_id+"'><button class='modifyClass btn waves-effect' style='background-color: #607D8B;' data-programid='"+program_id+"'><i class='zmdi zmdi-comment-edit zmdi-hc-fw' style='color:#fff;'></i></button></a>","<button class='deleteProgram btn waves-effect' style='background-color: #607D8B;' data-programid='"+program_id+"'><i class='zmdi zmdi-delete zmdi-hc-fw' style='color:#fff;'></i></button>"]);
                    }                        

                    $('#tables_listGroup').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="listProgram"></table>' );
             
                    $('#listProgram').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Name Program"},
                            { "title": "Name Group"},
                            { "title": "Number of Sessions"},
                            // { "title": "Schedule"},                            
                            { "title": "Modify Class", "width" : 30},
                            { "title": "Action", "width" : 30}
                        ]
                    });
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>logout';
                }
                else
                {
                    $('#tables_listGroup').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="listProgram"></table>' );
             
                    $('#listProgram').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Name Program"},
                            { "title": "Name Group"},
                            { "title": "Number of Sessions"},
                            // { "title": "Schedule"},                            
                            { "title": "Modify Class", "width" : 30},
                            { "title": "Action", "width" : 30},
                        ]
                    });
                }
            }
        }); 
        
        $(document).on('click', '.deleteProgram', function(){
            var programid    = $(this).data('programid');
            $('#proses_hapusprogram').attr('rtp',programid);
            $("#modalDeleteProgram").modal("show");
        });

        $("select.select2").select2({
            delay: 2000,                            
            ajax: {
                dataType: 'json',
                type: 'GET',
                url: '<?php echo AIR_API;?>getListNameGroup/access_token/'+access_token+'?channel_id='+channel_id,
                data: function (params) {
                    return {
                      term: params.term,
                      page: params.page || 1
                    };
                },
                processResults: function(data){
                    return {
                        results: data.results,
                        pagination: {
                            more: data.more
                        }                       
                    };
                }                   
            }
        });   

        $(document).on("click", "#proses_hapusprogram", function(){   
            $('#modalDeleteProgram').modal('hide');
            var program_id = $(this).attr('rtp');
            $.ajax({
                url :"<?php echo AIR_API;?>deleteProgram/access_token/"+access_token,
                type:"post",
                data: {
                    rtp: program_id,
                    channel_id: channel_id
                },
                success: function(response){   
                    var code = response['code'];
                    if (code == 200) {
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#button_ok").click( function(){
                           window.location.reload();
                        });
                        $("#text_modal").html("Successfully deleted the Program");
                        /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully deleted the Program");                        
                        setTimeout(function(){
                            window.location.reload();
                        },2000);*/
                    } 
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>logout';
                    }  
                    else{               
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#button_ok").click( function(){
                           window.location.reload();
                        });
                        $("#text_modal").html("There is an error!!!");         
                        /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"There is an error!!!");
                        setTimeout(function(){
                            window.location.reload();
                        },2000);*/
                    }                  
                } 
            });
        });
    });
</script>  