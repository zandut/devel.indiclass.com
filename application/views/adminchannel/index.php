<main class="main">
    <div class="page-loader">
        <div class="page-loader__spinner">
            <svg viewBox="25 25 50 50">
                <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>Dashboard</h1>            

            <div class="actions">
                    <a href="" class="actions__item zmdi zmdi-trending-up"></a>
                    <a href="" class="actions__item zmdi zmdi-check-all"></a>

                    <div class="dropdown actions__item">
                        <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="" class="dropdown-item">Refresh</a>
                            <a href="" class="dropdown-item">Manage Widgets</a>
                            <a href="" class="dropdown-item">Settings</a>
                        </div>
                    </div>
                </div>
        </header>

        <div class="row quick-stats">
            <div class="col-sm-6 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">Program Channel</h2>
                        <small class="card-subtitle">Number of Channel Program Details</small>
                    </div>

                    <div class="card-block">
                        <div class="quick-stats__item bg-light-blue">
                            <div class="quick-stats__info">
                                <h2 id="myDashProgram"></h2>
                                <small>Total Program</small>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

            <div class="col-sm-6 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">Class Session Channel</h2>
                        <small class="card-subtitle">Detail Class Session Channel</small>
                    </div>

                    <div class="card-block">
                        <div class="quick-stats__item bg-amber">
                            <div class="quick-stats__info">
                                <h2 id="myDashClass"></h2>
                                <small>Total Session Class</small>
                            </div>                            
                        </div>
                    </div>
                </div>                
            </div>

            <div class="col-sm-6 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">Tutor Channel</h2>
                        <small class="card-subtitle">Detail Tutor Channel</small>
                    </div>

                    <div class="card-block">
                        <div class="quick-stats__item bg-purple">
                            <div class="quick-stats__info">
                                <h2 id="myDashTutor"></h2>
                                <small>Total Tutor</small>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">Participant Channel</h2>
                        <small class="card-subtitle">Detail Participant Channel</small>
                    </div>

                    <div class="card-block">
                        <div class="quick-stats__item bg-red">
                            <div class="quick-stats__info">
                                <h2 id="myDashParti"></h2>
                                <small>Total Participant</small>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">Group Channel</h2>
                        <small class="card-subtitle">Detail Group Channel</small>
                    </div>

                    <div class="card-block">
                        <div class="quick-stats__item bg-green">
                            <div class="quick-stats__info">
                                <h2 id="myDashGroup"></h2>
                                <small>Total Group</small>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
</main>        

<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";    
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";

    $(document).ready(function() {
        $.ajax({
            url: '<?php echo AIR_API;?>dashboardChannel/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response); 
                if (response['code'] == -400) {
                    window.location.href='<?php echo base_url();?>logout';
                }

                var total_program   = response['total_program'];
                var total_group     = response['total_group'];
                var total_student   = response['total_student'];
                var total_tutor     = response['total_tutor'];
                var total_class     = response['total_class'];
                $("#myDashProgram").text(total_program);
                $("#myDashGroup").text(total_group);
                $("#myDashParti").text(total_student);
                $("#myDashTutor").text(total_tutor);
                $("#myDashClass").text(total_class);
            }
        });
    });
</script>