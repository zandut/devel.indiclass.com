<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> -->
<main class="main">

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>Edit Program</h1>
            <small>Edit program in your channel</small>

            <div class="actions">
                <a href="<?php echo base_url();?>Channel/listProgram"><button class="btn btn-secondary"><i class="zmdi zmdi-arrow-right"></i> Back</button></a>
            </div>
        </header>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">
                    <div class="row">
                    <div class="col-md-6">
                        <h3 class="card-block__title">Program Name</h3>
                        <div class="form-group">
                            <input type="text" class="form-control" required placeholder="Type Here" id="program_name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3 class="card-block__title">Count Session</h3>
                        <div class="form-group">
                            <input type="number" class="form-control" required name="sesi" min="1" max="31" placeholder="Choose Here" id="sesi">  
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3 class="card-block__title">Group</h3>
                        <div class="form-group">
                            <select class="select2 form-control" required id="select_group" style="width: 100%;">
                                <option disabled="disabled" selected="" value="0">Choose Group</option>                            
                            </select>   
                        </div>
                    </div>
                    <div class="col-md-12" id="boxLanjut" style="display: block;">
                        <button type="button" class="btn btn-lg btn-primary btn-block waves-effect" id="showCalendar">Next</button>
                    </div>
                    <div class="col-md-12" id="boxCalendar" style="display: none;">
                        <h3 class="card-block__title">Please select for every session</h3><br>
                        <header class="content__title content__title--calendar">
                            <h1>Add Program<</h1>                        

                            <div class="actions actions--calendar" style="margin-right: 1%;">
                                <a href="" class="actions__item actions__calender-prev"><i class="zmdi zmdi-long-arrow-left"></i></a>
                                <a href="" class="actions__item actions__calender-next"><i class="zmdi zmdi-long-arrow-right"></i></a> 
                                <a href="" class="ubahDataCalendar"><button class="btn btn-success">Change Data</button></a> 

                            </div>
                        </header>
                        <div class="calendar card"></div>
                    </div>                                        
                    <div class="col-md-12" id="boxSaveCalendar" style="display: none;">
                        <button type="button" class="btn btn-lg btn-primary btn-block waves-effect" id="saveProgram">Save</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" >
                <div class="modal-content" id="modal_konten">
                    <div class="modal-body" align="center">
                        <label id="text_modal">Halloo</label><br>
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>
                </div>
            </div>
        </div>

        <!-- Add new event -->
        <div class="modal fade" id="new-event" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Please choose your Time Session</h5>
                    </div>
                    <div class="modal-body">
                        <form class="new-event__form">            
                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                <div class="form-group">
                                    <!-- <input type='text' class='form-control new-event__title' id='timeee' placeholder='Waktu' /> -->
                                    <input type="text" id="timeee" class="form-control time-picker floating-label new-event__title" placeholder="Choose Time">
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="input-group" style="margin-top: 5%;">
                                <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                <div class="form-group" style="margin-top: 3%;">
		                            <select class="select2 form-control" required id="durasi_sesi" style="width: 100%;">
		                                <option disabled="disabled" selected="" value="0">Choose Duration</option>
		                                <option value="900">15 Minutes</option>
	                                    <option value="1800">30 Minutes</option>
	                                    <option value="2700">45 Minutes</option>
	                                    <option value="3600">1 Hours</option>
                                        <option value="5400">1 Hours 30 Minutes</option>
                                        <option value="7200">2 Hours</option>
                                        <option value="9000">2 Hours 30 Minutes</option>
                                        <option value="10800">3 Hours</option>
                                        <option value="12600">3 Hours 30 Minutes</option>
                                        <option value="14400">4 Hours</option>
                                        <!-- <option value="16200">4 Hours 30 Minutes</option>
                                        <option value="18000">5 Hours</option>
                                        <option value="19800">5 Hours 30 Minutes</option>
                                        <option value="21600">6 Hours</option>
                                        <option value="23400">6 Hours 30 Minutes</option>
                                        <option value="25200">7 Hours</option>
                                        <option value="27000">7 Hours 30 Minutes</option>
                                        <option value="28800">8 Hours</option>
                                        <option value="30600">8 Hours 30 Minutes</option>
                                        <option value="32400">9 Hours</option>
                                        <option value="34200">9 Hours 30 Minutes</option>
                                        <option value="36000">10 Hours</option>
                                        <option value="37800">10 Hours 30 Minutes</option>
                                        <option value="39600">11 Hours</option>
                                        <option value="41400">11 Hours 30 Minutes</option>
                                        <option value="43200">12 Hours</option>
                                        <option value="45000">12 Hours 30 Minutes</option>
                                        <option value="46800">13 Hours</option>
                                        <option value="48600">13 Hours 30 Minutes</option>
                                        <option value="50400">14 Hours</option>
                                        <option value="52200">14 Hours 30 Minutes</option>
                                        <option value="54000">15 Hours</option>
                                        <option value="55800">15 Hours 30 Minutes</option>
                                        <option value="57600">16 Hours</option>
                                        <option value="59400">16 Hours 30 Minutes</option>
                                        <option value="61200">17 Hours</option>
                                        <option value="63000">17 Hours 30 Minutes</option>
                                        <option value="64800">18 Hours</option>
                                        <option value="66600">18 Hours 30 Minutes</option>
                                        <option value="68400">19 Hours</option>
                                        <option value="70200">19 Hours 30 Minutes</option>
                                        <option value="72000">20 Hours</option>
                                        <option value="73800">20 Hours 30 Minutes</option>
                                        <option value="75600">21 Hours</option>
                                        <option value="77400">21 Hours 30 Minutes</option>
                                        <option value="79200">22 Hours</option>
                                        <option value="81000">22 Hours 30 Minutes</option>
                                        <option value="82800">23 Hours</option>
                                        <option value="84600">23 Hours 30 Minutes</option>
                                        <option value="86400">24 Hours</option>   -->
		                            </select>   
		                        </div>
                            </div> 
                            

                            <input type="hidden" class="new-event__start" />
                            <input type="hidden" class="new-event__end" />
                        </form>
                    </div>

                    <div class="modal-footer" style="background-color: #e5e5e5;">                        
                        <button type="button" class="btn btn-link" data-dismiss="modal" style="margin-top: 3%;">Cancel</button>
                        <button type="submit" class="btn btn-success new-event__add" style="margin-top: 3%;">Choose Session</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Edit event -->
        <div class="modal fade" id="edit-event" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                	<div class="modal-header">
                		<h5 class="modal-title">Hapus Sesi</h5>
                	</div>
                    <div class="modal-body">
                        <form class="edit-event__form">
                            <div class="form-group" hidden>
                                <input type="text" class="form-control edit-event__title" placeholder="Event Title">
                                <i class="form-group__bar"></i>
                            </div>

                            <div class="btn-group btn-group--colors event-tag" data-toggle="buttons" hidden>
                                <label class="btn bg-light-blue active"><input type="radio" name="event-tag" value="bg-light-blue" autocomplete="off" checked></label>
                                <label class="btn bg-teal"><input type="radio" name="event-tag" value="bg-teal" autocomplete="off"></label>
                                <label class="btn bg-red"><input type="radio" name="event-tag" value="bg-red" autocomplete="off"></label>
                                <label class="btn bg-purple"><input type="radio" name="event-tag" value="bg-purple" autocomplete="off"></label>
                                <label class="btn bg-amber"><input type="radio" name="event-tag" value="bg-amber" autocomplete="off"></label>
                                <label class="btn bg-cyan"><input type="radio" name="event-tag" value="bg-cyan" autocomplete="off"></label>
                            </div>

                            <div class="form-group" hidden>
                                <textarea class="form-control edit-event__description textarea-autosize" placeholder="Event Desctiption"></textarea>
                                <i class="form-group__bar"></i>
                            </div>

                            <label class="c-gray f-15">Are You sure want to delete ?</label> 
                            <input type="hidden" class="edit-event__id">
                        </form>
                    </div>

                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button class="btn btn-link" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-danger waves-effect" data-calendar="delete">Delete</button>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>        

<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";    
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = ''
    var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;    
    var countSesi = 0;
    var checkin = "";
    $(document).ready(function() {       
        
        // $('#timeee').datetimepicker({                    
        //     sideBySide: true, 
        //     showClose: true,                   
        //     format: 'HH:mm',
        //     stepping: 15,
        //     enabledHours: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],
        // });
        // $('#timeee').datetimepicker(optionsTime);

        var getUrlParameter = function getUrlParameter(sParam) {
		    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		        sURLVariables = sPageURL.split('&'),
		        sParameterName,
		        i;

		    for (i = 0; i < sURLVariables.length; i++) {
		        sParameterName = sURLVariables[i].split('=');

		        if (sParameterName[0] === sParam) {
		            return sParameterName[1] === undefined ? true : sParameterName[1];
		        }
		    }
		};

		var program_id = getUrlParameter("pid");

        $.ajax({
            url: '<?php echo AIR_API;?>listProgramFromProgramId/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id,
                program_id: program_id
            },
            success: function(response)
            {
            	var code = response['code'];
            	if (code == '-400') {
            		window.location.href="<?php echo BASE_URL();?>logout";
            	}
            	else
            	{
            		var program_name = response['data']['program_name'];
            		var sesi = response['data']['sesi'];
            		var group_id = response['data']['group_id'];
	            	var schedule = response['data']['schedule'];
	            	$("#program_name").val(program_name);
	            	$("#sesi").val(sesi);
	            	listGroup(group_id);
	            	// dataSet = schedule;
	            	console.log('INI SCHEDULE AWAL : '+dataSet);
            	}
            }
        });
        
        $('#timeee').datetimepicker   
        ({
            sideBySide: true, 
            showClose: true,                   
            format: 'HH:mm',
            stepping: 15,
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-angle-up',
                down: 'fa fa-angle-down',
                previous: 'fa fa-angle-left',
                next: 'fa fa-angle-right',
                today: 'fa fa-dot-circle-o',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
        });

        function calendarBox(){
            var date = new Date();
            var m = date.getMonth();
            var y = date.getFullYear();        
            var data = [
                {
                    id: 1,
                    title: 'D',
                    start: new Date(y, m, 1),
                    allDay: true,
                    className: 'bg-cyan',
                    description: 'Nullam id dolor id nibh ultricies vehicula ut id elit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'
                },
                {
                    id: 2,
                    title: 'Fusce dapibus tellus',
                    start: new Date(y, m, 10),
                    allDay: true,
                    className: 'bg-amber',
                    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
                },
                {
                    id: 3,
                    title: 'Egestas Justo',
                    start: new Date(y, m, 18),
                    allDay: true,
                    className: 'bg-green',
                    description: ''
                },
                {
                    id: 4,
                    title: 'Bibendum Vehicula Quam',
                    start: new Date(y, m, 20),
                    allDay: true,
                    className: 'bg-blue',
                    description: ''
                },
                {
                    id: 5,
                    title: 'Venenatis Dolor Porta',
                    start: new Date(y, m, 5),
                    allDay: true,
                    className: 'bg-teal',
                    description: 'Sed posuere consectetur est at lobortis. Nullam quis risus eget urna mollis ornare vel eu leo. Aenean lacinia bibendum nulla sed consectetur. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.'
                }
            ];

            $('.calendar').fullCalendar({
                header: {
                    right: '',
                    center: '',
                    left: ''
                },
                buttonIcons: {
                    prev: 'calendar__prev',
                    next: 'calendar__next'
                },                
                theme: false,
                selectable: true,
                selectHelper: true,
                editable: false,
                events: dataSet,
                select: function(start, end, allDay) {      
                    var isoDate = moment(start).format("YYYY-MM-DD");
                    var daa = moment(date).format("YYYY-MM-DD");                      
                    // alert(daa);
                    // console.log(moment("2015-07-15").format("DD MMMM YYYY"));

                    if(daa > isoDate) {     
                        $("#modal_alert").modal('show');                
                        $("#modal_konten").css('background-color','#ff3333');    
                        $("#text_modal").html("Sorry, You can not choose for the past time");
                        // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Mohon maaf, Anda tidak bisa memilih tanggal yang sudah lewat"); 
                    } else {
                        $('#new-event').modal('show');
                        $('.new-event__title').val('');
                        $('.new-event__start').val(isoDate);
                        $('.new-event__end').val(isoDate);

                    }
                },
                // dayClick: function(date) {                 
                //     alert(date);
                //     var isoDate = moment(date).toISOString();                    
                //     $('#new-event').modal('show');
                //     $('.new-event__title').val('');
                //     $('.new-event__start').val(isoDate);
                //     $('.new-event__end').val(isoDate);
                // },

                viewRender: function (view) {
                    var calendarDate = $('.calendar').fullCalendar('getDate');
                    var calendarMonth = calendarDate.month();

                    //Set data attribute for header. This is used to switch header images using css
                    $('.calendar .fc-toolbar').attr('data-calendar-month', calendarMonth);                

                    //Set title in page header
                    $('.content__title--calendar > h1').html(view.title);
                },

                eventClick: function (event, element) {                    
                    $('#edit-event input[value='+event.className+']').prop('checked', true);
                    $('#edit-event').modal('show');
                    $('.edit-event__id').val(event.id);
                    $('.edit-event__title').val(event.title);
                    $('.edit-event__description').val(event.description);
                }
            });

            $('.fc-toolbar').remove();

            //Add new Event

            function GetEndDate(start_date,duration){ // start_date = "05-25-2017 05:00"        
                var d = fecha.parse(start_date, 'YYYY-MM-DD HH:mm');

                d.setSeconds(d.getSeconds() + duration);
                return fecha.format(d, 'YYYY-MM-DD HH:mm');
            }

            function GetExpiredDate(start_date,minutes){ // start_date = "05-25-2017 05:00"        
                var d = fecha.parse(start_date, 'YYYY-MM-DD HH:mm:ss');

                d.setMinutes(d.getMinutes() - minutes);
                return fecha.format(d, 'YYYY-MM-DD HH:mm:ss');
            }    

            function GetDateHour(start_date,duration){ // start_date = "05-25-2017 05:00"        
                var d = fecha.parse(start_date, 'YYYY-MM-DD HH:mm:ss');

                d.setSeconds(d.getSeconds() + duration);
                return fecha.format(d, 'YYYY-MM-DD HH:mm:ss');
            }       

            $('body').on('click', '.new-event__add', function(){
                
                var date = new Date();
                var waktu_sekarang =  moment(date).format("YYYY-MM-DD HH:mm");     
                var waktu_expired =  moment(date).format("YYYY-MM-DD HH:mm:ss");
                var currentHours = date.getHours();
                var Hours_now = ("0" + currentHours).slice(-2) + ":" + date.getMinutes();
            	var eventDate = $('.new-event__start').val();
                var eventTime = $('.new-event__title').val();
                var eventDurasi = $('#durasi_sesi').val();                
                var tutor_id = "";
                var subject_id = "";
                var status = "0";       
                var desc = "";
                var waktu_sesi = eventDate+" "+eventTime;
                var expiredd = GetExpiredDate(waktu_expired,user_utc);
                var expired = GetDateHour(expiredd,'3600');
                if (eventDate == '' || eventTime == '' || eventDurasi == null) {
                    $("#new-event").modal('hide');
                    $("#modal_alert").modal('show');
                    $("#modal_konten").css('background-color','#ff3333');
                    $("#button_ok").click( function(){
                        $("#new-event").modal('show');
                    });
                    $("#text_modal").html("Please choose the time and duration first.");
                	// notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Mohon untuk memilih durasi terlebih dahulu");
                }
                else if(waktu_sekarang > waktu_sesi){
                    $("#new-event").modal('hide');
                    $("#modal_alert").modal('show');
                    $("#modal_konten").css('background-color','#ff3333');
                    $("#button_ok").click( function(){
                        $("#new-event").modal('show');
                    });
                    $("#text_modal").html("Sorry, You can not create the session on the past time");
                }
                else
                {
	                if (dataSet == "") {
	                	dataSet.push({eventDate,eventTime,eventDurasi,tutor_id,subject_id,desc,status,expired});
		                setSession();
	                }		
	                else
	                {		                    	     
                        var aa = JSON.stringify(dataSet);

		                for (var i = 0; i < dataSet.length; i++) {                            
                            eventDateSet    = dataSet[i]['eventDate'];
                            eventTimeSet    = dataSet[i]['eventTime'];
                            eventDurasiSet  = dataSet[i]['eventDurasi'];
                            
                            datebefore      = eventDateSet+" "+eventTimeSet;
                            datebeforeEnd   = GetEndDate(datebefore,eventDurasiSet);

                            dateChoose      = eventDate+" "+eventTime;
                            dateChooseEnd   = GetEndDate(dateChoose,eventDurasi);

                            if ( ( Date.parse(dateChoose) >= Date.parse(datebeforeEnd) ) || ( Date.parse(dateChooseEnd) <= Date.parse(datebefore) && Date.parse(dateChoose) > time() ) ) {
                                checkRange = 1;
                            }
                            else
                            {
                                checkRange = -1;
                            }                                                        

                            if (checkRange == 1) {
    		                	if (dataSet.length >= countSesi) {
    		                		checkin = -1;
    		                	}
    		                	else
    		                	{
    			                	var datecheck = dataSet[i][0];
    			                	if (datecheck != eventDate) {
    				                	if (dataSet[i] == eventDate+','+eventTime+','+eventDurasi) {			                		
    				                		checkin = 1;
    				                	}
    				                	else
    				                	{	           
    				                		if (checkin == "" || checkin != 1) {     		
    					                		checkin = 0;	
    					                	}
    				                	}
    			                	}
    			                	else
    			                	{		                		
    			                		checkin = 1;
    			                	}
    		                	}
                            }
		                }
                        if(checkRange == -1)
                        {
                            $("#new-event").modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#ff3333');
                            $("#text_modal").html("Sorry, You can not choose at the same time" );
                        }
                        else
                        {
    		                if (checkin == 1) {
    		                	$("#new-event").modal('hide');
                                $("#modal_alert").modal('show');
                                $("#modal_konten").css('background-color','#ff3333');

                                $("#button_ok").click( function(){
                                    // $("#new-event").modal('show');
                                });
                                $("#text_modal").html("Sorry there is a session dated "+eventDate+". please choose another date" );
    		                	// notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Maaf sudah terdapat sesi ditanggal "+eventDate+'. Mohon untuk memilih Tanggal lainnya');
    		                }
    		                else if(checkin == -1 ){
    		                	$("#new-event").modal('hide');
                                $("#modal_konten").css('background-color','#ff3333');
                                $("#modal_alert").modal('show');
                                $("#text_modal").html("Session has reached the limit. The maximum number of Sessions you select is "+countSesi);
    		                	// notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','Sesi sudah mencapai batas. Jumlah Sesi yang anda pilih maksimal '+countSesi);
    		                }
    		                else
    		                {
    		                	dataSet.push({eventDate,eventTime,eventDurasi,tutor_id,subject_id,desc,status,expired});
    		                	setSession();
    		                	$("#durasi_sesi").val(null);
    		                }
                        }
                        // alert("AA");
	                }     
                }                         
                
                function setSession(){
                	var GenRandom =  {
	                    Stored: [],
	                    Job: function(){
	                        var newId = Date.now().toString().substr(6); // or use any method that you want to achieve this string

	                        if( !this.Check(newId) ){
	                            this.Stored.push(newId);
	                            return newId;
	                        }
	                        return this.Job();
	                    },
	                    Check: function(id){
	                        for( var i = 0; i < this.Stored.length; i++ ){
	                            if( this.Stored[i] == id ) return true;
	                        }
	                        return false;
	                    }
	                }; 

            		if (eventTime != '' && eventDate != '' && eventDurasi != '') {
	                    $('.calendar').fullCalendar('renderEvent', {
	                        id: GenRandom.Job(),
	                        title: eventDate+' '+eventTime+' '+eventDurasi,
	                        start: $('.new-event__start').val(),
	                        end:  $('.new-event__end').val(),
	                        allDay: true,
	                        className: $('.event-tag input:checked').val()
	                    }, true);

	                    $('.new-event__form')[0].reset();
	                    $('.new-event__title').closest('.form-group').removeClass('has-danger');
	                    $('#new-event').modal('hide');
	                }
	                else {
	                    $('.new-event__title').closest('.form-group').addClass('has-danger');
	                    $('.new-event__title').focus();
	                }
                }

                console.warn("INI DATA SET : "+JSON.stringify(dataSet));
            });


            //Update/Delete an Event
            $('body').on('click', '[data-calendar]', function(){
                var calendarAction = $(this).data('calendar');
                var currentId = $('.edit-event__id').val();
                var currentTitle = $('.edit-event__title').val();
                var currentDesc = $('.edit-event__description').val();
                var currentClass = $('#edit-event .event-tag input:checked').val();
                var currentEvent = $('.calendar').fullCalendar('clientEvents', currentId);

                //Update
                if(calendarAction === 'update') {
                    if (currentTitle != '') {
                        currentEvent[0].title = currentTitle;
                        currentEvent[0].description = currentDesc;
                        currentEvent[0].className = currentClass;

                        $('.calendar').fullCalendar('updateEvent', currentEvent[0]);
                        $('#edit-event').modal('hide');
                    }
                    else {
                        $('.edit-event__title').closest('.form-group').addClass('has-error');
                        $('.edit-event__title').focus();
                    }
                }

                //Delete
                if(calendarAction === 'delete') {
                    $('#edit-event').modal('hide');
                    setTimeout(function () {
                    	// countSesi = countSesi-1;
                    	// alert(countSesi);
                    	var ad = currentTitle.split(" ",1);                    	
                    	for (var i = 0; i < dataSet.length; i++) {
                    		var a = JSON.stringify(dataSet[i]);
                    		if (dataSet[i]['eventDate'] == ad) {
                    			delete dataSet[i];
                    		}
                    	}
                    	$('.calendar').fullCalendar('removeEvents', currentId);
                    	console.log('INI SCHEDULE SETELAH DITAMBAH = '+dataSet);
                    	location.reload();
                    }, 200);
                }
            });


            //Calendar views switch
            $('body').on('click', '[data-calendar-view]', function(e){
                e.preventDefault();

                $('[data-calendar-view]').removeClass('actions__item--active');
                $(this).addClass('actions__item--active');
                var calendarView = $(this).attr('data-calendar-view');
                $('.calendar').fullCalendar('changeView', calendarView);
            });


            //Calendar Next
            $('body').on('click', '.actions__calender-next', function (e) {
                e.preventDefault();
                $('.calendar').fullCalendar('next');
            });


            //Calendar Prev
            $('body').on('click', '.actions__calender-prev', function (e) {
                e.preventDefault();
                $('.calendar').fullCalendar('prev');
            });
        }

        $('#select_group').select2();
        // $('#durasi_sesi').select2();
        
        function listGroup(txgroup_id = '')
        {
	        $.ajax({
	            url: '<?php echo AIR_API;?>listGroup/access_token/'+access_token,
	            type: 'POST',
	            data: {
	                channel_id: channel_id
	            },
	            success: function(response)
	            {
	                var a = JSON.stringify(response); 
	                if (response['code'] == -400) {
	                    window.location.href='<?php echo base_url();?>logout';
	                }

	                for (var i = 0; i < response.data.length; i++) {
	                    var group_id = response['data'][i]['group_id'];
	                    var group_name = response['data'][i]['group_name'];
	                    if (group_id == txgroup_id) {
	                    	$("#select_group").append("<option selected='true' value='"+group_id+"'>"+group_name+"</option>");	
	                    }
	                    else
	                    {
	                    	$("#select_group").append("<option value='"+group_id+"'>"+group_name+"</option>");
	                    }
	                    
	                }
	                
	            }
	        });
        }

        $(".ubahDataCalendar").click(function(){        	
        	dataSet = [];
        	$("#program_name").removeAttr('disabled','true');
        	$("#sesi").removeAttr('disabled','true');
        	$("#select_group").removeAttr('disabled','true');            
            $("#boxCalendar").css('display','none');
            $("#boxSaveCalendar").css('display','none');
            $("#boxLanjut").css('display','block');
        	calendarBox();
        });
        
        $("#showCalendar").click(function(){
            var program_name    = $("#program_name").val();
            var sesi            = $("#sesi").val();
            countSesi = sesi;            
            var group_id        = $("#select_group").val();
            var a = $('#calendar').fullCalendar('clientEvents');
            var b = JSON.stringify(a);
            if (program_name == "") {
                $("#modal_alert").modal('show');
                $("#modal_konten").css('background-color','#ff3333');
                $("#text_modal").html("Please enter the Program name");
                $("#button_ok").click( function(){
                    // location.reload();
                });
                // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Mohon untuk memasukkan Nama program"); 
            }
            else if(sesi == ""){
                $("#modal_alert").modal('show');
                $("#modal_konten").css('background-color','#ff3333');
                $("#text_modal").html("Please to choose how many sessions you want");
                $("#button_ok").click( function(){
                    // location.reload();
                });
                // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Mohon untuk memilih berapa sesi yang diinginkan!!!");    
            }
            else if(group_id == null){
                $("#modal_alert").modal('show');
                $("#modal_konten").css('background-color','#ff3333');
                $("#text_modal").html("Please to select Group first");
                $("#button_ok").click( function(){
                    // location.reload();
                });
                // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Mohon untuk memilih Group terlebih dahulu!!!"); 
            }
            else if(sesi >= 30)
            {
                $("#modal_alert").modal('show');
                $("#modal_konten").css('background-color','#ff3333');
                $("#text_modal").html("Sessions Must not exceed 30, Please re-select");
                $("#button_ok").click( function(){
                    // location.reload();
                });
                // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Sesi Tidak boleh melebihi 30, Harap pilih kembali");
                $("#sesi").val("");
            }
            else
            {
            	$("#program_name").attr('disabled','true');
            	$("#sesi").attr('disabled','true');
            	$("#select_group").attr('disabled','true');
                $("#boxLanjut").css('display','none');
                $("#boxCalendar").css('display','block');
                $("#boxSaveCalendar").css('display','block');
                calendarBox();
            }
        });         

        $("#saveProgram").click(function(){
            var program_name    = $("#program_name").val();
            var sesi            = $("#sesi").val();
            var group_id        = $("#select_group").val();
            if (program_name == "") {
                $("#modal_alert").modal('show');
                $("#modal_konten").css('background-color','#ff3333');
                $("#text_modal").html("Please enter the Program name");
                // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Mohon untuk memasukkan Nama program"); 
            }
            else if(sesi == ""){
                $("#modal_alert").modal('show');
                $("#modal_konten").css('background-color','#ff3333');
                $("#text_modal").html("Please to choose how many sessions you want");
                // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Mohon untuk memilih berapa sesi yang diinginkan!!!");    
            }
            else if(group_id == null){
                $("#modal_alert").modal('show');
                $("#modal_konten").css('background-color','#ff3333');
                $("#text_modal").html("Please to select Group first");
                // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Mohon untuk memilih Group terlebih dahulu!!!"); 
            }
            else if(dataSet == ""){
                $("#modal_alert").modal('show');
                $("#text_modal").html("Please to select Session Time first");
                // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Mohon untuk memilih Waktu Sesi terlebih dahulu!!!"); 
            }
            else
            {
            	$.ajax({
	                url : '<?php echo AIR_API;?>channel_saveProgram/access_token/'+access_token,
	                type:"post",
	                data: {
	                    program_name: program_name,
	                    sesi: sesi,
	                    group_id: group_id,
	                    schedule: JSON.stringify(dataSet),
	                    channel_id: channel_id
	                },
	                success: function(response){             
	                    var code = response['code'];
	                    if (code == 200) {
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#32c787');
                            $("#text_modal").html("Successfully Adding Program");
                            $("#button_ok").click( function(){
                                window.location.href="<?php echo base_url();?>Channel/listProgram";
                            });
	                    	/*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Berhasil Menambah Program');
	                        setTimeout(function(){                            
	                            window.location.href="<?php echo base_url();?>Channel/listProgram";
	                        },2000);*/
	                    }
                        else if (code == -400) {
                            window.location.href='<?php echo base_url();?>logout';
                        }
	                    else
	                    {
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#ff3333');
                            $("#text_modal").html("There is an error!!!");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
	                    	/*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','There is an error!!!');
	                        setTimeout(function(){
	                            location.reload();
	                        },2000);*/
	                    }
	                }
	            });                
            }
        }); 

        $(document).on("click", "#saveGroup", function(){
            $('#modalAddGroup').modal('hide');
            var group_name          = $("#group_name").val();
            var group_participants  = $("#taggroup").val();
            $.ajax({
                url : '<?php echo AIR_API;?>channel_addStudentToGroup/access_token/'+access_token,
                type:"post",
                data: {
                    group_name: group_name,
                    group_participants: group_participants,
                    channel_id: channel_id
                },
                success: function(response){             
                    var code = response['code'];
                    if (code == 200) {
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully Added Group");
                        $("#modal_konten").css('background-color','#32c787');
                        // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Berhasil Menambah Group');
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>logout';
                    }
                    else
                    {
                        $("#modal_alert").modal('show');
                        $("#text_modal").html("There is an error!!!");
                        $("#modal_konten").css('background-color','#ff3333');
                        // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','There is an error!!!');
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                       /* setTimeout(function(){
                            location.reload();
                        },2000);*/
                    }
                }
            });
        });

        $(document).on("click", "#proses_hapusgroup", function(){   
            $('#modalDeleteGroup').modal('hide');
            var group_id = $(this).attr('rtp');
            $.ajax({
                url :"<?php echo AIR_API;?>deleteGroup/access_token/"+access_token,
                type:"post",
                data: {
                    rtp: group_id,
                    channel_id: channel_id
                },
                success: function(response){   
                    var code = response['code'];
                    if (code == 200) {
                        // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Berhasil menghapus Group");  
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully deleted Group");
                        $("#button_ok").click( function(){
                            location.reload();
                        });                      
                        /*setTimeout(function(){
                            window.location.reload();
                        },2000);*/
                    }   
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>logout';
                    }
                    else{                        
                        ("#modal_alert").modal('show');
                        $("#text_modal").html("There is an error!!!");
                        $("#modal_konten").css('background-color','#ff3333');
                        // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','There is an error!!!');
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                       /* setTimeout(function(){
                            location.reload();
                        },2000);*/
                    }                  
                } 
            });
        });
        
    });
</script> 