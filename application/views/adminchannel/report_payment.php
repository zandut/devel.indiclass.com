<main class="main">

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>Payment Report</h1>
            <small>Details of payment on your channel</small>
        </header>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">
                    <div class="row table-responsive">
                        <div id="tables_reportPayment"></div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>   

<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = [];

    $(document).ready(function() {
        $.ajax({
            url: 'https://rltclass.com.sg/Rest/paypalReport/access_token/'+access_token,
            type: 'POST',
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                
                if (code == 200) {                     
                    for (var i = 0; i < response.data.length; i++) {
                        var payment_id  = response['data'][i]['payment_id'];
                        var user_id = response['data'][i]['user_id'];
                        var product_id  = response['data'][i]['product_id'];
                        var txn_id  = response['data'][i]['txn_id'];
                        var payment_gross   = response['data'][i]['payment_gross'];
                        var currency_code   = response['data'][i]['currency_code'];
                        var payer_email = response['data'][i]['payer_email'];
                        var payment_status  = response['data'][i]['payment_status'];
                        var id_package  = response['data'][i]['id_package'];
                        var name_package    = response['data'][i]['name_package'];
                        var credit_package  = response['data'][i]['credit_package'];
                        var price_package   = response['data'][i]['price_package'];
                        

                        dataSet.push([i+1, txn_id, name_package, credit_package, payment_gross, currency_code, payer_email, payment_status]);
                    }                        

                    $('#tables_reportPayment').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="reportList"></table>' );
             
                    $('#reportList').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Transaction ID"},
                            { "title": "Package Name"},
                            { "title": "Package Credit"},
                            { "title": "Payment Gross"},
                            { "title": "Currency Code"},
                            { "title": "Payer Email"},
                            { "title": "Payment Status"}
                                                        
                        ]
                    });
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>logout';
                }
                else
                {
                    $('#tables_reportPayment').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="reportList"></table>' );
             
                    $('#reportList').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Transaction ID"},
                            { "title": "Package Name"},
                            { "title": "Package Credit"},
                            { "title": "Payment Gross"},
                            { "title": "Currency Code"},
                            { "title": "Payer Email"},
                            { "title": "Payment Status"}
                             
                        ]
                    });
                }
            }
        }); 
        

        $.ajax({
            url: '<?php echo AIR_API;?>pointChannel/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                
                if (code == 200) {
                    var active_point = response['data']['active_point'];
                    $("#chart_totalpoint").text(active_point);
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>logout';
                }
            }
        });

    });
</script>