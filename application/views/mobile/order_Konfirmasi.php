<style type="text/css">
        body{
            background-color: white;
            height: 100%; overflow: hidden;
        }

</style>
<header id="header" class="clearfix" data-current-skin="blue" style="background-color: #008080;">
    <div class="" style="">
        <h2 style="color: white"><a style="color: white" href="<?php echo base_url();?>"><i class="zmdi zmdi-arrow-back"> </i></a> <b style="padding-left: 5%">Confirm Payment</b></h2>
    </div>
</header>

<section id="main" style="padding-top: 20%;">
     <?php
    // if ($_GET['trans'] == 'knf') {                               
        $order_id           = $_GET['order_id'];
        $getjumlah          = $this->db->query("SELECT ti.invoice_id, tor.*, tod.* FROM tbl_order as tor INNER JOIN tbl_order_detail as tod ON tor.order_id=tod.order_id INNER JOIN tbl_invoice as ti ON ti.order_id=tor.order_id WHERE tor.order_id='$order_id'")->row_array();
        if (empty($getjumlah)) {
            $this->session->set_flashdata('mes_alert','info');
            $this->session->set_flashdata('mes_display','block');
            $this->session->set_flashdata('mes_message','Maaf, Order tersebut tidak dapat kami temukan diserver kami.');
            redirect('/');
        }
        else
        {

        if($getjumlah['order_status'] == -3){
            $getunder       = $this->db->query("SELECT underpayment FROM tbl_invoice WHERE invoice_id='$getjumlah[invoice_id]'")->row_array()['underpayment'];
            $hasiljumlah    = number_format($getunder, 0, ".", ".");
        }
        else
        {
            $hasiljumlah    = number_format($getjumlah['total_price'], 0, ".", ".");
        }

        $user_utc           = $this->session->userdata('user_utc');
        $server_utcc        = $this->Rumus->getGMTOffset();
        $intervall          = $user_utc - $server_utcc;
        $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$getjumlah['last_update']);
        $tanggaltransaksi->modify("+".$intervall ." minutes");
        $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');
    ?>
    <div class="card" style="padding: 2%; margin-bottom: 0;">
        <div class="row">
            <div class="col-xs-4">
                <label>No <?php echo $this->lang->line('transaksi'); ?></label><br>    
                <label><?php echo $this->lang->line('tanggal'); ?></label>    
            </div>
            <div class="col-xs-8" align="right">
                <label><?php echo $getjumlah['invoice_id']; ?></label><br>
                <label><?php echo $tanggaltransaksi; ?></label><br>
            </div>
        </div>
        
    </div>

    <section id="">
        <div class="card bgm-white">                    
            <div class="card">
                <div class="card-body " style="background: white;">                
                    <div class="row">                        
                        <form method="post" action="<?php echo base_url('Process/confrim'); ?>" enctype="multipart/form-data">
                            <div class="col-xs-12" style="border: 1px solid gray; border-radius: 5px; padding: 15px;">

                                <input type="text" name="orderid" id="order_id" value="<?php echo $order_id; ?>" hidden>
                                <input type="text" name="type" id="type" required value="<?php echo $getjumlah['type'];?>" hidden>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('tujuantransfer'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="bank" id="bank" class="select2 col-md-6 form-control">
                                            <?php
                                                $allbank = $this->db->query("SELECT * FROM master_bank")->result_array();
                                                $tujuan = $this->lang->line('tujuantransfer');
                                                echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line("pilih").' '. $this->lang->line("tujuantransfer").'</option>';                                                                                                                                               
                                                foreach ($allbank as $row => $v) {
                                                    if ($v['bank_rekening'] != "" && $v['bank_name'] == "BANK MANDIRI") {
                                                        echo '<option value="'.$v['bank_id'].'">'.$v['bank_name'].' - '.$v['bank_rekening'].'</option>';
                                                    }
                                                }   
                                            ?>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('jumlahtransfer'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="nominaltransaksi" id="nominal" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('inputjumlah'); ?>" minlength="9" step="500" maxlength="14" min="10000" max="500000"/>
                                            <input type="text" name="nominalsave" id="nominalsave" hidden />
                                        </div>
                                        <label class="c-gray m-t-5"><ins><small>( <?php echo $this->lang->line("jumlahharus");?> Rp. <?php echo $hasiljumlah; ?> )</small></ins></label>
                                    </div>
                                </div>                                  
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('fromrekening'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="frombank" id="frombank" placeholder="<?php echo $this->lang->line('masukanrekening'); ?>" required class="input-sm form-control fg-input" onkeyup="validAngka(this)" onkeypress="return hanyaAngka(event)">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Atas nama pemilik</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="namapemilikbank" id="namapemilikbank" placeholder="<?php echo $this->lang->line('namadirekening'); ?>" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('denganmetode'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="metodepembayaran" id="metodepembayaran" class="select2 col-md-6 form-control">                                      
                                                <option disabled selected><?php echo $this->lang->line('pilihmetode'); ?></option>
                                                <option value="atm">ATM Transfer</option>                                                  
                                                <option value="internetbanking">Internet Banking</option>
                                                <option value="mobilebanking">Mobile Banking</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-5"><?php echo $this->lang->line('buktipembayaran'); ?></label>
                                    <div class="col-sm-9 m-t-5">
                                        <div class="fg-line">
                                            <input type="file" required name="buktibayar" id="buktibayar" accept="image/*">
                                        </div>
                                    </div>
                                </div>                                                                                            
                                <br><br><br>
                                <div class="col-xs-12">
                                    <button class="btn btn-md c-white btn-block m-t-10 bgm-blue" id="kotak" style="border: 1px solid gray;"><?php echo $this->lang->line('konfirmasi'); ?></button>    
                                </div>
                                
                            </div>
                            <!-- <div class="col-md-8">
                            	sdfasdf
                            </div> -->
                            <div class="col-md-2 m-t-25"></div>
                                                    
                        </form>

                    </div>
                </div>
                
                <!-- <footer class="p-20">
                    <a href="<?php echo base_url();?>first/topup"><button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;">Konfirmasi</button></a>
                </footer> -->
            </div>
            <?php
                }
            ?>
        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function(){
        $('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $("#nominal").keyup(function() {
          var clone = $(this).val();
          var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
          $("#nominalsave").val(cloned);
        });

        // $(document).on("click", "#kotak", function () {              	
        //     var order_id         = $("#order_id").val();
        //     var type             = $("#type").val();
        //     var bank             = $("#bank").val();
        //     var nominalsave      = $("#nominalsave").val();
        //     var frombank         = $("#frombank").val();
        //     var namapemilikbank  = $("#namapemilikbank").val();
        //     var metodepembayaran = $("#metodepembayaran").val();
        //     var buktibayar       = $("#buktibayar").val();            
        //     $.ajax({
        //         url: '<?php echo base_url(); ?>process/submitMethodPayment',
        //         type: 'POST',
        //         data: {
        //             orderid : order_id,
        //             type: type,
        //             bank : bank,
        //             nominalsave : nominalsave,
        //             frombank : frombank,
        //             namapemilikbank: namapemilikbank,
        //             metodepembayaran: metodepembayaran,
        //             buktibayar: buktibayar                    
        //         },               
        //         success: function(data)
        //         { 
        //             var sdata = JSON.parse(data);
        //             if (sdata['status'] == 1) {
        //             }
        //             else if (sdata['status'] == 0) {
        //             }
        //             else if (sdata['status'] == -1) {
        //             }
        //             else if (sdata['status'] == -2) {
        //             }
        //         }
        //     });
        // });
    });

</script>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datetimepicker({
            locale: 'id'
        });
    });
    $('#frombank').keyup(function() {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });
    function validAngka(a)
    {
        if(!/^[0-9.]+$/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
</script>
