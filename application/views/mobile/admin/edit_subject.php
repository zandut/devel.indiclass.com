<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container invoice">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Edit Subject</h2>
            </div>
                    
            <?php
                $subjectid = $_GET['id'];
                $getsubject = $this->db->query("SELECT * FROM master_subject WHERE subject_id='$subjectid'")->row_array();
            ?>
            <div class="card">
                <div class="card-header ch-alt">
                    <div class="pull-right f-14">Subject ID : <?php echo $_GET['id']; ?></div>
                    <div class="pull-left f-14"><a href="<?php echo base_url();?>Admin/Subjectadd"><button class="btn btn-danger"><i class="zmdi zmdi-arrow-left"></i> Back</button></a></div><br>
                </div>
                
                <div class="card-body card-padding" style="background: white;">                
                    <div class="row">                        
                        <form method="post" action="<?php echo base_url('Admin/edtsubject'); ?>" enctype="multipart/form-data">
                            <div class="col-md-3 m-t-25"></div>
                            <div class="col-md-6 m-t-25 m-b-25" style="border: 1px solid gray; border-radius: 5px; padding: 15px;">
                                
                                <input type="text" name="subject_id" id="order_id" value="<?php echo $subjectid; ?>" hidden/>                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Subject Name</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="subject_name" placeholder="Isi Subject" value="<?php echo $getsubject['subject_name'] ?>" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Jenjang</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="jenjang_id" class="select2 col-md-6 form-control">
                                            <?php

                                                $allsub = $this->db->query("SELECT * FROM master_jenjang ORDER BY jenjang_id")->result_array();
                                                foreach ($allsub as $row => $d) {
                                                    if($d['jenjang_id'] == $getsubject['jenjang_id']){
                                                        echo "<option value='".$d['jenjang_id']."' selected='true'>".$d['jenjang_level']." ".$d['jenjang_name']."</option>";
                                                    }
                                                    if ($d['jenjang_id'] != $getsubject['jenjang_id']) {
                                                        echo "<option value='".$d['jenjang_id']."'>".$d['jenjang_level']." ".$d['jenjang_name']."</option>";
                                                    }                                                            
                                                }
                                            ?> 
                                            </select>
                                        </div>
                                    </div>
                                </div>                                
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Indonesia</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="subject_name_indonesia" placeholder="Isi Subject Indonesia" value="<?php echo $getsubject['indonesia'] ?>" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">English</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="subject_name_inggris" placeholder="Isi Subject English" value="<?php echo $getsubject['english'] ?>" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-5">Foto Icon</label>
                                    <div class="col-sm-9 m-t-5">
                                        <div class="fg-line">       
                                            <input type="file" name="fotoiconbaru" accept="image/*">
                                            <input type="text" name="fotoiconlama" value="<?php echo $getsubject['icon']; ?>" hidden/>
                                        </div>
                                        <small class="c-red"><b><?php echo $this->lang->line('peringatan'); ?></b></small>
                                    </div>
                                </div>                                

                                <br><br><br>
                                <button class="btn btn-md c-white btn-block m-t-10 bgm-blue" style="border: 1px solid gray;">Ubah Subject</button>
                            </div>
                            <div class="col-md-3 m-t-25"></div>
                                                    
                        </form>

                    </div>
                </div>
                
            </div>
            
        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
