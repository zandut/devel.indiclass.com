<div class="profile-menu">
    <a href="">
        <div class="profile-pic">
            <img src="<?php echo base_url(); ?>aset/img/profile-pics/1.jpg" alt="">
        </div>

        <div class="profile-info">
            <?php echo $this->session->userdata('nama_lengkap'); ?>
        
            <i class="zmdi zmdi-caret-down"></i>
        </div>
    </a>

    <ul class="main-menu">
        <li>
        <a href="profile-about.php"><i class="zmdi zmdi-account"></i> <?php echo $this->lang->line('viewprofile'); ?></a>
        </li>
        <li>
            <a href=""><i class="zmdi zmdi-time-restore"></i> <?php echo $this->lang->line('logout'); ?></a>
        </li>
    </ul>
</div>
<ul class="main-menu">

    <li class="<?php if($sideactive=="home"){echo "active";}else{

    } ?>"><a href="<?php echo base_url(); ?>"><i class="zmdi zmdi-home"></i>Dashboard</a></li>     

</ul>