<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Classmiles</title>

    <!-- Vendor CSS -->
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/farbtastic/farbtastic.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/chosen/chosen.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/summernote/dist/summernote.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>aset/css/app.min.1.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/css/app.min.2.css" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url(); ?>aset/img/blur/icon.ico">

    <link href="<?php echo base_url(); ?>aset/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

</head>

<body <?php if(isset($sat)){ echo $sat; } ?>>
    <!-- Javascript Libraries -->
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery/dist/jquery.min.js"></script>

    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/sparklines/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js "></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/Waves/dist/waves.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/datatables.js"></script>

    <script src="<?php echo base_url(); ?>aset/vendors/bootgrid/jquery.bootgrid.updated.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>


    <script src="<?php echo base_url(); ?>aset/js/flot-charts/curved-line-chart.js"></script>
    <script src="<?php echo base_url(); ?>aset/js/flot-charts/line-chart.js"></script>
    <script src="<?php echo base_url(); ?>aset/js/charts.js"></script>

    <script src="<?php echo base_url(); ?>aset/js/charts.js"></script>
    <script src="<?php echo base_url(); ?>aset/js/functions.js"></script>

    <script src="<?php echo base_url(); ?>aset/vendors/bootgrid/jquery.bootgrid.updated.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/fileinput/fileinput.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/input-mask/input-mask.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/farbtastic/farbtastic.min.js"></script>

    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();


            $.ajaxSetup({
                type:"POST",
                url: "<?php echo base_url()?>First/ambil_data",
                cache: false,
            });

            $("#provinsi").change(function(){
                var value=$(this).val();                
                if(value>0){
                    $.ajax({                    
                        data:{modul:'kabupaten',id:value},
                        success: function(respond){
                            $("#kabupaten").html(respond);
                        }
                    })
                }

            });


            $("#kabupaten").change(function(){
                var value=$(this).val();                
                if(value>0){
                    $.ajax({
                        data:{modul:'kecamatan',id:value},
                        success: function(respond){
                            $("#kecamatan").html(respond);
                        }
                    })
                }
            });

            $("#kecamatan").change(function(){
                var value=$(this).val();
                if (value>0) {
                    $.ajax({
                        data:{modul:'kelurahan',id:value},
                        success: function(respond){
                            $("#kelurahan").html(respond);
                        }
                    })
                }
            });

            $("#jenjangname").change(function(){
                // var value=$(this).val();  
                var name = $("#jenjangname").val();              
                // alert(id);
                // if (value>0) {
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>First/getlevels",
                        cache: false,
                        data:{"name":name},
                        success: function(respond){
                            $("#jenjanglevel").html(respond);
                        }
                    })
                // }
            });

            $("#jenjang_name").change(function(){
                // var value=$(this).val();  
                var name = $("#jenjang_name").val();              
                // alert(id);
                // if (value>0) {
                    $.ajax({
                        type:"POST",
                        url: "<?php echo base_url()?>First/getlevelsregister",
                        cache: false,
                        data:{"name":name},
                        success: function(respond){
                            $("#jenjang_level").html(respond);
                        }
                    })
                // }
            });

                var cId = $('#calendar'); //Change the name if you want. I'm also using thsi add button for more actions

                $('#buttonSession').click(function(){
                    event.preventDefault();
                    var url = $(this).attr('href');
                    location.href=url;
                    var session = '';

                    // $.ajax({
                    //     url: '<?php //echo base_url(); ?>/first/createSession',
                    //     type: 'POST',
                    //     success: function(data){
                    //         session = data;
                    //         var win = 
                    //         win.focus();
                    //         // $(this).trigger('click');
                    //     }
                    // });
                });

                // $('#buttonfollow').click(function(){
                //     // event.preventDefault();
                //     var url = $(this).attr('href');
                //     location.href=url;
                    

                    // $.ajax({
                    //     url: '/first/createSession',
                    //     type: 'POST',
                    //     success: function(data){
                    //         session = data;
                    //         var win = 
                    //         win.focus();
                    //         // $(this).trigger('click');
                    //     }
                    // });
                // });



                $('#tanggal_lahir').on('keypress',function(e){

                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                });
                $('#tahun_lahir').on('keypress',function(e){

                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                });
                $('#no_hape').on('keypress',function(e){

                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                });

                $("#data-table-selection").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    },
                    selection: true,
                    multiSelect: true,
                    rowSelect: true,
                    keepSelection: true
                });
                
                //Generate the Calendar
                cId.fullCalendar({
                    header: {
                        right: '',
                        center: 'prev, title, next',
                        left: ''
                    },

                    theme: true, //Do not remove this as it ruin the design
                    selectable: true,
                    selectHelper: true,
                    editable: true,

                    //Add Events
                    events: [
                    {
                        title: 'Matematika',
                        start: new Date(y, m, 1),
                        allDay: true,
                        className: 'bgm-cyan'
                    },
                    {
                        title: 'Meeting with client',
                        start: new Date(y, m, 10),
                        allDay: true,
                        className: 'bgm-orange'
                    },
                    {
                        title: 'Repeat Event',
                        start: new Date(y, m, 18),
                        allDay: true,
                        className: 'bgm-amber'
                    },
                    {
                        title: 'Semester Exam',
                        start: new Date(y, m, 20),
                        allDay: true,
                        className: 'bgm-green'
                    },
                    {
                        title: 'Soccor match',
                        start: new Date(y, m, 5),
                        allDay: true,
                        className: 'bgm-lightblue'
                    },
                    {
                        title: 'Coffee time',
                        start: new Date(y, m, 21),
                        allDay: true,
                        className: 'bgm-orange'
                    },
                    {
                        title: 'Job Interview',
                        start: new Date(y, m, 5),
                        allDay: true,
                        className: 'bgm-amber'
                    },
                    {
                        title: 'IT Meeting',
                        start: new Date(y, m, 5),
                        allDay: true,
                        className: 'bgm-green'
                    },
                    {
                        title: 'Brunch at Beach',
                        start: new Date(y, m, 1),
                        allDay: true,
                        className: 'bgm-lightblue'
                    },
                    {
                        title: 'Live TV Show',
                        start: new Date(y, m, 15),
                        allDay: true,
                        className: 'bgm-cyan'
                    },
                    {
                        title: 'Software Conference',
                        start: new Date(y, m, 25),
                        allDay: true,
                        className: 'bgm-lightblue'
                    },
                    {
                        title: 'Coffee time',
                        start: new Date(y, m, 30),
                        allDay: true,
                        className: 'bgm-orange'
                    },
                    {
                        title: 'Job Interview',
                        start: new Date(y, m, 30),
                        allDay: true,
                        className: 'bgm-green'
                    },
                    ],

                    //On Day Select
                    select: function(start, end, allDay) {
                        $('#addNew-event').modal('show');   
                        $('#addNew-event input:text').val('');
                        $('#getStart').val(start);
                        $('#getEnd').val(end);
                    }
                });

                //Create and ddd Action button with dropdown in Calendar header. 
                var actionMenu = '<ul class="actions actions-alt" id="fc-actions">' +
                '<li class="dropdown">' +
                '<a href="" data-toggle="dropdown"><i class="zmdi zmdi-more-vert"></i></a>' +
                '<ul class="dropdown-menu dropdown-menu-right">' +
                '<li class="active">' +
                '<a data-view="month" href="">Month View</a>' +
                '</li>' +                                            
                '<li>' +
                '<a data-view="basicWeek" href="">Week View</a>' +
                '</li>' +
                '<li>' +                                            
                '<a data-view="agendaWeek" href="">Agenda Week View</a>' +
                '</li>' +                                            
                '<li>' +
                '<a data-view="basicDay" href="">Day View</a>' +
                '</li>' +
                '<li>' +
                '<a data-view="agendaDay" href="">Agenda Day View</a>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</li>';


                cId.find('.fc-toolbar').append(actionMenu);
                
                //Event Tag Selector
                (function(){
                    $('body').on('click', '.event-tag > span', function(){
                        $('.event-tag > span').removeClass('selected');
                        $(this).addClass('selected');
                    });
                })();

                //Add new Event
                $('body').on('click', '#addEvent', function(){
                    var eventName = $('#eventName').val();
                    var tagColor = $('.event-tag > span.selected').attr('data-tag');

                    if (eventName != '') {
                        //Render Event
                        $('#calendar').fullCalendar('renderEvent',{
                            title: eventName,
                            start: $('#getStart').val(),
                            end:  $('#getEnd').val(),
                            // allDay: true,
                            className: tagColor
                            
                        },true ); //Stick the event
                        
                        $('#addNew-event form')[0].reset()
                        $('#addNew-event').modal('hide');
                    }
                    
                    else {
                        $('#eventName').closest('.form-group').addClass('has-error');
                    }
                });   

                //Calendar views
                $('body').on('click', '#fc-actions [data-view]', function(e){
                    e.preventDefault();
                    var dataView = $(this).attr('data-view');
                    
                    $('#fc-actions li').removeClass('active');
                    $(this).parent().addClass('active');
                    cId.fullCalendar('changeView', dataView);  
                });
                //Basic Example
                $("#data-table-basic").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    },
                });
                
                //Selection
                $("#data-table-selection").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    },
                    selection: true,
                    multiSelect: true,
                    rowSelect: true,
                    keepSelection: true
                });
                
                //Command Buttons
                $("#data-table-command").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    },
                    formatters: {
                        "commands": function(column, row) {
                            return "<button type=\"button\" class=\"btn btn-icon command-edit waves-effect waves-circle\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button> " + 
                            "<button type=\"button\" class=\"btn btn-icon command-delete waves-effect waves-circle\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";
                        }
                    }
                });
            });                                                

        </script>
        