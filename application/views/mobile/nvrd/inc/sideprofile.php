<div class="pm-overview c-overflow">

                        <div class="pmo-pic">
                            <div class="p-relative">
                                <a href="">
                                    <img class="img-responsive" src="<?php echo base_url('aset/img/user/'.$this->session->userdata('user_image')); ?>" alt=""> 
                                </a>

                                <div class="dropdown pmop-message">
                                    <a data-toggle="dropdown" href="" class="btn bgm-white btn-float z-depth-1">
                                        <i class="zmdi zmdi-comment-text-alt"></i>
                                    </a>

                                    <div class="dropdown-menu">
                                        <textarea placeholder="Write something..."></textarea>

                                        <button class="btn bgm-green btn-float"><i class="zmdi zmdi-mail-send"></i></button>
                                    </div>
                                </div>

                                <a href="" class="pmop-edit">
                                    <i class="zmdi zmdi-camera"></i> <span class="hidden-xs"><?php echo $this->lang->line('upd_profpic'); ?></span>
                                </a>
                            </div>


                            <div class="pmo-stat">
                                <h2 class="m-0 c-white">1562</h2>
                                <?php echo $this->lang->line('totcon'); ?>
                            </div>
                        </div>

                        <div class="pmo-block pmo-contact hidden-xs">
                            <h2><?php echo $this->lang->line('contact'); ?></h2>

                            <ul>
                                <li><i class="zmdi zmdi-phone"></i> <?php echo $this->session->userdata('no_hp'); ?></li>
                                <li><i class="zmdi zmdi-email"></i> <?php echo $this->session->userdata('email'); ?></li>                                    
                                <li>
                                    <i class="zmdi zmdi-pin"></i>
                                    <address class="m-b-0 ng-binding">
                                        44-46 Morningside Road,<br>
                                        Edinburgh,<br>
                                        Scotland
                                    </address>
                                </li>
                            </ul>
                        </div>

                    </div>