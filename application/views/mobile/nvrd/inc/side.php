<div class="profile-menu">
    <a href="">
        <div class="profile-pic">
            <img src="<?php echo base_url('aset/img/user/'.$this->session->userdata('user_image')); ?>" alt="">
        </div>

        <div class="profile-info">
            <?php echo $this->session->userdata('nama_lengkap'); 
            ?>        
        
            <i class="zmdi zmdi-caret-down"></i>
        </div>
    </a>

    <ul class="main-menu">
        <li>
        <a href="profile-about.php"><i class="zmdi zmdi-account"></i> <?php echo $this->lang->line('viewprofile'); ?></a>
        </li>
        <li>
            <a href=""><i class="zmdi zmdi-settings"></i> <?php echo $this->lang->line('settings'); ?></a>
        </li>
        <li>
            <a href=""><i class="zmdi zmdi-time-restore"></i> <?php echo $this->lang->line('logout'); ?></a>
        </li>
    </ul>
</div>
<ul class="main-menu">
    <li class="    
        <?php if($sideactive=="home"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>"><i class="zmdi zmdi-home"></i>  <?php echo $this->lang->line('home'); ?></a></li>                   
    <li class="<?php if($sideactive=="subject"){echo "active";}else{

    } ?>"><a href="<?php echo base_url(); ?>first/subjects"><i class="zmdi zmdi-desktop-windows"></i>  <?php echo $this->lang->line('subject'); ?></a></li>
                                        
    <li class="<?php if($sideactive=="history"){echo "active";}else{

    } ?>"><a href="<?php echo base_url(); ?>first/history"><i class="zmdi zmdi-assignment"></i> <?php echo $this->lang->line('history'); ?></a></li>                
    <li class="<?php if($sideactive=="profile"){echo "active";}else{

    } ?>"><a href="<?php echo base_url(); ?>first/about"><i class="zmdi zmdi-account-circle"></i> <?php echo $this->lang->line('profil'); ?></a></li>
</ul>