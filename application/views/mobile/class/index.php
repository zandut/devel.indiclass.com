<div id="loader">
    <div class="page-loader" style="background-color:#1274b3;">
        <div class="preloader pl-xl pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Class Miles . . .</p>
        </div>
    </div>
</div>

<header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #2196f3;">
    
    <ul class="header-inner container">
        <li>
            <a href="#" class="m-l-10"><img src="https://cdn.classmiles.com/sccontent/logo/logoclass6.png" alt=""></a>
        </li> 
        <li class="pull-right">
            <ul class="top-menu">
                <li id="top-search">
                    <a href=""><i class="tm-icon zmdi zmdi-search"></i></a>
                </li>
            </ul>
        </li> 
    </ul>
    
    <!-- Top Search Content -->
    <div id="top-search-wrap" style="background-color: #2196f3; color: white">
        <div class="tsw-inner">
            <i id="top-search-close" class="zmdi zmdi-arrow-left"></i>
            <input type="text"  placeholder="Search Class in here">            
        </div>
    </div>

    <div class="container">     
        <nav class="ha-menu" style="background-color: rgba(33, 150, 243, 0.6); color: gray; width: 100%; padding: 0; margin: 0;">
        </nav>        
    </div>

    <div>        

    <script type="text/javascript">
        $(document).ready(function(){
            $('#btn_setindonesia').click(function(e){
                e.preventDefault();
                $.get('<?php echo base_url('first/set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
            });
            $('#btn_setenglish').click(function(e){
                e.preventDefault();
                $.get('<?php echo base_url('first/set_lang/english'); ?>',function(){ location.reload(); });
            });
        });
    </script>

</header>

<style type="text/css">
    body{
        /*background-color: white;*/
    }
    html{
        overflow-x: hidden;

    }
    #widgetright{
        /*margin-top: -26%;*/
    }
    #titlesubject{
        /*margin-top: -50%;*/
        /*margin-bottom: 20%;*/
        background-color: rgba(238, 238, 238, 0);
        border: none;
    }
    #boxblue{
        height: 370px; /*background-color: rgba(33, 150, 243, 0.77);*/ padding: 0; margin-bottom: 1.5%; width: 100%; display: block;
        background-image: url('https://cdn.classmiles.com/sccontent/headers/1.png'); background-repeat: no-repeat; background-size: 100%;
    }
    /*#titleone{
        color: #ffffff;
    }
    #titletwo{
        color: #EEEEEE;
    }
    #titlethree{
        color: #EEEEEE;
    }
    #titlefour{
        color: #EEEEEE;
    }
    #desc_date{
        color: #ffffff;
    }
    #desc_starttime{
        color: #ffffff;
    }
    #desc_finishtime{
        color: #ffffff;
    }
    #desc_classtype{
        color: #ffffff;
    }
    #desc_templatetype{
        color: #ffffff;
    }*/ 
    @media screen and (max-width: 1001px)
    {
        #widgetright{
            margin-top: 20%;
        }
        #boxblue{
            display: none;
        }
        #titlesubject{
            padding: 3%;
            margin-top: 0%;
            margin-bottom: 3%;
            background-color: #ffffff;
        }
        #titleone{
            color: #000000;
        }
        #titletwo{
            color: gray;
        }
        #titlethree{
            color: gray;
        }
        #titlefour{
            color: gray;
        }
        #desc_date{
            color: gray;
        }
        #desc_starttime{
            color: gray;
        }
        #desc_finishtime{
            color: gray;
        }
        #desc_classtype{
            color: gray;
        }
        #desc_templatetype{
            color: gray;
        }
    }
</style>

<section id="content" style="margin-top: 100px; overflow-x: hidden;">
    <div class="container c-alt"> 
        <div class="row" style="">
            <div class="col-md-12 col-sm-12 col-xs-12" id="boxdescchannel">
                <div class="card wall-posting p-15">
                    <div class="row" style="background-color: white;">
                        <div class="col-md-12" style="padding-top: 0;" align="center">
                            <h4 class="m-b-20 m-l-10 m-r-10 text-uppercase" style="margin-top: 0;" id="descc_channelname"></h4>                            
                        </div>
                        <div class="col-md-8">
                            <div class="banner_channel" style="margin-bottom: 2%">
                                <img id="banner_channel" src="" style="width: 100%; height: auto;">
                            </div>
                            <p class="m-l-10 m-r-10" id="program_desc"></p>
                        </div>
                        <div class="col-md-4 hidden-sm" id="">
                            <button id="btn_extraclass" class="buttonjoin btn btn-lg bgm-green" style=" margin-bottom: 5%; width: 100%; border-radius: 15px; cursor: pointer;"></i>  Gabung ke Program</button>
                            <h4 align="center" id="program_name"></h4>
                            <h4 class="c-gray">Subjects:</h4>
                            <ul id="list_bidang">
                            </ul>
                            <h4 class="c-gray">Features:</h4>
                            <ul id="list_fasilitas">
                            </ul>
                            <h4 class="c-gray">Contact Information:</h4>
                            <ul class="list-inline list-unstyled" id="data_channel" style="">
                                <button title="Email" class="btn btn-danger m-r-10"><i class="zmdi zmdi-email"></i></button>
                                <label id="descc_channelemail" class=" f-14"></label>
                            </ul>
                            <ul class="list-inline list-unstyled" id="">
                                <button title="Phone Number" class=" btn btn-success m-r-10"><i class="zmdi zmdi-phone"></i></button>
                                <label id="descc_channelphone" class=" f-14"></label>
                            </ul>
                        </div>         
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="boxdescbiasa">   
            <div class="col-sm-8" id="">
                <div class="card">
                    <div class="card-body card-padding">
                        <h4 class="c-gray">Class Information:</h4>
                        <h3><label id="titleone"></label><label id="titlefour" style="font-size: 12px;" class="pull-right"></label></h3>
                            <h5 id="titlethree"></h5>
                            <ul class="list-inline list-unstyled">
                                <!-- <li class="m-l-5 m-r-5 m-t-15">
                                    <button title="Date" class="btn btn-warning btn-icon waves-effect waves-circle waves-float m-r-10"><i class="zmdi zmdi-calendar"></i></button>
                                    <small id="titlethree" class="f-14"></small>                         
                                </li> -->
                                <li class="m-l-5 m-r-5 m-t-15">
                                    <button title="Date" class="btn btn-warning m-r-10"><i class="zmdi zmdi-calendar"></i></button>
                                    <small id="desc_date" class="f-14"></small>                         
                                </li>
                                <li class="m-l-5 m-r-5 m-t-15">
                                    <button title="Time" class="btn btn-info m-r-10"><i class="zmdi zmdi-time"></i></button>
                                    <small id="desc_starttime" class="f-14"></small>  
                                    <small  id="desc_finishtime" class="f-14"></small>
                                </li>
                                <li class="m-l-5 m-r-5 m-t-15">
                                    <button title="Type Class" class="btn btn-danger m-r-10"><i class="zmdi zmdi-view-web"></i></button>
                                    <small  id="desc_classtype" class="f-14"></small>
                                </li>
                            </ul>
                            <div id="aksi_biasa">
                                <a id="sudahlogin" href="#" class="bp-title m-t-20" style="display: none;">
                                <button class="buttonjoin btn btn-success btn-block" style="height: 50px; display: none;">Join Now</button>
                                <button class="buttonmasuk btn btn-success btn-block" style="height: 50px; display: none;">Enter Classroom</button>
                                <button class="buttontunggu btn btn-warning btn-block" style="height: 50px; display: none;">Waiting for Classroom</button>
                                <button class="buttonalert btn btn-warning btn-block" style="height: 50px; display: none;">This class for student channel only</button>
                                <div class="buttonalertinvite alert alert-warning" role="alert" style="display: none;">This class is for registered channel's participant only. Please contact channel administrator if you want join it.</div>
                            </a>
                            <a id="belumlogin" href="#" class="bp-title m-t-20 m-b-20" style="display: none;">                         
                                <!-- <label class="wp-text c-white" style="font-size: 24px;">Rp. 1.500.000</label> -->
                                <button class="btn btn-success btnnotjoin btn-block" style="">Join Now</button>                        
                                <!-- <button id="kelogin" class="btn bgm-blue" style="height: 50px;">Login</button>
                                <button id="keregister" class="btn btn-danger" style="height: 50px;">Register</button> -->
                            </a>
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 hidden-sm" id="widgetright">
                <div class="card">
                    <div class="card-body card-padding">
                        
                        <div class="" style="" align="center">
                            <img id="desc_tutorimage" alt="" style="padding: 2%; height: 250px; width: auto;">
                        </div>
                        <a hidden href="intent:#Intent;action=com.classmiles.student;category=android.intent.category.DEFAULT;category=android.intent.category.BROWSABLE;S.msg_from_browser=Launched%20from%20Browser;end"></a>
                        <!-- <small class="f-15">Tutor Description</small> -->
                        <div class="pmo-contact" >                              
                            <h4 class="m-l-10 c-gray">About Tutor :</h4>
                            <ul>
                                <li class="ng-binding" id="desc_tutorname"></li>
                                <li class="ng-binding" id="desc_tutorgender"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-12 col-sm-12 col-xs-12" id="other_class">
                
                <div class="card wall-posting p-15">
                    <div class="row" style="background-color: #f9f9f9;">
                        <div class="card-header">
                            <h2>Other class from <label id="nametutor"></label></h2>
                            <hr>
                        </div>
                        <div class="card-body card-padding" id="boxclass_more">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" style="margin-top: 13%; " id="alertnotlogin" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title c-black">Pemberitahuan</h4>
                        <hr>
                        <p><label class="c-gray f-15">Anda harus masuk terlebih dahulu dan menjadi murid di Classmiles</label></p>
                        <div class="col-xs-6">
                        <button id="kelogin" class="btn bgm-blue btn-block" style="height: 50px;">Login</button>
                        </div>
                        <div class="col-xs-6">
                        <button id="keregister" class="btn btn-danger btn-block" style="height: 50px;">Register</button>
                        </div>
                    </div>                                                
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                        <a id="proccessjoin" href=""><button type="button" class="btn bgm-green" id="confrimjoin">Ya</button></a>
                    </div>  -->                       
                </div>
            </div>
        </div>

    </div>



</section>

<script type="text/javascript">
    $(document).ready(function(){

        var tgl = new Date();
        var c = "<?php echo $_GET['c'];?>";
        var datac = null;
        var subject_id = null;
        var subject_name = null;
        var subject_description = null;
        var tutor_id = null;
        var start_time = null;
        var finish_time = null;
        var class_type = null;
        var template_type = null;
        var user_name = null;
        var email = null;
        var user_image = null;  
        var template = null;
        var user_gender = null;
        var user_age = null;    
        var dateclass = null;
        var statusall = null;
        var timeshow = null;
        var endtimeshow = null;
        var timeclass = null;
        var endtimeclass = null;
        var channelstatus = null;
        var typeclass = null;
        var participant_count = null;
        var cekiduser = "<?php echo $this->session->userdata('id_user');?>";
        var cekusertype_id = "<?php echo $this->session->userdata('usertype_id');?>";
        var formattedDate = moment(tgl).format('YYYY-MM-DD')        
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";

        var d = new Date();
        var time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        console.log(formattedDate);
        $("#kelogin").click(function(){            
            var classid = $(this).attr('class_id');    
            // alert(classid);
            $.ajax({
                url: '<?php echo base_url(); ?>Ajaxer/set_code/',
                type: 'POST',
                data: {                
                    class_id : classid
                }, 
                success: function(data)
                {              
                    window.location.href = '<?php echo base_url();?>';
                }
            });
        });

        $("#keregister").click(function(){                              
            window.location.href = '<?php echo base_url();?>Register';            
        });

        $(".btnnotjoin").click(function(){
            var ccode = "<?php echo $_GET['c'];?>";
            $.ajax({
                url: '<?php echo base_url(); ?>Tools/get_dc/'+ccode,
                type: 'GET',
                success: function(data)
                {              
                    
                    data = data.trim();                    
                    $("#kelogin").attr('class_id',data);
                    $("#alertnotlogin").modal("show");                    
                }
            });  
            
        });

        if (c!="") 
        {                    
            if (cekiduser!="") {
                if (cekusertype_id == "student") 
                {
                    $("#belumlogin").css('display','none');
                    $("#sudahlogin").css('display','block');
                }
            }    
            else
            {
                $("#sudahlogin").css('display','none');
                $("#belumlogin").css('display','block');
            }
            
            $.ajax({
                url: '<?php echo base_url(); ?>Tools/get_dc/'+c,
                type: 'GET',
                success: function(data)
                {              
                    data = data.trim();                    
                    Showdata(data);                    
                }
            });                        
        }
        else
        {
            window.location.href = '<?php echo base_url();?>';
        }
        
        function Showdata(classid)
        {           
            var id_user = "<?php echo $this->session->userdata('id_user');?>";
            var user_utc = new Date().getTimezoneOffset();
                    
            user_utc = -1 * user_utc;
            $.ajax({
                url: '<?php echo BASE_URL();?>Rest/get_classdesc/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    user_utc: user_utc,
                    user_device: 'web',
                    date: formattedDate,
                    class_id: classid
                },
                success: function(data)
                {                   
                    subject_id      = data['data'][0]['subject_id'];
                    subject_name    = data['data'][0]['name'];
                    statusall       = data['data'][0]['status_all'];
                    subject_description = data['data'][0]['description'];
                    tutor_id        = data['data'][0]['tutor_id'];
                    start_time      = data['data'][0]['start_time'];
                    finish_time     = data['data'][0]['finish_time'];
                    class_type      = data['data'][0]['class_type'];
                    template_type   = data['data'][0]['template_type'];
                    template        = data['data'][0]['template_type'];
                    user_name       = data['data'][0]['user_name'];
                    email           = data['data'][0]['email'];
                    user_gender     = data['data'][0]['user_gender'];
                    user_age        = data['data'][0]['user_age'];
                    user_image      = data['data'][0]['user_image'];
                    dateclass       = data['data'][0]['date'];
                    timeshow        = data['data'][0]['time'];
                    endtimeshow     = data['data'][0]['endtime'];
                    jenjangnamelevel= data['data'][0]['jenjangnamelevel'];
                    timeclass       = data['data'][0]['date']+" "+data['data'][0]['time'];
                    endtimeclass    = data['data'][0]['enddate']+" "+data['data'][0]['endtime'];
                    channelstatus   = data['data'][0]['channel_status'];
                    channel_name    = data['data'][0]['channel_name'];
                    channel_description     = data['data'][0]['channel_description'];
                    channel_email   = data['data'][0]['channel_email'];
                    channel_number  = data['data'][0]['channel_number'];
                    participant_count  = data['data'][0]['participant_count'];
                    program_list_id  = data['data'][0]['program_list_id'];
                    program_desc  = data['data'][0]['program_description'];
                    program_name = data['data'][0]['program_name'];
                    program_bidang = data['data'][0]['program_bidang'];
                    program_banner = data['data'][0]['program_poster_mobile'];
                    program_fasilitas = data['data'][0]['program_fasilitas'];
                    
                    // alert("Disini");
                    console.log(data['data'][0]);
                    if (user_name == "DPP IKA UNY") {
                        // alert("haai");
                        count = 350+participant_count+" Joined";
                        $("#desc_tutor").css('display','none');
                    }
                    if (participant_count == "0" && user_name != "DPP IKA UNY") {
                        count = " ";
                    }
                    else if (participant_count != "0" && user_name != "DPP IKA UNY") 
                    {
                        count = participant_count+" Joined";
                    }
                    if (statusall != null && jenjangnamelevel == "SMA - IPA 10, SMA - IPA 11, SMA - IPA 12") {
                        var desc_jenjang = "Semua Jenjang SMA IPA"
                    }
                    else if (statusall != null && jenjangnamelevel == "SMA - IPS 10, SMA - IPS 11, SMA - IPS 12") {
                        var desc_jenjang = "Semua Jenjang SMA IPS"
                    }
                    else if (statusall != null && jenjangnamelevel == "SMP 7, SMP 8, SMP 9") {
                        var desc_jenjang = "Semua Jenjang SMP"
                    }
                    else if (statusall != null && jenjangnamelevel == "SMK 10, SMK 11, SMK 12") {
                        var desc_jenjang = "Semua Jenjang SMK"
                    }
                    else if (statusall == null && jenjangnamelevel == "SD 1, SD 2, SD 3, SD 4, SD 5, SD 6") {
                        var desc_jenjang = "Semua Jenjang SD"
                    }
                    else if (statusall == null && jenjangnamelevel == "TK, SD 1, SD 2, SD 3, SD 4, SD 5, SD 6") {
                        var desc_jenjang = "TK dan SD"
                    }
                    else if (statusall == null) {
                        var desc_jenjang = jenjangnamelevel;
                    }
                    else if (statusall != null) {
                        var desc_jenjang = "Semua Jenjang"
                    }
                    // alert(statusall);
                    var userutc     = "<?php echo $this->session->userdata('user_utc');?>";
                    var participant_listt = data['data']['participant']['participant'];
                    var im_exist = false;                   
                    if(participant_listt != null){
                        for (var iai = 0; iai < participant_listt.length ;iai++) {                                                              
                            if (id_user == participant_listt[iai]['id_user']) {
                                im_exist = true;
                            }
                        }
                    }

                    var tgl = new Date();        
                    var formattedDate = moment(tgl).format('YYYY-MM-DD');        
                    var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes(); 
                    dateclass = moment(dateclass).format('DD-MM-YYYY');

                    if (class_type=="multicast") {typeclass="Multicast";}else if(class_type=="private"){typeclass="Private";}else if(class_type=="multicast_paid"){typeclass="Multicast Paid";}else{typeclass="Group";}
                    if (template_type=="whiteboard_digital") {template_type="Digital Board";}else if(template_type=="whiteboard_videoboard"){template_type="Video Board";}else if(template_type=="no_whiteboard"){template_type="No Whiteboard";}else{template_type="All Features";}
                    $("#desc_tutorname").append("<i class='zmdi zmdi-assignment-account'></i> "+user_name+" ("+user_age+" tahun)");
                    $("#desc_tutorgender").append("<i class='zmdi zmdi-male-female'></i> "+user_gender);
                    $("#banner_channel").attr('src', program_banner);
                    
                    $("#desc_tutorimage").attr('src', '<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>'+user_image);

                    $("#desc_classid").text(classid);
                    $("#program_desc").text(program_desc);
                    $("#program_name").text(program_name);
                    $("#titleone").text(subject_name +" | "+subject_description);
                    // $("#titletwo").text(subject_description);
                    $("#titlethree").text(desc_jenjang);
                    $("#titlefour").text(count);
                    $("#desc_date").text(dateclass);
                    $("#desc_starttime").text(timeshow+" - ");
                    $("#desc_finishtime").text(endtimeshow);
                    $("#desc_classtype").text(typeclass);
                    $("#desc_templatetype").text(template_type);
                    $("#nametutor").text(user_name);

                    if (class_type == "multicast_channel_paid" || class_type == "private_channel" || class_type == "group_channel") {
                        for (var c = program_bidang.length - 1; c >= 0; c--) {
                            var listbidang = "<li class='ng-binding'>"+program_bidang[c]+"</li>";
                            $("#list_bidang").append(listbidang);
                        }
                        for (var l = program_fasilitas.length - 1; l >= 0; l--) {
                            var listfasilitas = "<li class='ng-binding'>"+program_fasilitas[l]+"</li>";
                            $("#list_fasilitas").append(listfasilitas);
                        }
                        $("#aksi_biasa").css('display','none');
                        $("#boxdescchannel").css('display','block');
                        $("#descc_channelname").text(program_name+" by "+channel_name);
                        $("#descc_channeldesc").text(channel_description);
                        $("#descc_channelemail").text(channel_email);
                        $("#descc_channelphone").text("+"+channel_number);
                        if (im_exist) {                            
                            if (channelstatus == 1 || channelstatus == 2) {
                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                {
                                    $(".buttonalert").css('display','none');
                                    $(".buttonmasuk").css('display','none');
                                    $(".buttonjoin").css('display','none');
                                    $(".buttonalertinvite").css('display','none');
                                    $(".buttonalert").css('display','none');
                                    $(".buttontunggu").css('display','block');
                                    $(".buttontunggu").css('cursor','pointer');
                                }
                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {  

                                    $(".buttonalert").css('display','none');
                                    $(".buttonjoin").css('display','none');
                                    $(".buttontunggu").css('display','none');
                                    $(".buttonalertinvite").css('display','none');
                                    $(".buttonalert").css('display','none');
                                    $(".buttonmasuk").css('display','block');
                                    $(".buttonmasuk").css('cursor','pointer');
                                    $(".buttonmasuk").click(function(){
                                        window.location.href = '<?php echo base_url();?>master/tps_me/'+classid+'?t='+template;
                                    });
                                }
                                else
                                {   
                                    $(".buttonjoin").css('display','none');
                                    $(".buttonmasuk").css('display','none');
                                    $(".buttontunggu").css('display','none');
                                    $(".buttonalertinvite").css('display','none');
                                    $(".buttonalert").css('display','none');
                                    $(".buttonalert").css('display','block');
                                    $(".buttonalert").css('cursor','pointer');
                                }
                            }                        
                            else if (channelstatus == 0) {
                                $(".buttonalert").css('display','none');
                                $(".buttonmasuk").css('display','none');
                                $(".buttontunggu").css('display','none');
                                $(".buttonjoin").css('display','none');
                                $(".buttonalertinvite").css('display','block');
                                $(".buttonalertinvite").css('cursor','pointer');                            
                            }
                            else
                            {
                                $(".buttonjoin").css('display','none');
                                $(".buttonmasuk").css('display','none');
                                $(".buttontunggu").css('display','none');
                                $(".buttonalertinvite").css('display','none');
                                $(".buttonalert").css('display','block');
                                $(".buttonalert").css('cursor','default');
                            }
                        }
                        else
                        {                            
                            if (channelstatus == 1 ||channelstatus == 2) {
                                $(".buttonalert").css('display','none');
                                $(".buttonmasuk").css('display','none');
                                $(".buttontunggu").css('display','none');
                                $(".buttonalertinvite").css('display','none');
                                $(".buttonalert").css('display','none');
                                $(".buttonjoin").css('display','block');
                                $(".buttonjoin").css('cursor','pointer');
                                $(".buttonjoin").click(function(){
                                    // window.location.href = '<?php echo base_url();?>Transaction/multicast_join/'+classid+"?t=channel&user_utc="+userutc;
                                    // var classid = $(this).attr('class_id');    
                                    // alert(program_list_id);
                                    $.ajax({
                                        url: '<?php echo base_url(); ?>Ajaxer/set_code/',
                                        type: 'POST',
                                        data: {                
                                            list_id : program_list_id
                                        }, 
                                        success: function(data)
                                        {              
                                            window.location.href = '<?php echo base_url();?>';
                                        }
                                    });
                                });


                            }                        
                            else if ( channelstatus == 0) {
                                $(".buttonalert").css('display','none');
                                $(".buttonmasuk").css('display','none');
                                $(".buttontunggu").css('display','none');
                                $(".buttonalert").css('display','none');
                                $(".buttonjoin").css('display','none');
                                $(".buttonalertinvite").css('display','block');
                                $(".buttonalertinvite").css('cursor','pointer');                            
                            }
                            else
                            {
                                $(".buttonjoin").css('display','none');
                                $(".buttonmasuk").css('display','none');
                                $(".buttontunggu").css('display','none');
                                $(".buttonalertinvite").css('display','none');
                                $(".buttonalert").css('display','block');
                                $(".buttonalert").css('cursor','default');
                            }
                        }
                    }
                    else
                    {   
                        $("#aksi_biasa").css('display','block');
                        $("#boxdescchannel").css('display','none');
                        if (im_exist) {                     
                            $(".buttonjoin").css('display','none');
                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                            {                                               
                                $(".buttonmasuk").css('display','none');
                                $(".buttonalert").css('display','none');
                                $(".buttonalertinvite").css('display','none');
                                $(".buttontunggu").css('display','block');
                                $(".buttontunggu").css('cursor','default');
                            }
                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                $(".buttontunggu").css('display','none');
                                $(".buttonalert").css('display','none');
                                $(".buttonalertinvite").css('display','none');
                                $(".buttonmasuk").css('display','block');
                                $(".buttonmasuk").click(function(){
                                    window.location.href = '<?php echo base_url();?>master/tps_me/'+classid+'?t='+template;
                                });
                            }
                            
                        }
                        else
                        {
                            $(".buttonmasuk").css('display','none');
                            $(".buttonalert").css('display','none');
                            $(".buttonalertinvite").css('display','none');  
                            $(".buttonjoin").css('display','block');
                            // $("#linktomasuk").attr('href','<?php echo base_url();?>Transaction/multicast_join/'+classid);
                            $(".buttonjoin").click(function(){
                                window.location.href = '<?php echo base_url();?>Transaction/multicast_join/'+classid+'?t=join';
                            });
                        }
                    }

                    Showclass(classid);                 
                }
            });      
        }
  
        var link = null;
        function Showclass(classid)
        {
            // alert(formattedDate);
            var show_linkclass = null;
            var user_utc = new Date().getTimezoneOffset();            
            user_utc = -1 * user_utc;
            var date = new Date();
            date.setDate(date.getDate() + 7);            
            // var dateMsg = date.getDate()+'-'+ (date.getMonth()+1) +'-'+date.getFullYear();            
            var dateMsg = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
            dateMsg = moment(dateMsg).format('YYYY-MM-DD');  
            // alert(formattedDate);
            // alert(dateMsg);
            $.ajax({
                url: '<?php echo BASE_URL();?>Rest/getTutorClasses?tutor_id='+tutor_id+'&start_date='+formattedDate+'&end_date='+dateMsg+'&user_utc='+user_utc+'&user_device=web/access_token/'+tokenjwt,
                type: 'GET',
                success: function(data)
                {            
                    if (data.response.length != null) {
                        for (var i = data.response.length-1; i >=0;i--) {      
                            $("#other_class").css('display','block');                                                                      

                            if (classid == data['response'][i]['class_id']) {
                                var satu = true;
                            }   
                            else
                            {     
                                var key = i;                                                    
                                var show_classid            = data['response'][i]['class_id'];
                                // get_encrypt(show_classid); 
                                var show_subject_id     = data['response'][i]['subject_id'];
                                var show_tutor_id       = data['response'][i]['tutor_id'];
                                var show_user_name      = data['response'][i]['user_name'];
                                var show_subject_name   = data['response'][i]['subject_name'];
                                var show_template_type  = data['response'][i]['template_type'];
                                var show_class_type     = data['response'][i]['class_type'];
                                var show_description    = data['response'][i]['description'];
                                var show_harga          = data['response'][i]['harga'];
                                var show_user_image     = data['response'][i]['user_image'];
                                if (show_template_type == "whiteboard_digital") {
                                    show_template_type = "Digital Whiteboard";
                                }
                                else if (show_template_type == "whiteboard_videoboard") {
                                    show_template_type = "Videoboard Whiteboard";
                                }
                                else if (show_template_type == "no_whiteboard") {
                                    show_template_type = "No Whiteboard";
                                }
                                else{
                                    show_template_type = "All Features";
                                }

                                if (show_harga == null) {
                                    show_harga = "Free class";
                                }
                                
                                var boxclass            = "<a class='"+show_classid+"'><div class='col-md-3 col-sm-6 p-t-5'><div id='best-selling' class='dash-widget-item waves-effect z-depth-1' style='cursor: pointer;'><div class='dash-widget-header'><img class='' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+show_user_image+"' style='z-index: 1; position: absolute; margin: 3%; height: 40px; width: 40px; top: 5; right: 0;'><img src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>headers/BGclassroom-01.png'><div class='dash-widget-title'>"+show_template_type+"</div><div class='main-item'><h2>"+show_harga+"</h2></div></div><div class='listview p-15'><div class='f-14 c-gray'>"+show_user_name+"</div><br><small class='f-18 c-black'>"+show_subject_name+" - </small><br><small class='f-18 c-black'>"+show_description+"</small></div></div></div></a>";                                
                                $("#boxclass_more").append(boxclass);

                                get_encrypt(show_classid);
                            }

                        }
                    }
                    if (satu == true){
                        $("#other_class").css('display','none');
                    }
                    else{
                        $("#other_class").css('display','block');   
                    }
                }
            });

        }

        function get_encrypt(show_classid)
        {            
            $.ajax({
                url: '<?php echo base_url(); ?>Tools/get_c/' + show_classid,
                type: 'GET',
                /*data: {
                    class_id: classid
                },*/
                success: function(data)
                {    
                    
                    data = data.trim();                                          
                    c = data;   
                    
                    link = "<?php echo base_url();?>class?c="+data;                                        
                    $("."+show_classid).attr('href',link);                                        
                }
            });
        }

        function getMobileOperatingSystem() {
          var userAgent = navigator.userAgent || navigator.vendor || window.opera;

              // Windows Phone must come first because its UA also contains "Android"
            if (/windows phone/i.test(userAgent)) {
                return "Windows Phone";
            }

            if (/android/i.test(userAgent)) {
                return "Android";
            }

            // iOS detection from: http://stackoverflow.com/a/9039885/177710
            if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                return "iOS";
            }

            return "unknown";
        }
        var type = getMobileOperatingSystem();
        if (type == "Android") {
            window.location.href = 
            "intent:#Intent;action=com.classmiles.student;category=android.intent.category.DEFAULT;category=android.intent.category.BROWSABLE;S.msg_from_browser=Launched%20from%20Browser;end";
        }
    });
</script>