<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('topup/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" style="margin-top: 5%;">
        <div class="container invoice">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2><?php echo $this->lang->line('konfirmasipembayaran'); ?></h2>
                <!-- <ul class="actions">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><a href="<?php echo base_url();?>first/topup">Top up</a></li>
                        <li><?php echo $this->lang->line('konfirmasipembayaran'); ?></li>
                    </ol>                    
                </ul> -->
            </div>
                    
            <?php
            if ($_GET['trans'] == 'edt') {
                $trasaksiid = $_GET['transaksiid'];
                $getjumlah = $this->db->query("SELECT * FROM log_trf_confirmation WHERE trx_id='$trasaksiid'")->row_array();
                $gettotal  = $this->db->query("SELECT credit FROM log_transaction WHERE trx_id='$trasaksiid'")->row_array();
                $hasiljumlah = number_format($getjumlah['jumlah'], 0, ".", ".");
                $hasiltotal = number_format($gettotal['credit'], 0, ".", ".");
                $flag = $getjumlah['metod_tr'];
                $flagbank = $getjumlah['bank_id'];
                $from_bank = $getjumlah['from_bank'];
                $norek_tr = $getjumlah['norek_tr'];
                $iduserr = $this->session->userdata('id_user');
            ?>
            <div class="card">
                <div class="card-header ch-alt">
                    <div class="pull-left f-14" style="margin-top: 8px;">No <?php echo $this->lang->line('transaksi'); ?> <?php echo $_GET['transaksiid']; ?></div><br>
                    <div class="pull-right f-14" style="margin-top: -10px;"><?php echo $this->lang->line('transaksi'); ?> <?php echo $this->lang->line('tanggal'); ?> <?php echo $getjumlah['time']; ?></div><br>
                </div>
                
                <div class="card-body card-padding" style="background: white;">                
                    <div class="row">                        
                        <form method="post" action="<?php echo base_url('Process/edtconfirm'); ?>" enctype="multipart/form-data">
                            <div class="col-md-2 m-t-25"></div>
                            <div class="col-md-8 m-t-25 m-b-25" style="border: 1px solid gray; border-radius: 5px; padding: 15px;">
                                
                                <input type="text" name="order_id" id="order_id" value="<?php echo $trasaksiid; ?>" hidden />                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('tujuantransfer'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">

                                            <select required name="bank" class="select2 col-md-6 form-control">                                      
                                                <?php
                                                    $allbank = $this->db->query("SELECT * FROM master_bank ORDER BY no ASC")->result_array();
                                                    foreach ($allbank as $row => $d) {
                                                        if($d['bank_id'] == $flagbank){
                                                            echo "<option value='".$d['bank_id']."' selected='true'>".$d['bank_name']."</option>";
                                                        }
                                                        if ($d['bank_id'] != $flagbank) {
                                                            echo "<option value='".$d['bank_id']."'>".$d['bank_name']."</option>";
                                                        }                                                            
                                                    }
                                                ?> 
                                            </select>                                            
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('jumlahtransfer'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="nominaltransaksi" id="nominal" required class="input-sm form-control fg-input" placeholder="Isi Jumlah transfer" minlength="9" step="500" maxlength="14" min="10000" max="500000" value="<?php echo $getjumlah['jumlah'] ?>"/>
                                            <input type="text" name="nominalsave" id="nominalsave" value="<?php echo $getjumlah['jumlah'] ?>" hidden />
                                        </div>
                                        <label class="c-gray m-t-5"><ins><small>( <?php echo $this->lang->line('jumlahharus'); ?> Rp. <?php echo $hasiltotal; ?> )</small></ins></label>
                                    </div>
                                </div>                                  
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('fromrekening');?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="norekeningbank" class="select2 col-md-6 form-control">                                
                                                <?php
                                                    $allbank = $this->db->query("SELECT * FROM tbl_rekening WHERE id_user='$iduserr'")->result_array();
                                                    foreach ($allbank as $row => $d) {                                                        
                                                        if($d['nomer_rekening'] == $norek_tr){
                                                            echo "<option value='".$d['nomer_rekening']."' selected='true'>".$d['nomer_rekening']." - ".$d['nama_bank']." (".$d['nama_akun'].")</option>";
                                                        }
                                                        if ($d['nomer_rekening'] != $norek_tr) {
                                                            echo "<option value='".$d['nomer_rekening']."'>".$d['nomer_rekening']." - ".$d['nama_bank']." (".$d['nama_akun'].")</option>";
                                                        }                                                            
                                                    }
                                                ?> 
                                            </select>    
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Atas nama pemilik</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="namapemilikbank" placeholder="<?php echo $this->lang->line('namadirekening'); ?>" value="<?php echo $getjumlah['nama_tr'] ?>" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('denganmetode'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="metodepembayaran" class="select2 col-md-6 form-control">                                      
                                                <option disabled selected><?php echo $this->lang->line('pilihmetode'); ?></option>
                                                <option value="atm" <?php if($flag=="atm"){ echo "selected='true'";} ?>>ATM Transfer</option>                                                  
                                                <option value="internetbanking" <?php if($flag=="internetbanking"){ echo "selected='true'";} ?>>Internet Banking</option>
                                                <option value="mobilebanking" <?php if($flag=="mobilebanking"){ echo "selected='true'";} ?>>Mobile Banking</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-5"><?php echo $this->lang->line('buktipembayaran'); ?></label>
                                    <div class="col-sm-9 m-t-5">
                                        <div class="fg-line">
                                            <input type="file" name="buktibayar" accept="image/*" value="<?php echo $getjumlah['bukti_tr']; ?>">
                                            <input type="text" name="bukti_tr" value="<?php echo $getjumlah['bukti_tr']; ?>" hidden>
                                        </div>
                                        <small class="c-gray"><?php echo $this->lang->line('peringatan'); ?></small>
                                    </div>
                                </div>                                

                                <br><br><br>
                                <button class="btn btn-md c-white btn-block m-t-10 bgm-blue" style="border: 1px solid gray;"><?php echo $this->lang->line('ubahkonfirmasi'); ?></button>
                            </div>
                            <div class="col-md-2 m-t-25"></div>
                                                    
                        </form>

                    </div>
                </div>
                
                <!-- <footer class="p-20">
                    <a href="<?php echo base_url();?>first/topup"><button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;">Konfirmasi</button></a>
                </footer> -->
            </div>
            <?php
            }
            else if ($_GET['trans'] == 'knf') {                               
                $trasaksiid = $_GET['transaksiid'];
                $getjumlah = $this->db->query("SELECT * FROM log_transaction WHERE trx_id='$trasaksiid'")->row_array();
                $hasiljumlah = number_format($getjumlah['credit'], 0, ".", ".");
            ?>
            <div class="card">
                <div class="card-header ch-alt">
                    <div class="pull-left f-14" style="margin-top: 8px;">No <?php echo $this->lang->line('transaksi'); ?> <?php echo $this->session->userdata('idorder'); ?></div><br>
                    <div class="pull-right f-14" style="margin-top: -10px;"><?php echo $this->lang->line('transaksi'); ?> <?php echo $this->lang->line('tanggal'); ?> <?php echo $getjumlah['timestamp']; ?></div><br>
                </div>
                
                <div class="card-body card-padding" style="background: white;">                
                    <div class="row">                        
                        <form method="post" action="<?php echo base_url('Process/confrim'); ?>" enctype="multipart/form-data">
                            <div class="col-md-2 m-t-25"></div>
                            <div class="col-md-8 m-t-25 m-b-25" style="border: 1px solid gray; border-radius: 5px; padding: 15px;">

                                <input type="text" name="orderid" value="<?php echo $trasaksiid; ?>" hidden>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('tujuantransfer'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="bank" class="select2 col-md-6 form-control">
                                            <?php
                                               $allbank = $this->db->query("SELECT * FROM master_bank")->result_array();
                                               $tujuan = $this->lang->line('tujuantransfer');
                                               echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line("pilih").' '. $this->lang->line("tujuantransfer").'</option>'; 
                                               foreach ($allbank as $row => $v) {
                                                    echo '<option value="'.$v['bank_id'].'">'.$v['bank_name'].'</option>';
                                                }
                                            ?>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('jumlahtransfer'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="nominaltransaksi" id="nominal" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('inputjumlah'); ?>" minlength="9" step="500" maxlength="14" min="10000" max="500000"/>
                                            <input type="text" name="nominalsave" id="nominalsave" hidden />
                                        </div>
                                        <label class="c-gray m-t-5"><ins><small>( <?php echo $this->lang->line("jumlahharus");?> Rp. <?php echo $hasiljumlah; ?> )</small></ins></label>
                                    </div>
                                </div>                                  
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('fromrekening'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                             <select required name="frombank" class="select2 col-md-6 form-control">
                                            <?php
                                                $iduser= $this->session->userdata('id_user');
                                                $allbank = $this->db->query("SELECT * FROM tbl_rekening where id_user='$iduser'")->result_array();
                                                $tujuan = $this->lang->line('masukanrekening');
                                                echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line("pilih").' '. $this->lang->line("masukanrekening").'</option>'; 
                                                foreach ($allbank as $row => $v) {
                                                    echo '<option value="'.$v['nomer_rekening'].'">'.$v['nomer_rekening'].' - '.$v['nama_bank'].' ('.$v['nama_akun'].')</option>';
                                                }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Atas nama pemilik</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="namapemilikbank" placeholder="<?php echo $this->lang->line('namadirekening'); ?>" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('denganmetode'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="metodepembayaran" class="select2 col-md-6 form-control">                                      
                                                <option disabled selected><?php echo $this->lang->line('pilihmetode'); ?></option>
                                                <option value="atm">ATM Transfer</option>                                                  
                                                <option value="internetbanking">Internet Banking</option>
                                                <option value="mobilebanking">Mobile Banking</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-5"><?php echo $this->lang->line('buktipembayaran'); ?></label>
                                    <div class="col-sm-9 m-t-5">
                                        <div class="fg-line">
                                            <input type="file" required name="buktibayar" accept="image/*">
                                        </div>
                                    </div>
                                </div>                                

                                <br><br><br>
                                <button class="btn btn-md c-white btn-block m-t-10 bgm-blue" id="kotak" style="border: 1px solid gray;"><?php echo $this->lang->line('konfirmasi'); ?></button>
                            </div>
                            <div class="col-md-2 m-t-25"></div>
                                                    
                        </form>

                    </div>
                </div>
                
                <!-- <footer class="p-20">
                    <a href="<?php echo base_url();?>first/topup"><button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;">Konfirmasi</button></a>
                </footer> -->
            </div>
            <?php
                }
            ?>
        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function(){
        $('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $("#nominal").keyup(function() {
          var clone = $(this).val();
          var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
          $("#nominalsave").val(cloned);
        });
    });

</script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({
                    locale: 'id'
                });
            });
        </script>
