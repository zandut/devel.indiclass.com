<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('topup/navbar'); ?>
</header>
    <script type="text/javascript"
            src="https://app.midtrans.com/snap/snap.js"
            data-client-key="VT-client-Ei8u5Sr2RPLgCUew"></script>
<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" style="margin-top: 5%;">
        <div class="container">
            <?php if($this->session->flashdata('mes_alert')){ ?>
                <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
                </div>
            <?php } ?>
            <div class="block-header" style="margin-bottom: 50px;">
                <h2 style="margin-left: 3px;">Top up</h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li>Top up</li>
                    </ol>                    
                </ul> -->
            </div>

            <div class="card m-t-20" style="padding: 15px;">
                <div class="card-header">
                    <h2>Top up Uang Saku</h2><small class="m-t-15"><?php echo $this->lang->line('deskripsitopup'); ?></small>
                    <hr>               
                </div>

                <div class="card-header" style="margin-top: -4%;">
                    <div class="row">

                        <label class="f-16 p-l-15" style="margin-left: 3%;"><?php echo $this->lang->line('pembayaran'); ?></label>

                        <!-- <form method="post" action="<?php echo base_url();?>Process/finishtr"> -->
                        <!-- NOMINAL DAN METODE PEMBAYARAN -->
                        <!-- <div class="form-group fg-float" style="margin-left: 4%;">
                            <label><b><?php echo $this->lang->line('nominalinput'); ?></b></label>
                            <div class="fg-line">
                                <input type="text" name="nominal" id="nominal" required class="input-sm form-control fg-input" minlength="9" step="500" maxlength="14" min="10000" max="1000000"/>
                                <input type="text" name="nominalsave" id="nominalsave" hidden />
                            </div>
                            <label class="f-9">(Minimal Rp. 10.000)</label><br><br>
                            <small><?php echo $this->lang->line('pilihpayment'); ?></small>
                        </div> -->

                        <br><label class="p-l-15" style="margin-left: 3%;"><b><?php echo $this->lang->line('nominalinput'); ?></b></label>  
                        <div class="col-md-12 m-t-10 m-b-5" style="padding: 10px; margin-left: 3%;">		       

                        	<div class="col-md-2 m-t-5" style="padding:10px; border-radius: 8px; margin-right: 2.5%; background-color: #edecec;">
                        		<div class="radio m-l-10">
                                    <label>
                                        <input type="radio" name="nominaltopup" id="topup25" value="25000">
                                        <i class="input-helper"></i>
                                        <label style="font-family: Helvetica; font-size: 14px;">Rp. 25.000</label>
                                    </label>
                                </div>
                        	</div>                  	 
                        	<div class="col-md-2 m-t-5" style="padding:10px; border-radius: 8px; margin-right: 2.5%; background-color: #edecec;">
                        		<div class="radio m-l-10">
                                    <label>
                                        <input type="radio" name="nominaltopup" id="topup50" value="50000">
                                        <i class="input-helper"></i>
                                        <label style="font-family: Helvetica; font-size: 14px;">Rp. 50.000</label>
                                    </label>
                                </div>                        		                        		
                        	</div>                        	
                        	<div class="col-md-2 m-t-5" style="padding:10px; border-radius: 8px; margin-right: 2.5%; background-color: #edecec;">
                        		<div class="radio m-l-10">
                                    <label>
                                        <input type="radio" name="nominaltopup" id="topup100" value="100000">
                                        <i class="input-helper"></i>
                                        <label style="font-family: Helvetica; font-size: 14px;">Rp. 100.000</label>
                                    </label>
                                </div>                          		                        		
                        	</div>                        	
                        	<div class="col-md-2 m-t-5" style="padding:10px; border-radius: 8px; margin-right: 2.5%; background-color: #edecec;">
                        		<div class="radio m-l-10">
                                    <label>
                                        <input type="radio" name="nominaltopup" id="topup200" value="200000">
                                        <i class="input-helper"></i>
                                        <label style="font-family: Helvetica; font-size: 14px;">Rp. 200.000</label>
                                    </label>
                                </div>                         		                        		
                        	</div>                        	
                        	<div class="col-md-2 m-t-5" style="padding:10px; border-radius: 8px; margin-right: 2.5%; background-color: #edecec;">
                        		<div class="radio m-l-10">
                                    <label>
                                        <input type="radio" name="nominaltopup" id="topup500" value="500000">
                                        <i class="input-helper"></i>
                                        <label style="font-family: Helvetica; font-size: 14px;">Rp. 500.000</label>
                                    </label>
                                </div>                         		                        		
                        	</div>
                        	<input type="text" name="nominalsave" id="nominalsave" hidden />
                        	<br><br><br><hr class="col-md-11">
                        </div>

                    	<center>
                        
                        <div class="col-md-11 m-b-15" id="k4" style="background: #edecec; margin-left: 4%; padding: 15px;">
                            
                            <div id="mandiricheck" class="row">
                                <div class="pull-left" style="padding: 10px;">
                                    <div class="radio m-l-15 m-t-10">
                                        <label>
                                            <input type="radio" name="transferbank" id="mandiri" value="">
                                            <i class="input-helper"></i>
                                            <label class="f-14">Transfer Bank Mandiri</label>
                                        </label>
                                    </div>
                                </div>

                                <div class="pull-right" style="padding: 10px;">
                                    <img src="<?php echo base_url();?>aset/images/logo-mandiri.gif" style="width: 150px; height: 50px;">
                                    <!-- <img src="<?php echo base_url();?>aset/images/mastercard.png" style="width: 70px; height: 35px; margin-top: 4px;"> -->
                                </div>
                            </div>

                        </div>

                        <div class="col-md-11" id="k5" style="background: #edecec; margin-left: 4%; padding: 15px;">
                            
                            <div id="permatacheck" class="row">
                                <div class="pull-left" style="padding: 10px;">
                                    <div class="radio m-l-15 m-t-10">
                                        <label>
                                            <input type="radio" name="transferbank" id="permata" value="">
                                            <i class="input-helper"></i>
                                            <label class="f-14">Transfer Bank Permata</label>
                                        </label>
                                    </div>
                                </div>

                                <div class="pull-right" style="padding: 10px;">
                                    <img src="<?php echo base_url();?>aset/images/banklogo/permata.png" style="width: 130px; height: 35px;">
                                    <!-- <img src="<?php echo base_url();?>aset/images/mastercard.png" style="width: 70px; height: 35px; margin-top: 4px;"> -->
                                </div>
                            </div>

                        </div>

                        <div class="col-md-11 m-b-15" id="k1" style="margin-top: 2%; margin-left: 4%; padding: 15px; background: #edecec;">

                            <div id="kreditcheck" class="row">
                                <div class="pull-left" style="padding: 10px;">
                                    <div class="radio m-l-15 m-t-20">
                                        <label>
                                            <input type="radio" name="transferbank" id="transferbank" value="transfer_bank">
                                            <i class="input-helper"></i>
                                            <!-- <label class="f-14"><?php echo $this->lang->line('transferbank'); ?></label> -->
                                            <label class="f-14">Transfer Bank lainnya</label>
                                        </label>
                                    </div>
                                </div>

                                <div class="pull-right" style="padding: 10px;">
                                    <!-- <img src="<?php echo base_url();?>aset/images/logo-mandiri.gif" style="width: 100px; height: 35px; margin-top: 8px; margin-right: -7px;"> -->
                                    <img src="<?php echo base_url();?>aset/images/logo-bca.gif" style="width: 100px; height: 35px; margin-top: 8px; margin-right: -7px;">
                                    <img src="<?php echo base_url();?>aset/images/logo-bni.gif" style="width: 100px; height: 35px; margin-top: 8px; margin-right: -7px;">
                                    <img src="<?php echo base_url();?>aset/images/logo-bri.gif" style="width: 100px; height: 35px; margin-top: 8px; margin-right: -7px;">
                                    <img src="<?php echo base_url();?>aset/images/banklogo/cimb1.png" style="width: 120px; height: 45px; margin-top: 8px;">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-11 m-b-15" id="k2" style="background: #edecec; margin-left: 4%; padding: 15px;">
                            
                            <div id="kreditcheck" class="row">
                                <div class="pull-left" style="padding: 10px;">
                                    <div class="radio m-l-15 m-t-10">
                                        <label>
                                            <input type="radio" name="transferbank" id="kartukredit" value="">
                                            <i class="input-helper"></i>
                                            <label class="f-14"><?php echo $this->lang->line('kartukredit'); ?></label>
                                        </label>
                                    </div>
                                </div>

                                <div class="pull-right" style="padding: 10px;">
                                    <img src="<?php echo base_url();?>aset/images/custom2.png" style="width: 250px; height: 50px;">
                                    <!-- <img src="<?php echo base_url();?>aset/images/mastercard.png" style="width: 70px; height: 35px; margin-top: 4px;"> -->
                                </div>
                            </div>

                        </div>

                        <div class="col-md-11 m-b-15" id="k3" style="background: #edecec; margin-left: 4%; padding: 15px;">
                            
                            <div id="paypalcheck" class="row">
                                <div class="pull-left" style="padding: 10px;">
                                    <div class="radio m-l-15 m-t-10">
                                        <label>
                                            <input type="radio" name="transferbank" id="paypal" value="">
                                            <i class="input-helper"></i>
                                            <label class="f-14">Paypal</label>
                                        </label>
                                    </div>
                                </div>

                                <div class="pull-right" style="padding: 10px;">
                                    <img src="<?php echo base_url();?>aset/images/banklogo/paypal_logo.png" style="width: 150px; height: 35px;">
                                    <!-- <img src="<?php echo base_url();?>aset/images/mastercard.png" style="width: 70px; height: 35px; margin-top: 4px;"> -->
                                </div>
                            </div>

                        </div></center>
                        <hr>

                        <div class="col-md-11 m-b-15" id="k2" style="margin-left: 4%;">                            
                            <div class="row">
                                <button class="btn btn-md c-black btn-block m-t-5" style="background: #f9f9f9; border: 1px solid gray;" id="transferbtn"><?php echo $this->lang->line('selanjutnya'); ?></button>
                                <button class="btn btn-md c-black btn-block m-t-5" style="background: #f9f9f9; border: 1px solid gray; display: none;" id="pay-mandiri"><?php echo $this->lang->line('selanjutnya'); ?></button>
                                <button class="btn btn-md c-black btn-block m-t-5" style="background: #f9f9f9; border: 1px solid gray; display: none;" id="pay-permata"><?php echo $this->lang->line('selanjutnya'); ?></button>

                                <button class="btn btn-md c-black btn-block m-t-5" style="background: #f9f9f9; border: 1px solid gray; display: none;" id="pay-button"><?php echo $this->lang->line('selanjutnya'); ?></button>
                                <button class="btn btn-md c-black btn-block m-t-5" style="background: #f9f9f9; border: 1px solid gray; display: none;" id="paypaypal"><?php echo $this->lang->line('selanjutnya'); ?></button>
                            </div>                            
                        </div>
                        
                        <!-- </form> -->

                    </div>
                   
                    <form id="payment-form" method="post" action="<?=site_url()?>process/finish">
                      <input type="hidden" name="result_type" id="result-type" value=""></div>
                      <input type="hidden" name="result_data" id="result-data" value=""></div>
                    </form>
                                        
                </div>
            </div>           

        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

    <script type="text/javascript">
    if (document.getElementById("topup25").checked == true) {
        alert("You have selected Option 1");
    }

    $('#transferbtn').click(function(){        
        // if (a < 10000) {
        //     alert('Minimal nominal top up Rp.10.000');
        //     return false;
        // }
        if (!document.getElementById('topup25').checked&&!document.getElementById('topup50').checked&&!document.getElementById('topup100').checked&&!document.getElementById('topup200').checked&&!document.getElementById('topup500').checked) {
        	// alert('Mohon pilih nominal topup');
        	notify('top','center','fa fa-check','danger','animated fadeInDown','animated fadeOut','Mohon pilih nominal topup');
        	return false;
        }
    	else if(!document.getElementById('transferbank').checked) {
		  	// alert('Mohon pilih metode pembayaran');
		  	notify('top','center','fa fa-check','danger','animated fadeInDown','animated fadeOut','Mohon pilih metode pembayaran');
            return false;
		}
		else
		{			
			var a = $("input[name='nominaltopup']:checked").val();					
			$.ajax({   
				url: '<?php echo base_url();?>Process/finishtr',                             
                // url: '<?php echo base_url(); ?>admin/approve_support?idrequest='+idrequest+'&idttr='+idttr,    
                type: 'POST', // performing a POST request
                data : {
                    nominalsave : a                    
                },                  
                success: function(data){
                	data = JSON.parse(data);
                	var linkfinish = data['link'];
                	window.location.replace(linkfinish);
                }
            });
		}
    });

    $('#pay-mandiri').click(function (event) {
        if (!document.getElementById('topup25').checked&&!document.getElementById('topup50').checked&&!document.getElementById('topup100').checked&&!document.getElementById('topup200').checked&&!document.getElementById('topup500').checked) {
            // alert('Mohon pilih nominal topup');
            notify('top','center','fa fa-check','danger','animated fadeInDown','animated fadeOut','Mohon pilih nominal topup');
            return false;
        }
        else if(!document.getElementById('mandiri').checked) {
            // alert('Mohon pilih metode pembayaran');
            notify('top','center','fa fa-check','danger','animated fadeInDown','animated fadeOut','Mohon pilih metode pembayaran');
            return false;
        }
        else
        {
            event.preventDefault();
            var a = $("input[name='nominaltopup']:checked").val();

            $.ajax({
                url: '<?php echo base_url(); ?>/process/mandiri',
                method: 'POST',
                data: {
                    amount:a
                },
                cache: false,

                success: function(data) {
                //location = data;

                    console.log('token = '+data);
                
                    var resultType = document.getElementById('result-type');
                    var resultData = document.getElementById('result-data');

                    function changeResult(type,data){
                        $("#result-type").val(type);
                        $("#result-data").val(JSON.stringify(data));                      
                    }

                    snap.pay(data, {                  
                        onSuccess: function(result){
                            changeResult('success', result);
                            console.log(result.status_message);
                            console.log(result);
                            $("#payment-form").submit();
                            },
                            onPending: function(result){
                            changeResult('pending', result);
                            console.log(result.status_message);
                            $("#payment-form").submit();
                            },
                            onError: function(result){
                            changeResult('error', result);
                            console.log(result.status_message);
                            $("#payment-form").submit();
                        }
                    });
                }
            });
        }
    });

    $('#pay-permata').click(function (event) {
        if (!document.getElementById('topup25').checked&&!document.getElementById('topup50').checked&&!document.getElementById('topup100').checked&&!document.getElementById('topup200').checked&&!document.getElementById('topup500').checked) {
            // alert('Mohon pilih nominal topup');
            notify('top','center','fa fa-check','danger','animated fadeInDown','animated fadeOut','Mohon pilih nominal topup');
            return false;
        }
        else if(!document.getElementById('permata').checked) {
            // alert('Mohon pilih metode pembayaran');
            notify('top','center','fa fa-check','danger','animated fadeInDown','animated fadeOut','Mohon pilih metode pembayaran');
            return false;
        }
        else
        {
            event.preventDefault();
            var a = $("input[name='nominaltopup']:checked").val();

            $.ajax({
                url: '<?php echo base_url(); ?>/process/permata',
                method: 'POST',
                data: {
                    amount:a
                },
                cache: false,

                success: function(data) {
                //location = data;

                    console.log('token = '+data);
                
                    var resultType = document.getElementById('result-type');
                    var resultData = document.getElementById('result-data');

                    function changeResult(type,data){
                        $("#result-type").val(type);
                        $("#result-data").val(JSON.stringify(data));                      
                    }

                    snap.pay(data, {                  
                        onSuccess: function(result){
                            changeResult('success', result);
                            console.log(result.status_message);
                            console.log(result);
                            $("#payment-form").submit();
                            },
                            onPending: function(result){
                            changeResult('pending', result);
                            console.log(result.status_message);
                            $("#payment-form").submit();
                            },
                            onError: function(result){
                            changeResult('error', result);
                            console.log(result.status_message);
                            $("#payment-form").submit();
                        }
                    });
                }
            });
        }
    });

    $('#pay-button').click(function (event) {
    	if (!document.getElementById('topup25').checked&&!document.getElementById('topup50').checked&&!document.getElementById('topup100').checked&&!document.getElementById('topup200').checked&&!document.getElementById('topup500').checked) {
        	// alert('Mohon pilih nominal topup');
        	notify('top','center','fa fa-check','danger','animated fadeInDown','animated fadeOut','Mohon pilih nominal topup');
        	return false;
        }
    	else if(!document.getElementById('kartukredit').checked) {
		  	// alert('Mohon pilih metode pembayaran');
		  	notify('top','center','fa fa-check','danger','animated fadeInDown','animated fadeOut','Mohon pilih metode pembayaran');
            return false;
		}
		else
		{
		    event.preventDefault();
		    var a = $("input[name='nominaltopup']:checked").val();

		    $.ajax({
		      	url: '<?php echo base_url(); ?>/process/token',
		      	method: 'POST',
		      	data: {
			        amount:a
		      	},
		      	cache: false,

		      	success: function(data) {
		        //location = data;

		        	console.log('token = '+data);
		        
		        	var resultType = document.getElementById('result-type');
		        	var resultData = document.getElementById('result-data');

			        function changeResult(type,data){
			          	$("#result-type").val(type);
			          	$("#result-data").val(JSON.stringify(data));			          
			        }

			        snap.pay(data, {		          
						onSuccess: function(result){
							changeResult('success', result);
							console.log(result.status_message);
							console.log(result);
							$("#payment-form").submit();
							},
							onPending: function(result){
							changeResult('pending', result);
							console.log(result.status_message);
							$("#payment-form").submit();
							},
							onError: function(result){
							changeResult('error', result);
							console.log(result.status_message);
							$("#payment-form").submit();
						}
			        });
		      	}
		    });
    	}
  	});
  	$("#paypaypal").click(function(){
    	if(!document.getElementById('paypal').checked) {
		  	alert('Mohon pilih metode pembayaran');
            return false;
		}
		else
		{ 
             event.preventDefault();
            var a = $("input[name='nominaltopup']:checked").val();

            if (!document.getElementById('topup25').checked&&!document.getElementById('topup50').checked&&!document.getElementById('topup100').checked&&!document.getElementById('topup200').checked&&!document.getElementById('topup500').checked) {
	        	// alert('Mohon pilih nominal topup');
	        	notify('top','center','fa fa-check','danger','animated fadeInDown','animated fadeOut','Mohon pilih nominal topup');
	        	return false;
	        }
	    	else if(!document.getElementById('paypal').checked) {
			  	// alert('Mohon pilih metode pembayaran');
			  	notify('top','center','fa fa-check','danger','animated fadeInDown','animated fadeOut','Mohon pilih metode pembayaran');
	            return false;
			}
            else
            {
                // document.location = 'do_purchase';
                $.ajax({   
					url: '<?php echo base_url();?>first/do_purchase',                             
	                // url: '<?php echo base_url(); ?>admin/approve_support?idrequest='+idrequest+'&idttr='+idttr,    
	                type: 'POST', // performing a POST request
	                data : {
	                    nominalsave : a                    
	                },                  
	                success: function(data){
	                	// data = JSON.parse(data);
	                	// var linkfinish = data['link'];
	                	window.location.reload();
	                }
	            });
            }
		}
    });

  </script>

<script type="text/javascript">
    $(document).ready(function(){

        // $('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $(".nominaltopup").keyup(function() {
		  var clone = $(this).val();		  
		  $("#nominalsave").val(clone);
		});

		// $(".nominaltopup").keyup(function() {
		//   var clone = $(this).val();
		//   var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
		//   $("#total").val(cloned);
		// });
    

        $("#transferbank").click(function()
        { 
            $('#k2').css("border", "#ffffff solid 1px");
            $("k3").css("border", "#ffffff solid 1px");
            $('#k4').css("border", "#ffffff solid 1px");
            $('#k5').css("border", "#ffffff solid 1px");
            $('#k1').css("border", "#00796B solid 1px");
            $('#pay-permata').css('display','none');
            $('#pay-button').css('display','none');
            $('#pay-paypal').css('display','none');
            $('#pay-mandiri').css('display','none');
            $('#transferbtn').css('display','block');
        });

        $("#mandiri").click(function()
        { 
            $('#k1').css("border", "#ffffff solid 1px");
            $("k3").css("border", "#ffffff solid 1px");
            $('#k2').css("border", "#ffffff solid 1px");
            $('#k5').css("border", "#ffffff solid 1px");
            $('#k4').css("border", "#00796B solid 1px");            
            $('#pay-permata').css('display','none');
            $('#transferbtn').css('display','none');
            $('#pay-paypal').css('display','none');
            $('#pay-button').css('display','none');
            $('#pay-mandiri').css('display','block');
        });

        $("#permata").click(function()
        { 
            $('#k1').css("border", "#ffffff solid 1px");
            $("k3").css("border", "#ffffff solid 1px");
            $('#k2').css("border", "#ffffff solid 1px");
            $('#k4').css("border", "#ffffff solid 1px");
            $('#k5').css("border", "#00796B solid 1px");
            $('#transferbtn').css('display','none');
            $('#pay-paypal').css('display','none');
            $('#pay-button').css('display','none');
            $('#pay-mandiri').css('display','none');
            $('#pay-permata').css('display','block');
        });


        $("#kartukredit").click(function()
        { 
            $('#k1').css("border", "#ffffff solid 1px");
            $("k3").css("border", "#ffffff solid 1px");
            $('#k4').css("border", "#ffffff solid 1px");
            $('#k5').css("border", "#ffffff solid 1px");
            $('#k2').css("border", "#00796B solid 1px");
            $('#transferbtn').css('display','none');
            $('#pay-permata').css('display','none');
            $('#pay-paypal').css('display','none');
            $('#pay-mandiri').css('display','none');
            $('#pay-button').css('display','block');
            
        });

        $("#paypal").click(function()
        {	
        	$("k1").css("border", "#ffffff solid 1px");
        	$("k2").css("border", "#ffffff solid 1px");
            $('#k4').css("border", "#ffffff solid 1px");
            $('#k5').css("border", "#ffffff solid 1px");
        	$("k3").css("border", "#00796B solid 1px");
        	$('#transferbtn').css('display','none');
            $('#pay-button').css('display','none');
            $('#pay-permata').css('display','none');
            $('#pay-mandiri').css('display','none');
            $('#paypaypal').css('display','block');

        });

        $('#pay-button').click(function(){
            $('#pay-button').css('display','block');
            $('#pay-button').css('disabled','');
        })
    });
</script>
