<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('topup/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" style="margin-top: 5%;">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Cara Top up</h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li>Cara Top up</li>
                    </ol>                    
                </ul> -->
            </div>

            <div class="card m-t-20">
                
                <div class="card-header">
                    <h2>Bagaimana Cara Mengisi Saldo</h2><small class="m-t-15">Isi saldo anda untuk pembelian yang cepat dan mudah.</small>
                    <hr>               
                </div>

                <div class="card-body card-padding">
                    <div class="row f-13" style="margin: 2px;">
                        <p>Ingin menambah kategori pelajaran dengan mudah, cepat, dan aman. Transaksikan lewat Uang Saku saja, lebih praktis! Metode pembayaran untuk mengisi ulang Saldo yaitu Memakai kartu kredit dan pembayaran Transfer bank!</p>

                        <p>Berikut penjelasan bagaimana cara untuk <b>Top up Saldo Classmiles</b> :</p>

                        <p>1. Pastikan sudah login akun <b>Classmiles</b> Anda</p>
                        <p>2. Pilih <b>Menu E-Pocket / Uang Saku</b></p>
                        <img src="<?php echo base_url();?>aset/images/caratopup/pilihmenu.png" width="100%" height="600px">

                        <p class="m-t-20">3. Setelah Masuk ke Halaman Uang saku, Anda bisa memilih ke bagian menu <b>Top up</b>, dan Pilih Metode Pembayaran dan masukan Nominal isi ulang saldo untuk dimasukan ke Saldo Uang saku Classmiles anda. Jika sudah, Klik tombol <b>lanjut</b>. </p>
                        <center><img src="<?php echo base_url();?>aset/images/caratopup/topuangsaku.png" width="65%" height="300px"></center><br>
                        <center><img src="<?php echo base_url();?>aset/images/caratopup/topupsaldo.png" width="65%" height="350px"></center>                        

                        <p class="m-t-20">4. Anda akan diarahkan pada halaman rincian jumlah uang yang akan di transfer ke Rekening Classmiles. Klik tombol <b>Konfirmasi Pembayaran</b> Jika anda sudah membayar uang sesuai nominal yang anda isikan.</p>
                        <center><img src="<?php echo base_url();?>aset/images/caratopup/rinciantopupsaldo.png" width="75%" height="550px"></center><br>
                        <center><img src="<?php echo base_url();?>aset/images/caratopup/topupkonfirmasi.png" width="60%" height="350px"></center>

                        <p class="m-t-20">5. Setelah itu, Anda akan diarahkan ke halaman detail proses penambahan Saldo Uang saku yang sudah dilakukan. Anda juga dapat melihat di Keterangan Status. (Harap Pilih Pencarian Berdasarkan <b>"TOP UP"</b> )</p>
                        <center><img src="<?php echo base_url();?>aset/images/caratopup/topupdetail.png" width="60%" height="300px"></center>

                        <p class="m-t-20">6. Langkah Terakhir, tunggu beberapa saat setelah kami memverifikasi pembayaran Anda dan saldo Uang saku Anda akan bertambah dengan sendirinya.</p>
                    </div>
                </div>

            </div>            

        </div>
    </section>

</section>
