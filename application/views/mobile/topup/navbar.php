<?php    
    $iduser = $this->session->userdata('id_user');
    $ambilnama = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

    $getbalance = $this->db->query("SELECT active_balance FROM uangsaku WHERE id_user='$iduser'")->row_array();
    $hasil = number_format($getbalance['active_balance'], 0, ".", ".");
?>
<style type="text/css">
    /* scroller browser */
::-webkit-scrollbar {
    width: 9px;
}

/* Track */
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
    -webkit-border-radius: 7px;
    border-radius: 7px;
}

/* Handle */
::-webkit-scrollbar-thumb {
    -webkit-border-radius: 7px;
    border-radius: 7px;
    background: #a6a5a5;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
}
::-webkit-scrollbar-thumb:window-inactive {
    background: rgba(0,0,0,0.4); 
}
</style>
<ul class="header-inner clearfix m-l-10 m-r-10">
    <li id="menu-trigger" data-trigger="#sidebar" >
        <div class="line-wrap">
            <div class="line top"></div>
            <div class="line center"></div>
            <div class="line bottom"></div>
        </div>
    </li>

    <li>
        <a href="#" class="m-l-10"><img src="<?php echo "https://classmiles.com/"; ?>aset/img/logo/logoclass6.png" alt=""></a>
    </li> 

    <li class="pull-right hidden-xs">
        <ul class="top-menu">
            <!-- <li id="toggle-width">
                <div class="toggle-switch">
                    <input id="tw-switch" type="checkbox" hidden="hidden">
                    <label for="tw-switch" class="ts-helper"></label>
                </div>
            </li> -->
            
            <!-- <li id="top-search">
                <a href=""><i class="tm-icon zmdi zmdi-search"></i></a>
            </li> -->

            <li class="dropdown" id="notif" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;" id_user="<?php echo $this->session->userdata('id_user');?>">
                <a data-toggle="dropdown" href="" ng-init="getnotif()">
                    <i class="tm-icon zmdi zmdi-notifications"></i>
                    <i class='tmn-counts bara' ng-if="nes > '0'">{{nes}}</i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg pull-right" >
                    <div class="listview" id="notifications" >
                        <div class="lv-header">
                            <?php echo $this->lang->line('notification') ?>

                            <ul class="actions" data-toggle="tooltip" title="Read all" data-placement="right">
                                <li class="dropdown">   
                                    <a class="readsa" style="cursor: pointer;" id_user='<?php echo $this->session->userdata('id_user');?>'>
                                        <i class="zmdi zmdi-check-all" ></i>
                                    </a>                                                                  
                                </li>
                            </ul>
                        </div>                        
                        <div class="lv-body" id="scol" style="overflow-y:auto; height:350px; " when-scrolled="loadData()" style="cursor: pointer;">
                            <div ng-repeat='x in dataf' style="cursor: pointer;">
                                <a class="lv-item tif" ng-if="x.read_status == '1'" hrefa="{{x.link}}" notif_id="{{x.notif_id}}">
                                    <div class="media" data-toggle="tooltip" title="">
                                        <div class="pull-left">
                                            <img width="65px" height="65px" style="border-radius: 5px;" src="<?php echo ('https://classmiles.com/aset/img/user/'.$this->session->userdata('user_image')); ?>"> 
                                        </div>
                                        <div class="media-body" style="overflow:hidden; word-wrap: break-word;">
                                            <div style="font-size: 11px; color:black;">{{x.notif}}</div>                                            
                                        </div>
                                        <div class="media-footer">
                                            <small class="c-black" style="position:absolute; bottom:0px; margin-bottom: 1px; margin-top: auto;">{{x.interval_time}}</small>
                                        </div>
                                    </div>
                                </a>
                                <a class="lv-item tif" style="background: #ECF0F1;" ng-if="x.read_status == '0'" hrefa="{{x.link}}" notif_id="{{x.notif_id}}" >
                                    <div class="media">
                                        <div class="pull-left">
                                            <img width="65px" height="65px" src="<?php echo ('https://classmiles.com/aset/img/user/'.$this->session->userdata('user_image')); ?>"> 
                                        </div>
                                        <div class="media-body" style="overflow:hidden; word-wrap: break-word;">
                                            <div style="font-size: 11px; color:black;">{{x.notif}}</div>                                            
                                        </div>
                                        <div class="media-footer">
                                            <small class="c-black" style="position:absolute; bottom:0px; margin-bottom: 1px; margin-top: auto;">{{x.interval_time}}</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div ng-show="lodingnav">
                                <center><div class="preloader pl-lg">
                                    <svg class="pl-circular" viewBox="25 25 50 50">
                                        <circle class="plc-path" cx="50" cy="50" r="20" />
                                    </svg>
                                </div></center>
                            </div>  
                            <!-- <a class="lv-item" style="background: #ECF0F1;">
                                <div class="media" data-toggle="tooltip" data-placement="bottom" title="Please, complete your profile for tutor registration approval">
                                    <div class="pull-left">
                                        <img class="lv-img-sm" src="<?php echo base_url('aset/img/user/'.$this->session->userdata('user_image')); ?>">                                        
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">Administrator</div>
                                        <small class="lv-small">Please, complete your profile for tutor registration approval</small>
                                        <small class="c-black">34 Menit yang lalu</small>
                                    </div>                                    
                                </div>
                            </a> -->
                        </div>                     

                    </div>

                </div>
            </li>     

            <li class="dropdown" style="height: 35px;">
                <a data-toggle="dropdown" href="">
                     
                    <?php
                        $patokan =  $this->lang->line('profil');

                        if ($patokan == "Profile") {
                            ?>
                            <img class="m-t-5" width="35px" height="20px" style="border-radius:15%;" src="<?php echo ('https://classmiles.com/aset/img/language/flaginggris.png'); ?>"                
                            <?php
                        }
                        else if ($patokan == "Data Diri"){
                            ?>
                            <img class="m-t-5" width="35px" height="20px" style="border-radius:15%;" src="<?php echo ('https://classmiles.com/aset/img/language/flagindo.png'); ?>"                
                            <?php
                        }
                    ?>                           
                </a>
                <div class="dropdown-menu dropdown-menu-lg pull-right">
                    <div class="listview" id="notifications">

                        <div class="lv-header">
                            <?php echo $this->lang->line('chooselanguage'); ?>                                                            
                        </div>

                        <div class="lv-body" id="checklang">                            
                            <a class="lv-item" href="<?php echo base_url('set_lang/english'); ?>" id="btn_setenglish">
                                <div class="media">
                                    <div class="pull-left">
                                        <img width="35px" height="20px" style="border-radius:15%;" src="<?php echo "https://classmiles.com/"; ?>aset/img/language/flaginggris.png" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">English</div>                                                    
                                    </div>
                                </div>
                            </a>  
                            <a class="lv-item" href="<?php echo base_url('set_lang/indonesia'); ?>" id="btn_setindonesia">
                                <div class="media">
                                    <div class="pull-left">
                                        <img width="35px" height="20px" style="border-radius:15%;" src="<?php echo "https://classmiles.com/"; ?>aset/img/language/flagindo.png" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">Bahasa Indonesia</div>                                                    
                                    </div>
                                </div>
                            </a>         
                            <!-- <a class="lv-item" href="<?php echo base_url('set_lang/arab'); ?>">
                                <div class="media">
                                    <div disabled="true" class="pull-left">
                                        <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/language/saudi_arabia.png" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">Arabic</div>                                                    
                                    </div>
                                </div>
                            </a> -->                                      
                        </div>                                                
                    </div>

                </div>
            </li>

            <li class="dropdown hidden-xs">
                <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                <ul class="dropdown-menu dm-icon pull-right">
                   <!--  <li class="skin-switch hidden-xs">
                        <span class="ss-skin bgm-lightblue" data-skin="lightblue" id="lightblue"></span>
                        <span class="ss-skin bgm-bluegray" data-skin="bluegray" id="bluegray"></span>
                        <span class="ss-skin bgm-cyan" data-skin="cyan" id="cyan"></span>
                        <span class="ss-skin bgm-teal" data-skin="teal" id="teal"></span>
                        <span class="ss-skin bgm-orange" data-skin="orange" id="orange"></span>
                        <span class="ss-skin bgm-blue" data-skin="blue" id="blue"></span>
                    </li> -->
                    <li class="hidden-xs">
                        <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i><?php echo $this->lang->line('togglefullscreen'); ?></a>
                    </li>
                    <li class="divider hidden-xs"></li>
                    
                    <li>
                        <a onclick="signOut()" data-action="" href="<?php echo base_url('/logout'); ?>"><i class="zmdi zmdi-time-restore"></i><?php echo $this->lang->line('logout'); ?></a>
                    </li>
                </ul>
            </li>
           <!--  <li class="hidden-xs" id="chat-trigger" data-trigger="#chat">
                <a href=""><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>
            </li> -->
        </ul>
    </li>
</ul>

<!-- <div class="pull-left"> -->
    <nav class="ha-menu" style="background-color: #2090ea; width: 100%; padding: 0; margin: 0;">
        <ul>
            
            <li style="margin-left: 100px;" class="<?php if($sideactive=='daftartransaksi'){ echo 'active waves-effect';} else{ echo 'waves-effect'; } ?>"> <a href="<?php echo base_url('Epocket'); ?>"><?php echo $this->lang->line('menu1'); ?></a></li>
            <li class="<?php if($sideactive=='accountlist'){ echo 'active waves-effect';} else{ echo 'waves-effect'; } ?>"><a href="<?php echo base_url(); ?>Accountlist"><?php echo $this->lang->line('menu2'); ?></a></li>
            <li class="<?php if($sideactive=='topup'){ echo 'active waves-effect';} else{ echo 'waves-effect'; } ?>"><a href="<?php echo base_url(); ?>Topup"><?php echo $this->lang->line('menu3'); ?></a></li>
            <li class="<?php if($sideactive=='caratopup'){ echo 'active waves-effect';} else{ echo 'waves-effect'; } ?>"><a href="<?php echo base_url(); ?>Cara-Topup"><?php echo $this->lang->line('menu4'); ?></a></li>
            <li class="pull-right c-white f-16 p-r-10 p-t-10" style="margin-right: 100px;">
                <?php echo $ambilnama['user_name'];?>, <?php echo $this->lang->line('yourbalance'); ?> : Rp. <?php echo $hasil; ?>
            </li>
        </ul>        
    </nav>
<!-- </div>
<div class="pull-right">
    <label style="margin:12px; cursor: pointer; font-size: 17px; color:white; font-family: arial;"><?php echo $ambilnama['user_name'];?>, <?php echo $this->lang->line('yourbalance'); ?> : Rp. <?php echo $hasil; ?></label>
</div> -->

<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_setindonesia').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
        });
        $('#btn_setenglish').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
        });
    });
</script>