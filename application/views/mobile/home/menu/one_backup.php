<div id="carousel-header" class="carousel carousel-header slide" data-ride="carousel" data-interval="10000">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-header" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-header" data-slide-to="1"></li>
    <li data-target="#carousel-header" data-slide-to="2"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md7">
            <div class="carousel-caption">
              <h1 class="color-primary no-mt mb-4 animated zoomInDown animation-delay-7">Classroom in your hand</h1>
              <ul class="list-unstyled list-hero">
                <li>
                  <i class="animated flipInX animation-delay-6 color-warning zmdi zmdi-cloud"></i>
                  <span class="color-warning animated fadeInRightTiny animation-delay-7">High-speed servers and performance</span>
                </li>
                <li>
                  <i class="animated flipInX animation-delay-8 color-info zmdi zmdi-globe"></i>
                  <span class="color-info animated fadeInRightTiny animation-delay-9">Global web solutions &amp; cloud computing</span>
                </li>
                <li>
                  <i class="animated flipInX animation-delay-10 color-success zmdi zmdi-download"></i>
                  <span class="color-success animated fadeInRightTiny animation-delay-11">Lorem ipsum dolor sit amet consectetur</span>
                </li>
              </ul>
              <div class="text-center">
                <!-- <a href="#" class="btn btn-primary btn-xlg btn-raised animated flipInX animation-delay-16">
                  <i class="zmdi zmdi-settings"></i> Personalize</a>
                <a href="#" class="btn btn-warning btn-xlg btn-raised animated flipInX animation-delay-18">
                  <i class="zmdi zmdi-download"></i> Download</a> -->

                <a href="javascript:void(0)" class="btn btn-success btn-raised btn-app animated flipInX animation-delay-16">
                <div class="btn-container">
                  <i class="fa fa-android"></i>
                  <span>Aviable in </span>
                  <br>
                  <strong>Play Store</strong>
                </div>
              </a>
              <a href="javascript:void(0)" class="btn btn-danger btn-raised btn-app animated flipInX animation-delay-18">
                <div class="btn-container">
                  <i class="fa fa-apple"></i>
                  <span>Aviable in </span>
                  <br>
                  <strong>App Store</strong>
                </div>
              </a>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md5">
            <img src="<?php echo base_url(); ?>aset/landing_page/img/demo/mock.png" alt="..." class="img-responsive mt-6 center-block text-center animated zoomInDown animation-delay-5"> 
          </div>

        </div>
      </div>
    </div>
    <div class="item">
      <div class="container">
        <div class="row">
          <div class="col-md-7 pr-6">
              <h1 class="animated fadeInDown animation-delay-5 color-white">A single template, infinite possibilities</h1>
              <p class="lead animated fadeInDown animation-delay-5 color-white">We give you everything done, except the coffee.</p>
              <div class="card card-block card-warning card-dark-inverse animated fadeInLeft animation-delay-5">
                <div class="media">
                  <div class="media-left hidden-xs media-middle pr-4">
                    <i class="zmdi zmdi-account zmdi-hc-5x color-warning"></i>
                  </div>
                  <div class="media-body no-mt">
                    <h3 class="color-warning no-mt">Lorem ipsum dolor sit.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis voluptatibus atque minima provident obcaecati eaque.</p>
                  </div>
                </div>
              </div>
              <div class="card card-block card-success card-dark-inverse animated fadeInLeft animation-delay-7">
                <div class="media">
                  <div class="media-left hidden-xs media-middle pr-4">
                    <i class="zmdi zmdi-desktop-mac zmdi-hc-5x color-success"></i>
                  </div>
                  <div class="media-body no-mt">
                    <h3 class="color-success no-mt">Lorem ipsum dolor sit.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis voluptatibus atque minima provident obcaecati eaque.</p>
                  </div>
                </div>
              </div>
              <div class="text-center mt-4">
                <a href="javascript:void(0);" class="btn btn-xlg btn-white color-warning btn-raised animated fadeInLeft animation-delay-14 mr-2">
                  <i class="zmdi zmdi-settings"></i> Personalize</a>
                <a href="javascript:void(0);" class="btn btn-xlg btn-white color-success btn-raised animated fadeInRight animation-delay-14">
                  <i class="zmdi zmdi-download"></i> Download</a>
              </div>
            </div>
            <div class="col-md-5 text-center mt-6">
              <div class="img-phone-container">
                <img class="img-responsive animated zoomInDown animation-delay-3 index-1" src="<?php echo base_url(); ?>aset/landing_page/img/demo/pixel1.png">
                <img class="img-responsive img-phone-left" src="<?php echo base_url(); ?>aset/landing_page/img/demo/pixel3.png">
                <img class="img-responsive img-phone-right" src="<?php echo base_url(); ?>aset/landing_page/img/demo/pixel2.png"> </div>
            </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md7">
            <div class="carousel-caption">
              <h1 class="color-primary no-mt mb-4 animated zoomInDown animation-delay-7">At the Vanguard of Innovation</h1>
              <ul class="list-unstyled list-hero">
                <li>
                  <i class="animated flipInX animation-delay-8 color-info zmdi zmdi-nature"></i>
                  <span class="color-info animated fadeInRightTiny animation-delay-9">Global web solutions &amp; cloud computing</span>
                </li>
                <li>
                  <i class="animated flipInX animation-delay-6 color-danger zmdi zmdi-city-alt"></i>
                  <span class="color-danger animated fadeInRightTiny animation-delay-7">High-speed servers and performance</span>
                </li>
                <li>
                  <i class="animated flipInX animation-delay-10 color-warning zmdi zmdi-graduation-cap"></i>
                  <span class="color-warning animated fadeInRightTiny animation-delay-11">Lorem ipsum dolor sit amet consectetur</span>
                </li>
              </ul>
              <div class="text-center">
                <a href="#" class="btn btn-primary btn-xlg btn-raised animated flipInX animation-delay-16">
                  <i class="zmdi zmdi-settings"></i> Personalize</a>
                <a href="#" class="btn btn-warning btn-xlg btn-raised animated flipInX animation-delay-18">
                  <i class="zmdi zmdi-download"></i> Download</a>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md5">
            <img src="<?php echo base_url(); ?>aset/landing_page/img/demo/mock-imac.png" alt="..." class="img-responsive mt-6 center-block text-center animated zoomInDown animation-delay-5"> </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Controls -->
  <a href="#carousel-header" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
    <i class="zmdi zmdi-chevron-left"></i>
  </a>
  <a href="#carousel-header" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
    <i class="zmdi zmdi-chevron-right"></i>
  </a>
</div>