<html lang="en">
  <head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/preload.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/mobile.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/plugins.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/style.light-blue-500.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/width-boxed.min.css" id="ms-boxed" disabled="">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
  </head>
  <style type="text/css">
    body {
      background-color: white;
    }
    .slidee {
  		z-index: 1;
  		position: absolute;
  		/*width: 100%;*/
  		top: 0;
  		/*margin-left: -7%;*/
  		/*left: 0;*/
  		/*height: 100%;*/
  		transition: opacity 1s ease-in-out;
  		background-position: center center;
  		background-repeat: no-repeat;
  		background-size: cover;
  		/*opacity: 100*/
  	}
      .slider {
      max-width: 300px;
      height: 200px;
      margin: 20px auto;
      position: relative
    }
    .slide1,
    .slide2,
    .slide3,
    .slide4,
    .slide5 {
      position: absolute;
      background-repeat: no-repeat;
      height: 300px;
      width: 100%
    }
    .slide1 {
      background-size: 100% 350px;
      animation: fade 8s infinite;
      -webkit-animation: fade 8s infinite
    }
    .slide2 {
      background-size: 100% 350px;
      animation: fade2 8s infinite;
      -webkit-animation: fade2 8s infinite
    }
    .slide3 {
      background-size: 100% 350px;
      animation: fade3 8s infinite;
      -webkit-animation: fade3 8s infinite
    }
    @keyframes fade {
      0% {
        opacity: 1
      }
      33.333% {
        opacity: 0
      }
      66.666% {
        opacity: 0
      }
      100% {
        opacity: 1
      }
    }
    @keyframes fade2 {
      0% {
        opacity: 0
      }
      33.333% {
        opacity: 1
      }
      66.666% {
        opacity: 0
      }
      100% {
        opacity: 0
      }
    }
    @keyframes fade3 {
      0% {
        opacity: 0
      }
      33.333% {
        opacity: 0
      }
      66.666% {
        opacity: 1
      }
      100% {
        opacity: 0
      }
    }
    @media screen and (max-width: 1118px) {
      .tulisanbanner4 {
        font-size: 20px
      }
    }
    @media screen and (max-width: 1092px) {
      .tulisanbanner1 {
        font-size: 40px
      }
    }
    @media screen and (max-width: 1000px) {
      .banner {
        color: white;
        position: absolute;
        z-index: 1;
        right: 0;
        margin-right: 5.5%;
        margin-top: 13%;
        width: 35%;
        font-size: 15px
      }
      .bannerleft {
        color: white;
        position: absolute;
        z-index: 1;
        left: 0;
        margin-left: 5.5%;
        margin-top: 13%;
        width: 35%;
        font-size: 15px
      }
    }
    @media screen and (max-width: 640px) {
      .banner {
        color: white;
        position: absolute;
        z-index: 1;
        right: 0;
        margin-right: 5.5%;
        margin-top: 13%;
        width: 44%;
        font-size: 10px
      }
      .bannerleft {
        color: white;
        position: absolute;
        z-index: 1;
        left: 0;
        margin-left: 5.5%;
        margin-top: 13%;
        width: 35%;
        font-size: 10px
      }
    }
    .banner {
      color: white;
      position: absolute;
      z-index: 1;
      right: 0;
      margin-right: 5.5%;
      margin-top: 13%;
      width: 35%
    }
    .bannerleft {
      color: white;
      position: absolute;
      z-index: 1;
      left: 0;
      margin-left: 5.5%;
      margin-top: 13%;
      width: 35%
    }
    #kotak2aaa {
      margin-top: -90px
    }
    #namafitur {
      font-size: 1.7em;
      color: #333
    }
    #kotakkananheader {
      position: relative;
      padding-left: 3%;
      padding-right: 3%
    }
    @media screen and (min-width: 991px) {
      #kotak2nih {
        display: inline;
        margin-top: 20px;
        height: 480px;
        margin-left: -2px;
        background-image: url('<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>banner/Header_kanan.png')
      }
    }
    @media screen and (max-width: 1200px) {
      #namafitur {
        font-size: 1.5em;
        color: #333;
        margin-top: 12%
      }
    }
    @media screen and (max-width: 991px) {
      .kotakfitur {
        height: 200px
      }
      .header_atas {
        margin-top: 0px
      }
      #logoandroid_panjang {
        margin-left: 130px
      }
      #title_aplikasi {
        display: none
      }
      #title_aplikasi_android {
        display: none
      }
      #aplikasi_bawah {
        display: inline
      }
      #kotakdepan {
        margin-left: 90px
      }
      #namafitur {
        text-align: center;
        font-size: 1.3em;
        color: #333;
        margin-top: 12%
      }
      #kotakkananheader {
        font-size: 20px;
        position: relative;
        padding-left: 5%;
        margin-top: 2%;
        padding-right: 5%
      }
      #kotak2nih {
        background-image: none
      }
    }
    @media screen and (max-width: 770px) {
      #logo_android {
        margin-top: -9.5%;
        margin-right: 100px;
        padding: 0
      }
    }
    @media screen and (max-width: 700px) {
      #kotak2nih {
        margin: 0;
        margin-bottom: 14%;
        height: 500px;
        padding: 0;
        margin-left: -0.1%;
        margin-top: -12.5%
      }
      #namafitur {
        font-size: 1.2em;
        color: #333;
        margin-top: 14%
      }
      #kotak2aaa {
        margin-top: 1em
      }
      #kotakkananheader {
        display: hidden;
        position: relative;
        padding-left: 5%;
        margin-top: 4%;
        padding-right: 5%
      }
    }
    @media screen and (max-width: 562px) {
      #logo_android {
        margin-top: -15.5%;
        margin-right: 100px;
        padding: 0
      }
    }
    @media screen and (max-width: 384px) {
      #logo_android {
        margin-top: -30.5%;
        margin-right: 100px;
        padding: 0
      }
    }
    @media screen and (max-width: 320px) {
      .headerbanner {
        background-repeat: no-repeat;
        margin-top: 0px;
        height: 20px;
        width: 30%;
        background-size: 30% 30%;
        padding: 0
      }
    }
    @media screen and (orientation: portrait) {
    	#untuk_potrait{
    		display: block;
    	}
    	#slide_landscape{
    		display: none;
    	}
    	#slide_potrait{
    		display: block;
    	}
      #android_landscape{
        display: none;
      }
      #banner_landscape{
        display: none;
      }
      #banner_portrait{
        display: block;
      }
      #android_potrait{
        display: block;
      }
      #fitur_4landscape{
        display: none;
      }
      #fitur_4portrait{
        display: block;
      }
      #kotaklandscape{
        display: none;
      }
      #kotak2aaa{
        display: block;
      }
      .img_5fitur{
        margin-top: 3px;
        width: 130%; 
        height: auto;
      }
      .img_landscape {
        display: none
      }
      .img_portrait {
        display: block
      }
      .slide1 {
        background-size: 100% 350px;
        animation: fade 8s infinite;
        -webkit-animation: fade 8s infinite
      }
      .slide2 {
        background-size: 100% 350px;
        animation: fade2 8s infinite;
        -webkit-animation: fade2 8s infinite
      }
      .slide3 {
        background-size: 100% 350px;
        animation: fade3 8s infinite;
        -webkit-animation: fade3 8s infinite
      }
      .judul_potrait {
        display: inline
      }
      .judul_landscape {
        display: none
      }
      .fitur_potrait {
        display: inline
      }
      .fitur_landscape {
        display: hidden
      }
      .headerbanner {
        animation: changesback 6s infinite ease-in-out;
        background-repeat: no-repeat;
        margin-top: 0px;
        height: 260px;
        width: 100%;
        background-size: 100% 100%;
        padding: 0
      }
      .headerjudul {
        font-size: 1.0em;
        color: #333;
        font-weight: bold;
        text-align: center
      }
      .headerjudul_1 {
        display: none
      }
      #modal_welcome {
        margin-top: 30%
      }
      .comingsoonlogo {
        margin-top: -45%;
        margin-left: 75%;
        height: 80px;
        width: 80px
      }
    }
    @media screen and (orientation: landscape) {
    	#untuk_potrait{
    		display: none;
    	}
    	#slide_landscape{
    		display: block;
    	}
    	#slide_potrait{
    		display: none;
    	}
      #android_landscape{
        display: block;
      }
      #banner_landscape{
        display: block;
      }
      #banner_portrait{
        display: none;
      }
      #android_potrait{
        display: none;
      }
      #fitur_4landscape{
        display: block;
      }
      #fitur_4portrait{
        display: none;
      }
      #kotak2aaa{
        display: none;
      }
      #kotaklandscape{
        display: block;
      }
      .img_5fitur{
        margin-top: 3px;
        width: 100%; 
        height: auto;
      }
      .img_portrait {
        display: none
      }
      .slide1 {
        margin-top: -4%;
        background: url('<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/SMAUtuh.png')no-repeat center;
        background-size: 100% 250px;
        animation: fade 8s infinite;
        -webkit-animation: fade 8s infinite
      }
      .slide2 {
        margin-top: -4%;
        background: url('<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/Banner-Header-Tutor_Full_rev.png')no-repeat center;
        background-size: 100% 250px;
        animation: fade2 8s infinite;
        -webkit-animation: fade2 8s infinite
      }
      .slide3 {
        margin-top: -4%;
        background: url('<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/Banner-Header-Student_Full.png')no-repeat center;
        background-size: 100% 250px;
        animation: fade3 8s infinite;
        -webkit-animation: fade3 8s infinite
      }
      #modal_welcome {
        margin-top: 10%
      }
      .judul_potrait {
        display: none
      }
      .judul_landscape {
        display: inline
      }
      .judul {
        margin-top: -50%;
        font-size: 14px
      }
      .fitur_potrait {
        display: hidden
      }
      .fitur_landscape {
        display: inline
      }
      .headerbanner {
        background-repeat: no-repeat;
        margin-top: 0px;
        height: 240px;
        width: 100%;
        background-size: 100% 240px;
        padding: 3
      }
      .headerjudul {
        display: none
      }
      .headerjudul_1 {
        display: inline;
        margin-left: 320px;
        margin-top: 40px
      }
    }
    @media screen and (min-device-width: 357px) and (max-device-width: 667px) and (orientation: landscape) {
      .judul_potrait {
        display: none
      }
      .judul_landscape {
        display: inline
      }
      .judul {
        margin-top: -40%;
        font-size: 14px
      }
      .fitur_potrait {
        display: hidden
      }
      .fitur_landscape {
        display: inline
      }
      .headerbanner {
        background-repeat: no-repeat;
        margin-top: 0px;
        height: 240px;
        width: 100%;
        background-size: 100% 240px;
        padding: 3
      }
      .headerjudul {
        display: none
      }
      .headerjudul_1 {
        display: inline;
        margin-left: 320px;
        margin-top: 40px
      }
    }
    @media screen and (min-device-width: 320px) and (max-device-width: 568px) and (orientation: landscape) {
      .judul_potrait {
        display: none
      }
      .judul_landscape {
        display: inline
      }
      .judul {
        font-size: 14px
      }
      .fitur_potrait {
        display: hidden
      }
      .fitur_landscape {
        display: inline
      }
      .headerbanner {
        background-repeat: no-repeat;
        margin-top: 0px;
        height: 240px;
        width: 100%;
        background-size: 100% 240px;
        padding: 3
      }
      .headerjudul {
        display: none
      }
      .headerjudul_1 {
        display: inline;
        margin-left: 320px;
        margin-top: 40px
      }
    }
    @media screen and (min-device-width: 412px) and (max-device-width: 732px) and (orientation: landscape) {
      .judul_potrait {
        display: none
      }
      .judul_landscape {
        display: inline
      }
      .judul {
        font-size: 14px
      }
      .fitur_potrait {
        display: hidden
      }
      .fitur_landscape {
        display: inline
      }
      .headerbanner {
        background-repeat: no-repeat;
        margin-top: 0px;
        height: 240px;
        width: 100%;
        background-size: 100% 240px;
        padding: 3
      }
      .headerjudul {
        display: none
      }
      .headerjudul_1 {
        display: inline;
        margin-left: 320px;
        margin-top: 40px
      }
    }
    @media screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: landscape) {
      .judul_potrait {
        display: none
      }
      .judul_landscape {
        display: inline
      }
      .judul {
        margin-left: 900%;
        font-size: 26px
      }
      .fitur_potrait {
        display: hidden
      }
      .fitur_landscape {
        display: inline
      }
      .headerbanner {
        background-repeat: no-repeat;
        margin-top: 0px;
        height: 340px;
        width: 100%;
        background-size: 100% 340px;
        padding: 3
      }
      .headerjudul {
        display: none
      }
      .headerjudul_1 {
        display: inline;
        margin-left: 320px;
        margin-top: 40px
      }
    }
    #slide3 {
      background-image: url('<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/Banner-Header-Tutor_Full.png')
    }
  </style>

  <style type="text/css">
      .product {
       /* margin: 50px auto;
        width: 280px;
        height: 370px;*/
        background: white;
        /*border: 1px solid #333;*/
        position: relative;
        z-index: 90;
      }
      .ribbon-wrapper {
        /*background-color: red;*/
        width: 140px;
        height: 140px;
        overflow: hidden;
        position: absolute;
        top: 0px;
        left: -3px;
      }
      .ribbon-wrapper-right {
        /*background-color: red;*/
        width: 140px;
        height: 140px;
        overflow: hidden;
        position: absolute;
        top: 6px;
        left: -13px;
      }
        .ribbon {
          font: bold 15px sans-serif;
          color: #333;
          text-align: center;
          -webkit-transform: rotate(-45deg);
          -moz-transform:    rotate(-45deg);
          -ms-transform:     rotate(-45deg);
          -o-transform:      rotate(-45deg);
          position: relative;
          padding: 7px 0;
          top: 35px;
          left: -40px;
          width: 200px;
          background-color: #ebb134;
          color: #fff;
        }
        .ribbon-right {
          font: bold 15px sans-serif;
          color: #008080;
          text-align: center;
          position: relative;
          padding: 7px 0;
          top: 10px;
          left: -4px;
          width: 150px;
          background-color: #008080;
          color: #fff;
        }
      }

      .paper{
              display: inline-block;
              position: relative;
              overflow: hidden;
              padding: 15px 20px;
              background-color: #f3cc0c;
              -webkit-transition: 0.25s ease-in-out all;
              -moz-transition: 0.15s ease-in-out all;
              -o-transition: 0.15s ease-in-out all;
              transition: 0.15s ease-in-out all;
          }
          .paper:hover {
              -moz-border-radius: 0 0 20px 0;
              -webkit-border-radius: 0 0 20px 0;
              border-radius: 0 0 20px 0;
          }
          .paper:after {
              content: '';
              width: 0;
              height: 0;
              position: absolute;
              bottom: 0;
              right: 0;
              z-index: 30;
              -moz-box-shadow: -5px 2px 5px rgba( 0, 0, 0, 0.3 );
              -webkit-box-shadow: -5px 2px 5px rgba( 0, 0, 0, 0.3 );
              box-shadow: -5px 2px 5px rgba( 0, 0, 0, 0.3 );
              -webkit-transition: 0.25s ease-in-out all;
              -moz-transition: 0.15s ease-in-out all;
              -o-transition: 0.15s ease-in-out all;
              transition: 0.15s ease-in-out all;
          }
          .paper:hover:after{
              width: 15px;
              height: 15px;
              -webkit-transform: rotate(-10deg) translate3d( 0, 0, 0 );
              -moz-transform: rotate(-10deg) translate3d( 0, 0, 0 );
              -o-transform: rotate(-10deg) translate3d( 0, 0, 0 );
              transform: rotate(-10deg) translate3d( 0, 0, 0 );
          }
      #fpc_effect-back {
        background-color: #008080; /* some background color to match corner inside's */
        width: 100%;/* trivial */
        font: 12pt arial,sans-serif,helvetica,verdana;/* trivial */ 
        color: #666;//trivial
      }
      #fpc_effect-back * {
        box-sizing: border-box;
      }
      #fpc_box {
        /*width: 500px;any relative or absolute*/
        position: relative;
        background-color: #FFF;
      }
      #fpc_content {
        padding: 20px;
      }
      #fpc_content:before {
        content:"";
        /*width: 80px;*/
        /*height: 80px;*/
        float: right;
      }
      #fpc_page-tip:before, #fpc_page-tip:after {
        background-color: #FFF;
        position: absolute;
        display: block;
        z-index: 2;
        border-top-right-radius: 60%;
        width: 50%;
        height: 50%;
        content: "";
      }
      #fpc_page-tip:before {
        right: 100%;
        top: 0%;
        background: -webkit-radial-gradient(-180% 200%, circle, rgba(255,255,255,0) 85%, rgba(0,0,0,.1) 93%);
      }
      #fpc_box:hover #fpc_page-tip:before {
        border-right: solid 1px #fff;
      }
      #fpc_box div#fpc_corner-box:hover #fpc_page-tip:before {
        border-right: solid 2px #fff;
      }
      #fpc_page-tip:after {
        top: 100%;
        right: 0%;
        background: -webkit-radial-gradient(-250% 320%, circle, rgba(255,255,255,0) 85%, rgba(0,0,0,.10) 93%);
      }
      #fpc_box:hover #fpc_page-tip:after {
        border-top: solid 1px #fff;
      }
      #fpc_box div#fpc_corner-box:hover #fpc_page-tip:after {
        border-top: solid 2px #fff;
      }
      #fpc_corner-box {  /* edit these sizes for the default revealing corner size */
        height: 20px;
        width: 20px;
        right: 0;
        top: 0;
        position: absolute;
        overflow: visible;
      }
      #fpc_box:hover #fpc_corner-box { /* edit corner size (First animation, when the whole page is rollovered) */
        height: 50px;
        width: 50px;
      }
      #fpc_box div#fpc_corner-box:hover { /* edit corner size (Second animation, when the corner itself is rollovered) */
        height: 100px;
        width: 100px;
      }
      #fpc_corner-box:before {
        position: absolute;
        top: 0;
        right: 0;
        content: "";
        display: block;
        width: 133%;
        height: 133%;
      }
      #fpc_corner-contents:after {
        position: absolute;
        top: 0;
        right: 0;
        content: "";
        background:  -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0) 37%, #DDD 62%, rgba(230, 230, 230, 0.1) 64%, rgba(255, 255, 255, 0) 67%), -webkit-radial-gradient(-50% 150%, circle, transparent 74%, rgba(0, 0, 0, 0.2) 74%, transparent 81%);
        display: block;
        width: 133%;
        height: 133%;
      }
      #fpc_page-tip {
        position: absolute;
        top: 0;
        right: 0;
        content: "";
        background: -webkit-linear-gradient(45deg, #ddd 17%, #dfdfdf 18%, #f5f5f5 30%, #f8f8f8 34%, #eee 39%, rgba(200,200,200,0) 41%);
        display: block;
        width: 100%;
        height: 100%;
      }
      #fpc_corner-button {
        -webkit-transform: rotate(45deg);
          -moz-transform:    rotate(45deg);
          -ms-transform:     rotate(45deg);
          -o-transform:      rotate(45deg);
        position: absolute;
        width: 7em;
        top: 0;
        right: 0;
        background-color: #008080;
        /*background-color: red;*/
        color: #fff;
        font-family: Verdana, Geneva, sans-serif;
        text-align: center;
        padding: 8px 5px;
        border-radius: 5px;
        display: inline-block;
        font-size: 11px;
      }
      #fpc_corner-contents {
        width: 125%;
        position: absolute;
        display: block;
        overflow: hidden;
        -webkit-mask: -webkit-linear-gradient(45deg, transparent 49%, #000 53%);
        top: 0;
        right: 0;
        height: 125%;
      }
      #fpc_corner-contents:before {
        content: "";
        position: absolute;
        top: 0;
        right: 0;
        content: "";
        display: block;
        width: 100%;
        height: 100%;
        background-color: #008080; /* Match this background color to #fpc_effect-back */
      }
      #fpc_corner-box, #fpc_corner-contents, #fpc_page-tip {
        -webkit-transition-property: all;
        -webkit-transition-duration: .3s;
        -webkit-transition-timing-function: cubic-bezier(0, 0.35, .5, 1.7);
      }
      #fpc_corner-button strong {
        font-size: 13px;
        font-weight: bold;
        display: block;
      }
    </style>
  <style type="text/css">

    .animasiku {
      -webkit-animation-duration: 1s;
      /*-webkit-animation-delay: 3s;*/
      /*animation-delay: 2s;*/
      animation-duration: 1s infinite linear;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
      -webkit-animation-iteration-count: infinite;
      animation-iteration-count: infinite;
    }
    .animasi {
      -webkit-animation-duration: 1s;
      /*-webkit-animation-delay: 3s;*/
      /*animation-delay: 2s;*/
      animation-duration: 1s infinite linear;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
    }

    .animasi:hover {
      -webkit-animation-iteration-count: infinite;
      animation-iteration-count: infinite;
      color: #080800;
    }


   

    @-webkit-keyframes tada {
      0% {
        -webkit-transform: scale3d(1, 1, 1);
        transform: scale3d(1, 1, 1);
      }

      10%, 20% {
        -webkit-transform: scale3d(.9, .9, .9) rotate3d(0, 0, 1, -3deg);
        transform: scale3d(.9, .9, .9) rotate3d(0, 0, 1, -3deg);
      }

      30%, 50%, 70%, 90% {
        -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
        transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
      }

      40%, 60%, 80% {
        -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
        transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
      }

      100% {
        -webkit-transform: scale3d(1, 1, 1);
        transform: scale3d(1, 1, 1);
      }
    }

    @keyframes tada {
      0% {
        -webkit-transform: scale3d(1, 1, 1);
        -ms-transform: scale3d(1, 1, 1);
        transform: scale3d(1, 1, 1);
      }

      10%, 20% {
        -webkit-transform: scale3d(.9, .9, .9) rotate3d(0, 0, 1, -3deg);
        -ms-transform: scale3d(.9, .9, .9) rotate3d(0, 0, 1, -3deg);
        transform: scale3d(.9, .9, .9) rotate3d(0, 0, 1, -3deg);
      }

      30%, 50%, 70%, 90% {
        -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
        -ms-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
        transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
      }

      40%, 60%, 80% {
        -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
        -ms-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
        transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
      }

      100% {
        -webkit-transform: scale3d(1, 1, 1);
        -ms-transform: scale3d(1, 1, 1);
        transform: scale3d(1, 1, 1);
      }
    }

    .tada {
      -webkit-animation-name: tada;
      animation-name: tada;
    }
  </style>
  <header>                       
  	<div class="headerbanner">
  		<div id="slide_potrait">
  			<div>
  	     <img class="slidee" style="width: 100%; z-index: -5;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/SMA_Mobile.png">
  	   	</div>
  	   	<div>
  	     <img class="slidee" style="width: 100%; z-index: -5;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/Tutor_Mobile.png">
  	   	</div>
  	   	<div>
  	      <img class="slidee" style="width: 100%; z-index: -5;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/update_alyssa.png">
  	   	</div>
  	   	<!-- <div style=>
  	    <a href="<?php echo base_url();?>/class?c=8f7e4a3f01329737b60204e9baa53bc2" target="_blank" ><img class="slidee" style="width: 100%; z-index: -5;padding: 0; margin: 0;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>banner/IklanBannerUNY.png"></a>
  	   	</div> -->
     </div>
      <div id="slide_landscape">
  			<div>
  	     <img class="slidee" style="width: 100%; z-index: -5;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/SMAUtuh.png">
  	   	</div>
  	   	<div>
  	     <img class="slidee" style="width: 100%; z-index: -5;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/Banner-Header-Tutor_Full.png">
  	   	</div>
  	   	<div>
  	    <img class="slidee" style="width: 100%; z-index: -5;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>banner/banner_atas_full.png">
  	   	</div>
  	   	<!-- <div style=>
  	    <img class="slidee" style="width: 53%; z-index: -5;padding: 0; margin: 0;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>banner/IklanBannerUNY.png">
  	    <img class="slidee" style="width: 100%; z-index: -6;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>banner/banner_atas_full.png">
  	   	</div> -->
      </div>
  	</div>
  	<div style="display: none;">
        <div class='slide1'>
          <img id="img_header_portrait_1" class="img_portrait"  src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/SMA_Mobile.png" style="width: 100%;">
        </div>
        <div class='slide2'>
          <img id="img_header_portrait_2" class="img_portrait" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/Tutor_Mobile.png" style="width: 100%;">
        </div>
        <div class='slide3'>
          <img id="img_header_portrait_3" class="img_portrait" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/update_alyssa.png" style="width: 100%;">
        </div>
    </div>
    </div>
    <div class="container" id="kotakdepan" style="">     
      <div class="row" style="padding: 0; position: absolute; top: 0;">
        <div class="col-md-12" style=""></div>
        <div  class="col-md-12 zoomInUp judul_landscape"  style="">
            <br><br>
            <div class="lead animated fadeInUp animation-delay-7" id="banner_landscape" style="height: 60px; display: none;">
                <!-- <p class="lead animated fadeInUp animation-delay-7 judul"  style="margin-left: 45%; margin-right: 5%; padding: 0;"><?php echo $this->lang->line('banneratas1')?>
                </p>  -->
                <h4 class="text_judul" id="" style="margin-left: 45%; margin-right: 5%; padding: 0;">
    		        <?php echo $this->lang->line('banneratas1')?>
      		      </h4>
      		      <!-- <h4 class="text_judul" id="" style="margin-left: 45%; margin-right: 5%; padding: 0;">
      		        <?php echo $this->lang->line('banner2')?>
      		      </h4> -->
      		      <h4 class="text_judul" id="" style="margin-left: 45%; margin-right: 5%; padding: 0;">
      		        <?php echo $this->lang->line('banner3')?>
      		      </h4>
      		      <!-- <h4 class="text_judul" id="" style="margin-left: 45%; margin-right: 5%; padding: 0;">
      		        <?php echo $this->lang->line('banner4')?>
      		      </h4>
      		      <h4 class="text_judul" id="" style="margin-left: 45%; margin-right: 5%; padding: 0;">
      		        <?php echo $this->lang->line('banner5')?>
      		      </h4> -->
      		      <h4 class="text_judul" id="" style="margin-left: 45%; margin-right: 5%; padding: 0;">
      		        <?php echo $this->lang->line('banner6')?>
      		      </h4> 
            </div>
            <div id="android_landscape">
              <p class=" wow animated fadeInUp animation-delay-7" style="margin-left: 45%; font-size: 10px; color: #333333; font-weight: bold;" ><?php echo $this->lang->line('banneratas2')?></p>
                    <a target="_blank" href="https://play.google.com/store/apps/details?id=com.apps.indiclass" class="wow animated fadeInUp animation-delay-7">                
                    <img style="margin-left: 45%; margin-top: -5%; width: 15%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL ;?>logo/android-02.png"></a>
            </div>     
            <!--<img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/HeaderFull_rev.png" style="min-height: 500px; width: 100%; padding: 0;">!-->
        </div>
        <div class="col-md-12 header_atas"></div>
        <div class="col-md-6 kotak" hidden="true">
            <div>
                <p class="lead animated fadeInUp animation-delay-7"  style="margin-top: 20%; margin-left: 25px; padding: 0;"><?php echo $this->lang->line('banneratas1')?>
                </p>
                <?php if($this->session->flashdata('mes_alert')){ ?>
                <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                    <?php echo $this->session->flashdata('mes_message'); ?>
                </div>
                <?php }?><br><br><br><br>
            </div>
            <div id="logo_android" style="margin-left: 40px;">
                <p class=" wow animated fadeInUp animation-delay-7" style=" color: #333333; font-weight: bold;" ><?php echo $this->lang->line('banneratas2')?></p>
                <a target="_blank" href="https://play.google.com/store/apps/details?id=com.apps.indiclass" class="wow animated fadeInUp animation-delay-7">                
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL ;?>logo/android-02.png"></a>                
                <!--</a>
                <a href="#" class="wow animated fadeInUp animation-delay-7">                
                  <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL ;?>logo/iphone-02.png">                
                </a>-->
            </div>
        </div>
      </div>
    </div>
  </header>
  <nav class="navbar ms-lead-navbar navbar-mode" id="nav3"  style="z-index: 2; background-color: #FF00FF;; padding: 0;">
    <a class="navbar-brand" data-scroll href="" style="width: 40%;">
          <!-- <img src="assets/img/demo/logo-navbar.png" alt=""> -->
          <img style="margin-top: -2px; width: 100%" src="https://indiclass.id/aset/img/indiLogo2.png" alt="" >
    </a>
    <ul class="nav navbar-nav">             
        <li>
          <a id="nav_login" href="#" class="btn-circle btn-circle-success"><i class="zmdi zmdi-sign-in"></i></a>
          <a href="<?php echo base_url(); ?>Register" class="btn-circle btn-circle-primary"><i class="zmdi zmdi-account-add"></i></a>
        </li>               
    </ul>
  </nav>
  <div class="modal modal-success"  id="saran_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div class="modal-header shadow-2dp no-pb" style="background-color: #db1f00;">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">
                 <i class="zmdi zmdi-close"></i>
                 </span>
                 </button>
                 <div class="modal-title text-center">
                     <h2 class="no-m ms-site-title"><?php echo $this->lang->line('wlogin'); ?><b>&nbsp;Indiclass</b></h2>
                 </div>
              </div>
              <div class="modal-body" style="height: 250px;">
                  <div class="" style="text-align: center;" id="saran_login">
                    <p class="text-center">Untuk kinerja yang maksimal, kami sarankan anda untuk login menggunakan Aplikasi Indiclass</p>
                        <div class="col-md-6">
                          <button id="btn_login_saran_modal" class="btn btn-raised btn-success" data-dismiss="modal" aria-label="Close" style="background-color: #008080;">Tetap Login</button>
                          <button class="btn btn-raised btn-success"><a style="color: white;" href="intent://student/#Intent;scheme=classmiles;package=com.classmiles.student;end"> Ke Aplikasi </a></button>
                        </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
      <!-- Modal Login -->  
  <div>
    <div class="modal modal-success" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div class="modal-header shadow-2dp no-pb" style="background-color: #db1f00;">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">
                 <i class="zmdi zmdi-close"></i>
                 </span>
                 </button>
                 <div class="modal-title text-center">
                     <h2 class="no-m ms-site-title"><?php echo $this->lang->line('wlogin'); ?><b>&nbsp;Indiclass</b></h2>
                 </div>
              </div>
              <div class="modal-body">
                  <div hidden class="col-sm-6" style="text-align: center; background-color: #008080;" id="saran_login">
                    <p class="text-center">Untuk kinerja yang maksimal, kami sarankan anda untuk login menggunakan Aplikasi Indiclass</p>
                      <div  class="">
                        <a href="" class="btn btn-raised btn-success" style="width: 200px;">Tetap Login</a>
                      </div>
                        <!-- <div class="">
                        <a href="https://play.google.com/store/apps/details?id=com.classmiles.student" class="btn btn-raised btn-info" style="width: 200px;">Unduh Aplikasi</a>
                      </div> -->
                  </div>
                  <div  class="tab-content" id="menu_login">
                    <div  role="tabpanel" class="tab-pane fade active in" id="ms-login-tab">
                      <!-- <form role="form" action="<?php echo base_url('Master/aksi_login'); ?>" method="post"> -->
                          <input type="text" name="kode" id="kode" hidden>
                          <?php if($this->session->flashdata('mes_alert')){ ?>
                          <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->session->flashdata('mes_message'); ?>
                              </div>
                              <?php }?>
                              <div class="alert alert-danger" style="display: none; ?>" id="alertwrong">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('wrongpassword'); ?>
                              </div>
                              <div class="alert alert-danger" style="display: none; ?>" id="alertemailwrong">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  Email Facebook anda tidak mengizinkan!!!
                              </div>
                              <div class="alert alert-danger" style="display: none; ?>" id="alertfailedregister">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('failed');?> Mendaftar 
                              </div>
                              <div class="alert alert-success" style="display: none; ?>" id="alertverifikasisuccess">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('activationsuccess');?> 
                              </div>
                              <div class="alert alert-success" style="display: none; ?>" id="alertverifikasisuccesstutor">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('activationsuccess').' '.$this->lang->line('activationtutor');?> 
                              </div>
                              <div class="alert alert-danger" style="display: none; ?>" id="alertverifikasifailed">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('alreadyactivation');?> 
                              </div>
                              <div class="alert alert-warning" style="display: none; ?>" id="alertverifikasiemail">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php 
                                      $load = $this->lang->line('youremail3');
                                      $load2 = $this->lang->line('here');
                                      $load3 = $this->lang->line('emailresend');
                                      echo $load."<a href=".base_url('/Master/resendEmaill')."> <label style='color:#008080; cursor:pointer;'>".$load2."</label></a> ".$load3;
                                  ?> 
                              </div>
                              <div class="alert alert-success" style="display: none; ?>" id="alertsuccessendemail">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('successfullysent');?>
                              </div>
                              <div class="alert alert-danger" style="display: none; ?>" id="alertfailedsendemail">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('failed');?>
                              </div>
                              <div class="alert alert-warning" style="display: none; ?>" id="alertverifikasitutor">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('youremail2');?>
                              </div>

                              <div class="alert alert-danger" style="display: none; ?>" id="alertfailedchangepassword">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('failedchange');?>
                              </div>
                              <div class="alert alert-warning" style="display: none; ?>" id="alertsuccesschangepassword">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('successfullypassword');?>
                              </div>
                              <div class="alert alert-warning" style="display: none; ?>" id="alertloginfirstpackage">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('loginfirst');?>
                              </div>
                          <div id="kotakalert"></div>
                          <fieldset>      
                            <div class="form-group label-floating">
                               <div class="input-group">
                                   <span class="input-group-addon">
                                   <i class="zmdi zmdi-account"></i>
                                   </span>
                                   <label class="control-label" for="ms-form-user">Email/Kid's Username</label>
                                   <input type="text" name="email" id="emaill" required class="input-sm form-control fg-input"> 
                               </div>
                            </div>
                            <div class="form-group label-floating">
                                  <div class="input-group">
                                     <span class="input-group-addon">
                                     <i class="zmdi zmdi-lock"></i>
                                     </span>
                                     <label class="control-label" for="ms-form-pass"><?php echo $this->lang->line('password'); ?>
                                     </label>
                                     <input type="password" id="ms-form-pass"  required name="kata_sandi" class="input-sm form-control fg-input" >
                                 </div>
                            </div>
                            <input type="text" name="user_utc" id="user_utc" hidden>
                            <div class="row mt-2">
                                  <div class="col-xs-12">
                                     <div class="form-group no-mt">
                                          <div class="checkbox">
                                             <label style="color:#009688;">
                                             <input type="checkbox" value="V" name="bapuks" id="bapuks">
                                             <i class="input-helper"></i>
                                             <?php echo $this->lang->line('remember'); ?> 
                                             </label>                            
                                          </div>
                                     </div>
                                 </div>
                                 <div class="col-xs-12">
                                     <button  type="submit" id="loginnih" class="btn btn-raised btn-primary pull-right"><?php echo $this->lang->line('login'); ?>   
                                     </button>
                                 </div>
                                 <center>
                                     <label class="fg-label"><?php echo $this->lang->line('notaccountlogin'); ?></label>
                                     <a href="<?php echo base_url(); ?>Register"> <?php echo $this->lang->line('here'); ?></a>
                                 </center>
                                 <center>
                                     <label style="color:#ff9000;">
                                     <a href="#" data-toggle="modal"  data-target ="#forgot_modal"  data-dismiss="modal" ><?php echo $this->lang->line('lupasandi'); ?></a>
                                      </label>
                                 </center>
                            </div>
                            <div class="text-center">
                                 <a class="wave-effect-light btn btn-raised btn-facebook " id="f_signInbtn">
                                     <i class="zmdi zmdi-facebook" ></i><?php echo $this->lang->line('withfacebook'); ?>
                                 </a>
                                 <a class="wave-effect-light btn btn-raised btn-google" id="customBtn" >
                                     <i class="zmdi zmdi-google"></i><?php echo $this->lang->line('withgoogle'); ?>
                                 </a>
                                 <!-- <div class="" >Login</div> -->
                            </div>
                          </fieldset>
                      <!-- </form> -->
                      <form hidden method="POST" id='google_form' action="<?php echo base_url();?>master/google_login">
                           <!-- <input type="text" name="email" id="google_email"> -->
                          <textarea name="google_data" id="google_data" style="width: 900px;"></textarea>
                          <input type="text" name="usertype_id" value="student">
                      </form>
                      <form hidden method="POST" action='<?php echo base_url(); ?>master/facebook_loginn' id='facebook_form'>
                          <!-- <input type="text" name="email" id="google_email"> -->
                          <textarea name="fb_data" id="fb_data"></textarea>
                          <input type="text" name="usertype_id" value="student">
                      </form> 
                    </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  <div class="modal modal-dark" id="desc_package_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <input type="text" name="id_paket" id="txt_id_paket" hidden>
              <div class="modal-header shadow-2dp no-pb" style="background-color: #008080;">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                 <span aria-hidden="true">
                 <i class="zmdi zmdi-close"></i>
                 </span>
                 </button>
                 <div class="modal-title text-center">
                     <h2 class="no-m ms-site-title" ><b>Paket Indiclass</b></h2>
                 </div>
              </div>
              <div class="modal-body" style="color: black">
                <span class="ms-tag ms-tag-success" id="txt_price" style="display: none; margin-top: 5px; margin-left: 5px; position: absolute;"></span>  
              <img id="txt_poster" src="" style="width: 100%;">
              <h4 class="color-success"><b  id="txt_name"></b></h4>
        <label class="" style="text-align: justify; color: black; margin-top: 2px; font-size: 11px" id="txt_desc"></label>
                  <div class="col-xs-6" style="font-size: 10px; padding: 0;">Bidang :
                    <ul id="list_bidang"></ul>
                  </div>
                  <div class="col-xs-6" style="font-size: 10px; padding: 0;">Fasilitas :
                    <ul id="list_fasilitas" ></ul>
                  </div>
             </div>
              <div class="modal-footer" align="cek_aktif">
                <div class="col-xs-12" align="center">
                  <button type="button" class="btn_choose_packagee btn btn-raised btn-success" data-list_id=""><i class="zmdi zmdi-shopping-cart"></i>Beli Paket</button>
                </div>
            </div>
         </div>
     </div>
  </div>
        <!-- Modal Lupa Password -->  
  <div class="modal  modal-success" id="forgot_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
    <div class="modal-dialog modal-sm animated zoomIn animated-3x" role="document">
        <div class="modal-content">
            <div class="modal-header" style="text-align: center; background-color: #008080;">
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target ="#login_modal" aria-label="Close"><span aria-hidden="true">
                  <i class="zmdi zmdi-close"></i></span>
                </button>
                <h3 class="modal-title" id="myModalLabel3"><?php echo $this->lang->line('lupasandi'); ?></h3>
            </div>
            <div class="modal-body">
                <form role="form" action="<?php echo base_url('master/sendLink'); ?>" method="post">
                    <div class="lv-body">
                        <div class="lv-item">                         
                            <div style="text-align: center;" class="lv-title"><?php echo $this->lang->line('title1'); ?><?php echo $this->lang->line('title2'); ?><?php echo $this->lang->line('title3'); ?>
                            </div>
                        </div>
                        <div class="form-group label-floating">
                          <div class="input-group">
                              <span class="input-group-addon">
                                <i class="zmdi zmdi-email"></i>
                              </span>
                              <label class="control-label" for="ms-form-user">Email</label>
                              <input type="text" name="forgotemail" required class="input-sm form-control fg-input"> 
                          </div>
                        </div>
                        <div class="modal-footer">                   
                          <button class="btn btn-raised btn-success pull-right" name="sndmail" id="sndmail"><?php echo $this->lang->line('sendemail'); ?></button>
                        </div>
                    </div>    
                </form> 
            </div>
        </div>
    </div>
  </div>
  <div id="untuk_potrait">
  	<div id="banner_portrait" class="col-xs-12 zoomInUp judul_potrait" hidden="true" style="display: none; height: 80px; padding-top: 0; margin-top: -3%">
  	      <h4 class="text_judul" id="" style="font-size: 16px; text-align: center; padding: 0;">
  	        <?php echo $this->lang->line('banneratas1')?>
  	      </h4>
  	     <!--  <h4 class="text_judul" id="" style="font-size: 16px; text-align: center; padding: 0;">
  	        <?php echo $this->lang->line('banner2')?>
  	      </h4> -->
  	      <h4 class="text_judul" id="" style="font-size: 16px; text-align: center; padding: 0;">
  	        <?php echo $this->lang->line('banner3')?>
  	      </h4>
  	      <!-- <h4 class="text_judul" id="" style="font-size: 16px; text-align: center; padding: 0;">
  	        <?php echo $this->lang->line('banner4')?>
  	      </h4>
  	      <h4 class="text_judul" id="" style="font-size: 16px; text-align: center; padding: 0;">
  	        <?php echo $this->lang->line('banner5')?>
  	      </h4> -->
  	      <h4 class="text_judul" id="" style="font-size: 16px; text-align: center; padding: 0;">
  	        <?php echo $this->lang->line('banner6')?>
  	      </h4>
  	</div>
  	<div id="android_potrait" style="">
      <CENTER>
  	    <p class=" wow animated fadeInUp" style="color: #333333; font-weight: bold;" ><?php echo $this->lang->line('banneratas2')?></p>
        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.apps.indiclass" class="wow animated fadeInUp animation-delay-7">                
        <img style="" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL ;?>logo/android-02.png"></a>               
      </CENTER>
    </div>
  </div>
  <div class="container" id="fitur__classmiles">
    <div class="tiga_langkah" style="margin-top: 2%;">
      <center>
        <div class="animated zoomInLeft animation-delay-2">
          <h3 style="color:#008080 "><b><?php echo $this->lang->line('tiga_langkah')?> :</b></h3>  
        </div>
        <div class="">
            <div class="col-xs-4 animated zoomInLeft animation-delay-2">
                <div class="">
                    <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/select_tutor.png" style="height: 50px; width: 50px;" alt="" class="">
                </div>
                <div style="font-size: 12px; padding-top: 10px; color: #008080" class="">
                  <p><b><?php echo $this->lang->line('pilih_tutor')?></b></p>
                </div>
            </div>
            <div class="col-xs-4 animated zoomInLeft animation-delay-4">
                <div class="">
                    <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/select_date-time.png" style="height: 50px; width: 50px;" alt="" class="">
                </div>
                <div style="font-size: 12px; padding-top: 10px; color: #008080" class="">
                  <p><b><?php echo $this->lang->line('atur_waktu')?></b></p>
                </div>
            </div>
            <div class="col-xs-4 animated zoomInLeft animation-delay-6">
                <div class="">
                    <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/enter_class.png" style="height: 50px; width: 50px;" alt="" class="">
                </div>
                <div style="font-size: 12px; padding-top: 10px; color: #008080" class="">
                  <p><b><?php echo $this->lang->line('masuk_kelas')?></b></p>
                </div>
            </div>
        </div>
      </center>
    </div>
    <div class="animasi tada zoomInLeft animation-delay-2" align="center">
      <h5><a href="<?php echo base_url(); ?>first/register" style="color:#da0d08 "><?php echo $this->lang->line('daftar_disini')?>...!</a></h5>  
    </div>
    <!-- <div id="kotaklandscape" style="display: none; margin-top: 13%;">
      <center>
          <div style="font-size: 15px; text-align: center;" class="row card-hero no-shadow">
              <div class="col-xs-6 animated zoomInUp animation-delay-6" style="margin-top: -10%;">
                  <div class="panel-body ">
                      <div>
                          <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/InteractiveClass_rev.jpg" alt="Interactive Class" class="img-avatar-circle">
                      </div>
                      <div id="tulisanbanner1" style="margin-top: -10px; margin-left:-20px; margin-right:-20px;" class="card-block pt-4 text-center">
                          <h3 class="color-success">Interactive Classroom</h3>
                          <p>
                              <?php echo $this->lang->line('bannert1')?></p>
                      </div>
                  </div>
              </div>
              <div class="col-xs-6 animated zoomInUp animation-delay-6" style="margin-top: -10%;">
                  <div class="panel-body ">
                      <div>
                          <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/DigitalWhiteboard.jpg" alt="Digital Whiteboard" class="img-avatar-circle">
                      </div>
                      <div id="tulisanbanner2" style="margin-top: -10px; margin-left:-20px; margin-right:-20px;" class="card-block pt-4 text-center">
                          <h3 class="color-danger">Digital Whiteboard</h3>
                          <p>
                              <?php echo $this->lang->line('bannert2')?></p>
                      </div>
                  </div>
              </div>
              <div class="col-xs-6 animated zoomInUp animation-delay-6" style="margin-top: -10%;">
                  <div class="panel-body">
                      <div>
                          <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/Screenshare.jpg" alt="" class="img-avatar-circle">
                      </div>
                      <div id="tulisanbanner3" style="margin-top: -10px; margin-left:-20px; margin-right:-20px;" class="card-block pt-4 text-center">
                          <h3 class="color-warning">Screen</h3>
                          <h3 style="margin-top: -15px;" class="color-warning">Share</h3>
                          <p>
                              <?php echo $this->lang->line('bannert3')?></p>
                      </div>
                  </div>
              </div>
              <div class="col-xs-6 animated zoomInUp animation-delay-6" style="margin-top: -10%;">
                  <div class="panel-body">
                      <div>
                          <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/raise_hand.jpg" alt="" class="img-avatar-circle">
                      </div>
                      <div id="tulisanbanner4" style="margin-top: -10px; margin-left:-20px; margin-right:-20px;" class="card-block pt-4 text-center">
                          <h3 class="color-royal">Raise Hand</h3>
                          <br>
                          <p>
                              <?php echo $this->lang->line('bannert4')?></p>
                      </div>
                  </div>
              </div>
              <div class="col-xs-12 animated zoomInUp animation-delay-6" style="margin-top: -10%;">
                  <div class="panel-body">
                      <div>
                          <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/LiveChat.jpg" alt="" class="img-avatar-circle">
                      </div>
                      <div id="tulisanbanner5" style="margin-top: -10px; margin-left:-20px; margin-right:-20px;" class="card-block pt-4 text-center">
                          <h3 class="color-info">Live Chat</h3>
                          <br>
                          <p>
                              <?php echo $this->lang->line('bannert5')?></p>
                      </div>
                  </div>
              </div>
              <div class="col-md-1"></div>
          </div>
      </center>
    </div> -->
    <!-- <div style="display: none; font-size: 15px; margin-bottom: 10%; line-height: 100%;" class="row card-hero no-shadow" id="kotak2aaa">
      <div class="col-xs-12  animated zoomInUp animation-delay-6" style="margin-top: 0px;">
        <h3 align="center" class="color-success">Interactive Classroom</h3> 
        <div class="col-xs-3 animated fadeInLeft animation-delay-6">
            <img  src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/InteractiveClass_rev.png" alt="Interactive Class" class="img-circle img_5fitur">
        </div>
        <div style=""  class="col-xs-9"> 
              <p><?php echo $this->lang->line('bannert1')?></p>
        </div>            
      </div> 
      <div class="col-xs-12  animated zoomInUp animation-delay-6" style="margin-top: 0px;">
        <h3 align="center" class="color-danger">Digital Whiteboard</h3> 
        <div class="col-xs-3 animated fadeInLeft animation-delay-6">
            <img  src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/DigitalWhiteboard.png" alt="Digital Whiteboard" class="img-circle img_5fitur">
        </div>
        <div style=""  class="col-xs-9"> 
              <p ><?php echo $this->lang->line('bannert2')?></p>
        </div>            
      </div>
      <div class="col-xs-12  animated zoomInUp animation-delay-6" style="margin-top: 0px;">
        <h3 align="center" class="color-warning">Screen Share</h3> 
        <div class="col-xs-3 animated fadeInLeft animation-delay-6">
            <img  src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/Screenshare.png" alt="Screenshare" class="img-circle img_5fitur">
        </div>
        <div style=""  class="col-xs-9"> 
              <p ><?php echo $this->lang->line('bannert3')?></p>
        </div>            
      </div>
      <div class="col-xs-12  animated zoomInUp animation-delay-6" style="margin-top: 0px;">
        <h3 align="center" class="color-royal">Raise Hand</h3> 
        <div class="col-xs-3 animated fadeInLeft animation-delay-6">
            <img  src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/raise_hand.png" alt="Raise Hand" class="img-circle img_5fitur">
        </div>
        <div style=""  class="col-xs-9"> 
              <p ><?php echo $this->lang->line('bannert4')?></p>
        </div>            
      </div>
      <div class="col-xs-12  animated zoomInUp animation-delay-6" style="margin-top: 0px;">
        <h3 align="center" class="color-info">Live Chat</h3> 
        <div class="col-xs-3">
            <img  src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/LiveChat.png" alt="Live Chat" class="img-circle img_5fitur">
        </div>
        <div style=""  class="col-xs-9"> 
              <p ><?php echo $this->lang->line('bannert5')?></p>
        </div>            
      </div>
    </div> -->
  </div>
  <!-- <div id="kotakpaketclassmiles" class="bg-default ms-bg-fixed" style="background-color: rgba(236, 240, 241, 0.54);">
      <div class="">
        <div class="row no-gutters" style="margin-bottom: 2%; margin-top: 2%;">
          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12" align="center">
                  <h6><?php echo $this->lang->line('text_program')?></h6>
              </div>
          </div>
          <div class="">
            <div id="sliderposter" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators" id="indicator_poster"></ol>

              <div class="carousel-inner " style="cursor: pointer;" id="poster_only"></div>

              <a class="left carousel-control" href="#sliderposter" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#sliderposter" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
          <div id="carousel-listpackage" class="carousel slide hidden-xs" data-ride="carousel" data-interval="" pause="true">
              <div class="carousel-inner">
                  <div class="item active" id="package_front">
                      <div class="row" id="box_front_package">
                      </div>
                  </div>
                  <div class="item" id="package_back">
                      <div class="row" id="box_back_package"></div>
                  </div>
              </div>
          </div>
        </div>
      </div>
  </div> -->
  <!-- <div id="fitur_classmiles" class="row" style="padding: 0; max-width:100%;height:auto; background-color: #cbcaca; margin: 0; margin-bottom: 5px;">
    <div align="center" class="col-xs-12" style="padding: 0;">
      <div class="col-xs-3 " style="background-color: #008080; padding: 0;">
        <img class=" animasiku tada animated fadeInLeftBig animation-delay-10" id="btn_fitur1" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Interaktive Classroom.png" style="width: 65%; height: 65%; padding: 5px;" >
      </div>
      <div class="col-xs-3 " style="background-color: #008080; padding: 0;">
      <img class="animasiku tada animated fadeInLeftBig animation-delay-12" id="btn_fitur2" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Digital Whiteboard.png" style="width: 65%; height: 65%; padding: 5px;" >
      </div>
      <div class="col-xs-3 " style="background-color: #008080; padding: 0;">
        <img class=" animasiku tada animated fadeInLeftBig animation-delay-14" id="btn_fitur3" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Screenshare.png" style="width: 65%; height: 65%; padding: 5px;" >
      </div>
      <div class="col-xs-3 " style="background-color: #008080; padding: 0;">
        <img class=" animasiku tada animated fadeInLeftBig animation-delay-16" id="btn_fitur4" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Raise Hand.png" style="width: 65%; height: 65%; padding: 5px;" >
      </div>
    </div>
    <div class="col-xs-12" style="padding: 0; height: 140px;">
      <div id="video_fitur" class="" style="background-color: red; display: none;">
          <div align="center" class="video_class col-xs-6" style="margin: 0 auto; padding-left: 0; padding-right: 0; padding-bottom: 0; padding-top: 5%">
            <div class="col-sm-12 col-md-12" style="padding: 1px;">
              <div class="" >
              <iframe style='height: 89px; width:158px;'  src="https://www.youtube.com/embed/2bKS6Y8vJKQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>  
            </div>
          </div>
          <div align="center" class="video_class col-xs-6" style="margin: 0 auto; padding-left: 0; padding-right: 0; padding-bottom: 0; padding-top: 5%">
            <div class="col-sm-12 col-md-12" style="padding: 1px;">
              <div class="" >
              <iframe style='height: 89px; width:158px;'  src="https://www.youtube.com/embed/6Zl7FL22ooQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            </div>
          </div>
          <div id="container_fitur" class="col-xs-12" style="display: none; color:#414141;margin: 0 auto;">
            <div class="col-sm-12 col-md-12" style="padding-left: 5px; padding-top: 0;">
                  <h5><b id="judul_fitur" ></b></h5>
                  <h6 id="text_fitur" ></h6>
            </div>
          </div>
      </div>
      <div id="acontainer_fitur" class="container col-xs-12" style="display: none; margin-top: 6%; position: absolute; z-index: 10; border-radius: 5px; background-color: rgba(0,128,128,0.9); padding-top: 5px;">
        <div class="col-xs-9" style="padding: 0;">
          <h5><b id="judul_fitur" style="color: white;"></b></h5>
        </div>
        <div id="detail_fitur" class="col-md-3 col-xs-3 col-sm-3" style="">
          <img id="img_fitur" src="" style="width: 50px; height: 50px;" >
        </div>
        <div id="" class="col-md-7 col-sm-8 col-xs-12" style="padding: 0; color: black;">
          <h6 id="text_fitur"  style="color: white;"></h6>
        </div>
      </div>
    </div>
  </div> -->
  <section id="fiturr" style="display: none;">
      <!--  <div class="card">
        <div id="carousel-example-generic" class="ms-carousel carousel slide" data-ride="carousel">
          
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol>
          
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>screen/banner1.png" alt="...">
            </div>
            <div class="item">
              <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>screen/banner2.png" alt="...">
            </div>
            <div class="item">
              <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>screen/banner3.png" alt="...">                
            </div>
          </div>
          
          <a href="#carousel-example-generic" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
            <i class="zmdi zmdi-chevron-left"></i>
          </a>
          <a href="#carousel-example-generic" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
            <i class="zmdi zmdi-chevron-right"></i>
          </a>
        </div>
      </div> -->
      <div hidden="true" Class="fitur_potrait" style="padding: 0; background-color:white; ">
        <div class="col-md-12" style="padding: 0; margin-top: -20%;"> 
            <div class="zoomInUp">
                <p style ="margin-top: 85px; font-size: 15px; width: 90%; " class="lead text-right banner"><?php echo $this->lang->line('bannerb1')?></p>  
                <img  src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Time Flexible_Mobile.png" alt="..." style="width: 100%; margin-bottom: 1px; position: relative;">   
            </div>                                  
        </div>
        <div class="col-md-12" style="padding: 0;">
            <div class="zoomInUp">
                <p style ="margin-top: 85px; font-size: 15px; width: 90%; "  class="lead text-left banner"><?php echo $this->lang->line('bannerb2')?></p>
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Discussion_Mobile.png" alt="..." style="width: 100%;  margin-bottom: 1px;">
            </div>
        </div>
        <div class="col-md-12" style="padding: 0;">
            <div class="zoomInUp">
                <p style ="margin-top: 85px; font-size: 15px; width: 90%; "  class="lead text-right banner"><?php echo $this->lang->line('bannerb3')?></p>
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Private Class_Mobile.png" alt="..." style="width: 100%;  margin-bottom: 2px;">
            </div>
        </div>
        <div class="col-md-12" style="padding: 0;">
            <div class="zoomInUp">
                <p style ="margin-top: 85px; font-size: 15px; width: 90%; "  class="lead text-left banner"><?php echo $this->lang->line('bannerb4')?></p>
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Group Class_Mobile.png" alt="..." style="width: 100%;  margin-bottom: 2px;">
            </div>
        </div>               

        <!-- <div class="col-md-12" style="margin-bottom: 5%; padding: 0;">
            <div class="zoomInUp">
                <p class="lead text-right banner">Classmiles menyediakan berbagai kemudahan dalam proses pembayaran, hanya tinggal klik dan klik. Dan pastinya keamanannya sangat terjamin.</p>
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/ONLINE PAYMENT-01-01.png" alt="..." style="width: 100%;  margin-bottom: 2px;">
            </div>
        </div> -->
      </div>  
      <div hidden="true" class="fitur_landscape" style=" padding: 0; background-color:white;">
        <div class="col-md-12" style="padding: 0;margin-top:-7%;">
            <div class="zoomInUp">
                <p style="font-size: 15px" class="lead text-right banner"><?php echo $this->lang->line('bannerb1')?></p>
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/1354x520_landscape.png" alt="..." style="width: 100%;  margin-bottom: 1px;">
            </div>
        </div>
        <div class="col-md-12" style="padding: 0;">
            <div class="zoomInUp">
                <p style="font-size: 14px" class="lead text-left bannerleft"><?php echo $this->lang->line('bannerb2')?></p>
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/Discussion_landscape.png" alt="..." style="width: 100%;  margin-bottom: 1px;">
            </div>
        </div>
        <div class="col-md-12" style="padding: 0;">
            <div class="zoomInUp">
                <p style="font-size: 15px"  class="lead text-right banner"><?php echo $this->lang->line('bannerb3')?></p>
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/PrivateClass_landscape.png" alt="..." style="width: 100%;  margin-bottom: 2px;">
            </div>
        </div>
        <div class="col-md-12" style="padding: 0;">
            <div class="zoomInUp">
                <p  style="font-size: 15px" class="lead text-left bannerleft"><?php echo $this->lang->line('bannerb4')?></p>
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/GroupClass_landscape.png" alt="..." style="width: 100%;  margin-bottom: 2px;">
            </div>
        </div>               
        <!-- <div class="col-md-12" style="margin-bottom: 5%; padding: 0;">
            <div class="zoomInUp">
                <p class="lead text-right banner">Classmiles menyediakan berbagai kemudahan dalam proses pembayaran, hanya tinggal klik dan klik. Dan pastinya keamanannya sangat terjamin.</p>
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/ONLINE PAYMENT-01-01.png" alt="..." style="width: 100%;  margin-bottom: 2px;">
            </div>
        </div> -->
      </div>                     
  </section>    
  <!-- <div class="bg-default ms-bg-fixed" style="">
    <div class="container">
      <div class="text-center">
        <h4 class="uppercase color-warning wow fadeInDown animation-delay-2 text-normal"><?php echo $this->lang->line('fitur_tutor1')?></h4>
        <h4 class="uppercase color-warning  wow fadeInDown animation-delay-2 text-normal"><a href="<?php echo base_url(); ?>RegisterTutor"> <?php echo $this->lang->line('fitur_tutor3'); ?></a> <?php echo $this->lang->line('free'); ?>..!!</h4>
      <div id="fitur_4landscape" class="row" align="" style="line-height: 80%; color: #000000">
        <div class="col-xs-6 wow zoomInUp" style="">
          <div class="col-xs-3" style="padding-left: 0px; padding-right: 2px;">
              <img style="margin-top: 20px;  width: 80%; height: auto;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Kapanpun.png" alt=" " class="">
          </div>
          <div style=""  class="col-xs-9"> 
                 <h4 align="left" class="color-info"><b><u><?php echo $this->lang->line('fiturtutor1')?></u></b></h4> 
                <p style="text-align: left; font-size: 10px;"><?php echo $this->lang->line('isi_fitur1')?></p>
          </div>
        </div>
        <div class="col-xs-6 wow zoomInUp" style="">
         
          <div class="col-xs-3" style="padding-left: 0px; padding-right: 2px;">
              <img style="margin-top: 20px;  width: 80%; height: auto;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/DImanapun.png" alt=" " class="">
          </div>
          <div style=""  class="col-xs-9"> 
                 <h4 align="left" class="color-success"><b><u><?php echo $this->lang->line('fiturtutor2')?></u></b></h4> 
                <p style="text-align: left; font-size: 10px;"><?php echo $this->lang->line('isi_fitur2')?></p>
          </div>
        </div>
        <div class="col-xs-6 wow zoomInUp" style="">
         
          <div class="col-xs-3" style="padding-left: 0px; padding-right: 2px;">
              <img style="margin-top: 20px;  width: 80%; height: auto;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Tentukan Harga.png" alt=" " class="">
          </div>
          <div style=""  class="col-xs-9"> 
                 <h4 align="left" class="color-danger"><b><u><?php echo $this->lang->line('fiturtutor3')?></u></b></h4> 
                <p style="text-align: left; font-size: 10px;"><?php echo $this->lang->line('isi_fitur3')?></p>
          </div>
        </div>
        <div class="col-xs-6 wow zoomInUp" style="">
         
          <div class="col-xs-3" style="padding-left: 0px; padding-right: 2px;">
              <img style="margin-top: 20px;  width: 80%; height: auto;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>/baru/Efisiensi.png" alt=" " class="">
          </div>
          <div style=""  class="col-xs-9"> 
                 <h4 align="left" class="color-warning"><b><u><?php echo $this->lang->line('fiturtutor4')?></u></b></h4> 
                <p style="text-align: left; font-size: 10px;"><?php echo $this->lang->line('isi_fitur4')?></p>
          </div>
        </div>
      </div>
      <div id="fitur_4portrait" class="row" align="" style="line-height: 100%; color: #000000">
        <div class="col-xs-12 wow zoomInUp" style="padding-left: 0;">
          <div class="col-xs-3" style="padding-left: 0px; padding-right: 2px;">
              <img style="margin-top: 5px;  width: 80%; height: auto;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Kapanpun.png" alt=" " class="">
          </div>
          <div style="padding-left: 0;"  class="col-xs-9"> 
                 <h4 style="margin-bottom: 3px; margin-top: 5px;" align="left" class="color-info"><b><u><?php echo $this->lang->line('fiturtutor1')?></u></b></h4> 
                <h4 style="margin-top: 0; font-size: 12px; text-align: left;"><?php echo $this->lang->line('isi_fitur1')?></>
          </div>
        </div>
        <div class="col-xs-12 wow zoomInUp" style="padding-left: 0;">
         
          <div class="col-xs-3" style="padding-left: 0px; padding-right: 2px;">
              <img style="margin-top: 5px;  width: 80%; height: auto;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/DImanapun.png" alt=" " class="">
          </div>
          <div style="padding-left: 0;"  class="col-xs-9"> 
                 <h4 style="margin-bottom: 3px; margin-top: 5px;" align="left" class="color-success"><b><u><?php echo $this->lang->line('fiturtutor2')?></u></b></h4> 
                <h4 style="margin-top: 0; font-size: 12px; text-align: left;"><?php echo $this->lang->line('isi_fitur2')?></>
          </div>
        </div>
        <div class="col-xs-12 wow zoomInUp" style="padding-left: 0;">
         
          <div class="col-xs-3" style="padding-left: 0px; padding-right: 2px;">
              <img style="margin-top: 5px;  width: 80%; height: auto;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Tentukan Harga.png" alt=" " class="">
          </div>
          <div style="padding-left: 0;"  class="col-xs-9"> 
                 <h4 style="margin-bottom: 3px; margin-top: 5px;" align="left" class="color-danger"><b><u><?php echo $this->lang->line('fiturtutor3')?></u></b></h4> 
                <h4 style="margin-top: 0; font-size: 12px; text-align: left;"><?php echo $this->lang->line('isi_fitur3')?></>
          </div>
        </div>
        <div class="col-xs-12 wow zoomInUp" style="padding-left: 0;">
         
          <div class="col-xs-3" style="padding-left: 0px; padding-right: 2px;">
              <img style="margin-top: 5px;  width: 80%; height: auto;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>/baru/Efisiensi.png" alt=" " class="">
          </div>
          <div style="padding-left: 0;"  class="col-xs-9"> 
                 <h4 style="margin-bottom: 3px; margin-top: 5px;" align="left" class="color-warning"><b><u><?php echo $this->lang->line('fiturtutor4')?></u></b></h4> 
                <h4 style="margin-top: 0; font-size: 12px; text-align: left;"><?php echo $this->lang->line('isi_fitur4')?></>
          </div>
        </div>
      </div>
    </div>
  </div> -->
  <!-- <section class="ms-component-section">
    <div class="container">
      <h2 class="section-title no-margin-top">Featured Class</h2>
      <div class="owl-dots"></div>
      <div class="owl-carousel owl-theme" id="kotak_kelas">
      </div>
    </div>
  </section> -->

  <!-- <div class=" mb-4" style="background-color: rgba(236, 240, 241, 0.54);">
      <div class="container" >
        <div class="row " style="  margin-bottom: 2%; margin-top: 2%;">
          <div class="row">
              <div class="col-xs-12">
                <div class="col-xs-6">
                    <h4>Featured Classes</h4>
                </div>
                  <div id="controllernya" class="col-xs-6 controls pull-right">
                      <a class="left fa fa-chevron-left btn btn-success" href="#carousel-featuredclass"
                          data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-success" href="#carousel-featuredclass"
                              data-slide="next"></a>
                  </div>
              </div>
              <div class="col-xs-12">
              </div>
          </div>
          <div id="carousel-featuredclass" class="carousel slide" data-ride="carousel" data-interval="10000" style="margin-right: 10px; margin-left: 10px;">
              <div class="carousel-inner" id="inti_box" >
                  <div class="item active" id="featured_front"  > 
                      <div class="row" id="box_front" >

                      </div>
                  </div>
                  <div class="item" id="featured_back">
                      <div class="row" id="box_back">
                          
                      </div>
                  </div>

              </div>
          </div>
        </div>
      </div>
  </div> -->
  <!-- <div class="wrap ms-hero-page ms-hero-bg-info ms-bg-fixed color-white" style=" background: url('<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>headers/1.png'); background-repeat: no-repeat; background-size: cover; background-attachment:fixed; height: 80%;">
    <div class="container" style="" align="center">
          <h2 style="margin-top: -5px;" class="color-white"><?php echo $this->lang->line('bannerdapat')?>
            <span class="text-normal"><?php echo $this->lang->line('bannerdimana')?></span><?php echo $this->lang->line('dan')?><span class="text-normal"><?php echo $this->lang->line('bannerkapan')?></span>.
          </h2>
          <div class="row">
            <div class="col-xs-12" style="margin-top: -20px;">
                <img class="img-responsive animated zoomInUp animation-delay-3" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/AllMedia_rev2.png" > 
            </div>
            <div class="col-md-6 col-xs-12">
              <div align="center" style="">
                    <label class="color-white" style="font-size: 25px;"><?php echo $this->lang->line('bannerunduh')?><br> <b><?php echo $this->lang->line('free')?></b>!</label>
                </div>
                <div class="col-xs-12 col-sm-12 withripple zoom-img" align="center" style="">
                  <a href="https://play.google.com/store/search?q=classmiles.com" target="_blank" target="_blank" class="wow animated flipInX animation-delay-4"><img class="img-rounded img-fluid" style="padding: 4px; width: 20%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Google Playstore.png"></a>
                </div>
                <div class="col-xs-4 col-sm-4 withripple zoom-img   " align="center" style="">
                  <a href="intent://tutor/#Intent;scheme=classmiles;package=com.classmiles.tutor;end" class="wow animated flipInX animation-delay-4"><img class="img-rounded  img-fluid" style="padding: 4px; width: 50%; " src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/Classmiles Tutor.png"><br><label style="color: white;">Classmiles Tutor</label></a>
                </div>
                <div class="col-xs-4 col-sm-4 withripple zoom-img" align="center" style="">
                  <a href="intent://student/#Intent;scheme=classmiles;package=com.classmiles.student;end" class="wow animated flipInX animation-delay-4"><img class="img-rounded  img-fluid" style="padding: 4px; width: 50%; " src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/Classmiles.png"><br><label style="color: white;">Classmiles</label></a>
                </div>
                <div class="col-xs-4 col-sm-4 withripple zoom-img" align="center" style="">
                  <a href="intent://kids/#Intent;scheme=classmiles;package=com.classmiles.kids;end" class="wow animated flipInX animation-delay-4"><img class="img-rounded  img-fluid" style="padding: 4px; width: 50%; " src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/Classmiles Kids.png"><br><label style="color: white;">Classmiles Kids</label></a>
                </div>
            </div>
          </div>                            
    </div>
  </div> -->
  <!-- </div> -->
  <div  class="" id="contact" style="background-color: #EEEEEE;">
      <?php
          $this->load->view('mobile/home/footer.php');
      ?>
  </div>
  <center>
    <footer align="center" style="line-height: 80%; margin-top: 2%; background-color: white; color: black;">
        <div class="">  
          <p>Copyright &copy; Indiclass 2018 <!-- <br> <a href="https://meetaza.com/" target="_blank" style="color: white;">PT. Meetaza Prawira Media</a> --></p>
        </div>
    </footer>
  </center>
  <!-- sb-site-container -->
  <div class="ms-slidebar sb-slidebar sb-momentum-scrolling sb-style-overlay " align="center">
      <?php
          $this->load->view('home/sidemenu.php');
      ?>
  </div>
  <script type="text/javascript">
    $(document).ready(function(){var baba=$("#kotakdepan").width();$("#kotakdepan").css('background-size',baba);var code=null;var cekk=setInterval(function(){code="<?php echo $this->session->userdata('code');?>";cek();},500);function cek(){if(code=="101"){$("#login_modal").modal('show');$("#alertwrong").css('display','block');code==null;clearInterval(cekk);$.ajax({url:'<?php echo base_url(); ?>/Rest/clearsession',type:'POST',data:{code:code},success:function(response) {console.warn(response);}});} else if(code=="102"){$("#login_modal").modal('show');$("#alertwrong").css('display','none');code==null;clearInterval(cekk);$.ajax({url:'<?php echo base_url(); ?>/Rest/clearsession',type:'POST',data:{code:code},success:function(response) {console.warn(response);}});} else if(code=="109"){$("#login_modal").modal('show');code==null;clearInterval(cekk);$.ajax({url:'<?php echo base_url(); ?>/Rest/clearsession',type:'POST',data:{code:code},success:function(response) {console.warn(response);}});} else if(code=="800") {$("#login_modal").modal('show');code==null;clearInterval(cekk);} else {$("#login_modal").modal('hide');$("#alertwrong").css('display','none');clearInterval(cekk);} console.warn(code);} $('#g_signInbtn').click(function(){auth2.signIn().then(function(){var data=auth2.currentUser.get().getBasicProfile();$('#google_data').html(JSON.stringify(data));$('#google_form').submit();console.log(auth2.currentUser.get().getId());});});
      $('#f_signInbtn').click(function(){             
            FB.login(function(response){                
                if(response.status == 'connected'){
                    var new_data = {};
                    FB.api('/me', { locale: 'en_US', fields: 'name, email' }, function(response) {
                        console.log(JSON.stringify(response));
                        new_data['name'] = response.name;
                        new_data['email'] = response.email;
                        var id = response.id;
                        FB.api('/me/picture',{type: 'large'} ,function(response) {
                            // console.log(JSON.stringify(response));
                            new_data['image'] = "https://graph.facebook.com/"+id+"/picture";
                            new_data = JSON.stringify(new_data);
                            $('#fb_data').html(new_data);
                            $('#facebook_form').submit();
                        });
                    });
                }
            }, {scope: 'public_profile,email'});
        });
      function inDulu(){auth2.signIn().then(function(){console.log(auth2.currentUser.get().getId());});} $('#btn_setindonesia').click(function(e){e.preventDefault();$.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){location.reload();});});$('#btn_setenglish').click(function(e){e.preventDefault();$.get('<?php echo base_url('set_lang/english'); ?>',function(){location.reload();});});function inDulu(){auth2.signIn().then(function(){console.log(auth2.currentUser.get().getId());});} $('#btn_setindonesia').click(function(e){e.preventDefault();$.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){location.reload();});});$('#btn_setenglish').click(function(e){e.preventDefault();$.get('<?php echo base_url('set_lang/english'); ?>',function(hasil){location.reload();});});$('#submit_signup_student').click(function(e){var sEmail=$('#email').val();if(validateEmail(sEmail)){$("#capemail").css('display','none');} else{$("#capemail").css('display','block');e.preventDefault();}});$('#submit_signup').click(function(e){var sEmail=$('#emaill').val();if(validateEmail(sEmail)){$("#capemaill").css('display','none');} else{$("#capemaill").css('display','block');e.preventDefault();}});function validateEmail(sEmail){var filter=/^([w-.]+)@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.)|(([w-]+.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$/;if(filter.test(sEmail)){return true;} else{return false;}} $('#g_signInbtn').click(function(){auth2.signIn().then(function(){var data=auth2.currentUser.get().getBasicProfile();$('#google_data').html(JSON.stringify(data));$('#google_form').submit();console.log(auth2.currentUser.get().getId());});});$('#g2_signInbtn').click(function(){auth2.signIn().then(function(){var data=auth2.currentUser.get().getBasicProfile();$('#google_data').html(JSON.stringify(data));$('#google_form').submit();console.log(auth2.currentUser.get().getId());});});$('#f_signInbtn').click(function(){FB.login(function(response){if(response.status=='connected'){var new_data={};FB.api('/me',{locale:'en_US',fields:'name, email'},function(response){console.log(JSON.stringify(response));new_data['name']=response.name;new_data['email']=response.email;FB.api('/me/picture',{type:'large'},function(response){console.log(JSON.stringify(response));new_data['image']=response.data.url;new_data=JSON.stringify(new_data);$('#fb_data').html(new_data);$('#facebook_form').submit();});});}},{scope:'public_profile,email'});});$('#f_signInbtn2').click(function(){FB.login(function(response){if(response.status=='connected'){var new_data={};FB.api('/me',{locale:'en_US',fields:'name, email'},function(response){console.log(JSON.stringify(response));new_data['name']=response.name;new_data['email']=response.email;FB.api('/me/picture',{type:'large'},function(response){console.log(JSON.stringify(response));new_data['image']=response.data.url;new_data=JSON.stringify(new_data);$('#fb_data').html(new_data);$('#facebook_form').submit();});});}},{scope:'public_profile,email'});});$('#kata_sandi').on('keyup',function(){if($('#konf_kata_sandi').val()!=$(this).val()||$('#term_agreementt').is(':checked')==false){$('#submit_signup').attr('disabled','');}else{$('#submit_signup').removeAttr('disabled');}});$('#konf_kata_sandi').on('keyup',function(){if($('#kata_sandi').val()!=$(this).val()||$('#term_agreementt').is(':checked')==false){$('#submit_signup').attr('disabled','');}else{$('#submit_signup').removeAttr('disabled');}});$('#kata_sandi_student').on('keyup',function(){if($('#konf_kata_sandi_student').val()!=$(this).val()||$('#term_agreement').is(':checked')==false){$('#submit_signup_student').attr('disabled','');}else{$('#submit_signup_student').removeAttr('disabled');}});$('#konf_kata_sandi_student').on('keyup',function(){if($('#kata_sandi_student').val()!=$(this).val()||$('#term_agreement').is(':checked')==false){$('#submit_signup_student').attr('disabled','');}else{$('#submit_signup_student').removeAttr('disabled');}});$('#term_agreementt').change(function(){if($('#kata_sandi').val()!=$('#konf_kata_sandi').val()||$('#term_agreementt').is(':checked')==false){$('#submit_signup').attr('disabled','');}else{$('#submit_signup').removeAttr('disabled');}});$('#term_agreement').change(function(){if($('#kata_sandi_student').val()!=$('#konf_kata_sandi_student').val()||$('#term_agreement').is(':checked')==false){$('#submit_signup_student').attr('disabled','');}else{$('#submit_signup_student').removeAttr('disabled');}});});$('.btn_setindonesialp').click(function(e){e.preventDefault();var lang="<?php echo $this->session->userdata('lang'); ?>";if(lang=="indonesia"){$.get('<?php echo base_url('set_lang/english'); ?>',function(hasil){location.reload();});}else{$.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){location.reload();});}});

  </script>
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  <script src="https://apis.google.com/js/api:client.js"></script>
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/landing_page/js/plugins.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/landing_page/js/app.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/landing_page/js/component-carousel_mobiles.js"></script>
  <script src="<?php echo base_url(); ?>aset/sel2/js/select2.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.min.js"></script>
  <script type="text/javascript">
    $("#nav_login").click(function(){
      $("#saran_modal").modal('show');
      // alert("A");
    });
    $("#btn_login_saran_modal").click(function(){
      $("#login_modal").modal('show');
    });    
  </script>
  <script type="text/javascript">

  var A = window.matchMedia("(orientation: landscape)").matches;
  var cek_height = window.screen.availHeight;
  var cek_width = window.screen.availWidth
  // alert("H"+cek_height);
  // alert("W"+cek_width);

  var  kelascount = 0;
  var user_utc = new Date().getTimezoneOffset();
    user_utc = -1 * user_utc;        
    var currentdate = new Date(); 
    currentdate = currentdate.getFullYear()+'-'+(currentdate.getMonth()+1)+'-'+currentdate.getDate()+' '+currentdate.getHours()+":"+currentdate.getMinutes()+":"+currentdate.getSeconds();  
    $.ajax({
          url: '<?php echo base_url(); ?>Master/getFeaturedClass',
          type: 'POST',
          data: {                
              currentdate: currentdate,
              user_utc : user_utc,
              device: 'web'
          },            
          success: function(data)
          {         
              result = JSON.parse(data);  
              
              if (result['message'] == null) {
                  $("#kotakfeaturedclass").css('display','none');
              }   
              else
              {           
                  if (result['status'] == true) 
                  {                       
                      for (var i = 0; i < result['message'].length; i++) {
                          
                          var classid         = result['message'][i]['class_id'];
                          var name          = result['message'][i]['name'];                        
                          
                          var description     = result['message'][i]['description'];
                          var statusall       = result['message'][i]['status_all'];
                          // alert(statusall);
                          var alt_img         ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                          var class_type      = result['message'][i]['class_type'];
                          var channel_color    = result['message'][i]['channel_color'];
                          var user_name       = result['message'][i]['user_name'];
                          var user_image      = result['message'][i]['user_image'];
                          var subject_name    = result['message'][i]['subject_name'];
                          var channel_name    = result['message'][i]['channel_name'];
                          // var jenjang_name    = result['message'][i]['jenjang_name'];
                          // var jenjang_level   = result['message'][i]['jenjang_level'];
                          var jenjangnamelevel    = result['message'][i]['jenjangnamelevel'];  
                          if (jenjangnamelevel == "UMUM - 0") {
                            var jenjangnamelevel = "UMUM";
                          }    
                          
                          var all_sma_ipa = name.substr(name.length - 13);                  
                          // if (jenjang_level==0) {
                          //  jenjang_level="";
                          // }
                          var firstdate       = result['message'][i]['firstdate'];
                          var firsttime       = result['message'][i]['firsttime'];
                          var finishtime      = result['message'][i]['finishtime'];
                          var harga           = result['message'][i]['harga'];
                          var namee           = name.substring(0, 35);
                          var desc            = description.substring(0, 35);  
                          var link            = result['message'][i]['link'];
                          var participant_count = result['message'][i]['participant_count'];
                          if (user_name.length > 30) {
                            user_name           = user_name.substring(0, 32)+"...";
                          }
                          // alert(user_name);
                          if (user_name == "DPP IKA UNY") {
                            // alert("haai");
                            count = 350+participant_count+" Joined";
                          }
                          if (participant_count == "0" && user_name != "DPP IKA UNY") {
                            count = "<br>";
                          }
                          else if (participant_count != "0" && user_name != "DPP IKA UNY") 
                          {
                            count = participant_count+" Joined";
                          }
                          // console.warn(jenjangnamelevel);
                          var number_string = harga.toString(),
                              sisa    = number_string.length % 3,
                              rupiah  = number_string.substr(0, sisa),
                              ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                  
                          if (ribuan) {
                              separator = sisa ? '.' : '';
                              rupiah += separator + ribuan.join('.');
                          }
                          if (statusall != null && jenjangnamelevel == "SMA - IPA 10, SMA - IPA 11, SMA - IPA 12") {
                            var desc_jenjang = "Semua Jenjang SMA IPA"
                          }
                          else if (statusall != null && jenjangnamelevel == "SMA - IPS 10, SMA - IPS 11, SMA - IPS 12") {
                            var desc_jenjang = "Semua Jenjang SMA IPS"
                          }
                          else if (statusall != null && jenjangnamelevel == "SMP 7, SMP 8, SMP 9") {
                            var desc_jenjang = "Semua Jenjang SMP"
                          }
                          else if (statusall != null && jenjangnamelevel == "SMK 10, SMK 11, SMK 12") {
                            var desc_jenjang = "Semua Jenjang SMK"
                          }
                          else if (statusall == null && jenjangnamelevel == "SD 1, SD 2, SD 3, SD 4, SD 5, SD 6") {
                            var desc_jenjang = "Semua Jenjang SD"
                          }
                          else if (statusall == null && jenjangnamelevel == "TK, SD 1, SD 2, SD 3, SD 4, SD 5, SD 6") {
                            var desc_jenjang = "TK dan SD"
                          }
                          else if (statusall == null) {
                            var desc_jenjang = jenjangnamelevel;
                          }
                          else if (statusall != null) {
                            var desc_jenjang = "Semua Jenjang"
                          }
                          if (rupiah=="F") {
                            rupiah = "Free";
                          }
                          else
                          {
                            rupiah = "Rp. "+rupiah;
                          }
                         
                          //Untuk disabled jika kelasnya kurang dari 4
                          
                          kelascount++;

                              if (channel_name != null) {
                                var box_front = "<div class='card animation-delay-"+i+"' style=''>"+
                                                    "<div class='price-table wow zoomInUp'>"+
                                                      "<header class='price-table-header' style='background-color: "+channel_color+";'>"+
                                                        "<span class='price-table-category'>"+channel_name+"</span>"+
                                                      "</header>"+
                                                      "<div class='col-item'>"+
                                                        "<div class='photo' style='padding:3%'; align='center'  '>"+
                                                          "<img src='"+user_image+"' class='img-fluid img-responsive' onerror="+alt_img+" style='height: 200px; width: auto;' />"+
                                                        "</div>"+
                                                        "<div class='info' style='padding-left:10px;'>"+
                                                            "<div class='row'>"+
                                                                "<div class='price col-md-12 col-sm-12 col-xs-12' style='margin-bottom:0'>"+
                                                                    "<h5>"+namee+"</h5>"+
                                                                    "<p style='font-size:13px;'>"+desc+"</p>"+
                                                                    "<p class='price-text-color' style='margin-bottom:0; font-weight: 10px;'>"+user_name+"</p>"+
                                                                "</div>"+
                                                               "<div class='col-md-12 col-md-12 col-xs-12' style=''>"+
                                                                  "<p style='font-size:11px;'>"+desc_jenjang+"</p>"+
                                                                "</div>"+
                                                                "<div class='col-md-8 col-sm-8 col-xs-8' style=''>"+
                                                                  "<p style='font-size:11px; margin-bottom:0;'>"+firstdate+", "+firsttime+" - "+finishtime+"</p>"+
                                                                  "<p style='font-size:11px; margin-bottom:0;'>"+count+"</p>"+
                                                                "</div>"+
                                                                "<div class='col-md-4 col-sm-4 col-xs-4 price-text-color' style='bottom:0; font-size:15px; padding:0' align='center'>"+
                                                                "<b>"+rupiah+"</b>"+
                                                                "</div>"+
                                                            "</div>"+
                                                            "<div class='separator clear-left'>"+                                           
                                                                "<center>"+
                                                                    "<div style='margin-top: 3%;'>"+
                                                                      "<a style='background-color:"+channel_color+"; href='"+link+"' class='ml-1 btn btn-raised btn-info' title='Klik untuk join'>Join</a>"+
                                                                    "</div>"+
                                                                "</center>"+
                                                            "</div>"+
                                                            "<div class='clearfix'>"+
                                                            "</div>"+
                                                        "</div>"+
                                                    "</div>"+
                                                    "</div>"+
                                                "</div>";
                              }
                              else{
                              	var box_front = "<div class='card animation-delay-"+i+"' style=''>"+
                                                    "<div class='price-table wow zoomInUp' style=''>"+
                                                      "<header class='price-table-header'>"+
                                                      "</header>"+
                                                      "<div class='col-item' style='padding-top:46px;'>"+
                                                        "<div class='photo' style='padding:3%'; align='center'  '>"+
                                                          "<img src='"+user_image+"' class='img-fluid img-responsive' onerror="+alt_img+" style='height: 200px; width: auto;' />"+
                                                        "</div>"+
                                                        "<div class='info' style='padding-left:10px;'>"+
                                                            "<div class='row'>"+
                                                                "<div class='price col-md-12 col-sm-12 col-xs-12' style='margin-bottom:0'>"+
                                                                    "<h5>"+namee+"</h5>"+
                                                                    "<p style='font-size:13px;'>"+desc+"</p>"+
                                                                    "<p class='price-text-color' style='margin-bottom:0; font-weight: 10px;'>"+user_name+"</p>"+
                                                                "</div>"+
                                                               "<div class='col-md-12 col-md-12 col-xs-12' style=''>"+
                                                                  "<p style='font-size:11px;'>"+desc_jenjang+"</p>"+
                                                                "</div>"+
                                                                "<div class='col-md-8 col-sm-8 col-xs-8' style=''>"+
                                                                  "<p style='font-size:11px; margin-bottom:0;'>"+firstdate+", "+firsttime+" - "+finishtime+"</p>"+
                                                                  "<p style='font-size:11px; margin-bottom:0;'>"+count+"</p>"+
                                                                "</div>"+
                                                                "<div class='col-md-4 col-sm-4 col-xs-4 price-text-color' style='bottom:0; font-size:15px; padding:0' align='center'>"+
                                                                "<b>"+rupiah+"</b>"+
                                                                "</div>"+
                                                            "</div>"+
                                                            "<div class='separator clear-left'>"+                                           
                                                                "<center>"+
                                                                    "<div style='margin-top: 3%;'>"+
                                                                      "<a href='"+link+"' class='ml-1 btn btn-raised btn-info' title='Klik untuk join'>Join</a>"+
                                                                    "</div>"+
                                                                "</center>"+
                                                            "</div>"+
                                                            "<div class='clearfix'>"+
                                                            "</div>"+
                                                        "</div>"+
                                                    "</div>"+
                                                    "</div>"+
                                                "</div>";
                              } 
                              $("#kotak_kelas").append(box_front);
                           //    if (kelascount < 5) {
                            //  $("#carousel-featuredclass").removeAttr("data-interval");
                            //  $("#controllernya").css('display','none');
                            // }  
                      }
                      $("#kotakfeaturedclass").css('display','block');
                  }
                  else
                  {
                      $("#kotakfeaturedclass").css('display','none');
                    // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Anda telah menolak permintaan group class ini");
                   //    setTimeout(function(){
                   //        window.location.reload();
                   //    },3000);
                  }
              }                     
          }
      });      
  // $.ajax({
  //       url: '<?php echo base_url(); ?>Master/getFeaturedClass',
  //       type: 'POST',
  //       data: {                
  //         currentdate: currentdate,
  //           user_utc : user_utc,
  //           device: 'mobile'
  //       },            
  //       success: function(data)
  //       {         
  //         result = JSON.parse(data);                
  //           if (result['status'] == true) 
  //           {                       
  //              for (var i = 0; i < result['message'].length; i++) {
  //                   var classid         = result['message'][i]['class_id'];
  //                   var name          = result['message'][i]['name'];
  //                   var description     = result['message'][i]['description'];
  //                   var class_type      = result['message'][i]['class_type'];
  //                   var user_name       = result['message'][i]['user_name'];
  //                   var statusall       = result['message'][i]['status_all'];
  //                   var alt_img         ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
  //                   var user_image      = result['message'][i]['user_image'];
  //                   var subject_name    = result['message'][i]['subject_name'];
  //                   // var jenjang_name    = result['message'][i]['jenjang_name'];
  //                   // var jenjang_level   = result['message'][i]['jenjang_level'];
  //                   // if (jenjang_level==0) {
  //                   //  jenjang_level="";
  //                   // }
  //                   var jenjangnamelevel   = result['message'][i]['jenjangnamelevel'];
  //                   var firstdate       = result['message'][i]['firstdate'];
  //                   var firsttime       = result['message'][i]['firsttime'];
  //                   var finishtime      = result['message'][i]['finishtime'];
  //                   var harga         = result['message'][i]['harga'];
  //                   var namee       = name.substring(0, 22);
  //                   var desc      = description.substring(0, 35);  
  //                   var usr_name       = user_name.substr(0, 20);
  //                   var link      = result['message'][i]['link'];
  //                   var participant_count = result['message'][i]['participant_count'];
  //                   if (participant_count == "0") {
  //                     count = "<br>";
  //                   }
  //                   else
  //                   {
  //                     count = participant_count+" Joined";
  //                   }
  //                   if (statusall != null && jenjangnamelevel == "SMA - IPA 10, SMA - IPA 11, SMA - IPA 12") {
  //                     var desc_jenjang = "Semua Jenjang SMA IPA"
  //                   }
  //                   else if (statusall != null && jenjangnamelevel == "SMA - IPS 10, SMA - IPS 11, SMA - IPS 12") {
  //                     var desc_jenjang = "Semua Jenjang SMA IPS"
  //                   }
  //                   else if (statusall != null && jenjangnamelevel == "SMP 7, SMP 8, SMP 9") {
  //                     var desc_jenjang = "Semua Jenjang SMP"
  //                   }
  //                   else if (statusall != null && jenjangnamelevel == "SMK 10, SMK 11, SMK 12") {
  //                     var desc_jenjang = "Semua Jenjang SMK"
  //                   }
  //                   else if (statusall == null && jenjangnamelevel == "SD 1, SD 2, SD 3, SD 4, SD 5, SD 6") {
  //                     var desc_jenjang = "Semua Jenjang SD"
  //                   }
  //                   else if (statusall == null && jenjangnamelevel == "TK, SD 1, SD 2, SD 3, SD 4, SD 5, SD 6") {
  //                     var desc_jenjang = "TK dan SD"
  //                   }
  //                   else if (statusall == null) {
  //                     var desc_jenjang = jenjangnamelevel;
  //                   }
  //                   else if (statusall != null) {
  //                     var desc_jenjang = "Semua Jenjang"
  //                   }

  //                   var number_string = harga.toString(),
  //                       sisa    = number_string.length % 3,
  //                       rupiah  = number_string.substr(0, sisa),
  //                       ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                            
  //                   if (ribuan) {
  //                       separator = sisa ? '.' : '';
  //                       rupiah += separator + ribuan.join('.');
  //                   }
  //                   if (rupiah=="F") {
  //                     rupiah = "Free";
  //                   }
  //                   else
  //                   {
  //                     rupiah = "Rp. "+rupiah;
  //                   }
  //                  if (i+1 >= 3) {
  //                     kelascount++;
  //                       var box_back = "<div class='col-xs-6' style='border-left: 1px solid;  cursor: pointer;' title='"+name+" - "+description+"'>"+
  //                           "<div class='col-item'>"+
  //                               "<div class='photo'>"+
  //                                   "<img src='"+user_image+"' class='img-fluid img-responsive' onerror="+alt_img+"  style='height: 150px; width: 100%;' />"+
  //                               "</div>"+
  //                               "<div class='info'>"+
  //                                   "<div class='row'>"+
  //                                       "<div class='price col-xs-12' style='margin-left:2%; line-height:80%'>"+
  //                                           "<h6 style='' >"+namee+"</h6>"+
  //                                           "<p style='font-size:13px; height:25px;'>"+desc+"</p>"+
  //                                           "<p class='' style='color:#03a9f4; line-height:80%; font-weight: 10px;'>"+usr_name+"</p>"+
  //                                       "</div>"+
  //                                       "<div class='col-xs-12' style='line-height:80%; padding-right:0; margin-left:2%;'>"+
  //                                         "<p style='font-size:12px; height:25px;'>"+desc_jenjang+"</p>"+
  //                                         "<p style='font-size:11px; margin-top:-3%;'>"+firstdate+", "+firsttime+" - "+finishtime+"</p>"+
  //                                         "<p style='font-size:11px; line-height:80%;'>"+count+"</p>"+
  //                                         "<p style='color:#03a9f4; text-align:left'>"+rupiah+"</p>"+
  //                                       "</div>"+
  //                                   "</div>"+
  //                                   "<div class='separator clear-left'>"+                                           
  //                                       "<center>"+
  //                                           "<div style='margin-top: 3%;'>"+
  //                                             "<a href='"+link+"' class='ml-1 btn btn-raised btn-info' title='Klik untuk join'>Join</a>"+
  //                                           "</div>"+
  //                                       "</center>"+
  //                                   "</div>"+
  //                                   "<div class='clearfix'>"+
  //                                   "</div>"+
  //                               "</div>"+
  //                           "</div>"+
  //                       "</div>";
  //                     $("#box_back").append(box_back);
  //                   }
                    
  //                   else
  //                   {
  //                     kelascount++;
  //                     // alert(kelascount);
  //                     if (kelascount == 0) {

  //                         document.getElementById("carousel-featuredclass").removeAttribute('class');
  //                         // $("#carousel-featuredclass").removeAttr("data-interval");
  //                       $("#controllernya").css('display','none');
  //                       }
  //                       else
  //                       {
  //                         document.getElementById("carousel-featuredclass").setAttribute("class", "carousel slide");
  //                         // $("#carousel-featuredclass").removeAttr("data-interval");
  //                       $("#controllernya").css('display','block');
  //                       }
  //                     var box_front = "<div class='col-xs-6' style='border-left: 1px solid; cursor: pointer; ' title='"+name+" - "+description+"'>"+
  //                         "<div class='col-item' style=''>"+
  //                             "<div class='photo'>"+
  //                                 "<img src='"+user_image+"' class='img-fluid img-responsive' onerror="+alt_img+"  style='height: 150px; width: 100%;' />"+
  //                             "</div>"+
  //                             "<div class='info'>"+
  //                                 "<div class='row'>"+
  //                                     "<div class='price col-xs-12' style='margin-left:2%; line-height:80%'>"+
  //                                         "<h6 style='' >"+namee+"</h6>"+
  //                                         "<p style='font-size:13px; height:25px;'>"+desc+"</p>"+
  //                                         "<p class='' style='color:#03a9f4; line-height:80%; font-weight: 10px;'>"+usr_name+"</p>"+
  //                                     "</div>"+
  //                                     "<div class='col-xs-12' style='line-height:80%; padding-right:0; margin-left:2%;'>"+
  //                                       "<p style='font-size:12px; height:25px;'>"+desc_jenjang+"</p>"+
  //                                       "<p style='font-size:11px; margin-top:-3%;'>"+firstdate+", "+firsttime+" - "+finishtime+"</p>"+
  //                                       "<p style='font-size:11px; line-height:80%;'>"+count+"</p>"+
  //                                       "<p style='color:#03a9f4; text-align:left'>"+rupiah+"</p>"+
  //                                     "</div>"+
  //                                 "</div>"+
  //                                 "<div class='separator clear-left'>"+                                           
  //                                     "<center>"+
  //                                         "<div style='margin-top: 3%;'>"+
  //                                           "<a href='"+link+"' class='ml-1 btn btn-raised btn-info' title='Klik untuk join'>Join</a>"+
  //                                         "</div>"+
  //                                     "</center>"+
  //                                 "</div>"+
  //                                 "<div class='clearfix'>"+
  //                                 "</div>"+
  //                             "</div>"+
  //                         "</div>"+
  //                     "</div>";
  //                   $("#box_front").append(box_front);
  //                   }
  //               }
  //           }
  //           else
  //           {
  //             // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Anda telah menolak permintaan group class ini");
  //            //    setTimeout(function(){
  //            //        window.location.reload();
  //            //    },3000);
  //           }
  //       }
  //   });
  </script>
  <script type="text/javascript">
      $(document).ready(function() {
          setInterval(function(){  
             $('#banner_landscape').css('display','block');
          },1000);
          setInterval(function(){  
             $('#banner_portrait').css('display','block');
                   
          },1000);          
      });
      jQuery(document).ready(function(){ 
        $(function(){
            $('#banner_portrait h4:gt(0)').hide();
            setInterval(function(){
              $('#banner_portrait :first-child').fadeOut(10)
                 .next('h4').fadeIn(2000)
                 .end().appendTo('#banner_portrait');}, 
              3000);
        });
        $(function(){
            $('#banner_landscape h4:gt(0)').hide();
            setInterval(function(){
              $('#banner_landscape :first-child').fadeOut(10)
                 .next('h4').fadeIn(2000)
                 .end().appendTo('#banner_landscape');}, 
              3000);
        });
      });
  </script>
  <script type="text/javascript">
      $(document).ready(function(){
          $("#btn_fitur1").hover(function(){
            $("#container_fitur").css('display','block');
            var img_fitur = document.getElementById('img_fitur');
          img_fitur.src = "<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Interaktive Classroom.png";
              $("#judul_fitur").text("Interactive Classroom");
              $(".video_class").css('display','none');  
              $("#text_fitur").text("<?php echo $this->lang->line('bannert1')?>");
              }, function(){
              $(".video_class").css('display','block');  
              $("#container_fitur").css('display','none');
          });
          $("#btn_fitur2").hover(function(){
            $("#container_fitur").css('display','block');
            var img_fitur = document.getElementById('img_fitur');
          img_fitur.src = "<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Digital Whiteboard.png";
              $("#judul_fitur").text("Digital Whiteboard");
              $(".video_class").css('display','none');  
              $("#text_fitur").text("<?php echo $this->lang->line('bannert2')?>");
              }, function(){
                $(".video_class").css('display','block');  
              $("#container_fitur").css('display','none');
          });
          $("#btn_fitur3").hover(function(){
            $("#container_fitur").css('display','block');
            var img_fitur = document.getElementById('img_fitur');
          img_fitur.src = "<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Screenshare.png";
              $("#judul_fitur").text("Screenshare");
              $(".video_class").css('display','none');  
              $("#text_fitur").text("<?php echo $this->lang->line('bannert3')?>");
              }, function(){
              $("#container_fitur").css('display','none');
              $(".video_class").css('display','block');  
          });
          $("#btn_fitur4").hover(function(){
            $("#container_fitur").css('display','block');
            var img_fitur = document.getElementById('img_fitur');
          img_fitur.src = "<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Raise Hand.png";
              $("#judul_fitur").text("Raise Hand");
              $(".video_class").css('display','none');  
              $("#text_fitur").text("<?php echo $this->lang->line('bannert4')?>");
              }, function(){
              $("#container_fitur").css('display','none');
              $(".video_class").css('display','block');  
          });
          $(document).on("click", ".show_modal_desc", function () {
            $("#list_bidang").empty();
            $("#list_fasilitas").empty();
            $('#txt_poster').attr('src', '');
            var price = $(this).data('price');
            var desc = $(this).data('desc');
            var poster = $(this).data('poster');
            var id_paket = $(this).data('id_paket');
            var name = $(this).data('name');
            var bidang = $(this).data('bidang');
            var fasilitas = $(this).data('fasilitas');
            var listbidang = bidang.split(',');
            var listfasilitas = fasilitas.split(',');
            // console.log(listbidang);
            $("#desc_package_modal").modal('show');
            $("#txt_name").text(name);
            // $("#txt_price").text("Rp. "+price);
            $("#txt_desc").text(desc);
            $("#txt_id_paket").val(id_paket);
            $("#txt_fasilitas").text("Fasilitas : "+fasilitas);
            $("#txt_bidang").text("Bidang : "+bidang);
            // alert(fasilitas);
            document.getElementById("txt_poster").src = poster;
            // alert(bidang);
            for (var i = 0; i < listbidang.length; i++) {
              listbidang[i];
              var list_bidang = "<li>"+listbidang[i]+"</li>";
              $("#list_bidang").append(list_bidang);
            }
            for (var b = 0; b < listfasilitas.length; b++) {
              listfasilitas[b];
              var list_fasilitas = "<li>"+listfasilitas[b]+"</li>";
              $("#list_fasilitas").append(list_fasilitas);
            }
          });
          $(document).on("click", ".btn_choose_packagee", function () {
            // var list_id = $(this).data('list_id');
            var list_id = $("#txt_id_paket").val();
            // alert(list_id);
            $("#login_modal").modal('show');
            $("#desc_package_modal").modal('hide');
            $("#kode").val(list_id);
            $("#alertloginfirstpackage").css('display','block');
          });
      });
  </script>
  <script type="text/javascript">
                                $(document).ready(function(){

                                    // setInterval(function(){
                                    //     var code = "<?php echo $this->session->userdata('code');?>";

                                    //     if (code == "101") {
                                    //         $("#login_modal").
                                    //     }
                                    //     console.warn(code);
                                    // },500);
                                    // $("#loginnih").click(function(){
                                    //     var usernameemail = $("#emaill").val();
                                    //     var passwordlogin = $("#ms-form-pass").val();
                                    //     var userutclogin = $("#user_utc").val();
                                    //     var bapukslogin = $("#bapuks").val();
                                        
                                    //     $.ajax({
                                    //         url: 'https://classmiles.com/Rest/aksi_login',
                                    //         type: 'POST',
                                    //         data: {
                                    //             email: usernameemail,
                                    //             kata_sandi: passwordlogin,
                                    //             bapuks: bapukslogin,
                                    //             user_utc: userutclogin
                                    //         },
                                    //         success: function(response)
                                    //         {   
                                    //             if (response.code == 104) {
                                    //                 var alert = "<div class='alert alert-danger' style='display: block'><a href='#' class='close' data-dismiss='alert' aria-label='close' >&times;</a>"+response.message+"</div>";
                                    //                 $("#kotakalert").html("");
                                    //                 $("#kotakalert").html(alert);
                                    //             } 
                                    //             else if (response.code == 102) {
                                    //                 var load = "<?php echo $this->lang->line('youremail3')?>";
                                    //                 var load2 = "<?php echo $this->lang->line('here')?>";
                                    //                 var load3 = "<?php echo $this->lang->line('emailresend')?>";

                                    //                 var link = load+"<a href='https://classmiles.com/Master/resendEmaill'"+load2+"</a>"+load3;
                                    //                 var alert = "<div class='alert alert-danger' style='display: block'><a href='#' class='close' data-dismiss='alert' aria-label='close' >&times;</a>"+link+"</div>";
                                    //                 $("#kotakalert").html("");
                                    //                 $("#kotakalert").html(alert);
                                    //                 // $load."<a href=".base_url('/Master/resendEmaill')."> ".$load2."</a> ".$load3
                                    //                 // window.location.replace('/Subjects');
                                    //             }
                                    //             else if (response.code == 100) {
                                    //                 var alert = "<div class='alert alert-success' style='display: block'><a href='#' class='close' data-dismiss='alert' aria-label='close' >&times;</a>"+response.message+"</div>";
                                    //                 $("#kotakalert").html("");
                                    //                 window.location.replace('/');
                                    //             } 
                                    //             else if (response.code == 200) {                      
                                    //                 window.location.replace('https://classmiles.com'+response.link);
                                    //             } 
                                    //             else if (response.code == 203) { 
                                    //                 // $load = "<?php echo $this->lang->line('completeprofile');?>";    
                                    //                 // $rets['mes_alert'] ='warning';
                                    //                 // $rets['mes_display'] ='block';
                                    //                 // $rets['mes_message'] =$load;
                                    //                 // $this->session->set_flashdata('rets',$rets);
                                    //                 window.location.replace('https://classmiles.com'+response.link);
                                    //             }
                                    //             else if (response.code == 204) {                      
                                    //                 window.location.replace('https://classmiles.com'+response.link);
                                    //             }
                                    //             else if (response.code == 205) {                      
                                    //                 window.location.replace('https://classmiles.com'+response.link);
                                    //             }
                                    //             else if (response.code == 300) {                      
                                    //                 window.location.replace('https://classmiles.com'+response.link);
                                    //             }
                                    //             else if (response.code == 400) {                      
                                    //                 window.location.replace('https://classmiles.com'+response.link);
                                    //             }
                                    //             else if (response.code == 500) {                      
                                    //                 window.location.replace('https://classmiles.com'+response.link);
                                    //             }
                                    //         }
                                    //     }); 
                                    // });
                                });
  </script>
  <script type="text/javascript">
    var user_utc = new Date().getTimezoneOffset();
    user_utc = -1 * user_utc;
    $("#user_utc").val(user_utc);
  </script>
  <script>startApp();</script>
  <script type="text/javascript">
    function cycleBackgrounds(){var index=0;$imageEls=$('.slidee');setInterval(function(){index=index+1<$imageEls.length?index+1:0;$imageEls.eq(index).addClass('show');$imageEls.eq(index-1).removeClass('show');},3000);};$(function(){cycleBackgrounds();});
  </script>
  <script type="text/javascript">
    $("#slide_potrait > div:gt(0)").hide();
      setInterval(function() { 
        $('#slide_potrait > div:first')
          .fadeOut(1000)
          .next()
          .fadeIn(1000)
          .end()
          .appendTo('#slide_potrait');
      },  3000);
    $("#slide_landscape > div:gt(0)").hide();
      setInterval(function() { 
        $('#slide_landscape > div:first')
          .fadeOut(1000)
          .next()
          .fadeIn(1000)
          .end()
          .appendTo('#slide_landscape');
      },  3000);
  </script>
  <script type="text/javascript">
    $("#box_front_package").empty();
    $.ajax({
          url: 'https://classmiles.com/air/v1/listdata_Program',
          type: 'POST',
          success: function(response)
          {   
            var posteronly ="";
            var kotakpaket = "";
            var kotakpaket2 = "";   
            var cek_aktif =""; 
            var indikator ="";
            for (var i = 0; i < response.data.length; i++) {
              var a = JSON.stringify(response);
              list_id      = response['data'][i]['list_id'];
            var name      = response['data'][i]['name'];
            var poster      = response['data'][i]['poster'];
            var description      = response['data'][i]['description'];
            var bidang      = response['data'][i]['bidang'];
            
            
            var fasilitas      = response['data'][i]['fasilitas'];
            var price_plan      = response['data'][i]['price'];
            var price =price_plan.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            var alt_img         ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
            if (i==0) {
              cek_aktif="active";
            }
            else{
              cek_aktif="";
            }
            // indikator="<li data-target='#sliderposter' data-slide-to='"+i+"' class=''></li>";
            // $("#indicator_poster").append(indikator);
            // posteronly="<div class='show_modal_desc item ' data-desc='"+description+"' data-name='"+name+"' data-bidang='"+bidang+"' data-fasilitas='"+fasilitas+"' data-poster='"+poster+"' data-price='"+price+"' data-id_paket='"+list_id+"'>"+
            //     "<img src='"+poster+"' alt='Los Angeles' style='width:100%;'>"+
            //   "</div>";
            indikator="<li data-target='#sliderposter' data-slide-to='"+i+"' class='"+cek_aktif+"''></li>";
            // $("#indicator_poster").append(indikator);
            posteronly="<div class='show_modal_desc item "+cek_aktif+"' data-desc='"+description+"' data-name='"+name+"' data-bidang='"+bidang+"' data-fasilitas='"+fasilitas+"' data-poster='"+poster+"' data-price='"+price+"' data-id_paket='"+list_id+"'>"+
                "<img src='"+poster+"' alt='Los Angeles' style='width:100%;'>"+
              "</div>";
              var tambahan ="<div class='item'>"+
              "<img src='https://cdn.classmiles.com/sccontent/banner/Banner_IbnKhaldun.png' alt='Los Angeles' style='width:100%;'>"+
            "</div>"; 
            // $("#poster_only").append(tambahan);
            $("#poster_only").append(posteronly);
                if (i > 4) {
                  $("#controllernya-package").css('display','block');
                      kotakpaket2 += "<div class='col-sm-3 col-md-3 col-xs-3 btn_choose_package' style='cursor: pointer;' data-list_id='"+list_id+"a' title='"+name+" - "+description+"'>"+
                      "<div class='col-item'>"+
                          "<div class='photo' style='padding:3%'; '>"+
                              "<img src='"+poster+"' class='img-fluid img-responsive' onerror="+alt_img+" style='height: 200px; width: auto;' />"+
                          "</div>"+
                          "<div class='info'>"+
                              "<div class='row'>"+
                                  "<div class='price col-md-12 col-sm-12 col-xs-12' style='line-height:80%'>"+
                                      "<h5>PAKET "+name+"</h5>"+
                                      "<p style='font-size:13px;'>"+description+"</p>"+
                                  "</div>"+
                                  "<div class='col-md-12 col-sm-12 col-xs-12 price-text-color' style='bottom:0; font-size:15px; padding:0' align='center'>"+
                                  "<b>Rp. "+price+"</b>"+
                                  "</div>"+
                              "</div>"+
                              "<div class='separator clear-left'>"+
                                "<center>"+
                                    "<div style='margin-top: 3%;'>"+
                                      "<i class='zmdi zmdi-square-right zmdi-hc-fw'></i><a class='ml-1 ' title='Klik untuk join' >Beli Paket</a>"+
                                    "</div>"+
                                "</center>"+
                              "</div>"+
                              "<div class='clearfix'>"+
                              "</div>"+
                          "</div>"+
                      "</div>"+
                  "</div>";
                }
                else
                {

                  kotakpaket += "<div class='col-sm-3 col-md-3 col-xs-3 btn_choose_packagee' style='cursor: pointer;' title='"+name+" - "+description+"' data-list_id='"+list_id+"'>"+
                      "<div class='col-item'>"+
                          "<div class='photo' style='padding:3%'; '>"+
                              "<img src='"+poster+"' class='img-fluid img-responsive ' onerror="+alt_img+" style='height: 200px; width: auto;' />"+
                          "</div>"+
                          "<div class='info'>"+
                              "<div class='row'>"+
                                  "<div class='price col-md-12 col-sm-12 col-xs-12' style='line-height:80%'>"+
                                      "<h5>PAKET "+name+"</h5>"+
                                      "<p style='font-size:13px;'>"+description+"</p>"+
                                  "</div>"+
                                  "<div class='col-md-12 col-sm-12 col-xs-12 price-text-color' style='bottom:0; font-size:15px; padding:0' align='center'>"+
                                  "<b>Rp. "+price+"</b>"+
                                  "</div>"+
                              "</div>"+
                              "<div class='separator clear-left'>"+
                                "<center>"+
                                    "<div style='margin-top: 3%;'>"+
                                      "<i class='zmdi zmdi-square-right zmdi-hc-fw'></i><a class='ml-1 ' title='Klik untuk join'>Beli Paket</a>"+
                                    "</div>"+
                                "</center>"+
                              "</div>"+
                              "<div class='clearfix'>"+
                              "</div>"+
                          "</div>"+
                      "</div>"+
                  "</div>";
                }
          }

            // $("#box_front_package").append(kotakpaket);
            // $("#box_back_package").append(kotakpaket2);
          
          }
      });
  </script>
  <script type="text/javascript">
    var input = document.getElementById("ms-form-pass");
    input.addEventListener("keyup", function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
            document.getElementById("loginnih").click();
        }
    });
    $(document).on("click", "#loginnih", function () {
      $("#alertemailwrong").css('display', 'none');
      $("#alertfailedregister").css('display', 'none');
      $("#alertverifikasisuccess").css('display', 'none');
      $("#alertverifikasiemail").css('display', 'none');
      $("#alertfailedsendemail").css('display', 'none');
      $("#alertsuccessendemail").css('display', 'none');
      $("#alertverifikasifailed").css('display', 'none');
      $("#alertverifikasitutor").css('display', 'none');
      $("#alertwrong").css('display', 'none');
      var email = $("#emaill").val();
      var kata_sandi = $("#ms-form-pass").val();
      var user_utc = $("#user_utc").val();
      var bapuks =  $("#bapuks").val();
      var channel_id = $("#channel_id").val();
      $.ajax({
            url: '<?php echo base_url(); ?>Master/aksi_login_new',
            type: 'POST',
            data: {                
                email: email,
                kata_sandi : kata_sandi,
                user_utc : user_utc,
                bapuks: bapuks,
                channel_id : channel_id
            },            
            success: function(data)
            {         
              result = JSON.parse(data); 
              var login_code = result['login_code'];
              var redirect = result['redirect'];
              if (login_code == 1) {
                window.location.href = "<?php base_url();?>"+redirect;
              }
              else if (login_code == -2) {
                $("#login_modal").modal('show');
                $("#alertemailwrong").css('display', 'none');
                $("#alertfailedregister").css('display', 'none');
                $("#alertverifikasisuccess").css('display', 'none');
                $("#alertverifikasiemail").css('display', 'block');
                $("#alertfailedsendemail").css('display', 'none');
                $("#alertsuccessendemail").css('display', 'none');
                $("#alertverifikasifailed").css('display', 'none');
                $("#alertverifikasitutor").css('display', 'none');
                $("#alertwrong").css('display', 'none');
              }
              else if(login_code == 0){
                $("#login_modal").modal('show');
                $("#alertemailwrong").css('display', 'none');
                $("#alertfailedregister").css('display', 'none');
                $("#alertverifikasisuccess").css('display', 'none');
                $("#alertverifikasiemail").css('display', 'none');
                $("#alertfailedsendemail").css('display', 'none');
                $("#alertsuccessendemail").css('display', 'none');
                $("#alertverifikasifailed").css('display', 'none');
                $("#alertverifikasitutor").css('display', 'none');
                $("#alertwrong").css('display', 'block');
              }
              else{
                $("#login_modal").modal('show');
                $("#alertemailwrong").css('display', 'none');
                $("#alertfailedregister").css('display', 'none');
                $("#alertverifikasisuccess").css('display', 'none');
                $("#alertverifikasiemail").css('display', 'none');
                $("#alertfailedsendemail").css('display', 'none');
                $("#alertsuccessendemail").css('display', 'none');
                $("#alertverifikasifailed").css('display', 'none');
                $("#alertverifikasitutor").css('display', 'none');
                $("#alertwrong").css('display', 'block');
              }
            }
        });
    });
  </script>
</html>
