<style type="text/css">
    body{
         margin: 0; height: 100%; overflow: hidden;
    }
</style>
<div class="modal fade fade" style="" id="modal_loading" tabindex="-1" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document" style="background:rgba(0,00,00,0.0);">   
        <div style="margin-top: 50%;">
            <center>
               <div class="preloader pl-xxl pls-teal">
                    <svg class="pl-circular" viewBox="25 25 50 50">
                        <circle class="plc-path" cx="50" cy="50" r="20" />
                    </svg>
                </div><br>
               <p style="color: white;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
            </center>
        </div>
    </div>
</div>
<header id="header" class="clearfix" data-current-skin="blue" style="background-color: #ea2127;">
    <div class="" style="padding-left: 5%; padding-bottom: 1%;">
        <?php $this->load->view('mobile/inc/navbar'); ?>    
    </div>

    <div class="header-inner" style="color: white; padding-top: 0; margin-top: 0; padding-bottom: 0 " align="center">
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="<?php echo base_url(); ?>">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_home_white.png" style="width: 20px; margin-top:-5%;">
            <!-- <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Account White.svg" style="width: 20px;  height: auto;"> -->
            <br><label style="font-size: 9px; text-transform: uppercase; margin: 0"><?php echo $this->lang->line('home'); ?></label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="<?php echo base_url();?>/MyClass">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/myclassputih-02.png" style="width: 20px; margin-top: -5%;">
            <br><label style="font-size: 9px; text-transform: uppercase; " ><?php echo $this->lang->line('myclass'); ?></label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="<?php echo base_url(); ?>Subjects">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Tutor White.svg" style="width: 20px;  margin-top: -5%;  height: auto;">
            <br><label style="font-size: 9px; text-transform: uppercase;">TUTOR</label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="#">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_quiz_white.png" style="width: 20px;  margin-top: -5%; height: auto;">
            <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('quiz');?>Quiz</label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: orange;" href="#t">
            <i style="font-size: 20px;" class="zmdi zmdi-menu zmdi-hc-fw"></i>
            <!-- <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Account White.svg" style="width: 20px;  height: auto;"> -->
            <br><label style="font-size: 9px; text-transform: uppercase;">Others<?php echo $this->lang->line('tab_other');?></label></a>
        </button>                          
    </div>
</header>
<div id="menu_regis_anak" style="display: none;">
    <?php
    $this->load->view('mobile/kids/register');
    ?>
</div>
<?php if(isset($mes_alert)){ ?>
<div class="alert alert-<?php echo $mes_alert; ?>" style="display: <?php echo $mes_display; ?>">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $mes_message; ?>
</div>
<?php } ?>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->

<div id="main" data-layout="layout-1" style="margin-top: 0;">
    <div class=" modal fade" data-modal-color="blue" id="tambah_ortu" tabindex="-1"  role="dialog" style="margin-top: 150px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <center>
                    <div id='' class="modal-body" style="" ><br>
                          <label for='nama'>Silakan masukkan email Orang Tua dibawah ini</label>
                           <input type="email" name="email" id="emailOrtu" required class="input-sm form-control fg-input">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btnOkeOrtu btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                    </div>
                </center>
            </div>
        </div>
    </div>
    <div class='modal fade' tabindex='-1'  role='dialog' aria-hidden='true' id='detail_kid'>
        <div class='modal-dialog modal-sm'>
            <div class='modal-content' style='background-color:#d6d6d6'>
                <div class='header-inner' style='background-color: #008080;'>
                    <h4 style='color:white; padding-left:5%;  margin: 0;'><i style='margin-right:5%;' data-dismiss='modal' class='zmdi zmdi-arrow-left'></i> My Kids</h4>
                </div>
                <div id='view_image' align='center' style='margin: 5%; padding-top:5%;'>
                  <img id="user_image_kid" class='img-circle' src='' style='width: 30%; height: auto;'>
                  <br><br>
                  <label id="user_name_kid" style='color:black;'></label>
                </div>
                <div id='view_detail_ortu' style='margin: 2%; padding-bottom: 5%; padding-left: 5%; padding-right: 5%; background-color: white; border-radius: 10px; '>
                  <label style='font-size: 11px; color: gray;'>List Orang Tua :</label><br>
                  <div id="kotak_ortu"></div>
                  <br>
                  <button type='' name='addortukid_id' id='addortu_kid_id' kid_id='' class='btn_addortu btn btn-success btn-block'>Tambah Orang Tua</button>
                </div>
                <div id='view_detail' style='margin: 2%; padding-bottom: 5%; padding-left: 5%; padding-right: 5%; background-color: white; border-radius: 10px; '>
                  <label style='font-size: 11px; color: gray;'>Kids Username :</label>
                  <h6 id="username_kid" style='margin: 0;'></h6>
                  <label style='font-size: 11px; color: gray;'>Tempat, tanggal lahir :</label>
                  <h6 id="user_birth_kid" style='margin: 0;'></h5>
                  <label style='font-size: 11px; color: gray;'>Jenis Kelamin</label>
                  <h6 id="user_gender_kid" style='margin: 0;'></h5>
                  <br>
                </div>

                <div id='view_sekolah' style='margin-top: 0; margin: 2%; padding-bottom: 5%; padding-left: 5%; padding-right: 5%; background-color: white; border-radius: 10px; '>
                  <h4 style='padding-top: 3%; color: black;'>Detail Sekolah</h4>
                  <div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:black; border-bottom-style: solid; margin-bottom:10px;'></div>
                  <label style='font-size: 11px; color: gray;'>Jenjang :</label>
                  <h6 id="jenjang_kid" style='padding-bottom: 2%; margin: 0;'></h6>
                  <label style='font-size: 11px; color: gray;'>Propinsi :</label>
                  <h6 id="provinsi_school_kid" style='padding-bottom: 2%; margin: 0;'></h6>
                  <label style='font-size: 11px; color: gray;'>Kota :</label>
                  <h6 id="kabupaten_school_kid" style='padding-bottom: 2%; margin: 0;'></h6>
                  <label style='font-size: 11px; color: gray;'>Kecamatan :</label>
                  <h6 id="kecamatan_school_kid" style='padding-bottom: 2%; margin: 0;'></h6>
                  <label style='font-size: 11px; color: gray;'>Nama Sekolah :</label>
                  <h6 id="name_school_kid" style='padding-bottom: 2%; margin: 0;'></h6>
                  <label style='font-size: 11px; color: gray;'>Alamat Sekolah :</label>
                  <h6 id="address_school_kid" style='padding-bottom: 2%; margin: 0;'></h6>
                </div>
                <div id='ganti_password' align='center' Class='' style='padding:2%;'>
                    <div id='kid_id+"' align='center' Class='ganti_password_kids btn-success' style='padding:2%;'>
                        <label style='text-transform:uppercase margin:0;'>Ganti Password</label>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="menu_about" class="" style="color: #d6d6d6; display: block; ">
                <div id="view_akun" class=' col-xs-12' style='background-color: #d6d6d6'>
                    <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                        <div class="m-t-5 m-b-5"><img class="img-circle m-r-5" style="width: 20px; height: 20px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_account_circle_grey.png">
                            <label style="color: black; margin-top: 2%;"><?php echo $this->lang->line('tab_account'); ?></label>
                            <a href="<?php echo base_url('Profile-Account'); ?>"><label id="tambah_anaka" class="pull-right" style="font-size: 20px; color: black; margin-top: 1%; margin-right: 2%;"><img class="pull-right img-circle" style="position: relative; width: 20px; height: 20px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_account_edit_black.png"></label></a>
                        </div>
                        <div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:black; border-bottom-style: solid; margin-bottom:10px;'>
                        </div>
                        <div class='col-xs-3 col-sm-1 m-b-10 ' style='padding:0; width:50px;'>
                            <img class='img-circle' src='<?php echo $this->session->userdata('user_image');?>' onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" width='40px' height='40px' style='position: absolute; '>
                            <img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; height: 15px; width: 30px; bottom: 0; right: 0;'>
                        </div>
                        <div class='col-xs-9 m-b-10 ' style='padding:0; width:44% height:70px;' >
                            <h6 class='' style='margin-top: 0; font-size:11px; '><?php 
                                if ($this->session->userdata('user_name') == "") {
                                    echo $this->lang->line('nousername');
                                    }
                                    echo $this->session->userdata('user_name'); 
                                ?>
                            </h6>
                           <h6 class='' style='font-size:11px; '><?php 
                                $jenjang_id = $this->session->userdata('jenjang_id');
                                $get_jenjang = $this->db->query("SELECT jenjang_name, jenjang_level FROM `master_jenjang` where jenjang_id='$jenjang_id'")->row_array();
                                if ($this->session->userdata('umur') == "") {
                                    echo $this->lang->line('noage');

                                }
                                if ($profile['jenjang_level'] != 0) {
                                  echo $this->session->userdata('umur').' '.$this->lang->line('yearsold').' - '.$get_jenjang['jenjang_level'].' '.$get_jenjang['jenjang_name'];
                                }
                                else{
                                echo $this->session->userdata('umur').' '.$this->lang->line('yearsold').' - '.$get_jenjang['jenjang_name'];    
                                }
                                 ?> 
                            </h6>
                        </div>
                    </div>
                </div>

                <?php 
                $status_user = $this->session->userdata('status_user'); 
                if ($status_user =="umum") {
                    ?>
                    <div id="view_anaksaya" class=' col-xs-12' style='background-color: #d6d6d6'>
                        <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%; padding-bottom: 2%;' class=' col-xs-12'>
                            <div class="m-t-5 m-b-5"><img class="img-circle m-r-5" style="width: 20px; height: 20px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_kids_icon.png">
                                <label style="color: black; margin-top: 2%;"><?php echo $this->lang->line('mykids'); ?></label>
                                <a href="<?php echo base_url();?>Kids/Registration"><label id="tambah_anaka" class="pull-right" style="font-size: 20px; color: black; margin-top: 1%; margin-right: 2%;"><i class="zmdi zmdi-plus "></i></label></a>
                            </div>
                            <div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:black; border-bottom-style: solid; margin-bottom:10px;'>
                            </div>
                            <div id="kotak_list_anak">
                            </div>
                            
                        </div>
                    </div>
                    
                    <?php
                    }
                ?>
                
                <!-- <div id="view_invoice" class='col-xs-12' style='background-color: #d6d6d6'>
                    <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                        <div class="m-t-5 m-b-5"><i style="font-size: 15px;" class="tm-icon zmdi zmdi-shopping-cart"></i>
                            <label style="color: black; margin: 0; margin-left: 13px;"><?php echo $this->lang->line('listinvoice');?></label>
                        </div>
                    </div>
                </div> -->
                <div id="view_review" class='col-xs-12' style='background-color: #d6d6d6'>
                    <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                        <div class="m-t-5 m-b-5"><i class="m-r-10 m-l-5 zmdi zmdi-comment" style="color: #d6d6d6;"></i>
                            <label style="color: black; margin: 0;">Review<?php echo $this->lang->line('listreview');?></label>
                        </div>
                    </div>
                </div>
                <div id="view_syaratketentuan" class='col-xs-12' style='background-color: #d6d6d6'>
                    <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                         <a href="<?php echo base_url();?>syarat-ketentuan" target="_blank">
                            <div class="m-t-5 m-b-5"><img class=" m-r-5" style="width: 20px; height: 20px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_information_variant_grey.png">
                               <label style="color: black; margin: 0;"><?php echo $this->lang->line('bannersyarat')?></label>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- <div id="btn_logout" class='col-xs-12' style=''>
                    <div style='width:110%; margin-left:-5%; background-color:white;' class=' col-xs-12'>
                        <div class="m-b-10 m-t-10">
                            <a style="color: black" href="<?php echo base_url('/logout'); ?>"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/keluar1-01.svg" style="margin-right: 1%; height: 23px; width: 23px;"><label style="color: black; margin: 0;"><?php echo $this->lang->line('logout')?></label></a>
                        </div>
                    </div>
                </div> -->
                <div id="logout" class='col-xs-12' style='background-color: #d6d6d6'>
                    <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                         <a href="<?php echo base_url('/logout'); ?>">
                            <div class="m-t-5 m-b-5"><img class=" m-r-5" style="width: 20px; height: 20px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/keluar1-01.svg">
                               <label style="color: black; margin: 0;"><?php echo $this->lang->line('logout')?></label>
                            </div>
                        </a>
                    </div>
                </div>
    </div>
    <div id="kotak_detail_anak" style="display: none;">
    </div>
    <div id="menu_complain" style="display: none;">
    	<?php
    	$this->load->view('mobile/inc/complain');
    	?>
    </div>
    <div id="content">
        <div class="" style="position:relative;">
            <?php if(isset($mes_alert)){ ?>
            <div class="alert alert-<?php echo $mes_alert; ?>" style="display: <?php echo $mes_display; ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $mes_message; ?>
            </div>
            <?php } ?>
            
            <div class="block-header">
                <!-- <h2><?php echo $this->lang->line('profil'); ?></h2> -->


                <!-- <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_about'); ?></li>
                        </ol>
                    </li>
                </ul> -->
            </div><!-- akhir block header --> 
            <div class="alert alert-success" style="display: none; ?>" id="alertsuccessupdate">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                Foto akun berhasil diubah
            </div>
            <div class="alert alert-danger" style="display: none; ?>" id="alertsizebesar">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                Upload gagal, Mohon ukuran file tidak melebihi 2 MB.
            </div>
            <?php
                $iduser = $this->session->userdata('id_user');            
                $get_image = $this->db->query("SELECT * FROM tbl_user WHERE id_user='".$iduser."'")->row_array();                
            ?>
            


            <!-- <div class="row">
                <div class="col-sm-2"><img class="img-circle" id="avatar-edit-img" height="128" data-src="default.jpg"  data-holder-rendered="true" style="width: 140px; height: 140px;" src="default.jpg"/></div>
                <div class="col-sm-10"><a type="button" class="btn btn-primary" id="change-pic">Change Image</a>
                <div id="changePic" class="" style="display:none">
                    <form id="cropimage" method="post" enctype="multipart/form-data" action="profile.php">
                        <label>Upload your image</label><input type="file" name="photoimg" id="photoimg" />
                        <input type="hidden" name="hdn-profile-id" id="hdn-profile-id" value="1" />
                        <input type="hidden" name="hdn-x1-axis" id="hdn-x1-axis" value="" />
                        <input type="hidden" name="hdn-y1-axis" id="hdn-y1-axis" value="" />
                        <input type="hidden" name="hdn-x2-axis" value="" id="hdn-x2-axis" />
                        <input type="hidden" name="hdn-y2-axis" value="" id="hdn-y2-axis" />
                        <input type="hidden" name="hdn-thumb-width" id="hdn-thumb-width" value="" />
                        <input type="hidden" name="hdn-thumb-height" id="hdn-thumb-height" value="" />
                        <input type="hidden" name="action" value="" id="action" />
                        <input type="hidden" name="image_name" value="" id="image_name" />
                        
                        <div id='preview-avatar-profile'>
                    </div>
                    <div id="thumbs" style="padding:5px; width:600p"></div>
                    </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btn-crop" class="btn btn-primary">Crop & Save</button>
                </div>
            </div> -->
        </div><!-- akhir container -->
    </div>
                
</div>



<script type="text/javascript">
	$("#view_review").click(function(){
		$("#menu_about_").css('display','none');
		$("#menu_complain_").css('display','block');
	});
	$("#back_menu_about").click(function(){
		$("#menu_about").css('display','block');
        $("#header").css('display','block');
		$("#menu_regis_anak").css('display','none');
	});
	$("#tambah_anak").click(function(){
		$("#menu_about").css('display','none');
        $("#header").css('display','none');
		$("#menu_regis_anak").css('display','block');
	});
    $(".btn_addortu").click(function(){
        $("#detail_kid").modal('hide');
        $("#tambah_ortu").modal('show');
    })
    $(".btnOkeOrtu").click(function(){
      var emailOrtu = $("#emailOrtu").val();
      var id_kid = $("#addortu_kid_id").attr('kid_id');
      // alert(id_kid);
      // return false;
        $.ajax({
            url: '<?php echo base_url(); ?>Rest/addParentKids/',
            type: 'POST',
            data: {                
                email : emailOrtu,
                kid_id: id_kid
            },      
            success: function(data)
            {              
                if (data['status']) {
                  swal("Success add Parents", "", "success")

                  // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Success Join to Channel');
                  setTimeout(function(){
                      location.reload();
                  },3000);
                }
                else if (data['code'] == -101) {
                  swal("Email tersebut sudah menjadi parrent Anda", "", "info")
                }
                else{
                  swal("error", "", "error")

                  // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Success Join to Channel');
                  setTimeout(function(){
                      location.reload();
                  },3000);
                }
            }
        });
    });


    $("#kotak_list_anak").empty();
    var access_token_jwt = "<?php echo $this->session->userdata('access_token_jwt')?>";
    var parent_id  = "<?php echo $this->session->userdata('parent_id');?>";
    var id_user  = "<?php echo $this->session->userdata('id_user');?>";
    // alert(access_token_jwt);
            $.ajax({ 
                url: '<?php echo base_url(); ?>Rest/myListKids/access_token/'+access_token_jwt,
                type: 'POST',
                data: {

                    parent_id: parent_id,
                    user_utc: user_utc,
                    user_device: 'mobile'
                },
                success: function(response)
                {
                console.warn("mykidslist"+response['status']);
                    for (var i = 0; i < response.data.length; i++) {
                                
                                var username = response['data'][i]['username'];
                                var user_name = response['data'][i]['user_name'];
                                var user_image = response['data'][i]['user_image'];
                                var user_gender = response['data'][i]['user_gender'];
                                var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                                var user_birthdate = response['data'][i]['user_birthdate'];
                                user_birthdate = moment(user_birthdate).format('DD MMMM YYYY');
                                var user_birthplace = response['data'][i]['user_birthplace'];
                                var user_age = response['data'][i]['user_age'];
                                var jenjang_name = response['data'][i]['jenjang_name'];
                                var jenjang_level = response['data'][i]['jenjang_level'];
                                var first_name = response['data'][i]['first_name'];
                                var last_name = response['data'][i]['last_name'];
                                var kid_id = response['data'][i]['kid_id'];
                                var parent_id = response['data'][i]['parent_id'];
                                var school_id = response['data'][i]['school_id'];
                                var school_name = response['data'][i]['school_name'];
                                var school_address = response['data'][i]['school_address'];
                                var provinsi = response['data'][i]['provinsi'];
                                var kabupaten = response['data'][i]['kabupaten'];
                                var kecamatan = response['data'][i]['kecamatan'];
                                var kelurahan = response['data'][i]['kelurahan'];


                                var school_id_temp = response['data'][i]['school_id_temp'];
                                var school_name_temp = response['data'][i]['school_name_temp'];
                                var school_address_temp = response['data'][i]['school_address_temp'];
                                var provinsi_temp = response['data'][i]['provinsi_temp'];
                                var kabupaten_temp = response['data'][i]['kabupaten_temp'];
                                var kecamatan_temp = response['data'][i]['kecamatan_temp'];
                                var kelurahan_temp = response['data'][i]['kelurahan_temp'];

                                if (school_id == null) {
                                	school_id = school_id_temp;
                                }
                                if (school_name == null) {
                                	school_name = school_name_temp;
                                }
                                if (school_address == null) {
                                	school_address = school_address_temp;
                                }
                                if (provinsi == null) {
                                	provinsi = provinsi_temp;
                                }
                                if (kabupaten == null) {
                                	kabupaten = kabupaten_temp;
                                }
                                if (kecamatan == null) {
                                	kecamatan = kecamatan_temp;
                                }
                                if (kelurahan == null) {
                                	kelurahan = kelurahan_temp;
                                }

                                var kotak_anak = "<div id='kotak_"+kid_id+"' class='col-xs-12 klik_detail' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:#d6d6d6; border-bottom-style: solid; margin-bottom: 2px; background-color: #edecec'>"+
                                    "<div class=' col-xs-2 col-sm-1 m-b-5 ' style=' padding: 0; width: 40px;'>"+
                                        "<a id='"+kid_id+"' class=''><img class=' img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+user_image+"' onerror="+alt_img+"  width='30px' height='30px' style='padding:0; margin-top: 9px;'></a>"+
                                    "</div>"+
                                    "<div class='col-xs-9 m-b-10 ' style='padding: 0; width:44% height:70px; ' >"+
                                        "<h6 class='' style='margin-bottom: 2px;  font-size:11px; '>"+user_name+"</h6>"+
                                        "<h6 class='' style='margin: 0; font-size:11px; '>"+jenjang_name+" "+jenjang_level+"</h6>"+
                                    "</div>"+
                                "</div>"+

                                "<div class='modal fade' tabindex='-1'  role='dialog' aria-hidden='true' id='gantipassword_"+kid_id+"' style='display:none;'>"+
                					"<div style='margin-top:50%;' class='modal-dialog modal-sm'>"+
	                                    "<div class='modal-content' style='background-color:#d6d6d6'>"+
		                                    "<div id='kotak_gantipassword'Class='' style='padding:2%;'>"+
    		                         				"<div Class='bgm-white' style='padding:2%;'>"+
                                                    "<form action='<?php echo base_url('/master/EditPasswordKids') ?>' method='post'>"+
                                                        "<div class='input-group fg-float'>"+
                                                        "<input name='id_user' id='id_user_kids_"+kid_id+"' type='hidden' class='input-sm form-control fg-input' value='"+kid_id+"'>"+
                                                            "<div class='c-black col-xs-12'><?php echo $this->lang->line('new_pass') ;?> </div>"+
                                                            "<div class='col-xs-12 fg-line'>"+
                                                                "<input placeholder='<?php echo $this->lang->line('new_pass'); ?>' type='password' name='newpass_"+kid_id+"' id='newpass_"+kid_id+"' class='input-sm form-control fg-input'>"+
                                                            "</div>"+
                                                        "</div>"+
                                                        "<br>"+
                                                        "<div class='input-group fg-float'>"+
                                                            "<div class='c-black col-xs-12'><?php echo $this->lang->line('confirm_pass') ;?> </div>"+
                                                            "<div class='col-xs-12 fg-line'>"+
                                                                "<input placeholder='<?php echo $this->lang->line('confirm_pass'); ?>' type='password' name='conpass_"+kid_id+"' id='conpass_"+kid_id+"' class='check_pass input-sm form-control fg-input'>"+
                                                            "</div>"+
                                                            "<small class='help-block' id='divCheckPasswordMatch'></small>"+
                                                        "</div>"+
                                                        "<br>"+
                                                        "<button type='submit' name='simpanedit_"+kid_id+"' id='simpanedit_"+kid_id+"' class=' btn_gantipass btn btn-success btn-block'><?php echo $this->lang->line('button_save'); ?></button>"+
    			                                    "</div>"+
                                                    "</form>"+
		                                    "</div>"+
	                                    "</div>"+
                                    "</div>"+
                                "</div>";

                                
                                $("#kotak_list_anak").append(kotak_anak);
                                // $("#kotak_detail_anak").append(detail_anak);


                    }
                   
		            $(document).on("click", ".ganti_password_kids", function () {
		                // alert(this.id);
		                var id_kids = this.id;
		                $("#gantipassword_"+id_kids).modal('show');
		                $("#detail_"+id_kids).modal('hide');

		            });
                }
            });

            $(document).on("click", ".klik_detail", function () {
                $("#kotak_ortu").empty();
                var kid_id = this.id.substr(6);
                // alert (kid_id);
                
                $("#detail_kid").modal("show");
                $.ajax({
                    url: '<?php echo base_url(); ?>Rest/detailKid/access_token/'+access_token_jwt,
                    type: 'POST',
                    data: {

                        kid_id: kid_id,
                        parent_id: parent_id,
                        user_utc: user_utc,
                        user_device: 'mobile'
                    },
                    success: function(response)
                    {
                    // console.warn("detailkids"+response['status']);
                        for (var i = 0; i < response.data.length; i++) {
                                    
                                    var username = response['data'][i]['username'];
                                    var user_name = response['data'][i]['user_name'];
                                    var user_image = response['data'][i]['user_image'];
                                    var user_gender = response['data'][i]['user_gender'];
                                    var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                                    var user_birthdate = response['data'][i]['user_birthdate'];
                                    user_birthdate = moment(user_birthdate).format('DD MMMM YYYY');
                                    var user_birthplace = response['data'][i]['user_birthplace'];
                                    var user_age = response['data'][i]['user_age'];
                                    var jenjang_name = response['data'][i]['jenjang_name'];
                                    var jenjang_level = response['data'][i]['jenjang_level'];
                                    var first_name = response['data'][i]['first_name'];
                                    var last_name = response['data'][i]['last_name'];
                                    var kid_id = response['data'][i]['kid_id'];
                                    var parent_id = response['data'][i]['parent_id'];
                                    var school_id = response['data'][i]['school_id'];
                                    var school_name = response['data'][i]['school_name'];
                                    var school_address = response['data'][i]['school_address'];
                                    var provinsi = response['data'][i]['provinsi'];
                                    var kabupaten = response['data'][i]['kabupaten'];
                                    var kecamatan = response['data'][i]['kecamatan'];
                                    var kelurahan = response['data'][i]['kelurahan'];


                                    var school_id_temp = response['data'][i]['school_id_temp'];
                                    var school_name_temp = response['data'][i]['school_name_temp'];
                                    var school_address_temp = response['data'][i]['school_address_temp'];
                                    var provinsi_temp = response['data'][i]['provinsi_temp'];
                                    var kabupaten_temp = response['data'][i]['kabupaten_temp'];
                                    var kecamatan_temp = response['data'][i]['kecamatan_temp'];
                                    var kelurahan_temp = response['data'][i]['kelurahan_temp'];

                                    if (school_id == null) {
                                        school_id = school_id_temp;
                                    }
                                    if (school_name == null) {
                                        school_name = school_name_temp;
                                    }
                                    if (school_address == null) {
                                        school_address = school_address_temp;
                                    }
                                    if (provinsi == null) {
                                        provinsi = provinsi_temp;
                                    }
                                    if (kabupaten == null) {
                                        kabupaten = kabupaten_temp;
                                    }
                                    if (kecamatan == null) {
                                        kecamatan = kecamatan_temp;
                                    }
                                    if (kelurahan == null) {
                                        kelurahan = kelurahan_temp;
                                    }
                                    $("#addortu_kid_id").attr('kid_id',kid_id);
                                    $("#user_gender_kid").text(user_gender);
                                    $("#user_image_kid").attr('src','<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>'+user_image);
                                    $("#user_name_kid").text(user_name);
                                    $("#username_kid").text(username);
                                    $("#user_birth_kid").text(user_birthdate);
                                    $("#jenjang_kid").text(jenjang_level +" "+jenjang_name);
                                    $("#provinsi_school_kid").text(provinsi);
                                    $("#kabupaten_school_kid").text(kabupaten);
                                    $("#kecamatan_school_kid").text(kecamatan);
                                    $("#address_school_kid").text(school_address);
                                    $("#name_school_kid").text(school_name);

                        }
                       
                        $(document).on("click", ".ganti_password_kids", function () {
                            // alert(this.id);
                            var id_kids = this.id;
                            $("#gantipassword_"+id_kids).modal('show');
                            $("#detail_"+id_kids).modal('hide');

                        });
                    }
                });
                $.ajax({
                    url: '<?php echo base_url(); ?>Rest/listParentKids',
                    type: 'POST',
                    data: {

                        kid_id: kid_id
                    },
                    success: function(response)
                    {
                    console.warn("listparent"+response['status']);
                        for (var i = 0; i < response.data.length; i++) {
                                    
                                    var email = response['data'][i]['email'];
                                    var user_name = response['data'][i]['user_name'];
                                    var user_image = response['data'][i]['user_image'];
                                    var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                                    var kotak_ortu ="<div class='col-xs-1 m-r-5'>"+
                                                        "<img id='img_ortu' class='img-circle' style='width: 30px; height: 30px;'  onerror="+alt_img+"  src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+user_image+"'>"+
                                                        "</div>"+
                                                        "<div class='col-xs-10'>"+
                                                            "<label style='margin:0'>"+user_name+"</label><br>"+
                                                            "<label>"+email+"</label>"+
                                                        "</div>"+
                                                        "<br>";
                                                    $("#kotak_ortu").append(kotak_ortu);
                                    
                        }
                       
                        $(document).on("click", ".ganti_password_kids", function () {
                            // alert(this.id);
                            var id_kids = this.id;
                            $("#gantipassword_"+id_kids).modal('show');
                            $("#detail_"+id_kids).modal('hide');

                        });
                    }
                });
            });
            
        
</script>
<script type="text/javascript">
     /*$(document).on("click", ".btn_gantipass", function () {
        this.id;
        var id_user  = this.id;
        id_user = id_user.substr(11,10);
        var oldpass = $("#oldpass"+id_user).val();
        alert(oldpass);
        var password = $("#newpass"+id_user).val();
        var confirmPassword = $("#conpass"+id_user).val();
                       
         // alert(new_id_user);
            $('#id_user').val(id_user);
            $.ajax({
                url: '<?php echo base_url(); ?>Master/EditPasswordKids',
                type: 'POST',
                data: {
                    id_user : id_user,
                    oldpass : oldpass,
                    password : password,
                    confirmPassword : confirmPassword
                },               
                success: function(data){                    
                    // window.location.reload();
                }
            });

 
    });*/

    // $(document).ready(function () {
    //    $("#conpass").keyup(checkPasswordMatch);
    // });
</script>


<script type="text/javascript">

        jQuery(document).ready(function(){
        
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        var code = null;
        var cekk = setInterval(function(){
            code = "<?php echo $this->session->userdata('code');?>";
            cek();
        },500);

        function cek(){
            if (code == "4044") {
                $("#alertsizebesar").css('display','none');
                $("#alertsuccessupdate").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url();?>/Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
            else if (code == "4045") {
                $("#alertsuccessupdate").css('display','none');
                $("#alertsizebesar").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url();?>/Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
        }

        jQuery('#change-pic').on('click', function(e){
            jQuery('#changePic').show();
            jQuery('#change-pic').hide();
            
        });
        
        jQuery('#photoimg').on('change', function()   
        { 
            jQuery("#preview-avatar-profile").html('');
            jQuery("#preview-avatar-profile").html('Uploading....');
            jQuery("#cropimage").ajaxForm(
            {
            target: '#preview-avatar-profile',
            success:    function() { 
                    jQuery('img#photo').imgAreaSelect({
                    aspectRatio: '1:1',
                    onSelectEnd: getSizes,
                });
                jQuery('#image_name').val(jQuery('#photo').attr('file-name'));
                }
            }).submit();

        });

        });

        $(".tabmenu").click(function() {
            // body...
            $("#modal_loading").modal('show');
        });
        
        $(".klik_detail").click(function() {
            // body...
            alert('aaaaaa');
            $("#modal_loading").modal('show');
        });
    </script>
    <script type="text/javascript">
        var id_user_kids = "<?php echo $this->session->userdata('id_user_kids'); ?>";
        var id_user = "<?php echo $this->session->userdata('id_user'); ?>";

        // alert(id_user_kids);
        // alert(id_user);
        if (id_user == id_user_kids) {
            $("#edit_akun_ortu").css('display','block');
        }
        else{
            $("#edit_akun_ortu").css('display','none');

        }
</script>
