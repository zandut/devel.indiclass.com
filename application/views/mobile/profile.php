<style type="text/css">
    body{
        background-color: white;
    }
</style>
<header id="header" class="clearfix" data-current-skin="blue" style="background-color: #008080">
    <?php $this->load->view('mobile/inc/navbar'); ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">

        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="" style="width: 98%;">
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>
            <div class="block-header">
                <h2><?php echo $this->lang->line('profil'); ?></h2>

                <!-- <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_about'); ?></li>
                        </ol>
                    </li>
                </ul> -->
            </div><!-- akhir block header -->            
            <div class="" style="margin-top: 5%;">
                <div class="" id="profile-main">

                    <?php
                        $this->load->view('inc/sideprofile');
                    ?>

                    <div class="pm-body clearfix">
                                                <div class="">
                            
                            <blockquote class="m-b-25">
                                <p><?php echo $this->lang->line('tab_account'); ?></p>
                            </blockquote> 

                            <div class=" modal fade" data-modal-color="blue" id="complete_modal" tabindex="-1" data-backdrop="static" role="dialog">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content" style="margin-top: 75%;" >
                                        <div class="modal-header">
                                            <h4 class="modal-title"><?php echo $this->lang->line('completeprofiletitle');?></h4>
                                        </div>
                                            <center>
                                                <div id='modal_approv' class="modal-body" >
                                                    <div class="alert alert-info" style="display: block; ?>" id="alertcompletedata">
                                                        <?php echo $this->lang->line('alertcompletedata');?>
                                                    </div>
                                                </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                            </div>

                            <form action="<?php echo base_url('/master/saveEditProfileStudent') ?>" method="post">                            
                                <input type="hidden" name="id_user" required class="input-sm form-control fg-input" value="<?php if(isset($alluserprofile)){ echo $alluserprofile['id_user']; } ?>">
                                <div class="input-group fg-float">
                                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                    <div class="fg-line">
                                        <input type="text" name="user_birthplace" class="input-sm form-control fg-input" value="<?php if(isset($alluserprofile)){ echo $alluserprofile['user_birthplace']; } ?>">
                                        <label class="fg-label"><?php echo $this->lang->line('placeofbirth'); ?></label>
                                    </div>                                    
                                </div>

                                <br/>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-male-female"></i></span>
                                    <!-- <div class="select"> -->
                                    <?php 
                                        //$flag = null;
                                    	if (isset($alluserprofile)) {
                                    		$flag = $alluserprofile['user_gender'];
                                    	}
                                    ?>
                                        <label class="fg-label"><small><?php echo $this->lang->line('gender'); ?></small></label>
                                        <select name="user_gender" class="select2 form-control">
                                            <option disabled><?php echo $this->lang->line('select_gender'); ?></option>
                                            <option value="Male" <?php if($flag=="Male"){ echo "selected='true'";} ?> ><?php echo $this->lang->line('male');?></option>
                                            <option value="Female" <?php if($flag=="Female"){ echo "selected='true'";} ?>><?php echo $this->lang->line('women');?></option>
                                        </select>                                        
                                    <!-- </div> -->
                                </div>

                                <br/>                            
                            
                                <div class="input-group fg-float">
                                    <span class="input-group-addon"><i class="zmdi zmdi-home"></i></span>
                                    <div class="fg-line">
                                        <input type="text" name="user_address" class="input-sm form-control fg-input" value="<?php if(isset($alluserprofile)){ echo $alluserprofile['user_address']; } ?>">
                                        <label class="fg-label"><?php echo $this->lang->line('home_address'); ?></label>
                                    </div>                                    
                                </div>                       
                                <div class="" style="text-align: center;">                                                                           
                                    <button type="submit" name="simpanedit" style="width: 98%;" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>                    
                                </div>

                            </form>
                        </div>                         
                    </div>

                    <div class="pmb-block">
                        <div class="pmbb-header">
                        
                        </div>
                        <div class="pmbb-body p-l-30">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- akhir container -->
</section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script>
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
    function validAngka(a)
    {
        if(!/^[0-9.]+$/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
      function validPhone(a)
    {
        if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
</script>

<script type="text/javascript">
        $(document).ready(function(){
            var code_cek = null;
            var status_login = "<?php echo $this->session->userdata('status_user');?>";
            var cekk = setInterval(function(){
                code_cek = "<?php echo $this->session->userdata('code_cek');?>";
                
                cek();
            },500);
            // alert('aasdfsd');
            function cek(){
                if (status_login == "umum") {
                    $("#tab_school").css('display','none');
                }
                if (code_cek == "889") {
                    $("#complete_modal").modal('show');
                    $("#alertcompletedata").css('display','block');
                    
                    code_cek == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>Rest/clearsession',
                        type: 'POST',
                        data: {
                            cek_code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
                else
                {

                }
                // console.warn(code_cek);
                // console.warn(status_login);
            }

            
        });
</script>