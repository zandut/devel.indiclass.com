<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
 
  <title>Bootstrap Tour</title>
 
  <link href="<?php echo base_url(); ?>aset/tour/bootstrap.min.css" rel="stylesheet"> 
  <link href="<?php echo base_url(); ?>aset/tour/bootstrap-tour.min.css" rel="stylesheet">
 
</head>
<body>    

    <button id="tourlagi">START</button>
    <div class="container">
    	<div class="row text-center">
    		<div class="col-lg-4" id="paso1" style="border: 1px solid black;">
    			Paso 1
    		</div>
    		<div class="col-lg-4" id="paso2" style="border: 1px solid black;">
    			Paso 2
    		</div>
    		<div class="col-lg-4" id="paso3" style="border: 1px solid black;">
    			Paso 3
    		</div>
    	</div>
    </div>    
    <script src="<?php echo base_url(); ?>aset/tour/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/tour/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/tour/bootstrap-tour.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
 
    var tour = new Tour({
        steps: [
        {
          element: "#paso1",
          content: "<h5>Description paso 1</h5>",
          placement: "right"
        },
        {
          element: "#paso2",
          content: "<h5>Description paso 2</h5>",
          placement: "bottom"
        },
        {
          element: "#paso3",
          content: "<h5>Description paso 3</h5>",
          placement: "left"
        }
        ],
        backdrop: true
    });    
 
    $("#tourlagi").click(function(){
      // alert('a');
      // Initialize the tour
    tour.init();
 
    // Start the tour
    tour.end();
    tour.restart();
    });
    
 
});
    </script>
</body>
</html>