<header id="header" class="clearfix" data-current-skin="<?php echo $this->session->userdata('color'); ?>">
    <?php 
    $this->load->view('inc/navbar');
    echo "<label hidden id='time_servernow'>".strtotime(date('Y-m-d H:i:s'))."</label>";

    ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
        <?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
        ?>
    </aside>


    <section id="content">
        <div class="container">                
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
          </div>
          <?php } ?>
          <div class="block-header">
            <h2><?php echo $this->lang->line('home'); 
                ?></h2>

                <ul class="actions">
                    <li>
                        <ol class="breadcrumb">
                        </ol>
                    </li>
                </ul>
            </div> <!-- akhir block header    -->        
            <?php
            $now = date('N');
            if(isset($page) && $page == 0){
                $nextMon = date('Y-m-d', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));    
            }else if(isset($page) && $page == 1){
                $nextMon = date('Y-m-d', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
                $nextMon = date('Y-m-d', strtotime('+7 day', strtotime($nextMon)));
            }
            $oneWeek = date('Y-m-d', strtotime('+6 day', strtotime($nextMon)));
            $nextMon = DateTime::createFromFormat('Y-m-d',$nextMon);
            $oneWeek = DateTime::createFromFormat('Y-m-d',$oneWeek);

            $timeArray = array();

            $arrayFul;
            $stime = 0;
            for($x=0;$x<7;$x++){
                $timeArray[$x]["day"] =  date('D, d M', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));
                $timeArray[$x]["date"] = date('Y-m-d', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));
                $query_checker[$x] = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id=".$this->session->userdata('id_user')." AND date='".$timeArray[$x]["date"]."'")->result_array();

                for ($i=0; $i < 24; $i++) {
                    $timeArray[$x][$i] = $stime;
                    $stime+=3600;
                }
                $stime = 0;
            }

            ?> 
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            $useragent=$_SERVER['HTTP_USER_AGENT'];
                            if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){

                                ?>
                            <div class="col-md-4 col-xs-12 m-r-10 m-t-20">
                                <button id="btn_prevpage" style="float:right;" <?php if(isset($page) && $page == 0){ echo "disabled"; } ?> class="btn btn-primary btn-block"><?php echo $this->lang->line('previous'); ?></button>
                            </div>
                            <div class="col-md-3 m-t-20 col-xs-12">
                                <h2 class="text-center"><?php echo $nextMon->format('F d')." - ".$oneWeek->format('F d')." ".$nextMon->format('Y'); ?></h2>
                            </div>
                            <div class="col-md-4 col-xs-12 m-t-20">
                                <button id="btn_nextpage" style="float:left;" <?php if(isset($page) && $page == 1){ echo "disabled"; } ?> class="btn btn-primary btn-block"><?php echo $this->lang->line('next'); ?></button>
                            </div>
                            <?php
                            }
                            else
                            {
                            ?>
                            
                            <div class="col-md-4 m-l-20 col-xs-4">
                                <button id="btn_prevpage" style="float:right;" <?php if(isset($page) && $page == 0){ echo "disabled"; } ?> class="btn btn-primary"><?php echo $this->lang->line('previous'); ?></button>
                            </div>
                            <div class="col-md-3 m-t-5 col-xs-4">
                                <h2 class="text-center"><?php echo $nextMon->format('F d')." - ".$oneWeek->format('F d')." ".$nextMon->format('Y'); ?></h2>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <button id="btn_nextpage" style="float:left;" <?php if(isset($page) && $page == 1){ echo "disabled"; } ?> class="btn btn-primary"><?php echo $this->lang->line('next'); ?></button>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="card-body card-padding table-responsive" style="height:auto; width:100%;">
                    <table class="table"  style="text-align:center;">
                        <!-- <colgroup>
                            <col style="background-color:#0097A7">
                            <col style="background-color:#00ACC1">
                            <col style="background-color:#00BCD4">
                            <col style="background-color:#26C6DA">
                            <col style="background-color:#4DD0E1">
                            <col style="background-color:#80DEEA">
                            <col style="background-color:#B2EBF2">
                            <col style="background-color:#E1F5FE">
                        </colgroup>   -->   
                        <colgroup>
                            <col style="background-color:#ecf0f1">
                            <col style="background-color:#ecf0f1">
                            <col style="background-color:#ecf0f1">
                            <col style="background-color:#ecf0f1">
                            <col style="background-color:#ecf0f1">
                            <col style="background-color:#ecf0f1">
                            <col style="background-color:#ecf0f1">
                            <col style="background-color:#ecf0f1">
                        </colgroup>
                    <thead>
                        <tr>
                            <th width="5"></th>
                            <?php foreach ($timeArray as $key => $value) {
                                echo "<th style='text-align:center; font-size:12px;'>".$value['day']."</th>";
                            }
                            ?>
                        </tr>                                
                    </thead>                        
                    <tbody>
                        <?php
                        if($this->session->userdata('status') != 1){
                            echo "<tr><td colspan='8'>".$this->lang->line('nothing_here')."</td></tr>";
                        }
                        else
                        {
                            $jam_awal = 23;
                            $jam_akhir = 0;
                            $at_least_one = 0;
                            $first_date = $timeArray[0]['date'];
                            $last_date = $timeArray[6]['date'];
                            $tutor_id = $this->session->userdata('id_user');
                            $query_sche = $this->db->query("SELECT tc.class_id, tc.participant, tc.subject_id,tc.name, tc.description, tc.tutor_id, tu.first_name, tu.user_image, tc.start_time, tc.finish_time FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user where start_time>='{$first_date} 00:00:00' AND start_time<='{$last_date} 23:59:59' ORDER BY SUBSTRING(start_time,12) ASC, SUBSTRING(start_time,1,10) ASC")->result_array();


                            $all_schedule = array();
                            $checkval = 0;
                            $tot_arr = -1;
                            $cur_date = "";
                            $cur_time = "";
                            $buttonSession_counter = 0;
                            $fill_dates = 1;
                            $id_user = $this->session->userdata('id_user');
                            foreach ($query_sche as $key => $value) {
                                $participant = json_decode($value['participant'],true);
                                $in = 0;
                                $c_kind = "";
                                if($participant['visible'] == "followers"){
                                    $tutor_id = $value['tutor_id'];
                                    $subject_id = $value['subject_id'];
                                    $real = $this->db->query("SELECT * FROM tbl_booking WHERE tutor_id='{$tutor_id}' AND id_user='{$id_user}' AND subject_id='{$subject_id}'")->row_array();
                                    if(!empty($real)){
                                        $in = 1;
                                        $c_kind = "fol";
                                    }

                                }
                                else if($participant['visible'] == "all"){
                                    $in = 1;
                                    $c_kind = "all";
                                }
                                else if($participant['visible'] == "private"){
                                    foreach ($participant['participant'] as $p => $vp) {
                                        if($vp['id_user'] == $id_user){
                                            $in = 1;
                                            $c_kind = "priv";
                                        }
                                    }
                                }
                                if($in == 1){
                                    $at_least_one = 1;
                                    $now_date = substr($value['start_time'], 0,10);
                                    $now_time = substr($value['start_time'], 11,5);
                                    if($cur_time != $now_time){
                                        if($fill_dates<7 && $tot_arr>-1){
                                            for($i = 0;$i<(7-$fill_dates);$i++){
                                                $all_schedule[$tot_arr]['value'].="</td><td>";
                                            }
                                        }
                                        $fill_dates = 1;
                                        // ARRAY INDEX BARU
                                        $tot_arr++;

                                        $all_schedule[$tot_arr]['time'] = $now_time;
                                        $all_schedule[$tot_arr]['value'] = "";
                                        $cur_time = $now_time;
                                        $cur_date = $timeArray[0]['date'];
                                    }
                                    if($cur_date != $now_date){
                                        $diff = date_diff(date_create($cur_date),date_create($now_date));
                                        $diff = $diff->format('%a');
                                        $fill_dates+=$diff;
                                        for($i = 0;$i<$diff;$i++){
                                            $all_schedule[$tot_arr]['value'].="</td><td>";
                                        }
                                        $cur_date = $now_date;
                                    }
                                    if($c_kind == "fol"){
                                        $all_schedule[$tot_arr]['value'].= '
                                        <div class="me_timer" session_id="'.$value['class_id'].'" start_time="'.strtotime($value['start_time']).'" finish_time="'.strtotime($value['finish_time']).'">
                                        <h5>'.$this->lang->line('subject_'.$value['subject_id']).'</h5>
                                        <img src="'.base_url().'aset/img/user/'.$value["user_image"].'" style="border-radius:50%; " width="50px" height="50px"  alt="">
                                        <h6>'.$value["first_name"].'</h6>
                                        <a id="buttonSession_'.strtotime($value['start_time']).'_'.$buttonSession_counter.'" class="btn btn-danger" href="'.base_url().'master/tps_me/'.$value['class_id'].'"></a>
                                        </div>';
                                    }else if($c_kind == "priv"){
                                        $all_schedule[$tot_arr]['value'].= '
                                        <div class="me_timer" session_id="'.$value['class_id'].'" start_time="'.strtotime($value['start_time']).'" finish_time="'.strtotime($value['finish_time']).'">
                                        <h5>'.$this->lang->line('subject_'.$value['subject_id']).'</h5>
                                        <img src="'.base_url().'aset/img/user/'.$value["user_image"].'" style="border-radius:50%; " width="50px" height="50px"  alt="">
                                        <h6>'.$value["first_name"].'</h6>
                                        <a id="buttonSession_'.strtotime($value['start_time']).'_'.$buttonSession_counter.'" class="btn btn-danger" href="'.base_url().'master/tps_me_priv/'.$value['class_id'].'"></a>
                                        </div>';
                                    }
                                    
                                    $buttonSession_counter++;
                                }
                                
                            }
                            if($fill_dates<7 && $tot_arr>-1){
                                for($i = 0;$i<(7-$fill_dates);$i++){
                                    $all_schedule[$tot_arr]['value'].="</td><td>";
                                }
                            }
                            foreach ($all_schedule as $key => $value) {
                                echo '<tr><td>'.$value['time'].'</td><td>'.$value['value'].'</td></tr>';
                            }
                            if($at_least_one == 0){
                                echo "<tr><td colspan='8'>".$this->lang->line('no_schedule')."</td></tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_nextpage').click(function(){
            location.href='<?php echo base_url(); ?>first/index/1';
        });
        $('#btn_prevpage').click(function(){
            location.href='<?php echo base_url(); ?>first/index/0';
        });
        var schedule_time = [];
        var nowtime = [];
        var intervalTime = [];
        var finished = [];
        var finish_time = [];
        var threadHandler = [];
        var hour = [];
        var minute = [];
        var second = [];

        $('.me_timer').each(function(){
            var idnya = $(this).find('a').attr('id');            
            schedule_time[idnya] = $(this).attr('start_time');
            finish_time[idnya] = $(this).attr('finish_time');
            nowtime[idnya] = $('#time_servernow').html();
            intervalTime[idnya] = parseInt(schedule_time[idnya])-parseInt(nowtime[idnya]);
            intervalTime[idnya] = parseInt(schedule_time[idnya])-parseInt(nowtime[idnya]);
            finished[idnya] = parseInt(nowtime[idnya])-parseInt(finish_time[idnya]);

            
            if(intervalTime[idnya] > 43200){
                $('#'+idnya).css('display','none');
                    // $(this).html('not available');
                    $('#'+idnya).attr('class','btn btn-danger');
                    $('#'+idnya).css('pointer-events','none');
                    $('#'+idnya).attr('disabled','');
                }else if(finished[idnya] >= 0){
                    $('#'+idnya).html('<?php echo $this->lang->line('passed'); ?>');
                    $('#'+idnya).attr('class','btn btn-danger');
                    $('#'+idnya).css('pointer-events','none');
                    $('#'+idnya).attr('disabled','');
                }
                threadHandler[idnya] = setInterval(function(){
                    intervalTime[idnya]--;
                    finished[idnya]++;
                    second[idnya] = pad(intervalTime[idnya]%60,2);
                    hour[idnya] = pad(Math.floor(Math.floor(intervalTime[idnya]/60)/60),2);
                    minute[idnya] = pad(Math.floor(intervalTime[idnya]/60)%60,2);


                    if(intervalTime[idnya] > 43200){
                        $('#'+idnya).css('display','none');
                        // $('#'+idnya).html('not available');
                        $('#'+idnya).attr('class','btn btn-danger');
                        $('#'+idnya).css('pointer-events','none');
                        $('#'+idnya).attr('disabled','');
                    }else if(finished[idnya] >=0){

                        $('#'+idnya).html('<?php echo $this->lang->line('passed'); ?>');
                        // $('#'+idnya).html(hour[idnya]+':'+minute[idnya]+':'+second[idnya]);
                        $('#'+idnya).attr('class','btn btn-danger');
                        $('#'+idnya).css('pointer-events','none');
                        $('#'+idnya).attr('disabled','');
                        clearInterval(threadHandler[idnya]);
                    }else if(intervalTime[idnya] >=1 && intervalTime[idnya] <= 43200){
                        $('#'+idnya).html(hour[idnya]+':'+minute[idnya]+':'+second[idnya]);
                        $('#'+idnya).attr('class','btn btn-default');
                        $('#'+idnya).css('pointer-events','none');
                        $('#'+idnya).attr('disabled','');
                    }else if(finished[idnya] < 0 && intervalTime[idnya] <=0){
                        $('#'+idnya).html('<img src="<?php echo base_url(); ?>aset/img/login.png"></img>&nbsp;&nbsp;<?php echo $this->lang->line('enterclass'); ?>');
                        $('#'+idnya).attr('class','btn btn-success');
                        $('#'+idnya).css('pointer-events','auto');
                        $('#'+idnya).removeAttr('disabled');

                    }
            },1000);
            });
        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    });
</script>