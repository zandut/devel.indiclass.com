<header id="header-2" class="clearfix" data-current-skin="lightblue">
    <?php echo $this->load->view('topup/navbar'); ?>
</header>

<section id="main">

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2><?php echo $this->lang->line('uangsaku'); ?></h2>
                <ul class="actions">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('transactionlist'); ?></li>
                    </ol>                    
                </ul>
            </div>

            <div class="card m-t-20">
                <div class="card-header">
                    <h2><?php echo $this->lang->line('transactionlist'); ?></h2>

                    <ul class="actions">
                        <li>
                            <label class="f-17">Transaksi Bulan</label>

                            <select required name="bulan_lahir" class="select2 col-md-6 form-control">
                              <!-- <option disabled selected value=''><?php echo $this->lang->line('monthbirth'); ?></option> -->
                              <option value="01" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "01"){ echo 'selected="true"';} ?>>Januari 2016</option>
                              <option value="02" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "02"){ echo 'selected="true"';} ?>>Februari 2016</option>
                              <option value="03" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "03"){ echo 'selected="true"';} ?>>Maret 2016</option>
                              <option value="04" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "04"){ echo 'selected="true"';} ?>>April 2016</option>
                              <option value="05" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "05"){ echo 'selected="true"';} ?>>Mei 2016</option>
                              <option value="06" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "06"){ echo 'selected="true"';} ?>>Juni 2016</option>
                              <option value="07" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "07"){ echo 'selected="true"';} ?>>Juli 2016</option>
                              <option value="08" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "08"){ echo 'selected="true"';} ?>>Agustus 2016</option>
                              <option value="09" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "09"){ echo 'selected="true"';} ?>>September 2016</option>
                              <option value="10" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "10"){ echo 'selected="true"';} ?>>Oktober 2016</option>
                              <option value="11" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "11"){ echo 'selected="true"';} ?>>Nopember 2016</option>
                              <option value="12" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "12"){ echo 'selected="true"';} ?>>Desember 2016</option>                                                            
                          </select>
                      </li>                        
                  </ul>
              </div>

              <div class="card-body card-padding">
                  <table class="table table-inner table-vmiddle">
                      <thead>
                          <tr>
                              <th class="bgm-teal c-white" style="width: 10px">No</th>
                              <th class="bgm-teal c-white" style="width: 15px">Tanggal</th>
                              <th class="bgm-teal c-white" style="width: 30px">Keterangan</th>
                              <th class="bgm-teal c-white" style="width: 20px">Debet</th>
                              <th class="bgm-teal c-white" style="width: 20px">Kredit</th>
                              <th class="bgm-teal c-white" style="width: 20px">Saldo akhir</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>1</td>
                              <td>2 Desember 2016</td>
                              <td>Membeli pelajaran Mengaji</td>
                              <td>0</td>
                              <td>Rp. 200.000,00</td>
                              <td>Rp. 200.000,00</td>
                          </tr>
                          <tr>
                              <td>2</td>
                              <td>3 Desember 2016</td>
                              <td>Membeli pelajaran Mengaji</td>
                              <td>0</td>
                              <td>Rp. 200.000,00</td>
                              <td>Rp. 200.000,00</td>
                          </tr>
                          <tr>
                              <td>3</td>
                              <td>4 Desember 2016</td>
                              <td>Membeli pelajaran Mengaji</td>
                              <td>0</td>
                              <td>Rp. 200.000,00</td>
                              <td>Rp. 200.000,00</td>
                          </tr>
                          <tr>
                              <td>4</td>
                              <td>5 Desember 2016</td>
                              <td>Membeli pelajaran Mengaji</td>
                              <td>0</td>
                              <td>Rp. 200.000,00</td>
                              <td>Rp. 200.000,00</td>
                          </tr>
                      </tbody>
                  </table>
              </div>              

          </div>
      </div>            

  </div>
</section>
</section>
