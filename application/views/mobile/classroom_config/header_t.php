<!DOCTYPE html>
<html ng-app="cmApp">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="google-signin-client_id" content="850067769597-u7eaknsmcjfb2bc80lvfgt5nh71p7kuc.apps.googleusercontent.com">

  <title>Classroom</title>

  <!-- ________________________________________________________START DIPAKAI_______________________________________________________________________________________ -->

  <link href="<?php echo base_url(); ?>aset/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
  
  <link href="<?php echo base_url(); ?>aset/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>aset/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">    
  <link href="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>aset/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">  
  <link href="<?php echo base_url(); ?>aset/vendors/bower_components/chosen/chosen.min.css" rel="stylesheet">  
  <link href="<?php echo base_url(); ?>aset/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>aset/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>aset/css/app.min.1.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>aset/css/app.min.2.css" rel="stylesheet">
<!--   <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script> -->

  <link href="<?php echo base_url(); ?>aset/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/sel2/css/select2.css" />  
  <link href="<?php echo base_url(); ?>aset/css/custom.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>aset/css/css.css" rel="stylesheet">
  <link rel="icon" href="<?php echo base_url(); ?>aset/img/cm.ico">
  <link href="<?php echo base_url(); ?>aset/tour/bootstrap-tour.min.css" rel="stylesheet">
  
  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>aset/dhtmlxscheduler/dhtmlxscheduler_flat.css" type="text/css" media="screen" title="no title" charset="utf-8"> -->

  <style type="text/css">
      /* scroller browser */
      ::-webkit-scrollbar {
          width: 9px;
      }

      /* Track */
      ::-webkit-scrollbar-track {
          -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
          -webkit-border-radius: 7px;
          border-radius: 7px;
      }

      /* Handle */
      ::-webkit-scrollbar-thumb {
          -webkit-border-radius: 7px;
          border-radius: 7px;
          background: #a6a5a5;
          -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
      }
      ::-webkit-scrollbar-thumb:window-inactive {
          background: rgba(0,0,0,0.4); 
      }
</style>


  <style type="text/css">
    .toggle-switch .ts-label {
      min-width: 130px;
    }
    html, body {
      /*margin: 0px;
      padding: 0px;*/
      /*height: 100%;*/
      overflow: auto;
    }
  </style>
  
  

</head>

<body>
  <!-- Javascript Libraries -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.1.5/adapter.min.js" ></script>
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery/dist/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.7.2/jquery.min.js" ></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js" ></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.1.0/bootbox.min.js"></script>
  <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/spin.js/2.3.2/spin.min.js"></script> -->
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/janus.js" ></script>
  <!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/videoroom.js"></script> -->
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/aceng/janusconfig.js"></script>
  <!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/aceng/chatplugin.js"></script> -->
  <script src="<?php echo base_url(); ?>aset/tour/bootstrap-tour.js"></script>
   
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/testt.js"></script>
    
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cerulean/bootstrap.min.css" type="text/css"/>
  <!-- <link rel="stylesheet" href="css/demo.css" type="text/css"/> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css"/>
  
  <script src="<?php echo base_url(); ?>aset/js/jquery-ui.min.js"></script>
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script> -->
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>  

  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/alertify/alertify.js "></script>
  
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/moment/min/moment.min.js"></script> 
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>   -->
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/chosen/chosen.jquery.min.js"></script>
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js "></script> -->
  
  <script src="<?php echo base_url(); ?>aset/tour/scripts.js"></script>
  
  
  <script src="<?php echo base_url(); ?>aset/vendors/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/js/functions.js"></script>  
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/Waves/dist/waves.min.js"></script>
  
  <script src="<?php echo base_url(); ?>aset/vendors/input-mask/input-mask.min.js"></script>
  
  <script src="<?php echo base_url(); ?>aset/js/jquery.smartWizard.js"></script>
  <!-- <script src="<?php echo base_url(); ?>aset/js/custom.min.js"></script> -->
  <script src="<?php echo base_url(); ?>aset/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>



  <!-- <script src="<?php echo base_url(); ?>aset/sel2/js/select2.js"></script> -->
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/vendors/fileinput/fileinput.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/js/scripts.js"></script>
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/mediaelement/build/mediaelement-and-player.min.js"></script>   -->
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/bootstrap-filestyle.min.js"> </script>
  <!-- <script type="text/javascript" src="https://raw.githubusercontent.com/DeVoresyah/Countdown-Timer-With-Button-jQuery/master/timer.min.js"></script> -->
  <!-- UNTUK DELETE -->
  <!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/backbone.js"></script> -->
  <!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/janus.js"></script> -->
  <!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jquery.maxlength.min.js"> </script> -->
  <script type="text/javascript">
    function notify(from, align, icon, type, animIn, animOut, message){
        $.growl({
            icon: icon,
            title: ' ',
            message: message,
            url: ''
        },{
                element: 'body',
                type: type,
                allow_dismiss: true,
                placement: {
                        from: from,
                        align: align
                },
                offset: {
                    x: 20,
                    y: 85
                },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 5000,
                url_target: '_blank',
                mouse_over: false,
                animate: {
                        enter: animIn,
                        exit: animOut
                },
                icon_type: 'class',
                template: '<div data-growl="container" class="alert" role="alert">' +
                                '<button type="button" class="close" data-growl="dismiss">' +
                                    '<span aria-hidden="true">&times;</span>' +
                                    '<span class="sr-only">Close</span>' +
                                '</button>' +
                                '<span data-growl="icon"></span>' +
                                '<span data-growl="title"></span>' +
                                '<span data-growl="message"></span>' +
                                '<a href="#" data-growl="url"></a>' +
                            '</div>'
        });
    };
  </script>
  
 

 			

