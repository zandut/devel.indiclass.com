<div id="loader">
    <div class="page-loader" style="background-color:#1274b3;">
        <div class="preloader pl-xl pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Classmiles. . .</p>
        </div>
    </div>
</div>
<?php     
    $this->session->set_userdata("type", $type);
?>
<script type="text/javascript">
 	$(document).ready(function(){
 		var token = "<?php echo $inisession; ?>";  
        var tokenjwt = window.localStorage.getItem('access_token_jwt');      
        if (token != "") 
        {
            cekdatabro();
        }
        else
        {
            console.warn("kosong bro");
        }

        function cekdatabro()
        {
            function checkdata(){                
                $.ajax({   
                    url: "https://classmiles.com/Rest/get_token/access_token/"+tokenjwt,
                    type: "POST",
                    data : {
                        session : token                    
                    },                  
                    success: function(data){
                    
                        if (data['error']['code'] == '200') {
                            
                            var usertype = data['response']['usertype'];
                            var st_tour = data['response']['st_tour'];
                            var iduser = data['response']['id_user'];   
                            var janus = data['response']['janus'];                     
                            window.localStorage.setItem('usertype', usertype);
                            window.localStorage.setItem('st_tour', st_tour);
                            window.localStorage.setItem('iduser', iduser);
                            window.localStorage.setItem('janus', janus);                        
                            setTimeout(function(){
                                window.localStorage.setItem('session', token);                             
                                window.location.replace("https://classmiles.com/Classroom");

                            },500);
                        }
                        else
                        {                        
                            checkdata();
                        }                         
                    },
                });
            }
            checkdata();
		}
	});    
</script>