<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>


<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">

            <div class="block-header">
                <h2><?php echo $this->lang->line('profil'); ?></h2>

                <ul class="actions">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_account'); ?></li>
                        </ol>
                    </li>
                </ul>
            </div><!-- akhir block header -->            
            <div class="bs-item z-depth-5">
                <div class="card" id="profile-main">

                    <?php
                        $this->load->view('inc/sideprofile');
                    ?>

                    <div class="pm-body clearfix">
                        <ul class="tab-nav tn-justified" role="tablist">
                            <li class="waves-effect"><a href="<?php echo base_url('first/about'); ?>"><?php echo $this->lang->line('tab_about'); ?></a></li>
                            <li class="active  waves-effect"><a href="<?php echo base_url('first/profile_account'); ?>"><?php echo $this->lang->line('tab_account'); ?></a></li> 
                            <li class="waves-effect"><a href="<?php echo base_url('first/profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li> 
                            <li class="waves-effect"><a href="<?php echo base_url('first/profile_forgot'); ?>"><?php echo $this->lang->line('tab_cp'); ?></a></li>      
                        </ul>                                                                                   

                        <br>
                        <div class="card-padding card-body">
                            <div class="col-sm-12" >  

                            	<div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                    <div class="fg-line">
                                        <input type="text" required class="form-control" placeholder="<?php echo $this->lang->line('username'); ?>">
                                    </div>
                                </div>

                                <br>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                                    <div class="fg-line">
                                        <input type="text" required class="form-control" placeholder="<?php echo $this->lang->line('email_address'); ?>">
                                    </div>
                                </div>                                

                                <br/>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-pin"></i></span>
                                    <div class="fg-line">
                                        <input type="text" required class="form-control" placeholder="<?php echo $this->lang->line('placeofbirth'); ?>">
                                    </div>
                                </div>                                

                                <br/>

                                <div class="input-group">
                                	<span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                	<div class="col-sm-4 m-t-10">
                                		<div class="fg-line">
                                			<input type="text" name="tanggal_lahir" id="tanggal_lahir" maxlength="2" minlength="2" required class="form-control" placeholder="Date">
                                		</div>
                                	</div>
                                	<div class="col-sm-4 m-t-10">
                                		<div class="fg-line">
                                			<select name="bulan_lahir" class="selectpicker">
                                				<option disabled selected>Month</option>
                                				<option value="01">January</option>
                                				<option value="02">February</option>
                                				<option value="03">Maret</option>
                                				<option value="04">April</option>
                                				<option value="05">Mey</option>
                                				<option value="06">Juni</option>
                                				<option value="07">Juli</option>
                                				<option value="08">Agustus</option>
                                				<option value="09">September</option>
                                				<option value="10">Oktober</option>
                                				<option value="11">November</option>
                                				<option value="12">Desember</option>                                                            
                                			</select>
                                		</div>
                                		<!-- <span class="zmdi zmdi-triangle-down form-control-feedback"></span> -->
                                	</div>
                                	<div class="col-sm-4 m-t-10">                                	
                                		<div class="fg-line">
                                			<input type="text" name="tahun_lahir" id="tahun_lahir" maxlength="4" minlength="4" required class="form-control " placeholder="Year">
                                		</div>
                                	</div>
                                </div>
                                <br>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-phone"></i></span>
                                    <div class="fg-line">
                                        <input type="text" required class="form-control" placeholder="<?php echo $this->lang->line('mobile_phone'); ?>">
                                    </div>
                                </div> 

                                <br/>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-male-female"></i></span>
                                    <div class="select">
                                        <select class="selectpicker">
                                            <option><?php echo $this->lang->line('select_gender'); ?></option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                </div>

                                <br/>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-library"></i></span>
                                    <div class="select">
                                        <select class="selectpicker">
                                            <option><?php echo $this->lang->line('select_religion'); ?></option>
                                            <option value="islam">Islam</option>
                                            <option value="protestan">Kristen Protestan</option>
                                            <option value="katolik">Kristen Katolik</option>
                                            <option value="hindu">Hindu</option>
                                            <option value="buddha">Buddha</option>
                                            <option value="Konghucu">Konghucu</option>
                                        </select>
                                    </div>
                                </div>

                                <br>                              

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
                                    <div class="select">
                                        <select style="width:100%; height:5%; font-size:14px; border: solid 1px #e0e0e0; border-top:none; border-left:none;" id='provinsi'>
                                            <option value='0'>SELECT PROVINSI</option>
                                            <?php 
                                                foreach ($provinsi as $prov) {
                                                echo "<option value='$prov[provinsi_id]'>$prov[provinsi_name]</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <br>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
                                    <div class="select">
                                        <select style="width:100%; height:5%; font-size:14px; border: solid 1px #e0e0e0; border-top:none; border-left:none;" id='kabupaten'>
                                            <option value='0'>SELECT KABUPATEN/KOTA</option>
                                        </select>
                                    </div>
                                </div>

                                <br>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
                                    <div class="select">
                                        <select style="width:100%; height:5%; font-size:14px; border: solid 1px #e0e0e0; border-top:none; border-left:none;" id='kecamatan'>
                                            <option value='0'>SELECT KECAMATAN</option>
                                        </select>
                                    </div>
                                </div>

                                <br>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
                                    <div class="select">
                                        <select style="width:100%; height:5%; font-size:14px; border: solid 1px #e0e0e0; border-top:none; border-left:none;" id='kelurahan'>
                                            <option value='0'>SELECT KELURAHAN/DESA</option>
                                        </select>
                                    </div>
                                </div>

                                <br/>
								
								<div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-home"></i></span>
                                    <div class="fg-line">    
                                        <textarea class="form-control" rows="5" placeholder="<?php echo $this->lang->line('home_address'); ?>"></textarea>
                                    </div>
                                </div>  

                                <br/>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-email-open"></i></span>
                                    <div class="fg-line">
                                        <input type="number" required class="form-control" placeholder="<?php echo $this->lang->line('postalcode'); ?>">
                                    </div>
                                </div>                                

                                <br/>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
                                    <div class="select">
                                        <select class="selectpicker" id="jenjangname">
                                            <option value="0"><?php echo $this->lang->line('select_level'); ?></option>
                                            <?php 
                                                foreach ($levels as $l) {
                                                echo "<option value='$l[jenjang_name]'>$l[jenjang_name]</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>                                                       

                                <br/>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-graduation-cap"></i></span>
                                    <div class="select">                                        
                                        <select style="width:100%; height:5%; font-size:14px; border: solid 1px #e0e0e0; border-top:none; border-left:none;" id='jenjanglevel'>
                                            <option value="0"><?php echo $this->lang->line('select_class'); ?></option>
                                        </select>
                                    </div>
                                </div>                                                                                          
                                <br><br>
                            </div>                                                    

                            <div class="card-padding card-body">                                                                           
                                <button class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>                                        
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div><!-- akhir container -->
    </section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

