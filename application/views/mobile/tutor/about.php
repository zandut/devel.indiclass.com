<style type="text/css">
 html, body {
            max-width: 100%;
            overflow-x: hidden;
        }
</style>


<header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #Cf000f;">
        <?php $this->load->view('mobile/inc/navbar'); ?>
        <div class="header-inner" style="color: white; padding-top: 0; margin-top: 0; ">
            <center>
            <!-- <div class="col-xs-12" style="background-color: #2196f3;"><a href="#" class=" m-l-10"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/logoclass6.png" alt=""></a></div>     -->
            <div class="tabmenu col-xs-4" style=""><a style="color: white;" href="<?php echo base_url();?>">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/My Class White.svg" style="width: 20px;">
                <br><label style="font-size: 9px; text-transform: uppercase; "><?php echo $this->lang->line('home'); ?></label></a>
            </div>
            <!-- <div class="tabmenu col-xs-4" style=""><a style="color: white;" href="<?php echo base_url(); ?>tutor/set_class">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Jam White.svg" style="width: 20px;  height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('aturkelas'); ?></label></a>
            </div> -->
            <div class="tabmenu col-xs-4" style=""><a style="color: white;" href="<?php echo base_url(); ?>tutor/quiz">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_quiz_white.png" style="width: 20px; height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;">Quiz</label></a>
            </div>
            <div class=" col-xs-4" style=""><a style="color: white;" href="<?php echo base_url(); ?>tutor/about">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Account Green.svg" style="width: 20px; margin-top: 3%;  height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('tab_account');?></label></a>
            </div>
            </center>                           
        </div>
</header>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">

    <div id="content" class="" style="position: absolute; top: 2;">
        <div class="modal fade fade" style="" id="modal_loading" tabindex="-1" data-backdrop="static" role="dialog">
            <div class="modal-dialog" role="document" style="background:rgba(0,00,00,0.0);">   
                <div style="margin-top: 50%;">
                    <center>
                       <div class="preloader pl-xxl pls-teal">
                            <svg class="pl-circular" viewBox="25 25 50 50">
                                <circle class="plc-path" cx="50" cy="50" r="20" />
                            </svg>
                        </div><br>
                       <p style="color: white;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
                    </center>
                </div>
            </div>
        </div>
        <div class="alert alert-success" style="display: none; ?>" id="alertsuccessupdate">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
            Foto akun berhasil diubah
        </div>
        <div class="alert alert-danger" style="display: none; ?>" id="alertsizebesar">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
            Upload gagal, Mohon ukuran file tidak melebihi 2 MB.
        </div>       
        <div class="" style="">
            <?php
                $iduser = $this->session->userdata('id_user');            
                $get_image = $this->db->query("SELECT * FROM tbl_user WHERE id_user='".$iduser."'")->row_array();                
            ?>
            <div class="card header-inner">
                <div class='col-xs-12' style='background-color: #d6d6d6'>
                    <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                        <div class=""><img class="img-circle m-r-10" style="width: 20px; height: 20px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_account_circle_grey.png">
                            <label style="color: black; margin-top: 4%;"><?php echo $this->lang->line('tab_account'); ?></label>
                            <a href="<?php echo base_url('tutor/profile_account'); ?>"><img class="pull-right m-t-10" style=" width: 20px; height: 20px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_account_edit_black.png"></a>
                        </div>
                        <div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:black; border-bottom-style: solid; margin-bottom:10px;'>
                        </div>
                        <div class='col-xs-3 col-sm-1 m-b-10  ' style=' width:50px;'>
                            <img style="margin-left: -70%;" class='img-circle' src='<?php echo $this->session->userdata('user_image');?>' onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" width='40px' height='40px' style=' '>
                        </div>
                        <div class='col-xs-5 m-b-10 ' style='width:44% height:70px; margin-left: -5%;' >
                            <h6 class='' style=' margin-top: 0; font-size:11px; '><?php 
                                if ($profile['user_name'] == "") {
                                    echo $this->lang->line('nousername');
                                    }
                                    echo $profile['user_name']; 
                                ?>
                            </h6>
                           <h6 class='' style='font-size:11px; '><?php 
                                if ($profile['user_age'] == "") {
                                    echo $this->lang->line('noage');
                                }
                                echo $profile['user_age'] ;
                            ?> <?php echo $this->lang->line('yearsold');  ?> 
                            </h6>
                        </div>
                    </div>
                </div>
                <div class='col-xs-12' style='background-color: #d6d6d6'>
                    <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                        <div class="m-t-10 m-b-5"><img class=" m-r-10" style="width: 25px; height: 25px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_subject.png">
                            <label style="color: black; margin-top: 2%;"><?php echo $this->lang->line('subject');?></label>
                        </div>
                        <div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:black; border-bottom-style: solid; margin-bottom:10px;'></div>
                        <div class="col-xs-3  m-l-10 m-b-10 m-t-10" style="margin-left: -5%; "><a style="color: black;" href="<?php echo base_url();?>tutor/choose"> Add/Remove</a></div>
                        <div class="col-xs-3 m-l-10 m-b-10 m-t-10" ><a style="color: black;" href="<?php echo base_url();?>tutor/subject">Modify</a></div>
                    </div>
                </div>
                <div class='col-xs-12' style='background-color: #d6d6d6'>
                    <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                         <a href="">
                            <div class="m-t-10 m-b-5"><img class=" m-r-10" style="width: 25px; height: 25px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_information_variant_grey.png">
                               <label style="color: black; margin-top: 2%;">Laporan</label>
                            </div>
                        </a>
                    </div>
                </div>
                <div class='col-xs-12' style='background-color: #d6d6d6'>
                    <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                         <a href="<?php echo base_url();?>syarat-ketentuan" target="_blank">
                            <div class="m-t-10 m-b-5"><img class=" m-r-10" style="width: 25px; height: 25px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_information_variant_grey.png">
                               <label style="color: black; margin-top: 2%;"><?php echo $this->lang->line('bannersyarat')?></label>
                            </div>
                        </a>
                    </div>
                </div>
                <div id="btn_logout" class='col-xs-12' style=''>
                    <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                        <a href="<?php echo base_url();?>logout">
                            <div class="m-t-10 m-b-5"><img class=" m-r-10" style="width: 25px; height: 25px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/keluar1-01.svg">
                               <label style="color: black; margin-top: 2%;"><?php echo $this->lang->line('logout')?></label>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>         
</section>


<script type="text/javascript">
    var code = null;
        var cekk = setInterval(function(){
            code = "<?php echo $this->session->userdata('code');?>";
            cek();
        },500);
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        function cek(){
            if (code == "4044") {
                $("#alertsizebesar").css('display','none');
                $("#alertsuccessupdate").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: 'https://classmiles.com/Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
            else if (code == "4045") {
                $("#alertsuccessupdate").css('display','none');
                $("#alertsizebesar").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: 'https://classmiles.com/Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
        }
        $(".tabmenu").click(function() {
            // body...
            $("#modal_loading").modal('show');
        });
</script>