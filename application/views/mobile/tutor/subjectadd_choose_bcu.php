<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2>Menentukan Harga</h2>

				<!-- <ul class="actions hidden-xs">
					<li>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url();?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>                            
							<li class="active"><?php echo $this->lang->line('selectsubjecttutor'); ?></li>
						</ol>
					</li>
				</ul> -->
			</div> <!-- akhir block header    -->        
			<br>
			<?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>
			<!-- disini untuk dashboard murid -->               
			<div class="card">
				<div class="card-header"></div>
				<div class="card-body card-padding">
				<div class="row">
				<div class="table-responsive">
					<table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
						<thead>
							<?php
							$idtutor = $this->session->userdata('id_user');
							$select_all = $this->db->query("SELECT tb.*, mj.* FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$idtutor' AND tb.tutor_id='0' order by ms.jenjang_id ASC")->result_array();
							?>
							<tr>
								<th data-column-id="no" data-type="numeric" data-identifier="true">No</th>
								<th data-column-id="name"><?php echo $this->lang->line('subject'); ?></th>
								<th data-column-id="price">Harga</th>
								<th data-column-id="setprice">Pasang Harga</th>
								<th data-column-id="type" width="20%">Tipe</th>								
								<th data-column-id="kelas"><?php echo $this->lang->line('aksi');?></th>

							</tr>
						</thead>
						<tbody>  
							<?php
							$no=1;
							foreach ($select_all as $row => $v) {
								$data = $this->db->query("SELECT * FROM tbl_service_price WHERE subject_id='".$v['subject_id']."' AND tutor_id='".$this->session->userdata('id_user')."' ")->row_array();
								?>       
								<tr>
									<td><?php echo($no); ?></td>
									<td>
										<?php 
											echo $v['jenjang_level'].' '.$v['jenjang_name'].' - '.$this->lang->line('subject_'.$v['subject_id']);
										?>
										
									</td>
									<td><b><label id='harganich'><?php 
									if(empty($data['harga'])){ echo "Rp. 0 ";}else{ echo "Rp. ".$data['harga'];}?></label> <input type="text" name="iniid<?php echo $v['subject_id'];?>" id="iniid<?php echo $v['subject_id'];?>" value="<?php echo $v['subject_id'];?>" hidden></td></b>
									<td>
										<!-- <input type="text" name="price<?php echo $v['subject_id']?>" id="price<?php echo $v['subject_id']; ?>" class="input-sm form-control fg-input" placeholder=" Masukan harga anda"> -->

										<input type="text" id="nominal" subject="<?php echo $v['subject_id']; ?>" required class="price<?php echo $v['subject_id']; ?> input-sm form-control fg-input nominal" placeholder=" Masukan harga anda" minlength="9" step="500" maxlength="14" min="10000" max="500000"/>
                                            <input type="text" id="price<?php echo $v['subject_id']; ?>" class="hargakelas<?php echo $v['subject_id']; ?>" value="<?php echo $data['harga']; ?>" hidden />
									</td>	
									<td>
										<select class="select2 btn-block" id="type<?php echo $v['subject_id']; ?>">
											<option disabled selected>Pilih Tipe</option>
											<option value="private" <?php if($data['class_type'] == "private"){ echo "selected='true'";} ?>>Private</option>
                                            <option value="group" <?php if($data['class_type'] == "group"){ echo "selected='true'";} ?>>Group</option>
                                        </select>
                                    </td>							
									<td>
										<?php 
										// if(!empty($booked)){
										// 	echo '<a class="waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax" id="'.$v['subject_id'].'" href="'.base_url('/master/unchoosesub?id_user='.$this->session->userdata('id_user').'&subject_id='.$v['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('tidakikutisubject').'</a>';
										// }else{
										// 	echo '<a class="waves-effect bgm-green c-white btn btn-success btn-block choose_ajax" id="'.$v['subject_id'].'" href="'.base_url('/master/choosesub?id_user='.$this->session->userdata('id_user').'&subject_id='.$v['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('ikutisubject').'</a>';
										// }
										if ($data)
										{
											echo '<a class="waves-effect c-white btn bgm-deeporange btn-block simpanprice" id="'.$v['subject_id'].'"><i class="zmdi zmdi-face-add"></i>Ubah</a>';
										}
										else
										{
											echo '<a class="waves-effect bgm-green c-white btn btn-success btn-block simpanprice" id="'.$v['subject_id'].'"><i class="zmdi zmdi-face-add"></i>Tambah</a>';
										}
										
										?>
									</td>
								</tr>
								<?php 
								$no++;
							}
							?>		
						</tbody>   
					</table><hr>
					<label class="c-red">* Silahkan Masukan harga dan pilih tipe kembali untuk mengubah data yang sebelumnya</label>   
				</div>
				</div>
				</div>
			</div>
		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();
	$(document).ready(function(){
		

        $('.nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $(".nominal").keyup(function() {
        	var sb = $(this).attr('subject');        	
			var clone = $(this).val();
			var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
			$(".hargakelas"+sb).val(cloned);
        });
    });
	/*$(document).on('click', "a.choose_ajax", function(e) {
		e.preventDefault();
		$(this).removeClass('choose_ajax');
    	alert('ke choose');
	});
	$(document).on('click', "a.unchoose_ajax", function() {
    	alert('ke unchoose');
	});*/

	$(document).on('click', "a.simpanprice", function(e){
		var idtutorni = "<?php echo $this->session->userdata('id_user');?>";
		var subject_idd = $(this).attr('id');		
		var harga = $(".hargakelas"+subject_idd).val();
		var type = $("#type"+subject_idd).val();
		
		$.ajax({
			url: 'https://classmiles.com/Rest/subjectadd',
			type: 'POST',
			data: {
				id_tutor: idtutorni,
				subject_id: subject_idd,
				harga: harga,
				class_type: type
			},
			success: function(data)
			{	
				code = data['code'];
				if (code == "200") {
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Menambah harga");
					setTimeout(function(){
						location.reload();
					},500);
				}
				else if(code == "201")
				{
					notify('top','right','fa fa-check','failed','animated fadeInDown','animated fadeOut', "Gagal Menambah harga");
					setTimeout(function(){
						location.reload();
					},500);
				}
				else if (code == "300") 
				{
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Merubah harga");
					setTimeout(function(){
						location.reload();
					},500);
				}
				else if(code == "201")
				{
					notify('top','right','fa fa-check','failed','animated fadeInDown','animated fadeOut', "Gagal Merubah harga");
					setTimeout(function(){
						location.reload();
					},500);
				}
			}
		});

	});		

	$(document).on('click', "a.choose_ajax", function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax');
					that.attr('href',"<?php echo base_url('/master/unchoosesub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('tidakikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		
	});
	$(document).on('click','a.unchoose_ajax', function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block choose_ajax');
					that.attr('href',"<?php echo base_url('/master/choosesub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('ikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		
	});
	
</script>