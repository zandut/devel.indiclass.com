<style type="text/css">
 html, body {
            max-width: 100%;
            overflow-x: hidden;
        }
    body{
        
    }
</style>
<header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #Cf000f;">
        <?php $this->load->view('mobile/inc/navbar'); ?>
        <div class="header-inner" style="color: white; padding-top: 0; margin-top: 0; ">
            <center>
            <!-- <div class="col-xs-12" style="background-color: #2196f3;"><a href="#" class=" m-l-10"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/logoclass6.png" alt=""></a></div>     -->
            <div class="tabmenu col-xs-3" style=""><a style="color: white;" href="<?php echo base_url();?>">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/My Class White.svg" style="width: 16px;">
                <br><label style="font-size: 9px; text-transform: uppercase; "><?php echo $this->lang->line('home'); ?></label></a>
            </div>
            <div class=" col-xs-3" style=""><a style="color: white;" href="<?php echo base_url(); ?>tutor/set_class">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Jam Green.svg" style="width: 16px;  height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('aturkelas'); ?></label></a>
            </div>
            <div class="tabmenu col-xs-3" style=""><a style="color: white;" href="<?php echo base_url(); ?>tutor/approval_ondemand">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Extra Class White.svg" style="width: 16px;  height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('private');?></label></a>
            </div>
            <div class="tabmenu col-xs-3" style=""><a style="color: white;" href="<?php echo base_url(); ?>tutor/about">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Account White.svg" style="width: 16px;  height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('tab_account');?></label></a>
            </div>
            </center>                           
        </div>  
</header><br><br><br><br>
<div class="modal fade fade" style="" id="modal_loading" tabindex="-1" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document" style="background:rgba(0,00,00,0.0);">   
        <div style="margin-top: 50%;">
            <center>
               <div class="preloader pl-xxl pls-teal">
                    <svg class="pl-circular" viewBox="25 25 50 50">
                        <circle class="plc-path" cx="50" cy="50" r="20" />
                    </svg>
                </div><br>
               <p style="color: white;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
            </center>
        </div>
    </div>
</div>
 <div class="atur_kelas m-t-30">
        <div id="kelas_pribadi" class='col-xs-12' style='background-color: #d6d6d6'>
            <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                <a href="">
                    <div class="m-t-10 m-b-5">
                       <label style="color: black; margin-top: 2%;"><?php echo $this->lang->line('kelaspribadi'); ?></label>
                    </div>
                </a>
                <div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:#d6d6d6; border-bottom-style: solid; margin-bottom:10px;'>
                    
                </div>
                <center>
                    <div class="col-xs-6 tabmenu" id="kls_free">
                        <a href="<?php echo base_url('tutor/free'); ?>">
                            <img  style="  width: 25%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/freeclass.png"><br>  
                            <label style="font-size: 10px; color: black;"><?php echo $this->lang->line('gratis'); ?></label>
                        </a>
                    </div>
                    <div class="col-xs-6 tabmenu" id="kls_paid">
                        <a href="<?php echo base_url('tutor/paid'); ?>">
                            <img  style="  width: 25%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/paidclass.png"><br>  
                            <label style="font-size: 10px; color: black;"><?php echo $this->lang->line('berbayar'); ?></label>
                        </a>
                    </div>
                    
                </center> 
            </div>
        </div>
        <div id="kelas_grup" class='col-xs-12' style=''>
            <div style='width:110%; margin-left:-5%; background-color:white; margin-bottom: 2%;' class=' col-xs-12'>
                <a href="">
                    <div class="m-t-10 m-b-5">
                       <label style="color: black; margin-top: 2%;"><?php echo $this->lang->line('kelaschannel'); ?></label>
                    </div>
                </a>
                <div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:#d6d6d6; border-bottom-style: solid; margin-bottom:10px;'>
                    
                </div>
                <center>
                    <div class="col-xs-6 tabmenu" >
                        <a href="<?php echo base_url('tutor/privatechannel'); ?>">
                            <img  style="  width: 26%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/channelprivate.png"><br>  
                            <label style="font-size: 10px; color: black;"><?php echo $this->lang->line('privateclass'); ?></label>
                        </a>
                    </div>
                    <div class="col-xs-6 tabmenu">
                        <a href="<?php echo base_url('tutor/groupchannel'); ?>">
                            <img  style="  width: 26%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/channelgroup.png"><br>  
                            <label style="font-size: 10px; color: black;"><?php echo $this->lang->line('groupclass'); ?></label>
                        </a>
                    </div>
                    <div class="col-xs-12 tabmenu">
                        <a href="<?php echo base_url('tutor/multicastchannel'); ?>">
                            <img  style="  width: 15%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/channelmulticast.png"><br>  
                            <label style="font-size: 10px; color: black;">Multicast</label>
                        </a>
                    </div>
                    
                </center> 
            </div>
        </div>
    </div>
    <script type="text/javascript">
    	$(".tabmenu").click(function() {
            // body...
            $("#modal_loading").modal('show');
        });
    </script>