<style type="text/css">
    .modal-backdrop.in {
        opacity: 0.9;
    }
    body{
        background-color: white;
        /*margin: 0; height: 100%; overflow: hidden;*/
    }

    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;  
        opacity: 0.7;      
        background: url('https://2aih25gkk2pi65s8wfa8kzvi-wpengine.netdna-ssl.com/gre/wp-content/plugins/magoosh-lazyload-comments-better-ui/assets/img/loading.gif') center no-repeat #000;
        background-size: 150px 150px;
    }
    .modal-content  {
        -webkit-border-radius: 0px !important;
        -moz-border-radius: 0px !important;
        border-radius: 10px !important; 
    }
    .bp-header:hover
    {
        cursor: pointer;
    }
    .ci-avatar {
        background-color: rgba(255, 255, 255, 1);
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .ci-avatar:hover {
        background-color: rgba(52, 73, 94, 0.2);
    }
    .modal-body{
        max-height: 80vh;
    }
</style>

<header id="header" class="clearfix" data-current-skin="blue" style="background-color: #008080;">
    <div class="" style="">
        <h2 style="color: white"><a style="color: white" href="<?php echo base_url();?>"><i class="zmdi zmdi-arrow-back"> </i></a> <b style="padding-left: 5%">Detail Order</b></h2>
    </div>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="">
                <h2>Rincian Pembayaran</h2>
            </div> 

            <?php        
                $order_id = null;
                $UnixCode = null;         
                if (!isset($_GET['order_id']) != null) {
                    ?>
                    <div class="alert alert-info" role="alert">Tidak ada data</div>
                    <?php 
                }
                else
                {                
                    $order_id           = $_GET['order_id'];
                    $order              = $this->db->query("SELECT * FROM tbl_order as tor LEFT JOIN tbl_invoice as ti ON tor.order_id=ti.order_id WHERE tor.order_id='$order_id'")->row_array();
                    if (empty($order)) {
                        $this->session->set_flashdata('mes_alert','info');
                        $this->session->set_flashdata('mes_display','block');
                        $this->session->set_flashdata('mes_message','Maaf, Order tersebut tidak dapat kami temukan diserver kami.');
                        redirect('/');
                    }
                    else
                    {
                    $invoice_id         = $order['invoice_id'];
                    $datetrans          = $order['payment_due']; 
                    $UnixCode           = $order['unix_code'];
                    $order_status       = $order['order_status'];
                    $hargakelas         = $order['total_price']-$UnixCode;
                    $hargakelas         = number_format($hargakelas, 0, ".", ".");
                    if ($order['underpayment'] == 0) {
                        $hasiljumlah    = number_format($order['total_price'], 0, ".", ".");    
                    }
                    else
                    {
                        $hasiljumlah    = number_format($order['underpayment'], 0, ".", ".");
                    }
                    $user_utc           = $this->session->userdata('user_utc');
                    $server_utcc        = $this->Rumus->getGMTOffset();
                    $intervall          = $user_utc - $server_utcc;
                    $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$datetrans);
                    $tanggaltransaksi->modify("+".$intervall ." minutes");
                    $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');

                    // $dateee             = $tanggaltransaksi;
                    // $datetime           = new DateTime($dateee);
                    // $datetime->modify('+1 hours');
                    // $limitdateee        = $datetime->format('Y-m-d H:i:s');

                    $datelimit          = date_create($tanggaltransaksi);
                    $dateelimit         = date_format($datelimit, 'd/m/y');
                    $harilimit          = date_format($datelimit, 'd');
                    $hari               = date_format($datelimit, 'l');
                    $tahunlimit         = date_format($datelimit, 'Y');
                    $waktulimit         = date_format($datelimit, 'H:i');
                                                            
                    $datelimit          = $dateelimit;
                    $sepparatorlimit    = '/';
                    $partslimit         = explode($sepparatorlimit, $datelimit);
                    $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                        

                    ?>
                    <div class="card" style="padding: 2%;  margin-bottom: 0;">
                        <div class="row">
                            <div class="col-xs-4">
                                <label>No <?php echo $this->lang->line('transaksi'); ?></label><br>    
                                <label><?php echo $this->lang->line('tanggal'); ?></label>    
                            </div>
                            <div class="col-xs-8" align="right">
                                <label><?php echo $invoice_id; ?></label><br>
                                <label><?php echo $tanggaltransaksi; ?></label><br>
                            </div>
                        </div>
                        
                    </div>
                    <div class="card">
                                                
                        <div class="card-body card-padding" style="background: white;">
                            <div class="row m-b-25 text-center">
                                
                                <div class="col-xs-12">
                                    <div class="i-to">
                                        

                                        <?php 
                                        if ($order_status == 1 || $order_status == -2 || $order_status == -3) {
                                            echo "<p class='c-gray'>Detail Pembayaran</p>";
                                        }
                                        else
                                        {
                                            ?>
                                            <p class="c-gray"><?php echo $this->lang->line('segerabayar'); ?></p>

                                            <div  class="c-blue" style="">
                                                <!-- <label style="margin-top: 5%;" class="f-14">Kamis, 08 Desember 2016 Pukul 10:00 WIB</label> -->
                                                <label style="" class="c-blue f-14"><?php echo $hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit. ' Pukul '.$waktulimit ?></label>
                                            </div>                                
                                            <?php
                                        }
                                        ?>
                                        <center>                                                
                                            <table style='border: 1px solid gray; border-collapse: collapse; color: #6C7A89; margin-top: 1.5%;'>
                                                <tr>                                                                                              
                                                    <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse; text-align: left;'> 
                                                        Total Harga Produk :
                                                    </th>                 
                                                    <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse; text-align: right;'>Rp. <?php echo $hargakelas;?></th>
                                                </tr> 
                                                <tr>
                                                    <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse; text-align: left;'>
                                                        Kode Unik :
                                                    </td>
                                                    <td style='text-align: right; padding: 15px;'>Rp. <?php echo $UnixCode;?></td>
                                                </tr>                                                                                   
                                            </table>
                                        </center>

                                        <h6 class="c-gray m-t-15"><?php echo $this->lang->line('totalpayment'); ?></h6>
                                        <h1>Rp. <?php echo $hasiljumlah; ?></h1>
                                        
                                        <span class="text-muted">
                                            <address>
                                                <?php echo $this->lang->line('bedatransfer'); ?>
                                            </address>        
                                        </span>

                                        <!-- <form action="<?php echo site_url()?>/vtweb/vtweb_checkout" method="POST" id="payment-form"> -->
                                        <?php 
                                            if ($order_status == -3) {
                                                ?>
                                                <a href="<?php echo base_url('ConfirmPayment?order_id='.$order_id.'&trans=knf');?>"><button class="btn btn-lg bgm-blue f-15"><?php echo $this->lang->line('konfirmasiunderpayment'); ?></button></a>
                                                <br><br>
                                                <?php 
                                            }
                                            else
                                            {
                                                $cekconfirmpayment = $this->db->query("SELECT * FROM log_trf_confirmation WHERE trx_id='$order_id'")->row_array();
                                                if (empty($cekconfirmpayment)) {                                                                                            
                                                ?>
                                                <a href="<?php echo base_url('ConfirmPayment?order_id='.$order_id.'&trans=knf');?>"><button class="btn btn-lg bgm-blue"><?php echo $this->lang->line('konfirmasipembayaran'); ?></button></a>
                                                <br><br>
                                                <?php 
                                                }
                                            }
                                        ?>
                                        
                                        <hr>
                                        <center>
                                            <p class="f-13">Status Pembayaran :</p>
                                            <?php 
                                                if ($order_status == 2) {
                                                    echo "<label class='f-16 c-blue'>".$this->lang->line('notif_waitingpayment')."</label>";
                                                }
                                                else if ($order_status == 1) {
                                                    echo "<label class='f-16 c-green'>".$this->lang->line('notif_successpayment')."</label>";
                                                }
                                                else if ($order_status == -2) {
                                                    echo "<label class='f-16 c-red'>".$this->lang->line('notif_classexpired')."</label>";
                                                    echo "<br>";
                                                    echo "<label class='f-13 c-red'>".$this->lang->line('notif_classexpired2')."</label>";
                                                }
                                                else if ($order_status == -3) {
                                                    echo "<label class='f-16 c-red'>".$this->lang->line('notif_underpayment')."</label>";
                                                }
                                            ?>
                                        </center>                                        
                                        <span class="text-muted">                                     
                                            <center>
                                            <div class="table-responsive" style="margin-top: 2.5%;">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Nama Kelas</th>
                                                            <th>Deskripsi Kelas</th>
                                                            <th>Harga Kelas</th>
                                                            <th>Nama Tutor</th>
                                                            <th>Waktu Mulai</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $getdataorder = $this->db->query("SELECT tod.*, trm.class_id as class_id, tc.name as subject_name, tr.subject_id as tr_subject_id, tr.tutor_id as tr_tutor_id, tr.approve as tr_approve, tr.date_requested as tr_date_requested, tr.topic as tr_topic, trg.subject_id as trg_subject_id, trg.tutor_id as trg_tutor_id, trg.approve as trg_approve, trg.date_requested as trg_date_requested, trg.topic as trg_topic, tc.subject_id as trm_subject_id, tc.tutor_id as trm_tutor_id, trm.status as trm_approve, tc.start_time as trm_date_requested, tc.description as trm_topic, trp.status as trp_approve, trp.created_at as trp_date_requested, tpp.name as trp_topic FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id LEFT JOIN (tbl_request_program as trp INNER JOIN tbl_price_program as tpp ON trp.program_id=tpp.list_id) ON trp.request_id=tod.product_id WHERE tod.order_id='$order_id'")->result_array();

                                                        $no = 1;
                                                        foreach ($getdataorder as $row => $v) {
                                                            $startimeclass  = $v['tr_date_requested'] != null ? $v['tr_date_requested'] : ( $v['trg_date_requested'] != null ? $v['trg_date_requested'] : ($v['trm_date_requested'] != null ? $v['trm_date_requested'] : $v['trp_date_requested']));

                                                            $subject_id     = $v['tr_subject_id'] != null ? $v['tr_subject_id'] : ( $v['trg_subject_id'] != null ? $v['trg_subject_id'] : ($v['trm_subject_id'] != null ? $v['trm_subject_id'] : ''));
                                                            $topic          = $v['tr_topic'] != null ? $v['tr_topic'] : ( $v['trg_topic'] != null ? $v['trg_topic'] : ( $v['trm_topic'] != null ? $v['trm_topic'] : $v['trp_topic']));
                                                            $tutor_id       = $v['tr_tutor_id'] != null ? $v['tr_tutor_id'] : ( $v['trg_tutor_id'] != null ? $v['trg_tutor_id'] : ( $v['trm_tutor_id'] != null ? $v['trm_tutor_id'] : ''));

                                                            $subject_name       = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
                                                            $tutor_name         = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];

                                                            $waktumulai         = DateTime::createFromFormat ('Y-m-d H:i:s',$startimeclass);
                                                            $waktumulai->modify("+".$intervall ." minutes");
                                                            $waktumulai         = $waktumulai->format('H:i:s d-m-Y'); 
                                                            $priceperclass      = $v['init_price'];  
                                                            $priceperclass      = number_format($priceperclass, 0, ".", ".");                                   
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $no; ?></td>
                                                            <td><?php echo $subject_name; ?></td>
                                                            <td><?php echo $topic; ?></td>
                                                            <td><?php echo 'Rp '.$priceperclass; ?></td>
                                                            <td><?php echo $tutor_name; ?></td>
                                                            <td><?php echo $waktumulai; ?></td>
                                                        </tr> 
                                                        <?php
                                                        $no++;
                                                    }
                                                    ?>                                                  
                                                    </tbody>
                                                </table>
                                            </div>

                                            </center>
                                        </span>                                        

                                    </div>
                                </div>
                                
                            </div>
                            <hr>
                            <p class="c-gray text-center"><?php echo $this->lang->line('pembayaranmelalui'); ?></p>
                            <div class="clearfix"></div>
                            
                            <div class="row m-t-25 p-0 m-b-25">

                                <div class="col-xs-1"></div>
                                <!-- <div class="col-xs-2">
                                    <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                        <div class="c-white m-b-5"><img style="margin-left: -20%;" src="<?php echo base_url();?>aset/images/banklogo/bca1.png"></div>
                                        <h2 class="m-0 c-gray f-12">Bank BCA, Jakarta
                                        <h2 class="m-0 c-gray f-12">254 689 2003 </h2>
                                        <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                                    </div>
                                </div>
                                
                                <div class="col-xs-2">
                                    <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                        <div class="c-white m-b-5"><img style="margin-left: -20%;" src="<?php echo base_url();?>aset/images/banklogo/bni1.png"></div>
                                        <h2 class="m-0 c-gray f-12">Bank BNI, Jakarta
                                        <h2 class="m-0 c-gray f-12">0381486211 </h2>
                                        <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                                    </div>
                                </div> -->
                                
                                <div class="col-xs-10 col-md-10">
                                    <div class="brd-2 p-15 text-center" style="margin-top: -2%;">
                                        <div class="c-white m-b-5"><img  src="<?php echo base_url();?>aset/images/banklogo/mandiri1.png"></div>
                                        <h2 class="m-0 c-gray f-12">Bank Mandiri, Yogyakarta
                                        <h2 class="m-0 c-gray f-12">137-0014152082 </h2>
                                        <h2 class="m-0 c-gray f-12">a.n. PT Meetaza Prawira Media</h2>
                                    </div>
                                </div>
                                
                                <!-- <div class="col-xs-2">
                                    <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                        <div class="c-white m-b-5"><img style="margin-left: -25%;" src="<?php echo base_url();?>aset/images/banklogo/bri1.png"></div>
                                        <h2 class="m-0 c-gray f-12">Bank BRI, Jakarta
                                        <h2 class="m-0 c-gray f-12">580 475 900 684 136 </h2>
                                        <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                                    </div>
                                </div>

                                <div class="col-xs-2">
                                    <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                        <div class="c-white m-b-5"><img style="margin-left: -25%;" src="<?php echo base_url();?>aset/images/banklogo/cimb1.png"></div>
                                        <h2 class="m-0 c-gray f-12">Bank CIMB, Jakarta
                                        <h2 class="m-0 c-gray f-12">5454 2212 3333 </h2>
                                        <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                                    </div>
                                </div> -->
                                <div class="col-xs-1"></div>

                            </div>                  
                            <hr>
                            <div class="clearfix"></div>
                            
                            <div class="p-25">
                                <h4 class="c-green f-400"><?php echo $this->lang->line('notes'); ?> :</h4>                        
                                <blockquote class="m-t-5">
                                    <p class="c-gray f-12"><?php echo $this->lang->line('notesorder'); ?> <?php echo $waktulimit; ?> WIB <?php echo $this->lang->line('hari'); ?> <?php echo $hari; ?>, <?php echo $harilimit.' '. $bulanlimit. ' '.$tahunlimit ?> <?php echo $this->lang->line('notpaidorder'); ?></p>
                                </blockquote>                                                                        
                            </div>
                        </div>
                    </div>
                    <?php 
                    }
                }
            ?>

        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>