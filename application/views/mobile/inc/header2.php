<!DOCTYPE html>
<html ng-app="cmApp">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="google-signin-client_id" content="947006145421-ubsui7grgk3j9bhi7icjmgjqsr3c1l7r.apps.googleusercontent.com">
  <meta name="theme-color" content="#008080"> 


  <title>Classmiles</title>
  <?php
  header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
  header("Expires: Sat, 26 Jul 2097 05:00:00 GMT"); // Date in the past
  ?>
  <!-- ________________________________________________________START DIPAKAI_______________________________________________________________________________________ -->

  <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css" rel="stylesheet">
  
  <link href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js" rel="stylesheet">    
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" rel="stylesheet">  
  <link href="<?php echo base_url(); ?>aset/vendors/bower_components/chosen/chosen.min.css" rel="stylesheet">  
  <link href="<?php echo base_url(); ?>aset/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>aset/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>aset/css/app.min.1.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>aset/css/app.min.2.css" rel="stylesheet">
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

  <?php
      $iduser = $this->session->userdata('id_user');
      $querycek = $this->db->query("SELECT count(*) as jumlah FROM tbl_notif WHERE id_user='$iduser' AND read_status=0")->row_array();
      $jumlahnotif = $querycek['jumlah'];
  ?>
  <link rel="icon" href="<?php echo base_url(); ?>aset/img/<?php if ($jumlahnotif > 0){ echo 'favicon.ico'; } else { echo 'cm.ico'; } ?>">

  <link href="<?php echo base_url(); ?>aset/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/sel2/css/select2.css" />  
  <link href="<?php echo base_url(); ?>aset/css/custom.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>aset/css/css.css" rel="stylesheet">
  <!-- LOAD ANGULAR -->
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/js/anjeondemand.js"></script>
  <!-- <script src="<?php echo base_url(); ?>aset/js/anjenavbar.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/ngInfiniteScroll/1.3.0/ng-infinite-scroll.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>aset/dhtmlxscheduler/dhtmlxscheduler_flat.css" type="text/css" media="screen" title="no title"
        charset="utf-8">

  <style type="text/css">
      /* scroller browser */
      ::-webkit-scrollbar {
          width: 9px;
      }

      /* Track */
      ::-webkit-scrollbar-track {
          -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
          -webkit-border-radius: 7px;
          border-radius: 7px;
      }

      /* Handle */
      ::-webkit-scrollbar-thumb {
          -webkit-border-radius: 7px;
          border-radius: 7px;
          background: #a6a5a5;
          -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
      }
      ::-webkit-scrollbar-thumb:window-inactive {
          background: rgba(0,0,0,0.4); 
      }
</style>

  <!-- UNTUK VIDEO5HTML -->        
<!--   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/video5/css/jquery.jscrollpane.css" media="all" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/video5/css/jquery.selectbox.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/video5/css/videoGallery_scroll.css" /> -->

  <!-- <link href="<?php echo base_url(); ?>aset/vendors/bower_components/mediaelement/build/mediaelementplayer.css" rel="stylesheet"> -->
  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>aset/css/jslider.css" type="text/css"> -->

  <!-- ______________________________________________________ DIPAKAI TUTUP________________________________________________________________________________________ -->


  
  <!-- ______________________________________________________START TIDAK DIPAKAI___________________________________________________________________________________ -->
<!--  -->
  <!-- <link href="<?php echo base_url(); ?>aset/vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css" rel="stylesheet"> -->
  <!-- <link href="<?php echo base_url(); ?>aset/vendors/farbtastic/farbtastic.css" rel="stylesheet"> -->
  <!-- <link href="<?php echo base_url(); ?>aset/vendors/summernote/dist/summernote.css" rel="stylesheet"> -->
  <!-- <link href="<?php echo base_url(); ?>aset/vendors/bower_components/alertify/alertify.core.css" rel="stylesheet"> -->


  <!-- <link href="<?php echo base_url(); ?>aset/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet"> -->
  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>aset/font-awesome/css/font-awesome.min.css"> -->
  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>aset/css/form-elements.css"> -->
  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>aset/css/style.css"> -->
  <!--   <link href="<?php echo base_url(); ?>aset/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="<?php echo base_url(); ?>aset/css/jquery.awesome-cropper.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/css/imgareaselect.css"> -->

  <!-- _____________________________________________________TIDAK DIPAKAI TUTUP___________________________________________________________________________________ -->

  <style type="text/css">
    .toggle-switch .ts-label {
      min-width: 130px;
    }
    html, body {
      /*margin: 0px;
      padding: 0px;*/
      /*height: 100%;*/
      overflow: auto;
    }     

  </style>
  
  

</head>

<body <?php if(isset($sat)){ echo $sat; } ?> ng-controller="ModelController as ondemand">
  <!-- Javascript Libraries -->
  
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/alertify/alertify.js "></script> -->
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script> 
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>   -->
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/moment/min/moment.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/fullcalendar.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/vendors/bootgrid/jquery.bootgrid.updated.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/sel2/js/select2.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/js/functions.js"></script>  
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/Waves/dist/waves.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.4/js/fileinput.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/js/scripts.js"></script>
  <script src="<?php echo base_url(); ?>aset/js/jquery.smartWizard.js"></script>
  <script src="<?php echo base_url(); ?>aset/js/custom.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>aset/camerajs/webcam.js"></script> 
  <!-- <script src="https://apis.google.com/js/api:client.js"></script>
  <script src="https://apis.google.com/js/platform.js" async defer></script> -->    
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js"></script>
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/mediaelement/build/mediaelement-and-player.min.js"></script> -->
  <script>
    var FileAPI = {
        debug: true
      , media: true
      , staticPath: './FileAPI/'
    };
  </script>
  <script src="<?php echo base_url(); ?>aset/FileAPI/FileAPI.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/FileAPI/FileAPI.exif.js"></script>
  <script src="<?php echo base_url(); ?>aset/jquery.fileapi.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/bootstrap-filestyle.min.js"> </script>

  <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
  
  <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_editors.js" type="text/javascript" charset="utf-8"></script>
  <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_minical.js" type="text/javascript" charset="utf-8"></script>
  <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/<?php if ($this->session->userdata('lang') == 'indonesia') { echo 'locale_id'; } else if ($this->session->userdata('lang') == 'english'){ echo 'locale_en';} ?>.js" type="text/javascript" charset="utf-8"></script>
  <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_serialize.js" type="text/javascript" charset="utf-8"></script>
  <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_collision.js" type="text/javascript" charset="utf-8"></script>

  <!-- UNTUK DELETE -->
  <!--<script type="text/javascript" src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/backbone.js"></script>-->
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/underscore.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_mvc.js?!"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jquery.hotkeys.js"></script>
 
  <!-- <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_readonly.js" type="text/javascript" charset="utf-8"></script> -->

  <!-- UNTUK VIDEO5HTML -->
  <!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/swfobject.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.dotdotdot-1.5.1.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.address.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.mousewheel.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.jscrollpane.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.selectbox-0.2.js"></script>
  <script type="text/javascript" src="http://www.youtube.com/player_api"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.apPlaylistManager.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.apYoutubePlayer.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.vg.settings_scroll.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.func.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.vg.func.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.videoGallery.min.js"></script> -->

  <!-- JS SLIDER - START -->
<!--   <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jslider/tmpl.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jslider/jquery.dependClass-0.1.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jslider/draggable-0.1.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jslider/jquery.slider.js"></script>  -->   
  <!-- JS SLIDER STOP -->
 <!--  <style type="text/css" media="screen">    
  body {  }
   .layout { margin-bottom: 10%; font-family: Georgia, serif; }
   .layout-slider { margin-bottom: 60px; width: 50%; }
   .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
   .layout-slider-settings pre { font-family: Courier; }
  </style> -->
<!--   <style type="text/css" media="screen">
   .layout { margin-bottom: 10%; font-family: Georgia, serif;}
   .layout-slider { margin-bottom: 60px; width: 50%; }
   .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
   .layout-slider-settings pre { font-family: Courier; }
  </style> -->
    
  <!-- TIDAK DI PAKAI -->
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/summernote/dist/summernote-updated.min.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/flot/jquery.flot.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/flot/jquery.flot.resize.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/flot.curvedlines/curvedLines.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/sparklines/jquery.sparkline.min.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>      -->
  <!-- <script src="<?php echo base_url(); ?>aset/js/flot-charts/curved-line-chart.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>aset/js/flot-charts/line-chart.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>aset/js/charts.js"></script>     -->
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/farbtastic/farbtastic.min.js"></script>   -->
  <!-- <script src="<?php echo base_url(); ?>aset/js/jquery.backstretch.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>aset/js/jquery.backstretch.min.js"></script> -->
  <!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/imgareaselect/scripts/jquery.imgareaselect.pack.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>aset/js/cropbox.js"></script>   -->
  <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script> -->


  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-91410789-1', 'auto');
  ga('send', 'pageview');

</script>
  <script>
    jQuery(function ($){
      var $blind = $('.splash__blind');
    
   
      $('.splash')
        .mouseenter(function (){
          $('.splash__blind', this)
            .animate({ top: -10 }, 'fast', 'easeInQuad')
            .animate({ top: 0 }, 'slow', 'easeOutBounce')
          ;
        })
        .click(function (){
          if( !FileAPI.support.media ){
            $blind.animate({ top: -$(this).height() }, 'slow', 'easeOutQuart');
          }

          FileAPI.Camera.publish($('.splash__cam'), function (err, cam){
            if( err ){
              alert("Unfortunately, your browser does not support webcam.");
            } else {
              $('.splash').off();
              $blind.animate({ top: -$(this).height() }, 'slow', 'easeOutQuart');
            }
          });
        })
      ;   

      $('body').on('click', '[data-tab]', function (evt){
        evt.preventDefault();

        var el = evt.currentTarget;
        var tab = $.attr(el, 'data-tab');
        var $example = $(el).closest('.example');

        $example
          .find('[data-tab]')
            .removeClass('active')
            .filter('[data-tab="'+tab+'"]')
              .addClass('active')
              .end()
            .end()
          .find('[data-code]')
            .hide()
            .filter('[data-code="'+tab+'"]').show()
        ;
      });

      // function init() {
      //     scheduler.config.multi_day = true;
      //     var pizza_size = [
      //       { key: 1, label: 'Small' },
      //       { key: 2, label: 'Medium' },
      //       { key: 3, label: 'Large' }
      //     ];

      //     scheduler.config.lightbox.sections = [
                    
      //     ];

      //     scheduler.config.full_day = true;

      //     scheduler.config.xml_date = "%Y-%m-%d %h:%i";
      //     scheduler.config.details_on_create = true;

      //     scheduler.attachEvent("onBeforeLightbox", function (id){
      //         var event = scheduler.getEvent(id);
      //       event.text = "";
      //         return true;
      //     });

      //     scheduler.init("scheduler_here",new Date(2016,11,15),"week");
      //     scheduler.parse(small_events_list, "json");
      // }

      function _getCode(node, all){
        var code = FileAPI.filter($(node).prop('innerHTML').split('\n'), function (str){ return !!str; });
        if( !all ){
          code = code.slice(1, -2);
        }

        var tabSize = (code[0].match(/^\t+/) || [''])[0].length;

        return $('<div/>')
          .text($.map(code, function (line){
            return line.substr(tabSize).replace(/\t/g, '   ');
          }).join('\n'))
          .prop('innerHTML')
            .replace(/ disabled=""/g, '')
            .replace(/&amp;lt;%/g, '<%')
            .replace(/%&amp;gt;/g, '%>')
        ;
      }


      // Init examples
      /*FileAPI.each(examples, function (fn){
        fn();
      });*/
    });
  </script>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-16483888-3', 'rubaxa.github.io');
    ga('send', 'pageview');
  </script>
  


  <div id="fb-root"></div>
  <script type="text/javascript">



    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1647513202217309',
        xfbml      : true,
        version    : 'v2.8'
      });
      FB.AppEvents.logPageView();
      FB.getLoginStatus(function (response) {

      })
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));

    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        alert(profile.getName());
        console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
        console.log('Name: ' + profile.getName());
        console.log('Image URL: ' + profile.getImageUrl());
        console.log('Email: ' + profile.getEmail());
    }
    function signOut() {
      FB.logout(function(response) {
        alert(JSON.stringify(response));
      });
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function () {
        console.log('User signed out.');
      });
      
    }
        function onLoad() {
          gapi.load('auth2', function() {
            auth2 = gapi.auth2.init({
              client_id: '947006145421-ubsui7grgk3j9bhi7icjmgjqsr3c1l7r.apps.googleusercontent.com',
                // scope: 'profile email'
              });
          });
        }
        var pfp = "AAA";
        var pfka = "AAA";
        var pfkec = "AAA";
        var jname = '';
        $(document).ready(function() {
          var date = new Date();
          var d = date.getDate();
          var m = date.getMonth();
          var y = date.getFullYear();

          $.ajaxSetup({
            type:"POST",
            url: "<?php echo base_url()?>First/ambil_data",
            cache: false,
          });
          
          $('select.select2').each(function(){
            if($(this).attr('id') == 'jenjang_name'){
              $(this).select2({
                delay: 2000,
                ajax: {
                  dataType: 'json',
                  type: 'GET',
                  url: '<?php echo base_url(); ?>ajaxer/ajax_jenjang_name/',
                  data: function (params) {
                    return {
                      term: params.term,
                      page: params.page || 1,
                      jname: jname,
                    };
                  },
                  processResults: function(data){
                    return {
                      results: data.results,
                      pagination: {
                        more: data.more
                      }
                    };
                  }
                }
              });
            }else if($(this).attr('id') == 'jenjang_level'){
              $(this).select2({
                  // minimumInputLength: 3,
                  delay: 2000,
                  ajax: {
                    dataType: 'json',
                    type: 'GET',
                    url: '<?php echo base_url(); ?>ajaxer/ajax_jenjang_level/',
                    data: function (params) {
                      return {
                        term: params.term,
                        page: params.page || 1,
                        jname: jname,
                      };
                    },
                    processResults: function(data){
                      return {
                        results: data.results,
                        pagination: {
                          more: data.more
                        }
                      };
                    }
                  }
                });
            }else if($(this).attr('id') == 'provinsi_id' || $(this).attr('id') == 'kecamatan_id' || $(this).attr('id') == 'kabupaten_id' || $(this).attr('id') == 'desa_id'){
              $(this).select2({
                minimumInputLength: 1,
                delay: 2000,
                ajax: {
                  dataType: 'json',
                  type: 'GET',
                  url: '<?php echo base_url(); ?>ajaxer/ajax_indonesia/' + $(this).attr('id'),
                  data: function (params) {
                    return {
                      term: params.term,
                      page: params.page || 1,
                      pfp: pfp,
                      pfka: pfka,
                      pfkec: pfkec
                    };
                  },
                  processResults: function(data){
                    return {
                      results: data.results,
                      pagination: {
                        more: data.more
                      }
                    };
                  }
                }
              });

            }
            else if($(this).attr('name') == "educational_level"){
              $(this).select2({
                delay: 2000,
                ajax: {
                  dataType: 'json',
                  type: 'GET',
                  url: '<?php echo base_url(); ?>ajaxer/getJenjangLevel',
                  data: function (params) {
                    return {
                      term: params.term,
                      page: params.page || 1
                    };
                  },
                  processResults: function(data){
                    return {
                      results: data.results,
                      pagination: {
                        more: data.more
                      }
                    };
                  }
                }
              });

            }
            else{
              $(this).select2();
            }
          });

          $("#jenjang_name").change(function(){
            jname = $(this).val();
            $('#jenjang_level.select2').select2("val", "");

          });
          $('#provinsi_id').change(function(){
            var prov = $(this).val();
            pfp = prov;
            $('#kabupaten_id.select2').select2("val","");
          });
          $('#kabupaten_id').change(function(){
            var prov = $(this).val();
            pfka = prov;
            $('#kecamatan_id.select2').select2("val","");
          });
          $('#kecamatan_id').change(function(){
            var prov = $(this).val();
            pfkec = prov;
            $('#desa_id.select2').select2("val","");
          });
          $('#desa_id').change(function(){
            var prov = $(this).val();
            $.ajax({
              type: "POST",
              url: '<?php echo base_url(); ?>ajaxer/getPos/'+prov,
              success:function(datis){
                $('#kodepos').val(datis);
              }
            });
          });

          $('#wizard').smartWizard({
            enableFinishButton: false,
            hideButtonsOnDisabled: true,
            labelFinish:'Submit for Approval',
            reverseButtonsOrder: false,
            onLeaveStep:leaveAStepCallback,
            onFinish:validationFinishCallback
          });

          $('#wizard_verticle').smartWizard({
            transitionEffect: 'slide'
          });

          $('.buttonNext').addClass('btn btn-success');
          $('.buttonPrevious').addClass('btn btn-primary');
          $('.buttonFinish').addClass('btn btn-warning disabled');
          $('.buttonPrevious').click(function(){
            $('.buttonFinish').show('none');
          });
          $('.buttonNext').click(function(){
            $('.buttonFinish').show('block');
          });

          function leaveAStepCallback(obj, context){

                    return validateSteps(); // return false to stay on step and true to continue navigation 
                  }

                  function validateSteps(stepnumber){
                    var isStepValid = true;
                    // validate step 1
                    
                    if($('#user_birthplace').val() == ''){
                      $('#user_birthplace').focus();
                      $("#valid_modal").modal('show');
                      $('#cek_tempat').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                      return false;                       
                    }
                    if($('#user_religion').val() == '') {
                      $('#cek_agama').focus();
                      $("#valid_modal").modal('show');
                      $('#cek_agama').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                      return false;
                    }
                    if($('#user_address').val() == '') {
                      $('#user_address').focus();
                      $("#valid_modal").modal('show');
                      $('#cek_alamat').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                      return false;
                    }
                    if($('#provinsi').val() == '') {
                      $('#cek_provinsi').focus();
                      $("#valid_modal").modal('show');
                      $('#cek_provinsi').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                      return false;
                    }
                    if($('#kabupaten').val() == '') {
                      $('#cek_kabupaten').focus();
                      $("#valid_modal").modal('show');
                      $('#cek_kabupaten').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                      return false;
                    }
                    if($('#kecamatan').val() == '') {
                      $('#cek_kecamatan').focus();
                      $("#valid_modal").modal('show');
                      $('#cek_kecamatan').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                      return false;
                    }
                    if($('#kelurahan').val() == '') {
                      $('#cek_keluarahan').focus();
                      $("#valid_modal").modal('show');
                      $('#cek_keluarahan').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                      return false;
                    }
                    var o = {};
                    var a = $('#approvalForm_account').serializeArray();
                    $.each(a, function() {
                      if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                          o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                      } else {
                        o[this.name] = this.value || '';
                      }
                    });
                    var y = {};
                    var b = $('#approvalForm_profile').serializeArray();
                    $.each(b, function() {
                      if (y[this.name] !== undefined) {
                        if (!y[this.name].push) {
                          y[this.name] = [y[this.name]];
                        }
                        y[this.name].push(this.value || '');
                      } else {
                        y[this.name] = this.value || '';
                      }
                    });
                    $.ajax({
                      url: '<?php echo base_url(); ?>master/tutor_tempApproval/<?php echo $this->session->userdata('id_user'); ?>',
                      type: 'POST',
                      data: {
                        'form_account': JSON.stringify(o),
                        'form_profile': JSON.stringify(y)
                      },
                      success: function(hasil){
                        // location.href='<?php echo base_url(); ?>tutor';
                      }
                    });                

                    // if($('#kodepos').val() == '') {
                    //   $('#cek_kodepos').focus();
                    //   $("#valid_modal").modal('show');
                    //   // $('#cek_kodepos').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                    //   return false;
                    // }  
                    // if($('#imgdata').val() == '') {
                    //   $('#imgdata').focus();
                    //   $("#valid_modal").modal('show');

                    //   return false;
                    // }  
                    // if($('#name').val() == '') {
                    //   $('#name').focus();
                    //   $("#valid_modal").modal('show');
                    //   return false;
                    // }                


                    // validasi step 2
                    // if($('#educational_level').val() == '') {
                    //     alert('Please complete your data');
                    //     return false;
                    // } 

                  
                    return true;   
                  }
                  
                  function validationFinishCallback(argument) {
                    var isStepValid = true;
                    // validate step 1
                    
                    if($('#educational_level1').val() == ''){
                     $("#valid_modal").modal('show');
                      return false;                       
                    }
                    if($('#educational_competence1').val() == '') {
                      $("#valid_modal").modal('show');
                      return false;
                    }
                    if($('#educational_institution1').val() == '') {
                      $("#valid_modal").modal('show');
                      return false;
                    }
                    if($('#institution_address1').val() == '') {
                      $("#valid_modal").modal('show');
                      return false;
                    }
                    if($('#graduation_year1').val() == '') {
                      $("#valid_modal").modal('show');
                      return false;
                    }
                    if($('#description_experience1').val() == '') {
                      $("#valid_modal").modal('show');
                      return false;
                    }
                    if($('#year_experience1').val() == '') {
                      $("#valid_modal").modal('show');
                      return false;
                    }
                    if($('#year_experience2').val() == '') {
                      $("#valid_modal").modal('show');
                      return false;
                    }  
                    if($('#place_experience1').val() == '') {
                      $("#valid_modal").modal('show');
                      return false;
                    }  
                    if($('#information_experience1').val() == '') {
                      $("#valid_modal").modal('show');
                      return false;
                    }
                    

                    onFinishCallback(argument);
                  }

                  function onFinishCallback(argument) {
                    // $('#approvalForm_account').submit();
                    var o = {};
                    var a = $('#approvalForm_account').serializeArray();
                    $.each(a, function() {
                      if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                          o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                      } else {
                        o[this.name] = this.value || '';
                      }
                    });
                    var y = {};
                    var b = $('#approvalForm_profile').serializeArray();
                    $.each(b, function() {
                      if (y[this.name] !== undefined) {
                        if (!y[this.name].push) {
                          y[this.name] = [y[this.name]];
                        }
                        y[this.name].push(this.value || '');
                      } else {
                        y[this.name] = this.value || '';
                      }
                    });
                    $.ajax({
                      url: '<?php echo base_url(); ?>master/tutor_submitApproval/<?php echo $this->session->userdata('id_user'); ?>',
                      type: 'POST',
                      data: {
                        'form_account': JSON.stringify(o),
                        'form_profile': JSON.stringify(y)
                      },
                      success: function(hasil){
                        location.href='<?php echo base_url(); ?>tutor';
                      }
                    });
                  }

                  $("#provinsi").change(function(){
                    var value=$(this).val();                
                    if(value>0){
                      $.ajax({                    
                        data:{modul:'kabupaten',id:value},
                        success: function(respond){
                          $("#kabupaten").html(respond);
                        }
                      })
                    }

                  });


                  $("#kabupaten").change(function(){
                    var value=$(this).val();                
                    if(value>0){
                      $.ajax({
                        data:{modul:'kecamatan',id:value},
                        success: function(respond){
                          $("#kecamatan").html(respond);
                        }
                      })
                    }
                  });

                  $("#kecamatan").change(function(){
                    var value=$(this).val();
                    if (value>0) {
                      $.ajax({
                        data:{modul:'kelurahan',id:value},
                        success: function(respond){
                          $("#kelurahan").html(respond);
                        }
                      })
                    }
                  });

                  $("#jenjangname").change(function(){
                // var value=$(this).val();  
                var name = $("#jenjangname").val();              
                // alert(id);
                // if (value>0) {
                  $.ajax({
                    type:"POST",
                    url: "<?php echo base_url()?>First/getlevels",
                    cache: false,
                    data:{"name":name},
                    success: function(respond){
                      $("#jenjanglevel").html(respond);
                    }
                  })
                // }
              });


                var cId = $('#calendar'); //Change the name if you want. I'm also using thsi add button for more actions

                $('#buttonSession').click(function(){
                  event.preventDefault();
                  var url = $(this).attr('href');
                  location.href=url;
                  var session = '';

                    // $.ajax({
                    //     url: '<?php //echo base_url(); ?>/first/createSession',
                    //     type: 'POST',
                    //     success: function(data){
                    //         session = data;
                    //         var win = 
                    //         win.focus();
                    //         // $(this).trigger('click');
                    //     }
                    // });
                  });

                // $('#buttonfollow').click(function(){
                //     // event.preventDefault();
                //     var url = $(this).attr('href');
                //     location.href=url;


                    // $.ajax({
                    //     url: '/first/createSession',
                    //     type: 'POST',
                    //     success: function(data){
                    //         session = data;
                    //         var win = 
                    //         win.focus();
                    //         // $(this).trigger('click');
                    //     }
                    // });
                // });                

                $('#tanggal_lahir').on('keypress',function(e){

                  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                  }
                });
                $('#tahun_lahir').on('keypress',function(e){

                  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                  }
                });
                $('#no_hape').on('keypress',function(e){

                  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                  }
                });
                $('#no_hp_ortu').on('keypress',function(e){

                  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                  }
                });
                $('#user_callnum').on('keypress',function(e){

                  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                  }
                });

                $("#data-table-basic").bootgrid({
                  css: {
                    icon: 'zmdi icon',
                    iconColumns: 'zmdi-view-module',
                    iconDown: 'zmdi-expand-more',
                    iconRefresh: 'zmdi-refresh',
                    iconUp: 'zmdi-expand-less'
                  },
                });
                
                $("#data-table-selection").bootgrid({
                  css: {
                    icon: 'zmdi icon',
                    iconColumns: 'zmdi-view-module',
                    iconDown: 'zmdi-expand-more',
                    iconRefresh: 'zmdi-refresh',
                    iconUp: 'zmdi-expand-less'
                  },
                  selection: true,
                  multiSelect: true,
                  rowSelect: true,
                  keepSelection: true
                });
                
                //Generate the Calendar
                cId.fullCalendar({
                  header: {
                    right: '',
                    center: 'prev, title, next',
                    left: ''
                  },

                    theme: true, //Do not remove this as it ruin the design
                    selectable: true,
                    selectHelper: true,
                    editable: true,

                    //Add Events
                    events: [
                    {
                      title: 'Matematika',
                      start: new Date(y, m, 1),
                      allDay: true,
                      className: 'bgm-cyan'
                    },
                    {
                      title: 'Meeting with client',
                      start: new Date(y, m, 10),
                      allDay: true,
                      className: 'bgm-orange'
                    },
                    {
                      title: 'Repeat Event',
                      start: new Date(y, m, 18),
                      allDay: true,
                      className: 'bgm-amber'
                    },
                    {
                      title: 'Semester Exam',
                      start: new Date(y, m, 20),
                      allDay: true,
                      className: 'bgm-green'
                    },
                    {
                      title: 'Soccor match',
                      start: new Date(y, m, 5),
                      allDay: true,
                      className: 'bgm-lightblue'
                    },
                    {
                      title: 'Coffee time',
                      start: new Date(y, m, 21),
                      allDay: true,
                      className: 'bgm-orange'
                    },
                    {
                      title: 'Job Interview',
                      start: new Date(y, m, 5),
                      allDay: true,
                      className: 'bgm-amber'
                    },
                    {
                      title: 'IT Meeting',
                      start: new Date(y, m, 5),
                      allDay: true,
                      className: 'bgm-green'
                    },
                    {
                      title: 'Brunch at Beach',
                      start: new Date(y, m, 1),
                      allDay: true,
                      className: 'bgm-lightblue'
                    },
                    {
                      title: 'Live TV Show',
                      start: new Date(y, m, 15),
                      allDay: true,
                      className: 'bgm-cyan'
                    },
                    {
                      title: 'Software Conference',
                      start: new Date(y, m, 25),
                      allDay: true,
                      className: 'bgm-lightblue'
                    },
                    {
                      title: 'Coffee time',
                      start: new Date(y, m, 30),
                      allDay: true,
                      className: 'bgm-orange'
                    },
                    {
                      title: 'Job Interview',
                      start: new Date(y, m, 30),
                      allDay: true,
                      className: 'bgm-green'
                    },
                    ],

                    //On Day Select
                    select: function(start, end, allDay) {
                      $('#addNew-event').modal('show');   
                      $('#addNew-event input:text').val('');
                      $('#getStart').val(start);
                      $('#getEnd').val(end);
                    }
                  });

                $('#gunakanCamera').click(function(){
                  $(this).attr('class','btn btn-info active');
                  $('#unggahFoto').attr('class','btn btn-default');
                  Webcam.attach( '#my_camera' );
                  $('#take_pics').prop('disabled',false);
                  $('#retake_pics').prop('disabled',true);
                  $('#my_camera').css('display','block');                      
                  $('#imgprev').css('display','none');
                  $('#pics_div').css('display','block');
                  $('#imgprev').css('visibility','visible');
                  $('#upload_foto').css('display','none');
                  $('#imgfile').val('');              
                });
                $('#unggahFoto').click(function(){
                  $(this).attr('class','btn btn-info active');
                  $('#gunakanCamera').attr('class','btn btn-default');
                  Webcam.reset();
                  $('#imgfile').css('display','block');
                  $('#my_camera').css('display','none');
                  $('#pics_div').css('display','none'); 
                  $('#imgprev').css('visibility','inherit');
                  $('#upload_foto').css('display','block');
                });
                $('#take_pics').click(function(){
                  Webcam.snap( function(data_uri) {
                    $('#imgdata').html(data_uri);
                  });
                  Webcam.freeze();
                  $(this).prop('disabled',true);
                  $('#retake_pics').prop('disabled',false);
                  $('my_camera').css('display','none');
                  $('my_camera').css('display','block');

                });
                $('#retake_pics').click(function(){
                  $('#imgdata').html('');
                  Webcam.unfreeze();
                  $(this).prop('disabled',true);
                  $('#take_pics').prop('disabled',false);
                });
                $('#imgfile').change(function(){
                  readURL(this);
                });
                function readURL(input){
                  var file = document.getElementById('imgfile').files[0];
                  var inp = $("#imgfile");

                  if(file && file.size < 2000000) { // 10 MB (this size is in bytes)
                      console.warn('nothing');   
                      var reader = new FileReader();

                      reader.onload = function(e){
                        $('#imgprev').attr('src', e.target.result);
                        $('#imgdata').html(e.target.result);
                        $('#imgprev').css('visibility','visible');
                        $('#imgprev').css('display','block');
                      }  
                      reader.readAsDataURL(input.files[0]);                               
                  } else {
                      //Prevent default and display error                      
                      var $el = $('#imgfile');
                      $el.wrap('<form>').closest('form').get(0).reset();
                      $el.unwrap();
                      $("#modal_foto").modal("show");                         
                  }
                  // if(input.files && input.files[0]){
                  //   var reader = new FileReader();

                  //   reader.onload = function(e){
                  //     $('#imgprev').attr('src', e.target.result);
                  //     $('#imgdata').html(e.target.result);
                  //     $('#imgprev').css('visibility','visible');
                  //     $('#imgprev').css('display','block');
                  //   }

                  //   reader.readAsDataURL(input.files[0]);
                  // }
                }



                //Create and ddd Action button with dropdown in Calendar header. 
                var actionMenu = '<ul class="actions actions-alt" id="fc-actions">' +
                '<li class="dropdown">' +
                '<a href="" data-toggle="dropdown"><i class="zmdi zmdi-more-vert"></i></a>' +
                '<ul class="dropdown-menu dropdown-menu-right">' +
                '<li class="active">' +
                '<a data-view="month" href="">Month View</a>' +
                '</li>' +                                            
                '<li>' +
                '<a data-view="basicWeek" href="">Week View</a>' +
                '</li>' +
                '<li>' +                                            
                '<a data-view="agendaWeek" href="">Agenda Week View</a>' +
                '</li>' +                                            
                '<li>' +
                '<a data-view="basicDay" href="">Day View</a>' +
                '</li>' +
                '<li>' +
                '<a data-view="agendaDay" href="">Agenda Day View</a>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</li>';


                cId.find('.fc-toolbar').append(actionMenu);
                
                //Event Tag Selector
                (function(){
                  $('body').on('click', '.event-tag > span', function(){
                    $('.event-tag > span').removeClass('selected');
                    $(this).addClass('selected');
                  });
                })();

                //Add new Event
                $('body').on('click', '#addEvent', function(){
                  var eventName = $('#eventName').val();
                  var tagColor = $('.event-tag > span.selected').attr('data-tag');

                  if (eventName != '') {
                        //Render Event
                        $('#calendar').fullCalendar('renderEvent',{
                          title: eventName,
                          start: $('#getStart').val(),
                          end:  $('#getEnd').val(),
                            // allDay: true,
                            className: tagColor
                            
                        },true ); //Stick the event
                        
                        $('#addNew-event form')[0].reset()
                        $('#addNew-event').modal('hide');
                      }
                      
                      else {
                        $('#eventName').closest('.form-group').addClass('has-error');
                      }
                    });   

                //Calendar views
                $('body').on('click', '#fc-actions [data-view]', function(e){
                  e.preventDefault();
                  var dataView = $(this).attr('data-view');
                  
                  $('#fc-actions li').removeClass('active');
                  $(this).parent().addClass('active');
                  cId.fullCalendar('changeView', dataView);  
                });
                //Basic Example
                $("#data-table-basic").bootgrid({
                  css: {
                    icon: 'zmdi icon',
                    iconColumns: 'zmdi-view-module',
                    iconDown: 'zmdi-expand-more',
                    iconRefresh: 'zmdi-refresh',
                    iconUp: 'zmdi-expand-less'
                  },
                });
                
                //Selection
                $("#data-table-selection").bootgrid({
                  css: {
                    icon: 'zmdi icon',
                    iconColumns: 'zmdi-view-module',
                    iconDown: 'zmdi-expand-more',
                    iconRefresh: 'zmdi-refresh',
                    iconUp: 'zmdi-expand-less'
                  },
                  selection: true,
                  multiSelect: true,
                  rowSelect: true,
                  keepSelection: true
                });
                
                //Command Buttons
                $("#data-table-command").bootgrid({
                  css: {
                    icon: 'zmdi icon',
                    iconColumns: 'zmdi-view-module',
                    iconDown: 'zmdi-expand-more',
                    iconRefresh: 'zmdi-refresh',
                    iconUp: 'zmdi-expand-less'
                  },
                  formatters: {
                    "commands": function(column, row) {
                      return "<button type=\"button\" class=\"btn btn-icon command-edit waves-effect waves-circle\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button> " + 
                      "<button type=\"button\" class=\"btn btn-icon command-delete waves-effect waves-circle\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";
                    }
                  }
                });
              });



            </script>
      <script>
      function notify(from, align, icon, type, animIn, animOut, message){
                $.growl({
                    icon: icon,
                    title: ' ',
                    message: message,
                    url: ''
                },{
                        element: 'body',
                        type: type,
                        allow_dismiss: true,
                        placement: {
                                from: from,
                                align: align
                        },
                        offset: {
                            x: 20,
                            y: 85
                        },
                        spacing: 10,
                        z_index: 1031,
                        delay: 3500,
                        timer: 1000,
                        url_target: '_blank',
                        mouse_over: false,
                        animate: {
                                enter: animIn,
                                exit: animOut
                        },
                        icon_type: 'class',
                        template: '<div data-growl="container" class="alert" role="alert">' +
                                        '<button type="button" class="close" data-growl="dismiss">' +
                                            '<span aria-hidden="true">&times;</span>' +
                                            '<span class="sr-only">Close</span>' +
                                        '</button>' +
                                        '<span data-growl="icon"></span>' +
                                        '<span data-growl="title"></span>' +
                                        '<span data-growl="message"></span>' +
                                        '<a href="#" data-growl="url"></a>' +
                                    '</div>'
                });
            };
      </script>
      
           <!--  <div class="page-loader" style="background-color:#1274b3;">
              <div class="preloader pl-xl pls-white">
                <svg class="pl-circular" viewBox="25 25 50 50">
                  <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>

                <p>Please wait...</p>
              </div>
            </div>
             -->