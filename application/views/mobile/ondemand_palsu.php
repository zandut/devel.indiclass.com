<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1" ng-app="cmApp">
    <aside id="sidebar" class="sidebar c-overflow">

        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" ng-controller="ModelController as ondemand" >
        <div class="container">

            <div class="block-header">
                <h2><?php  echo $this->lang->line('ondemand'); ?></h2>

                <ul class="actions">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="#"><?php  echo $this->lang->line('home'); ?></a></li>
                            <li class="active"><?php  echo $this->lang->line('ondemand'); ?></li>
                        </ol>
                    </li>
                </ul>
            </div><!-- akhir block header -->           

            <br>
            <form>
                <div class="card" style="border-radius: 10px 10px 0px 0px;">
                    <div class="row">
                        <br>
                        <div class="col-sm-2">
                                <div class="form-input" style="margin-left: 10%;">
                                <!-- ini subject_id -->
                                <select class="selectpicker" id="subject_id" >
                                     <?php
                                     $allsub = $this->Rumus->getSubject();
                                     echo '<option value="0">Semua Subject</option>'; 
                                     foreach ($allsub as $row => $v) {
                                        echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].'</option>';
                                    }
                                    ?>
                                </select>
                                <input type="text" name="subjecta" id="subjecton" hidden="true">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="input-group form-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                <div class="dtp-container fg-line">
                                <!-- ini date -->
                                    <input id="dateon" type='text' class="form-control date-picker" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="input-group form-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                <div class="dtp-container fg-line">
                                <!-- ini time -->
                                    <select class='selectpicker' id="time">
                                    <option value="0"><?php echo $this->lang->line('searchtime'); ?></option>
                                    <?php                 
                                        for ($i=0; $i < 24; $i++) { 
                                          for($y = 0; $y<=45; $y+=15){
                                            // echo "<a href='". base_url('first/video')."'><button class='btntime' style=''>".sprintf("%02d",$i).":".sprintf('%02d',$y)."</button></a>";        
                                            echo "<option value=".sprintf('%02d',$i).":".sprintf('%02d',$y).">".sprintf('%02d',$i).":".sprintf('%02d',$y)."</option>";                                            
                                          }
                                        }
                                    ?>
                                    </select>  
                                    <input type="text" name="timea" id="timeon" hidden="true">                              
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="input-group form-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                <div class="dtp-container fg-line">
                                <!-- ini time -->
                                    <select class="selectpicker" id="duration" >
                                        <option value="0"><?php echo $this->lang->line('duration'); ?></option>
                                        <option value="15">15 Minutes</option>
                                        <option value="30">30 Minutes</option>
                                        <option value="45">45 Minutes</option>
                                        <option value="60">60 Minutes</option>
                                    </select>
                                    <input type="text" name="durationa" id="durationon" hidden="true">
                                </div>
                            </div>
                        </div>

                    <div class="col-sm-2" >
                        <button ng-click="getondemand()"  class="btn btn-primary btn-block" style="margin-left: 10%;"><?php echo $this->lang->line('button_search'); ?></button> 
                    </div>
                </div>

                <div class="card col-sm-12 bgm-blue " style="height:3px; margin-top:19px;">            
                </div>            
                <br>
            </div>     
        </form>

        <!-- awal card -->
        <div class="card">
            <div class="card-header bgm-bluegray" ng-show="!firste">
                <label class="c-white">Result 2</label>
            </div>

            <div class="card-body card-padding">
                
                <div class="alert alert-success alert-dismissible text-center" role="alert" ng-show="firste">                                
                    <label>Tidak ada data.</label>
                </div>  

                <div class="contacts c-profile clearfix row">  
                    <div class="col-md-2 col-sm-2 col-xs-2" ng-repeat=" x in datas">
                        <div class="c-item">
                            <a href="" class="ci-avatar">                                       
                                <img ng-src="{{x.user_image}}" style="border-radius: 50%;" alt="">
                            </a>

                            <div class="c-info rating-list">
                                <strong>{{x.user_name}}</strong>
                                <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                <label ng-if="x.exact == '1'">{{times}}</label>
                                <!-- <div class="rl-star">
                                    <i class="zmdi zmdi-star active"></i>
                                    <i class="zmdi zmdi-star active"></i>
                                    <i class="zmdi zmdi-star active"></i>
                                    <i class="zmdi zmdi-star active"></i>
                                    <i class="zmdi zmdi-star"></i>
                                </div> -->
                            </div>

                            <div class="c-footer"> 

                                <button ng-if="x.exact == '1'" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{times}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-confirm btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                <button ng-if="x.exact == '0'" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-confirm btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                            </div>

                            <!-- Modal Small -->    
                            <div class="modal fade" id="modalconfrim" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Confrim</h4>
                                            <hr>
                                        </div>
                                        <div class="modal-body">
                                            <p>Apakah anda yakin ingin memilih tutor ini ???</p>                                                    
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn-choose-demand btn btn-success" demand-link=""><?php echo $this->lang->line('ikutidemand');?></button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('logout');?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>        

                </div>
            </div>
        </div>  



        <!-- awal card -->
        <div class="card">
            <div class="card-header bgm-gray">
                <h2>Recommendation</h2>                
            </div>

            <div class="card-body card-padding">
                
                <div class="contacts c-profile clearfix row">  
                    <div class="col-md-2 col-sm-2 col-xs-2" ng-repeat=" x in datas">
                        <div class="c-item">
                            <a href="" class="ci-avatar">                                       
                                <img ng-src="{{x.user_image}}" style="border-radius: 50%;" alt="">
                            </a>

                            <div class="c-info rating-list">
                                <strong>{{x.user_name}}</strong>
                                <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                <label ng-if="x.exact == '1'">{{times}}</label>
                                <!-- <div class="rl-star">
                                    <i class="zmdi zmdi-star active"></i>
                                    <i class="zmdi zmdi-star active"></i>
                                    <i class="zmdi zmdi-star active"></i>
                                    <i class="zmdi zmdi-star active"></i>
                                    <i class="zmdi zmdi-star"></i>
                                </div> -->
                            </div>

                            <div class="c-footer"> 

                                <button ng-if="x.exact == '1'" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{times}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-confirm btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                <button ng-if="x.exact == '0'" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-confirm btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                            </div>

                            <!-- Modal Small -->    
                            <div class="modal fade" id="modalconfrim" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Confrim</h4>
                                            <hr>
                                        </div>
                                        <div class="modal-body">
                                            <p>Apakah anda yakin ingin memilih tutor ini ???</p>                                                    
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn-choose-demand btn btn-success" demand-link=""><?php echo $this->lang->line('ikutidemand');?></button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('logout');?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>

            </div>
        </div>

    </div>

    </div><!-- akhir container -->
</section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript" src="<?php echo base_url();?>aset/timepicker/jquery.timepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>aset/timepicker/jquery.timepicker.css" />

  <!-- <script type="text/javascript" src="<?php echo base_url();?>aset/timepicker/lib/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>aset/timepicker/lib/bootstrap-datepicker.css" /> -->

<!--   <script type="text/javascript" src="<?php echo base_url();?>aset/timepicker/lib/site.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>aset/timepicker/lib/site.css" /> -->

<script type="text/javascript">
    $('#subject_id').change(function(e){
        $('#subjecton').val($(this).val());
    });
    $('#time').change(function(e){
        $('#timeon').val($(this).val());
    });
    $('#duration').change(function(e){
        $('#durationon').val($(this).val());
    });
    /*$('.btn-confirm').click(function(e){
        alert('yes');
       
    });*/
    $(document.body).on('click', '.btn-confirm' ,function(e){
        var subject_id = $(this).attr('subject_id');
        var tutor_id = $(this).attr('tutor_id');
        var date = $(this).attr('date')+" "+$(this).attr('start_time')+":00";
        
        $('.btn-choose-demand').attr('demand-link','<?php echo base_url(); ?>/process/demand_me?start_time='+date+"&tutor_id="+tutor_id+"&subject_id="+subject_id);
        $('#modalconfrim').modal('show');
    });
    $(document.body).on('click', '.btn-choose-demand' ,function(e){
        $.get($(this).attr('demand-link'),function(data){
            alert(data);
        });
    });

    $(function() {
        $('#stepExample1').timepicker({ 
            'step': 15, 
            'timeFormat': 'H:i',
       });
        $("#stepExample1").css('padding','5px');
        $("#stepExample1").css('padding','5px');
        // $('#stepExample2').timepicker({
        //  'timeFormat': 'H:i',
        //     'step': function(i) {
        //         return (i%4) ? 15 : 45;
        //     }
        // });
    });
</script>