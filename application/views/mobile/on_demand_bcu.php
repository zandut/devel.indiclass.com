<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1" style="padding: 0; margin-top: 3%;" >
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>
    
    <section id="content">                

        <?php if($this->session->flashdata('mes_alert')){ ?>
        <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('mes_message'); ?>
        </div>
        <?php } ?>
        <br>
        
        <div class="col-md-12" style="background-image: url('../aset/img/headers/4.png'); height: auto; margin-bottom: 2%;">
    
            <div class="mini-charts" style="margin-left: 150px; margin-right: 150px; margin-top: 25px;">
                <div class="row">
                    <div class="col-sm-6 col-md-2">
                        
                    </div>
                    
                    <div class="col-sm-12 col-md-4 col-xs-12 m-t-10">
                        <a href="#">
                            <div class="mini-charts-item bgm-cyan">
                                <div class="clearfix">
                                    <div class="chart stats-bar-2"><img src="../aset/img/class_icon/Private_revisi.png" class="img-responsive" style="height: 65px; width: 65px; margin-left: 9px;" alt=""></div>
                                    <div class="count">
                                        <h2 class="m-l-20 m-t-10">Private Class</h2>
                                        <small class="m-l-20 m-t-5">Only you and tutor in class</small>                                            
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    
                    <div class="col-sm-12 col-md-4 col-xs-12 m-t-10">
                        <a href="#">
                        <div class="mini-charts-item bgm-lightgreen">
                            <div class="clearfix">
                                <div class="chart stats-bar-2"><img src="../aset/img/class_icon/Group_revisi.png" class="img-responsive" style="height: 65px; width: 65px; margin-left: 9px;" alt=""></div>
                                <div class="count">                                            
                                    <h2 class="m-l-20 m-t-10">Group Class</h2>
                                    <small class="m-l-20 m-t-5">4 students in class</small>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                    
                    <div class="col-sm-6 col-md-2">
                        
                    </div>
                    
                </div>
            </div>
            
        </div>        

        <br>
        <div class="container">
            <div class="block-header">
                <h2><?php  echo $this->lang->line('ondemand'); ?></h2>
                <!-- <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url() ?>first/"><?php  echo $this->lang->line('home'); ?></a></li>
                            <li class="active"><?php  echo $this->lang->line('ondemand'); ?></li>
                        </ol>
                    </li>
                </ul> -->
            </div><!-- akhir block header -->

            <div id="box_privateclass">
                <!-- <div class="card col-md-12 col-xs-12 bgm-white" style="margin-bottom: 3%; z-index: 5;">
                    <form>                
                        <div class="row m-t-10">
                            <div class="col-md-3" id="carisubject">
                                <div class="form-input p-r-15" style="margin-left: 10%;">
                                    
                                    <select class="select2 form-control" id="subject_id" >
                                        <?php
                                            $allsub = $this->Rumus->getSubject();
                                            echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line('allsubject').'</option>'; 
                                            foreach ($allsub as $row => $v) {                                            
                                                echo '<option value="'.$v['subject_id'].'">'.$this->lang->line('subject_'.$v['subject_id']).' - '.$v['jenjang_level'].' '.$v['jenjang_name'].'</option>';
                                            }
                                        ?>
                                    </select>
                                    <input type="text" name="subjecta" id="subjecton" hidden="true">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group form-group p-r-15">
                                    <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                    <div class="dtp-container fg-line">
                                        
                                        <input id="dateon" type='text' class="form-control date-picker" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group form-group p-r-15">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <div class="dtp-container fg-line">
                                        
                                        <select class='select2 form-control' id="time">
                                            <option disabled selected><?php echo $this->lang->line('searchtime'); ?></option>
                                            <?php                 
                                                for ($i=0; $i < 24; $i++) { 
                                                  for($y = 0; $y<=45; $y+=15){
      
                                                    echo "<option value=".sprintf('%02d',$i).":".sprintf('%02d',$y).">".sprintf('%02d',$i).":".sprintf('%02d',$y)."</option>";                                            
                                                }
                                            }
                                            ?>
                                        </select>  
                                        <input type="text" name="timea" id="timeon" hidden="true">                              
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group form-group p-r-15">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                    <div class="dtp-container fg-line">
                                        
                                        <select required class="select2 form-control" id="duration" >
                                            <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                            <option value="15">15 Minutes</option>
                                            <option value="30">30 Minutes</option>
                                            <option value="45">45 Minutes</option>
                                            <option value="60">60 Minutes</option>
                                        </select>
                                        <input type="text" name="durationa" id="durationon" hidden="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button ng-click="getondemandprivate()" id="button_search" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                            </div>
                        </div>                
                    </form>
                    
                </div> -->
                <div ng-show="loding" class="page-loader" style="background-color:black; opacity: 0.5;">
                    <div class="preloader pl-lg pls-white">
                        <svg class="pl-circular" viewBox="25 25 50 50">
                            <circle class="plc-path" cx="50" cy="50" r="20" />
                        </svg>

                        <p style="margin-left: -18px;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
                    </div>
                </div> 
                <div class="row">
                    <!-- <div class="col-md-12"> -->
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header ch-alt">
                                <h2>Cari Private Class</h2>
                            </div>

                            <div class="card-body card-padding">
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-input p-r-15" style="margin-left: 10%;">                                            
                                            <select class="select2 form-control" id="subject_id" >
                                                <?php
                                                    $allsub = $this->Rumus->getSubject();
                                                    echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line('allsubject').'</option>'; 
                                                    foreach ($allsub as $row => $v) {                                            
                                                        echo '<option value="'.$v['subject_id'].'">'.$this->lang->line('subject_'.$v['subject_id']).' - '.$v['jenjang_level'].' '.$v['jenjang_name'].'</option>';
                                                    }
                                                ?>
                                            </select>
                                            <input type="text" name="subjecta" id="subjecton" hidden="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group form-group p-r-15">
                                            <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                            <div class="dtp-container fg-line">                                                
                                                <input id="dateon" type='text' class="form-control date-picker" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group form-group p-r-15">
                                            <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                            <div class="dtp-container fg-line">                                                
                                                <select class='select2 form-control' id="time">
                                                    <option disabled selected><?php echo $this->lang->line('searchtime'); ?></option>
                                                    <?php                 
                                                        for ($i=0; $i < 24; $i++) { 
                                                          for($y = 0; $y<=45; $y+=15){  
                                                            echo "<option value=".sprintf('%02d',$i).":".sprintf('%02d',$y).">".sprintf('%02d',$i).":".sprintf('%02d',$y)."</option>";                                            
                                                        }
                                                    }
                                                    ?>
                                                </select>  
                                                <input type="text" name="timea" id="timeon" hidden="true">                              
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group form-group p-r-15">
                                            <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                            <div class="dtp-container fg-line">                                                
                                                <select required class="select2 form-control" id="duration" >
                                                    <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                                    <option value="15">15 Minutes</option>
                                                    <option value="30">30 Minutes</option>
                                                    <option value="45">45 Minutes</option>
                                                    <option value="60">60 Minutes</option>
                                                </select>
                                                <input type="text" name="durationa" id="durationon" hidden="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button ng-click="getondemandprivate()" id="button_search" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                                    </div>
                                </div>

                            </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="card">

                            <div class="card-header ch-alt" ng-show="resulte">
                                <h2>Hasil Pencarian : {{hasils.length}}</h2>
                            </div>

                            <div class="card-body card-padding">
                                <div class="row">
                                    <div class="alert alert-success alert-dismissible text-center" role="alert" ng-if="firste">                                
                                        <label><?php echo $this->lang->line('nodata'); ?></label>
                                    </div>
                                    <div class="col-md-12" ng-repeat=" x in datas | filter: { 'exact' : '1' } as hasils">
                                        <div class="media-demo col-md-12 m-b-20">
                                            <div class="media">
                                                <div class="pull-left">                                                    
                                                    <img class="media-object img-responsive m-t-5" ng-src="{{x.user_image}}" style="height: 120px; width: 120px;" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="col-md-6">
                                                        <h2 class="media-heading f-14">{{x.user_name}}</h2>
                                                        <label class="f-12">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                        <label ng-if="x.exact == '1'">{{times}}</label>
                                                    </div>
                                                    <div class="col-md-3">                                                        
                                                        <!-- <a data-toggle="modal" href="#modaladdfriends" class="btn btn-success waves-effect m-t-20"><i class="zmdi zmdi-check" ></i> Pilih<
                                                        /a> -->
                                                        <button ng-if="x.exact == '1'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{times}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                                        <button ng-if="x.exact == '0'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="card" ng-if="resulte">
                            <div class="card-header bgm-deeporange">
                                <h2>Recommendation</h2>                
                            </div>

                            <div class="card-body card-padding">
                                
                                <div class="contacts c-profile clearfix row">  
                                    <div class="col-md-2 col-sm-2 col-xs-2" ng-repeat=" x in datas | filter: { 'exact' : '0' }">
                                        <div class="c-item">
                                            <a href="" class="ci-avatar">                                       
                                                <img ng-src="{{x.user_image}}" style="border-radius: 50%;" alt="">
                                            </a>

                                            <div class="c-info rating-list">
                                                <strong>{{x.user_name}}</strong>
                                                <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                <label ng-if="x.exact == '1'">{{times}}</label>
                                            </div>

                                            <div class="c-footer"> 
                                                <button ng-if="x.exact == '1'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{times}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                                <button ng-if="x.exact == '0'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                                            </div>

                                            <!-- Modal Small -->    
                                            <div class="modal fade" id="modalconfrim" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Confrim</h4>
                                                            <hr>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Apakah anda yakin ingin memilih tutor ini ???</p>                                                    
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn-req-demand btn btn-success" demand-link=""><?php echo $this->lang->line('ikutidemand');?></button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('logout');?></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>                    
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
            <div id="buttonback" style="bottom: 60px; position: fixed; right: 60px; cursor: pointer; display: none; z-index: 100;">                
                <button class="btn btn-danger btn-icon waves-effect waves-circle waves-float"  id="btnkembali" title="Kembali">
                    <i class="zmdi zmdi-arrow-back" ></i>
                </button>                
            </div> 

            <!-- Modal Default -->  
            <div class="modal" id="modaladdfriends" tabindex="-1" role="dialog" style="top: 20%;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-lightblue">
                            <h4 class="modal-title c-white">Modal title</h4>
                        </div>
                        <div class="modal-body m-t-10">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sodales orci ante, sed ornare eros vestibulum ut. Ut accumsan vitae eros sit amet tristique. Nullam scelerisque nunc enim, non dignissim nibh faucibus ullamcorper. Fusce pulvinar libero vel ligula iaculis ullamcorper. Integer dapibus, mi ac tempor varius, purus nibh mattis erat, vitae porta nunc nisi non tellus. Vivamus mollis ante non massa egestas fringilla. Vestibulum egestas consectetur nunc at ultricies. Morbi quis consectetur nunc.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link">Save changes</button>
                            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Small -->    
            <div class="modal" id="modalconfrim" tabindex="-1" style="top: 15%;" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-lightblue">
                            <h4 class="modal-title c-white">Confrim</h4>                            
                        </div>
                        <div class="modal-body m-t-15">
                            <center>
                            <img id="image" style="border-radius: 1%; height: 40%; width: 40%;" class="m-b-5" alt=""><br>
                            <label><h3 id="namemodal"></h3></label><br>
                            <label id="timestart"></label>
                            <hr>
                            </center>
                            <label class="f-13"><?php echo $this->lang->line('checkdemand'); ?></label>
                        </div>
                        <div class="modal-footer">
                            <hr>
                            <button type="button" class="btn-req-demand btn btn-success" demand-link=""><?php echo $this->lang->line('ikutidemand');?></button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
                        </div>
                    </div>
                </div>
            </div>  
               

        </div><!-- akhir container -->            

    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript" src="<?php echo base_url();?>aset/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>aset/timepicker/jquery.timepicker.css" />

  <!-- <script type="text/javascript" src="<?php echo base_url();?>aset/timepicker/lib/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>aset/timepicker/lib/bootstrap-datepicker.css" /> -->

<!--   <script type="text/javascript" src="<?php echo base_url();?>aset/timepicker/lib/site.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>aset/timepicker/lib/site.css" /> -->

  <script type="text/javascript">
  $('input.timepicker').timepicker({
              timeFormat: 'HH:mm:ss',
              minTime: '11:45:00',
              maxHour: 20,
              maxMinutes: 30,
              startTime: new Date(0,0,0,15,0,0), // 3:00:00 PM - noon
              interval: 15 // 15 minutes
          });
    $('#subject_id').change(function(e){
        $('#subjecton').val($(this).val());
    });
    $('#time').change(function(e){
        $('#timeon').val($(this).val());
    });
    $('#duration').change(function(e){
        $('#durationon').val($(this).val());
    });

    $('#subject_idg').change(function(e){
        $('#subjectong').val($(this).val());
    });
    $('#timeg').change(function(e){
        $('#timeong').val($(this).val());
    });
    $('#durationg').change(function(e){
        $('#durationong').val($(this).val());
    });

    /*$('.btn-confirm').click(function(e){
        alert('yes');
       
    });*/
    $('#button_search').click(function(){
        // alert('sdaf');
    })
    $(document.body).on('click', '.btn-ask' ,function(e){
        var subject_id = $(this).attr('subject_id');
        var start_time = $(this).attr('start_time');
        var avtime_id = $(this).attr('avtime_id');
        var duration = $(this).attr('duration');      
        var user_name = $(this).attr('username');
        var image_user = $(this).attr('image');

        $('#namemodal').text(user_name);
        $('#timestart').text(start_time);
        $('#image').attr("src", image_user);        

        if (subject_id == '' && start_time == '' && avtime_id == '' && duration == '') {
            alert('tidak boleh kosong');
        }
        else
        {
            $('.btn-req-demand').attr('demand-link','<?php echo base_url(); ?>/process/demand_me?start_time='+start_time+"&avtime_id="+avtime_id+"&subject_id="+subject_id+"&duration="+duration);
            $('#modalconfrim').modal('show');
        }
    });
    $(document.body).on('click', '.btn-req-demand' ,function(e){
        $.get($(this).attr('demand-link'),function(data){
            location.reload();
        });
    });    

    $("#privateclassclick").click(function(){ 
        $("#kotakawal").css('display','none');        
        $("#kotakgroup").css('display','none');        
        
        $("#kotakprivate").css('display','block');
        $("#buttonback").css('display','block');
    });
    $("#groupclassclick").click(function(){
        $("#kotakawal").css('display','none');
        $("#kotakprivate").css('display','none');
        $("#buttonback").css('display','block');
        $("#kotakgroup").css('display','block');
    });
    $("#btnkembali").click(function(){         
        $("#kotakprivate").css('display','none');
        $("#buttonback").css('display','none');
        $("#kotakgroup").css('display','none');
        $("#kotakawal").css('display','block');
    });

</script>