<header id="header" class="clearfix" data-current-skin="<?php echo $this->session->userdata('color'); ?>">
  <?php 
  $this->load->view('inc/navbar');
  echo "<label hidden id='time_servernow'>".strtotime(date('Y-m-d H:i:s'))."</label>";

  ?>
</header>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">

    <aside id="sidebar" class="sidebar c-overflow">
      <?php $this->load->view('inc/side'); ?>
      <?php
               //$msg2 = echo $this->session->flashdata('msgSucces');
               //echo '<script>alert($msg2);</script>';
      ?>
    </aside>

    <section id="content">

      <div class="container">                
          <?php if($this->session->flashdata('mes_alert')){ ?>
          <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('mes_message'); ?>
          </div>
          <?php } ?>  

          <div class="block-header">
              <h2><?php echo $this->lang->line('uangsaku'); ?></h2>

              <ul class="actions">
                  <li>
                      <ol class="breadcrumb">
                          <li><a href="<?php echo base_url(); ?>
                            <?php 
                             if($this->session->userdata('status') == 1) 
                             { 
                               'first/';
                             } 
                             else{ 
                               echo "#"; 
                             } 
                            ?>"><?php echo $this->lang->line('home'); ?></a></li>
                          <li class="active"><?php echo $this->lang->line('uangsaku'); ?></li>
                          <!-- <li class="active"><?php echo $this->lang->line('transactionlist'); ?></li> -->

                      </ol>
                  </li>
              </ul>
          </div><!-- akhir block header --> 

          <div class="card">
              <div class="card-header">
                  <!-- <h2>Tabs <small>Add quick, dynamic tab functionality to transition through panes of local content, even via dropdown menus.</small></h2> -->
              </div>
              
              <div class="card-body card-padding">
                  <div role="tabpanel">
                      <ul class="tab-nav tn-justified tn-icon" role="tablist">
                          <li class="active"><a href="#home11" aria-controls="home11" role="tab" data-toggle="tab"><?php echo $this->lang->line('transactionlist'); ?></a></li>
                          <li><a href="#profile11" aria-controls="profile11" role="tab" data-toggle="tab"><?php echo $this->lang->line('accountlist'); ?></a></li>
                          <li><a href="#messages11" aria-controls="messages11" role="tab" data-toggle="tab">Top up</a></li>
                          <li><a href="#settings11" aria-controls="settings11" role="tab" data-toggle="tab">Cara Top up</a></li>
                      </ul>
                    
                      <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active" id="home11">
                              <div class="pull-left">
                                  <label class="f-17">Saldo anda : Rp. 100.000.000,00</label>
                              </div>
                              <div class="pull-right">
                                  
                                  <label class="f-17">Transaksi Bulan</label>
                                  
                                  <select required name="bulan_lahir" class="select2 col-md-6 form-control">
                                      <!-- <option disabled selected value=''><?php echo $this->lang->line('monthbirth'); ?></option> -->
                                      <option value="01" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "01"){ echo 'selected="true"';} ?>>Januari 2016</option>
                                      <option value="02" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "02"){ echo 'selected="true"';} ?>>Februari 2016</option>
                                      <option value="03" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "03"){ echo 'selected="true"';} ?>>Maret 2016</option>
                                      <option value="04" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "04"){ echo 'selected="true"';} ?>>April 2016</option>
                                      <option value="05" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "05"){ echo 'selected="true"';} ?>>Mei 2016</option>
                                      <option value="06" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "06"){ echo 'selected="true"';} ?>>Juni 2016</option>
                                      <option value="07" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "07"){ echo 'selected="true"';} ?>>Juli 2016</option>
                                      <option value="08" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "08"){ echo 'selected="true"';} ?>>Agustus 2016</option>
                                      <option value="09" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "09"){ echo 'selected="true"';} ?>>September 2016</option>
                                      <option value="10" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "10"){ echo 'selected="true"';} ?>>Oktober 2016</option>
                                      <option value="11" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "11"){ echo 'selected="true"';} ?>>Nopember 2016</option>
                                      <option value="12" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "12"){ echo 'selected="true"';} ?>>Desember 2016</option>                                                            
                                  </select>
                              </div><br>
                              <table class="table table-inner table-vmiddle">
                                  <thead>
                                      <tr>
                                          <th class="bgm-teal c-white" style="width: 10px">No</th>
                                          <th class="bgm-teal c-white" style="width: 15px">Tanggal</th>
                                          <th class="bgm-teal c-white" style="width: 30px">Keterangan</th>
                                          <th class="bgm-teal c-white" style="width: 20px">Debet</th>
                                          <th class="bgm-teal c-white" style="width: 20px">Kredit</th>
                                          <th class="bgm-teal c-white" style="width: 20px">Saldo akhir</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>1</td>
                                          <td>2 Desember 2016</td>
                                          <td>Membeli pelajaran Mengaji</td>
                                          <td>0</td>
                                          <td>Rp. 200.000,00</td>
                                          <td>Rp. 200.000,00</td>
                                      </tr>
                                      <tr>
                                          <td>2</td>
                                          <td>3 Desember 2016</td>
                                          <td>Membeli pelajaran Mengaji</td>
                                          <td>0</td>
                                          <td>Rp. 200.000,00</td>
                                          <td>Rp. 200.000,00</td>
                                      </tr>
                                      <tr>
                                          <td>3</td>
                                          <td>4 Desember 2016</td>
                                          <td>Membeli pelajaran Mengaji</td>
                                          <td>0</td>
                                          <td>Rp. 200.000,00</td>
                                          <td>Rp. 200.000,00</td>
                                      </tr>
                                      <tr>
                                          <td>4</td>
                                          <td>5 Desember 2016</td>
                                          <td>Membeli pelajaran Mengaji</td>
                                          <td>0</td>
                                          <td>Rp. 200.000,00</td>
                                          <td>Rp. 200.000,00</td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                          <div role="tabpanel" class="tab-pane" id="profile11">
                              <div class="pull-left col-md-6">Ini Form Input Rekening</div>
                              <div class="pull-left col-md-6">Ini Card view daftar yang udh di input</div>
                          </div>
                          <div role="tabpanel" class="tab-pane" id="messages11">
                              <p>Etiam rhoncus. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Cras id dui. Curabitur turpis.Etiam ut purus mattis mauris sodales aliquam. Aenean viverra rhoncus pede. Nulla sit amet est. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Praesent ac sem eget est egestas volutpat.Cras varius. Morbi mollis tellus ac sapien. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Fusce vel dui.Morbi mattis ullamcorper velit. Etiam rhoncus. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Cras id dui. Curabitur turpis.Etiam ut purus mattis mauris sodales aliquam. Aenean viverra rhoncus pede. Nulla sit amet est. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Praesent ac sem eget est egestas volutpat.Cras varius. Morbi mollis tellus ac sapien. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Fusce vel dui.</p>
                          </div>
                          <div role="tabpanel" class="tab-pane" id="settings11">
                              <p>Praesent turpis. Phasellus magna. Fusce vulputate eleifend sapien. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis.</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          

      </div>
    </section>
</section>

<footer id="footer">
  <?php $this->load->view('inc/footer'); ?>
</footer>