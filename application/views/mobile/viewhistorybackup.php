<style type="text/css">
	.bp-header:hover
	{
		cursor: pointer;
	}
	.thumb {
		padding: 10px;
	}
	#playervideo {
        object-fit: fill;
        width: 100%;
        height: 100%;
        padding: 0px;
    }
</style>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main" data-layout="layout-1" >
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" >        
        <div class="container">            

            <div class="card">
            <div class="row">
            	<div class="card-header">
            		<h2 class="m-l-15">Bahasa Indonesia - Pribahasa<small>15 January 2016 - 11:30</small></h2>            		
            		<!-- <div class="pull-right m-r-15" style="margin-top: -3.5%;"><a href="<?php echo base_url(); ?>first/classhistory"><button class="btn btn-warning">Kembali</button></a></div> -->

            		<ul class="actions" style="margin-right: 2%;">
                        <li class="dropdown action-show">
                            <a href="" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>
            
                            <div class="dropdown-menu pull-right">
                                <p class="p-5">
                                    <button class="btn btn-info btn-block">Whiteboard</button>
                                    <button class="btn btn-info btn-block">Video</button>
                                    <button class="btn btn-info btn-block">Screen Share</button>
                                    <hr>                                
                                	<button class="btn btn-warning btn-block">Kembali</button>
                                </p>
                                
                            </div>
                        </li>
                    </ul>
            	</div>

            	<div class="card-body card-padding">

            		<div class="row">
            				
            			<div class="col-md-12">
            				
            				<div class="col-md-6 bgm-teal" style="height: 70vh; padding: 0px;">
            					<video id="playervideo" controls="controls" preload="none" autoplay muted>
                                    <source id="source" type="video/mp4" src="<?php echo base_url(); ?>aset/video/bigbunny.mp4" />	
                                </video>
            				</div>
            				<div class="col-md-6">
            					<div class="col-md-12" style="height: 34vh; padding: 0px; background-color: #ececec; margin-bottom: 1vh;">
            						<h2>Whiteboard</h2>
            					</div>
            					<div class="col-md-12" style="height: 34vh; padding: 0px; background-color: #ececec; margin-top: 1vh;">
            						<h2>Screen Share</h2>
            					</div>
            				</div>
            			</div>

            		</div>

            	</div>
            	
            	<div class="card-body card-padding">
            	<div class="col-md-12">
		            
		            	<div class="card blog-post z-depth-2">
			                <div class="bp-header">
			                	<div class="thumb">			                	
			                		<video id="videoPlayer" style="height: 60vh; width: 100%" controls="controls" preload="none" autoplay muted> <!-- id could be any according to you -->
                                        <source id="source" type="video/mp4" src="<?php echo base_url(); ?>aset/video/bigbunny.mp4" />	
                                    </video>	                                   
                                
			                    </div>   
			                    <a href="" class="bp-title">
			                        <h2>Sesi List</h2>
			                        <small>Bahasa Indonesia - Pribahasa</small>
			                        |
			                        <small>15 January 2016 - 11:30</small>
			                        <br>
			                    </a>

			                    <div class="bp-title bgm-white c-black">

			                    	<div class="table-responsive card-body">
			                    		<table class="table table-hover">
			                                <thead>
			                                    <tr>
			                                        <th>No</th>
			                                        <th>Waktu Mulai</th>
			                                        <th>Waktu Selesai</th>
			                                        <th>#</th>
			                                    </tr>
			                                </thead>
			                                <tbody>
			                                    <tr>
			                                        <td>1</td>
			                                        <td>00:00</td>
			                                        <td>10:51</td>
			                                        <td><button class="btn btn-success" id="klikvideo1">Play</button></td>
			                                    </tr>
			                                    <tr>
			                                        <td>2</td>
			                                        <td>12:30</td>
			                                        <td>60:00</td>
			                                        <td><button class="btn btn-success" id="klikvideo2">Play</button></td>
			                                    </tr>			                                   
			                                </tbody>
			                            </table>
			                    	</div>

			                    </div>
			                </div>
						</div>                                	            

		        </div>
		        </div>
		    </div>
            </div>

		</div><!-- akhir container -->
	</section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
	$("#klikvideo1").click(function(){
		document.getElementById("source").src = "<?php echo base_url(); ?>aset/video/line.mp4";
        document.getElementById("videoPlayer").load();
	});
	$("#klikvideo2").click(function(){
		document.getElementById("source").src = "<?php echo base_url(); ?>aset/video/juki.mp4";
        document.getElementById("videoPlayer").load();
	});
	// function toggle() {
 //        if ($(this).attr('data-click-state') == 1) {
 //            $(this).attr('data-click-state', 0)		
 //            document.getElementById("source").src = "<?php echo base_url(); ?>aset/video/bigbunny.mp4";
 //            document.getElementById("videoPlayer").load();
 //        } else {
 //            $(this).attr('data-click-state', 1)
 //            document.getElementById("source").src = "<?php echo base_url(); ?>aset/video/juki.mp4";
 //            document.getElementById("videoPlayer").load();
 //        }
 //    } 
	
</script>