<style type="text/css">
    html, body, main-content {
        height:100%;
        overflow:hidden;
        max-height: 100%;
        /*background-image: url(../aset/img/headers/BGclassroom-01.png);*/
        background-color: #0FA7A7;
        background-repeat: no-repeat; 
        background-position: 0 0;
        background-size: cover; 
        background-attachment: fixed;
    }
    .video2 {
        object-fit: fill;
        padding: 0px;
        width: 100%;
        height: 100%;        
    }
    #menu1{
        bottom: 0px;
        position: relative;
    }
    .outer-container {
        width: 100%;
        height: 100%;
        text-align: center;
    }
    .inner-container {
        display: inline-block;
        position: relative;
        width: 100%;
        background-color: #ececec;      
    }
    .papanwhiteboard {
        display: block;
        position: relative;
        width: 100%;
        background-color: #ececec;
        height: 60vh;
    }
    .papanscreen {
        display: none;
        position: relative;
        width: 100%;
        background-color: #ececec;
        height: 60vh;
    }
    .video-overlay {
        position: absolute;
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 100%;
        height: 46vh;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.3);*/
    }
    .toggle-overlay {
        position: absolute;
        right: 0;
        bottom: 0;        
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 20%;
        height: 25vh;
        float: right;
        /*background-color: rgba(50, 50, 50, 0.3);*/
    }
    .toggle-overlay1 {
        right: 0;
        bottom: 0;
        margin: 0px;
        margin-left: 2%;
        margin-bottom: 2%;      
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 95%;
        float: right;
        /*background-color: rgba(50, 50, 50, 0.4);*/
    }
    .chating {
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        margin-bottom: 3%;
        font-size: 16px;
        font-family: Helvetica;
        color: #FFF;
        width: 100%;
        float: left;
        text-align: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    .video-overlay1 {
        left: 0px;
        bottom: 5px;
        margin: 0px;
        margin-left: 2%;
        margin-bottom: 2%;      
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 95%;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.4);*/
    }
    .video-overlay2 {
        right: 0px;
        bottom: 0px;
        margin: 0px;
        padding: 5px 5px;
        margin-left: 3%;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 30%;
        float: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    #remotevideo {
        width: 100%;
        height: 40vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #remotevideo.toggle {
        width: 100%;
        height: 33vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #content {
        width: 100%;
        height: 100vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #content.toggle {
        width: 100%;
        height: 50vh;
        padding: 0; margin: 0;      
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #papanchat{
        display: block;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #papanchat.toggle{
        display: none;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    @media all and (orientation:portrait) {
        #content {
            width: 100%;
            height: 60vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #remotevideo {
            width: 100%;
            height: 40vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        .papanwhiteboard {
            display: block;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 60vh;
        }
        .papanscreen {
            display: none;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 60vh;
        }
    }

    @media all and (orientation:landscape) {
        #content {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #remotevideo {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        .papanwhiteboard {
            display: block;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 30vh;
        }
        .papanscreen {
            display: none;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 30vh;
        }
    }
</style>

<section id="content" class="tampilanandro">

    <div id="loader">
        <div class="page-loader" style="background-color:#1274b3;">
            <div class="preloader pl-xl pls-white">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>
                <p>Class Miles . . .</p>
            </div>
        </div>
    </div>

    <input type="text" name="orientation" id="orientation" hidden>
    <div id="potrait_design">
        <!-- NAVBAR MENU  -->
        <div class="" style="position: absolute; width: 100%; z-index: 1;">
            <div class="pull-left p-5 p-t-10">          
                <img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
            </div>
        </div>

        <div class="outer-container" style="z-index: 2;">
            <div class="inner-container" id="tempatplayer">                                                               
                <video class="video2" id="myvideo" style="z-index: 1; position: absolute; margin: 2%; height: 80px; width: 80px; bottom: 0; right: 0;" autoplay loop muted poster="../aset/img/banner/Groupclass.png" data-video-aspect-ratio="16:9"></video>
                <video class="video2" id="remotevideo" disabled width="100%" height="100%" autoplay poster="../aset/img/banner/Discussion.png" data-video-aspect-ratio="16:9"/>         
            </div>
            <div class="papanwhiteboard c-black" id="kotakpwhite">
                <img id="settingvideowb" src="<?php echo base_url();?>aset/img/icons/Arrow 4-01.png" width="40px" height="40px" style="z-index: 1; position: absolute; margin: 2%; cursor: pointer; left:0;"></img>
                <video id="myvideo3" class="video2" autoplay muted poster="../aset/img/banner/Discussion.png" data-video-aspect-ratio="16:9"/>
            </div>
            <div class="papanscreen c-black" id="kotaksscreen">
                <iframe id="screensharestudenttt" class="screen-share-iframe" scrolling="no" frameborder="0" allowfullscreen style="width: 100%; height: 100%; max-width: 100%;" ></iframe>
            </div>
            <div class="toggle-overlay" id="papantoggle" style="z-index: 11; display: block;">
                <div class="toggle-overlay1" style="position: absolute;">
                    <div style="height:auto;bottom: 0; right: 0; overflow-y: auto;" id="toggle">
                        <div style="margin-top: 2%;">                    
                            <button id="papanwhiteboard" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><img src="../aset/img/baru/Whiteboard.png" style="height: 25px; width: 25px; margin-top: -1%;"></button>
                        </div>
                        <div style="margin-top: 2%;">                    
                            <button id="papanscreen" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><i class="zmdi zmdi-window-maximize"></i></button>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>

    <div id="landscape_design" style="display: none;">

      <!-- NAVBAR MENU  -->
      <div class="" style="position: absolute; width: 100%; z-index: 1;">
          <div class="pull-left p-5 p-t-10">          
              <img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
          </div>
          <div class="pull-right p-15">          
              <div class="m-l-5" style="float: right;">
                 <button id="papanwhiteboardd" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><img src="../aset/img/baru/Whiteboard.png" style="height: 25px; width: 25px; margin-top: -3%;"></button>
                 <button id="papanscreenn" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><i class="zmdi zmdi-window-maximize"></i></button>
              </div>
          </div>
      </div>

      <div class="outer-container" style="z-index: 2;">
          <div id="ddd" style="position: relative; display: inline-block; width: 100%; height: 100vh;"> 
                <div class="col-md-12" style="padding: 0;">
                    <div class="col-md-6" style="width: 50%; float: left; padding: 0; background-color: rgba(0, 0, 0, 0.3);">
                        <div class="papanwhiteee c-black">         
                            <div id="kotakwhitee">
                            </div>
                        </div>
                        <div class="papanscreennn c-black" style="display: none;">         
                            <div style="height: 431px;" id="kotakscreenn">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"  style="width: 50%; float: left; padding: 0; background-color: rgba(0, 0, 0, 0.3);">
                        <div class="papanvideotutor c-black">         
                            <div style="height: 431px;" id="kotakvideotutor">                        
                            </div>
                        </div>
                    </div>

                </div>
                <div id="tempatplayerr" style="position: absolute; z-index: 11; bottom: 0; right: 0; height: 110px; width: 110px; margin-bottom: 1.5%; margin-right: 1.5%;">

                </div>                
              <div class="video-overlay" id="papanchatt" style="z-index: 10; display: block;">                
                  
              </div>  
          </div>

      </div>
    </div>

</section>

<script type="text/javascript">
    
    // $("#papanshow2").css('display','none');
    $("#showpapan").click(function(){
        $("#content").toggleClass('toggle');
    });

    $("#papanwhiteboard").click(function(){                
        $(".papanscreen").css('display','none');
        $(".papanwhiteboard").css('display','block');
    });

    $("#papanscreen").click(function(){        
        $(".papanwhiteboard").css('display','none');
        $(".papanscreen").css('display','block');     
    });

    $("#papanwhiteboardd").click(function(){        
        $(".papanscreennn").css('display','none');
        $(".papanwhiteee").css('display','block');
    });

    $("#papanscreenn").click(function(){          
        $(".papanwhiteee").css('display','none');
        $(".papanscreennn").css('display','block');     
    });

    var mql = window.matchMedia("(orientation: portrait)");

    // If there are matches, we're in portrait
    if(mql.matches) {  
        // Portrait orientation
        $("#orientation").val('');
        $("#orientation").val('potrait');        
        $("#landscape_design").css('display','none');
        $("#potrait_design").css('display','block'); 

        $("#myvideo3").appendTo('#kotakpwhite');
        $("#screensharestudenttt").appendTo('#kotaksscreen');
        $(".video-overlay1").appendTo('#papanchat');
         
        $('#myvideo').appendTo('#tempatplayer');
        $("#remotevideo").appendTo('#tempatplayer');
        $('#myvideo').get(0).play();
        $('#remotevideo').get(0).play();
        $('#myvideo3').get(0).play();
    } else {  
        // Landscape orientation
        var heightlayar = $(window).height();
        var heightvideo = $("#ddd").height();
        var sisa        = heightlayar - heightvideo+"px";
        $("#kotakwhitee").css('height',heightvideo);        
        $("#orientation").val('');
        $("#orientation").val('landscape');
        $("#potrait_design").css('display','none');
        $("#landscape_design").css('display','block');       
        
        $("#myvideo3").appendTo('#kotakwhitee');
        $("#screensharestudenttt").appendTo('#kotakscreenn');
        $(".video-overlay1").appendTo('#papanchatt');  

        $('#myvideo').appendTo('#tempatplayerr');   
        $("#remotevideo").appendTo('#kotakvideotutor');           
        $('#myvideo').get(0).play();
        $('#remotevideo').get(0).play();
        $('#myvideo3').get(0).play();
    }

    $(document).on("pagecreate",function(event){
        $(window).on("orientationchange",function(){
            if(window.orientation == 0)
            {
              // $("p").text("The orientation has changed to portrait!").css({"background-color":"yellow","font-size":"300%"});
              // alert('potraitt');
              $("#landscape_design").css('display','none');
              $("#potrait_design").css('display','block'); 

              $("#myvideo3").appendTo('#kotakpwhite');
              $("#screensharestudenttt").appendTo('#kotaksscreen');
              $(".video-overlay1").appendTo('#papanchat');
               
              $('#myvideo').appendTo('#tempatplayer');
              $("#remotevideo").appendTo('#tempatplayer');
              $('#myvideo').get(0).play();
              $('#remotevideo').get(0).play();
              $('#myvideo3').get(0).play();
            }
            else
            {
              // $("p").text("The orientation has changed to landscape!").css({"background-color":"pink","font-size":"200%"});
              // alert('landscape');
              $("#potrait_design").css('display','none');
              $("#landscape_design").css('display','block');  

              $("#myvideo3").appendTo('#kotakwhitee');
              $("#screensharestudenttt").appendTo('#kotakscreenn');
              $(".video-overlay1").appendTo('#papanchatt');  

              $('#myvideo').appendTo('#tempatplayerr');   
              $("#remotevideo").appendTo('#kotakvideotutor');           
              $('#myvideo').get(0).play();
              $('#remotevideo').get(0).play();
              $('#myvideo3').get(0).play();

            }
        });                   
    });
</script>



