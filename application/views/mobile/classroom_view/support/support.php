<div id="loader">
    <div class="page-loader" style="background-color:#1274b3;">
        <div class="preloader pl-xl pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Class Miles . . .</p>
        </div>
    </div>
</div>

<style type="text/css">
    html, body, main-content {
        height:100%;
        overflow:hidden;
        max-height: 100%;
        background-image: url(../aset/img/headers/BGclassroom-01.png);
        background-repeat: no-repeat; 
        background-position: 0 0;
        background-size: cover; 
        background-attachment: fixed;
    }
    .video2 {
        object-fit: fill;
        padding: 0px;
        width: 90%;
        height: 90%;        
    }
    #menu1{
        bottom: 0px;
        position: relative;
    }
    .outer-container {
        width: 100%;
        height: 100%;
        text-align: center;
    }
    .inner-container {
        display: inline-block;
        position: relative;
        width: 100%;
        background-color: #ececec;      
    }
    .papanwhiteboard {
        display: inline-block;
        position: relative;
        width: 100%;
        background-color: #ececec;
        height: auto;
    }
    .papanscreen {
        display: none;
        position: relative;
        width: 100%;
        background-color: #ececec;
        height: auto;
    }
    .video-overlay {
        position: absolute;
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 100%;
        height: 46vh;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.3);*/
    }
    .chating {
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        margin-bottom: 3%;
        font-size: 16px;
        font-family: Helvetica;
        color: #FFF;
        width: 100%;
        float: left;
        text-align: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    .video-overlay1 {
        left: 0px;
        bottom: 5px;
        margin: 0px;
        margin-left: 2%;
        margin-bottom: 2%;      
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 95%;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.4);*/
    }
    .video-overlay2 {
        right: 0px;
        bottom: 0px;
        margin: 0px;
        padding: 5px 5px;
        margin-left: 3%;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 30%;
        float: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    #player {
        width: 100%;
        height: 60vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #player.toggle {
        width: 100%;
        height: 60vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #content {
        width: 100%;
        height: 100vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #content.toggle {
        width: 100%;
        height: 50vh;
        padding: 0; margin: 0;      
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #papanchat{
        display: block;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #papanchat.toggle{
        display: none;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    @media all and (orientation:portrait) {
        #content {
            width: 100%;
            height: 60vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #player {
            width: 100%;
            height: 60vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        .papanwhiteboard {
            display: inline-block;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 40vh;
        }
        .papanscreen {
            display: none;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 40vh;
        }
    }

    @media all and (orientation:landscape) {
        #content {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #player {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
    }
</style>

<header id="header" class="clearfix" style="background-color: rgba(33, 150, 243, 0.5);">    
    <div style="margin-left: 7%; margin-right: 9%;">
        <ul class="header-inner clearfix">
            <li id="menu-trigger" data-trigger=".ha-menu" class="visible-xs">
                <div class="line-wrap">
                    <div class="line top"></div>
                    <div class="line center"></div>
                    <div class="line bottom"></div>
                </div>
            </li>
            <li class="logo hidden-xs">
                <a href="#" class="m-l-20"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" style='margin-top: -7px;' alt=""></a>
            </li>
            <li class="pull-right">            

                <div id="usertypestudent" style="display: none;">
                    <ul class="top-menu">                                                                                                
                        <li data-toggle="tooltip" data-placement="bottom" title="Logout" style="cursor: pointer;" id="tour5">
                            <a id="keluarcobaaa"><i class="tm-icon zmdi zmdi-power-setting"></i></a>               
                        </li>
                    </ul>
                </div>

                <div id="usertypetutor" style="display: none;">
                    <ul class="top-menu">                                        
                        <!-- START PLAY AND PAUSE -->                                                
                        <li data-toggle="tooltip" id="tourtutor9" data-placement="bottom" title="Logout" style="cursor: pointer;">
                            <a id="keluarcobaa"><i class="tm-icon zmdi zmdi-power-setting"></i></a>               
                        </li>
                    </ul>
                </div>

            </li>
        </ul>
        <div style="width: 125%; margin-left: -10%;">
            <div class="col-md-12" style="text-align: left; background-color: rgba(33, 150, 243, 0.3);">
                <nav class="ha-menu">
                    <ul>
                        <li class="pull-left c-white" style="margin-left: 8%;"><label class="c-white"><label id="classname"></label></label></li>                        
                        <li class="pull-right" style="margin-right: 12%;"><label class="c-white"><label id="datenow"></label></label></li>
                    </ul>
                </nav>
            </div>                    
        </div>
    </div>   
     
</header>

<section id="main" class="tampilanweb" style="overflow: hidden; height: 100%;">

    <section id="content">      
        <!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/janus.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/test.js"></script> -->
<style type="text/css">
    #container {
        width: 100%;       
        position: relative;
        text-align:center;
    }
    #container div {
        position: relative;
        margin:3px;     
        background-color: #bfbfbf;
        border-radius:3px;
        text-align:center;
        display:inline-block;
    }
    #video{
        width: 100%;
    }

    .video2 {
        object-fit: fill;
        width: 100%;
        height: 100%;
    }
    #tempatstudent{
        width: 100%;     
    }
    #tempatstudent.toggle{
        width: 80%;     
    }
    #tempattutor{
        width: 100%;     
    }
    #tempattutor.toggle{
        width: 80%;     
    }
    .raise-hand-video-box{
      height: 360px; 
    }

</style>
    
    <div id="tempatstudent" style="margin-top:4%; display: none;">
        <section class="row" id="container2" style="margin-left: 5%; margin-right: 5%; width: 90%; height: 70vh;">
        <!-- <div class="container"> -->

            <div class="col-md-12" style="height: 100%; padding: 0; margin: 0;">
                
                <div class="col-md-5" style="height: 100%; margin-left: 4%; background: rgba(0, 0, 0, 0.3); padding: 0;" id="sshare">
                    <video class="video2" id="remotevideo" disabled width="90%" height="90%" autoplay />
                </div>                
                <div class="col-md-5" style="height: 100%; margin-left: 4%; background: rgba(0, 0, 0, 0.3); padding: 0;" id="vdio">                    
                    <video class="video2" id="myvideo" autoplay loop muted></video>                    
                </div>
                            
            </div>
            
        <!-- </div> -->
        </section> 
        <div id="berkas" class="col-md-12" style="display: none;"></div>        
    </div>

    <br>

    <div id="tempattutor" style="margin-top:4%; display: none;">
        <section class="row" id="container2" style="height: 70vh; width: 90%; margin-left: 5%; margin-right: 5%;">
            <!-- <div class="container"> -->
            <div class="col-md-12" style="height: 100%; padding: 0; margin: 0;">                
                <div class="col-md-5" style="height: 100%; margin-left: 4%; background: rgba(0, 0, 0, 0.3); padding: 0; " id="sshare">
                    <video class="video2" id="remotevideot" disabled autoplay width="90%" height="90%"/> 
                </div>
                
                <div class="col-md-5" style="height: 100%; margin-left: 4%; background: rgba(0, 0, 0, 0.3); padding: 0;" id="vdio">                    
                    <video class="video2" id="myvideot" autoplay loop></video>                    
                </div>
                            
            </div>
            <!-- </div>           -->
        </section> 
        <div id="berkas" class="col-md-12" style="display: none;"></div>       
        </div>
        <div id="tempatwaktu" class="text-center" style="position:absolute;
        width:300px;
        height:40px;
        background:rgba(3, 169, 244, 0.6);
        padding-left: 3px;
        padding-right: 3px;
        bottom:5px;
        border-radius: 5px;
        right:25%;
        left:50%;
        margin-left: -150px;
        display: none;">
            <h5 class="c-white">Classroom will be ended in <label id="countdown"></label></h5>
        </div>

    </section>

    <div id="classkeluar" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p class="f-15 pull-left m-t-20">Anda Yakin Keluar ?</p><br><br><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    <button type="button" class="btn btn-info" id="keluar">OK</button>
                </div>
            </div>
        </div>
    </div>

    <div id="deviceConfigurationModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Change Video Devices <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-offset-2 col-md-8" id="buttonlist">
                  <!-- <button class="btn btn-default btn-block" id="iddevice">
                    <label id="listdevice"></label>
                  </button> -->
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>  

    <div id="kotakmenulist" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">List User Raise Hands <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-offset-2 col-md-8" id="kotakraisehand">
                  <!-- <button class="btn btn-default btn-block" id="iddevice">
                    <label id="listdevice"></label>
                  </button> -->
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>  

</section>

<section id="content" class="tampilanandro">
    
    <!-- NAVBAR MENU  -->
    <div class="" style="position: absolute; width: 100%; z-index: 1;">
        <div class="pull-left p-5 p-t-10">          
            <img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
        </div>
        <div class="pull-right p-15">
            <!-- <div class="m-l-5" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a data-toggle="modal" href="#modalDefault"><img src="<?php echo base_url(); ?>aset/img/icons/close.png" height="30px" width="30px"></a>
            </div> -->
            <!-- <div class="m-l-5" id="showchat" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a ><img src="<?php echo base_url(); ?>aset/img/icons/comment.png" height="35px" width="35px"></a>
            </div> -->
            <!-- <div class="m-l-5" id="showpapan" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a href="#"><img src="<?php echo base_url(); ?>aset/img/icons/whiteboard.png" height="35px" width="35px"></a>
            </div> -->
            <div class="m-l-5" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a id="raisehandclickAndroid" ><img src="<?php echo base_url(); ?>aset/img/icons/raisehandd.png" height="35px" width="35px"></a>
            </div>
        </div>
    </div>

    <div class="outer-container" style="z-index: 2;">
        <div class="inner-container" id="tempatplayer">                                                               
            <video id="player" autoplay loop></video>          
        </div>
        <div class="video-overlay" id="papanchat" style="z-index: 10;">
                
            <div class="video-overlay1" style="position: absolute;">
                <div style="height:38vh; bottom: 0; overflow-y: auto; margin-bottom: 2%;" id="chatAndroid">
                    <!-- <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">koasdflksdjfkladsjfkladsjlfdaslfjldsakfjkldasjfkldsajflkdasjflkdsajklfjdsalkfjlksdafjklsadjflkdsajflkdsjflksdjflkdsjf</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div> -->
                </div>
                <div style="width: 100%;">
                    <input maxlength="300" id="chatInputAndroid" type="text" name="sent" class="input form-control c-black" style="padding: 5px;" placeholder="Type your message">
                </div>
            </div>
        </div>
        <div class="papanwhiteboard c-black">
            <a href="#" id="papanwhiteboard"><img src="<?php echo base_url() ?>aset/img/icons/rotate-3d.png" width="35px" height="35px" style="z-index: 1; position: absolute; margin: 2%; left:0;"></a>
            <iframe id="wbcarddd" width="100%" height="100%" style="padding: 0px;"></iframe>
        </div>
        <div class="papanscreen c-black">
            <a href="#" id="papanscreen"><img src="<?php echo base_url() ?>aset/img/icons/rotate-3d.png" width="35px" height="35px" style="z-index: 1; position: absolute; margin: 2%; left:0;"></a>
            <iframe id="screensharestudenttt" class="screen-share-iframe" scrolling="no" frameborder="0" allowfullscreen style="width: 100%; height: 100%; max-width: 100%;" ></iframe>
        </div>
    </div>

    <!-- MODAL RAISE HAND -->   
    <!-- <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">   
                <div class="modal-body">
                    <video src="<?php echo base_url(); ?>aset/video/line.mp4" autoplay loop muted style="height: 30vh; width: 100%; padding: 0; margin-top: 10%;"></video>  
                    <video src="<?php echo base_url(); ?>aset/video/juki.mp4" autoplay loop muted style="height: 30vh; width: 100%; padding: 0; margin-top: 10%;"></video>  
                    
                </div>             
                <div class="modal-footer">                    
                    <hr>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hangout</button>                    
                </div>
            </div>
        </div>
    </div>
 -->
</section>

<div id="videoCallModall" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-raise-hand" style="width: 85%;">
      <div class="modal-content">
        <div class="modal-header bgm-blue">
            <h4 class="modal-title c-white">Raise Hand <label id="hangups" class="close c-white btn-small" aria-label="Close"><span aria-hidden="true" class="c-white">Hangup</span></label></h4>
        </div>
        <div class="modal-body">
        <br>
          <div class="row">
            <div class="col-md-12" id="kotakr">
              <div class="col-md-6 raise-hand-video-box" id="kotaka">
                <center id="videoBoxLocal" ></center>
              </div>
              <div class="col-md-6 raise-hand-video-box">
                <center id="videoBoxRemote"></center>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" id="hangups" >Hangup</button>
        </div> -->
      </div>
    </div>
  </div>

  <div id="tidakadadata" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header bgm-orange">
            <h4 class="modal-title c-white">INFO <label id="tutupalert" class="close c-white btn-small" aria-label="Close"><span aria-hidden="true" class="c-white f-13">X</span></label></h4>
        </div>
        <div class="modal-body">
        <br>
          <div class="row">
            <center><h4>Tidak ada data</h4></center>
          </div>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" id="hangups" >Hangup</button>
        </div> -->
      </div>
    </div>
  </div>

  <div id="callingModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Calling ... <button type="button" id="hangupss" class="close" aria-label="Close"><span aria-hidden="true">X</span></button></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <b id="messageCalling"></b>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
    
    // $("#papanshow2").css('display','none');
    $("#showpapan").click(function(){
        $("#content").toggleClass('toggle');
    });

    $("#showchat").click(function(){
        $("#papanchat").toggleClass('toggle');
    });

    $("#player").click(function(){
        $("#papanchat").toggleClass('toggle');
    });

    $("#papanwhiteboard").click(function(){
        $(".papanwhiteboard").css('display','none');
        $(".papanscreen").css('display','inline-block');
    });

    $("#papanscreen").click(function(){
        $(".papanscreen").css('display','none');
        $(".papanwhiteboard").css('display','inline-block');
    });
</script>



