<style type="text/css">
	html, body {
	    height:100%;
	    width: 100%;
	    overflow:hidden;	    
	    max-height: 100%;
	    background-color: #ffffff;	    
	}

	.video2 {
        object-fit: fill;
        padding: 0px;
        width: 100%;
        height: 100%;        
    }
    #menu1{
    	bottom: 0px;
    	position: relative;
    }
    .outer-container {
	    width: 100%;
	    height: 100%;
	    text-align: center;
	}
	.inner-container {
	    display: inline-block;
	    position: relative;
	    width: 100%;
	    background-color: #ececec;	    
	}
	.papanwhiteboard {
		display: inline-block;
		position: relative;
		width: 100%;
		background-color: #ececec;
		height: 50vh;
	}
	.papanscreen {
		display: none;
		position: relative;
		width: 100%;
		background-color: #ececec;
		height: 50vh;
	}
	.video-overlay {
	    position: absolute;
	    left: 0px;
	    bottom: 0px;	    
	    padding: 5px 5px;
	    font-size: 20px;
	    font-family: Helvetica;
	    color: #FFF;
	    width: 100%;
	    height: 46vh;
	    float: left;
	    /*background-color: rgba(50, 50, 50, 0.3);*/
	}
	.chating {
	    left: 0px;
	    bottom: 0px;	    
	    padding: 5px 5px;
	    margin-bottom: 3%;
	    font-size: 16px;
	    font-family: Helvetica;
	    color: #FFF;
	    width: 100%;
	    float: left;
	    text-align: left;
	    background-color: rgba(50, 50, 50, 0.4);
	}
	.video-overlay1 {
	    left: 0px;
	    bottom: 5px;
	    margin: 0px;
	    margin-left: 2%;
	    margin-bottom: 2%;	    
	    padding: 5px 5px;
	    font-size: 20px;
	    font-family: Helvetica;
	    color: #FFF;
	    width: 95%;
	    float: left;
	    /*background-color: rgba(50, 50, 50, 0.4);*/
	}
	.video-overlay2 {
	    right: 0px;
	    bottom: 0px;
	    margin: 0px;
	    padding: 5px 5px;
	    margin-left: 3%;
	    font-size: 20px;
	    font-family: Helvetica;
	    color: #FFF;
	    width: 30%;
	    float: left;
	    background-color: rgba(50, 50, 50, 0.4);
	}
	#player {
	    width: 100%;
	    height: 60vh;
	    object-fit: fill;
	    transition: all .5s ease;
		  -webkit-transition: all .5s ease;
		  -moz-transition: all .5s ease;
		  -ms-transition: all .5s ease;
		  -o-transition: all .5s ease;
	}
	#player.toggle {
	    width: 100%;
	    height: 60vh;
	    object-fit: fill;
	    transition: all .5s ease;
		  -webkit-transition: all .5s ease;
		  -moz-transition: all .5s ease;
		  -ms-transition: all .5s ease;
		  -o-transition: all .5s ease;
		-moz-transform:scale(1) rotate(90deg);
		-webkit-transform:scale(1) rotate(90deg);
		-o-transform:scale(1) rotate(90deg);
		-ms-transform:scale(1) rotate(90deg);
		transform:scale(1) rotate(90deg);
	}
	#content {
	    width: 100%;
	    height: 100vh;
	    object-fit: fill;
	    transition: all .5s ease;
		  -webkit-transition: all .5s ease;
		  -moz-transition: all .5s ease;
		  -ms-transition: all .5s ease;
		  -o-transition: all .5s ease;
	}
	#content.toggle {
	    width: 100%;
	    height: 50vh;
	    padding: 0; margin: 0;	    
	    transition: all .5s ease;
		  -webkit-transition: all .5s ease;
		  -moz-transition: all .5s ease;
		  -ms-transition: all .5s ease;
		  -o-transition: all .5s ease;
		-moz-transform:scale(1) rotate(90deg);
		-webkit-transform:scale(1) rotate(90deg);
		-o-transform:scale(1) rotate(90deg);
		-ms-transform:scale(1) rotate(90deg);
		transform:scale(1) rotate(90deg);
	}
	#papanchat{
		display: block;
		transition: all .5s ease;
		  -webkit-transition: all .5s ease;
		  -moz-transition: all .5s ease;
		  -ms-transition: all .5s ease;
		  -o-transition: all .5s ease;
	}
	#papanchat.toggle{
		display: none;
		transition: all .5s ease;
		  -webkit-transition: all .5s ease;
		  -moz-transition: all .5s ease;
		  -ms-transition: all .5s ease;
		  -o-transition: all .5s ease;
	}
	@media all and (orientation:portrait) {
		#content {
		    width: 100%;
		    height: 60vh;
		    object-fit: fill;
		    transition: all .5s ease;
			  -webkit-transition: all .5s ease;
			  -moz-transition: all .5s ease;
			  -ms-transition: all .5s ease;
			  -o-transition: all .5s ease;
		}
		#player {
		    width: 100%;
		    height: 60vh;
		    object-fit: fill;
		    transition: all .5s ease;
			  -webkit-transition: all .5s ease;
			  -moz-transition: all .5s ease;
			  -ms-transition: all .5s ease;
			  -o-transition: all .5s ease;
		}
		.papanwhiteboard {
			display: inline-block;
			position: relative;
			width: 100%;
			background-color: #ececec;
			height: 40vh;
		}
		.papanscreen {
			display: none;
			position: relative;
			width: 100%;
			background-color: #ececec;
			height: 40vh;
		}
	}

	@media all and (orientation:landscape) {
	  	#content {
		    width: 100%;
		    height: 100vh;
		    object-fit: fill;
		    transition: all .5s ease;
			  -webkit-transition: all .5s ease;
			  -moz-transition: all .5s ease;
			  -ms-transition: all .5s ease;
			  -o-transition: all .5s ease;
		}
		#player {
		    width: 100%;
		    height: 100vh;
		    object-fit: fill;
		    transition: all .5s ease;
			  -webkit-transition: all .5s ease;
			  -moz-transition: all .5s ease;
			  -ms-transition: all .5s ease;
			  -o-transition: all .5s ease;
		}
	}
</style>

<script type="text/javascript">
	var a = $(window).height();
	var b = $(window).width(); //UNTUK HEIGHT
	// var b = $(window).height() / 0.65;
	var c = b * 0.5625;
	var d = a - c;
	var e = a / 0.5625; //UNTUK WIDTH LANDSCAPE
	// alert(b);

	if (a > b * 0.5625) {						
	  	$("#player").css('height', c);
	  	$("#player").css('width', b);	  	
	}
	else
	{				
		$("#player").css('width', e)
		$("#player").css('height', a);
	}
</script>
<section id="content">
	
	<!-- NAVBAR MENU  -->
	<div class="" style="position: absolute; width: 100%; z-index: 1;">
		<div class="pull-left p-5 p-t-10">			
			<img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
		</div>
		<div class="pull-right p-15">
			<!-- <div class="m-l-5" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
				<a data-toggle="modal" href="#modalDefault"><img src="<?php echo base_url(); ?>aset/img/icons/close.png" height="30px" width="30px"></a>
			</div> -->
			<!-- <div class="m-l-5" id="showchat" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
				<a ><img src="<?php echo base_url(); ?>aset/img/icons/comment.png" height="35px" width="35px"></a>
			</div> -->
			<!-- <div class="m-l-5" id="showpapan" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
				<a href="#"><img src="<?php echo base_url(); ?>aset/img/icons/whiteboard.png" height="35px" width="35px"></a>
			</div> -->
			<div class="m-l-5" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
				<a data-toggle="modal" href="#modalDefault"><img src="<?php echo base_url(); ?>aset/img/icons/raisehandd.png" height="35px" width="35px"></a>
			</div>
		</div>
	</div>

	<div class="outer-container" style="z-index: 2;">
	    <div class="inner-container">	    	    		                	        
	        <div class="video-overlay" id="papanchat" style="z-index: 10;">
	    		
	    		<div class="video-overlay1" style="position: absolute;">
	    			<div style="height:32vh; bottom: 0; overflow-y: auto; margin-bottom: 2%;">
		    			<div class="chating">
		    				<label>Robith Ritz<br><small style="word-wrap: break-word;">koasdflksdjfkladsjfkladsjlfdaslfjldsakfjkldasjfkldsajflkdasjflkdsajklfjdsalkfjlksdafjklsadjflkdsajflkdsjflksdjflkdsjf</small></label>
		    			</div>
		    			<div class="chating">
		    				<label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
		    			</div>
		    			<div class="chating">
		    				<label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
		    			</div>
		    			<div class="chating">
		    				<label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
		    			</div>
		    			<div class="chating">
		    				<label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
		    			</div>
	    			</div>
	    			<div style="width: 100%;">
	    				<input type="text" name="sent" class="input form-control c-black" style="padding: 5px;" placeholder="Enter your message">
	    			</div>
	    		</div>
	    	</div>
	    	<video id="player" src="<?php echo base_url(); ?>aset/video/line.mp4" autoplay muted loop></video>	    	
	    </div>
	    <div class="papanwhiteboard c-black">
	    	<a href="" id="papanwhiteboard"><img src="<?php echo base_url() ?>aset/img/icons/rotate-3d.png" width="35px" height="35px" style="z-index: 1; position: absolute; margin: 2%; left:0;"></a>
	    	<h3>Ini WhiteBoard</h3>
	    </div>
	    <div class="papanscreen c-black">
	    	<a href="" id="papanscreen"><img src="<?php echo base_url() ?>aset/img/icons/rotate-3d.png" width="35px" height="35px" style="z-index: 1; position: absolute; margin: 2%; left:0;"></a>
	    	<h3>Ini Screen Share</h3>
	    </div>
	</div>

	<!-- MODAL RAISE HAND -->	
    <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">   
                <div class="modal-body">
                	<video src="<?php echo base_url(); ?>aset/video/line.mp4" autoplay loop muted style="height: 30vh; width: 100%; padding: 0; margin-top: 10%;"></video>	
                	<video src="<?php echo base_url(); ?>aset/video/juki.mp4" autoplay loop muted style="height: 30vh; width: 100%; padding: 0; margin-top: 10%;"></video>	
                    
                </div>             
                <div class="modal-footer">                    
                    <hr>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hangout</button>                    
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
	
	// $("#papanshow2").css('display','none');
	$("#showpapan").click(function(){
		$("#content").toggleClass('toggle');
	});

	$("#showchat").click(function(){
		$("#papanchat").toggleClass('toggle');
	});

	$("#player").click(function(){
		$("#papanchat").toggleClass('toggle');
	});

	$("#papanwhiteboard").click(function(){
		$(".papanwhiteboard").css('display','none');
		$(".papanscreen").css('display','inline-block');
	});

	$("#papanscreen").click(function(){
		$(".papanscreen").css('display','none');
		$(".papanwhiteboard").css('display','inline-block');
	});
</script>


	<!-- FULL VIDEO -->	
	<!-- <div id="video" class="bgm-cyan" style="height: 50vh; padding: 0px;">	
		<video id="video2" class="video2" src="<?php echo base_url(); ?>aset/video/bigbunny.mp4" autoplay></video>
		sadflksdjflksadjklfjdsalfjasldkfjladskjflksdjflksajflaksjf ldasj flksdjflk dslkf slda falsd jflsk dj	 -->
		<!-- FOOTER CHAT -->
	<!-- 	<div id="footer" style="height: 35vh;" class="bgm-blue">

			<div id="menu1">
				<div class="pull-left">
				<div class="col-md-12 bgm-gray p-15 c-gray" style="opacity: 0.8; bottom: 0px;">
					<input type="text" name="sent" class="input">
				</div>
				</div>
				<div class="pull-right">
				<div class="col-md-6 bgm-gray c-gray" style="opacity: 0.8;">Kirim</div>
				</div>
			</div>
		</div>
	</div> -->

	<!-- VIDEO + WHITEBOARD / SCREEN SHARE -->
	<!-- <div id="video" class="" style="height: 60vh; padding: 0px;">
		<video id="video2" class="video2" src="<?php echo base_url(); ?>aset/video/bigbunny.mp4" autoplay loop></video>	
	</div>
	<div id="whiteboard" class="bgm-blue" style="height: 40vh; padding: 0px; object-fit: fill;">		
		INI WHITEBOARD		
	</div> -->

	