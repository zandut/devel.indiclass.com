<style type="text/css">
    html, body{
      height: 100%;
      width: 100%;
      overflow-x: hidden;
      overflow-y: hidden;
      background-color: rgba(0, 0, 0, 1);
    }
</style>
<div id="loader">
    <div class="page-loader" style="background-color:#1274b3;">
        <div class="preloader pl-xl pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Classmiles. . .</p>
        </div>
    </div>
</div>

<img class="hidden-xs" src="<?php echo base_url(); ?>aset/img/logo/LogoIjo.png" style='position: absolute; top: 0; left: 0; margin-left: 5%; margin-top: 1%; z-index: 99999;' alt="">
<header id="header" class="clearfix" style="display: none; background-color: rgba(0, 0, 0, 0.1); width: 99%;">         

    <div style="margin-left: 5%; margin-right: 5%;">
        <ul class="header-inner clearfix">
            <li id="menu-trigger" data-trigger=".ha-menu" class="visible-xs">
                <div class="line-wrap">
                    <div class="line top"></div>
                    <div class="line center"></div>
                    <div class="line bottom"></div>
                </div>
            </li>
            <!-- <li class="logo hidden-xs">
                <a href="#" class="m-l-20"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" style='margin-top: -7px;' alt=""></a>
            </li> -->

            <li class="pull-right">
                
                <div id="usertypestudent" style="display: none;">
                    <ul class="top-menu">
                        <li data-toggle="tooltip" data-placement="bottom" title="Raise Hands" style="cursor: pointer;" id="tour1">
                            <!-- <a data-toggle="modal" href="#videoCallModalstudent"> -->
                            <a id="hangupssss" class="tutuprhh" style="display: none;">
                                <div style="background: url('../aset/img/icons/close.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px;">
                                </div>
                            </a>
                            <a id="raisehandclick">
                                <div id="icontangan">
                                    <div id="tanganbiasa" style="background: url('../aset/images/raisehandd.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px;">
                                    </div>
                                    <div id="tanganmerah" style="background: url('../aset/images/raisehanddd.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px; display: none;">
                                    </div>
                                </div>
                            </a>               
                        </li>
                        <!-- <li title="Show/Hide ScreenShare" style="cursor: pointer;" id="tour12">
                            <a id="ssshowhide"><i class="tm-icon zmdi zmdi-window-maximize"></i></a>               
                        </li> -->
                        <li title="Chat" style="cursor: pointer;" id="tour3">
                          <a id="chat-trigger" data-trigger="#chat"><img src='../aset/img/baru/chatnotif/chat icon-03.png' class="iconchatstudent" id="klikchat" style='height:23px; width:23px; margin-top: 7px;'/></a>               
                        </li>                                
                        <li title="Tour" style="cursor: pointer;" id="tour4">
                            <a id="tourlagi"><i class="tm-icon zmdi zmdi-navigation"></i></a>               
                        </li>                        
                      <li title="Logout" style="cursor: pointer;" id="tour5">
                          <a id="keluarcobaaa"><i class="tm-icon zmdi zmdi-power-setting"></i></a>               
                      </li>
                  </ul>
                </div>

                <div id="usertypetutor" style="display: none;">
                    <ul class="top-menu">
                        <li id="tourtutor1" title="Raise Hands" style="cursor: pointer;"> 
                            <a id="hangups" class="tutuprh" style="display: none;">
                                <div style="background: url('../aset/img/icons/close.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px;">
                                </div>
                            </a>                            
                            <a id="bukakotakraise" data-toggle="dropdown">
                                <div style="background: url('../aset/images/raisehandd.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px;">
                                </div>
                                <i class='tmn-counts bara' id="totalraisehand"></i>
                            </a>                          
                        </li>                        
                        <!-- START PLAY AND PAUSE -->
                        <li id="tourtutor2" id="tempatx" style="cursor: pointer;">
                            <a href="#" id="play" style="display: none;" title="Play"><i class="tm-icon zmdi zmdi-play"></i></a>
                            <a href="#" id="playboongan" style="display: none;" title="Play"><i class="tm-icon zmdi zmdi-play"></i></a>
                            <!-- <button id="play" style="display: none;" title="Play"><i class="tm-icon zmdi zmdi-play"></i></button> -->
                            <a href="#" id="pausee" title="Break"  ><i class="tm-icon zmdi zmdi-pause"></i></a>
                        </li>
                        <!-- END PLAY AND PAUSE -->
                        <!-- START SHOW AND OFF -->

                        <li id="tourtutor3" title="Unpublish" style="cursor: pointer;">
                          <a id="unpublish"><i class="tm-icon zmdi zmdi-eye"></i></a>
                        </li>
                        <!-- END SHOW AND OFF -->
                        <!-- START AUDIO -->
                        <li id="tourtutor4" title="Mute" style="cursor: pointer;">
                          <a id="mute"><i class="tm-icon zmdi zmdi-volume-up"></i></a>
                        </li>
                        <!-- <li id="tourtutor4" title="Mute" style="cursor: pointer;">
                          <a id="mute"><div style="background: url('../aset/img/baru/volume-mute.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px; margin-top: 3px;">
                                </div></a>
                        </li> -->
                        <!-- END AUDIO -->
                        <!-- START SCREENSHARE -->
                        <li id="tourtutor5" title="Open Screen Share" style="cursor: pointer;">
                          <a id="openscreenshare" target="_blank"><i class="tm-icon zmdi zmdi-window-maximize"></i></a>
                        </li>                    
                        <!-- END SCREEN SET -->
                        <li id="tourtutor6" title="Chat" style="cursor: pointer;">
                          <a id="chat-trigger" data-trigger="#chat"><img src='../aset/img/baru/chatnotif/chat icon-03.png' class="iconchattutor" id="klikchat" style='height:23px; width:23px; margin-top: 7px;'/></a>
                        </li>
                        <li id="tourtutor7" title="Settings" style="cursor: pointer;">
                          <a id="devicedetect"><img src='../aset/img/baru/camera1-02.png' style='height:23px; width:23px; margin-top: 7px;'/></a>
                        </li>
                        <li id="tourtutor8" title="Tour" style="cursor: pointer;" >
                            <a id="tourlagii"><i class="tm-icon zmdi zmdi-navigation"></i></a>               
                        </li>
                        <li id="tourtutor9" title="Logout" style="cursor: pointer;">
                            <a id="keluarcobaa"><i class="tm-icon zmdi zmdi-power-setting"></i></a>               
                        </li>

                    </ul>
                </div>

            </li>
        </ul>        

    </div>

</header>

<section id="" class="tampilanweb" style="overflow: hidden; height: 100%; width: 100%; top: 0;">
    

    <aside id="chat" class="sidebar c-overflow" style="overflow: hidden; margin-top: 1.5%; background-color: rgba(0, 0, 0, 0.5);">         
        <div class="col-md-12" id="panel1">
            <div class="chat-search" id="panel2" style="height: 40px; margin-top: -1%;">
                <label id="totalstudent" class="c-white"></label>            
            </div>
            <hr>

            <div id="chatBoxScroll" class="ba c-white" style="overflow-y: auto; margin-top: -6%; margin-bottom: -6%;">
                
            </div>
            
            <hr>
            <div class="chat-search" id="panel3" style="height: 80px; width: 100%; padding: 0;">            
                <div class="fg-line" id="panel4"> 
                    <textarea maxlength="300" id="chatInput" rows="4" style="width:100%; border: 1px #ececec solid; resize: none; padding: 0; background-color: rgba(0, 0, 0, 0.8); color: white; padding: 3%;" class="form-control btn-block c-white" placeholder="Type your message"></textarea>          
                </div>
            </div><br>
        </div>        
    </aside>
    
    <section id="content"> 
        
        <input type="text" name="usertypelogin" id="usertypelogin" hidden>
        <div id="tempatstudent" style="display: none; width: 100%; height: 100%; position: fixed;">
            <section class="row" id="container2" style="width:100%; height:100%;">
            <!-- <div class="container"> -->
            	
                <div id="menurightstudent">
     				<a id="hiddenmenustudent" title="Hidden Menu" style="position: absolute; bottom: 0; right: 0; z-index: 10; margin-bottom: 7%; margin-right: 2.5%; background-color: rgba(0, 0, 0, 0.5); cursor: pointer;"><img src='../aset/img/baru/move-resize-variant.png' style='height:30px; width:30px; margin-top: 7px;'/></a>
    	            <a id="fullscreen" title="Fullscreen Window" style="position: absolute; bottom: 0; right: 0; z-index: 10; margin-bottom: 10%; margin-right: 2.5%; background-color: rgba(0, 0, 0, 0.5); cursor: pointer;"><img src='../aset/img/baru/fullscreen.png' style='height:30px; width:30px; margin-top: 7px;' onclick="toggleFullScreen();" /></a>
    	            <a id="ssshowhide" title="Show/Hide Menu" style="position: absolute; bottom: 0; right: 0; z-index: 10; margin-bottom: 13%; margin-right: 2.5%; background-color: rgba(0, 0, 0, 0.5); cursor: pointer;"><img src='../aset/img/baru/window-maximize.png' style='height:30px; width:30px; margin-top: 7px;'/></a>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12" id="setengah1" style="height: 100%;">
                    <!-- <div class="move col-md-12" id="whboard" style="height: 100%; background-color: purple; padding: 0; margin: 0;">
                        <img id="pindah1" class="klikbego" src="<?php echo base_url();?>aset/img/icons/Arrow 4-01.png" width="30px" height="30px" style="display:none; z-index: 1; position: absolute; margin: 2%; cursor: pointer;" data-toggle="tooltip"
                        data-placement="bottom" title="Switch Screen">
                        <iframe id="wbcard" width="100%" height="100%" style="padding: 0px; z-index: 1;" ></iframe>
                    </div> -->                    
                    <div id="videoBoxLocal" style="margin-right: 10%;"></div>
                    <video style="width:100%; height:100%;" id="myvideostudent" disabled autoplay poster="../aset/img/baru/barposter_umum.png"/>                    
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6" id="setengah2" style="height: 100%; text-align:center;" align="center">
                    <iframe id="screensharestudent" class="screen-share-iframe" scrolling="no" noresize frameborder="0" allowfullscreen style="width: 100%; height: 100%; max-width: 100%; text-align:center;"  align="center"></iframe>
                </div>
                
            <!-- </div> -->
            </section> 
            <div id="berkas" class="col-md-12" style="display: none;"></div> 

            <footer id="footer" class="footerstudent" style="margin-left: 5%; margin-right: 5%; width: 90%;">
                <div class="col-md-12">
                    <div class="col-md-3 text-left">
                        <label class="c-white f-18 lead"><label id="nametutor"></label>
                        </label>
                    </div>
                    <div class="col-md-6 text-center">
                        <label class="c-white f-18 lead">
                            <label id="classname" class="lead"></label> - 
                            <label id="subclass" class="lead"></label>
                        </label>
                    </div>
                    <div class="col-md-3 text-right">
                        <label class="c-white f-18 lead">
                            <label id="datenow" class="lead"></label>
                        </label>
                    </div>
                </div>                  
            </footer>

        </div>

        <div id="tempattutor" style="display: none; top: 0; position: fixed; width: 100%; height: 100%;">
            
            <div id="menurighttutor">
                <a id="hiddenmenu" title="Hidden Menu" style="position: absolute; bottom: 0; right: 0; z-index: 10; margin-bottom: 7%; margin-right: 2.5%; background-color: rgba(0, 0, 0, 0); cursor: pointer;"><img src='../aset/img/baru/move-resize-variant.png' style='height:30px; width:30px; margin-top: 7px;'/></a>
                <a id="fullscreen" title="Fullscreen Window" style="position: absolute; bottom: 0; right: 0; z-index: 10; margin-bottom: 10%; margin-right: 2.5%; background-color: rgba(0, 0, 0, 0); cursor: pointer;"><img src='../aset/img/baru/fullscreen.png' style='height:30px; width:30px; margin-top: 7px;' onclick="toggleFullScreen();"/></a>  
            </div>

            <video id="myvideo" autoplay loop muted style="width:100%; height:100%;" src="../aset/video/cocacola2.mp4" poster="../aset/img/baru/barposter_umum.png" data-video-aspect-ratio="16:9"></video>
            <div id="videoBoxRemote"  style="margin-right: 10%;">
            <!-- <video class="video2" id="myvideot" style="z-index: 1; position: absolute; margin: 2%; height: 150px; width: 250px; bottom: 0; right: 0;" autoplay loop muted></video> -->
            <!-- <video id="myvideo" autoplay loop muted style="width:100%; height:90%;" src="../aset/video/juki.mp4"></video></div> -->
            
            
        </div>

        <footer id="footer" class="footer" style="margin-left: 5%; margin-right: 5%; width: 90%;">
            <div class="col-md-12">
                <div class="col-md-3 text-left">
                    <label class="c-white f-18 lead"><label id="nametutorr"></label>
                    </label>
                </div>
                <div class="col-md-6 text-center">
                    <label class="c-white f-18 lead">
                        <label id="classnamee" class="lead"></label> - 
                        <label id="subclasss" class="lead"></label>
                    </label>
                </div>
                <div class="col-md-3 text-right">
                    <label class="c-white f-18 lead">
                        <label id="datenoww" class="lead"></label>
                    </label>
                </div>
            </div>                  
        </footer>


    </section>
</section>

<script type="text/javascript">
    var geser = 0;
    
    $("#ssshowhide").click(function(){
        if (geser == 0) {
            $("#setengah1").removeClass();
            $("#setengah1").addClass('col-md-6');
            $("#setengah1").addClass('col-sm-6');
            $("#setengah1").addClass('col-xs-6');
            $("#setengah2").css('display','block');
            geser = 1;
        }
        else
        {            
            $("#setengah1").removeClass();
            $("#setengah1").addClass('col-md-12');
            $("#setengah1").addClass('col-sm-12');
            $("#setengah1").addClass('col-xs-12');
            $("#setengah2").css('display','none');
            geser = 0;
        }
    });

    $("#hiddenmenu").click(function(){    	
    	if ($("#header").css('margin-top') == "0px") {
    		$("#header").css('margin-top', '-15%');
    		$(".footer").css('margin-bottom', '-15%');	
    	}
    	else
    	{
    		$("#header").css('margin-top', '0%');
    		$(".footer").css('margin-bottom', '0%');
    	}    	
    });

    $("#hiddenmenustudent").click(function(){    	
    	if ($("#header").css('margin-top') == "0px") {
    		$("#header").css('margin-top', '-15%');
    		$(".footerstudent").css('margin-bottom', '-15%');	
    	}
    	else
    	{
    		$("#header").css('margin-top', '0%');
    		$(".footerstudent").css('margin-bottom', '0%');
    	}
    	
    });

    function toggleFullScreen() {
		if ((document.fullScreenElement && document.fullScreenElement !== null) ||    // alternative standard method
		  (!document.mozFullScreen && !document.webkitIsFullScreen)) {               // current working methods
			if (document.documentElement.requestFullScreen) {
			  document.documentElement.requestFullScreen();
			} else if (document.documentElement.mozRequestFullScreen) {
			  document.documentElement.mozRequestFullScreen();
			} else if (document.documentElement.webkitRequestFullScreen) {
			  document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
			}
		} 
		else 
		{
			if (document.cancelFullScreen) {
			  document.cancelFullScreen();
			} else if (document.mozCancelFullScreen) {
			  document.mozCancelFullScreen();
			} else if (document.webkitCancelFullScreen) {
			  document.webkitCancelFullScreen();
			}
		}
	}

    function add(int) {    
        $("#classkeluar").modal('show');
    } 

    function hiddenmenu(int){
        if ($("#header").css('margin-top') == "0px") {
            $("#header").css('margin-top', '-15%');
            $(".footer").css('margin-bottom', '-15%');  
        }
        else
        {
            $("#header").css('margin-top', '0%');
            $(".footer").css('margin-bottom', '0%');
        } 
    }

    function hiddenmenustudent(int){
        if ($("#header").css('margin-top') == "0px") {
            $("#header").css('margin-top', '-15%');
            $(".footerstudent").css('margin-bottom', '-15%');  
        }
        else
        {
            $("#header").css('margin-top', '0%');
            $(".footerstudent").css('margin-bottom', '0%');
        } 
    }

    $(document).ready(function(){
            
        $(document)

        .bind('keydown', 'shift+9', function(){
            var usertypelogin = $("#usertypelogin").val();
            if (usertypelogin == "tutor") {
                hiddenmenu();    
            }
            else
            {
                hiddenmenustudent();
            }
        })

        .bind('keydown', 'shift+8', function(){
            var usertypelogin = $("#usertypelogin").val();            
            if (usertypelogin == "tutor") {
                var menurighttutor = $("#menurighttutor").css('display');
                if (menurighttutor == "block") {
                    $("#menurighttutor").css('display','none');
                }
                else
                {
                    $("#menurighttutor").css('display','block');   
                }
            }
            else
            {
                var menurightstudent = $("#menurightstudent").css('display');
                if (menurightstudent == "block") {
                    $("#menurightstudent").css('display','none');
                }
                else
                {
                    $("#menurightstudent").css('display','block');   
                }
            }
        })

        .bind('keydown', 'shift+7', function(){                             
            if (geser == 0) {
                $("#setengah1").removeClass();
                $("#setengah1").addClass('col-md-6');
                $("#setengah1").addClass('col-sm-6');
                $("#setengah1").addClass('col-xs-6');
                $("#setengah2").css('display','block');
                geser = 1;
            }
            else
            {            
                $("#setengah1").removeClass();
                $("#setengah1").addClass('col-md-12');
                $("#setengah1").addClass('col-sm-12');
                $("#setengah1").addClass('col-xs-12');
                $("#setengah2").css('display','none');
                geser = 0;
            }
            
        })        

    });

    // $("#papanscreen").click(function(){        
    //     $(".papanscreen").toggleClass('toggle');      
    // });
</script>

<section id="content" class="tampilanandro" style="display: none;">
    
    <!-- NAVBAR MENU  -->
    <div class="" style="position: absolute; width: 100%; z-index: 1;">
        <div class="pull-left p-5 p-t-10">          
            <img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
        </div>
        <div class="pull-right p-15">
            <!-- <div class="m-l-5" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a data-toggle="modal" href="#modalDefault"><img src="<?php echo base_url(); ?>aset/img/icons/close.png" height="30px" width="30px"></a>
            </div> -->
            <!-- <div class="m-l-5" id="showchat" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a ><img src="<?php echo base_url(); ?>aset/img/icons/comment.png" height="35px" width="35px"></a>
            </div> -->
            <!-- <div class="m-l-5" id="showpapan" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a href="#"><img src="<?php echo base_url(); ?>aset/img/icons/whiteboard.png" height="35px" width="35px"></a>
            </div> -->
            <div class="m-l-5" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a id="raisehandclickAndroid" ><img src="<?php echo base_url(); ?>aset/img/icons/raisehandd.png" height="35px" width="35px"></a>
            </div>
        </div>
    </div>

    <div class="outer-container" style="z-index: 2;">
        <div class="inner-container" id="tempatplayer">                                                              
            <video id="player" autoplay loop></video>          
        </div>
        <div class="video-overlay" id="papanchat" style="z-index: 10; display: none;">
                
            <div class="video-overlay1" style="position: absolute;">
                <div style="height:38vh; bottom: 0; overflow-y: auto; margin-bottom: 2%;" id="chatAndroid">
                    <!-- <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">koasdflksdjfkladsjfkladsjlfdaslfjldsakfjkldasjfkldsajflkdasjflkdsajklfjdsalkfjlksdafjklsadjflkdsajflkdsjflksdjflkdsjf</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div> -->
                </div>
                <div style="width: 100%;">
                    <input maxlength="300" id="chatInputAndroid" type="text" name="sent" class="input form-control c-black" style="padding: 5px;" placeholder="Type your message">
                </div>
            </div>
        </div>
        <div class="papanwhiteboard c-black">
            <a href="#" id="papanwhiteboard"><img src="<?php echo base_url() ?>aset/img/icons/rotate-3d.png" width="35px" height="35px" style="z-index: 1; position: absolute; margin: 2%; left:0;"></a>
            <iframe id="wbcarddd" width="100%" height="100%" style="padding: 0px;"></iframe>
        </div>
        <div class="papanscreen c-black">
            <a href="#" id="papanscreen"><img src="<?php echo base_url() ?>aset/img/icons/rotate-3d.png" width="35px" height="35px" style="z-index: 1; position: absolute; margin: 2%; left:0;"></a>
            <iframe id="screensharestudenttt" class="screen-share-iframe" scrolling="no" frameborder="0" allowfullscreen style="width: 100%; height: 100%; max-width: 100%;" ></iframe>
        </div>
    </div>

    <!-- MODAL RAISE HAND -->   
    <!-- <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">   
                <div class="modal-body">
                    <video src="<?php echo base_url(); ?>aset/video/line.mp4" autoplay loop muted style="height: 30vh; width: 100%; padding: 0; margin-top: 10%;"></video>  
                    <video src="<?php echo base_url(); ?>aset/video/juki.mp4" autoplay loop muted style="height: 30vh; width: 100%; padding: 0; margin-top: 10%;"></video>  
                    
                </div>             
                <div class="modal-footer">                    
                    <hr>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hangout</button>                    
                </div>
            </div>
        </div>
    </div>
 -->
</section>

<section id="content_tutor" class="tampilanandrotutor" style="display: none;">
    
  <div id="deviceConfigurationModals" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><?php echo $this->lang->line('ubahcamera'); ?> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-offset-2 col-md-8" id="buttonlists">
              </div>
            </div>
          </div>
        </div>
      </div>
  </div> 

  <!-- NAVBAR MENU  -->
  <div class="" style="position: absolute; width: 100%; z-index: 1;">
      <div class="pull-left p-5 p-t-10">          
          <img id="devicedetects" src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
      </div>     
  </div>

  <div class="outer-container_tutor" style="z-index: 2;">
      <div class="inner-container_tutor" id="tempatplayer">        
          <video id="player_tutor" autoplay loop></video>          
      </div>
  </div>

</section>

<!-- MODAL KELUAR -->
<div id="classkeluar" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
              <p class="f-15 pull-left m-t-20"><?php echo $this->lang->line('yakinkeluar'); ?></p><br><br><br>
          </div>
          <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="button" class="btn btn-info" id="keluar">OK</button>
          </div>
        </div>
    </div> 
</div>

<!-- MODAL KONFIGURATION -->
<div id="deviceConfigurationModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo $this->lang->line('ubahcamera'); ?> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8" id="buttonlist">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 

<!-- MODAL KONFIGURATION -->
<div id="deviceConfigurationModalWB" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo $this->lang->line('ubahcamera'); ?> Whiteboard<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8" id="buttonlistWB">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 

<!-- MODAL MENU LIST -->
<div id="kotakmenulist" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List User Raise Hands <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8" id="kotakraisehand">
                      <!-- <button class="btn btn-default btn-block" id="iddevice">
                        <label id="listdevice"></label>
                      </button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  

<!-- MODAL TIDAK ADA -->
<div id="tidakadadata" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bgm-orange">
                <h4 class="modal-title c-white">INFO <label id="tutupalert" class="close c-white btn-small" aria-label="Close"><span aria-hidden="true" class="c-white f-13">X</span></label></h4>
            </div>
            <div class="modal-body">
                <br>
                <div class="row">
                    <center><h4>Tidak ada data</h4></center>
                </div>
            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-default" id="hangups" >Hangup</button>
            </div> -->
        </div>
    </div>
</div>

<!-- MODAL WHITEBOARD LIST -->
<div id="whiteboardlist" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bgm-lightblue">
                <h4 class="modal-title c-white">List Whiteboard <label id="tutup" class="close c-white btn-small" aria-label="Close"><span aria-hidden="true" class="c-white f-13">X</span></label></h4>
            </div>
            <div class="modal-body">
                <br>
                <div class="row">
                    <button class="btn btn-info btn-block" id="digitalwhiteboard">Digital Whiteboard</button>
                    <button class="btn btn-info btn-block" id="webcamwhiteboard">Webcam Whiteboard</button>
                </div>
            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-default" id="hangups" >Hangup</button>
            </div> -->
        </div>
    </div>
</div>

<!-- MODAL CALLING MODAL -->
<div id="callingModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Calling ... <button type="button" id="hangupss" class="close" aria-label="Close"><span aria-hidden="true">X</span></button></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <b id="messageCalling"></b>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
