<style type="text/css">
  html, body, main-content {
      height:100%;
      overflow:hidden;
      max-height: 100%;
      /*background-image: url(../aset/img/headers/BGclassroom-01.png);*/
      /*background-color: #0FA7A7;*/
      background-color: #000000;
      background-repeat: no-repeat; 
      background-position: 0 0;
      background-size: cover; 
      background-attachment: fixed;        
  }
  .video2 {
        object-fit: fill;
        padding: 0px;
        width: 100%;
        height: 100%;        
    }
    #menu1{
        bottom: 0px;
        position: relative;
    }
    .outer-container {
        width: 100%;
        height: 100%;
        text-align: center;
    }
    .inner-container {
        display: inline-block;
        position: relative;
        width: 100%;
        background-color: #ececec;      
    }
    .papanwhiteboard {
        display: inline-block;
        position: relative;
        width: 100%;
        background-color: #ececec;
        height: auto;
    }
    .papanscreen {
        display: none;
        position: relative;
        width: 100%;
        background-color: #ececec;
        height: auto;
    }
    .video-overlay {
        position: absolute;
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 100%;
        height: 46vh;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.3);*/
    }
    .toggle-overlay {
        position: absolute;
        right: 0;
        bottom: 0;        
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 20%;
        height: 25vh;
        float: right;
        /*background-color: rgba(50, 50, 50, 0.3);*/
    }
    .toggle-overlay1 {
        right: 0;
        bottom: 0;
        margin: 0px;
        margin-left: 2%;
        margin-bottom: 2%;      
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 95%;
        float: right;
        /*background-color: rgba(50, 50, 50, 0.4);*/
    }
    .chating {
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        margin-bottom: 3%;
        font-size: 16px;
        font-family: Helvetica;
        color: #FFF;
        width: 100%;
        float: left;
        text-align: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    .video-overlay1 {
        left: 0px;
        bottom: 5px;
        margin: 0px;
        margin-left: 2%;
        margin-bottom: 2%;      
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 95%;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.4);*/
    }
    .video-overlay2 {
        right: 0px;
        bottom: 0px;
        margin: 0px;
        padding: 5px 5px;
        margin-left: 3%;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 30%;
        float: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    /*#playerse {
        width: 100%;
        height: 20vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #playerse.toggle {
        width: 100%;
        height: 20vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }*/
    #player_tutor {
        width: 100%;
        height: 100vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #player_tutor.toggle {
        width: 100%;
        height: 100vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #content {
        width: 100%;
        height: 100vh;
        z-index: 2;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #content_tutor {
        width: 100%;
        height: 100vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #content.toggle {
        width: 100%;
        height: 50vh;
        padding: 0; margin: 0;      
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #content_tutor.toggle {
        width: 100%;
        height: 100vh;
        padding: 0; margin: 0;      
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #papanchat{
        display: block;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #papanchat.toggle{
        display: none;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    @media all and (orientation:portrait) {
        #content {
            width: 100%;
            height: 60vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #playerse {
            width: 100%;
            height: 38vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }  
        #content_tutor {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #player_tutor {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        .papanwhiteboard {
            display: inline-block;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 70vh;
        }
        .papanscreen {
            display: none;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 40vh;
        }
    }

    @media all and (orientation:landscape) {
        #playerse {
            width: 140px;
            height: 120px;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #content_tutor {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #player_tutor {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
    }
</style>
 
<section id="content" class="tampilanandro" style="display: none;">

    <input type="text" name="orientation" id="orientation" hidden>
    <div id="potrait_design">
    
        <!-- NAVBAR MENU  -->
        <div class="" style="position: absolute; width: 100%; z-index: 1;">
            <div class="pull-left p-5 p-t-10">          
                <img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
            </div>
        </div>

        <div class="outer-container" style="z-index: 2;">
            <div class="inner-container" id="tempatplayer">                     
                <div id="videoBoxLocal"></div>                                                         
                <video id="playerse" autoplay loop poster="../aset/img/baru/barposter_umum.png" data-video-aspect-ratio="16:9"></video>          
            </div>
            <div class="video-overlay" id="papanchat" style="z-index: 10; display: block;">                
                <div class="video-overlay1" style="position: absolute;">
                    <div style="height:38vh; bottom: 0; overflow-y: auto; margin-bottom: 2%;" id="chatAndroid">
                    </div>
                    <div style="width: 100%;">
                        <input maxlength="300" id="chatInputAndroid" type="text" name="sent" class="input form-control c-black" style="padding: 5px;" placeholder="Type your message">
                    </div>
                </div>
            </div> 
            <div id="boxshowhide" style="display: none;">
                <div class="papanwhiteboard c-black" id="tempatwhite">
                    <iframe id="wbcarddd" width="100%" height="100%" style="padding: 0px;"></iframe>
                </div>       
                <div class="papanscreen c-black bgm-orange">            
                    <div class="bgm-black" style="height: 431px;" id="kotakscreen">
                        <iframe id="screensharestudenttt" class="screen-share-iframe" scrolling="no" frameborder="0" allowfullscreen style="width: 100%; height: 100%; max-width: 100%;" ></iframe>
                    </div>
                </div>
            </div>
            <div class="toggle-overlay" id="papantoggle" style="z-index: 11; display: block;">
                <div class="toggle-overlay1" style="position: absolute;">
                    <div style="height:auto;bottom: 0; right: 0; overflow-y: auto;" id="toggle">
                        <div style="margin-top: 2%;" id="tempatraise">                    
                            <button id="raisehandclickAndroid" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10">
                                <img src="<?php echo base_url(); ?>aset/img/icons/raisehandd.png" height="20px" width="20px">
                            </button>                            
                        </div>
                        <div style="margin-top: 2%; display: none;" id="tempathangup">
                            <button id="hangups" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10">
                                <img src="<?php echo base_url(); ?>aset/img/icons/close.png" height="20px" width="20px">
                            </button>
                        </div>
                        <div style="margin-top: 2%;">                    
                            <button id="showhide" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><img src="../aset/img/baru/camera-switch.png" style="height: 25px; width: 25px; margin-top: -1%;"></button>
                        </div>
                        <div id="kotakswitch" style="display: none;">
                            <div style="margin-top: 2%;">                    
                                <button id="papanwhiteboard" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><img src="../aset/img/baru/pencil-02.png" style="height: 25px; width: 25px; margin-top: -1%;"></button>
                            </div>
                            <div style="margin-top: 2%;">                    
                                <button id="papanscreen" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10">
                                <img src="../aset/img/baru/screenshare-02.png" style="height: 25px; width: 25px; margin-top: -3%;">
                                </button>
                            </div>
                        </div>
                        <div style="margin-top: 2%;">
                            <button id="showchat"  class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10">
                              <img src="<?php echo base_url(); ?>aset/img/icons/comment.png" height="20px" width="20px">
                            </button>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>

    <div id="landscape_design" style="display: none;">

        <!-- NAVBAR MENU  -->
        <div class="" style="position: absolute; width: 100%; z-index: 1;">
            <div class="pull-left p-5 p-t-10">          
                <img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
            </div>
            <div class="pull-right p-15">
                <div class="m-l-5" style="float: right;">
                    <button id="showchatt" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10">
                        <img src="<?php echo base_url(); ?>aset/img/icons/comment.png" height="20px" width="20px">
                    </button>
                </div>          
                <div class="m-l-5" style="float: right;">
                    <button id="papanscreenn" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><img src="../aset/img/baru/screenshare-02.png" style="height: 25px; width: 25px; margin-top: -3%;"></button>
                </div>
                <div class="m-l-5" style="float: right;">
                    <button id="papanwhiteboardd" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><img src="../aset/img/baru/pencil-02.png" style="height: 25px; width: 25px; margin-top: -3%;"></button>
                </div>
                <div class="m-l-5" style="float: right;" id="tempatraisee">
                    <button id="raisehandclickAndroidd" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10">
                        <img src="<?php echo base_url(); ?>aset/img/icons/raisehandd.png" height="20px" width="20px">
                    </button>
                </div>
                <div class="m-l-5" style="float: right; display: none" id="tempathangupp">
                    <button id="hangupsss" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10">
                        <img src="<?php echo base_url(); ?>aset/img/icons/close.png" height="20px" width="20px">
                    </button>
                </div>
            </div>
        </div>

        <div class="outer-container" style="z-index: 2;">
          <div style="position: relative; display: inline-block; width: 100%; height: 100vh;">                
                <div class="papanscreenn c-black">         
                    <div style="height: 100vh;" id="kotakscreenn">
                    </div>
                </div>
                <div id="tempatplayerr" style="position: absolute; z-index: 11; bottom: 0; right: 0; height: 120px; width: 150px; margin-bottom: 1.5%; margin-right: 1.5%;">
                
                </div>
                <div id="tempatraisehand" style="position: absolute; z-index: 11; bottom: 0; right: 0; height: 120px; width: 150px; margin-bottom: 1.5%; margin-right: 27%; ">                    
                </div>
                <div class="papanwhiteboardd c-black" style="height: 100vh;" id="tempatwhitee">
                    
                </div> 
                <div class="video-overlay" id="papanchatt" style="z-index: 10; display: block;">                
                  
                </div>                  
            </div>
        </div>
    </div>

</section>

<section id="content_tutor" class="tampilanandrotutor" style="display: none;">
    
    <div id="deviceConfigurationModals" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><?php echo $this->lang->line('ubahcamera'); ?> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8" id="buttonlists">
                          <!-- <button class="btn btn-default btn-block" id="iddevice">
                            <label id="listdevice"></label>
                          </button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  

    <!-- NAVBAR MENU  -->
    <div class="" style="position: absolute; width: 100%; z-index: 1;">
        <div class="pull-left p-5 p-t-10">          
            <img id="devicedetects" src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
        </div>     
    </div>

    <div class="outer-container_tutor" style="z-index: 2;">
        <div class="inner-container_tutor" id="tempatplayer">
            <video id="playerse_tutor" autoplay loop></video>          
        </div>
    </div>

</section>

<div id="callingModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Calling ... <button type="button" id="hangupss" class="close" aria-label="Close"><span aria-hidden="true">X</span></button></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <b id="messageCalling"></b>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
    
    // $("#papanshow2").css('display','none');
    $("#showpapan").click(function(){
        $("#content").toggleClass('toggle');
    });

    $("#showchat").click(function(){
        $("#papanchat").toggleClass('toggle');
    });    
    
    // $("#playerse").click(function(){
    //     $("#papanchat").toggleClass('toggle');
    // });

    $("#papanwhiteboard").click(function(){                
        $(".papanscreen").css('display','none');
        $(".papanwhiteboard").css('display','block');
    });

    $("#papanscreen").click(function(){                   
        $(".papanwhiteboard").css('display','none');
        $(".papanscreen").css('display','block');     
    });

    $("#papanwhiteboardd").click(function(){                
        $(".papanscreenn").css('display','none');
        $(".papanwhiteboardd").css('display','block');
    });

    $("#papanscreenn").click(function(){                   
        $(".papanwhiteboardd").css('display','none');
        $(".papanscreenn").css('display','block');     
    });

    $("#showhide").click(function(){
        var display = $("#boxshowhide").css('display');
        
        if (display == "none") {            
            block();
        }
        if (display == "block")
        {
            none();
        }
    });

    function block(){
        $("#boxshowhide").css('display','block');
        $("#kotakswitch").css('display','block');        
    }

    function none(){        
        $("#boxshowhide").css('display','none');
        $("#kotakswitch").css('display','none');
    }
    
    var mql = window.matchMedia("(orientation: portrait)");
    // If there are matches, we're in portrait
    if(mql.matches) {  
        $("#orientation").val('');
        $("#orientation").val('potrait');
        // Portrait orientation
        $("#landscape_design").css('display','none');
        $("#potrait_design").css('display','block');  
        $('#playerse').appendTo('#tempatplayer');
        $(".video-overlay1").appendTo('#papanchat');
        $("#screensharestudenttt").appendTo('#kotakscreen');
        $("#wbcarddd").appendTo('#tempatwhite') ;
        $("#videoBoxLocal").appendTo("#tempatplayer");
        $('#playerse').get(0).play();
        $("#videoCallLocal").css('z-index','1');
        $("#videoCallLocal").css('position','absolute');
        $("#videoCallLocal").css('margin-bottom','2%');
        $("#videoCallLocal").css('margin-right','-4%');
        $("#videoCallLocal").css('height','120px');
        $("#videoCallLocal").css('width','150px');
        $("#videoCallLocal").css('bottom','0');
        $("#videoCallLocal").css('right','0');
        $('#playerse').css('width','100%');
    } else {  
        // Landscape orientation
        $("#orientation").val('');
        $("#orientation").val('landscape');
        $("#potrait_design").css('display','none');
        $("#landscape_design").css('display','block');       
        $(".video-overlay1").appendTo('#papanchatt');
        $("#screensharestudenttt").appendTo('#kotakscreenn');
        $("#videoBoxLocal").appendTo("#tempatraisehand");
        $("#wbcarddd").appendTo('#tempatwhitee');
        $('#playerse').appendTo('#tempatplayerr');              
        $('#playerse').get(0).play();
        $("#videoCallLocal").css('height','120px');
        $("#videoCallLocal").css('width','150px');
        $('#playerse').css('width','150px');  
    }

    $(document).on("pagecreate",function(event){            
        $(window).on("orientationchange",function(){                
            if(window.orientation == 0)
            {
                
                $("#orientation").val('');
                $("#orientation").val('potrait');
                $("#landscape_design").css('display','none');
                $("#potrait_design").css('display','block');  
                $('#playerse').appendTo('#tempatplayer');
                $(".video-overlay1").appendTo('#papanchat');
                $("#screensharestudenttt").appendTo('#kotakscreen');
                $("#videoBoxLocal").appendTo("#tempatplayer");
                $("#wbcarddd").appendTo('#tempatwhite') ;
                $('#playerse').get(0).play();                
                $('#playerse').css('width','100%');                  
                $("#videoCallLocal").css('z-index','1');
                $("#videoCallLocal").css('position','absolute');
                $("#videoCallLocal").css('margin-bottom','2%');
                $("#videoCallLocal").css('margin-right','-4%');
                $("#videoCallLocal").css('height','120px');
                $("#videoCallLocal").css('width','150px');
                $("#videoCallLocal").css('bottom','0');
                $("#videoCallLocal").css('right','0');
                $('#videoCallLocal').get(0).play();
            }
            else
            {        
                       
                $("#orientation").val('');
                $("#orientation").val('landscape');
                $("#potrait_design").css('display','none');
                $("#landscape_design").css('display','block');       
                $(".video-overlay1").appendTo('#papanchatt');
                $("#screensharestudenttt").appendTo('#kotakscreenn');
                $("#videoBoxLocal").appendTo("#tempatraisehand");
                $("#wbcarddd").appendTo('#tempatwhitee');
                $('#playerse').appendTo('#tempatplayerr');              
                $('#playerse').get(0).play();                  
                $('#playerse').css('width','150px');  
                $("#videoCallLocal").css('height','120px');
                $("#videoCallLocal").css('width','150px');
                $('#videoCallLocal').get(0).play();
            }
        });                   
    });
</script>



