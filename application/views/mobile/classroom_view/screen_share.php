<div class="row" ng-if="!isAlreadyIn && user.userType == 'tutor'">
    <div class="container" style="margin-top:2em">
        <center><span dynamic="checkIframeOrNot('html')"></span></center>
        <button ng-show="checkIframeOrNot('boolean')" ng-disabled="!showButtonShare" type="button" ng-click="preScreenShare()" class="btn btn-primary btn-lg btn-block">{{getButtonLabel()}}</button>
    </div>
</div>

<!-- ==================================================================================== -->

<div class="row" ng-show="isAlreadyIn">
    <div class="col-md-12">
        <button class="btn btn-danger" style="position:absolute;top:10px;right:30px;z-index:100" ng-if="user.userType == 'tutor'" type="button" ng-click="exitPresentation()">Exit</button>
        <video style="width: 100%; height: 100%;" ng-repeat="x in listFeeds | filter:{'rf_userType':'tutor'} | limitTo:1" ng-src="{{x.rf_stream | trustUrl}}" autoplay></video>
    </div>
</div>

<!-- ==================================================================================== -->

<div class="row " ng-if="!isAlreadyIn && user.userType=='student' ">
    <center>
        <img src="./aset/img/ScreenShare.jpg" style="width: 100%; height: 100%;">
    </center>
</div>