<div id="loader">
    <div class="page-loader" style="background-color:#1274b3;">
        <div class="preloader pl-xl pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Class Miles . . .</p>
        </div>
    </div>
</div>

<style type="text/css">
	html, body, main-content {
	    height:100%;
	    overflow:hidden;
	    max-height: 100%;
	    background-image: url(../aset/img/headers/math.jpg);
	    background-repeat: no-repeat; 
	    background-position: 0 0;
	    background-size: cover; 
	    background-attachment: fixed;
	}
	.video2 {
        object-fit: fill;
        padding: 0px;
        width: 100%;
        height: 100%;        
    }
    #menu1{
        bottom: 0px;
        position: relative;
    }
    .outer-container {
        width: 100%;
        height: 100%;
        text-align: center;
    }
    .inner-container {
        display: inline-block;
        position: relative;
        width: 100%;
        background-color: #ececec;      
    }
    .papanwhiteboard {
        display: inline-block;
        position: relative;
        width: 100%;
        background-color: #ececec;
        height: auto;
    }
    .papanscreen {
        display: none;
        position: relative;
        width: 100%;
        background-color: #ececec;
        height: auto;
    }
    .video-overlay {
        position: absolute;
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 100%;
        height: 46vh;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.3);*/
    }
    .chating {
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        margin-bottom: 3%;
        font-size: 16px;
        font-family: Helvetica;
        color: #FFF;
        width: 100%;
        float: left;
        text-align: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    .video-overlay1 {
        left: 0px;
        bottom: 5px;
        margin: 0px;
        margin-left: 2%;
        margin-bottom: 2%;      
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 95%;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.4);*/
    }
    .video-overlay2 {
        right: 0px;
        bottom: 0px;
        margin: 0px;
        padding: 5px 5px;
        margin-left: 3%;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 30%;
        float: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    #player {
        width: 100%;
        height: 60vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #player.toggle {
        width: 100%;
        height: 60vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #content {
        width: 100%;
        height: 100vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #content.toggle {
        width: 100%;
        height: 50vh;
        padding: 0; margin: 0;      
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #papanchat{
        display: block;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #papanchat.toggle{
        display: none;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    @media all and (orientation:portrait) {
        #content {
            width: 100%;
            height: 60vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #player {
            width: 100%;
            height: 60vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        .papanwhiteboard {
            display: inline-block;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 40vh;
        }
        .papanscreen {
            display: none;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 40vh;
        }
    }

    @media all and (orientation:landscape) {
        #content {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #player {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
    }
</style>

<header id="header" class="clearfix" data-current-skin="lightblue">    
    <div style="margin-left: 7%; margin-right: 9%;">
        <ul class="header-inner clearfix">
            <li id="menu-trigger" data-trigger=".ha-menu" class="visible-xs">
                <div class="line-wrap">
                    <div class="line top"></div>
                    <div class="line center"></div>
                    <div class="line bottom"></div>
                </div>
            </li>
            <li class="logo hidden-xs">
                <a href="#" class="m-l-20"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" style='margin-top: -7px;' alt=""></a>
            </li>
            <li class="pull-right">            

                <div id="usertypestudent" style="display: none;">
                    <ul class="top-menu">
	                    <li data-toggle="tooltip" data-placement="bottom" title="Raise Hands" style="cursor: pointer;" id="tour1">
	                        
	                        <a id="raisehandclick">
	                        	<div id="icontangan">
		                            <div id="tanganbiasa" style="background: url('../aset/images/raisehandd.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px;">
		                            </div>
		                            <div id="tanganmerah" style="background: url('../aset/images/raisehanddd.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px; display: none;">
		                            </div>
	                            </div>
	                        </a>               
	                    </li>
	                    <!-- <li class="dropdown" id="tour2">
	                        <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-tv-list"></i></a>
	                        <ul class="dropdown-menu dm-icon pull-right" style="margin: 5px;">                    
	                            <li style="margin:4%;">
	                                <div id="checkwhiteboard" style="border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
	                                    <input type="checkbox" name="klikwhiteboard" id="klikwhiteboard" value="klikwhiteboard" checked onchange="toggleCheckbox(this)"  style="display: none;">
	                                    <label for="klikwhiteboard" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5 f-12" rel="tooltip" style="width: 90%; height: 30px;">Whiteboard.</label>
	                                </div>
	                            </li>
	                            <li style="margin:4%;">
	                                <div id="checkvideo" style="margin-top: -6%; border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
	                                    <input type="checkbox" name="klikvideo" id="klikvideo" value="klikvideo" checked onchange="toggleCheckbox(this)" style="display: none; margin-top: -3%;">
	                                    <label for="klikvideo" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5" rel="tooltip" style="width: 90%; height: 30px;">Video.</label>
	                                </div>
	                            </li>
	                            <li style="margin:4%;">
	                                <div id="checkscreen" style="margin-top: -6%; border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
	                                    <input type="checkbox" name="klikscreen" id="klikscreen" value="klikscreen" checked onchange="toggleCheckbox(this)" style="display: none; margin-top: -3%; ">                                        
	                                    <label for="klikscreen" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5" rel="tooltip" style="width: 90%; height: 30px;">Screen Share.</label></p>
	                                </div>
	                            </li>
	                        </ul>
	                    </li> -->

	                    <!-- <li data-toggle="tooltip" data-placement="bottom" title="Chat" style="cursor: pointer;" id="tour3">
	                        <a id="chat-trigger" data-trigger="#chat"><i class="tm-icon zmdi zmdi-comment-alt-text" id="klikchat"></i></a>               
	                    </li>   -->                              
	                    <!-- <li data-toggle="tooltip" data-placement="bottom" title="Settings">
	                        <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-settings"></i></a>
	                    </li> -->
                        <!-- <li data-toggle="tooltip" data-placement="bottom" title="Tour" style="cursor: pointer;" id="tour4">
                            <a id="tourlagi"><i class="tm-icon zmdi zmdi-navigation"></i></a>               
                        </li> -->
	                    <li data-toggle="tooltip" data-placement="bottom" title="Logout" style="cursor: pointer;" id="tour5">
	                        <a id="keluarcobaaa"><i class="tm-icon zmdi zmdi-power-setting"></i></a>               
	                    </li>
	                    <!-- <li  style="cursor: pointer;">
	                        <a id="clearinterval"><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>               
	                    </li>   -->
                	</ul>
                </div>

                <div id="usertypetutor" style="display: none;">
                    <ul class="top-menu">
                    	<li data-toggle="tooltip" id="tourtutor1" data-placement="bottom" title="Raise Hands" style="cursor: pointer;">                            
                            <a id="bukakotakraise" data-toggle="dropdown">
                                <div style="background: url('../aset/images/raisehandd.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px;">
                                </div>
                                <i class='tmn-counts bara' id="totalraisehand"></i>
                            </a>                          
                        </li>                        
                        <!-- START PLAY AND PAUSE -->
                        <li data-toggle="tooltip" id="tourtutor2" id="tempatx" data-placement="bottom" style="cursor: pointer;">
                            <a href="#" id="play" style="display: none;" title="Play"><i class="tm-icon zmdi zmdi-play"></i></a>
                            <a href="#" id="playboongan" style="display: none;" title="Play"><i class="tm-icon zmdi zmdi-play"></i></a>
                            <!-- <button id="play" style="display: none;" title="Play"><i class="tm-icon zmdi zmdi-play"></i></button> -->
                            <a href="#" id="pause" title="Break"  ><i class="tm-icon zmdi zmdi-pause"></i></a>
                        </li>
                        <!-- END PLAY AND PAUSE -->
                        <!-- START SHOW AND OFF -->

                        <li data-toggle="tooltip" id="tourtutor3" data-placement="bottom" style="cursor: pointer;">
                          <a href="#" id="unpublish"><i class="tm-icon zmdi zmdi-eye"></i></a>
                        </li>
                        <!-- END SHOW AND OFF -->
                        <!-- START AUDIO -->
                        <li data-toggle="tooltip" id="tourtutor4" data-placement="bottom" style="cursor: pointer;">
                          <a href="#" id="mute"><i class="tm-icon zmdi zmdi-volume-up" ></i></a>
                        </li>
                        <!-- END AUDIO -->
                        <!-- START SCREENSHARE -->
                        <li data-toggle="tooltip" id="tourtutor5" data-placement="bottom" title="Open Screen Share" style="cursor: pointer;">
                          <a id="openscreenshare" target="_blank"><i class="tm-icon zmdi zmdi-window-maximize"></i></a>
                        </li>                    
                        <!-- END SCREEN SET -->
                       <!--  <li data-toggle="tooltip" id="tourtutor6" data-placement="bottom" title="Chat" style="cursor: pointer;">
                          <a id="chat-trigger" data-trigger="#chat"><i class="tm-icon zmdi zmdi-comment-alt-text" id="klikchat"></i></a>
                        </li> -->
                        <li data-toggle="tooltip" id="tourtutor7" data-placement="bottom" title="Settings" style="cursor: pointer;">
                          <a id="devicedetect"><i class="tm-icon zmdi zmdi-settings"></i></a>
                        </li>
                        <!-- <li data-toggle="tooltip" id="tourtutor8" data-placement="bottom" title="Tour" style="cursor: pointer;" >
                            <a id="tourlagii"><i class="tm-icon zmdi zmdi-navigation"></i></a>               
                        </li> -->
                        <li data-toggle="tooltip" id="tourtutor9" data-placement="bottom" title="Logout" style="cursor: pointer;">
                            <a id="keluarcobaa"><i class="tm-icon zmdi zmdi-power-setting"></i></a>               
                        </li>

                    </ul>
                </div>

            </li>
        </ul>
        <div style="width: 125%; margin-left: -10%;">
            <div class="col-md-12" style="text-align: left; background-color: #03a2ea;">
                <nav class="ha-menu">
                    <ul>
                        <li class="pull-left c-white" style="margin-left: 8%;"><label class="c-white">Tutor : <label id="nametutor"></label></label></li>
                        <li style="margin-left: 23%;"><label class="c-white"><label id="classname"></label></label></li>
                        <li class="pull-right" style="margin-right: 12%;"><label class="c-white"><label id="datenow"></label></label></li>
                    </ul>
                </nav>
            </div>                    
        </div>
    </div>   
     
</header>

<section id="main" class="tampilanweb" style="overflow: hidden; height: 100%;">

    <!-- <aside id="chat" class="sidebar c-overflow" style="margin-top: 3.7%; background-color:blue;"> 
        
        <div class="col-md-12" style="background-color: black; padding: 0px;">
            <label id="totalstudent" class="p-5 m-5 col-md-12" style="background-color: purple; padding: 0px;">
                
            </label>
            <hr>
            <div id="" style="height: 370px; overflow-y: auto; word-wrap: break-word; background-color: teal; padding: 0px;">                
            </div>

            <hr>

            <div class="col-md-12 chat-search" style="background-color: yellow; padding: 0px;">                
                <div class="fg-line">
                    <input type="text" class="form-control" placeholder="Type your message" autocomplete="off" id="chatInput">                
                </div>
            </div>
        </div>
        
    </aside> -->
     <!-- <aside id="chat" class="sidebar c-overflow" style="margin-top: 3.7%; overflow: hidden;"> 
        
        <div class="col-md-12" id="panel1">
            <div class="chat-search" id="panel2" style="height: 40px; margin-top: -1%;">
                <label id="totalstudent" ></label>            
            </div>
            <hr>

            <div id="chatBoxScroll" class="ba" style="overflow-y: auto; margin-top: -6%; margin-bottom: -6%;">
                
            </div>
            
            <hr>
            <div class="chat-search" id="panel3" style="height: 80px; width: 100%; padding: 0;">            
                <div class="fg-line" id="panel4">          
                    <textarea maxlength="300" id="chatInput" rows="4" style="width:100%; border: 1px #ececec solid; resize: none; padding: 0;" class="form-control btn-block" placeholder="Type your message"></textarea>                        
                </div>
            </div><br>
        </div>
        
    </aside> -->

    <section id="content">    	
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/janus.js" ></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/test.js"></script> -->
    <style type="text/css">
    #container {
        width: 100%;       
        position: relative;
        text-align:center;
    }
    #container div {
        position: relative;
        margin:3px;     
        background-color: #bfbfbf;
        border-radius:3px;
        text-align:center;
        display:inline-block;
    }
    #video{
        width: 100%;
    }

    .video2 {
        object-fit: fill;
        width: 100%;
        height: 100%;
    }
    #tempatstudent{
        width: 100%;     
    }
    #tempatstudent.toggle{
        width: 80%;     
    }
    #tempattutor{
        width: 100%;     
    }
    #tempattutor.toggle{
        width: 80%;     
    }
    .raise-hand-video-box{
      height: 360px; 
    }

</style>
    
    <div id="tempatstudent" style="margin-top:4%; display: none;">
        <section class="row" id="container2" style="margin-left: 5%; margin-right: 5%; width: 90%;">
        <!-- <div class="container"> -->            
            <!-- <div class="col-md-3" style="height: 100%; background-color: #ececec;">                    
            </div> -->
            <div class="col-md-8" id="setengah1" style="height: 480px;">
                <div class="move col-md-12" id="whboard" style="height: 480px; background-color: purple; padding: 0; margin: 0;">
                    <img id="pindah1" class="klikbego" src="<?php echo base_url();?>aset/images/Arrow.png" width="30px" height="30px" style="display:none; z-index: 1; position: absolute; margin: 2%; cursor: pointer;" data-toggle="tooltip"
                    data-placement="bottom" title="Switch Screen">
                                        
                    <video class="video2" id="myvideostudent" disabled width="100%" height="100%" autoplay />                     
                </div>
            </div>
            <div class="col-md-4" id="tempatchat" style="height: 480px; background-color: #ffffff; padding: 0; overflow:hidden; ">                    
                <?php $this->load->view('classroom_view/chat');?>
            </div>
            <!-- <div class="col-md-2"></div> -->            
            
        <!-- </div> -->
        </section> 
        <div id="berkas" class="col-md-12" style="display: none;"></div>        
    </div>

    <br>

    <div id="tempattutor" style="margin-top:4%; display: none;">
        <section class="row" id="container2" style="margin-left: 5%; margin-right: 5%; width: 90%;
        ">

            <!-- <div class="col-md-12" id="fulled" style="height: 100%; display: none;">
                
            </div> -->
            <!-- <div class="col-md-3" style="height: 450px; background-color: #ececec;">                    
            </div> -->
            <div class="col-md-8" style="height: 450px;">
                <div class="move col-md-12" id="whboardtutor" style="height: 100%; background-color: #efefef; padding: 0; margin: 0;">
                    <!-- <div class="col-md-12" style="z-index: 1px; position: absolute; padding-right: 3%;"><label class="bgm-blue f-20 pull-right " >Ini Whiteboard</label></div> -->
                    <video id="myvideo" class="video2" autoplay ></video> 
                </div>
            </div>
            <div class="col-md-4" id="tempatchat" style="height: 450px; background-color: #ffffff; max-height: 450px; overflow:hidden; padding: 0;">
                <?php $this->load->view('classroom_view/chatt');?>
            </div>
            <!-- <div class="col-md-2"></div> -->

        </div>          
        </section> 
        <script type="text/javascript">
    var a = $('#tempatchat').height();
    var b = $('#tempatchat').width(); //UNTUK HEIGHT
    // var b = $(window).height() / 0.65;
    var c = a - $('#panel2').height();
    var d = c - $('#panel3').height() - 100;
    var e = a / 0.5625; //UNTUK WIDTH LANDSCAPE
    // alert(d);
    $("#chatBoxScroll").css('height', d);
    $("#chatBoxScrolll").css('height', d);
        // $("#myvideostudent").css('width', b);       

</script>
        <div id="berkas" class="col-md-12" style="display: none;"></div>       
    </div>
    <div id="tempatwaktu" class="text-center" style="position:absolute;
    width:300px;
    height:40px;
    background:rgba(3, 169, 244, 0.6);
    padding-left: 3px;
    padding-right: 3px;
    bottom:5px;
    border-radius: 5px;
    right:25%;
    left:50%;
    margin-left: -150px;
    display: none;">
        <h5 class="c-white">Classroom will be ended in <label id="countdown"></label></h5>
    </div>

    </section>

    <div id="classkeluar" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	    <div class="modal-dialog">
	      	<div class="modal-content">
		        <div class="modal-body">
		          	<p class="f-15 pull-left m-t-20">Anda Yakin Keluar ?</p><br><br><br>
		        </div>
		        <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
	                <button type="button" class="btn btn-info" id="keluar">OK</button>
		        </div>
	      	</div>
	    </div>
	</div>

    <div id="deviceConfigurationModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h4 class="modal-title">Change Video Devices <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
	        </div>
	        <div class="modal-body">
	          <div class="row">
	            <div class="col-md-offset-2 col-md-8" id="buttonlist">
	              <!-- <button class="btn btn-default btn-block" id="iddevice">
	              	<label id="listdevice"></label>
	              </button> -->
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	</div>	

    <div id="kotakmenulist" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">List User Raise Hands <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-offset-2 col-md-8" id="kotakraisehand">
                  <!-- <button class="btn btn-default btn-block" id="iddevice">
                    <label id="listdevice"></label>
                  </button> -->
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>  

    <div id="videoCallModall" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg modal-raise-hand" style="width: 85%;">
          <div class="modal-content">
            <div class="modal-header bgm-blue">
                <h4 class="modal-title c-white">Raise Hand <label id="hangups" class="close c-white btn-small" aria-label="Close"><span aria-hidden="true" class="c-white">Hangup</span></label></h4>
            </div>
            <div class="modal-body">
            <br>
              <div class="row">
                <div class="col-md-12" id="kotakr">
                  <div class="col-md-6 raise-hand-video-box" id="kotaka">
                    <center id="videoBoxLocal" ></center>
                  </div>
                  <div class="col-md-6 raise-hand-video-box">
                    <center id="videoBoxRemote"></center>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-default" id="hangups" >Hangup</button>
            </div> -->
          </div>
        </div>
      </div>

      <div id="tidakadadata" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header bgm-orange">
                <h4 class="modal-title c-white">INFO <label id="tutupalert" class="close c-white btn-small" aria-label="Close"><span aria-hidden="true" class="c-white f-13">X</span></label></h4>
            </div>
            <div class="modal-body">
            <br>
              <div class="row">
                <center><h4>Tidak ada data</h4></center>
              </div>
            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-default" id="hangups" >Hangup</button>
            </div> -->
          </div>
        </div>
      </div>

      <div id="callingModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Calling ... <button type="button" id="hangupss" class="close" aria-label="Close"><span aria-hidden="true">X</span></button></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <b id="messageCalling"></b>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

