<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.1.5/adapter.min.js" ></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.7.2/jquery.min.js" ></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js" ></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.1.0/bootbox.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/spin.js/2.3.2/spin.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/janus.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/test.js" ></script>
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/videoroom.js"></script> -->
<style type="text/css">
html, body, main-content {
    height:100%;
    overflow:hidden;
    margin-top: 2%;
    max-height: 100%;
    background-image: url(../aset/img/headers/math.jpg);
    background-repeat: no-repeat; 
    background-position: 0 0;
    background-size: cover; 
    background-attachment: fixed;
}
#footer {
    position:fixed;
    right:0;
    bottom:0;
    margin:0;
    width:75%;
}
#footer img {width:100%;}
    /* scroller browser */
::-webkit-scrollbar {
    width: 9px;
}

/* Track */
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
    -webkit-border-radius: 7px;
    border-radius: 7px;
}

/* Handle */
::-webkit-scrollbar-thumb {
    -webkit-border-radius: 7px;
    border-radius: 7px;
    background: #a6a5a5;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
}
::-webkit-scrollbar-thumb:window-inactive {
    background: rgba(0,0,0,0.4); 
}
</style>
<header id="header" class="clearfix" data-current-skin="lightblue">    
    <div style="margin-left: 7%; margin-right: 9%;">
        <ul class="header-inner clearfix">
            <li id="menu-trigger" data-trigger=".ha-menu" class="visible-xs">
                <div class="line-wrap">
                    <div class="line top"></div>
                    <div class="line center"></div>
                    <div class="line bottom"></div>
                </div>
            </li>
            <li class="logo hidden-xs">
                <a href="#" class="m-l-20"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" style='margin-top: -7px;' alt=""></a>
            </li>
            <li class="pull-right">            

                <div id="usertypestudent" style="display: none;">
                    <ul class="top-menu">

                    <li data-toggle="tooltip" data-placement="bottom" title="Raise Hands">
                        <a data-toggle="modal" href="#videoCallModalstudent">
                            <div style="background: url('../aset/images/raisehandd.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px;">
                            </div>
                        </a>               
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-tv-list"></i></a>
                        <ul class="dropdown-menu dm-icon pull-right" style="margin: 5px;">                    
                            <li style="margin:4%;">
                                <div id="checkwhiteboard" style="border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                                    <input type="checkbox" name="klikwhiteboard" id="klikwhiteboard" value="klikwhiteboard" checked onchange="toggleCheckbox(this)"  style="display: none;">
                                    <label for="klikwhiteboard" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5 f-12" rel="tooltip" style="width: 90%; height: 30px;">Whiteboard.</label>
                                </div>
                            </li>
                            <li style="margin:4%;">
                                <div id="checkvideo" style="margin-top: -6%; border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                                    <input type="checkbox" name="klikvideo" id="klikvideo" value="klikvideo" checked onchange="toggleCheckbox(this)" style="display: none; margin-top: -3%;">
                                    <label for="klikvideo" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5" rel="tooltip" style="width: 90%; height: 30px;">Video.</label>
                                </div>
                            </li>
                            <li style="margin:4%;">
                                <div id="checkscreen" style="margin-top: -6%; border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                                    <input type="checkbox" name="klikscreen" id="klikscreen" value="klikscreen" checked onchange="toggleCheckbox(this)" style="display: none;margin-top: -3%; ">                                        
                                    <label for="klikscreen" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5" rel="tooltip" style="width: 90%; height: 30px;">Screen Share.</label></p>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <li data-toggle="tooltip" data-placement="bottom" title="Chat" >
                        <a href="" id="chat-trigger" data-trigger="#chat"><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>               
                    </li>                                
                    <li data-toggle="tooltip" data-placement="bottom" title="Settings">
                        <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-settings"></i></a>
                    </li>
                    <li data-toggle="tooltip" data-placement="bottom" title="Logout">
                        <a data-toggle="modal" href="#modalDefault"><i class="tm-icon zmdi zmdi-power-setting"></i></a>               
                    </li>
                </ul>
                </div>

                <div id="usertypetutor" style="display: none;">
                    <ul class="top-menu">

                        <li class="dropdown" id="notif" data-toggle="tooltip" data-placement="bottom" title="Raise Hands" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;" id_user="<?php echo $this->session->userdata('id_user');?>">
                            <a data-toggle="dropdown" href="">
                                <div style="background: url('../aset/images/raisehandd.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px;">
                                </div>
                                <!-- <i class='tmn-counts bara' ng-if="nes > '0'">{{nes}}</i> -->
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg pull-right" >
                                <div class="listview" id="notifications">
                                    <div class="lv-header">
                                        Raise Hand
                                    </div>                        
                                    <div class="lv-body" id="scol" style="overflow-y:auto; height: 10px;">
                                        <div class="col-md-12">

                                            <div class="card-body card-padding">
                                                <div class="col-md-4 m-t-15">
                                                    <div class="pull-left">
                                                    Ramdhani
                                                    </div>
                                                </div>
                                                <div class="col-md-8 m-t-15">
                                                    <div class="pull-right">
                                                    <a data-toggle="modal" href="#videoCallModaltutor"><img src="<?php echo base_url(); ?>aset/img/iconvideo.png"></a>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 m-t-15">
                                                    <div class="pull-left">
                                                    Fikri
                                                    </div>
                                                </div>
                                                <div class="col-md-8 m-t-15">
                                                    <div class="pull-right">
                                                    <a data-toggle="modal" href="#videoCallModaltutor"><img src="<?php echo base_url(); ?>aset/img/iconvideo.png"></a>
                                                    </div>
                                                </div>                                   
                                            </div>
                                            

                                        </div>
                                    </div>                     

                                </div>

                            </div>
                        </li>
                        <!-- START PLAY AND PAUSE -->
                        <li data-toggle="tooltip" data-placement="bottom" title="Play" style="cursor: pointer; display: none;">
                          <a href="#"><i class="tm-icon zmdi zmdi-play"></i></a>
                        </li>
                        <li data-toggle="tooltip" data-placement="bottom" title="Break" style="cursor: pointer;">
                          <a href="#"><i class="tm-icon zmdi zmdi-pause"></i></a>
                        </li>
                        <!-- END PLAY AND PAUSE -->
                        <!-- START SHOW AND OFF -->
                        <li data-toggle="tooltip" data-placement="bottom" title="Video On" style="cursor: pointer; display: none;">
                          <a href="#"><i class="tm-icon zmdi zmdi-eye-off"></i></a>
                        </li>
                        <li data-toggle="tooltip" data-placement="bottom" title="Video Off" style="cursor: pointer;">
                          <a href="#"><i class="tm-icon zmdi zmdi-eye"></i></a>
                        </li>
                        <!-- END SHOW AND OFF -->
                        <!-- START AUDIO -->
                        <li data-toggle="tooltip" data-placement="bottom" title="Audio On" style="cursor: pointer;">
                          <a href="#"><i class="tm-icon zmdi zmdi-volume-up" ></i></a>
                        </li>
                        <li data-toggle="tooltip" data-placement="bottom" title="Audio Off" style="cursor: pointer; display: none;">
                          <a href="#"><i class="tm-icon zmdi zmdi-volume-off" ></i></a>
                        </li>
                        <!-- END AUDIO -->
                        <!-- START SCREENSHARE -->
                        <li data-toggle="tooltip" data-placement="bottom" title="Open Screen Share" style="cursor: pointer;">
                          <a href="#" target="_blank"><i class="tm-icon zmdi zmdi-window-maximize"></i></a>
                        </li>
                        <!-- END SCREENSHARE -->
                        <!-- START SCREEN SET -->
                        <!-- <li class="dropdown">
                            <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-tv-list"></i></a>
                            <ul class="dropdown-menu dm-icon pull-right" style="margin: 5px;">                    
                                <li style="margin:4%;">
                                    <div id="checkwhiteboard" style="border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                                        <input type="checkbox" name="klikwhiteboardtutor" id="klikwhiteboardtutor" value="klikwhiteboardtutor" checked onchange="toggleCheckboxtutor(this)">
                                        <label for="klikwhiteboardtutor" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5 f-12" rel="tooltip" style="width: 90%; height: 30px;">Whiteboard.</label>
                                    </div>
                                </li>
                                <li style="margin:4%;">
                                    <div id="checkvideo" style="margin-top: -6%; border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                                        <input type="checkbox" name="klikvideotutor" id="klikvideotutor" value="klikvideotutor" checked onchange="toggleCheckboxtutor(this)" style="margin-top: -3%;">
                                        <label for="klikvideotutor" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5" rel="tooltip" style="width: 90%; height: 30px;">Video.</label>
                                    </div>
                                </li>                           
                            </ul>
                        </li> -->
                        <!-- END SCREEN SET -->
                        <li data-toggle="tooltip" data-placement="bottom" title="Chat" style="cursor: pointer;">
                          <a id="chat-trigger" data-trigger="#chat"><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>
                        </li>
                        <li data-toggle="tooltip" data-placement="bottom" title="Settings" style="cursor: pointer;">
                          <a ng-click="getListDevice()"><i class="tm-icon zmdi zmdi-settings"></i></a>
                        </li>
                        <li data-toggle="tooltip" data-placement="bottom" title="Logout" style="cursor: pointer;">
                          <a data-toggle="modal" href="#modalDefault"><i class="tm-icon zmdi zmdi-power-setting"></i></a>
                        </li>

                    </ul>
                </div>
            </li>
        </ul>
        <div style="width: 125%; margin-left: -10%;">
            <div class="col-md-12" style="text-align: left; background-color: #03a2ea;">
                <nav class="ha-menu">
                    <ul>
                        <li class="pull-left" style="margin-left: 8%;"><a href="#" class="c-white">Tutor : <label id="nametutor"></label></a></li>
                        <li style="margin-left: 23%;"><a href="#" class="c-white"><label id="classname"></label></a></li>
                        <li class="pull-right" style="margin-right: 12%;"><a href="#" class="c-white"><label id="datenow"></label></a></li>
                    </ul>
                </nav>
            </div>                    
        </div>
    </div>   
     
</header>

<section id="main">

    <aside id="chat" class="sidebar c-overflow" style="margin-top: 3.7%;"> 
        
        <div class="chat-search">
            <label>Enter your message</label>
            <div class="fg-line">
                <input type="text" class="form-control" placeholder="Type your message">
                <button class="btn btn-info btn-block m-t-10">Kirim</button>
            </div>
        </div>
        <hr>

        <div class="listview" id="checkklik">
            <a class="lv-item" href="">
                <div class="media">                    
                    <div class="pull-left p-relative">
                        <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/user/32.jpg" alt="">
                        <i class="chat-status-busy"></i>
                    </div>
                    <div class="media-body">
                        <div class="lv-title">Testing</div>
                        <small class="lv-small">blablabla</small>
                    </div>
                </div>
            </a>
            <a class="lv-item" href="">
                <div class="media">                    
                    <div class="pull-left p-relative">
                        <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/user/32.jpg" alt="">
                        <i class="chat-status-busy"></i>
                    </div>
                    <div class="media-body">
                        <div class="lv-title">Testing</div>
                        <small class="lv-small">blablabla</small>
                    </div>
                </div>
            </a>
            <a class="lv-item" href="">
                <div class="media">                    
                    <div class="pull-left p-relative">
                        <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/user/32.jpg" alt="">
                        <i class="chat-status-busy"></i>
                    </div>
                    <div class="media-body">
                        <div class="lv-title">Testing</div>
                        <small class="lv-small">blablabla</small>
                    </div>
                </div>
            </a>
        </div>
        
    </aside>

    <section id="content">
        
        <div style="margin-left: 7%; margin-right: 7%;">
            <div class="main-content" >

                <div class="col-md-12">
                    
                    <video id="myvideo" class="video2" src="<?php echo base_url(); ?>aset/video/juki.mp4" autoplay ></video>

                </div>
                
            </div>
        </div>

    </section>

    <!-- Modal Default -->  
    <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">   
                <div class="modal-body">
                    <p class="f-15 pull-left m-t-20">Anda Yakin Keluar ?</p><br>
                </div>             
                <div class="modal-footer">                    
                    <hr>
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    <button type="button" class="btn btn-info" onclick="destroyToken();">OK</button>
                </div>
            </div>
        </div>
    </div>

    <div id="videoCallModalstudent" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg modal-raise-hand" style="width: 65%;">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Raise Hand</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
              <div class="col-md-4" style="height: 360px; width: 40%; margin-right: : 10%;">
                <center id="videoBoxLocal">
                  <video width="360" height="360" id="videoCallLocal" muted autoplay src="<?php echo base_url(); ?>aset/video/juki.mp4"></video>
                </center>
              </div>
              <div class="col-md-4" style="height: 360px; width: 40%; margin-left: 10%;">
                <center id="videoBoxRemote">
                  <!--<video width="480" height="360" ng-src="{{videoCall.remoteStream | trustUrl}}" autoplay></video>-->
                  <video width="360" height="360" id="videoCallRemote" muted autoplay src="<?php echo base_url(); ?>aset/video/juki.mp4"></video>
                </center>
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">          
            <button type="button" class="btn btn-default"  data-dismiss="modal">Hangup</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div id="videoCallModaltutor" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg modal-raise-hand" style="width: 65%;">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Raise Hand</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
              <div class="col-md-4" style="height: 360px; width: 40%; margin-right: : 10%;">
                <center id="videoBoxLocal">
                  <video width="360" height="360" id="videoCallLocal" muted autoplay src="<?php echo base_url(); ?>aset/video/juki.mp4"></video>
                </center>
              </div>
              <div class="col-md-4" style="height: 360px; width: 40%; margin-left: 10%;">
                <center id="videoBoxRemote">
                  <!--<video width="480" height="360" ng-src="{{videoCall.remoteStream | trustUrl}}" autoplay></video>-->
                  <video width="360" height="360" id="videoCallRemote" muted autoplay src="<?php echo base_url(); ?>aset/video/juki.mp4"></video>
                </center>
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hangup</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    


</section>
