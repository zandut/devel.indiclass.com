<style type="text/css">
    #container {
        width: 100%;       
        position: relative;
        text-align:center;
    }
    #container div {
        position: relative;
        margin:3px;     
        background-color: #bfbfbf;
        border-radius:3px;
        text-align:center;
        display:inline-block;
    }

    #whiteboard{    
        width: 50%;
        background-color: blue;
        height: 70vh;
        position: static;
        float: left;
        color: white;
        font-size: 30px;

        -webkit-animation-duration: 10s;
        animation-duration: 10s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    } 
    #video{
        width: 30%;
        background-color: teal;
        height: 35vh;
        float: left;
        color: white;
        font-size: 30px;

        -webkit-animation-duration: 10s;
        animation-duration: 10s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    } 
    #screen{
        width: 30%;
        background-color: purple;
        height: 35vh;
        float: left;
        color: white;
        font-size: 30px;

        -webkit-animation-duration: 10s;
        animation-duration: 10s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    }
    
    #one.gedee{
        cursor: pointer;
    }
    #one{
        cursor: default;
    }
    #three.gedee{
        cursor: default;
        height: 35vh;
    }
    #three{
        cursor: pointer;
        height: 70.7vh;
    }
    .video2 {
        object-fit: fill;
        width: 100%;
        height: 35vh;
        margin: 0px;
        padding: 0px;
        cursor: pointer;
    }
    .video2.gede {
        object-fit: fill;
        width: 100%;
        height: 70.8vh;
        margin:0px;
        padding: 0px;
        cursor: default;
    }
    .video2.gedek {
        object-fit: fill;
        width: 100%;
        height: 35vh;
        margin: 0px;
        padding: 0px;
        cursor: pointer;
    }
    .video2.gedemenu2 {
        object-fit: fill;
        width: 100%;
        height: 70.7vh;
        margin: 0px;
        padding: 0px;
        cursor: pointer;
    }
    .menu{
        margin: 5%;
    }
</style>
    
    

    <div id="container">
        <div id="one" class="move" style="height:70.7vh; width:60%; float:left; background-color: purple;"><div id="whiteboard2">INI WHITEBOARD</div></div>
        <div id="two" class="move" style="height:35vh; width:36%; float:left;"><video id="video2" class="video2" src="<?php echo base_url(); ?>aset/video/bigbunny.mp4" autoplay muted loop></video></div>
        <div id="three" class="move" style="height:35vh; width:36%; float:left; background-color: teal;"><div id="screenn">INI SCREEN SHARE</div></div>
    </div>   

    <!-- INI WHITEBOARD DAN VIDEO -->
    <!-- <div id="container">
        <div id="one" class="move" style="height:70.7vh; width:49%; float:left; background-color: purple;"><div id="whiteboard2">INI WHITEBOARD</div></div>
        <div id="two" class="move" style="height:70.7vh; width:49%; float:left;"><video id="video2" class="videomenu1" src="<?php echo base_url(); ?>aset/video/bigbunny.mp4" autoplay muted loop></video></div>        
    </div>  --> 


    <br>
    
<script type="text/javascript">
    
    $("video").bind("contextmenu",function(){
            return false;
        });
    $('#container').on('click', '.move', function () {
          
        $("#video2").css("width","100%");
        var clickedDiv = $(this).closest('div'),
            prevDiv = $("#container > :first-child"),
            distance = clickedDiv.offset().right - prevDiv.offset().right;
        if (klikwhiteboard.checked && klikvideo.checked && klikscreen.checked) {
            if (!clickedDiv.is(":first-child")) {
                prevDiv.css('left', '0px');
                prevDiv.css('width', '36%');
                prevDiv.css('height', '35vh');
                clickedDiv.css('left', '0px');
                clickedDiv.css('width', '60%');
                clickedDiv.css('height', '70.7vh');
                prevDiv.insertBefore(clickedDiv);
                clickedDiv.prependTo("#container");
                $('#video2').get(0).play();                 
                var contentPanelId = clickedDiv.attr("id");        
                var contentPanelprev = prevDiv.attr("id");
                if (contentPanelId == "two" || contentPanelprev == "two") {
                    $("#video2").toggleClass("gede");
                    var x = $("#video2").offset();
                    var y = $("#screenn").offset();
                    var z = $("#whiteboard2").offset();
                    // alert(x.left);
                    // alert(y.left);
                    // alert(z.left);
                    if (x.left < y.left) {
                        $("#video2").css("height","70.7vh");
                    }
                    else if (x.left > y.left)
                    {
                        $("#video2").css("height","35vh");
                    }
                    else if (x.left > z.left)
                    {
                        $("#video2").css("height","35vh");
                    }            
                }
                if (contentPanelId == "three" || contentPanelprev == "three") {
                    $("#three").toggleClass("gedee");
                    var x = $("#video2").offset();
                    var y = $("#screenn").offset();
                    var z = $("#whiteboard2").offset();
                    if (x.left > y.left)
                    {
                        $("#video2").css("height","35vh");
                    }                
                }
                if (contentPanelId == "one" || contentPanelprev == "one") {
                    $("#one").toggleClass("gedee");
                    var x = $("#video2").offset();
                    var y = $("#screenn").offset();
                    var z = $("#whiteboard2").offset();
                    if (x.left > z.left)
                    {
                        $("#video2").css("height","35vh");
                    }
                }
            }     
        }       

    });

    function toggleCheckbox(element)
    {
        if(klikwhiteboard.checked && klikvideo.checked && klikscreen.checked) {
            var a = "whiteboard";
            var b = "video";
            var c = "screen";
            $("#one").css("display","block");
            $("#two").css("display","block");
            $("#three").css("display","block");
            $("#one").css("width","60%" );
            $("#two").css("width","36%" );
            $("#three").css("width","36%");
            $("#two").css("height","35vh");
            $("#three").css("height","35vh");
            // $("#video2").toggleClass("gede"); 
            
            
            var x = $("#video2").offset();
            var y = $("#screenn").offset();
            var z = $("#whiteboard2").offset();
            // alert(x.left);
            // alert(y.left);
            // alert(z.left);
            if (x.left < y.left) {
                $("#video2").css("height","35vh");
            }
            else if (x.left > y.left)
            {
                $("#video2").css("height","70vh");
            }
            else if (x.left > z.left)
            {
                $("#video2").css("height","70vh");
            }

        }
        else if (klikwhiteboard.checked && klikvideo.checked) {
            $("#three").css("display","none");
            $("#one").css("display","block");
            $("#two").css("display","block");
            $("#two").css("height","70.7vh");
            $("#two").css("width","49%");
            $("#one").css("height","70.7vh");
            $("#one").css("width","49%");
            // $("#video2").toggleClass("gede");
            $("#video2").css("height","70.7vh");
        }  
        else if (klikwhiteboard.checked && klikscreen.checked) {
            $("#two").css("display","none");
            $("#one").css("display","block");
            $("#three").css("display","block");
            $("#three").css("height","70.7vh");
            $("#three").css("width","49%");
            $("#one").css("height","70.7vh");
            $("#one").css("width","49%"); 
            $("#video2").toggleClass("gede");                           
        }
        else if (klikvideo.checked && klikscreen.checked) {
            $("#one").css("display","none");
            $("#two").css("display","block");
            $("#three").css("display","block");
            $("#three").css("height","70.7vh");
            $("#three").css("width","49%");
            $("#two").css("height","70.7vh");
            $("#two").css("width","49%");
            $("#video2").toggleClass("gede"); 
            $("#video2").css("height","70.7vh");                   
        } 
        else if (klikwhiteboard.checked) {
            $("#two").css("display","none");
            $("#three").css("display","none");
            $("#one").css("display","block");
            $("#one").css("height","70.7vh");
            $("#one").css("width","98%"); 
            $("#video2").toggleClass("gede");                                       
        } 
        else if (klikvideo.checked) {
            $("#one").css("display","none");
            $("#three").css("display","none");
            $("#two").css("display","block");
            $("#two").css("height","70.7vh");
            $("#two").css("width","98%");           
            $("#video2").css("height","70.7vh");                    
        }
        else if (klikscreen.checked) {
            $("#one").css("display","none");
            $("#two").css("display","none");
            $("#three").css("display","block");
            $("#three").css("height","70.7vh");
            $("#three").css("width","98%");
            $("#video2").toggleClass("gede");
        }        
    }   

    $("#test-with-checked").on("click", function(){ 
        
        var x = $("#video2").offset();
        alert("Top: " + x.top + " Left: " + x.left);     
        // if(klikwhiteboard.checked && klikvideo.checked && klikscreen.checked) {
        //     var a = "whiteboard";
        //     var b = "video";
        //     var c = "screen";
        //     $("#one").css("display","block");
        //     $("#two").css("display","block");
        //     $("#three").css("display","block");
        //     $("#one").css("width","60%" );
        //     $("#two").css("width","36%" );
        //     $("#three").css("width","36%");
        //     $("#two").css("height","35vh");
        //     $("#three").css("height","35vh");
        //     $("#video2").toggleClass("gede");            

        // }
        // else if (klikwhiteboard.checked && klikvideo.checked) {
        //     $("#three").css("display","none");
        //     $("#two").css("height","70.7vh");
        //     $("#two").css("width","49%");
        //     $("#one").css("height","70.7vh");
        //     $("#one").css("width","49%");
        //     $("#video2").toggleClass("gede");            
                        
        // }
        // else if (klikwhiteboard.checked && klikscreen.checked) {
        //     var a = "whiteboard";
        //     var b = "screen";
        // }  
        // else if (klikvideo.checked && klikscreen.checked) {
        //     var a = "video";
        //     var b = "screen";
        // } 
        // else if (klikwhiteboard.checked) {
        //     var a = "whiteboard";
        //     alert(a);
        // }
        // else if (klikvideo.checked) {
        //     var a = "video";
        //     alert(a);
        // }
        // else if (klikscreen.checked) {
        //     var a = "screen";
        //     alert(a);
        // }
        // else {
        //     alert("Checkbox is unchecked.");
        // }

    });     

        // $('#container').on('click', '.move', function () {
        //     if(!$("#klikwhiteboard").is(':checked') && !$("#klikvideo").is(':checked') && !$("#klikscreen").is(':checked'))
        //     {   
        //         $("#video2").css("width","100%");
        //         var clickedDiv = $(this).closest('div'),
        //             prevDiv = $("#container > :first-child"),
        //             distance = clickedDiv.offset().right - prevDiv.offset().right;
        //         if (klikwhiteboard.checked && klikvideo.checked && klikscreen.checked) {
        //             if (!clickedDiv.is(":first-child")) {
        //                 prevDiv.css('left', '0px');
        //                 prevDiv.css('width', '36%');
        //                 prevDiv.css('height', '35vh');
        //                 clickedDiv.css('left', '0px');
        //                 clickedDiv.css('width', '60%');
        //                 clickedDiv.css('height', '70.7vh');
        //                 prevDiv.insertBefore(clickedDiv);
        //                 clickedDiv.prependTo("#container");
        //                 $('#video2').get(0).play();                 
        //                 var contentPanelId = clickedDiv.attr("id");        
        //                 var contentPanelprev = prevDiv.attr("id");            
        //                 if (contentPanelId == "two" || contentPanelprev == "two") {
        //                     $("#video2").toggleClass("gede");            
        //                 }
        //                 if (contentPanelId == "three" || contentPanelprev == "three") {
        //                     $("#three").toggleClass("gedee");                
        //                 }
        //                 if (contentPanelId == "one" || contentPanelprev == "one") {
        //                     $("#one").toggleClass("gedee");                
        //                 }
        //             }
        //         }
        //     }
        //     if($("#klikwhiteboard").is(':checked') && $("#klikvideo").is(':checked') && $("#klikscreen").is(':checked'))
        //     {
        //         $("#video2").css("width","100%");
        //         var clickedDiv = $(this).closest('div'),
        //             prevDiv = $("#container > :first-child"),
        //             distance = clickedDiv.offset().right - prevDiv.offset().right;
                
        //         if (!clickedDiv.is(":first-child")) {
        //             prevDiv.css('left', '0px');
        //             prevDiv.css('width', '36%');
        //             prevDiv.css('height', '35vh');
        //             clickedDiv.css('left', '0px');
        //             clickedDiv.css('width', '60%');
        //             clickedDiv.css('height', '70.7vh');
        //             prevDiv.insertBefore(clickedDiv);
        //             clickedDiv.prependTo("#container");
        //             $('#video2').get(0).play();                 
        //             var contentPanelId = clickedDiv.attr("id");        
        //             var contentPanelprev = prevDiv.attr("id");            
        //             if (contentPanelId == "two" || contentPanelprev == "two") {
        //                 $("#video2").toggleClass("gede");            
        //             }
        //             if (contentPanelId == "three" || contentPanelprev == "three") {
        //                 $("#three").toggleClass("gedee");                
        //             }
        //             if (contentPanelId == "one" || contentPanelprev == "one") {
        //                 $("#one").toggleClass("gedee");                
        //             }
        //         }
        //     }

        // });

    // $('#container').on('click', '.move', function () {
        
    //     var clickedDiv = $(this).closest('div'),
    //         prevDiv = $("#container > :first-child"),
    //         distance = clickedDiv.offset().right - prevDiv.offset().right;
        
    //     if (!clickedDiv.is(":first-child")) {
    //         prevDiv.css('left', '0px');
    //         prevDiv.css('width', '49%');
    //         prevDiv.css('height', '70.7vh');
    //         clickedDiv.css('left', '0px');
    //         clickedDiv.css('width', '49%');
    //         clickedDiv.css('height', '70.7vh');
    //         prevDiv.insertBefore(clickedDiv);
    //         clickedDiv.prependTo("#container");
    //         $('#video2').get(0).play();                 
    //         var contentPanelId = clickedDiv.attr("id");        
    //         var contentPanelprev = prevDiv.attr("id");            
    //         if (contentPanelId == "two" || contentPanelprev == "two") {
    //             $("#video2").toggleClass("gede");            
    //         }
    //         if (contentPanelId == "one" || contentPanelprev == "one") {
    //             $("#one").toggleClass("gedee");                
    //         }
    //     }
    // });

</script>