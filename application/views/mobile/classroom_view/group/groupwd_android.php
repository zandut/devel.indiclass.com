<style type="text/css">
    html, body, main-content {
        height:100%;
        overflow:hidden;
        max-height: 100%;
        /*background-image: url(../aset/img/headers/BGclassroom-01.png);*/
        background-color: #0FA7A7;
        background-repeat: no-repeat; 
        background-position: 0 0;
        background-size: cover; 
        background-attachment: fixed;
    }
    .video2 {
        object-fit: fill;
        padding-right: 1%;
        width: 100%;
        height: 100%;        
    }
    #menu1{
        bottom: 0px;
        position: relative;
    }
    .outer-container {
        width: 100%;
        height: 100%;
        text-align: center;
    }
    .inner-container {
        display: inline-block;
        position: relative;
        width: 100%;        
        /*background-color: #ececec; */     
    }
    .papanwhiteboard {
        display: inline-block;
        position: relative;
        width: 100%;
        background-color: #ececec;  
        height: 60vh;      
    }
    .papanscreen {
        display: none;
        position: relative;
        width: 100%;
        background-color: #ececec; 
        height: 60vh;       
    }
    .video-overlay {
        position: absolute;
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 90%;
        height: 46vh;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.3);*/
    }
    .chating {
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        margin-bottom: 3%;
        font-size: 16px;
        font-family: Helvetica;
        color: #FFF;
        width: 100%;
        float: left;
        text-align: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    .toggle-overlay {
        position: absolute;
        right: 0;
        bottom: 0;        
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 20%;
        height: 25vh;
        float: right;
        /*background-color: rgba(50, 50, 50, 0.3);*/
    }
    .toggle-overlay1 {
        right: 0;
        bottom: 0;
        margin: 0px;
        margin-left: 2%;
        margin-bottom: 2%;      
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 95%;
        float: right;
        /*background-color: rgba(50, 50, 50, 0.4);*/
    }
    .video-overlay1 {
        left: 0px;
        bottom: 5px;
        margin: 0px;
        margin-left: 2%;
        margin-bottom: 2%;      
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 95%;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.4);*/
    }
    .video-overlay2 {
        right: 0px;
        bottom: 0px;
        margin: 0px;
        padding: 5px 5px;
        margin-left: 3%;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 30%;
        float: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    #remotevideo {
        width: 100%;
        height: 40vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }    
    #content {
        width: 100%;
        height: 100vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #content.toggle {
        width: 100%;
        height: 50vh;
        padding: 0; margin: 0;      
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #papanchat{
        display: block;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #papanchat.toggle{
        display: none;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #papankirivideo{
        height: 100vh; 
        overflow-y: auto; 
        padding: 0; 
        display: block;
    }
    #papankirivideo.toggle{
        height: 100vh; 
        overflow-y: auto; 
        padding: 0; 
        display: none;
    }
    @media all and (orientation:portrait) {
        #content {
            width: 100%;
            height: 60vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #remotevideo {
            width: 100%;
            height: 40vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        .papanwhiteboard {
            display: inline-block;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 60vh;            
        }
        .papanscreen {
            display: none;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 60vh;            
        }
    }

    @media all and (orientation:landscape) {
        #content {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #remotevideo {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
    }
</style>

<section id="content" class="tampilanandro">
    
    <div id="loader">
        <div class="page-loader" style="background-color:#1274b3;">
            <div class="preloader pl-xl pls-white">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>
                <p>Class Miles . . .</p>
            </div>
        </div>
    </div>

    <input type="text" name="orientation" id="orientation" hidden>
    <div id="potrait_design">
        <!-- NAVBAR MENU  -->
        <div class="" style="position: absolute; width: 100%; z-index: 1;">
            <div class="pull-left p-5 p-t-10">          
                <img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
            </div>
        </div>

        <div class="outer-container" style="z-index: 2;">
            <div class="inner-container" id="vdio">                                                               
                <video class="video2" id="myvideo" style="z-index: 1; position: absolute; margin: 2%; height: 100px; width: 100px; bottom: 0; right: 0;" autoplay loop muted poster="../aset/img/banner/Whiteboard.png" data-video-aspect-ratio="16:9"></video> 
                <video class="video2" id="remotevideo" disabled width="100%" height="100%" autoplay poster="../aset/img/banner/Discussion.png" data-video-aspect-ratio="16:9"/>        
            </div>
            <div class="col-md-12" id="tempatnyastudent" style="height: 16vh; overflow-y: auto; display: none;">
                <div class="col-md-3" style="height: 16vh; margin-top: -2%; display: none; float: left; padding: 0;" id="tempatstudent_private">        
                </div>
            </div>
            <div class="video-overlay" id="papanchat" style="z-index: 10;">
                    
                <div class="video-overlay1" style="position: absolute;">
                    <div style="height:38vh; bottom: 0; overflow-y: auto; margin-bottom: 2%;" id="chatAndroid">
                    </div>
                    <div style="width: 100%;">
                        <input maxlength="300" id="chatInputAndroid" type="text" name="sent" class="input form-control c-black" style="padding: 5px;" placeholder="Type your message">
                    </div>
                </div>
            </div>
            <div class="papanwhiteboard c-black">
                <iframe id="wbcard" width="100%" height="100%" style="padding: 0px; z-index: 1;" ></iframe>
            </div>
            <div class="papanscreen c-black">            
                <iframe id="screensharestudenttt" class="screen-share-iframe" scrolling="no" frameborder="0" allowfullscreen style="width: 100%; height: 100%; max-width: 100%;" ></iframe>
            </div>
            <div class="toggle-overlay" id="papantoggle" style="z-index: 11; display: block;">
                <div class="toggle-overlay1" style="position: absolute;">
                    <div style="height:auto;bottom: 0; right: 0; overflow-y: auto;" id="toggle">
                        <div style="margin-top: 2%;">                    
                            <button id="papanscreen" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><img src="../aset/img/baru/Whiteboard.png" style="height: 25px; width: 25px; margin-top: -1%;"></button>
                        </div>
                        <div style="margin-top: 2%;">                    
                            <button id="papanwhiteboard" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><i class="zmdi zmdi-window-maximize"></i></button>
                        </div>
                        <div style="margin-top: 2%;">
                            <button id="showchat"  class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10">
                              <img src="<?php echo base_url(); ?>aset/img/icons/comment.png" height="20px" width="20px">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="landscape_design" style="display: none;">

      <!-- NAVBAR MENU  -->
      <div class="" style="position: absolute; width: 100%; z-index: 1;">
          <div class="pull-left p-5 p-t-10">          
              <img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
          </div>
          <div class="pull-right p-15">          
                <div class="m-l-5" style="float: right;">              
                    <button id="papanhidevideokiri" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><i class="zmdi zmdi-account"></i></button>      
                    <button id="papanscreenn" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><img src="../aset/img/baru/Whiteboard.png" style="height: 25px; width: 25px; margin-top: -3%;"></button>
                    <button id="papanwhiteboardd" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10"><i class="zmdi zmdi-window-maximize"></i></button>
                    <button id="showchatt" class="btn btn-info btn-xl btn-icon waves-effect waves-circle waves-float m-b-10">
                        <img src="<?php echo base_url(); ?>aset/img/icons/comment.png" height="20px" width="20px">
                    </button>
                </div>
          </div>
      </div>

      <div class="outer-container" style="z-index: 2;">
          <div style="position: relative; display: inline-block; width: 100%; height: 100vh;" id="vdio"> 
                      
                <!-- <div class="col-md-3 col-xs-3" id="papankirivideo" style="background-color: rgba(0, 0, 0, 0.3); width: 15%; padding: 0;"> 
                    <div id="tempatnyanih" class="col-md-3" style="width: 100%; overflow-y: auto; margin-left: -15px; margin-top: 55px;">
                     
                    </div>
                </div> -->
                <div class="col-md-3 col-xs-3" id="papankirivideo" style="background-color: rgba(0, 0, 0, 0.3); width: 15%; padding: 0;"> 

                    <div id="tempatnyanih" class="col-md-3 col-sm-3" style="display: inline-block; padding:0; margin: 0; margin-top: 60px;">   

                    </div>

                </div>
                <div class="col-md-8 col-xs-8" id="papantengahvideo" style="float: left; height: 100vh; padding: 0; display: block; width: 85%;">
                    <div class="col-md-6" style="width: 50%; float: left; padding: 0; background-color: rgba(0, 0, 0, 0.3);">
                        <div class="papanwhiteee c-black" style="padding: 0;">   
                            <img class="klikwb" src="<?php echo base_url();?>aset/img/icons/Arrow 4-01.png" width="30px" height="30px" style="z-index: 1; position: absolute; cursor: pointer; left:0; top:0;" ></img>      
                            <div style="height: 431px;" id="kotakwhitee">
                            </div>
                        </div>
                        <div class="papanscreenn c-black" style="padding: 0;">         
                            <div style="height: 431px;" id="kotakscreenn">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"  style="width: 50%; float: left; padding: 0; background-color: rgba(0, 0, 0, 0.3);">
                        <div class="papanvideotutor c-black">         
                            <div style="height: 431px;" id="kotakvideotutor">                        
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tempatplayerr" style="position: absolute; z-index: 11; bottom: 0; right: 0; height: 100px; width: 100px; margin-bottom: 1.5%; margin-right: 1.5%;">

                </div>        
                <div class="video-overlay" id="papanchatt" style="z-index: 10; display: block;">                
                  
                </div> 
          </div>

      </div>
    </div>

</section>

<script type="text/javascript">
    
    // $("#papanshow2").css('display','none');
    $("#showpapan").click(function(){
        $("#content").toggleClass('toggle');
    });

    // $("#showchat").click(function(){
    //     $("#papanchat").toggleClass('toggle');
    // });

    // $("#player").click(function(){
    //     $("#papanchat").toggleClass('toggle');
    // });

    $("#tutupalert").click(function(){
        $("#tidakadadata").modal("hide");
    });

    $("#papanwhiteboard").click(function(){
        $(".papanwhiteboard").css('display','none');
        $(".papanscreen").css('display','inline-block');
    });

    $("#papanscreen").click(function(){
        $(".papanscreen").css('display','none');
        $(".papanwhiteboard").css('display','inline-block');
    });

    $("#papanwhiteboardd").click(function(){
        $(".papanwhiteee").css('display','none');
        $(".papanscreenn").css('display','inline-block');
    });

    $("#papanscreenn").click(function(){
        $(".papanscreenn").css('display','none');
        $(".papanwhiteee").css('display','inline-block');
    });

    $("#papanhidevideokiri").click(function(){
        $("#papankirivideo").toggleClass('toggle');
        var papankiri = $("#papankirivideo").css('display');
        if (papankiri == 'none') {            
            // $("#papantengahvideo").removeClass('col-xs-10');
            // $("#papantengahvideo").addClass('col-xs-12');
            $("#papantengahvideo").css('width','100%');
        }
        else if ($("#papankirivideo").css('display') == 'block') {
            // $("#papantengahvideo").removeClass('col-xs-12');
            // $("#papantengahvideo").addClass('col-xs-10');
            $("#papantengahvideo").css('width','85%');
        }
    });

    var mql = window.matchMedia("(orientation: portrait)");

    // If there are matches, we're in portrait
    if(mql.matches) {  
        $("#orientation").val('');
        $("#orientation").val('potrait');
        // Portrait orientation
        $("#landscape_design").css('display','none');
        $("#potrait_design").css('display','block');  
        
        $("#tempatstudent_private").appendTo("#tempatstudent_privatee");
        $("#screensharestudenttt").appendTo('.papanscreen');
        $("#wbcard").appendTo('.papanwhiteboard');

        $('#myvideo').appendTo('#vdio');
        $("#remotevideo").appendTo('#vdio');
        $(".video-overlay1").appendTo('#papanchat');
        $("#tempatstudent_private").appendTo('#tempatnyastudent');

        $('#myvideo').get(0).play();
        $('#remotevideo').get(0).play();
        $('.video2').get(0).play();
    } else {  
        // Landscape orientation
        $("#orientation").val('');
        $("#orientation").val('landscape');
        $("#potrait_design").css('display','none');
        $("#landscape_design").css('display','block');       
        
        $("#screensharestudenttt").appendTo('#kotakscreenn');
        $("#wbcard").appendTo('#kotakwhitee');
        $(".video-overlay1").appendTo('#papanchatt');
        $("#tempatstudent_private").appendTo('#tempatnyanih');

        $('#myvideo').appendTo('#tempatplayerr');   
        $("#remotevideo").appendTo('#kotakvideotutor');

        $('#myvideo').get(0).play();
        $('#remotevideo').get(0).play();
        $('.video2').get(0).play();
    }

    $(document).on("pagecreate",function(event){
        $(window).on("orientationchange",function(){
            if(window.orientation == 0)
            {
              // $("p").text("The orientation has changed to portrait!").css({"background-color":"yellow","font-size":"300%"});
              // alert('potraitt');
              $("#landscape_design").css('display','none');
              $("#potrait_design").css('display','block');

              $("#tempatstudent_private").appendTo("#tempatstudent_privatee");
              $("#screensharestudenttt").appendTo('.papanscreen');
              $("#wbcard").appendTo('.papanwhiteboard');

              $('#myvideo').appendTo('#vdio');
              $("#remotevideo").appendTo('#vdio');
              $(".video-overlay1").appendTo('#papanchat');
              $("#tempatstudent_private").appendTo('#tempatnyastudent');
               
              $('#myvideo').get(0).play();
              $('#remotevideo').get(0).play();
              $('.video2').get(0).play();
            }
            else
            {
              // $("p").text("The orientation has changed to landscape!").css({"background-color":"pink","font-size":"200%"});
              // alert('landscape');
              $("#potrait_design").css('display','none');
              $("#landscape_design").css('display','block');

              $("#screensharestudenttt").appendTo('#kotakscreenn');
              $("#wbcard").appendTo('#kotakwhitee');
              $(".video-overlay1").appendTo('#papanchatt');
              $("#tempatstudent_private").appendTo('#tempatnyanih');

              $('#myvideo').appendTo('#tempatplayerr');   
              $("#remotevideo").appendTo('#kotakvideotutor');

              $('#myvideo').get(0).play();
              $('#remotevideo').get(0).play();
              $('.video2').get(0).play();

            }
        });                   
    });
</script>



