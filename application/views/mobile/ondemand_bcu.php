<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1" style="padding: 0; margin-top: 3%;" >
    
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>
    
    <section id="content" style="padding: 0;">                        
        
        <div class="col-md-12" style="background-color: #8fc7a6; width: 100%; height: auto; margin-bottom: 2%;">
    
            <div class="mini-charts" style="margin-top: 4%;">
                <div class="row">
                    <div class="col-sm-6 col-md-2">
                        
                    </div>
                    
                    <div class="col-sm-12 col-md-4 col-xs-12 m-t-10" id="privateclick">
                        <a id="privateclassclick" style="cursor: pointer;">
                            <div class="mini-charts-item bgm-orange" id="kotaknyaprivate">
                                <div class="clearfix">
                                    <div class="chart stats-bar-2"><img src="../aset/img/class_icon/Private_revisiw.png" class="img-responsive" style="height: 65px; width: 65px; margin-left: 9px;" alt=""></div>
                                    <div class="count">
                                        <h2 class="m-l-20 m-t-10">Private Class</h2>
                                        <small class="m-l-20 m-t-5">Only you and tutor in class</small>                                            
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    
                    <div class="col-sm-12 col-md-4 col-xs-12 m-t-10">
                        <a id="groupclassclick" style="cursor: pointer;">
                        <div class="mini-charts-item bgm-orange" id="kotaknyagroup">
                            <div class="clearfix">
                                <div class="chart stats-bar-2"><img src="../aset/img/class_icon/Group_revisiw.png" class="img-responsive" style="filter: grayscale(); height: 65px; width: 65px; margin-left: 9px;" alt=""></div>
                                <div class="count">                                            
                                    <h2 class="m-l-20 m-t-10">Group Class</h2>
                                    <small class="m-l-20 m-t-5">4 students in class</small>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                    
                    <div class="col-sm-6 col-md-2">
                        
                    </div>
                    
                </div>
            </div>
            
        </div>        

        <br>
        <div class="container">

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>
            <br>

            <div class="block-header">
                <h2><?php  echo $this->lang->line('ondemand'); ?></h2>
                <!-- <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url() ?>first/"><?php  echo $this->lang->line('home'); ?></a></li>
                            <li class="active"><?php  echo $this->lang->line('ondemand'); ?></li>
                        </ol>
                    </li>
                </ul> -->
            </div><!-- akhir block header -->

            <div ng-show="loding" class="page-loader" style="background-color:black; opacity: 0.5;">
                <div class="preloader pl-lg pls-white">
                    <svg class="pl-circular" viewBox="25 25 50 50">
                        <circle class="plc-path" cx="50" cy="50" r="20" />
                    </svg>

                    <p style="margin-left: -18px;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
                </div>
            </div>

            <!-- <a data-toggle="modal" href="#modaladdfriends">
                <button>klik</button>                                                                                        
            </a> -->

            <div id="box_privateclass">
                 
                <div class="row">
                    <!-- <div class="col-md-12"> -->
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header ch-alt">
                                <h2>Cari Private Class</h2>
                            </div>

                            <div class="card-body card-padding">
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group form-group p-r-15">
                                            <span class="input-group-addon"><i class="zmdi zmdi-storage"></i></span>
                                            <select class="select2 form-control" required id="subject_id">
                                                <?php
                                                    $allsub = $this->Rumus->getSubject();
                                                    echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line('allsubject').'</option>'; 
                                                    foreach ($allsub as $row => $v) {                                            
                                                        echo '<option value="'.$v['subject_id'].'">'.$this->lang->line('subject_'.$v['subject_id']).' - '.$v['jenjang_level'].' '.$v['jenjang_name'].'</option>';
                                                    }
                                                ?>
                                            </select>
                                            <input type="text" name="subjecta" id="subjecton" hidden="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group form-group p-r-15">
                                            <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                            <div class="dtp-container fg-line m-l-5">                                                
                                                <input id="dateon" type='text' required class="form-control date-picker" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group form-group p-r-15">
                                            <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                            <div class="dtp-container fg-line">                                                
                                                <select class='select2 form-control' required id="time">
                                                    <option disabled selected><?php echo $this->lang->line('searchtime'); ?></option>
                                                    <?php                 
                                                        for ($i=0; $i < 24; $i++) { 
                                                          for($y = 0; $y<=45; $y+=15){  
                                                            echo "<option value=".sprintf('%02d',$i).":".sprintf('%02d',$y).">".sprintf('%02d',$i).":".sprintf('%02d',$y)."</option>";                                            
                                                        }
                                                    }
                                                    ?>
                                                </select>  
                                                <input type="text" name="timea" id="timeon" hidden="true">                              
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group form-group p-r-15">
                                            <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                            <div class="dtp-container fg-line">                                                
                                                <select required class="select2 form-control" required id="duration" >
                                                    <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                                    <option value="15">15 Minutes</option>
                                                    <option value="30">30 Minutes</option>
                                                    <option value="45">45 Minutes</option>
                                                    <option value="60">60 Minutes</option>
                                                </select>
                                                <input type="text" name="durationa" id="durationon" hidden="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button ng-click="getondemandprivate()" id="button_search" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                                    </div>
                                </div>

                            </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="card">

                            <div class="card-header ch-alt" ng-show="resulte">
                                <h2>Hasil Pencarian : {{hasils.length}}</h2>
                            </div>

                            <div class="card-body card-padding">
                                <div class="contacts clearfix row" ng-repeat=" x in datas | filter: { 'exact' : '1' } as hasils">
                                    <div class="col-md-3 col-sm-4 col-xs-6">
                                        <div class="c-item">
                                            <a href="" class="ci-avatar">
                                                <img ng-src="{{x.user_image}}" alt="">
                                            </a>
                    
                                            <div class="c-info">
                                                <strong>{{x.user_name}}</strong>
                                                <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                        <label ng-if="x.exact == '1'">{{times}}</label>
                                            </div>
                    
                                            <div class="c-footer">
                                                 <button ng-if="x.exact == '1'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{times}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                                        <button ng-if="x.exact == '0'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="alert alert-danger alert-dismissible text-center" role="alert" ng-if="firste">                                
                                        <label><?php echo $this->lang->line('nodata'); ?></label>
                                    </div>
                                    <div class="col-md-12" ng-repeat=" x in datas | filter: { 'exact' : '1' } as hasils">
                                        <div class="media-demo col-md-12 m-b-20">
                                            <div class="media">
                                                <div class="pull-left">                                                    
                                                    <img class="media-object img-responsive m-t-5" ng-src="{{x.user_image}}" style="height: 120px; width: 120px;" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="col-md-6">
                                                        <h2 class="media-heading f-14">{{x.user_name}}</h2>     
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                        <label ng-if="x.exact == '1'">{{times}}</label>
                                                    </div>
                                                    <div class="col-md-3">                                                        
                                                        <!-- <a data-toggle="modal" href="#modaladdfriends" class="btn btn-success waves-effect m-t-20"><i class="zmdi zmdi-check" ></i> Pilih<
                                                        /a> -->
                                                        <button ng-if="x.exact == '1'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{times}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                                        <button ng-if="x.exact == '0'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="card" ng-if="resulte">
                            <div class="card-header bgm-deeporange">
                                <h2>Recommendation</h2>                
                            </div>

                            <div class="card-body card-padding">
                                
                                <div class="contacts c-profile clearfix row">  
                                    <div class="col-md-2 col-sm-2 col-xs-2" ng-repeat=" x in datas | filter: { 'exact' : '0' }">
                                        <div class="c-item">
                                            <a href="" class="ci-avatar">                                       
                                                <img ng-src="{{x.user_image}}" style="border-radius: 50%;" alt="">
                                            </a>

                                            <div class="c-info rating-list">
                                                <strong>{{x.user_name}}</strong>
                                                <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                <label ng-if="x.exact == '1'">{{times}}</label>
                                            </div>

                                            <div class="c-footer"> 
                                                <button ng-if="x.exact == '1'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{times}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                                <button ng-if="x.exact == '0'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                                            </div>

                                            <!-- Modal Small -->    
                                            <div class="modal fade" id="modalconfrim" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Confrim</h4>
                                                            <hr>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Apakah anda yakin ingin memilih tutor ini ???</p>                                                    
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn-req-demand btn btn-success" demand-link=""><?php echo $this->lang->line('ikutidemand');?></button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('logout');?></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>                    
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div id="box_groupclass">
                <div class="row">
                    <!-- <div class="col-md-12"> -->
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header ch-alt">
                                <h2>Cari Group Class</h2>
                            </div>

                            <div class="card-body card-padding">
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group form-group p-r-15">
                                            <span class="input-group-addon"><i class="zmdi zmdi-storage"></i></span>
                                            <select class="select2 form-control" required id="subject_idg">
                                                <?php
                                                    $allsub = $this->Rumus->getSubject();
                                                    echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line('allsubject').'</option>'; 
                                                    foreach ($allsub as $row => $v) {                                            
                                                        echo '<option value="'.$v['subject_id'].'">'.$this->lang->line('subject_'.$v['subject_id']).' - '.$v['jenjang_level'].' '.$v['jenjang_name'].'</option>';
                                                    }
                                                ?>
                                            </select>
                                            <input type="text" name="subjecta" id="subjectong" hidden="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group form-group p-r-15">
                                            <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                            <div class="dtp-container fg-line m-l-5">                                                
                                                <input id="dateong" type='text' required class="form-control date-picker" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group form-group p-r-15">
                                            <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                            <div class="dtp-container fg-line">                                                
                                                <select class='select2 form-control' required id="timeg">
                                                    <option disabled selected><?php echo $this->lang->line('searchtime'); ?></option>
                                                    <?php                 
                                                        for ($i=0; $i < 24; $i++) { 
                                                          for($y = 0; $y<=45; $y+=15){  
                                                            echo "<option value=".sprintf('%02d',$i).":".sprintf('%02d',$y).">".sprintf('%02d',$i).":".sprintf('%02d',$y)."</option>";                                            
                                                        }
                                                    }
                                                    ?>
                                                </select>  
                                                <input type="text" name="timea" id="timeong" hidden="true">                              
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="durasigroup">
                                        <div class="input-group form-group p-r-15">
                                            <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                            <div class="dtp-container fg-line">                                                
                                                <select required class="select2 form-control" id="durationg" >
                                                    <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                                    <option value="15">15 Minutes</option>
                                                    <option value="30">30 Minutes</option>
                                                    <option value="45">45 Minutes</option>
                                                    <option value="60">60 Minutes</option>
                                                </select>
                                                <input type="text" name="durationa" id="durationong" hidden="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button ng-click="getondemandgroup()" id="button_search" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                                    </div>
                                </div>

                            </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="card">

                            <div class="card-header ch-alt" ng-show="resulte">
                                <h2>Hasil Pencarian : {{ghasils.length}}</h2>
                            </div>

                            <div class="card-body card-padding">
                                <div class="row">
                                    <div class="alert alert-success alert-dismissible text-center" role="alert" ng-if="firste">                                
                                        <label><?php echo $this->lang->line('nodata'); ?></label>
                                    </div>
                                    <div class="col-md-12" ng-repeat=" x in datasg | filter: { 'exact' : '1' } as ghasils">
                                        <div class="media-demo col-md-12 m-b-20">
                                            <div class="media">
                                                <div class="pull-left">                                                    
                                                    <img class="media-object img-responsive m-t-5" ng-src="{{x.user_image}}" style="height: 120px; width: 120px;" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="col-md-4" style="border-right: 1px solid #ececec;">
                                                        <p class="f-13">Nama : <br>{{x.user_name}}</p>
                                                        <p class="f-13" style="margin-top: -5%;">Waktu : <br>
                                                        <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                        <label ng-if="x.exact == '1'">{{times}}</label>                                                        
                                                        </p>
                                                        <p class="f-13" style="margin-top: -5%;">
                                                            Harga : <br>Rp. 1.500.000
                                                        </p>                                                        
                                                    </div>
                                                    <div class="col-md-5" style="border-right: 1px solid #ececec;">
                                                        <p>Metode Pembayaran</p>
                                                        <div class="radio m-b-15">
                                                            <label>
                                                                <input type="radio" name="sample" value="">
                                                                <i class="input-helper"></i>
                                                                Beban Sendiri
                                                            </label>
                                                        </div>
                                                        
                                                        <div class="radio m-b-15">
                                                            <label>
                                                                <input type="radio" name="sample" value="">
                                                                <i class="input-helper"></i>
                                                                Beban Rame-rame
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-3">                         
                                                        <a data-toggle="modal" href="#modaladdfriends">
                                                            <button ng-if="x.exact == '1'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{times}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                                                                        
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="card m-b-20" ng-if="resulte">
                            <div class="card-header bgm-deeporange">
                                <h2>Recommendation</h2>                
                            </div>

                            <div class="card-body card-padding">
                                
                                <div class="contacts c-profile clearfix row">  
                                    <div class="col-md-2 col-sm-2 col-xs-2" ng-repeat=" x in datas | filter: { 'exact' : '0' }">
                                        <div class="c-item">
                                            <a href="" class="ci-avatar">                                       
                                                <img ng-src="{{x.user_image}}" style="border-radius: 50%;" alt="">
                                            </a>

                                            <div class="c-info rating-list">
                                                <strong>{{x.user_name}}</strong>
                                                <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                <label ng-if="x.exact == '1'">{{times}}</label>
                                            </div>

                                            <div class="c-footer"> 
                                                <button ng-if="x.exact == '1'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{times}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                                <button ng-if="x.exact == '0'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                                            </div>

                                            <!-- Modal Small -->    
                                            <div class="modal fade" id="modalconfrim" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Confrim</h4>
                                                            <hr>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Apakah anda yakin ingin memilih tutor ini ???</p>                                                    
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn-req-demand btn btn-success" demand-link=""><?php echo $this->lang->line('ikutidemand');?></button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('logout');?></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>                    
                                </div>

                            </div>
                        </div><br><br><br>

                    </div>
                </div>
            </div>                 

            <!-- Modal Default -->  
            <div class="modal" id="modaladdfriends" tabindex="-1" role="dialog" style="top: 15%;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                            <h4 class="modal-title c-white">Undang Teman Grup anda</h4>
                        </div>
                        <div class="modal-body m-t-20">
                            <form id="frmdata">
                                <div class="col-md-12" style="padding: 0;">
                                    <div class="col-md-12">
                                        <label>Cari Temanmu</label><br>
                                        <input name='tags3' class="col-md-8" style="height: 50px;" id="tags3" placeholder='Cari' value=''>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-12"><br>
                                        <p class="c-black">Catatan</p>                            
                                        <ol>
                                            <li>Minimal Undangan 1 Orang</li>
                                            <li>Maksimal Undangan 3 Orang</li>
                                            <li>Pastikan anda memiliki Saldo yang cukup</li>
                                            <li>Pastikan teman anda memiliki Saldo yang cukup</li>
                                            <li>Kami akan memotong Saldo anda sementara.</li>
                                            <li>Maksimal teman anda mensetujui ajakan anda 2 Jam.</li>
                                        </ol>

                                    </div>
                                </div>
                            </form>
                        </div>
                        <hr>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-block m-t-15" id="cekpilihan">Proses</button>                            
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Small -->    
            <div class="modal" id="modalconfrim" tabindex="-1" style="top: 15%;" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <h4 class="modal-title c-white">Confrim</h4>                            
                        </div>
                        <div class="modal-body m-t-15">
                            <center>
                            <img id="image" style="border-radius: 1%; height: 50%; width: 50%;" class="m-b-5" alt=""><br>
                            <label><h3 id="namemodal"></h3></label><br>
                            <label id="timestart"></label>
                            <hr>
                            </center>
                            <label class="f-13"><?php echo $this->lang->line('checkdemand'); ?></label>
                        </div>
                        <div class="modal-footer">
                            <hr>
                            <button type="button" class="btn-req-demand btn btn-success" demand-link=""><?php echo $this->lang->line('ikutidemand');?></button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
                        </div>
                    </div>
                </div>
            </div>  
               

        </div><!-- akhir container -->            

    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript" src="<?php echo base_url();?>aset/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>aset/timepicker/jquery.timepicker.css" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/css/tagify.css" /> 
    <script type="text/javascript" src="<?php echo base_url();?>aset/js/tagify.js"></script>    
    <script type="text/javascript">
        $(document).ready(function() {
            
            var namaa = [];
            var ininama = null;
            $.ajax({
				url: 'https://classmiles.com/Rest/getnameuser',
				type: 'POST',				
				success: function(data)
				{	

					var a = JSON.stringify(data['response']);
                    var b = JSON.parse(a);

                    // console.warn(a);
                    console.warn(b.length);
                    for (i = 0; i < b.length; i++) {
                        namaa.push(b[i].user_name);
                    }
                    console.warn(namaa);
                    tagify();
				}
			});	

            function tagify()
            {
            var input3 = document.querySelector('input[name=tags3]'),
                tagify3 = new Tagify(input3, {
                    suggestionsMinChars : 2,
                    autocomplete        : true,
                    maxTags             : 3,
                    blacklist           : ["fuck", "shit", "pussy"],
                    enforeWhitelist     : true,
                    whitelist           : namaa
                })

            tagify3.on('maxTagsExceed', function(e){
                console.log(e, e.detail);
            });

            tagify3.on('blacklisted', function(e){
                console.log(e, e.detail);
            });

            tagify3.on('notWhitelisted', function(e){
                console.log(e, e.detail);
            });

            $("#cekpilihan").click(function(){
                var a = $("#tags3").val();
                alert(a);
            });
            }
            });
    </script>

    <script type="text/javascript">

    $('input.timepicker').timepicker({
              timeFormat: 'HH:mm:ss',
              minTime: '11:45:00',
              maxHour: 20,
              maxMinutes: 30,
              startTime: new Date(0,0,0,15,0,0), // 3:00:00 PM - noon
              interval: 15 // 15 minutes
    });

    $('#subject_id').change(function(e){
        $('#subjecton').val($(this).val());
    });
    $('#time').change(function(e){
        $('#timeon').val($(this).val());
    });

    $('#duration').change(function(e){
        $('#durationon').val($(this).val());
    });

    $('#subject_idg').change(function(e){
        $('#subjectong').val($(this).val());
    });
    $('#timeg').change(function(e){
        $('#timeong').val($(this).val());
    });
    $('#durationg').change(function(e){
        $('#durationong').val($(this).val());
    });

    var cekkotak = 0;
    $("#btntambah").click(function(){
        if (cekkotak == 0) 
        {            
            $("#kotakpilihanteman").append("<div class='col-md-8 m-b-25' style='padding: 0;' id='kotakpilihanteman2'><div class='col-md-12'><label for='nama'>Cari Temanmu</label><input type='text' class='form-control typeahead' id='nama' data-provide='typeahead' autocomplete='off'></div></div><div class='col-md-4'><button class='btn btn-info waves-effect btn-sm m-t-20' id='btntambah'><i class='zmdi zmdi-plus'></i></button><button class='btn btn-danger waves-effect btn-sm m-t-20' id='btnkurang'><i class='zmdi zmdi-minus'></i></button></div>");
            cekkotak+1;
        }        
    });

    /*$('.btn-confirm').click(function(e){
        alert('yes');
       
    });*/
    $('#button_search').click(function(){
        
    })
    $(document.body).on('click', '.btn-ask' ,function(e){
        var subject_id = $(this).attr('subject_id');
        var start_time = $(this).attr('start_time');
        var avtime_id = $(this).attr('avtime_id');
        var duration = $(this).attr('duration');    

        var subject_idg = $(this).attr('subject_idg');
        var start_timeg = $(this).attr('start_timeg');
        var avtime_idg = $(this).attr('avtime_idg');
        var durationg = $(this).attr('durationg'); 

        var user_name = $(this).attr('username');
        var image_user = $(this).attr('image');

        $('#namemodal').text(user_name);
        $('#timestart').text(start_time);
        $('#image').attr("src", image_user);        

        if (subject_id == '' && start_time == '' && avtime_id == '' && duration == '') {
            alert('tidak boleh kosong');
        }
        else
        {
            $('.btn-req-demand').attr('demand-link','<?php echo base_url(); ?>/process/demand_me?start_time='+start_time+"&avtime_id="+avtime_id+"&subject_id="+subject_id+"&duration="+duration);
            $('#modalconfrim').modal('show');
        }
    });
    $(document.body).on('click', '.btn-req-demand' ,function(e){
        $.get($(this).attr('demand-link'),function(data){
            location.reload();
        });
    });    

    $("#box_privateclass").css('opacity','100');
    $("#box_groupclass").css('opacity','0');    
    $("#kotaknyagroup").removeClass('bgm-orange');
    $("#kotaknyagroup").addClass('bgm-gray');    
    $("#privateclassclick").click(function(){ 
        $("#kotaknyagroup").removeClass('bgm-orange');
        $("#kotaknyagroup").addClass('bgm-gray');
        $("#kotaknyaprivate").removeClass('bgm-gray');
        $("#kotaknyaprivate").addClass('bgm-orange');

        $("#box_groupclass").css('display','none');
        $("#privateclick").css('filter','grayscale();');
        $("#groupklik").css('filter','none');
        $("#box_privateclass").css('display','block');
    });
    $("#groupclassclick").click(function(){
        $("#kotaknyaprivate").removeClass('bgm-orange');
        $("#kotaknyaprivate").addClass('bgm-gray'); 
        $("#kotaknyagroup").removeClass('bgm-gray');
        $("#kotaknyagroup").addClass('bgm-orange');
        $("#box_privateclass").css('display','none');
        $("#box_groupclass").css('opacity','100');        
        $("#durasigroup").addClass('col-md-12');
        $("#box_groupclass").css('display','block');
    });


</script>