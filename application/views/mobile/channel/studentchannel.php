<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sidechannel'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Daftar Siswa</h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul> -->
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card m-t-20 p-20" style="">
                <div class="card-header">
                    <div class="pull-left"><h2>List Siswa</h2></div>
                    <div class="pull-right"><a data-toggle="modal" href="#modaladdstudent"><button class="btn btn-success">+ Tambah Siswa</button></a></div>
                <br><br>
                <hr>

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $channel_id = $this->session->userdata('channel_id');
                                    $select_all = $this->db->query("SELECT mct.*, tu.user_name, tu.email, tu.user_callnum, tu.user_gender, tu.user_address FROM tbl_user as tu INNER JOIN master_channel_student as mct ON tu.id_user=mct.id_user WHERE mct.channel_id = '$channel_id'")->result_array();
                                    ?>
                                    <tr>
                                        <th data-column-id="no">No</th>
                                        <th data-column-id="name">Nama Tutor</th>
                                        <th data-column-id="email">Email</th>
                                        <th data-column-id="notelp">No Telp</th>
                                        <th data-column-id="jeniskelamin">Jenis Kelamin</th>
                                        <th data-column-id="alamat">Alamat</th>
                                        <th data-column-id="aksi" style="text-align: center;" >Action</th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    if(empty($select_all)){
                                        ?>
                                        <tr>
                                        <td colspan="8" style="text-align: center;">TIdak ada data</td>
                                        </tr>
                                        <?php
                                    }else
                                    {
                                    foreach ($select_all as $row => $v) {
                                        
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['user_name']; ?></td>
                                            <td><?php echo($v['email']); ?></td>
                                            <td><?php echo($v['user_callnum']); ?></td>
                                            <td><?php echo($v['user_gender']); ?></td>
                                            <td><?php echo($v['user_address']); ?></td>
                                            <td>
                                                <a data-toggle="modal" rtp="<?php echo $this->encryption->encrypt($v['id_user']); ?>" class="hapusstudent" data-target-color="green" href="#modalDelete"><button class="btn bgm-bluegray" title="Hapus Rekening"><i class="zmdi zmdi-delete"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }}
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div> 

            <!-- Modal TAMBAH -->  
            <div class="modal" id="modaladdstudent" tabindex="-1" role="dialog" style="top: 13%;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                            <h4 class="modal-title c-white">Tambah Siswa</h4>
                        </div>
                        <div class="modal-body m-t-20">
                            <form id="frmdata">
                                <div class="col-md-12" style="padding: 0;">
                                    <div class="col-md-12">
                                        <label>Cari Siswa</label><br>
                                        <!-- <input name='ms' class="col-md-8 m-t-5" style="height: 35px;" id="ms"> -->
                                        <div class="col-md-12 m-t-10">                                          
                                            <select required name="tagorang[]" id="tagorang" multiple class="select2 form-control" style="width: 100%;">                                                
                                            </select>                                           
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>  
                                    <div class="col-md-12"><br><br>
                                        <p class="c-black">Catatan</p>                            
                                        <ol>
                                            <li>Pastikan Tambah Siswa yang terdaftar di Tempat anda</li>                                            
                                        </ol>
                                        <hr>
                                    </div>                                  
                                </div>
                            </form>
                            <br>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-block m-t-15" id="savetutor">Tambah</button>                            
                        </div>
                    </div>
                </div>
            </div>                             

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Hapus Akun Siswa</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>
                            <input hidden type="text" name="id_rekeningg" id="id_rekeningg" class="c-black" value=""/>
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="hapusdatauser" rtp="">Ya</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </section>

</section>
<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){

    	 $('select.select2').each(function(){
                
            if($(this).attr('id') == 'tagorang'){
                $(this).select2({
                    delay: 2000,                    
                    maximumSelectionLength: 1,                  
                    ajax: {
                        dataType: 'json',
                        type: 'GET',
                        url: '<?php echo base_url(); ?>ajaxer/getStudentName',
                        data: function (params) {
                            return {
                              term: params.term,
                              page: params.page || 1
                            };
                        },
                        processResults: function(data){
                            return {
                                results: data.results,
                                pagination: {
                                    more: data.more
                                }                       
                            };
                        }                   
                    }
                });  
            }
                              
            
        });


		$('#savetutor').click(function(e){
            
            var idtage = $("#tagorang").val()+ '';
            // alert(idtage);
            var channelid = "<?php echo $this->session->userdata('channel_id'); ?>";
            // alert(channelid);
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/addstudentchannel",
                type:"post",
                data: {
                    channel_id: channelid,
                    id_user: idtage
                },
                success: function(html){             
                    // alert("Berhasil Menambah Siswa");
                    $('#modalTambah').modal('hide');
                    location.reload();                 
                } 
            });
         });


        $('#hapusdatauser').click(function(e){
            var rtp = $(this).attr('rtp');
            var channelid = "<?php echo $this->session->userdata('channel_id'); ?>";
            // alert(id_user);
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deletestudentchannel",
                type:"post",
                data: {
                	rtp: rtp,
                    channel_id: channelid
                },
                success: function(html){             
                    // alert("Berhasil menghapus data siswa");
                    $('#modalDelete').modal('hide');
                    location.reload();                     
                } 
            });
        });
    });
    $(document).on("click", ".hapusstudent", function () {
         var myBookId = $(this).attr('rtp');
         $('#hapusdatauser').attr('rtp',myBookId);

    }); 

    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();   
</script>
