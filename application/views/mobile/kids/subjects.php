<style type="text/css">
    .bp-header:hover
    {
        cursor: pointer;
    }
    .ci-avatar img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .ci-avatar:hover img {
        opacity: 0.6;
    }
</style>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); 
            $id_user = $this->session->userdata('id_user');
        ?>
    </aside>
    <section id="content">

        <div class="container">

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="block-header">
                <h2><?php echo $this->lang->line('title_pg_subject'); ?></h2>
            </div>
            
            <?php            
            if(empty($allsub))
            {
                echo '<div class="alert alert-info">'.$this->lang->line('no_subjects').'</div>';
            }
            else
            {
                foreach ($allsub as $row => $v) {
                    $alltutor = $this->db->query("SELECT ts.usertype_id, ts.user_name, ts.first_name, ts.user_image, tb.* FROM tbl_booking as tb INNER JOIN tbl_user as ts ON tb.id_user=ts.id_user WHERE ts.usertype_id='tutor' AND tb.subject_id='$v[subject_id]'")->result_array();
                    if (empty($alltutor)) {
                    	?>
                    	<div class="card">
	                        <div class="lv-header-alt clearfix m-b-5">
	                            <h2 class="lvh-label f-16"><?php echo $v['subject_name'];?></h2>	                           
	                            <div class="lvh-search">
	                                <input type="text" placeholder="Search typing..." class="lvhs-input">
	                                
	                                <i class="lvh-search-close">&times;</i>
	                            </div>
	                        </div>                        
	                        
	                        <div class="card-body card-padding">
	                        	<center>
                    				<div class=""><?php echo $this->lang->line('no_tutor_here').$v['subject_name']; ?></div>
                    			</center>
                    		</div>
                    	</div>
                    	<?php
                    }
                    else
                    {
                    	?>
	                    <div class="card">
	                        <div class="lv-header-alt clearfix m-b-5">

	                            <h2 class="lvh-label f-16">
	                            <img class="m-r-5" src="../aset/img/class_icon/<?php echo $v['icon'];?>" style="height: 25px; width: 25px;">
	                            <?php 
	                            $bahasa =  $this->session->userdata('lang');
	                            if ($bahasa=="english") {echo $v['english'];}else{echo $v['indonesia'];}?></h2>
	                            
	                            <!-- <ul class="lv-actions actions">
	                                <li>
	                                    <a href="" class="lvh-search-trigger">
	                                        <i class="zmdi zmdi-search"></i>
	                                    </a>
	                                </li>
	                                <li class="dropdown">
	                                    <a href="" data-toggle="dropdown"="" aria-expanded="false" aria-haspopup="true">
	                                        <i class="zmdi zmdi-more-vert"></i>
	                                    </a>
	                
	                                    <ul class="dropdown-menu dropdown-menu-right">
	                                        <li>
	                                            <a href="">Refresh</a>
	                                        </li>
	                                    </ul>
	                                </li>
	                            </ul> -->
	                        </div>                        
	                        
	                        <div class="card-body card-padding">
	                            
	                            <div class="contacts clearfix row">
	                            <?php
	                                foreach ($alltutor as $row_tutor => $va) 
	                                {
	                                    $tutor_id = $va['id_user'];
	                                ?>
	                                <div class="col-md-2 col-sm-4 col-xs-6">
	                                    <?php
	                                        if ($this->session->userdata('status')==0) 
	                                        {
	                                            ?>
	                                            <div class="c-item">
	                                                <a href="#" class="ci-avatar showModal" id="card_<?php echo $tutor_id; ?>">
	                                                    <img src="<?php echo base_url('aset/img/user/'.$va['user_image'].'?'.time()); ?>" style="height: 20vh; width: 100%;" alt="">
	                                                </a>
	                        
	                                                <div class="c-info">
	                                                    <strong><?php echo($va['first_name']); ?></strong>
	                                                    <!-- <small>cathy.shelton31@example.com</small> -->
	                                                </div>
	                        
	                                                <!-- <div class="c-footer">
	                                                    <button class="waves-effect"><i class="zmdi zmdi-person-add"></i> Add</button>
	                                                </div> -->
	                                            </div>
	                                        <?php
	                                        }
	                                        else
	                                        {
	                                        ?>
	                                            <div class="c-item">
	                                                <a href="#" class="ci-avatar showModal" id="card_<?php echo $tutor_id; ?>">
	                                                    <img src="<?php echo base_url('aset/img/user/'.$va['user_image'].'?'.time()); ?>" style="height: 22vh; width: 100%;" alt="">
	                                                </a>
	                        
	                                                <div class="c-info">
	                                                    <strong><?php echo($va['first_name']); ?></strong>
	                                                    <!-- <small>cathy.shelton31@example.com</small> -->
	                                                </div>
	                        
	                                                <?php
	                                                    $mybooking = $this->db->query("SELECT *FROM tbl_booking WHERE id_user='$id_user'")->result_array();
	                                                    $flag = 0;
	                                                    
	                                                    foreach ($mybooking as $rowbook => $valbook) {
	                                                        if ($tutor_id==$valbook['tutor_id'] && $va['subject_id']==$valbook['subject_id']) {
	                                                            $flag = 1;
	                                                        }
	                                                    }

	                                                    if ($flag==1) {
	                                                    ?>
	                                                        <div class="c-footer">
	                                                            <!-- <a id="buttonunfollow" class=" waves-effect bgm-red c-white btn btn-danger btn-block unfollow_ajax f-12" id="'.$v['subject_id'].'" href="<?php echo base_url('/master/unsavebooking?id_user='.$id_user.'&subject_id='.$va['subject_id'].'&tutor_id='.$va['id_user'].'&subject_name='.$v['subject_name']);?>">
	                                                                <?php echo $this->lang->line('unfollow'); ?>
	                                                            </a> -->
	                                                            <button id="buttonunfollow" subject_id='<?php echo $va['subject_id'];?>' tutorid='<?php echo $va['id_user'];?>' subject_name='<?php echo $v['subject_name'];?>' class="waves-effect bgm-red c-white btn btn-danger btn-block unfollow_ajax f-12"><?php echo $this->lang->line('unfollow'); ?></button>
	                                                        </div>
	                                                    <?php
	                                                    }
	                                                    else
	                                                    {
	                                                    ?>
	                                                        <div class="c-footer">
	                                                            <!-- <a id="buttonfollow" class="waves-effect bgm-green c-white btn btn-success btn-block follow_ajax" href="<?php echo base_url('/master/savebooking?id_user='.$id_user.'&subject_id='.$va['subject_id'].'&tutor_id='.$va['id_user'].'&subject_name='.$v['subject_name']); ?>">
	                                                            <?php echo $this->lang->line('follow'); ?>
	                                                            </a> -->
	                                                            <button id="buttonfollow" subject_id='<?php echo $va['subject_id'];?>' tutorid='<?php echo $va['id_user'];?>' subject_namee='<?php echo $v['subject_name'];?>' class="waves-effect bgm-green c-white btn btn-success btn-block follow_ajax f-12"><?php echo $this->lang->line('follow'); ?></button>
	                                                        </div>
	                                                    <?php
	                                                    }
	                                                ?>
	                                            </div>
	                                        <?php
	                                        }
	                                        ?>
	                                </div>
	                                <?php 
	                                }
	                            ?>                                
	                            </div>
	                
	                            <!-- <div class="load-more">
	                                <a href=""><i class="zmdi zmdi-refresh-alt"></i> Load More...</a>
	                            </div> -->
	                        </div>
	                    </div>
	                    <?php
                    }
                }
            }
            ?>
        </div>  

        <!-- Modal Default -->  
        <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style=" background: #f7f7f7;">
                    <div class="modal-header bgm-teal">                                                     
                        <div class="pull-left">
                            <h3 class="modal-title c-white"><?php echo $this->lang->line('profil'); ?></h3>
                            
                        </div>
                        <div class="pull-right">                                        
                            <button type="button" class="btn bgm-white" data-dismiss="modal">X</button>
                        </div>
                    </div>

                    <div class="modal-body" style="margin-top:30px;">
                        <div class="col-sm-12">

                            <div class="col-sm-4">
                                <img class="z-depth-3-bottom" id="modal_image" width="100%" height="100%" alt=""><br><br>
                                <div class="card" style="padding: 10px;">
                                    <label style="font-size:15px;"><?php echo $this->lang->line('tutorname');?></label>
                                    <hr style="margin-top: -1px;">
                                    <p style="font-size:13px; margin-top: 5px; overflow:hidden; word-wrap: break-word;" id="modal_user_name"></p>                                                                                       
                                </div>
                                <div class="card" style="padding: 10px; margin-top: -15px;">
                                    <label style="font-size:15px;"><?php echo $this->lang->line('gender');?></label>
                                    <hr style="margin-top: -1px;">
                                    <label style="font-size:13px; margin-top: -5px;"  id="modal_gender"></label>
                                </div>
                            </div>

                            <div class="col-sm-8" style="margin-top:0px;">                                              
                                <div class="col-sm-12">
                                    <div class="card" style="padding: 10px; margin-top: 10px;">
                                        <label style="font-size:16px;"><?php echo $this->lang->line('description_tutor');?></label>
                                        <hr style="margin-top: -1px;">
                                        <label id="modal_self_desc" style="width:100%; text-align:justify;">
                                        </label><br><br>
                                    </div>
                                </div>
                                <div class="col-lg-12 m-l-25">
                                    <div class="col-sm-1" style="margin-left:-20px;">
                                        <img src="<?php echo base_url('aset/img/icons/topi.png').'?'.time() ?>" width="30px" height="30px" alt="">
                                    </div>
                                    <div id="modal_eduback" class="col-sm-5 m-t-5">                                                 
                                    </div>
                                    <!-- <div class="col-sm-1" style="margin-left:-30px;">
                                        <img src="<?php echo base_url('aset/img/icons/waktu.png').'?'.time() ?>" width="27px" height="27px" alt="">
                                    </div>
                                    <div id="modal_teachexp" class="col-sm-6 m-t-5">
                                    </div> -->
                                </div>                                                                                  

                                <div class="col-lg-12 m-t-10">
                                    <div class="col-sm-12 m-t-5">
                                        <div class="table-responsive">      
                                        <center>
                                            <table style="height:10px;" class="table">
                                                <thead>
                                                    <center>    
                                                        <th class="bgm-teal c-white">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $this->lang->line('competency');?></th>
                                                    <th class="bgm-teal c-white"></th>                                                              
                                                    </center>
                                                </thead>
                                                <tbody id="modal_competency" style="height: 10px;">                                                 
                                                </tbody>
                                            </table>
                                        </center>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- AKHIR col-sm-8 -->

                        </div><!-- AKHIR col-sm-12 -->
                    </div><!-- AKHIR modal-body -->

                    <div class="modal-footer m-r-30" >
                    </div>                  
                    <br><br><br>

                </div><!-- AKHIR modal-content -->
            </div><!-- AKHIR modal-dialog -->
        </div>
        <!-- AKHIR modal fade -->

    </section>
</section>
                
<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>                

<script type="text/javascript">
    
    $(document).on('click', ".unfollow_ajax", function(e){
		e.preventDefault();
    // $(".unfollow_ajax").click(function(){    	
    	var id_user 	= "<?php echo $this->session->userdata('id_user');?>";
    	var subject_id 	= $(this).attr('subject_id');
    	var tutor_id 	= $(this).attr('tutorid');
    	var subject_name= $(this).attr('subject_name');
    	var that 		= $(this);
    	$.ajax({
			url: '<?php echo base_url();?>Master/unsavebooking',
			type: 'GET',
			data: {
				id_user: id_user,
				tutor_id: tutor_id,
				subject_id: subject_id,
				subject_name: subject_name
			},
			success: function(data)
			{ 
				data = JSON.parse(data);
				if(data['code'] == 1){
					notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block follow_ajax f-12');
					that.html('<?php echo $this->lang->line('follow'); ?>');				
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
      	});  
    });

    $(document).on('click', ".follow_ajax", function(e){
		e.preventDefault();
    // $(".follow_ajax").click(function(){
    	var id_user 	= "<?php echo $this->session->userdata('id_user');?>";
    	var subject_id 	= $(this).attr('subject_id');
    	var tutor_id 	= $(this).attr('tutorid');
    	var subject_namee= $(this).attr('subject_namee');
    	var that 		= $(this);
    	$.ajax({
			url: '<?php echo base_url();?>Master/savebooking',
			type: 'GET',
			data: {
				id_user: id_user,
				tutor_id: tutor_id,
				subject_id: subject_id,
				subject_namee: subject_namee
			},
			success: function(data)
			{ 		
				data = JSON.parse(data);		
				if(data['code'] == 1){
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-red c-white btn btn-danger btn-block unfollow_ajax f-12');
					that.html('<?php echo $this->lang->line('unfollow'); ?>');								
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
      	}); 
    });

    $('.showModal.ci-avatar').click(function(){
        var ids = $(this).attr('id');
        ids = ids.substr(5,10);
        $.get('<?php echo base_url(); ?>master/onah?id_user='+ids,function(hasil){
            hasil = JSON.parse(hasil);
            $('.modal_approve').attr('id',ids);
            $('.modal_decline').attr('id',ids);
            $('#modal_image').attr('src','<?php echo base_url(); ?>aset/img/user/'+hasil['user_image']+'?'+new Date().getTime());            
            if (hasil['self_description'] == '') {
                $('#modal_self_desc').html("<?php echo $this->lang->line('nodescription'); ?>");
            }
            else
            {
                $('#modal_self_desc').html(hasil['self_description']);
            }
            $('#modal_user_name').html(hasil['user_name']);
            $('#modal_birthplace').html(hasil['user_birthplace']);
            $('#modal_birthdate').html(hasil['user_birthdate']);
            $('#modal_age').html(hasil['user_age']);
            $('#modal_callnum').html(hasil['user_callnum']);
            // $('#modal_gender').html(hasil['user_gender']);
            // alert(hasil['user_gender']);
            if (hasil['user_gender'] == 'Male') {
                $('#modal_gender').html("<?php echo $this->lang->line('male');?>");
            }
            else if(hasil['user_gender'] == 'Female')
            {
                $('#modal_gender').html("<?php echo $this->lang->line('women');?>");
            }
            $('#modal_religion').html(hasil['user_religion']);
            $('#modal_competency').html(hasil['competency']);
            $('#modal_eduback').html("Pendidikan Terakhir "+hasil['last_education']);
            // $('#modal_teachexp').html("Pengalaman Mengajar "+hasil['year_experience'] +" Tahun" );
            if(hasil['competency'] != ''){
                for (var i = 0; i<hasil['competency'].length; i++) {
                    $('#modal_competency').append('<tr>'+
                        '<td>'+hasil['competency'][i]['subject_name']+'</td>'+
                        '<td>'+hasil['competency'][i]['jenjang_name']+'  '+hasil['competency'][i]['jenjang_level']+'</td>'+                     
                        // '<td>'+hasil['education_background'][i]['institution_address']+'</td>'+
                        // '<td>'+hasil['education_background'][i]['graduation_year']+'</td>'+
                        '</tr>');
                }
            }
            $('#modalDefault').modal('show');
        });
        $('.modal_approve').click(function(){
            var newids = $(this).attr('id');
            $.get('<?php echo base_url(); ?>first/approveTheTutor/'+newids,function(s){
                location.reload();
            });
        });
        $('.modal_decline').click(function(){
            var newids = $(this).attr('id');
            $.get('<?php echo base_url(); ?>first/declineTheTutor/'+newids,function(s){
                location.reload();
            });
        });

        $("#show1").click(function(){            
            $("#contoh2").css('display','none');
            $("#contoh1").css('display','block');
        });
        $("#show2").click(function(){            
            $("#contoh1").css('display','none');
            $("#contoh2").css('display','block');    
        });

    });

    
</script>
