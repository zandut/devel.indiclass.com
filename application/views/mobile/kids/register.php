<style type="text/css">
            html, body {
            max-width: 100%;
            overflow-x: hidden;

        }

</style>

<section style="background-color: white;">
    <header id="header" class="clearfix" data-current-skin="blue" style="padding-left: 0; padding-right: 0; background-color: #008080; ">
        <div class="card-header ch-alt " style="color: white; padding: 7%;">
            <a href="<?php echo base_url();?>About"><div style="z-index: 10; position: absolute; color: white;" id="back_menu_abouta" class="pull-left f-19"><b><i class="zmdi zmdi-arrow-left m-r-10"></i></b></div></a>
            <div align="center" style="position: relative; z-index: -10" class="col-xs-12 f-19">Daftarkan Anak anda</div>
        </div>
    </header>
    <div class=" modal fade" id="alert_modal" tabindex="-1" data-backdrop="static" role="dialog" style="color: white;">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content" id="content_modal">
                    <center>
                        <div id='' class="modal-body" style="color: white; margin-top: 75%;" ><br>
                                <label id="label_modal">Hai</label>
                        </div>
                    <div class="modal-footer" >
                        <button type="button" style="color: white;" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                    </div>
                </center>
            </div>
        </div>
    </div>
    <div class="" style="padding-top: 20%">
        <div class="" style="background: white;">                
            <div class="row p-r-10">                        
                <form method="post" action="<?php echo base_url('Kids/registerkidsMobile'); ?>" enctype="multipart/form-data">
                    
                    <div class="col-xs-12 m-t-10 m-b-25" style="">

                        <div class="form-group">
                            <div class="col-xs-12 m-t-10">
                                <div class="fg-line">
                                    <input type="text" name="username" required class="input-sm form-control fg-input" placeholder="Type username here..."/>
                                </div>
                            </div>
                        </div>  
                        <br>
                        <div class="form-group">
                            <div class="col-xs-12 m-t-10">
                                <div class="fg-line">
                                    <input type="password" name="password" required class="input-sm form-control fg-input" placeholder="Type password here..."/>
                                </div>
                                <label class="c-red m-t-5 f-12">( Akses untuk login Anak )</label>
                            </div>
                        </div>                         
                        
                        <div class="form-group" style="margin-top: 11%;">
                            <div class="col-xs-12 m-t-10">
                                <div class="fg-line">
                                    <input type="text" name="first_name" required class="input-sm form-control fg-input" placeholder="Type first name here..."/>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <div class="col-xs-12 m-t-10">
                                <div class="fg-line">
                                    <input type="text" name="last_name" required class="input-sm form-control fg-input" placeholder="Type last name here..."/>
                                </div>
                            </div>
                        </div>   
                        <br>
                        <div class="form-group">
                            <div class="col-xs-12 m-t-10">Tempat Tanggal Lahir :</div>
                            <div class="col-xs-4 m-t-10" style="">
                                <div class="fg-line">
                                    <select name="tanggal_lahir" id="tanggal_lahir" class="select2 form-control">
                                            <option disabled selected><?php echo $this->lang->line('datebirth'); ?></option>
                                            <?php 
                                            if(isset($alluser)){
                                                $tgl = substr($alluser['user_birthdate'], 8,2);
                                                $bln = substr($alluser['user_birthdate'], 5,2);
                                                $thn = substr($alluser['user_birthdate'], 0,4);
                                            }

                                                for ($date=01; $date <= 31; $date++) {
                                                    echo '<option value="'.$date.'" ';
                                                    if(isset($alluser) && $tgl == $date){ echo "selected='true'";}
                                                    echo '>'.$date.'</option>';
                                                }
                                            ?>  
                                        </select>
                                </div>
                            </div>
                            <div class="col-xs-4 m-t-10" style="">
                                <div class="fg-line">
                                    <select name="bulan_lahir" class="select2 form-control">
                                        <option disabled selected><?php echo $this->lang->line('monthbirth'); ?></option>
                                        <option value="01" <?php if(isset($alluser) && $bln == "01"){ echo "selected='true'";} ?>>January</option>
                                        <option value="02" <?php if(isset($alluser) && $bln == "02"){ echo "selected='true'";} ?>>February</option>
                                        <option value="03" <?php if(isset($alluser) && $bln == "03"){ echo "selected='true'";} ?>>March</option>
                                        <option value="04" <?php if(isset($alluser) && $bln == "04"){ echo "selected='true'";} ?>>April</option>
                                        <option value="05" <?php if(isset($alluser) && $bln == "05"){ echo "selected='true'";} ?>>May</option>
                                        <option value="06" <?php if(isset($alluser) && $bln == "06"){ echo "selected='true'";} ?>>June</option>
                                        <option value="07" <?php if(isset($alluser) && $bln == "07"){ echo "selected='true'";} ?>>July</option>
                                        <option value="08" <?php if(isset($alluser) && $bln == "08"){ echo "selected='true'";} ?>>August</option>
                                        <option value="09" <?php if(isset($alluser) && $bln == "09"){ echo "selected='true'";} ?>>September</option>
                                        <option value="10" <?php if(isset($alluser) && $bln == "10"){ echo "selected='true'";} ?>>October</option>
                                        <option value="11" <?php if(isset($alluser) && $bln == "11"){ echo "selected='true'";} ?>>November</option>
                                        <option value="12" <?php if(isset($alluser) && $bln == "12"){ echo "selected='true'";} ?>>December</option>  
                                </select> 
                                </div>
                                <!-- <span class="zmdi zmdi-triangle-down form-control-feedback"></span> -->
                            </div>
                            <div class="col-xs-4 m-t-10" style="">                                   
                                <div class="fg-line">
                                    <select name="tahun_lahir" id="tahun_lahir" class="select2 form-control">
                                        <option disabled selected><?php echo $this->lang->line('yearbirth'); ?></option>
                                        <?php 
                                            for ($i=1960; $i <= 2016; $i++) {                                                                            
                                                echo '<option value="'.$i.'" ';
                                                if(isset($alluser) && $thn == $i){ echo "selected='true'";}
                                                echo '>'.$i.'</option>';
                                            } 
                                        ?>
                                    </select>

                                </div>
                            </div>
                        </div>                                
                        <div class="form-group m-t-10">
                            <div class="col-xs-12 m-t-10">
                                <label class="m-t-10">Tempat Lahir :</label>
                                <div class="fg-line">
                                    <input type="text" name="user_birthplace" required class="input-sm form-control fg-input" placeholder="Type birthplace here..."/>
                                </div>
                            </div>
                        </div>   
                        <br><br>
                        <div class="form-group">
                            <div class="col-xs-12 m-t-10">
                                <label>Jenis Kelamin :</label>
                                <div class="fg-line">
                                    <label class="radio radio-inline ">
                                        <input type="radio" name="user_gender" value="Male">
                                        <i class="input-helper"></i><?php echo $this->lang->line('male'); ?>  
                                    </label>
                                    
                                    <label class="radio radio-inline " >
                                        <input type="radio" name="user_gender" value="Female">
                                        <i class="input-helper"></i><?php echo $this->lang->line('women'); ?>  
                                    </label>
                                </div>
                            </div>
                        </div>   
                        <?php 
                                $provinsi = "";
                                $d_kabupaten = "";
                                $d_kecamatan = "";
                                $d_kelurahan = "";
                                $d_schoolname = "";
                            ?>
                        <div id="input_data_sekolah">
                            <div class="form-group">
                                <div class="col-xs-12 m-t-20">
                                    <label>Pilih Jenjang :</label>
                                    <div class="fg-line">
                                        <select required name="jenjang_id"  class="selectSD form-control" id='jenjang_level' data-placeholder="<?php echo $this->lang->line('select_class'); ?>" style="width: 100%">
                                            <option value=""></option>
                                            <?php
                                                $jen = $this->Master_model->getMasterJenjang();
                                                if($jen != ''){
                                                    echo "<option value='".$jen['jenjang_id']."' selected='true'>".$jen['jenjang_name'].' '.$jen['jenjang_level']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                
                                <div class="col-xs-12 m-t-10">
                                    <label>Data Sekolah :</label>
                                    <div class="fg-line">                                        
                                        <select id='school_provinsi' class="select2 form-control" name="school_provinsi" data-placeholder="<?php echo $this->lang->line('selectprovinsii'); ?>" style="width: 100%">                                            
                                        <?php
                                            
                                            $prv = $this->Master_model->school_provinsi($provinsi);                                                
                                            echo '<option disabled="disabled" selected="" value="0">Provinsi</option>'; 
                                            foreach ($prv as $row => $v) {                                            
                                                echo '<option value="'.$v['provinsi'].'">'.$v['provinsi'].'</option>';
                                            }
                                        ?>
                                        </select>
                                    </div>                                           
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 m-t-20">
                                    <div class="fg-line" id="boxkabupaten">                                        
                                        <select disabled id='school_kabupaten' class="select2 form-control" name="school_kabupaten" data-placeholder="<?php echo $this->lang->line('selectkabupaten'); ?>" style="width: 100%"> 
                                            <option value=""></option>
                                            <?php
                                                
                                                $kbp = $this->Master_model->school_getAllKabupaten($d_kabupaten,$provinsi);
                                                if($kbp != ''){
                                                    foreach ($kbp as $key => $value) {
                                                        echo "<option value='".$value['kabupaten']."'>".$value['kabupaten']."</option>";
                                                    }
                                                    // echo "<script>pfka = '".$kbp['kabupaten']."'</script>";
                                                }

                                            ?>
                                        </select>                                            
                                    </div>
                                </div>                                    
                            </div>
                            
                            <div class="form-group">
                                <div class="col-xs-12 m-t-20">
                                    <div class="fg-line">                                       
                                        <select  disabled id='school_kecamatan' class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkecamatann'); ?>" name="id_kecamatan" style="width: 100%"> 
                                            <option value=''></option>
                                            <?php
                                                $kcm = $this->Master_model->school_getAllKecamatan($d_kecamatan,$d_kabupaten);
                                                if($kcm != ''){
                                                    echo "<option value='".$kcm['kecamatan']."' selected='true'>".$kcm['kecamatan']."</option>";
                                                    echo "<script>pfkec = '".$kcm['kecamatan']."'</script>";

                                                }

                                            ?>
                                        </select>
                                        <small id="cek_kecamatan" style="color: red;"></small>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-xs-12 m-t-20">
                                    <div class="fg-line">
                                        <select disabled id='school_kelurahan' required class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkelurahann'); ?>" name="id_desa" style="width: 100%"> 
                                            <option value=''></option>
                                             <?php
                                                    $des = $this->Master_model->school_getAllKelurahan($d_kelurahan,$d_kecamatan);
                                                    if($des != ''){
                                                        echo "<option value='".$des['kelurahan']."' selected='true'>".$des['kelurahan']."</option>";
                                                        echo "<script>pfkel = '".$kcm['kelurahan']."'</script>";
                                                    }

                                                ?>
                                        </select>
                                        <small id="cek_keluarahan" style="color: red;"></small>
                                    </div>
                                </div>
                            </div>  
                            
                            <div class="form-group">
                                <div class="col-xs-12 m-t-20">
                                    <div class="fg-line">
                                        <select disabled id='school_name' name="school_name" required class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkelurahann'); ?>" name="id_desa" style="width: 100%"> 
                                            <option value=''></option>
                                             <?php
                                                    // $name = $this->Master_model->school_getName($d_schoolname,$d_kelurahan);
                                                  

                                                    // $name = $this->db->query("SELECT school_name FROM master_school group by school_name ASC")->result_array();
                                                   
                                                   
                                                    
                                                    if($name == ''){
                                                        echo "<option value='".$name['school_name']."' selected='true'>".$name['school_name']."</option>";
                                                        echo "<script>pfname = '".$name['school_name']."'</script>";
                                                    }

                                                ?>
                                        </select>
                                        <small id="cek_schoolname" style="color: red;"></small>
                                    </div>
                                </div>
                            </div>  
                            
                            <div class="form-group">
                                <div class="col-xs-12 m-t-20">
                                    <div class="fg-line">
                                        <textarea name="school_address" id="school_address" rows="3" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typeschooladdress'); ?>"></textarea>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                        <center>
                            <button class="btn btn-md c-white btn-block m-t-10 bgm-blue" id="kotak" style= "width: 92%; border: 1px solid gray;">Daftar</button>
                        </center>   
                    </div>                                         
                </form>

            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $("#nominal").keyup(function() {
          var clone = $(this).val();
          var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
          $("#nominalsave").val(cloned);
        });
    });

</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $("#nominal").keyup(function() {
          var clone = $(this).val();
          var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
          $("#nominalsave").val(cloned);
        });

    });

</script>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datetimepicker({
            locale: 'id'
        });

        // $('#school_provinsi').change(function(){
        //     var prov = $(this).val();
        //     var placeprov = "<?php echo $this->lang->line('selectprovinsii'); ?>";
        //     $("#boxkabupaten").empty();
        //     var kotakboxprov = "<select id='school_kabupaten' class='select2 form-control' name='school_kabupaten' data-placeholder='"+placeprov+"'>"+isi+"</select>";
        //     $("#boxkabupaten").append(kotakboxprov);
        // });
        $('select.select2').each(function(){            
            if($(this).attr('id') == 'school_provinsi' || $(this).attr('id') == 'school_kabupaten' || $(this).attr('id') == 'school_kecamatan' || $(this).attr('id') == 'school_kelurahan' || $(this).attr('id') == 'school_name'){
                var a = null;
                if ($(this).attr('id') == 'school_provinsi') { a = "<?php echo $this->lang->line('chooseyourpropinsi'); ?>";} if ($(this).attr('id') == 'school_kecamatan') { a = "<?php echo $this->lang->line('chooseyourkecamatan'); ?>";} if ($(this).attr('id') == 'school_kabupaten') { a = "<?php echo $this->lang->line('chooseyourkabupaten'); ?>";} if ($(this).attr('id') == 'school_kelurahan') { a = "<?php echo $this->lang->line('chooseyourkelurahan'); ?>";}
                $(this).select2({
                    minimumInputLength: 1,
                    placeholder: a,
                    delay: 2000,
                    ajax: {
                        dataType: 'json',
                        type: 'GET',
                        url: '<?php echo base_url(); ?>ajaxer/ajax_indonesia_school/' + $(this).attr('id'),
                        data: function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1,
                                pfp: pfp,
                                pfka: pfka,
                                pfkec: pfkec,
                                pfkel: pfkel,
                                pfname: pfname
                            };
                        },
                        processResults: function(data){
                            return {
                                results: data.results,
                                pagination: {
                                    more: data.more
                                }
                            };
                        }
                    }
                });
            }
            else{
              $(this).select2();
            }
        });

        $('#school_provinsi').change(function(){
            var prov = $(this).val();
            pfp = prov;            
            $('#school_kabupaten.select2').select2("val","");
            $("#school_kabupaten").removeAttr('disabled');
            $("#school_kecamatan").attr('disabled','');
            $("#school_kelurahan").attr('disabled','true');
            $("#school_name").attr('disabled','true');
        });
        $('#school_kabupaten').change(function(){
            var prov = $(this).val();
            pfka = prov;            
            $('#school_kecamatan.select2').select2("val","");
            $("#school_kecamatan").removeAttr('disabled');            
            $("#school_kelurahan").attr('disabled','true');
            $("#school_name").attr('disabled','true');
        });
        $('#school_kecamatan').change(function(){
            var prov = $(this).val();
            pfkec = prov;            
            $('#school_kelurahan.select2').select2("val","");
            $("#school_kelurahan").removeAttr('disabled');            
            $("#school_name").attr('disabled','true');
        });
        $('#school_kelurahan').change(function(){
            var prov = $(this).val();
            pfkel = prov;            
            $('#school_name.select2').select2("val","");
            $("#school_name").removeAttr('disabled');
        });
        $('#school_name').change(function(){
            var prov = $(this).val();            
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>ajaxer/getSchoolAddress/'+prov,
                success:function(datis){
                    var address = $.trim(datis);
                    $('#school_address').val(address);
                }
            });
        });
    });
</script>
<script type="text/javascript">
       $(document).ready(function(){
            var code = null;
            var cekk = setInterval(function(){
                code = "<?php echo $this->session->userdata('code');?>";
                cek();
            },500);
            var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
            function cek(){
                if (code == "342") {
                    $("#alert_modal").modal('show');
                    $("#label_modal").html("Gagal menambahakan Anak.");
                    $("#content_modal").css('background-color','#ff3333');
                    code == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>Rest/clearsession/access_token/'+tokenjwt,
                        type: 'POST',
                        data: {
                            code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
                else if (code == "343") {
                    $("#alert_modal").modal('show');
                    $("#label_modal").html("Sukses menambahakan Anak.");
                    $("#content_modal").css('background-color','#008080');
                    code == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>Rest/clearsession/access_token/'+tokenjwt,
                        type: 'POST',
                        data: {
                            code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
                console.warn(code);
            }
        });           
</script>