<div id="">
    <div class="page-loader" style="background-color:#ff9800;">
        <div class="preloader pl-xs pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Class Miles . . .</p>
        </div>
    </div>
</div>

<style type="text/css">

        html, body {
            max-width: 100%;
            overflow-x: hidden;

        }


        hr.style-two {
            margin-top: -2%;
            margin-left: -1%;
            color: black;
            background-color: black;
            height: 1px;
            width: 96%; 
        }

        hr.style-one {
            margin-top: -2%;
            margin-left: 3%;
            color: #ededed;
            background-color: #ededed;
            /*height: 1px;*/
            width: 95%; 
        }

        #kalender{
            text-align: center; 
            /*margin-bottom: 10%;*/
            /*margin-top: -15%;*/
        }
        @media screen and (max-width: 768px){
            #kalender{
            text-align: center; 
            /*margin-bottom: 10%;*/

            }
            
        }
        @media screen and (max-width: 475px){
            #calender_xl{
               display: none;

           }
            #calender_m{
               display: inline;
           }

         }
          @media screen and (min-width: 475px){
            #calender_xl{
               display: inline;
           }
            #calender_m{
               display: none;
           }

         }
        body{
            /*background-color: white;*/
            margin: 0; height: 100%; overflow: hidden;
        }

            /* scroller browser */
        ::-webkit-scrollbar {
            width: 9px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
            -webkit-border-radius: 7px;
            border-radius: 7px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            -webkit-border-radius: 7px;
            border-radius: 7px;
            background: #a6a5a5;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
        }
        ::-webkit-scrollbar-thumb:window-inactive {
            background: rgba(0,0,0,0.4); 
        }
        </style>
<header id="header" class="clearfix" data-current-skin="blue" style="background-color: #ff9800;">
    <center>
        <ul class="header-inner "  >
            <div class="col-xs-1" style="padding: 0;"></div>
            <div class="col-xs-10" style="padding: 0; margin-bottom: 5%;">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Classmiles Putih High.png" style='width: 40%;' alt="">    
            </div>
            <div class="dropdown col-xs-1" style="font-size: 20px; color: white; padding: 0;" data-animation="fadeIn,fadeOut,600">
                <i class="zmdi zmdi-more-vert" data-toggle="dropdown" aria-expanded="false"></i>
                
                <ul class="dropdown-menu pull-right">
                    <li><a href="<?php echo base_url('/logout'); ?>">Logout</a></li>
                </ul>
            </div>

        </ul>
    </center>
    <div id="kalender"  class=" col-xs-12" style="z-index: 1; background-color: white; height: 55px; width: 107%; margin-left: -3.5%;">
        <div class=""  style="text-align: center;"><a style="color:grey; font-size: 13px;" id="bln_ini"></a></div>
        <div id='prevbtn' class="col-xs-2" style="">
            <a style="font-size: 20px;" ><i class="zmdi zmdi-chevron-left zmdi-hc-fw"></i>
            </a>    
        </div>
        <div class=" col-xs-8" style="text-align: center;">
            <?php echo "<a style=' display:none; font-size: 15px' id='dt' class='c waves-effect active ' ></a>"; ?>
            <?php echo "<a style='display:none; font-size: 15px' id='dt2' class='c waves-effect active ' ></a>"; ?>
            <?php echo "<a style='display:none; font-size: 15px' id='dt3' class='c waves-effect active ' ></a>"; ?>
            <?php echo "<a style='display:none; font-size: 15px' id='dt4' class='c waves-effect active ' ></a>"; ?>
            <?php echo "<a style='display:none; font-size: 15px' id='dt5' class='c waves-effect active ' ></a>"; ?>
            

            <?php echo "<a style='padding-left: 5px; padding-right: 5px; text-align:center; font-size: 12px' id='d1' class=' c waves-effect active ' ></a>"; ?>
            <?php echo "<a style='padding-left: 5px; padding-right: 5px; text-align:center; font-size: 12px' id='d2' class=' c waves-effect active ' ></a>"; ?>
            <?php echo "<a style='padding-left: 5px; padding-right: 5px; text-align:center; font-size: 12px' id='d3' class=' c waves-effect active ' ></a>"; ?>
            <?php echo "<a style='padding-left: 5px; padding-right: 5px; text-align:center; font-size: 12px' id='d4' class=' c waves-effect active ' ></a>"; ?>
            <?php echo "<a style='padding-left: 5px; padding-right: 5px; text-align:center; font-size: 12px' id='d5' class=' c waves-effect active ' ></a>"; ?>
        </div>
        <div id='nextbtn' class=" col-xs-2">
             <a style="font-size: 20px" ><i class="zmdi zmdi-chevron-right zmdi-hc-fw"></i>
            </a>
        </div>
    </div> 
</header>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section style="margin-left: -1%;" id="main" data-layout="layout-1">

   
    <aside id="sidebar" class="sidebar c-overflow" style=" z-index: 11; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>
    
    <!-- <div id='dt'></div> -->
    <div class="modal fade fade" style="" id="modal_loading" tabindex="-1" data-backdrop="static" role="dialog">
            <div class="modal-dialog" role="document" style="background:rgba(0,00,00,0.0);">   
                <div style="margin-top: 50%;">
                    <center>
                       <div class="preloader pl-xxl pls-teal">
                            <svg class="pl-circular" viewBox="25 25 50 50">
                                <circle class="plc-path" cx="50" cy="50" r="20" />
                            </svg>
                        </div><br>
                       <p style="color: white;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
                    </center>
                </div>
            </div>
    </div>
    <div id="loading" style="display: none; margin-top: 22%;">
        <center>
          <img src="https://apps.applozic.com/resources/2636/sidebox/css/app/images/mck-loading.gif" style="height: 40%; width: 40%;">
          <p style="margin-left: -18px;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
        </center>
    </div>
    <section id="content_myclass" class="bgm-grey" style="margin-left: -0%; ">

       <div  class="bgm-grey" style="margin-left: 0%; width: 95%; margin-top: 10%;">
                        
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="block-header">
              <!--   <h2><?php echo $this->lang->line('home'); 
                    ?></h2> -->
            </div> <!-- akhir block header    -->                   
            <div class="bgm-white" style=" display: none; width: 106%;  padding: 5px;  margin-bottom:10px;" id="utamaextraclass">
                <div class="row">

                    <div class="card-header" style="margin-left: 5%">

                        <h4><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/extraclassorange-02.png" style="height: 25px; width: 25px;"> <label class="c-black"><?php echo $this->lang->line('extraclass') ?></label></h4>
                        <hr class="style-two">
                    </div>
                    <div class="" style="margin-top: -2%;">
                        <section style="margin-left: 5%; width: 90%;" class="" id="xboxprivateclass"> 

                        </section>
                        <section style="margin-left: 5%; width: 90%;" class="" id="xboxgroupclass"> 

                        </section>                       
                    </div>
                </div>
            </div>

            <div class="bgm-white" style=" width: 106%;  padding: 5px;  margin-bottom:10px;" id="utama1">
                <div class="row">

                    <div class="card-header" style="margin-left: 5%">

                        <h4><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/liveclass-04.png" style="height: 25px; width: 25px;"> <label class="c-black"><?php echo $this->lang->line('livenow') ?></label></h4>
                        <hr class="style-two">
                    </div>
                    <div class="" style="margin-top: -2%;">
                        <section style="margin-left: 5%; width: 90%;" class="" id="liveboxclass"> 

                        </section>                      
                    </div>
                </div>
            </div>

            <div class="bgm-white" style=" width: 106%;  margin-top: 0px; padding: 5px;  margin-bottom:10px;" id="utama2">
                <div class="row">
                    <div class="card-header" style="margin-left: 5%">
                        <h4><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/comingup-04.png" style="height: 25px; width: 25px;"> <label class="c-black"><?php echo $this->lang->line('comingup') ?></label></h4>
                        <hr class="style-two">
                    </div>
                    <div class="" style="margin-top: -2%;">
                        <section style="margin-left: 5%; width: 90%;" class="" id="xboxclass">
                        </section>                    
                    </div>
                </div>
            </div>

            <div class="bgm-white" style=" width: 106%; margin-top: 0px; padding: 5px;  margin-bottom:10px;" id="utama5">
                <div class="row">
                    <div class="card-header" style="margin-left: 5%">
                        <h4><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/missedclass-04.png" style="height: 25px; width: 25px;"> <label class="c-black"><?php echo $this->lang->line('missed') ?></label></h4>
                        <hr class="style-two">
                    </div>
                    <div class="" style="margin-top: -2%;">
                        <section style="margin-left: 5%; width: 90%;" class="" id="xboxmissed">                        
                        </section>                    
                    </div>
                </div>
            </div>

            <div class="" style=" width: 106%;  margin-top: 0px; padding: 5px;  margin-bottom:10px;" id="utama4" style="display: none;">
                <div class="">                
                    <div class="" style="margin-top:  0%;"><br><br><br><br><br><br>                      
                        <div class="text-center f-16 " role="alert"><i style="font-size: 100px;" class="zmdi zmdi-calendar-note zmdi-hc-fw"></i><br><?php echo $this->lang->line('no_class');?>                            
                        </div>

                    </div>
                </div>
            </div>

            <!-- Modal CONFRIM -->  
            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="confrim" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black"><?php echo $this->lang->line('purchasecomfirmation'); ?></h4>
                            <hr>
                            <p><label class="c-gray f-15"><?php echo $this->lang->line('konfirmasibelikelas'); ?></label></p>
                            <input type="text" name="classid" id="classid" class="c-black" value="" hidden />
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal"><?php echo $this->lang->line('no'); ?></button>
                            <a id="proccess" href=""><button type="button" class="btn bgm-green" id="confrimbuy"><?php echo $this->lang->line('yes'); ?></button></a>
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="confrimjoinn" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Konfirmasi Join</h4>
                            <hr>
                            <p><label class="c-gray f-15"><?php echo $this->lang->line('konfirmasigabungkelas'); ?></label></p>
                            <input type="text" name="classid" id="classid" class="c-black" value="" hidden />
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal"><?php echo $this->lang->line('no'); ?></button>
                            <a id="proccessjoin" href=""><button type="button" class="btn bgm-green" id="confrimjoin"><?php echo $this->lang->line('yes'); ?></button></a>
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="sharelink_show" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black"><?php echo $this->lang->line('berbagilinkkelas'); ?></h4>
                            <hr>
                            <p><label class="c-gray f-15">A<?php echo $this->lang->line('konfirmasigabungkelas'); ?></label></p>
                            <input type="text" name="classid" id="classid" class="c-black" value="" hidden />
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal"><?php echo $this->lang->line('no'); ?></button>
                            <a id="proccessjoin" href=""><button type="button" class="btn bgm-green" id="confrimjoin"><?php echo $this->lang->line('yes'); ?></button></a>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</section>


<script type="text/javascript">
    var stat_showdetail = "<?php echo $this->session->userdata('stat_showdetail');?>";
    // alert(stat_showdetail);
    var tokenjwttt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
    var myjenjang = "<?php echo $this->session->userdata('jenjang_id'); ?>";
    var user_utcc = new Date().getTimezoneOffset();
    var tgll = new Date();
    var formattedDatee = moment(tgll).format('YYYY-MM-DD');
    var id_user = "<?php echo $this->session->userdata('id_user');?>";
    user_utcc = -1 * user_utcc;    
    if (stat_showdetail!="") {        

        $.ajax({
            url: '<?php echo BASE_URL();?>Rest/get_classdesc/access_token/'+tokenjwttt,
            type: 'POST',
            data: {
                user_utc: user_utcc,
                user_device: 'web',
                date: formattedDatee,
                class_id: stat_showdetail
            },
            success: function(data)
            {                   
                var a = JSON.stringify(data);
                console.warn('data '+a);
                subject_id      = data['data'][0]['subject_id'];
                jenjang_id      = data['data'][0]['jenjang_id'];
                status_all      = data['data'][0]['status_all'];
                jenjangnamelevel     = data['data'][0]['jenjangnamelevel'];
                subject_name    = data['data'][0]['name'];
                subject_description = data['data'][0]['description'];
                tutor_id        = data['data'][0]['tutor_id'];
                start_time      = data['data'][0]['start_time'];
                finish_time     = data['data'][0]['finish_time'];
                class_type      = data['data'][0]['class_type'];
                template_type   = data['data'][0]['template_type'];
                template        = data['data'][0]['template_type'];
                user_name       = data['data'][0]['user_name'];
                email           = data['data'][0]['email'];
                user_gender     = data['data'][0]['user_gender'];
                user_age        = data['data'][0]['user_age'];
                user_image      = data['data'][0]['user_image'];
                dateclass       = data['data'][0]['date'];
                timeshow        = data['data'][0]['time'];
                endtimeshow     = data['data'][0]['endtime'];
                jenjang_idd     = data['data'][0]['jenjang_idd'];
                timeclass       = data['data'][0]['date']+" "+data['data'][0]['time'];
                endtimeclass    = data['data'][0]['enddate']+" "+data['data'][0]['endtime'];
                
                var userutc     = "<?php echo $this->session->userdata('user_utc');?>";
                var participant_listt = data['data']['participant']['participant'];
                // alert("Haiii");
                if (status_all == null) {
                    var desc_jenjang = jenjangnamelevel;
                }
                else if (status_all != null) {
                    var desc_jenjang ="Semua Jenjang"
                }

                if (myjenjang != jenjang_idd) {
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Kelas Tersebut tidak tersedia di jenjang Anda");
                    $("#sidebar").css('margin-top','3.5%');
                    $("#kalender").css('display','block');
                    $("#content_classdescription").css('display','none');
                    $("#content_extraclass").css('display','none');
                    $("#content_myclass").css('display','block'); 
                    // $.ajax({
                    //     url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                    //     type: 'POST',
                    //     success: function(data)
                    //     {              
                            
                    //     }
                    // });
                }
                else
                {
                    var im_exist = false;                   
                    if(participant_listt != null){
                        for (var iai = 0; iai < participant_listt.length ;iai++) {                                                              
                            if (id_user == participant_listt[iai]['id_user']) {
                                im_exist = true;
                            }                        
                        }
                    }

                    if (im_exist) {
                        notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut',"Anda telah join di kelas "+subject_name+' - '+subject_description);
                        $("#sidebar").css('margin-top','3.5%');
                        $("#kalender").css('display','block');
                        $("#content_classdescription").css('display','none');
                        $("#content_extraclass").css('display','none');
                        $("#content_myclass").css('display','block'); 
                        // $.ajax({
                        //     url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                        //     type: 'POST',
                        //     success: function(data)
                        //     {              
                                
                        //     }
                        // });
                    }
                    else
                    {
                        if (class_type=="multicast") {typeclass="Multicast";}else if(class_type=="private"){typeclass="Private";}else{typeclass="Group";}
                        if (template_type=="whiteboard_digital") {template_type="Digital Board";}else if(template_type=="whiteboard_videoboard"){template_type="Video Board";}else{template_type="No Whiteboard";}
                        $("#desc_tutorname").append(user_name);
                        $("#desc_tutorgender").append(user_gender);
                        $("#desc_tutorage").append(user_age);
                        $("#desc_tutorimage").attr('src', '<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>'+user_image);

                        $("#titleone").text(subject_name);
                        $("#desc_jenjang").text(desc_jenjang);
                        $("#titletwo").text(subject_description);
                        $("#desc_date").text(dateclass);
                        $("#desc_starttime").text(timeshow+" - ");
                        $("#desc_finishtime").text(endtimeshow);
                        $("#desc_classtype").text(typeclass);
                        $("#desc_templatetype").text(template_type);
                        $("#nametutor").text(user_name);

                        $(".buttonjoinn").attr('link','Transaction/multicast_join/'+stat_showdetail+'?t=kids&child_id='+kid_id);

                        $("#content_myclass").css('display','none');
                        $("#content_extraclass").css('display','none');
                        $("#sidebar").css('margin-top','0%');
                        $("#kalender").css('display','none');
                        $("#modal_extraclass").modal('hide');
                        $("#content_classdescription").css('display','block');

                        // $.ajax({
                        //     url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                        //     type: 'POST',
                        //     success: function(data)
                        //     {              
                                
                        //     }
                        // });
                    }
                }
            }
        });
        
        $(".buttonjoinn").click(function(){
            var link = "<?php echo base_url();?>"+$(this).attr('link');
            $.ajax({
                url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                type: 'POST',
                success: function(data)
                {              
                    window.location.replace(link);
                }
            });            
        });
    }
    $('#selectkelas').change(function(e){
        var kelas = $("#selectkelas").val();

        if (kelas =="private") {
            $("#xboxprivateclass").css('display','block');
            $("#xboxgroupclass").css('display','none');
            $("#allclass").css('display','none');
        }
        else if (kelas =="group") {
            $("#xboxprivateclass").css('display','none');
            $("#xboxgroupclass").css('display','block');
            $("#allclass").css('display','none');
        }
        else if (kelas =="all") {
            $("#xboxprivateclass").css('display','block');
            $("#xboxgroupclass").css('display','block');
            $("#allclass").css('display','block');
        }
    });

    $("#btn_extraclass").click(function(){
            $("#modal_extraclass").modal('show');
            // $("#content_myclass").css('display','none');
            // $("#content_extraclass").css('display','block');
        });
        $(".btn_back_home").click(function(){
            $("#sidebar").css('margin-top','3.5%');
            $("#kalender").css('display','block');
            $("#content_classdescription").css('display','none');
            $("#content_myclass").css('display','block');
            $("#content_extraclass").css('display','none');
        });

        $("#btn_notifgroup").click(function(){
            $("#modal_invitation").modal('show');
            angular.element(document.getElementById('btn_notifgroup')).scope().getondemandprivate();
            getinvitation();
        });

    $(document).ready(function(){

       var currentDate = new Date(new Date().getTime());
        var kemarin = new Date(new Date().getTime() -1 * 24 * 60 * 60 * 1000);
        // alert(kemarin);
        var day = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();

        var kliktgl = null;
        var iduser = "<?php echo $this->session->userdata('id_user'); ?>";
        var tgl = new Date();
        
        var formattedDate = moment(new Date()).format("YYYY-MM-DD");      
        var hari = moment(tgl).format('ddd');    
        var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes();   

        var schedule_time = [];
        var nowtime = [];
        var intervalTime = [];
        var finished = [];
        var finish_time = [];
        var threadHandler = [];
        var hour = [];
        var minute = [];
        var second = [];
        var idnya = null;        
        var c = null;
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        // var email = "<?php echo $this->session->userdata('email');?>";
        // var number = "<?php echo $this->session->userdata('number');?>";
        
        // if (tokenjwt == null) {
        //     $.ajax({
        //         url:'<?php echo base_url();?>Rest/login',
        //         type:'POST',
        //         data:{
        //             email: email,
        //             password: number
        //         },
        //         success:function(response){
        //             console.warn(response);
        //             var access_token = response['response']['access_token']
        //             window.localStorage.setItem('access_token_jwt', response['response']['access_token']);
        //             tokenjwt = access_token;
                    
        //         }
        //     });
        // }

        var mydate = new Date();
        var str = month + ' ' + tgl.getFullYear();
        //Variable untuk tampilan tanggal
        var today = new Date(tgl.getTime());
        var tomorrow = new Date(tgl.getTime() + 1 * 24 * 60 * 60 * 1000);
        var twodays = new Date(tgl.getTime() + 2 * 24 * 60 * 60 * 1000);
        var threedays = new Date(tgl.getTime() + 3 * 24 * 60 * 60 * 1000);
        var fourdays = new Date(tgl.getTime() + 4 * 24 * 60 * 60 * 1000);
        var fivedays = new Date(tgl.getTime() + 5 * 24 * 60 * 60 * 1000);
        var sixdays = new Date(tgl.getTime() + 6 * 24 * 60 * 60 * 1000);



        //Variable untuk disamakan dengan format database
        var bln_tgl1 = moment(today).format('YYYY-MM-DD');
        var bln_tgl2 = moment(tomorrow).format('YYYY-MM-DD');
        var bln_tgl3 = moment(twodays).format('YYYY-MM-DD');
        var bln_tgl4 = moment(threedays).format('YYYY-MM-DD');
        var bln_tgl5 = moment(fourdays).format('YYYY-MM-DD');
        var bln_tgl6 = moment(fivedays).format('YYYY-MM-DD');
        var bln_tgl7 = moment(sixdays).format('YYYY-MM-DD');

        //Variable untuk tampilan hari
        var hari1 = moment(today).format('ddd');
        var hari2 = moment(tomorrow).format('ddd');
        var hari3 = moment(twodays).format('ddd');
        var hari4 = moment(threedays).format('ddd');
        var hari5 = moment(fourdays).format('ddd');
        var hari6 = moment(fivedays).format('ddd');
        var hari7 = moment(sixdays).format('ddd');

        //Variable untuk tampilan hari
        var tanggal1 = moment(today).format('DD');
        var tanggal2 = moment(tomorrow).format('DD');
        var tanggal3 = moment(twodays).format('DD');
        var tanggal4 = moment(threedays).format('DD');
        var tanggal5 = moment(fourdays).format('DD');
        var tanggal6 = moment(fivedays).format('DD');
        var tanggal7 = moment(sixdays).format('DD');

        //Variable untuk tampilan hari
        var bulan1 = moment(today).format('MMM');
        var bulan2 = moment(tomorrow).format('MMM');
        var bulan3 = moment(twodays).format('MMM');
        var bulan4 = moment(threedays).format('MMM');
        var bulan5 = moment(fourdays).format('MMM');
        var bulan6 = moment(fivedays).format('MMM');
        var bulan7 = moment(sixdays).format('MMM');

        //Variable untuk tampilan bulan
        var bulan = moment(currentDate).format('MMMM - YYYY');
        var tglafter1 = new Date(new Date().getTime() + 0 * 24 * 60 * 60 * 1000);
        var tglafter2 = new Date(new Date().getTime() + 1 * 24 * 60 * 60 * 1000);
        var tglafter3 = new Date(new Date().getTime() + 2 * 24 * 60 * 60 * 1000);
        var tglafter4 = new Date(new Date().getTime() + 3 * 24 * 60 * 60 * 1000);
        var tglafter5 = new Date(new Date().getTime() + 4 * 24 * 60 * 60 * 1000);
        var tglafter6 = new Date(new Date().getTime() + 5 * 24 * 60 * 60 * 1000);
        var tglafter7 = new Date(new Date().getTime() + 6 * 24 * 60 * 60 * 1000);


         $(function() {

           
            //Tampilan Bulan di Kalender
            $("#bln_ini").html(bulan);

            //Untuk mencocokan dengan yang ada didatabase
            $("#dt").html(tgl.getFullYear() + '-' + (today.getMonth()+1) + '-' + tanggal1);
            $("#dt2").html(tgl.getFullYear() + '-' + (tomorrow.getMonth()+1) + '-' +tanggal2);
            $("#dt3").html(tgl.getFullYear() + '-' + (twodays.getMonth()+1) + '-' +tanggal3);
            $("#dt4").html(tgl.getFullYear() + '-' + (threedays.getMonth()+1) + '-' +tanggal4);
            $("#dt5").html(tgl.getFullYear() + '-' + (fourdays.getMonth()+1) + '-' +tanggal5);

            //Tampilan untuk tanggal di Kalender
            $("#d1").html(hari + '<br><strong>' +tanggal1 + '</strong>');
            $("#d2").html(hari2 + '<br><strong>' +tanggal2 + '</strong>');
            $("#d3").html(hari3 + '<br><strong>' +tanggal3 + '</strong>');
            $("#d4").html(hari4 + '<br><strong>' +tanggal4 + '</strong>');
            $("#d5").html(hari5 + '<br><strong>' +tanggal5 + '</strong>');
            
            $("#d1").css("color","#2196f3");
            $("#d2").css("color","grey");
            $("#d3").css("color","grey");
            $("#d4").css("color","grey");
            $("#d5").css("color","grey");


            $("#d1").click(function(){
                $("#d1").css("color","#2196f3");
                $("#d2").css("color","grey");
                $("#d3").css("color","grey");
                $("#d4").css("color","grey");
                $("#d5").css("color","grey");
                var aj = $("#dt").text();
                kliktgl = aj;
                 // alert(aj);
                 // alert(formattedDate);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    formattedDate = kliktgl;   
                    // alert(aj);     
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d3").click(function(){
                $("#d1").css("color","grey");
                $("#d2").css("color","grey");
                $("#d3").css("color","#2196f3");
                $("#d4").css("color","grey");
                $("#d5").css("color","grey");
                var aj = $("#dt3").text();
                kliktgl = aj;
                 //alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");

                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d2").click(function(){
                $("#d1").css("color","grey");
                $("#d2").css("color","#2196f3");
                $("#d3").css("color","grey");
                $("#d4").css("color","grey");
                $("#d5").css("color","grey");
                var aj = $("#dt2").text();
                kliktgl = aj;
                 //alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");

                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d4").click(function(){
                $("#d1").css("color","grey");
                $("#d2").css("color","grey");
                $("#d3").css("color","grey");
                $("#d4").css("color","#2196f3");
                $("#d5").css("color","grey");
                var aj = $("#dt4").text();
                kliktgl = aj;
                 //alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");

                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d5").click(function(){
                $("#d1").css("color","grey");
                $("#d2").css("color","grey");
                $("#d3").css("color","grey");
                $("#d4").css("color","grey");
                $("#d5").css("color","#2196f3");
                var aj = $("#dt5").text();
                kliktgl = aj;
                 //alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");

                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
           
        });
        $("#nextbtn").click(function() { 
                $("#d1").css("color","#2196f3");
                $("#d2").css("color","grey");
                $("#d3").css("color","grey");
                $("#d4").css("color","grey");
                $("#d5").css("color","grey");



                currentDate.setDate(currentDate.getDate() + 1);

                var tglafter1 = (currentDate.getTime() + 0 * 24 * 60 * 60 * 1000);
                var tglafter2 = (currentDate.getTime() + 1 * 24 * 60 * 60 * 1000);
                var tglafter3 = (currentDate.getTime() + 2 * 24 * 60 * 60 * 1000);
                var tglafter4 = (currentDate.getTime() + 3 * 24 * 60 * 60 * 1000);
                var tglafter5 = (currentDate.getTime() + 4 * 24 * 60 * 60 * 1000);

                var bulan = moment(currentDate).format('MMMM - YYYY');
                var tgl1 = moment(tglafter1).format('DD');
                var tgl2 = moment(tglafter2).format('DD');
                var tgl3 = moment(tglafter3).format('DD');
                var tgl4 = moment(tglafter4).format('DD');
                var tgl5 = moment(tglafter5).format('DD');

                var hari1 = moment(tglafter1).format('ddd');
                var hari2 = moment(tglafter2).format('ddd');
                var hari3 = moment(tglafter3).format('ddd');
                var hari4 = moment(tglafter4).format('ddd');
                var hari5 = moment(tglafter5).format('ddd');

                var bln1 = moment(tglafter1).format('MM');
                var bln2 = moment(tglafter2).format('MM');
                var bln3 = moment(tglafter3).format('MM');
                var bln4 = moment(tglafter4).format('MM');
                var bln5 = moment(tglafter5).format('MM');
                var bln6 = moment(tglafter6).format('MM');
                var bln7 = moment(tglafter7).format('MM');

                var year1 = moment(tglafter1).format('YYYY');
                var year2 = moment(tglafter2).format('YYYY');
                var year3 = moment(tglafter3).format('YYYY');
                var year4 = moment(tglafter4).format('YYYY');
                var year5 = moment(tglafter5).format('YYYY');
                var year6 = moment(tglafter6).format('YYYY');
                var year7 = moment(tglafter7).format('YYYY');

                $("#bln_ini").html(bulan);

                //Untuk mencocokan dengan yang ada didatabase
                $("#dt").html(year1 + '-' + bln1 + '-' + tgl1);
                $("#dt2").html(year2 + '-' + bln2 + '-' +tgl2);
                $("#dt3").html(year3 + '-' + bln3 + '-' +tgl3);
                $("#dt4").html(year4 + '-' + bln4 + '-' +tgl4);
                $("#dt5").html(year5 + '-' + bln5 + '-' +tgl5);

                //Tampilan untuk tanggal di Kalender
                $("#d1").html(hari1 + '<br><strong>' + tgl1 + '</strong>');
                $("#d2").html(hari2 + '<br><strong>' + tgl2 + '</strong>');
                $("#d3").html(hari3 + '<br><strong>' + tgl3 + '</strong>');
                $("#d4").html(hari4 + '<br><strong>' + tgl4 + '</strong>');
                $("#d5").html(hari5 + '<br><strong>' + tgl5 + '</strong>');
                var aj = $("#dt").text();
                var cekhari = tgl.getDay();
                //alert(aj);
                
                kliktgl = aj;
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");

                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
        });
        $("#prevbtn").click(function() {
                $("#d1").css("color","#2196f3");
                $("#d2").css("color","grey");
                $("#d3").css("color","grey");
                $("#d4").css("color","grey");
                $("#d5").css("color","grey");

                currentDate.setDate(currentDate.getDate() -1);
                
                var tglafter1 = (currentDate.getTime() + 0 * 24 * 60 * 60 * 1000);
                var tglafter2 = (currentDate.getTime() + 1 * 24 * 60 * 60 * 1000);
                var tglafter3 = (currentDate.getTime() + 2 * 24 * 60 * 60 * 1000);
                var tglafter4 = (currentDate.getTime() + 3 * 24 * 60 * 60 * 1000);
                var tglafter5 = (currentDate.getTime() + 4 * 24 * 60 * 60 * 1000);

                var bulan = moment(currentDate).format('MMMM - YYYY');
                var tgl1 = moment(tglafter1).format('DD');
                var tgl2 = moment(tglafter2).format('DD');
                var tgl3 = moment(tglafter3).format('DD');
                var tgl4 = moment(tglafter4).format('DD');
                var tgl5 = moment(tglafter5).format('DD');

                var hari1 = moment(tglafter1).format('ddd');
                var hari2 = moment(tglafter2).format('ddd');
                var hari3 = moment(tglafter3).format('ddd');
                var hari4 = moment(tglafter4).format('ddd');
                var hari5 = moment(tglafter5).format('ddd');

                var bln1 = moment(tglafter1).format('MM');
                var bln2 = moment(tglafter2).format('MM');
                var bln3 = moment(tglafter3).format('MM');
                var bln4 = moment(tglafter4).format('MM');
                var bln5 = moment(tglafter5).format('MM');
                var bln6 = moment(tglafter6).format('MM');
                var bln7 = moment(tglafter7).format('MM');

                var year1 = moment(tglafter1).format('YYYY');
                var year2 = moment(tglafter2).format('YYYY');
                var year3 = moment(tglafter3).format('YYYY');
                var year4 = moment(tglafter4).format('YYYY');
                var year5 = moment(tglafter5).format('YYYY');
                var year6 = moment(tglafter6).format('YYYY');
                var year7 = moment(tglafter7).format('YYYY');

                $("#bln_ini").html(bulan);

                //Untuk mencocokan dengan yang ada didatabase
                $("#dt").html(year1 + '-' + bln1 + '-' + tgl1);
                $("#dt2").html(year2 + '-' + bln2 + '-' +tgl2);
                $("#dt3").html(year3 + '-' + bln3 + '-' +tgl3);
                $("#dt4").html(year4 + '-' + bln4 + '-' +tgl4);
                $("#dt5").html(year5 + '-' + bln5 + '-' +tgl5);

                //Tampilan untuk tanggal di Kalender
                $("#d1").html(hari1 + '<br><strong>' + tgl1 + '</strong>');
                $("#d2").html(hari2 + '<br><strong>' + tgl2 + '</strong>');
                $("#d3").html(hari3 + '<br><strong>' + tgl3 + '</strong>');
                $("#d4").html(hari4 + '<br><strong>' + tgl4 + '</strong>');
                $("#d5").html(hari5 + '<br><strong>' + tgl5 + '</strong>');
                var aj = $("#dt").text();
                var cekhari = tgl.getDay();
                //alert(aj);
                
                kliktgl = aj;
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");

                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
        });


        getschedule(formattedDate);

        function timeToSeconds(time) {
            time = time.split(/:/);
            return time[0] * 3600 + time[1] * 60;
        }  

        function dateTimeToSeconds(datetime){
            dt = datetime.split(" ");
            date = dt[0].split("-");
            time = dt[1].split(":");
            return date[0];
            // return JSON.stringify(dt);
        } 

        $(document).on("click", ".datacheck", function () {
            var myBookId = $(this).data('id');
            var ceklink = $(this).data('link');
            var usertype_now = "<?php echo $this->session->userdata('usertype_usertype_now');?>";
            var usertype_now = "<?php echo $this->session->userdata('usertype_now');?>";
            var id_user_kids = "<?php echo $this->session->userdata('id_user_kids');?>";
            if (usertype_now == 'student kid') {
                $("#proccessjoin").attr("href", ceklink+myBookId+"?t=kids&child_id="+id_user_kids+"&user_utc="+user_utc);
            }
            else
            {
                $("#proccess").attr("href", ceklink+myBookId+"?t=user"+"&user_utc="+user_utc);
            }
            $(".modal-header #classid").val( myBookId );
        });

        $(document).on("click", ".datacheckk", function () {
            var myBookId = $(this).data('id');
            var ceklink = $(this).data('link');
            var usertype_now = "<?php echo $this->session->userdata('usertype_now');?>";
            var id_user_kids = "<?php echo $this->session->userdata('id_user_kids');?>";            
            if (usertype_now == 'student kid') {
                $("#proccessjoin").attr("href", ceklink+myBookId+"?t=kids&child_id="+id_user_kids+"&user_utc="+user_utc);
            }
            else
            {
                $("#proccessjoin").attr("href", ceklink+myBookId+"?t=user"+"&user_utc="+user_utc);
            }
            $(".modal-header #classid").val( myBookId );
        });

        // getschedule(formattedDate);

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        function getschedule(aj)
        {
            $("#liveboxclass").empty();
            $("#xboxclass").empty();
            $("#xboxrecommended").empty();
            $("#xboxmissed").empty();
            $("#xboxgroupclass").empty();
            $("#xboxprivateclass").empty();
            var user_utc = new Date().getTimezoneOffset();
            var livecount = 0;
            var waitcount = 0;
            var groupcount = 0;
            var privatecount = 0;
            var passcount = 0;
            user_utc = -1 * user_utc;
            //UNTUK TAMPILAN YANG MISSED CLASS
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/scheduleone/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    id_user: iduser,
                    user_utc: user_utc,
                    user_device: 'android',
                    date: formattedDate
                },
                success: function(response)
                {   
                    var a = JSON.stringify(response);
                    var jsonPretty = JSON.stringify(JSON.parse(a),null,2);                      
                    // $("#aaa").text(jsonPretty);  
                    // alert(response['data'][1]['class_id']);

                    if (response['data_private'] == null) {
                            // alert(a);
                           // $("#utama4").css('display','block'); 
                           $("#utamaextraclass").css('display','none');
                           $("#filterkelas").css('display','none');
                    }
                    if (response['data_group'] == null) {
                            // alert(a);
                           $("#utama4").css('display','block'); 
                           $("#filterkelas").css('display','none');
                           $("#utamaextraclass").css('display','none');
                    }
                    if (response['data_private'] != null) {

                          // $("#utama4").css('display','none');
                            
                          if (response.data_private.length != null || response.data_group.length != null) {
                            for (var i = 0; i < response.data_private.length; i++) {
                                
                                var request_id = response['data_private'][i]['request_id'];
                                var avtime_id = response['data_private'][i]['avtime_id'];
                                var id_user_requester = response['data_private'][i]['id_user_requester'];
                                var tutor_id = response['data_private'][i]['tutor_id'];
                                var subject_id = response['data_private'][i]['subject_id'];
                                var topic = response['data_private'][i]['topic'];
                                var date_requested = response['data_private'][i]['date_requested'];
                                var duration_requested = response['data_private'][i]['duration_requested'];
                                var approve = response['data_private'][i]['approve'];
                                var template = response['data_private'][i]['template'];
                                var user_utc = response['data_private'][i]['user_utc'];
                                var datetime = response['data_private'][i]['datetime'];
                                var date_requested_utc = response['data_private'][i]['date_requested_utc'];
                                var country = response['data_private'][i]['country'];
                                var user_name = response['data_private'][i]['user_name'];
                                var user_image = response['data_private'][i]['user_image'];
                                var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                                var subject_name = response['data_private'][i]['subject_name'];
                                day = moment(date_requested_utc).format('dddd');
                                date = moment(date_requested_utc).format('DD MMMM YYYY');
                                time = moment(date_requested_utc).format('HH:mm');

                                if (approve == 0) {
                                    var status_tutor ="<small style='margin-top:-1px; color:orange'>Waiting for Tutor approval</small>";

                                }else if (approve == 1) {
                                    var status_tutor ="<small style='margin-top:-1px; color:green'>Confirmed by Tutor</small>";
                                }else if (approve == 2){ 
                                    var status_tutor ="<small style='margin-top:-1px; color:orange'>Waiting for Payment</small>";
                                }
                                else if (approve == -1){ 
                                    var status_tutor ="<small style='margin-top:-1px; color:red'>Reject by Tutor</small>";
                                }
                                else { 
                                    var status_tutor ="<small style='margin-top:-1px; color:red'>Tutor Not available</small>";
                                }
                                
                                var kotak_extrakelasprivate = "<div class='col-xs-12' style='background-color: #d6d6d6'><div style='width:103%; margin-left:-1.5%; padding:1px; background-color:white;' class='m-t-5 m-b-5 col-xs-12'><div  class='col-xs12 m-l-25'>"+day+", "+date+" - "+time+"</div><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:#ff9800; border-bottom-style: solid; margin-bottom:10px;'></div><div class='col-xs-3 col-sm-1 ' style=' width:80px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+" width='60px' height='60px' style=' '><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5 ' style='width:44% height:70px;' ><h6 class=' c-red' style='font-size:11px; '>"+user_name+"</h6><h6 class='c-blue' style=' font-size:10px; height:12px; ; cursor:pointer;  margin-right:-28px;'>"+subject_name+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'>"+status_tutor+"</h5></div></div></div>";
                                $("#utamaextraclass").css('display','block');
                                $("#utama4").css('display','none');
                                
                                privatecount++;
                                $("#xboxprivateclass").append(kotak_extrakelasprivate);
                            }
                            for (var i = 0; i < response.data_group.length; i++) {
                                
                                var request_id = response['data_group'][i]['request_id'];
                                var avtime_id = response['data_group'][i]['avtime_id'];
                                var id_user_requester = response['data_group'][i]['id_user_requester'];
                                var tutor_id = response['data_group'][i]['tutor_id'];
                                var subject_id = response['data_group'][i]['subject_id'];
                                var topic = response['data_group'][i]['topic'];
                                var date_requested = response['data_group'][i]['date_requested'];
                                var duration_requested = response['data_group'][i]['duration_requested'];
                                var approve = response['data_group'][i]['approve'];
                                var template = response['data_group'][i]['template'];
                                var user_utc = response['data_group'][i]['user_utc'];
                                var datetime = response['data_group'][i]['datetime'];
                                var date_requested_utc = response['data_group'][i]['date_requested_utc'];
                                var country = response['data_group'][i]['country'];
                                var user_name = response['data_group'][i]['user_name'];
                                var user_image = response['data_group'][i]['user_image'];
                                var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                                var subject_name = response['data_group'][i]['subject_name'];
                                day = moment(date_requested_utc).format('dddd');
                                date = moment(date_requested_utc).format('DD MMMM YYYY');
                                time = moment(date_requested_utc).format('HH:mm');

                                if (approve == 0) {
                                    var status_tutor ="<small style='margin-top:-1px; color:orange'>Waiting Your Friends approval</small>";

                                }else if (approve == 1) {
                                    var status_tutor ="<small style='margin-top:-1px; color:green'>Confirmed by Tutor</small>";
                                }else if (approve == 2){ 
                                    var status_tutor ="<small style='margin-top:-1px; color:orange'>Waiting for Payment</small>";
                                }
                                else if (approve == -1){ 
                                    var status_tutor ="<small style='margin-top:-1px; color:red'>Reject by Tutor</small>";
                                }
                                else { 
                                    var status_tutor ="<small style='margin-top:-1px; color:red'>Tutor Not available</small>";
                                }
                                
                                var kotak_extrakelasgroup = "<div class='col-xs-12' style='background-color: #d6d6d6'><div style='width:103%; margin-left:-1.5%; padding:1px; background-color:white;' class='m-t-5 m-b-5 col-xs-12'><div  class='col-xs12 m-l-25'>"+day+", "+date+" - "+time+"</div><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:#ff9800; border-bottom-style: solid; margin-bottom:10px;'></div><div class='col-xs-3 col-sm-1 ' style=' width:80px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+" width='60px' height='60px' style=' '><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5 ' style='width:44% height:70px;' ><h6 class=' c-red' style='font-size:11px; '>"+user_name+"</h6><h6 class='c-blue' style=' font-size:10px; height:12px; ; cursor:pointer;  margin-right:-28px;'>"+subject_name+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'>"+status_tutor+"</h5></div></div></div>";
                                $("#utamaextraclass").css('display','block');
                                $("#utama4").css('display','none');
                                
                                groupcount++;
                                $("#xboxgroupclass").append(kotak_extrakelasgroup);
                            }
                          }
                    }
                    if (response['data'] == null) 
                    {                                                
                        // $("#utama1").css('display','none');
                        // $("#utama2").css('display','none');
                        // $("#utama3").css('display','none');
                        // $("#utama5").css('display','none');
                        $("#utama4").css('display','block');
                    }                   
                    else
                    {
                        $("#utama4").css('display','none');
                        // $("#utama1").css('display','block');
                        // $("#utama3").css('display','block');
                        $("#utama5").css('display','block');
                        // $("#utama2").css('display','block');
                        if (response.data.length != null) {
                            // for (var i = response.data.length-1; i >=0;i--) {
                            for (var i = 0; i < response.data.length; i++) {


                                var visible = response['data'][i]['participant']['visible'];
                                var idclass = response['data'][i]['class_id'];
                                var sbjname = response['data'][i]['subject_name'];
                                var desname = response['data'][i]['description'];
                                var desfull = response['data'][i]['description'];
                                var ttrname = response['data'][i]['user_name'];
                                var datee   = response['data'][i]['date'];
                                var enddatee= response['data'][i]['enddate'];
                                datee       = moment(datee).format('DD-MM-YYYY');
                                enddatee    = moment(enddatee).format('DD-MM-YYYY');
                                var time    = response['data'][i]['time'];
                                var endtime = response['data'][i]['endtime'];
                                var timeclass    = response['data'][i]['date']+" "+response['data'][i]['time'];
                                var endtimeclass = response['data'][i]['enddate']+" "+response['data'][i]['endtime'];
                                showtimeclass =moment(timeclass).format('HH:mm');
                                showendtimeclass = moment(endtimeclass).format('HH:mm');
                                var usrimg  = response['data'][i]['user_image'];    
                                var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";            
                                var hrgkls  = response['data'][i]['harga_cm'];
                                var templatetype  = response['data'][i]['template_type'];
                                var classtype  = response['data'][i]['class_type'];
                                var country = response['data'][i]['country'];
                                desname = desname.substring(0, 33);
                                
                                
                                if (classtype == "multicast") 
                                {
                                    var participant_listt = response['data'][i]['participant']['participant'];
                                        var im_exist = false;
                                        if(participant_listt != null){
                                            for (var iai = 0; iai < participant_listt.length ;iai++) {

                                                var a = moment(tgl).format('YYYY-MM-DD');
                                                if (aj == null) {
                                                    if (iduser == participant_listt[iai]['id_user']) {
                                                        im_exist = true;
                                                    }
                                                }
                                                else
                                                {
                                                    if (iduser == participant_listt[iai]['id_user']) {
                                                        im_exist = true;
                                                    }
                                                }
                                            }
                                        }
                                        
                                        //KOTAK DESIGN 
                                        var kotak_missedmulticast = "<section class='me_timer' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer; height: 70px; margin-left: 0px;'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; height: 60px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-blue' style='font-size:10px; margin-top:2%; margin-bottom:1%; cursor:pointer; '>"+desname+"</h5><h5 class='c-black' style='font-size:10px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='visibility:hidden; bottom: 0; margin-left:70%; width:105%; '>DI ISI</p><button class='btn btn-warning btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('kelasberakhir') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";


                                        if(im_exist){
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {   
                                                   
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    
                                                }
                                                else
                                                {       
                                                    passcount++;
                                                    $("#xboxmissed").append(kotak_missedmulticast);
                                                }
                                            }
                                }
                                else if (classtype == "multicast_channel_paid") 
                                {
                                    var participant_listt = response['data'][i]['participant']['participant'];
                                        var im_exist = false;
                                        if(participant_listt != null){
                                            for (var iai = 0; iai < participant_listt.length ;iai++) {
                                                // alert(participant_listt[iai]['id_user']);
                                                var a = moment(tgl).format('YYYY-MM-DD');
                                                if (aj == null) {
                                                    if (iduser == participant_listt[iai]['id_user']) {
                                                        im_exist = true;
                                                    }
                                                }
                                                else
                                                {
                                                    if (iduser == participant_listt[iai]['id_user']) {
                                                        im_exist = true;
                                                    }
                                                }
                                            }
                                        }
                                        
                                        //KOTAK DESIGN 
                                        
                                        var kotak_missedmulticast = "<section style='margin-left:-%; height:70px;' class='me_timer' start_time='"+time+"' finish_time='"+endtime+"'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; height: 60px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-blue' style='font-size:10px; margin-top:2%; margin-bottom:1%; cursor:pointer; '>"+desname+"</h5><h5 class='c-black' style='font-size:10px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='visibility:hidden; bottom: 0; margin-left:70%; width:105%; '>DI ISI</p><button class='btn btn-warning btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('kelasberakhir') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                        

                                        if(im_exist){
                                            passcount++;                                                
                                            $("#xboxmissed").append(kotak_missedmulticast);
                                            
                                        }
                                }
                                else if (classtype == "multicast_paid") 
                                {
                                    var participant_list = response['data'][i]['participant']['participant'];                                                                    

                                    var number_string = hrgkls.toString(),
                                        sisa    = number_string.length % 3,
                                        rupiah  = number_string.substr(0, sisa),
                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                            
                                    if (ribuan) {
                                        separator = sisa ? '.' : '';
                                        rupiah += separator + ribuan.join('.');
                                    } 
                                    
                                    // alert(participant_list);  
                                    if (participant_list!=null) {
                                        var gueAda = false;
                                        var a = moment(tgl).format('YYYY-MM-DD'); 
                                            
                                        var number_string = hrgkls.toString(),
                                            sisa    = number_string.length % 3,
                                            rupiah  = number_string.substr(0, sisa),
                                            ribuan  = number_string.substr(sisa).match(/\d{3}/g);

                                        if (ribuan) {
                                            separator = sisa ? '.' : '';
                                            rupiah += separator + ribuan.join('.');
                                        }  

                                        var kotak_missedmulticastpaid = "<section style='margin-left:-0%; height:120px;' class='me_timer' start_time='"+time+"' finish_time='"+endtime+"'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; height: 60px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-blue' style='font-size:10px; margin-top:2%; margin-bottom:1%; cursor:pointer; '>"+desname+"</h5><h5 class='c-black' style='font-size:10px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '>Rp. "+rupiah+"</p><button class='btn btn-warning btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('kelasberakhir') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                        
                                        for (var iai = 0; iai < participant_list.length ;iai++) {                                            
                                            
                                             
                                            if (aj == null) {                                             
                                                if (iduser == participant_list[iai]['id_user']) {                           
                                                    var number_string = hrgkls.toString(),
                                                        sisa    = number_string.length % 3,
                                                        rupiah  = number_string.substr(0, sisa),
                                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                                            
                                                    if (ribuan) {
                                                        separator = sisa ? '.' : '';
                                                        rupiah += separator + ribuan.join('.');
                                                    }                                      
                                                    
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                       
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else
                                                    {
                                                        passcount++;
                                                        $("#xboxmissed").append(kotak_missedmulticastpaid);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var a = moment(tgl).format('YYYY-MM-DD');

                                                if (iduser == participant_list[iai]['id_user']) {
                                                    gueAda = true;
                                                }
                                            }
                                        }
                                        if(gueAda) {
                                            if (aj == a) 
                                            {                                                                                                                                 
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {                                                            
                                                   
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    
                                                }
                                                else
                                                {       
                                                    passcount++;                                                     
                                                    $("#xboxmissed").append(kotak_missedmulticastpaid);
                                                }
                                            }
                                            else
                                            {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {                                                            
                                                   
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    
                                                }
                                                else
                                                {
                                                    passcount++;                                                     
                                                    $("#xboxmissed").append(kotak_missedmulticastpaid);
                                                }
                                            }
                                        }
                                    }                          
                                }
                                else if (classtype == "private") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];

                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);                                    
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {

                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                    
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    
                                                }
                                                else
                                                {
                                                        passcount++;
                                                    var kotakprivate = "<section style=''><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '><?php echo $this->lang->line('privateclass') ?></p><button class='btn btn-warning btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('kelasberakhir') ?></button></div></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                    $("#xboxmissed").append(kotakprivate);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {

                                                
                                                var kotak_missedprivate = "<section style=''><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '><?php echo $this->lang->line('privateclass') ?></p><button class='btn btn-warning btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('kelasberakhir') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";

                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {                                                         
                                                       
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                       
                                                    }
                                                    else
                                                    {
                                                    passcount++;                                                        
                                                        $("#xboxmissed").append(kotak_missedprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {                                                        
                                                        
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else
                                                    {     
                                                        passcount++;                                                 
                                                        $("#xboxmissed").append(kotak_missedprivate);
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "private_channel") 
                                {                                    
                                    var participant = response['data'][i]['participant']['participant'];                                    

                                    var aca = JSON.stringify(participant);                                    
                                    var jsa = JSON.parse(aca); 
                                                                     
                                    // alert(jsa[0]['id_user']);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');                                        
                                        if (aj == null) {
                                            alert('a');
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {

                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                    
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    
                                                }
                                                else
                                                {
                                                   
                                                }
                                            }
                                        }
                                        else
                                        {                                           
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {

                                                
                                                var kotak_missedprivate = "<section style=''><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '><?php echo $this->lang->line('privateclass') ?></p><button class='btn btn-warning btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('kelasberakhir') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";

                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {                                                         
                                                        
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else
                                                    {
                                                        passcount++;                                                        
                                                        $("#xboxmissed").append(kotak_missedprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {                                                        
                                                       
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                       
                                                    }
                                                    else
                                                    {   
                                                        passcount++;                                                   
                                                        $("#xboxmissed").append(kotak_missedprivate);
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "group") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];
                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                    
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) || Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    
                                                }
                                                else
                                                {
                                                    passcount++;
                                                    var kotakgroup = "<section style='' class=''><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('kelasberakhir') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                    $("#xboxmissed").append(kotakgroup);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else
                                                    {
                                                        passcount++;
                                                        var kotakprivate = "<section style='' class=''><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; color:green;color: green; '>Group Class</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('kelasberakhir') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                        $("#xboxmissed").append(kotakprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                       
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else
                                                    {
                                                        
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "group_channel") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];
                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                    
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) || Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    
                                                }
                                                else
                                                {
                                                    passcount++;
                                                    var kotakgroup = "<section style='' class=''><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='color:green; text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('kelasberakhir') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                    $("#xboxmissed").append(kotakgroup);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                       
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else
                                                    {
                                                        passcount++;
                                                        var kotakprivate = "<section style='' class=''><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; color:green;'>Group Class</p><button class='btn btn-warning btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('kelasberakhir') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                        $("#xboxmissed").append(kotakprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                       
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else
                                                    {
                                                        passcount++;
                                                        var kotakprivate = "<section style='' class=''><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; color:green;'>Group Class</p><button class='btn btn-warning btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('kelasberakhir') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                        $("#xboxmissed").append(kotakprivate);
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }                                                                                            
                                
                            }
                        } 
                    } 
                    // alert(passcount);
                    if (passcount != 0) {
                      $("#utama5").css('display','block');  
                      $("#utama4").css('display','none');  
                    }
                    else
                    {
                     $("#utama5").css('display','none'); 
                     $("#utama4").css('display','block');  
                    }     
                }
            });

            //UNTUK TAMPILAN COMING UP DAN LIVE NOW
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/scheduleone/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    id_user: iduser,
                    user_utc: user_utc,
                    user_device: 'web',
                    date: formattedDate
                },
                success: function(response)
                {   
                    var a = JSON.stringify(response);
                    var jsonPretty = JSON.stringify(JSON.parse(a),null,2);                      
                    // $("#aaa").text(jsonPretty);  
                    // alert(response['data'][1]['class_id']);
                    if (response['data_private'] == null) {
                            // alert(a);
                           // $("#utama4").css('display','block'); 
                           $("#utamaextraclass").css('display','none');
                           $("#filterkelas").css('display','none');
                        }
                        if (response['data_group'] == null) {
                            // alert(a);
                           $("#utama4").css('display','block'); 
                           $("#filterkelas").css('display','none');
                           $("#utamaextraclass").css('display','none');
                        }

                        if (response['data_private'] != null) {

                          // $("#utama4").css('display','none');
                            
                          if (response.data_private.length != null || response.data_group.length != null) {
                            for (var i = 0; i < response.data_private.length; i++) {
                                
                                var request_id = response['data_private'][i]['request_id'];
                                var avtime_id = response['data_private'][i]['avtime_id'];
                                var id_user_requester = response['data_private'][i]['id_user_requester'];
                                var tutor_id = response['data_private'][i]['tutor_id'];
                                var subject_id = response['data_private'][i]['subject_id'];
                                var topic = response['data_private'][i]['topic'];
                                var date_requested = response['data_private'][i]['date_requested'];
                                var duration_requested = response['data_private'][i]['duration_requested'];
                                var approve = response['data_private'][i]['approve'];
                                var template = response['data_private'][i]['template'];
                                var user_utc = response['data_private'][i]['user_utc'];
                                var datetime = response['data_private'][i]['datetime'];
                                var date_requested_utc = response['data_private'][i]['date_requested_utc'];
                                var country = response['data_private'][i]['country'];
                                var user_name = response['data_private'][i]['user_name'];
                                var user_image = response['data_private'][i]['user_image'];
                                var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                                var subject_name = response['data_private'][i]['subject_name'];
                                day = moment(date_requested_utc).format('dddd');
                                date = moment(date_requested_utc).format('DD MMMM YYYY');
                                time = moment(date_requested_utc).format('HH:mm');

                                if (approve == 0) {
                                    var status_tutor ="<small style='margin-top:-1px; color:orange'><?php echo $this->lang->line('waitapprovedtutor');?></small>";

                                }else if (approve == 1) {
                                    var status_tutor ="<small style='margin-top:-1px; color:green'><?php echo $this->lang->line('confirmedbytutor');?></small>";
                                }else if (approve == 2){ 
                                    var status_tutor ="<small style='margin-top:-1px; color:orange'><?php echo $this->lang->line('waitforpayment');?></small>";
                                }
                                else if (approve == -1){ 
                                    var status_tutor ="<small style='margin-top:-1px; color:red'><?php echo $this->lang->line('rejectedbytutor');?></small>";
                                }
                                else { 
                                    var status_tutor ="<small style='margin-top:-1px; color:red'><?php echo $this->lang->line('tutornotavailable');?></small>";
                                }
                                
                                var kotak_extrakelasprivate = "<div class='me_timer col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:280px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px;'>"+subject_name+" - "+topic+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+user_name+"</small>"+status_tutor+"<small>"+day+", "+date+" - "+time+"</small></h2></a></div></a></div>";
                                $("#utamaextraclass").css('display','block');
                                $("#utama4").css('display','none');
                                
                                privatecount++;
                                $("#xboxprivateclass").append(kotak_extrakelasprivate);
                            }
                            for (var i = 0; i < response.data_group.length; i++) {
                                
                                var request_id = response['data_group'][i]['request_id'];
                                var avtime_id = response['data_group'][i]['avtime_id'];
                                var id_user_requester = response['data_group'][i]['id_user_requester'];
                                var tutor_id = response['data_group'][i]['tutor_id'];
                                var subject_id = response['data_group'][i]['subject_id'];
                                var topic = response['data_group'][i]['topic'];
                                var date_requested = response['data_group'][i]['date_requested'];
                                var duration_requested = response['data_group'][i]['duration_requested'];
                                var approve = response['data_group'][i]['approve'];
                                var template = response['data_group'][i]['template'];
                                var user_utc = response['data_group'][i]['user_utc'];
                                var datetime = response['data_group'][i]['datetime'];
                                var date_requested_utc = response['data_group'][i]['date_requested_utc'];
                                var country = response['data_group'][i]['country'];
                                var user_name = response['data_group'][i]['user_name'];
                                var user_image = response['data_group'][i]['user_image'];
                                var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                                var subject_name = response['data_group'][i]['subject_name'];
                                day = moment(date_requested_utc).format('dddd');
                                date = moment(date_requested_utc).format('DD MMMM YYYY');
                                time = moment(date_requested_utc).format('HH:mm');
                                var approve = response['data_group'][i]['approve'];
                                var buttonnih = null;
                                var cekstatusdia = response['data_group'][i]['id_friends']['id_friends'];
                                var myStatus = null;
                                var statusKelas = null;
                                var statusteman = null;
                                for (var z = 0; z < cekstatusdia.length; z++) {
                                    if (cekstatusdia[z]['id_user'] == iduser) 
                                    {
                                        myStatus = cekstatusdia[z]['status'];

                                    }
                                    else if (cekstatusdia[z]['id_user'] != iduser) 
                                    {
                                        statusteman = cekstatusdia[z]['status'];

                                    }


                                }

                                if (approve == 0 && statusteman != 0) {
                                    var status_tutor ="<small style='margin-top:-1px; color:orange'><?php echo $this->lang->line('waitapprovedtutor');?></small>";

                                }
                                else if (approve == 0 && statusteman == 0) {
                                    var status_tutor ="<small style='margin-top:-1px; color:orange'><?php echo $this->lang->line('waitapprovedfriend');?></small>";

                                }else if (approve == 1) {
                                    var status_tutor ="<small style='margin-top:-1px; color:green'><?php echo $this->lang->line('confirmedbytutor');?></small>";
                                }else if (approve == 2){ 
                                    var status_tutor ="<small style='margin-top:-1px; color:orange'><?php echo $this->lang->line('waitforpayment');?></small>";
                                }
                                else if (approve == -1){ 
                                    var status_tutor ="<small style='margin-top:-1px; color:red'><?php echo $this->lang->line('rejectedbytutor');?></small>";
                                }
                                else { 
                                    var status_tutor ="<small style='margin-top:-1px; color:red'><?php echo $this->lang->line('tutornotavailable');?></small>";
                                }
                                
                                var kotak_extrakelasgroup = "<div class='me_timer col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:280px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px;'>"+subject_name+" - "+topic+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+user_name+"</small>"+status_tutor+"<small>"+day+", "+date+" - "+time+"</small></h2></a></div></a></div>";
                                $("#utamaextraclass").css('display','block');
                                $("#utama4").css('display','none');
                                
                                groupcount++;
                                $("#xboxgroupclass").append(kotak_extrakelasgroup);
                            }
                          }
                        }

                    if (response['data'] == null) 
                    {                                                
                        // $("#utama1").css('display','none');
                        // $("#utama2").css('display','none');
                        // $("#utama3").css('display','none');
                        // $("#utama5").css('display','none');
                        $("#utama4").css('display','block');
                    }                   
                    else
                    {
                        $("#utama4").css('display','none');
                        // $("#utama1").css('display','block');
                        // $("#utama3").css('display','block');
                        // $("#utama5").css('display','block');
                        // $("#utama2").css('display','block');
                        if (response.data.length != null) {
                            for (var i = 0; i < response.data.length; i++) {

                                var visible = response['data'][i]['participant']['visible'];
                                var idclass = response['data'][i]['class_id'];
                                var sbjname = response['data'][i]['subject_name'];
                                var desname = response['data'][i]['description'];
                                var desfull = response['data'][i]['description'];
                                var ttrname = response['data'][i]['user_name'];
                                var datee   = response['data'][i]['date'];
                                var enddatee= response['data'][i]['enddate'];
                                datee       = moment(datee).format('DD-MM-YYYY');
                                enddatee    = moment(enddatee).format('DD-MM-YYYY');
                                var time    = response['data'][i]['time'];
                                var endtime = response['data'][i]['endtime'];
                                var timeclass    = response['data'][i]['date']+" "+response['data'][i]['time'];
                                var endtimeclass = response['data'][i]['enddate']+" "+response['data'][i]['endtime'];
                                showtimeclass =moment(timeclass).format('HH:mm');
                                showendtimeclass = moment(endtimeclass).format('HH:mm');
                                var usrimg  = response['data'][i]['user_image'];     
                                var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";           
                                var hrgkls  = response['data'][i]['harga_cm'];
                                var templatetype  = response['data'][i]['template_type'];
                                var classtype  = response['data'][i]['class_type'];
                                var country = response['data'][i]['country'];
                                desname = desname.substring(0, 33);
                                
                                
                                if (classtype == "multicast") 
                                {
                                    var participant_listt = response['data'][i]['participant']['participant'];
                                        var im_exist = false;
                                        if(participant_listt != null){
                                            for (var iai = 0; iai < participant_listt.length ;iai++) {

                                                var a = moment(tgl).format('YYYY-MM-DD');
                                                if (aj == null) {
                                                    if (iduser == participant_listt[iai]['id_user']) {
                                                        im_exist = true;
                                                    }
                                                }
                                                else
                                                {
                                                    if (iduser == participant_listt[iai]['id_user']) {
                                                        im_exist = true;
                                                    }
                                                }
                                            }
                                        }
                                        
                                        //KOTAK DESIGN 
                                        var kotak_comingmulticast = "<section  class='me_timer' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer; height: 70px; margin-left: 0px;'><a data-id='"+idclass+"' href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; height: 60px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style='margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-blue' style='font-size:10px; margin-top:2%; margin-bottom:1%; cursor:pointer; '>"+desname+"</h5><h5 class='c-black' style='font-size:10px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='visibility:hidden; bottom: 0; margin-left:70%; width:105%; '>DIISI</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                        var kotak_livemulticast = "<section class=' me_timer' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer; height: 70px; margin-left: 0px;'><a href='<?php echo base_url(); ?>master/tps_me/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; height: 60px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style='margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-blue' style='font-size:10px; margin-top:2%; margin-bottom:1%; cursor:pointer; '>"+desname+"</h5><h5 class='c-black' style='font-size:10px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='visibility:hidden; bottom: 0; margin-left:70%; width:105%; '>DIISI</p><button class='btn btn-success btn-block btn-sm' style=' bottom: 0; '>Masuk</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                        
                                        
                                        

                                        if(im_exist){
                                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                                            {                                               
                                                waitcount++;
                                                $("#xboxclass").append(kotak_comingmulticast);
                                            }
                                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                livecount++;
                                                
                                                $("#liveboxclass").append(kotak_livemulticast);
                                            }
                                            else
                                            {
                                            
                                            }
                                        }
                                }
                                else if (classtype == "multicast_channel_paid") 
                                {
                                    var participant_listt = response['data'][i]['participant']['participant'];
                                        var im_exist = false;
                                        if(participant_listt != null){
                                            for (var iai = 0; iai < participant_listt.length ;iai++) {
                                                // alert(participant_listt[iai]['id_user']);
                                                var a = moment(tgl).format('YYYY-MM-DD');
                                                if (aj == null) {
                                                    if (iduser == participant_listt[iai]['id_user']) {
                                                        im_exist = true;
                                                    }
                                                }
                                                else
                                                {
                                                    if (iduser == participant_listt[iai]['id_user']) {
                                                        im_exist = true;
                                                    }
                                                }
                                            }
                                        }
                                        
                                        //KOTAK DESIGN 
                                        var kotak_comingmulticast = "<section  style='height:70px;' class='me_timer' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; height: 60px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-blue' style='font-size:10px; margin-top:2%; margin-bottom:1%; cursor:pointer; '>"+desname+"</h5><h5 class='c-black' style='font-size:10px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='visibility:hidden; bottom: 0; margin-left:70%; width:105%; '>DI ISI</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                        var kotak_livemulticast = "<section style='margin-left:-%; height:70px;' class='me_timer' start_time='"+time+"' finish_time='"+endtime+"'><a href='<?php echo base_url(); ?>master/tps_me/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; height: 60px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-blue' style='font-size:10px; margin-top:2%; margin-bottom:1%; cursor:pointer; '>"+desname+"</h5><h5 class='c-black' style='font-size:10px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='visibility:hidden; bottom: 0; margin-left:70%; width:105%; '>DI ISI</p><button class='btn btn-success btn-block btn-sm' style=' bottom: 0; '>Masuk</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                        
                                        

                                        if(im_exist){
                                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                                            {                                               
                                                waitcount++;
                                                $("#xboxclass").append(kotak_comingmulticast);
                                            }
                                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                livecount++;
                                                
                                                $("#liveboxclass").append(kotak_livemulticast);
                                            }
                                            else
                                            {
                                           
                                            }
                                        }
                                }
                                else if (classtype == "multicast_paid") 
                                {
                                    var participant_list = response['data'][i]['participant']['participant'];                                                                    

                                    var number_string = hrgkls.toString(),
                                        sisa    = number_string.length % 3,
                                        rupiah  = number_string.substr(0, sisa),
                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                            
                                    if (ribuan) {
                                        separator = sisa ? '.' : '';
                                        rupiah += separator + ribuan.join('.');
                                    } 
                                    // alert(participant_list);  
                                    if (participant_list!=null) {
                                        var gueAda = false;
                                        var a = moment(tgl).format('YYYY-MM-DD'); 
                                            
                                        var number_string = hrgkls.toString(),
                                            sisa    = number_string.length % 3,
                                            rupiah  = number_string.substr(0, sisa),
                                            ribuan  = number_string.substr(sisa).match(/\d{3}/g);

                                        if (ribuan) {
                                            separator = sisa ? '.' : '';
                                            rupiah += separator + ribuan.join('.');
                                        }  

                                        var kotak_comingmulticastpaid = "<section  style='height:120px;' class='me_timer' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; height: 60px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-blue' style='font-size:10px; margin-top:2%; margin-bottom:1%; cursor:pointer; '>"+desname+"</h5><h5 class='c-black' style='font-size:10px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='visibility:hidden; bottom: 0; margin-left:70%; width:105%; '>DI ISI</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                        var kotak_livemulticastpaid = "<section style='margin-left:-0%; height:120px;' class='me_timer' start_time='"+time+"' finish_time='"+endtime+"'><a href='<?php echo base_url(); ?>master/tps_me/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; height: 60px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-blue' style='font-size:10px; margin-top:2%; margin-bottom:1%; cursor:pointer; '>"+desname+"</h5><h5 class='c-black' style='font-size:10px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='visibility:hidden; bottom: 0; margin-left:70%; width:105%; '>DI ISI</p><button class='btn btn-success btn-block btn-sm' style=' bottom: 0; '>Masuk</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                        
                                        
                                        for (var iai = 0; iai < participant_list.length ;iai++) {                                            
                                            
                                             
                                            if (aj == null) {                                             
                                                if (iduser == participant_list[iai]['id_user']) {                           
                                                    var number_string = hrgkls.toString(),
                                                        sisa    = number_string.length % 3,
                                                        rupiah  = number_string.substr(0, sisa),
                                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                                            
                                                    if (ribuan) {
                                                        separator = sisa ? '.' : '';
                                                        rupiah += separator + ribuan.join('.');
                                                    }                                      
                                                    
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        waitcount++;
                                                        $("#xboxclass").append(kotak_comingmulticastpaid);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        
                                                        $("#liveboxclass").append(kotak_livemulticastpaid);
                                                    }
                                                    else
                                                    {
                                                       
                                                    }
                                                }
                                                
                                            }
                                            else
                                            {
                                                var a = moment(tgl).format('YYYY-MM-DD');

                                                if (iduser == participant_list[iai]['id_user']) {
                                                    gueAda = true;
                                                }
                                            }
                                        }
                                        if(gueAda) {
                                            if (aj == a) 
                                            {                                                                                                                                 
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {                                                            
                                                    waitcount++;
                                                    $("#xboxclass").append(kotak_comingmulticastpaid);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    livecount++;
                                                    $("#liveboxclass").append(kotak_livemulticastpaid);
                                                }
                                                else
                                                {       
                                                    passcount++;                                                     
                                                    $("#xboxmissed").append(kotak_missedmulticastpaid);
                                                }
                                            }
                                            else
                                            {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {                                                            
                                                    waitcount++;
                                                    $("#xboxclass").append(kotak_comingmulticastpaid);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    livecount++;
                                                    $("#liveboxclass").append(kotak_livemulticastpaid);
                                                }
                                                else
                                                {
                                                    
                                                }
                                            }
                                        }
                                    }                              
                                }
                                else if (classtype == "private") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];

                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);                                    
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {

                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                    waitcount++;
                                                    var kotakprivate = "<section style=''><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '><?php echo $this->lang->line('privateclass') ?></p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                    $("#xboxclass").append(kotakprivate);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    livecount++;
                                                    var kotakprivate = "<section style=''><a href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '><?php echo $this->lang->line('privateclass') ?></p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('belikelas') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                    $("#liveboxclass").append(kotakprivate);
                                                }
                                                else
                                                {
                                                       
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {

                                                var kotak_comingprivate = "<section style=''><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '><?php echo $this->lang->line('privateclass') ?></p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                var kotak_liveprivate = "<section style=''><a href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '><?php echo $this->lang->line('privateclass') ?></p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('belikelas') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                
                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {                                                         
                                                        waitcount++;
                                                        $("#xboxclass").append(kotak_comingprivate);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        
                                                        $("#liveboxclass").append(kotak_liveprivate);
                                                    }
                                                    else
                                                    {
                                                    
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {                                                        
                                                        waitcount++;
                                                        $("#xboxclass").append(kotak_comingprivate);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        $("#liveboxclass").append(kotak_liveprivate);
                                                    }
                                                    else
                                                    {     
                                                       
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "private_channel") 
                                {                                    
                                    var participant = response['data'][i]['participant']['participant'];                                    

                                    var aca = JSON.stringify(participant);                                    
                                    var jsa = JSON.parse(aca); 
                                                                     
                                    // alert(jsa[0]['id_user']);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');                                        
                                        if (aj == null) {
                                            alert('a');
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {

                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                    waitcount++;
                                                    var kotakprivate = "<section style=''><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '><?php echo $this->lang->line('privateclass') ?></p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                    $("#xboxclass").append(kotakprivate);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    livecount++;
                                                    var kotakprivate = "<section style=''><a href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '><?php echo $this->lang->line('privateclass') ?></p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('belikelas') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                    $("#liveboxclass").append(kotakprivate);
                                                }
                                                else
                                                {
                                                    
                                                }
                                            }
                                        }
                                        else
                                        {                                           
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {

                                                var kotak_comingprivate = "<section style=''><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '><?php echo $this->lang->line('privateclass') ?></p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                var kotak_liveprivate = "<section style=''><a href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6>><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%; '><?php echo $this->lang->line('privateclass') ?></p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'><?php echo $this->lang->line('belikelas') ?></button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                               

                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {                                                         
                                                        waitcount++;
                                                        $("#xboxclass").append(kotak_comingprivate);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        
                                                        $("#liveboxclass").append(kotak_liveprivate);
                                                    }
                                                    else
                                                    {
                                                       
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {                                                        
                                                        waitcount++;
                                                        $("#xboxclass").append(kotak_comingprivate);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        $("#liveboxclass").append(kotak_liveprivate);
                                                    }
                                                    else
                                                    {   
                                                       
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "group") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];
                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                    waitcount++;
                                                     var kotakgroup = "<section class=''><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                     $("#xboxclass").append(kotakgroup);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) || Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    var kotakgroup = "<section style='' class=''><a href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-success btn-block btn-sm' style=' bottom: 0; '>Masuk</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                    $("#liveboxclass").append(kotakgroup);
                                                }
                                                else
                                                {
                                                   
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        waitcount++;
                                                        var kotakprivate = "<section class=''><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        var kotakprivate = "<section style='' class=''><a href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-success btn-block btn-sm' style=' bottom: 0; '>Masuk</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                        
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        waitcount++;
                                                        var kotakprivate = "<section class=''><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style=' width:60px; height:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        var kotakprivate = "<section style='' class=''><a href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-success btn-block btn-sm' style=' bottom: 0; '>Masuk</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                       
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "group_channel") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];
                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                    waitcount++;
                                                     var kotakgroup = "<section class=''><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                     $("#xboxclass").append(kotakgroup);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) || Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    livecount++;
                                                    var kotakgroup = "<section style='' class=''><a href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-success btn-block btn-sm' style=' bottom: 0; '>Masuk</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                    $("#liveboxclass").append(kotakgroup);
                                                }
                                                else
                                                {
                                                   
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        waitcount++;
                                                        var kotakprivate = "<section class=''><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        var kotakprivate = "<section style='' class=''><a href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-success btn-block btn-sm' style=' bottom: 0; '>Masuk</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                       
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        waitcount++;
                                                        var kotakprivate = "<section class=''><a href='#'><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-info btn-block btn-sm' style=' bottom: 0;'>Tunggu</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        var kotakprivate = "<section style='' class=''><a href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=android'><div class=' m-t-10 col-xs-3 col-sm-1 ' style='height:60px; width:60px;  vertical-align: middle; line-height: 60px; '><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+" width='60px' height='60px' style=' filter: grayscale(00%); margin-left: -15px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; margin: 2%; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5' style='width:44% height:70px; padding-right:0;' ><h6 class=' c-red' style='margin-bottom:1%; font-size:11px; '>"+ttrname+"</h6><h6 class='c-blue' style=' margin-bottom:2%; margin-top: 2%; font-size:10px; height:12px; ; cursor:pointer;'>"+sbjname+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'><i class='zmdi zmdi-time zmdi-hc-fw'></i>"+showtimeclass+" - "+showendtimeclass+"</h5></div><div class='col-xs-4 m-t-10' style='height:70px; padding-right:0; width:35%; right:0;'><p class='' style='text-align:center; margin-left:30%; width:105%;color: green; '>Group Class</p><button class='btn btn-success btn-block btn-sm' style=' bottom: 0; '>Masuk</button></div></a></section><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; margin-bottom:10px;'></div>";
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                       
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }                                                                                            
                                
                            }
                        } 
                    } 
                    // alert(waitcount);
                    if (livecount != 0) {
                      $("#utama1").css('display','block');  
                      $("#utama4").css('display','none');  
                    }
                    else
                    {
                     $("#utama1").css('display','none');  
                     // $("#utama4").css('display','block');  
                    }
                    if (waitcount != 0) {
                      $("#utama2").css('display','block');  
                      $("#utama4").css('display','none');  
                    }
                    else
                    {
                     $("#utama2").css('display','none'); 
                     // $("#utama4").css('display','block');   
                    }
                }
            });
        }  


    });
$(".tabmenu").click(function() {
    // body...
    $("#modal_loading").modal('show');
});
$('#selectkelas').change(function(e){
        var kelas = $("#selectkelas").val();

        if (kelas =="private") {
            $("#xboxprivateclass").css('display','block');
            $("#xboxgroupclass").css('display','none');
            $("#allclass").css('display','none');
        }
        else if (kelas =="group") {
            $("#xboxprivateclass").css('display','none');
            $("#xboxgroupclass").css('display','block');
            $("#allclass").css('display','none');
        }
        else if (kelas =="all") {
            $("#xboxprivateclass").css('display','block');
            $("#xboxgroupclass").css('display','block');
            $("#allclass").css('display','block');
        }
    });
    $("#privateclick").click(function(){
        $("#sidebar").css('margin-top','0%');
        $("#kalender").css('display','none');
        $("#modal_extraclass").modal('hide');
        $("#content_myclass").css('display','none');
        $("#content_classdescription").css('display','none');
        $("#content_extraclass").css('display','block');

        // $("#kotaknyagroup").removeClass('bgm-orange');
        // $("#kotaknyagroup").addClass('bgm-gray');
        // $("#kotaknyaprivate").removeClass('bgm-gray');
        // $("#kotaknyaprivate").addClass('bgm-orange');

        $("#box_groupclass").css('display','none');
        $("#privateclick").css('filter','grayscale();');
        $("#groupklik").css('filter','none');
        $("#box_privateclass").css('display','block');
    });
    $("#groupclassclick").click(function(){
        $("#sidebar").css('margin-top','0%');
        $("#kalender").css('display','none');
        $("#modal_extraclass").modal('hide');
        $("#content_myclass").css('display','none');
        $("#content_classdescription").css('display','none');
        $("#content_extraclass").css('display','block');
        // $("#kotaknyaprivate").removeClass('bgm-orange');
        // $("#kotaknyaprivate").addClass('bgm-gray'); 
        // $("#kotaknyagroup").removeClass('bgm-gray');
        // $("#kotaknyagroup").addClass('bgm-orange');
        $("#box_privateclass").css('display','none');
        $("#box_groupclass").css('opacity','100');        
        $("#durasigroup").addClass('col-md-12');
        $("#box_groupclass").css('display','block');
    });
</script>  
    <script type="text/javascript">    
        $(document).ready(function() {
            setInterval(function(){  
                cekNotif();  
                     
            },3000);
        });

        function cekNotif(){
            var methodpembayaran = null;
            var hargakelas = null;
            var datetime = null;
            var iduser = "<?php echo $this->session->userdata('id_user'); ?>";
            var balanceuser = null;
            var requestid = null;
            var user = "<?php echo $this->session->userdata('id_user'); ?>";        
            var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
            var user_utc = new Date().getTimezoneOffset();
            user_utc = 1 * user_utc;

            $.ajax({
                url: '<?php echo base_url(); ?>Rest/get_friendinvitation/access_token/'+tokenjwt,
                type: 'POST',   
                data: {
                    id_user: user,
                    user_utc:user_utc
                },             
                success: function(data)
                {                   
                    var a = JSON.stringify(data['response']);
                    var b = JSON.parse(a);
                    // console.warn(a);
                    // hargakelas = data['data']['harga_kelas'];
                    if (data['code'] == 300) {
                    }
                    else
                    {    
                     // alert(a); 
                        var no = 0;           
                        for (var i = data.response.length-1; i >=0;i--) {
                            var request_id = data['response'][i]['request_id'];
                            var requester_name = data['response'][i]['requester_name'];
                            var tutor_name = data['response'][i]['tutor_name'];
                            var subject_name = data['response'][i]['subject_name'];
                            var method_pembayaran = data['response'][i]['method_pembayaran'];
                            var harga_kelas = data['response'][i]['harga_kelas'];
                            var harga_kelass = data['response'][i]['harga_kelas'];
                            var duration_requested = data['response'][i]['duration_requested'];
                            var date_requested = data['response'][i]['date_requested'];
                            var approve = data['response'][i]['approve'];
                            var buttonnih = null;
                            var cekstatusdia = data['response'][i]['id_friends']['id_friends'];
                            var myStatus = null;
                            for (var z = cekstatusdia.length - 1; z >= 0; z--) {
                                if (cekstatusdia[z]['id_user'] == iduser) 
                                {
                                    myStatus = cekstatusdia[z]['status'];
                                }
                            }
                            no = no+1;
                            if (myStatus == 0 && approve == 0) {

                               $(".notif_friend").css('display','block');
                            }   
                            else{                    
                                $(".notif_friend").css('display','none');
                            }
                            
                        }
                    }
                }
            });
        }
        function getinvitation() {


            $("#tempatfriendinvitation").empty();
            var methodpembayaran = null;
            var hargakelas = null;
            var datetime = null;
            var tgl = new Date();
            var iduser = "<?php echo $this->session->userdata('id_user'); ?>";
            var formattedDate = moment(tgl).format('YYYY-MM-DD'); 
            var balanceuser = null;
            var requestid = null;
            var user = "<?php echo $this->session->userdata('id_user'); ?>";        
            var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
            var user_utc = new Date().getTimezoneOffset();
            user_utc = -1 * user_utc;
            // alert(user_utc);
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/get_friendinvitation/access_token/'+tokenjwt,
                type: 'POST',   
                data: {
                    id_user: user,
                    user_utc: user_utc,
                    user_device: 'web'                
                },             
                success: function(data)
                {                   
                    var a = JSON.stringify(data['response']);
                    var b = JSON.parse(a);
                    // hargakelas = data['data']['harga_kelas'];
                    if (data['code'] == 300) {
                        var kosong = "<tr><td colspan='11'><center>Tidak ada Data</center></td></tr>";
                        $("#tempatfriendinvitation").append(kosong);
                    }
                    else
                    {    
                     // console.warn(a);
                        var no = 0;           
                        for (var i = 0; i < data.response.length; i++) {
                            var request_id = data['response'][i]['request_id'];
                            var requester_name = data['response'][i]['requester_name'];
                            var tutor_name = data['response'][i]['tutor_name'];
                            var user_image = data['response'][i]['user_image'];
                            var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                            var country = data['response'][i]['country'];
                            var topic = data['response'][i]['topic'];
                            var user_utc = data['response'][i]['user_utc'];
                            var subject_name = data['response'][i]['subject_name'];
                            var method_pembayaran = data['response'][i]['method_pembayaran'];
                            var harga_kelas = data['response'][i]['harga_kelas'];
                            var harga_kelass = data['response'][i]['harga_kelas'];
                            var duration_requested = data['response'][i]['duration_requested'];
                            var date_requested = data['response'][i]['date_requested'];
                            var date_requested_utc = data['response'][i]['date_requested_utc'];
                            // var endtime = date_requested + duration_requested;
                            var endtime = duration_requested / 60;

                            day = moment(date_requested_utc).format('dddd');
                            date = moment(date_requested_utc).format('DD MMMM YYYY');
                            time = moment(date_requested_utc).format('HH:mm');

                            var approve = data['response'][i]['approve'];
                            var buttonnih = null;
                            var cekstatusdia = data['response'][i]['id_friends']['id_friends'];
                            var myStatus = null;
                            var statusKelas = null;
                            methodpembayaran = method_pembayaran;
                            if (method_pembayaran=="bayar_sendiri") {
                                harga_kelas = "Rp. 0";
                                method_pembayaran = "Bayar Sendiri";
                            }
                            else
                            {
                                method_pembayaran = "Bagi Rata";
                            }
                            for (var z = 0; z < cekstatusdia.length; z++) {
                                if (cekstatusdia[z]['id_user'] == iduser) 
                                {
                                    myStatus = cekstatusdia[z]['status'];

                                }


                            }
                            if (approve == 0 && myStatus == 0) {
                                statusKelas = "<i class='m-l-5 c-orange'>Status : <?php echo $this->lang->line('waitapprovedme');?></i> ";
                            }
                            else if (approve == 0 && myStatus != 0) {
                                statusKelas = "<i class='m-l-5 c-orange'>Status : <?php echo $this->lang->line('waitapprovedfriend');?></i> ";
                            }
                            else if (approve == -1) {
                                statusKelas = "<i class='m-l-5 c-red'>Status : <?php echo $this->lang->line('tutornotavailable');?></i>";
                            }
                            else if (approve == -2) {
                                statusKelas = "<i class='m-l-5 c-red'>Status : <?php echo $this->lang->line('tutornotavailable');?></i>";
                            }
                            else if (approve == -3) {
                                statusKelas = "<i class='m-l-5 c-red'>Status : <?php echo $this->lang->line('classexpired');?></i>";
                            }
                            // alert(JSON.stringify(cekstatusdia));
                            no = no+1;
                            if (myStatus == 0 && approve == 0) {
                                buttonnih = "<div class='col-md-6'><a data-toggle='modal' class='m-t-5' request_id='"+request_id+"' href='#Delete'><button class='aa-t btn btn-danger btn-block' request_id='"+request_id+"' title='Hapus'><?php echo $this->lang->line('tolak'); ?></button></a></div><div class='col-md-6'><button class='aa btn btn-success btn-block' id='kaka' request_id='"+request_id+"' gh='"+harga_kelass+"' title='Terima'><?php echo $this->lang->line('terima'); ?></button>";
                                var kotak ="<tr><td><div class='me_timer col-sm-6 col-md-12 col-xs-12'  ><div class='col-md-3'><div class='card picture-list'><div class='card blog-post' style=''><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'></div></div></div><div class='col-md-9'><span class='card-header '><h5 class='c-black' >"+day+", "+date+" - "+time+" <b class='m-l-5'></b>| "+statusKelas+"</h5><hr><h5 class='c-blue'>"+tutor_name+" - "+subject_name+"</h5><h5 class='c-blue' ><?php echo $this->lang->line('topikbelajar'); ?> "+topic+"</h5><h5 class='c-blue'><?php echo $this->lang->line('invitedby'); ?> : "+requester_name+"</h5><h5 class='c-blue'><?php echo $this->lang->line('demandprice'); ?> : "+harga_kelas+"</h5></span></div><div class='col-md-12'>"+buttonnih+"</div></div></td></tr>";
                                var kotaks = "<tr><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td><?php echo $this->lang->line('waitingfriend'); ?></td><td>"+buttonnih+"</td></tr>";
                            }   
                            else{                    
                                if (approve == 1) {
                                    buttonnih = "<button class='btn btn-gray f-16 btn-block' disabled>-</button>";
                                    var kotak ="<tr><td><div class='me_timer col-sm-6 col-md-12 col-xs-12'  ><div class='col-md-3'><div class='card picture-list'><div class='card blog-post' style=''><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'></div></div></div><div class='col-md-9'><span class='card-header '><h5 class='c-black' >"+day+", "+date+" - "+time+" <b class='m-l-5'></b>| "+statusKelas+"</h5><hr><h5 class='c-blue'>"+tutor_name+" - "+subject_name+"</h5><h5 class='c-blue' ><?php echo $this->lang->line('topikbelajar'); ?> "+topic+"</h5><h5 class='c-blue'><?php echo $this->lang->line('invitedby'); ?> : "+requester_name+"</h5><h5 class='c-blue'><?php echo $this->lang->line('demandprice'); ?> : "+harga_kelas+"</h5></span></div><div class='col-md-12'>"+buttonnih+"</div></div></td></tr>";
                                    var kotaks = "<tr><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td><?php echo $this->lang->line('kelasapprove'); ?><a href='<?php echo base_url(); ?>'><?php echo $this->lang->line('home'); ?></a></td><td>"+buttonnih+"</td></tr>";
                                }
                                else if(approve == -1){
                                    buttonnih = "<button class='btn btn-gray f-16 btn-block' disabled>-</button>";
                                    var kotak ="<tr><td><div class='me_timer col-sm-6 col-md-12 col-xs-12'  ><div class='col-md-3'><div class='card picture-list'><div class='card blog-post' style=''><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'></div></div></div><div class='col-md-9'><span class='card-header '><h5 class='c-black' >"+day+", "+date+" - "+time+" <b class='m-l-5'></b>| "+statusKelas+"</h5><hr><h5 class='c-blue'>"+tutor_name+" - "+subject_name+"</h5><h5 class='c-blue' ><?php echo $this->lang->line('topikbelajar'); ?> "+topic+"</h5><h5 class='c-blue'><?php echo $this->lang->line('invitedby'); ?> : "+requester_name+"</h5><h5 class='c-blue'><?php echo $this->lang->line('demandprice'); ?> : "+harga_kelas+"</h5></span></div><div class='col-md-12'></div></div></td></tr>";
                                    var kotaks = "<tr><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td><?php echo $this->lang->line('waitingtutor'); ?></td><td>"+buttonnih+"</td></tr>";
                                }
                                else
                                {
                                    // buttonnih = "<button class='aa btn btn-success btn-block' id='kaka' request_id='"+request_id+"' gh='"+harga_kelas+"' title='Terima'>Terima</button><br><a data-toggle='modal' class='m-t-5' request_id='"+request_id+"' href='#Delete'><button class='aa-t btn btn-danger btn-block' request_id='"+request_id+"' title='Hapus'>Tolak</button></a>";
                                    buttonnih = "<button class='btn btn-gray f-16 btn-block' disabled>-</button>";
                                    var kotak ="<tr><td><div class='me_timer col-sm-6 col-md-12 col-xs-12'  ><div class='col-md-3'><div class='card picture-list'><div class='card blog-post' style=''><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'></div></div></div><div class='col-md-9'><span class='card-header '><h5 class='c-black' >"+day+", "+date+" - "+time+" <b class='m-l-5'></b>| "+statusKelas+"</h5><hr><h5 class='c-blue'>"+tutor_name+" - "+subject_name+"</h5><h5 class='c-blue' ><?php echo $this->lang->line('topikbelajar'); ?> "+topic+"</h5><h5 class='c-blue'><?php echo $this->lang->line('invitedby'); ?> : "+requester_name+"</h5><h5 class='c-blue'><?php echo $this->lang->line('demandprice'); ?> : "+harga_kelas+"</h5></span></div><div class='col-md-12'></div></div></td></tr>";
                                    var kotaks = "<tr><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td><?php echo $this->lang->line('waitingtutor'); ?></td><td>"+buttonnih+"</td></tr>";
                                }
                            }
                            
                            $("#tempatfriendinvitation").append(kotak);
                            $('.datainvitation').DataTable();
                        }
                    }
                    // methodpembayaran = data['response']['method_pembayaran'];
                    // if (methodpembayaran == "beban_sendiri") 
                    // {
                    //     successandsave();
                    // }else{
                    //     ceksaldo();
                    // }
                }
            });


            $(document).on("click", ".aa", function () {       
                requestid = $(this).attr("request_id");
                var hargakelas = parseInt($(this).attr("gh"));  
                // alert(hargakelas);
                if (methodpembayaran == "bagi_rata") 
                {                    
                    // $.ajax({
                    //     url: '<?php echo base_url(); ?>Rest/ceksaldo/access_token/'+tokenjwt,
                    //     type: 'POST',
                    //     data: {
                    //         id_user: iduser
                    //     },               
                    //     success: function(data)
                    //     {             
                    //         balanceuser = data['balance'];    
                    //         // alert(balanceuser);
                    //         // alert(hargakelas);
                    //         if (balanceuser >= hargakelas)
                    //         {
                                // alert(balanceuser);
                                $.ajax({
                                    url: '<?php echo base_url(); ?>/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                                    type: 'POST',
                                    data: {
                                        request_id: requestid,
                                        id_user : iduser
                                    },               
                                    success: function(data)
                                    {                    
                                        if (data['status'] == true) 
                                        {
                                            notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                                            $(".aa").attr('disabled','true');
                                            $(".aa-t").attr('disabled','true');
                                            setTimeout(function(){
                                                window.location.reload();
                                            },3000);
                                            
                                        }
                                    }
                                });
                    //         }
                    //         else
                    //         {
                    //             notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Saldo anda tidak mencukupi. Mohon untuk mengisi saldo terlebih dahulu.");
                    //             $(".aa").attr('disabled','true');
                    //             $(".aa-t").attr('disabled','true');
                    //             setTimeout(function(){
                    //                 window.location.replace('<?php echo base_url(); ?>/Topup');
                    //             },3000);
                    //         }
                    //     }
                    // });
                }
                else
                {
                    $.ajax({
                        url: '<?php echo base_url(); ?>/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                        type: 'POST',
                        data: {
                            request_id: requestid,
                            id_user : iduser
                        },               
                        success: function(data)
                        {                    
                            if (data['status'] == true) 
                            {
                                notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                                $(".aa").attr('disabled','true');
                                $(".aa-t").attr('disabled','true');
                                setTimeout(function(){
                                    window.location.reload();
                                },3000);
                                
                            }
                        }
                    });
                } 
            });

            $(document).on("click", ".aa-t", function () {       
                // alert($(this).attr("request_id"));
                requestid = $(this).attr("request_id");   
                $.ajax({
                    url: '<?php echo base_url(); ?>/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        request_id: requestid,
                        id_user : iduser,
                        answer: "t"

                    },               
                    success: function(data)
                    {                  

                        if (data['status'] == true) 
                        {
                            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Anda telah menolak permintaan group class ini");
                            setTimeout(function(){
                                window.location.reload();
                            },3000);
                        }
                    }
                });

            });


            // function ceksaldo()
            // {
            //     $.ajax({
            //         url: '<?php echo base_url(); ?>/Rest/ceksaldo',
            //         type: 'POST',
            //         data: {
            //             id_user: iduser
            //         },               
            //         success: function(data)
            //         { 
            //             balanceuser = data['balance'];
            //             if (parseInt(balanceuser) > parseInt(hargakelas)) 
            //             {
            //                 successandsave();
            //             }
            //             else
            //             {
            //                 notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Saldo anda tidak mencukupi! Mohon isi dahulu saldo anda");                        
            //             }
            //         }
            //     });
            // }

            function successandsave()
            {
                $.ajax({
                    url: '<?php echo base_url(); ?>/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        request_id: requestid,
                        id_user : iduser

                    },               
                    success: function(data)
                    {                    
                        if (data['status'] == true) 
                        {
                            notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                            window.location.reload();
                        }
                    }
                });
            }

            $('table.display').DataTable( {
                fixedHeader: {
                    header: true,
                    footer: true
                }
            } );
            $('table.displayy').DataTable( {
                fixedHeader: {
                    header: true,
                    footer: true
                }
            } );
            
            $('.data').DataTable();    
        }
    </script>                                