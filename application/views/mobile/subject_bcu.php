<style type="text/css">
	.bp-header:hover
	{
		cursor: pointer;
	}
	.thumb img {
		opacity: 1;		
		-webkit-transition: .3s ease-in-out;
		transition: .3s ease-in-out;
	}
	.thumb:hover img {
		opacity: 0.6;
	}
</style>
<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar'); ?>
</header>

<!-- <div style="background-image: url('../aset/img/headers/blurrr.jpg'); background-size: cover; height: 300px; width: 100%; position: relative;">
	
	<div class="col-md-12" style="position: absolute; z-index: 50; margin-top: 13%;">
	<div class="row">
		<div class="col-md-4">
		<center>
		<div class="card-body">
                            <form class="row" role="form">
                                <div class="col-sm-10">
                                    <div class="form-group fg-line">
                                        <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                        <input type="email" class="form-control input-sm" id="exampleInputEmail2" placeholder="Enter email">
                                    </div>
                                </div>
                                
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-5">Cari</button>
                                </div>
                            </form>
                        </div>
		</center>
		</div>
		</div>
	</div>
	<div class="col-md-12" style="z-index: 100;">
		<div class="row">
			<div class="col-md-3">
				a
			</div>
			<div class="col-md-3">
				a
			</div>
			<div class="col-md-3">
				a
			</div>
			<div class="col-md-3">
				a
			</div>
		</div>
	</div>
</div> -->
<!-- <div style="height: 400px;">
	<center>
		<div class="cari" style="z-index: 10; position: absolute;">
		<input type="text" name="">
		</div>
	</center>
	<img src="../aset/img/headers/patern2.jpg" style="width: 100%; height: 400px; position: relative;">	
</div> -->

<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/side'); 
		$id_user = $this->session->userdata('id_user');
		?>
	</aside>

	<section id="content">
		
		<div class="container">

			<?php if($this->session->flashdata('mes_alert')){ ?>
	        <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <?php echo $this->session->flashdata('mes_message'); ?>
	        </div>
	        <?php } ?>

	        <div class="block-header">
				<h2><?php echo $this->lang->line('title_pg_subject'); ?></h2>

				<!-- <ul class="actions hidden-xs">
					<li>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>
							<?php 
								if($this->session->userdata('status') == 1) 
								{ 
									'first/';
								} 
								else{ 
									echo "#"; 
								} 
							?>"><?php echo $this->lang->line('home'); ?></a></li>
							<li class="active"><?php echo $this->lang->line('subject'); ?></li>
						</ol>
					</li>
				</ul> -->

			</div><!-- akhir block header --> 		

			<?php
			if(empty($allsub))
			{
				echo '<div class="alert alert-info">'.$this->lang->line('no_subjects').'</div>';
			}
			else
			{

				foreach ($allsub as $row => $v) {
					$alltutor = $this->db->query("SELECT ts.usertype_id, ts.user_name, ts.first_name, ts.user_image, tb.* FROM tbl_booking as tb INNER JOIN tbl_user as ts ON tb.id_user=ts.id_user WHERE ts.usertype_id='tutor' AND tb.subject_id='$v[subject_id]'")->result_array();
					?>

					<div class="card" style="margin-top: 4%;">
						<div class="card-header">
							<div class="row">
								<div class="col-md-12 m-b-20">
									<img width="30%" src="<?php echo base_url('aset/img/class_icon/'.$v['iconweb']); ?>">
									<hr>	
								</div>	
								<div class="col-md-12">
																	
									<div class="col-md-12">
										<?php
										foreach ($alltutor as $row_tutor => $va) 
										{
											$tutor_id = $va['id_user'];
										?>
											<div class="col-sm-2 col-md-2 col-xs-12">

												<?php
													if ($this->session->userdata('status')==0) 
													{
                                                    	?>
									            	<div class="card blog-post z-depth-2" style="margin-top: 5%;">
										                <div class="bp-header">
										               		<a href="#">
											                	<div class="thumb showModal" id="card_<?php echo $tutor_id; ?>">							                		
											                    	<img src="<?php echo base_url('aset/img/user/'.$va['user_image']); ?>" alt="" style="height: 22vh; width: 100%;">
											                    </div>  
											                    <div class="bp-title text-center">
											                        <label class="f-16 c-white"><?php echo($va['first_name']); ?></label>							             
											            		</div>
										                    </a>
										                </div>
													</div>
													<?php
													}
													else
													{
													?>
														<div class="card blog-post z-depth-2" style="margin-top: 5%;">
											                <div class="bp-header">
											               		<a href="#">
												                	<div class="thumb showModal" id="card_<?php echo $tutor_id; ?>">				
												                    	<img src="<?php echo base_url('aset/img/user/'.$va['user_image']); ?>" alt="" style="height: 22vh; width: 100%;">
												                    </div>  
												                    <div class="bp-title text-center" style="background-color: #446CB3; opacity: 0.9;">
												                        <label class="f-16 c-white"><?php echo($va['first_name']); ?></label>							                        
												                        <?php
																			$mybooking = $this->db->query("SELECT *FROM tbl_booking WHERE id_user='$id_user'")->result_array();
																			$flag = 0;
																			
																			foreach ($mybooking as $rowbook => $valbook) {
																				if ($tutor_id==$valbook['tutor_id'] && $va['subject_id']==$valbook['subject_id']) {
																					$flag = 1;
																				}
																			}

																			if ($flag==1) {
																				echo '
																				<div class="c-footer m-t-5">
																					
																					<a id="buttonfollow" class=" waves-effect bgm-red c-white btn btn-danger btn-block unfollow_ajax f-12" id="'.$v['subject_id'].'" href="'.base_url('/master/unsavebooking?id_user='.$id_user.'&subject_id='.$va['subject_id'].'&tutor_id='.$va['id_user'].'&subject_name='.$v['subject_name']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('unfollow').'
																					</a>
																				</div>';																
																			}
																			else
																			{
																				echo '
																				<div class="c-footer m-t-5">
																					<a id="buttonfollow" class="waves-effect bgm-green c-white btn btn-success btn-block follow_ajax" href="'.base_url('/master/savebooking?id_user='.$id_user.'&subject_id='.$va['subject_id'].'&tutor_id='.$va['id_user'].'&subject_name='.$v['subject_name']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('follow').'
																					</a>
																				</div>';
																			}
																		?>
												                    </div>
											                    </a>
											                </div>
														</div>
													<?php
													}
													?>
											</div>											
							        
							       	 	<?php 
							    		}
							   		?>
							   		</div>

					            </div>			            
							</div>					
						</div>
					</div>
				<?php
				}
			}
			?>	

			<!-- <div class="card" style="margin-top: 4%;">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-3 m-b-20">
								<img width="100%" src="<?php echo base_url('aset/img/class_icon/binggris.png'); ?>">	
							</div>
							<div class="col-md-9">
								<div class="col-md-3">
					            	<div class="card blog-post z-depth-2" style="margin-top: 5%;">
						                <div class="bp-header">
						               		<a href="#">
							                	<div class="thumb">							                		
							                    	<img src="<?php echo base_url();?>aset/img/user/32.jpg" alt="" style="height: 22vh; width: 100%;">
							                    </div>  
							                    <div class="bp-title text-center">
							                        <label class="f-15">Aroh Claten</label>							                        
							                        <button class="btn bgm-teal btn-block m-t-5">Ikuti</button>
							                    </div>
						                    </a>
						                </div>
									</div>
								</div>
								<div class="col-md-3">
					            	<div class="card blog-post z-depth-2" style="margin-top: 5%;">
						                <div class="bp-header">
						               		<a href="#">
							                	<div class="thumb">							                		
							                    	<img src="<?php echo base_url();?>aset/img/user/32.jpg" alt="" style="height: 22vh; width: 100%;">
							                    </div>  
							                    <div class="bp-title text-center">
							                        <label class="f-15">Aroh Claten</label>								                        
							                        <button class="btn bgm-teal btn-block m-t-5">Ikuti</button>
							                    </div>
						                    </a>
						                </div>
									</div>
								</div>       
								<div class="col-md-3">
					            	<div class="card blog-post z-depth-2" style="margin-top: 5%;">
						                <div class="bp-header">
						               		<a href="#">
							                	<div class="thumb">							                		
							                    	<img src="<?php echo base_url();?>aset/img/user/32.jpg" alt="" style="height: 22vh; width: 100%;">
							                    </div>  
							                    <div class="bp-title text-center">
							                        <label class="f-15">Aroh Claten</label>								                        
							                        <button class="btn bgm-teal btn-block m-t-5">Ikuti</button>
							                    </div>
						                    </a>
						                </div>
									</div>
								</div>       
								<div class="col-md-3">
					            	<div class="card blog-post z-depth-2" style="margin-top: 5%;">
						                <div class="bp-header">
						               		<a href="#">
							                	<div class="thumb">							                		
							                    	<img src="<?php echo base_url();?>aset/img/user/32.jpg" alt="" style="height: 22vh; width: 100%;">
							                    </div>  
							                    <div class="bp-title text-center">
							                        <label class="f-15">Aroh Claten</label>								                        
							                        <button class="btn bgm-teal btn-block m-t-5">Ikuti</button>
							                    </div>
						                    </a>
						                </div>
									</div>
								</div>                       
				            </div>
			            </div>			            
					</div>					
				</div>
			</div>
 -->

			<!-- Modal Default -->	
			<div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content" style=" background: #f7f7f7;">
						<div class="modal-header bgm-teal">														
							<div class="pull-left">
								<h3 class="modal-title c-white"><?php echo $this->lang->line('profil'); ?></h3>
								
							</div>
							<div class="pull-right">										
								<button type="button" class="btn bgm-white" data-dismiss="modal">X</button>
							</div>
						</div>

						<div class="modal-body" style="margin-top:30px;">
							<div class="col-sm-12">

								<div class="col-sm-4">
									<img class="z-depth-3-bottom" id="modal_image" width="100%" height="100%" alt=""><br><br>
									<div class="card" style="padding: 10px;">
										<label style="font-size:15px;"><?php echo $this->lang->line('tutorname');?></label>
										<hr style="margin-top: -1px;">
										<p style="font-size:13px; margin-top: 5px; overflow:hidden; word-wrap: break-word;" id="modal_user_name"></p>																						
									</div>
									<div class="card" style="padding: 10px; margin-top: -15px;">
										<label style="font-size:15px;"><?php echo $this->lang->line('gender');?></label>
										<hr style="margin-top: -1px;">
										<label style="font-size:13px; margin-top: -5px;"  id="modal_gender"></label>
									</div>
								</div>

								<div class="col-sm-8" style="margin-top:0px;">												
									<!-- <div class="col-sm-12">
										<label style="font-size:25px;" id="modal_first_name"></label> 																							
									</div>
									<div class="col-sm-12">																					
										<label class="f-15" id="modal_gender"></label>
										<hr>
									</div>
-->
									<div class="col-sm-12">
										<div class="card" style="padding: 10px; margin-top: 10px;">
											<label style="font-size:16px;"><?php echo $this->lang->line('description_tutor');?></label>
											<hr style="margin-top: -1px;">
											<label id="modal_self_desc" style="width:100%; text-align:justify;">
											</label><br><br>
										</div>
									</div>
									<div class="col-lg-12 m-l-25">
										<div class="col-sm-1" style="margin-left:-20px;">
											<img src="<?php echo base_url('aset/img/icons/topi.png') ?>" width="30px" height="30px" alt="">
										</div>
										<div id="modal_eduback" class="col-sm-5 m-t-5">													
										</div>
										<div class="col-sm-1" style="margin-left:-30px;">
											<img src="<?php echo base_url('aset/img/icons/waktu.png') ?>" width="27px" height="27px" alt="">
										</div>
										<div id="modal_teachexp" class="col-sm-6 m-t-5">
										</div>
									</div>																					

									<div class="col-lg-12 m-t-10">
										<div class="col-sm-12 m-t-5">
											<div class="table-responsive">		
											<center>
												<table style="height:10px;" class="table">
													<thead>
														<center>	
															<th class="bgm-teal c-white">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $this->lang->line('competency');?></th>
														<th class="bgm-teal c-white"></th>																
														</center>
													</thead>
													<tbody id="modal_competency" style="height: 10px;">													
													</tbody>
												</table>
											</center>
											</div>
										</div>
									</div>

								</div><!-- AKHIR col-sm-8 -->

							</div><!-- AKHIR col-sm-12 -->
						</div><!-- AKHIR modal-body -->

						<div class="modal-footer m-r-30" >
						</div>    				
						<br><br><br>

					</div><!-- AKHIR modal-content -->
				</div><!-- AKHIR modal-dialog -->
			</div>
			<!-- AKHIR modal fade -->

		</div>

	</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
	
	$('.showModal.thumb').click(function(){
		var ids = $(this).attr('id');
		ids = ids.substr(5,10);
		$.get('<?php echo base_url(); ?>master/onah?id_user='+ids,function(hasil){
			hasil = JSON.parse(hasil);
			$('.modal_approve').attr('id',ids);
			$('.modal_decline').attr('id',ids);
			$('#modal_image').attr('src','<?php echo base_url(); ?>aset/img/user/'+hasil['user_image']);			
			if (hasil['self_description'] == '') {
				$('#modal_self_desc').html("<?php echo $this->lang->line('nodescription'); ?>");
			}
			else
			{
				$('#modal_self_desc').html(hasil['self_description']);
			}
			$('#modal_user_name').html(hasil['user_name']);
			$('#modal_birthplace').html(hasil['user_birthplace']);
			$('#modal_birthdate').html(hasil['user_birthdate']);
			$('#modal_age').html(hasil['user_age']);
			$('#modal_callnum').html(hasil['user_callnum']);
			// $('#modal_gender').html(hasil['user_gender']);
			// alert(hasil['user_gender']);
			if (hasil['user_gender'] == 'Male') {
				$('#modal_gender').html("<?php echo $this->lang->line('male');?>");
			}
			else if(hasil['user_gender'] == 'Female')
			{
				$('#modal_gender').html("<?php echo $this->lang->line('women');?>");
			}
			$('#modal_religion').html(hasil['user_religion']);
			$('#modal_competency').html(hasil['competency']);
			$('#modal_eduback').html("Pendidikan Terakhir "+hasil['last_education']);
			$('#modal_teachexp').html("Pengalaman Mengajar "+hasil['year_experience'] +" Tahun" );
			if(hasil['competency'] != ''){
				for (var i = 0; i<hasil['competency'].length; i++) {
					$('#modal_competency').append('<tr>'+
						'<td>'+hasil['competency'][i]['subject_name']+'</td>'+
						'<td>'+hasil['competency'][i]['jenjang_name']+'  '+hasil['competency'][i]['jenjang_level']+'</td>'+						
						// '<td>'+hasil['education_background'][i]['institution_address']+'</td>'+
						// '<td>'+hasil['education_background'][i]['graduation_year']+'</td>'+
						'</tr>');
				}
			}
			$('#modalDefault').modal('show');
		});
		$('.modal_approve').click(function(){
			var newids = $(this).attr('id');
			$.get('<?php echo base_url(); ?>first/approveTheTutor/'+newids,function(s){
				location.reload();
			});
		});
		$('.modal_decline').click(function(){
			var newids = $(this).attr('id');
			$.get('<?php echo base_url(); ?>first/declineTheTutor/'+newids,function(s){
				location.reload();
			});
		});

		$("#show1").click(function(){            
			$("#contoh2").css('display','none');
			$("#contoh1").css('display','block');
		});
		$("#show2").click(function(){            
			$("#contoh1").css('display','none');
			$("#contoh2").css('display','block');    
		});


	});
</script>
