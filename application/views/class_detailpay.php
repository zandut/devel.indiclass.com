<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container invoice">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Rincian Pembelian Kelas</h2>
            </div>         

            <?php            
            if(empty($classid))
            {
                echo '<div class="alert alert-info">Tidak ada data</div>';
            }
            else
            {
                $getclass = $this->db->query("SELECT tc.*, tcp.* FROM tbl_class as tc INNER JOIN tbl_class_price as tcp ON tc.class_id=tcp.class_id WHERE tc.class_id='$classid'")->result_array();
                foreach ($getclass as $row => $v) {
                    $idtutor            = $v['tutor_id'];
                    $tutorname          = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$idtutor'")->row_array()['user_name'];
                    $hargakelas         = number_format($v['harga_cm'], 0, ".", ".");

                    $userutc            = $this->session->userdata('user_utc');
                    $server_utc         = $this->Rumus->getGMTOffset();
                    $interval           = $userutc - $server_utc;
                    $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$v['start_time']);
                    $tanggal->modify("+".$interval ." minutes");
                    $tanggal            = $tanggal->format('Y-m-d H:i:s');
                    $datelimit          = date_create($tanggal);
                    $dateelimit         = date_format($datelimit, 'd/m/y');
                    $harilimit          = date_format($datelimit, 'd');
                    $hari               = date_format($datelimit, 'l');
                    $tahunlimit         = date_format($datelimit, 'Y');
                    $waktulimit         = date_format($datelimit, 'H:s');                

                    $thisBeginDay       = DateTime::createFromFormat('Y-m-d H:i:s',$v['start_time']);
                    $thisBeginDay->modify("+$interval minute");
                    $thisBeginDay       = $thisBeginDay->format('Y-m-d H:i:s');
                    
                    $thisEndDay         = DateTime::createFromFormat('Y-m-d H:i:s',$v['finish_time']);
                    $thisEndDay->modify("+$interval minute");
                    $thisEndDay         = $thisEndDay->format('Y-m-d H:i:s');

                    $datelimit          = $dateelimit;
                    $sepparatorlimit    = '/';
                    $partslimit         = explode($sepparatorlimit, $datelimit);
                    $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
            ?>
            <div class="card">
                <div class="card-header ch-alt">
                    <div class="pull-left f-14">No <?php echo $this->lang->line('transaksi'); ?> <?php echo $trx_id; ?></div>
                    <div class="pull-right f-14"><?php echo $this->lang->line('transaksi'); ?> <?php echo $this->lang->line('tanggal'); ?> <?php echo $v['created_at']; ?></div><br>
                </div>
                
                <div class="card-body card-padding" style="background: white;">
                    <div class="row m-b-25 text-center">
                        
                        <div class="col-xs-12">
                            <div class="i-to">
                                <p class="c-gray"><?php echo $this->lang->line('amountpayment'); ?></p>
                                <div style="background: #edecec; width: 30%; height: 50px; margin-left: 35%;">                                    
                                          
                                    <label style="margin-top: 10px; font-size: 24px;">Rp. <?php echo $hargakelas; ?></label>                              
                                </div>                                
                                
                                <h6 class="c-gray m-t-15"><?php echo $this->lang->line('expiredtext'); ?></h6>
                                <label style="font-size: 18px;"><?php echo $hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit. ' Pukul '.$waktulimit ?></label>
                                <hr>
                                
                                <?php 
                                        $cekconfirmpayment = $this->db->query("SELECT * FROM log_trf_confirmation WHERE trx_id='$order_id'")->row_array();
                                        if (empty($cekconfirmpayment)) {                                                                                            
                                        ?>
                                        <a href="<?php echo base_url('ConfirmPayment?order_id='.$v['order_id'].'&trans=knf');?>"><button class="btn btn-lg bgm-blue"><?php echo $this->lang->line('konfirmasipembayaran'); ?></button></a>
                                        <!-- <button class="btn btn-lg bgm-blue" type="submit">Konfirmasi Pembayaran</button> -->
                                        <!-- </form> -->
                                        <?php 
                                        }
                                    ?>

                                <span class="text-muted">
                                    <address>
                                        <?php echo $this->lang->line('bedatransfer'); ?>
                                    </address>                                     
                                    <center>
                                    <div class="table-responsive" style="width: 60%;">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>                                                                                                        
                                                    <th>Nama Kelas</th>
                                                    <th>Deskripsi Kelas</th>
                                                    <th>Nama Tutor</th>
                                                    <th>Waktu Mulai</th>
                                                    <th>Waktu Selesai</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $v['name']; ?></td>
                                                    <td><?php echo $v['description']; ?></td>
                                                    <td><?php echo $tutorname; ?></td>
                                                    <td><?php echo $thisBeginDay; ?></td>
                                                    <td><?php echo $thisEndDay; ?></td>
                                                </tr>                                                   
                                            </tbody>
                                        </table>
                                    </div>

                                    </center>
                                </span>

                                
                            </div>
                        </div>
                        
                    </div>
                    <hr>
                    <p class="c-gray text-center"><?php echo $this->lang->line('pembayaranmelalui'); ?></p>
                    <div class="clearfix"></div>
                    
                    <div class="row m-t-25 p-0 m-b-25">

                    	<div class="col-xs-1"></div>
                        <div class="col-xs-2">
                            <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                <div class="c-white m-b-5"><img style="margin-left: -20%;" src="<?php echo base_url();?>aset/images/banklogo/bca1.png"></div>
                                <h2 class="m-0 c-gray f-12">Bank BCA, Jakarta
                                <h2 class="m-0 c-gray f-12">254 689 2003 </h2>
                                <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                            </div>
                        </div>
                        
                        <div class="col-xs-2">
                            <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                <div class="c-white m-b-5"><img style="margin-left: -20%;" src="<?php echo base_url();?>aset/images/banklogo/bni1.png"></div>
                                <h2 class="m-0 c-gray f-12">Bank BNI, Jakarta
                                <h2 class="m-0 c-gray f-12">0381486211 </h2>
                                <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                            </div>
                        </div>
                        
                        <div class="col-xs-2">
                            <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                <div class="c-white m-b-5"><img style="margin-left: -20%;" src="<?php echo base_url();?>aset/images/banklogo/mandiri1.png"></div>
                                <h2 class="m-0 c-gray f-12">Bank Mandiri, Jakarta
                                <h2 class="m-0 c-gray f-12">1290010901359 </h2>
                                <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                            </div>
                        </div>
                        
                        <div class="col-xs-2">
                            <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                <div class="c-white m-b-5"><img style="margin-left: -25%;" src="<?php echo base_url();?>aset/images/banklogo/bri1.png"></div>
                                <h2 class="m-0 c-gray f-12">Bank BRI, Jakarta
                                <h2 class="m-0 c-gray f-12">580 475 900 684 136 </h2>
                                <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                            </div>
                        </div>

                        <div class="col-xs-2">
                            <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                <div class="c-white m-b-5"><img style="margin-left: -25%;" src="<?php echo base_url();?>aset/images/banklogo/cimb1.png"></div>
                                <h2 class="m-0 c-gray f-12">Bank CIMB, Jakarta
                                <h2 class="m-0 c-gray f-12">5454 2212 3333 </h2>
                                <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                            </div>
                        </div>
                        <div class="col-xs-1"></div>

                    </div>                  
                    <hr>
                    <div class="clearfix"></div>
                    
                    <!-- <div class="p-25">
                        <h4 class="c-green f-400"><?php echo $this->lang->line('notes'); ?> :</h4>                        
                        <blockquote class="m-t-5">
                            <p class="c-gray f-12"><?php echo $this->lang->line('notesdes'); ?> <?php echo $waktulimit; ?> WIB <?php echo $this->lang->line('hari'); ?> <?php echo $hari; ?>, <?php echo $harilimit.' '. $bulanlimit. ' '.$tahunlimit ?> <?php echo $this->lang->line('notpaid'); ?></p>
                        </blockquote>                                                                        
                    </div> -->
                </div>
                
                <footer class="p-20">
                    <a href="<?php echo base_url();?>"><button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;"><?php echo $this->lang->line('kembali'); ?></button></a>
                </footer>
            </div>
            <?php 
            }
            }
            ?>

        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>