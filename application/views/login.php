<?php

$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){

    ?>

    <div class="container">

	    <div class="col-sm-12">

	        <div class="col-sm-8" style="margin-top:2%;">

	            <div class="left">
	                <div class="col-sm-8" >
	                <center>
	                    <img id="logologin" src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" style="margin-left:-30px;" height="5%" width="120%" alt="">
	                    </center>
	                </div>
	                <div class="col-lg-8">

	                    <p class="hidden-xs" style="font-size: 26px; color: #5f9ea0">Classroom in your hand</p> 
	                    <br>
	                    <!-- <div class="col-sm-8"> -->
	                    <div class="col-sm-5 bgm-indigo hidden-xs">
	                        <img src="<?php echo base_url(); ?>aset/img/icons/appstore.png" style="margin-left:-15px;" width="120%" height="110%" alt="App Store">
	                    </div>
	                    <div class="col-sm-5 bgm-orange hidden-xs" style="margin-left:10px;">
	                        <img src="<?php echo base_url(); ?>aset/img/widgets/btn-googleplay.png" style="margin-left:-15px;" width="120%" height="110%" alt="Google Play">
	                    </div>
	                    <!-- </div> -->
	                </div>
	                <div class="col-sm-12">
	                    <center>
	                        <p class="hidden-xs" style="color:#ffffff; font-size:10.44px; top: 5%; text-align:justify; font-family:tahoma;">
	                            <?php  echo $this->lang->line('description_loginfooter'); ?>
	                        </p>
	                    </center>
	                </div>
	            </div>
	        </div>    
	        <div class="right">
	            <div class="col-sm-4" style="margin-top:0%; ">
	                <div class="card">
	                    <div class="card-header">
	                        <p class="hidden-xs" style="font-size: 23px; color: #009688; text-align:center;"><?php echo $this->lang->line('wlogin'); ?> Classmiles</p>
	                        <?php if($this->session->flashdata('mes_alert')){ ?>
	                        <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
	                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                          <?php echo $this->session->flashdata('mes_message'); ?>
	                      </div>
	                      <?php } ?>
	                      <!-- <p style="font-size: 23px; color: #009688; text-align:center;"><?php //echo $this->sessions->flashdata('msgLogout'); ?></p> -->
	                      <hr class="hidden-xs">
	                  </div>

	                  <div class="card-body card-padding">
	                    <form role="form" action="<?php echo base_url('Master/aksi_login'); ?>" method="post">
	                        <div class="form-group fg-float">
	                            <div class="fg-line">
	                                <input type="text" name="email" autofocus required class="input-sm form-control fg-input" style="color:#009688;">
	                                <label class="fg-label" style="color:#009688;">Email</label>
	                            </div>
	                        </div>
	                        <br>
	                        <div class="form-group fg-float">
	                            <div class="fg-line">
	                                <input type="password" required name="kata_sandi" class="input-sm form-control fg-input" style="color:#009688;">
	                                <label class="fg-label" style="color:#009688;"><?php echo $this->lang->line('password'); ?></label>
	                            </div>
	                        </div>
	                        <br>
	                        <div class="checkbox">
	                            <!-- <label style="color:#009688;">
	                                <input type="checkbox" value="">
	                                <i class="input-helper"></i>
	                                Ingat saya
	                            </label> -->
	                            
	                        </div>
	                        <br>
	                        <button type="submit" class="btn btn-primary btn-block"><?php echo $this->lang->line('login'); ?></button>
	                        <br>
	                        <br>
	                        <center><label class="fg-label"><?php echo $this->lang->line('notaccountlogin'); ?></label><a href="<?php echo base_url(); ?>first/Register"> <?php echo $this->lang->line('here'); ?></a></center>
	                        <center><label style="color:#ff9000;"><a href="<?php echo base_url(); ?>first/forgot"><?php echo $this->lang->line('forgotpassword'); ?></a></label></center><hr>
	                        <button type="button" id="f_signInbtn" class="btn bgm-facebook waves-effect btn-icon-text btn-block"><i class="zmdi zmdi-facebook"></i> <?php echo $this->lang->line('withfacebook'); ?></button>
	                        <button id="g_signInbtn" type="button" class="btn bgm-google waves-effect btn-icon-text btn-block"><i class="zmdi zmdi-google"></i> <?php echo $this->lang->line('withgoogle'); ?></button>
	                        <!-- <div class="g-signin2" data-onsuccess="onSignIn"></div> -->
	                    </form>
	                </div>                            
	            </div>
	            <div class="col-xs-4" style="margin-top:1%; width:100%;">
	                <div class="col-xs-6" style="margin-top:3%; width:50%; bottom:10px; ">	                    
	                    <button id="btn_setindonesia" <?php if($this->session->userdata('lang') == 'indonesia'){ echo "disabled = 'disabled'"; }; ?> class="btn btn-primary btn-block">Indonesia</button>		                    
	                </div>
	                <div class="col-xs-6" style="margin-top:3%; width:50%; bottom:10px; ">	                    
	                    <button id="btn_setenglish" <?php if($this->session->userdata('lang') == 'english'){ echo "disabled = 'disabled'"; }; ?> class="btn btn-warning btn-block">English</button>		                    
	                </div>
	            </div>                             
	        </div>
	    </div>    
	    
	</div>

	</div><!-- akhir container -->
	<form hidden method="POST" action='<?php echo base_url(); ?>master/google_login' id='google_form'>
	    <!-- <input type="text" name="email" id="google_email"> -->
	    <textarea name="google_data" id="google_data"></textarea>
	    <input type="text" name="usertype_id" value="student">
	</form>
	<form hidden method="POST" action='<?php echo base_url(); ?>master/facebook_login' id='facebook_form'>
	    <!-- <input type="text" name="email" id="google_email"> -->
	    <textarea name="fb_data" id="fb_data"></textarea>
	    <input type="text" name="usertype_id" value="student">
	</form>
	<script type="text/javascript">
	    $(document).ready(function(){
	        $('#g_signInbtn').click(function(){
	            auth2.signIn().then(function() {
	                var data = auth2.currentUser.get().getBasicProfile();
	                // $('#google_email').val(data.getEmail());
	                $('#google_data').html(JSON.stringify(data));
	                $('#google_form').submit();
	                console.log(auth2.currentUser.get().getId());
	            });
	        });
	        $('#f_signInbtn').click(function(){
	            FB.login(function(response){
	                if(response.status == 'connected'){
	                    var new_data = {};
	                    FB.api('/me', { locale: 'en_US', fields: 'name, email' }, function(response) {
	                        console.log(JSON.stringify(response));
	                        new_data['name'] = response.name;
	                        new_data['email'] = response.email;
	                        FB.api('/me/picture',{type: 'large'} ,function(response) {
	                            console.log(JSON.stringify(response));
	                            new_data['image'] = response.data.url;
	                            new_data = JSON.stringify(new_data);
	                            $('#fb_data').html(new_data);
	                            $('#facebook_form').submit();
	                        });
	                    });
	                }
	            }, {scope: 'public_profile,email'});
	        });

	     /*   function checking(){
	            if(auth2.isSignedIn.get()){
	                var data = auth2.currentUser.get().getBasicProfile();
	                console.log(data.getName());

	            }
	        }*/

	        function inDulu() {
	            auth2.signIn().then(function() {
	                console.log(auth2.currentUser.get().getId());
	            });
	        }

	        $('#btn_setindonesia').click(function(e){
	            e.preventDefault();	            	            
	            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });	            
	        });
	        $('#btn_setenglish').click(function(e){
	            e.preventDefault();
	            $.get('<?php echo base_url('set_lang/english'); ?>',function(hasil){ location.reload(); });
	        });

	    });

	</script>

	<?php
}
else
{
    ?>
    <style type="text/css">
    #classroom{
        font-size:26px; 
        color: #5f9ea0;
    }

    #welcomeclassmiles{
        font-size: 23px; 
        color: #009688; 
        text-align:center;
    }

    @media screen and (max-width: 1199px){
        #utama{
            width: 92%;
            margin-left: 4%;            
        }
        #utamakiri{
            width: 58%;           
            float: left;
        }
        #utamakanan{
            width: 37%;
            float:left;
            margin-left: 2.5%;
        }

        #classroom{
            font-size: 26px;
        }

        #welcomeclassmiles{
            font-size: 22px;
        }

        #utamafooter{
            width: 92%;
            margin-left: 4%;             
        }
        #utamakirifooter{
            width: 58%;
            float: left;
        }
        #utamakananfooter{
            width: 42%;
            float:left;
        }
        #btnindo{
            margin-left: 8px;
            width: 47%;
            float: left;
        }    
        #btnenglish{
            margin-left: 5px;
            width: 47%;  
            float: left;          
        }
    }


    @media screen and (max-width: 768px){
        #utamakiri{
            width: 90%;
            margin-left: 20%;

        }
        #utamakanan{
            width: 90%;
            margin-left: 1%;
            margin-top: 5%;
        }

        #appstore{
            display: none;
        }

        #classroom{
            font-size: 23px;
        }

        #welcomeclassmiles{
            font-size: 21px;
        }

        #playstore{
            display: none;
        }
        #btnindo{
            width: 100%;    
        }    
        #btnenglish{
            width: 100%;
        }
        #utamakirifooter{
            display: none;
        }
        #utamafooter{
            width: 90%;
        }
        #utamakananfooter{
            margin-left: 7%;
            width: 87%;
            margin-bottom: 2%;
        }
    }

    @media screen and (max-width: 660px){
        #utamakiri{
            margin-left: 16%;
        }

        #utamakirifooter{
            display: none;
        }

        #classroom{
            font-size: 21px;
        }

        #welcomeclassmiles{
            font-size: 18px;
        }

        #utamafooter{
            width: 90%;
        }

        #utamakananfooter{
            margin-left: 3%;
            width: 87%;
            margin-bottom: 2%;
        }
    }
    
</style>
<div class="container">

    <div class="col-lg-12" id="utama" style="margin-top:4%;">        
        <div class="col-lg-8" id="utamakiri">

            <div class="pull-left" style="margin-left:-15px; margin-top: 10%;" >

                <div class="col-lg-12">
                    <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" style="margin-left:-50px;" height="10%" width="80%" alt=""></a>
                </div>
                <div class="col-lg-12" style="margin-top: 1vh;">
                    <p id="classroom">Classroom in your hand</p> 
                </div>
                <div class="col-lg-12" height="100px" style="margin-top: 2vh;">
                    <img id="appstore" class="hidden-xs" src="<?php echo base_url(); ?>aset/img/appstore.png" width="25%" height="65px" style="margin-right: 10px;">
                    <img id="playstore" class="hidden-xs" src="<?php echo base_url(); ?>aset/img/playstore.png" width="25%" height="65px">
                </div>
            </div>

        </div>

        <div class="col-lg-4" id="utamakanan">

            <div class="pull-right">

                <div class="card">
                    <div class="card-header">
                        <p id="welcomeclassmiles"><?php echo $this->lang->line('wlogin'); ?> Classmiles</p>
                        <?php if($this->session->flashdata('mes_alert')){ ?>
                        <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <?php echo $this->session->flashdata('mes_message'); ?>
                        </div>
                        <?php } ?>
                        <!-- <p style="font-size: 23px; color: #009688; text-align:center;"><?php //echo $this->sessions->flashdata('msgLogout'); ?></p> -->
                        <hr>
                    </div>

                    <div class="card-body card-padding">
                        <form role="form" action="<?php echo base_url('Master/aksi_login'); ?>" method="post">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <input type="text" autofocus name="email" required class="input-sm form-control fg-input" style="color:#009688;">
                                    <label class="fg-label" style="color:#009688;">Email</label>
                                </div>
                            </div>
                            <br>
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <input type="password" required name="kata_sandi" class="input-sm form-control fg-input" style="color:#009688;">
                                    <label class="fg-label" style="color:#009688;"><?php echo $this->lang->line('password'); ?></label>
                                </div>
                            </div>
                            <br>
                            <div class="checkbox">
                                <label style="color:#009688;">
                                    <input type="checkbox" value="V" name="bapuks">
                                    <i class="input-helper"></i>
                                    <?php echo $this->lang->line('remember'); ?> 
                                </label>                            
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary btn-block"><?php echo $this->lang->line('login'); ?></button>
                            <br>
                            <br>                               
                            <center><label class="fg-label"><?php echo $this->lang->line('notaccountlogin'); ?></label><a href="<?php echo base_url(); ?>first/Register"> <?php echo $this->lang->line('here'); ?></a></center>
                            <center><label style="color:#ff9000;"><a href="<?php echo base_url(); ?>first/forgot"><?php echo $this->lang->line('forgotpassword'); ?></a></label></center><hr>
                            <button type="button" id="f_signInbtn" class="btn bgm-facebook waves-effect btn-icon-text btn-block"><i class="zmdi zmdi-facebook"></i> <?php echo $this->lang->line('withfacebook'); ?></button>
                            <button id="g_signInbtn" type="button" class="btn bgm-google waves-effect btn-icon-text btn-block"><i class="zmdi zmdi-google"></i> <?php echo $this->lang->line('withgoogle'); ?></button>
                            <!-- <div class="g-signin2" data-onsuccess="onSignIn"></div> -->                        
                        </form>
                    </div>
                </div>
            </div>

        </div>             
    </div>

    <div class="col-lg-12" id="utamafooter">
        <div style="margin-top: -13px;">
            
            <div class="col-lg-8" id="utamakirifooter" style="margin-top: 5px;"> 
                <!-- <p class="hidden-xs" style="color:#ffffff; font-size:10.44px; top: 5%; text-align:justify; font-family:tahoma;"><?php  echo $this->lang->line('description_loginfooter'); ?></p> -->
            </div>

            <div class="col-lg-4" id="utamakananfooter">
                <div class="col-lg-6" style="margin-top: 3%; margin-bottom: 2%;" id="btnindo">
                    <button id="btn_setindonesia" <?php if($this->session->userdata('lang') == 'indonesia'){ echo "disabled = 'disabled'"; }; ?> class="btn btn-primary btn-block">Indonesia</button>
                </div>
                <div class="col-lg-6" style="margin-top: 3%; margin-bottom: 2%;" id="btnenglish">
                    <button id="btn_setenglish" <?php if($this->session->userdata('lang') == 'english'){ echo "disabled = 'disabled'"; }; ?> class="btn btn-warning btn-block">English</button>
                </div>
            </div>
        </div>
    </div> 

</div>

	</div><!-- akhir container -->
	<form hidden method="POST" action='<?php echo base_url(); ?>master/google_login' id='google_form'>
	    <!-- <input type="text" name="email" id="google_email"> -->
	    <textarea name="google_data" id="google_data" style="width: 900px;"></textarea>
	    <input type="text" name="usertype_id" value="student">
	</form>
	<form hidden method="POST" action='<?php echo base_url(); ?>master/facebook_login' id='facebook_form'>
	    <!-- <input type="text" name="email" id="google_email"> -->
	    <textarea name="fb_data" id="fb_data"></textarea>
	    <input type="text" name="usertype_id" value="student">
	</form>
	<script type="text/javascript">
	    $(document).ready(function(){
	        $('#g_signInbtn').click(function(){
	            auth2.signIn().then(function() {
	                var data = auth2.currentUser.get().getBasicProfile();
	                // $('#google_email').val(data.getEmail());
	                $('#google_data').html(JSON.stringify(data));
	                $('#google_form').submit();
	                console.log(auth2.currentUser.get().getId());
	            });
	        });
	        $('#f_signInbtn').click(function(){
	            FB.login(function(response){
	                if(response.status == 'connected'){
	                    var new_data = {};
	                    FB.api('/me', { locale: 'en_US', fields: 'name, email' }, function(response) {
	                        console.log(JSON.stringify(response));
	                        new_data['name'] = response.name;
	                        new_data['email'] = response.email;
	                        FB.api('/me/picture',{type: 'large'} ,function(response) {
	                            console.log(JSON.stringify(response));
	                            new_data['image'] = response.data.url;
	                            new_data = JSON.stringify(new_data);
	                            $('#fb_data').html(new_data);
	                            $('#facebook_form').submit();
	                        });
	                    });
	                }
	            }, {scope: 'public_profile,email'});
	        });

	     /*   function checking(){
	            if(auth2.isSignedIn.get()){
	                var data = auth2.currentUser.get().getBasicProfile();
	                console.log(data.getName());

	            }
	        }*/

	        function inDulu() {
	            auth2.signIn().then(function() {
	                console.log(auth2.currentUser.get().getId());
	            });
	        }			

	        $('#btn_setindonesia').click(function(e){
	            e.preventDefault();	            
	            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
	        });
	        $('#btn_setenglish').click(function(e){
	            e.preventDefault();	            
	            $.get('<?php echo base_url('set_lang/english'); ?>',function(hasil){ location.reload(); });
	        });

	    });

	</script>
	<?php
}



?>