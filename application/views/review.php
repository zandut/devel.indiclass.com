<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="block-header">
                <h2>Ulasan</h2>
                
            </div> <!-- akhir block header    --> 

            <div class="col-md-12">
                <div class="card m-t-20">
                    <div class="card-header">                                                
                        <ul class="tab-nav pull-left" role="tablist">                                                                      
                            <li role="presentation" class="pull-left active">
                                <a class="col-xs-4" href="#tab-2" id="friendinvitaion" aria-controls="tab-2" role="tab" data-toggle="tab">
                                    Menunggu ulasan
                                </a>
                            </li>
                            <li role="presentation" class="pull-left">
                                <a class="col-xs-4" href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">
                                    Ulasan saya
                                </a>
                            </li>                            
                        </ul>
                        <hr style="margin-top: 2%;">
                    </div>


                    <div class="card-body card-padding" style="margin-top: -2%;">                   
                        
                        <div class="tab-content p-20">

                            <div role="tabpanel" class="tab-pane animated fadeIn in active" id="tab-2">
                                <div class="card-body card-padding">
                                    <form class="row" role="form">
                                        <div class="col-sm-1">
                                            <div class="form-group fg-line">
                                                <label class="f-15" style="margin-bottom: 1%;">Filter</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-3">
                                            <div class="form-group fg-line">
                                                <select id="month_waiting" required name="month_waiting" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    <option disabled selected value='0'>Semua Waktu</option>
                                                    <option value="1">1 Hari terakhir</option>
                                                    <option value="2">7 Hari terakhir</option>
                                                    <option value="3">1 Bulan terakhir</option>
                                                    <option value="4">3 Bulan terakhir</option>
                                                </select>
                                            </div>
                                        </div>  

                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-5">                                            
                                            <div class="input-group">
                                                <div class="fg-line">
                                                    <input type="text" class="form-control p-5" placeholder="Kelas / Nama Tutor / Pelajaran" style="border: 1px solid #EEEEEE;">
                                                </div>
                                                <span class="input-group-addon last" style="background-color: #EEEEEE; cursor: pointer;"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="card m-t-20" style="display: none;">
                                        <div class="card-header ch-alt">
                                            <center>
                                                <h2 class="f-20 p-10">Tidak ada ulasan</h2>
                                            </center>
                                        </div>
                                    </div>

                                    <div id="box_inputulasan">
                                        
                                    </div>

                              </div>  
                            </div>
                            
                            <div role="tabpanel" class="tab-pane animated fadeIn in" id="tab-1">
                                <div class="card-body card-padding">
                                    <form class="row" role="form">
                                        <div class="col-sm-1">
                                            <div class="form-group fg-line">
                                                <label class="f-15" style="margin-bottom: 1%;">Filter</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-3">
                                            <div class="form-group fg-line">
                                                <select id="month_all" required name="month_all" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    <option disabled selected value='0'>Semua Waktu</option>
                                                    <option value="1">1 Hari terakhir</option>
                                                    <option value="2">7 Hari terakhir</option>
                                                    <option value="3">1 Bulan terakhir</option>
                                                    <option value="4">3 Bulan terakhir</option>
                                                </select>
                                            </div>
                                        </div>  

                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-5">                                            
                                            <div class="input-group">
                                                <div class="fg-line">
                                                    <input type="text" class="form-control p-5" placeholder="Kelas / Nama Tutor / Pelajaran" style="border: 1px solid #EEEEEE;">
                                                </div>
                                                <span class="input-group-addon last" style="background-color: #EEEEEE; cursor: pointer;"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="card m-t-20" style="display: none;">
                                        <div class="card-header ch-alt">
                                            <center>
                                                <h2 class="f-20 p-10">Tidak ada ulasan</h2>
                                            </center>
                                        </div>
                                    </div>

                                    <div id="box_viewulasan">
                                        
                                    </div>

                              </div>  
                            </div>

                        </div>
                    </div>   
                </div>            
            </div>            

    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<!-- <div class="page-loader bgm-blue">
    <div class="preloader pls-white pl-xxl">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p class="c-white f-14">Please wait...</p>
    </div>
</div> -->

<script type="text/javascript">

    $.ajax({
        url: '<?php echo base_url(); ?>Rating/polling_unrate/',
        type: 'POST',                
        success: function(response)
        {       
            result = JSON.parse(response);
            if (result == "") {
                var box_empty = "<br><br><div class='alert alert-danger' role='alert'><center>Tidak ada Ulasan</center></div>";
                $("#box_inputulasan").append(box_empty);
            }
            else
            {
                for (var i = result.length-1; i >=0;i--) {
                    var class_id        = result[i]['class_id'];                
                    var class_ack       = result[i]['class_ack'];
                    var subject_id      = result[i]['subject_id'];
                    var name            = result[i]['name'];
                    var description     = result[i]['description'];
                    var start_time      = result[i]['starttime'];
                    var date_class      = result[i]['date_class'];
                    var class_type      = result[i]['class_type'];
                    var subject_icon    = "https://cdn.classmiles.com/sccontent/class_icon/"+result[i]['subject_icon'];

                    var tutor_name      = result[i]['user_name'];
                    var tutor_image     = result[i]['user_image'];
                    var subject_name    = result[i]['subject_name'];
                    var jenjang_name    = result[i]['jenjang_name'];
                    var jenjang_level   = result[i]['jenjang_level'];
                    // var alt_img         = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+tutor_image;

                    // if (class_ack == -1) {
                    // }
                    var box_inputulasan = 
                    "<div class='card m-t-20 z-depth-1'><div class='card-header ch-alt'><h2 class='f-14'>"+tutor_name+"</h2><ul class='actions m-t-5 f-14'><li>Waktu Kelas : "+date_class+"</li></ul></div><div class='card-body card-padding'><div class='row'><div class='col-sm-5' style='border-right: 1px solid #DADFE1;'><center><img class='lv-img' id='image' style='height: 70px; width: 70px;' alt='' src='"+tutor_image+"'><br><label class='c-orange m-t-10'>Menunggu ulasan anda</label><hr><p><label>Bagaimana pengalaman anda mengikuti kelas ini ?</label></p><p><button class_id='"+class_id+"' class_ack='"+class_ack+"' subject_name='"+subject_name+"' description='"+description+"' class_type='"+class_type+"' date_class='"+date_class+"' tutor_name='"+tutor_name+"' tutor_image='"+tutor_image+"' id='btn_caseyes' data-toggle='modal' data-target='#modalRating' type='button' class='btn btn-success btn-block btn_caseyes'>Tulis Ulasan</button></p></center></div><div class='col-sm-7'><p><label>Details Class :</label></p><div class='media'><div class='media-body'><div class='col-sm-2'><img class='media-object img-responsive' id='image' style='height: 70px; width: 70px;' alt='' src='"+subject_icon+"'></div><div class='col-sm-10'><p>"+subject_name+" - "+jenjang_name+" "+jenjang_level+"</p><p>"+description+"</p></div><hr><div class='col-sm-5 f-15 m-t-10'><button title='Type Class' class='btn btn-danger btn-icon waves-effect waves-circle waves-float m-r-10'><i class='zmdi zmdi-view-web'></i></button>"+class_type+"</div><div class='col-sm-7 f-15 m-t-10'><button title='Date' class='btn btn-warning btn-icon waves-effect waves-circle waves-float m-r-10'><i class='zmdi zmdi-calendar'></i></button>"+start_time+"</div></div></div></div></div></div></div>";                

                    $("#box_inputulasan").append(box_inputulasan);
                }
            }
        }
    });

    $.ajax({
        url: '<?php echo base_url(); ?>Rating/polling_rated/',
        type: 'POST',                
        success: function(response)
        {       
            result = JSON.parse(response);
            if (result == "") {
                var box_empty = "<br><br><div class='alert alert-danger' role='alert'><center>Tidak ada Ulasan</center></div>";
                $("#box_viewulasan").append(box_empty);
            }
            else
            {
                for (var i = result.length-1; i >=0;i--) {
                    var class_id        = result[i]['class_id'];                
                    var class_ack       = result[i]['class_ack'];
                    var subject_id      = result[i]['subject_id'];
                    var name            = result[i]['name'];
                    var description     = result[i]['description'];
                    var start_time      = result[i]['starttime'];
                    var date_class      = result[i]['date_class'];
                    var class_type      = result[i]['class_type'];
                    var subject_icon    = "https://cdn.classmiles.com/sccontent/class_icon/"+result[i]['subject_icon'];

                    var tutor_name      = result[i]['user_name'];
                    var tutor_image     = result[i]['user_image'];
                    var subject_name    = result[i]['subject_name'];
                    var jenjang_name    = result[i]['jenjang_name'];
                    var jenjang_level   = result[i]['jenjang_level'];
                    // var alt_img         = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+tutor_image;

                    var class_rate      = result[i]['class_rate'];
                    var class_review    = result[i]['class_review'];

                    var box_viewulasan = 
                    "<div class='card m-t-20'><div class='card-header ch-alt'><h2 class='f-14 c-green'>"+class_id+"</h2><ul class='actions m-t-5 f-14'><li>Waktu Kelas : "+date_class+"</li></ul></div><div class='card-body card-padding'><div class='row'><div class='col-sm-5' style='border-right: 1px solid #DADFE1;'><center><img class='lv-img' id='image' style='height: 70px; width: 70px;' alt='' src='"+tutor_image+"'><br><p class='f-16'>"+tutor_name+"</p><div id='rateYo"+i+"'></div><hr><p><label class='c-gray f-13'>"+class_review+"</label></p></center></div><div class='col-sm-7'><p><label>Details Class :</label></p><div class='media'><div class='media-body'><div class='col-sm-2'><img class='media-object img-responsive' id='image' style='height: 70px; width: 70px;' alt='' src='"+subject_icon+"'></div><div class='col-sm-10'><p>"+subject_name+" - "+jenjang_name+" "+jenjang_level+"</p><p>"+description+"</p></div><hr><div class='col-sm-5 f-15 m-t-10'><button title='Type Class' class='btn btn-danger btn-icon waves-effect waves-circle waves-float m-r-10'><i class='zmdi zmdi-view-web'></i></button>"+class_type+"</div><div class='col-sm-7 f-15 m-t-10'><button title='Date' class='btn btn-warning btn-icon waves-effect waves-circle waves-float m-r-10'><i class='zmdi zmdi-calendar'></i></button>"+start_time+"</div></div></div></div></div></div></div>";                                                             
                    
                    $("#box_viewulasan").append(box_viewulasan);

                    $(function () {
                        var rateYo = $("#rateYo"+i).rateYo({
                            starWidth: "20px",
                            fullStar: true,
                            spacing: "5px",
                            rating: class_rate,
                            readOnly: true
                        }); 
                    });
                }
            }
        }
    });

    $(document).on("click", ".btn_caseyes", function () {
        var class_id = $(this).attr('class_id');
        var class_ack = $(this).attr('class_ack');
        var subject_name = $(this).attr('subject_name');
        var description = $(this).attr('description');
        var class_type = $(this).attr('class_type');
        var date_class = $(this).attr('date_class');
        var tutor_name = $(this).attr('tutor_name');
        var tutor_image = $(this).attr('tutor_image');
        desc = description.substr(0, 15);
        $("#class_id").val("");
        $("#class_ack").val("");
        if (class_ack == -1) {
            $("#show_caseno").css('display','none');
            $("#show_caseyes").css('display','none');
            $("#show_choosecase").css('display','block');            
            $(".footer_showno").css('display','none');
            $(".footer_showyes").css('display','none');
            $(".footer_showcase").css('display','block');

            $("#class_id").val(class_id);
            $("#class_ack").val(class_ack);
            $("#title_subjectname").text(subject_name);
            $("#title_description").text(description);
            $("#title_type").text(class_type);
            $("#title_date").text(date_class);
        }
        else if(class_ack == 1){
            $("#titlefeedback").text();
            $("#titlefeedback").text("Mohon tinjau pengalaman Anda");
            $("#show_caseno").css('display','none');
            $("#show_choosecase").css('display','none');
            $("#show_caseyes").css('display','block'); 
            $(".footer_showcase").css('display','none');
            $(".footer_showno").css('display','none');
            $(".footer_showyes").css('display','block');
            $("#yes_detailclass").text(subject_name+' - '+desc);
            
            $("#class_id").val(class_id);
            $("#class_ack").val(class_ack);
            $("#yes_imagetutor").attr('src',tutor_image);
            $("#yes_tutorname").text(tutor_name);
        }        
    });

    // $(document).on("click", ".btn_caseyno", function () {

    // });

</script>