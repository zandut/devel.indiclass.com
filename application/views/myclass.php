<style type="text/css">
    .pulse {
      display: none;
      border-radius: 1000%;
      background: #008080;
      cursor: pointer;
      box-shadow: 0 0 0 rgba(12,130,129, 1);
      animation: pulse 1s infinite;

      cursor: pointer; 
      position: fixed; 
      bottom:13%; right:15%; 
      width: 40px; 
      height: 40px; 
      z-index: 5;
    }
    .pulse:hover {
      animation: none;
    }

    @-webkit-keyframes pulse {
      0% {
        -webkit-box-shadow: 0 0 0 0 rgba(12,130,129, 0.4);
      }
      70% {
          -webkit-box-shadow: 0 0 0 20px rgba(12,130,129, 0);
      }
      100% {
          -webkit-box-shadow: 0 0 0 0 rgba(12,130,129, 0);
      }
    }
    @keyframes pulse {
      0% {
        -moz-box-shadow: 0 0 0 0 rgba(12,130,129, 0.4);
        box-shadow: 0 0 0 0 rgba(12,130,129, 0.4);
      }
      70% {
          -moz-box-shadow: 0 0 0 20px rgba(12,130,129, 0);
          box-shadow: 0 0 0 20px rgba(12,130,129, 0);
      }
      100% {
          -moz-box-shadow: 0 0 0 0 rgba(12,130,129, 0);
          box-shadow: 0 0 0 0 rgba(12,130,129, 0);
      }
    }
</style>

<div ng-show="loding" class="page-loader" style="background-color:black; opacity: 0.5;">
    <div class="preloader pl-lg pls-white">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p style="margin-left: -18px;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
    </div>
</div>

<header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #ea2127;">
    
        <style type="text/css">
            /* scroller browser */
            ::-webkit-scrollbar {
                width: 9px;
            }

            /* Track */
            ::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
                -webkit-border-radius: 7px;
                border-radius: 7px;
            }

            /* Handle */
            ::-webkit-scrollbar-thumb {
                -webkit-border-radius: 7px;
                border-radius: 7px;
                background: #a6a5a5;
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
            }
            ::-webkit-scrollbar-thumb:window-inactive {
                background: rgba(0,0,0,0.4); 
            }
        </style>

        <ul class="header-inner clearfix m-l-10 m-r-10">
            <li id="menu-trigger" data-trigger="#sidebar" >
                <div class="line-wrap">
                    <div class="line top"></div>
                    <div class="line center"></div>
                    <div class="line bottom"></div>
                </div>
            </li>

            <li>
                <a href="#" class="m-l-10"><img src="<?php echo BASE_URL();?>aset/img/indiLogo2.png" style="width: 130px; height: auto; margin-top: 3%;" alt=""></a>
                <?php 
                
                    $channel_logo = $this->session->userdata('channel_logo');
                    if ($channel_logo != null) {
                    ?>
                    <img style="width: auto; height: 35px; padding-left: 5px;" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$channel_logo ?>" alt="">
                    <?php
                    }
                ?>
            </li> 
            <li class="pull-right">
                <ul class="top-menu">
                     
                    <li class="dropdown" id="notif" title="Notifikasi" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;" utc="420" id_user="<?php echo $this->session->userdata('id_user');?>">
                        <a data-toggle="dropdown" href="" ng-init="getnotif()">
                            <i class="tm-icon zmdi zmdi-notifications"></i>
                            <i class='tmn-counts bara' ng-if="nes > '0'"><span ng-bind="nes"></span></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg pull-right" >
                            <div class="listview" id="notifications" >
                                <div class="lv-header">
                                    <?php echo $this->lang->line('notification') ?>

                                    <ul class="actions" data-toggle="tooltip" title="Read all" data-placement="right">
                                        <li class="dropdown">   
                                            <a class="readsa" style="cursor: pointer;" id_user='<?php echo $this->session->userdata('id_user');?>'>
                                                <i class="zmdi zmdi-check-all" ></i>
                                            </a>                                                                  
                                        </li>
                                    </ul>
                                </div>                        
                                <div class="lv-body" id="scol" style="overflow-y:auto; height:350px; " when-scrolled="loadData()" style="cursor: pointer;">
                                    <div ng-repeat='x in dataf' style="cursor: pointer;">
                                        <a class="lv-item tif" ng-if="x.read_status == '1'" hrefa="{{x.link}}" notif_id="{{x.notif_id}}">
                                            <div class="media" data-toggle="tooltip" title="">
                                                <div class="pull-left">
                                                    <img width="65px" height="65px" style="border-radius: 5px;" src="<?php echo  $this->session->userdata('user_image'); ?>"> 
                                                </div>
                                                <div class="media-body" style="overflow:hidden; word-wrap: break-word;">
                                                    <div style="font-size: 11px; color:black;">{{x.notif}}</div>                                            
                                                </div>
                                                <div class="media-footer">
                                                    <small class="c-black" style="position:absolute; bottom:0px; margin-bottom: 1px; margin-top: auto;">{{x.interval_time}}</small>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="lv-item tif" style="background: #ECF0F1;" ng-if="x.read_status == '0'" hrefa="{{x.link}}" notif_id="{{x.notif_id}}" >
                                            <div class="media">
                                                <div class="pull-left">
                                                    <img width="65px" height="65px" src="<?php echo $this->session->userdata('user_image'); ?>"> 
                                                </div>
                                                <div class="media-body" style="overflow:hidden; word-wrap: break-word;">
                                                    <div style="font-size: 11px; color:black;">{{x.notif}}</div>                                            
                                                </div>
                                                <div class="media-footer">
                                                    <small class="c-black" style="position:absolute; bottom:0px; margin-bottom: 1px; margin-top: auto;">{{x.interval_time}}</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div ng-show="lodingnav">
                                        <center><div class="preloader pl-lg">
                                            <svg class="pl-circular" viewBox="25 25 50 50">
                                                <circle class="plc-path" cx="50" cy="50" r="20" />
                                            </svg>
                                        </div></center>
                                    </div>  
                                </div>                     

                            </div>

                        </div>
                    </li>                    
                    <li class="dropdown">
                        <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                        <ul class="dropdown-menu dm-icon pull-right">
                            <li>
                                <a onclick="signOut()" data-action="" href="<?php echo base_url('/logout'); ?>"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/keluar1-01.svg" style="margin-left: -7%; margin-right: 6%; height: 23px; width: 23px;">  <?php echo $this->lang->line('logout'); ?></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>

        <div id="kalender">        
	        <?php

	            $now = date('N');

	            $user_utc = $this->session->userdata('user_utc');
	            $server_utc = $this->Rumus->getGMTOffset();
	            $interval   = $user_utc - $server_utc;            

	            $ctd = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
	            $ctd->modify("+$interval minute");
	            $clientToday = $ctd->format('Y-m-d');
	            $now = $ctd->format('N');
	            $nextMon = date('Y-m-d', strtotime('-'.($now-1).' day', strtotime($clientToday)));
	            $oneWeek = date('Y-m-d', strtotime('+6 day', strtotime($nextMon)));
	            $nextMon = DateTime::createFromFormat('Y-m-d',$nextMon);
	            $oneWeek = DateTime::createFromFormat('Y-m-d',$oneWeek);

	            $timeArray = array();

	            $arrayFul;
	            $stime = 0;
	            for($x=0;$x<7;$x++){
	                $timeArray[$x]["day"] =  date('D, d M', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));
	                $timeArray[$x]["date"] = date('Y-m-d', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));                

	                for ($i=0; $i < 24; $i++) {
	                    $timeArray[$x][$i] = $stime;
	                    $stime+=3600;
	                }
	                $stime = 0;
	            }
	            // print_r($timeArray);

	        ?> 
	        <center>
	            <nav class="ha-menu" style="background-color: #d3111f; width: 100%; padding: 0; margin: 0;">
	                <div hidden  style="size: 50%;">
	                    <?php echo "<a style=' font-size: 10px' id='dt' class='c waves-effect active ' ></a>"; ?>
	                    <?php echo "<a style='font-size: 10px' id='dt2' class='c waves-effect active ' ></a>"; ?>
	                    <?php echo "<a style='font-size: 10px' id='dt3' class='c waves-effect active ' ></a>"; ?>
	                    <?php echo "<a style='font-size: 10px' id='dt4' class='c waves-effect active ' ></a>"; ?>
	                    <?php echo "<a style='font-size: 10px' id='dt5' class='c waves-effect active ' ></a>"; ?>
	                    <?php echo "<a style='font-size: 10px' id='dt6' class='c waves-effect active ' ></a>"; ?>
	                    <?php echo "<a style='font-size: 10px' id='dt7' class='c waves-effect active ' ></a>"; ?>
	                </div>
	                <ul>
	                    <?php

	                        echo "<li class=' waves-effect '><a id='prevbtn' ><i class='zmdi zmdi-chevron-left zmdi-hc-fw'></i></a></li>";
	                        echo "<li class='c waves-effect active '><a id='d1' ></a></li>";
	                        echo "<li class='c waves-effect '><a id='d2' ></a></li>";
	                        echo "<li class='c waves-effect '><a id='d3' ></a></li>";
	                        echo "<li class='c waves-effect '><a id='d4' ></a></li>";
	                        echo "<li class='c waves-effect '><a id='d5' ></a></li>";
	                        echo "<li class='c waves-effect '><a id='d6' ></a></li>";
	                        echo "<li class='c waves-effect '><a id='d7' ></a></li>";
	                        echo "<li class=' waves-effect '><a id='nextbtn' ><i class='zmdi zmdi-chevron-right zmdi-hc-fw'></i></a></li>";
	                    ?>
	                </ul>
	            </nav>
	        </center>
        </div>
        <div class="pull-right">
            
        </div>

        <script type="text/javascript">
            $(document).ready(function(){

                $('#btn_setindonesia').click(function(e){
                    e.preventDefault();
                    $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
                });
                $('#btn_setenglish').click(function(e){
                    e.preventDefault();
                    $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
                });
            });
        </script>
</header>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 6; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>
    <button id="btn_extraclass" class="btn btn-lg bgm-orange" style="border-radius: 50px; cursor: pointer; position: fixed; bottom:5%; right: 5px; z-index: 5;"><i class="zmdi zmdi-plus m-r-5"></i> <a id="text_extraclass" style="color: white"><?php echo $this->lang->line('createnewclass'); ?></a></button>
    <!-- <img  class="img-circle" id="btn_extraclass" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/logo_plus.jpg" data-toggle="tooltip" data-placement="right" data-original-title="Create new class" title="Create new class" style="cursor: pointer; position: fixed; bottom:5%; right:15%; width: 50px; height: 50px; z-index: 5;"> -->
    <img  class="notif_friend" id="btn_notifgroup" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Class Invitation.png" data-toggle="tooltip" data-placement="right" data-original-title="Class invitation" title="Class invitation" style="display: none; cursor: pointer; position: fixed; bottom:12.15%; right:14.75%; width: 50px; height: 50px; z-index: 6;">
    <span class="pulse notif_friend"></span>
        
    <section id="content_myclass" style="margin-top: 4%;">
        <div class="container">                
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="alert alert-danger" style="display: none; ?>" id="alertalreadyonline">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                <?php echo $this->lang->line('alreadyonline');?> 
            </div>
            <div class="block-header">
                
                <h2><?php echo $this->lang->line('myclass'); 
                    ?></h2>

                <ul class="actions">
                    <li>
                        <ol class="breadcrumb">
                        </ol>
                    </li>
                </ul>
                <br>
                <div id="filterkelas" style="display: none;">
                    <div class="">
                        <div class=" fg-line">                                                
                            <select required class="select2" id="selectkelas" style="width: 140px;" >
                                <option disabled selected>Filter <?php echo $this->lang->line('myclass'); ?></option>
                                <option value="all">All Class</option>
                                <option value="private">Private Class</option>
                                <option value="group">Group Class</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div> <!-- akhir block header    -->                   
            <div id="allclass">
                <div class=" card p-l-15 p-r-15" id="utama1">
                    <div class="row">

                        <div class="card-header">
                            <h2><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/liveclass-04.png" style="height: 25px; width: 25px;"> <label class="c-red"><?php echo $this->lang->line('livenow'); ?></label></h2>
                            <hr>
                        </div>
                        <div class="card-body" style="margin-top: -2%;">
                            <div class="col-lg-12" id="liveboxclass">                           
                            </div>                      
                        </div>
                    </div>
                </div>

                <div class="card p-l-15 p-r-15" id="utama2">
                    <div class="row">
                        <div class="card-header">
                            <h2><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/comingup-04.png" style="height: 25px; width: 25px;"> <label class="c-bluegray"><?php echo $this->lang->line('comingup'); ?></label></h2>
                            <hr>
                        </div>
                        <div class="card-body" style="margin-top: -2%;">
                            <div class="col-lg-12" id="xboxclass">
                            </div>                    
                        </div>
                    </div>
                </div>

                <div class="card p-l-15 p-r-15" id="utama5">
                    <div class="row">
                        <div class="card-header">
                            <h2><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/missedclass-04.png" style="height: 25px; width: 25px;"> <label class="c-bluegray"><?php echo $this->lang->line('missed'); ?></label></h2>
                            <hr>
                        </div>
                        <div class="card-body" style="margin-top: -2%;">
                            <div class="col-lg-12" id="xboxmissed">                        
                            </div>                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="card p-l-15 p-r-15" id="utamaextraclass" style="display: none;">
                <div class="row">
                    <div class="card-header">
                        <h2><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/extraclassorange-02.png" style="height: 25px; width: 25px;"> <label class="c-orange"><?php echo $this->lang->line('extraclass'); ?></label></h2>
                        <hr>
                    </div>
                    <div class="card-body" style="margin-top: -2%;">
                        <div class="col-lg-12" id="xboxprivateclass">                           
                        </div>                      

                        <div class="col-lg-12" id="xboxgroupclass">                           
                        </div>
                    </div>
                </div>
            </div>

            <div class="card p-l-15 p-r-15 p-t-5 p-b-5" id="utama4" style="display: none;">
                <div class="row">                
                    <div class="card-header" style="margin-top: 0%;">                        
                        <div class="alert alert-danger text-center f-14" role="alert"><?php echo $this->lang->line('no_class');?>                            
                        </div>
                    </div>
                </div>
            </div>       

            <!-- Modal CONFRIM -->  
            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="confrim" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="height: 10px;">
                            <h4 class="modal-title c-black"><?php echo $this->lang->line('purchasecomfirmation'); ?></h4>
                            <!-- <h4 class="pull-right" style="margin-top: -3%;">Saldo anda : Rp. 1 M</h4>                             -->
                        </div> 
                        <div class="modal-body">
                            <hr>
                            <p><label class="c-gray f-15"><?php echo $this->lang->line('konfirmasibelikelas'); ?></label></p>
                            <input type="text" name="classid" id="classid" class="c-black" value="" hidden />
                        </div>                                               
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal"><?php echo $this->lang->line('no'); ?></button>
                            <a id="proccess" href=""><button type="button" class="btn bgm-green" id="confrimbuy"><?php echo $this->lang->line('yes'); ?></button></a>
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="confrimjoinn" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <!-- <h4 class="modal-title c-black">Konfirmasi Join</h4>
                            <hr> -->
                            <p><label class="c-gray f-15"><?php echo $this->lang->line('konfirmasigabungkelas'); ?></label></p>
                            <input type="text" name="classid" id="classid" class="c-black" value="" hidden />
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <a id="proccessjoin" href=""><button type="button" class="btn bgm-green" id="confrimjoin">Ya</button></a>
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="sharelink_show" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black"><?php echo $this->lang->line('berbagilinkkelas'); ?></h4>
                            <hr>
                            <p><label class="c-gray f-15"><?php echo $this->lang->line('konfirmasigabungkelas'); ?></label></p>
                            <input type="text" name="classid" id="classid" class="c-black" value="" hidden />
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal"><?php echo $this->lang->line('no'); ?></button>
                            <a id="proccessjoin" href=""><button type="button" class="btn bgm-green" id="confrimjoin"><?php echo $this->lang->line('yes'); ?></button></a>
                        </div>                        
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section id="content_classdescription" style="padding: 0; display: none">
        <br>
        <div class="container">            

            <div class="block-header">
                <h2 style="opacity: 0;">Class Description</h2>                
            </div>
            <!-- akhir block header -->
            
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="margin-top: 35%; display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>
            <br>
            <div id="btn_back_home" class="block-header btn_back_home" style="cursor: pointer">
                <h2 style=""><i class="zmdi zmdi-arrow-left"></i> <?php echo $this->lang->line('myclass');?></h2>                
            </div>
                 
            <div class="row">
                <!-- <div class="col-md-12"> -->
                <div class="col-md-3" style="z-index: 5;">
                    <div class="card">
                        <div class="ribbonn"><span>Verified</span></div>
                        <img id="desc_tutorimage" src="" style="height: 250px; width: 100%;">
                        <div class="card-body card-padding">
                            <p class="f-16">
                                Tutor Description<br><br>
                                <label class="lead f-15" id="desc_tutorname"></label><br>
                                <label class="lead f-15" id="desc_tutorgender"></label><br>
                                <label class="lead f-15" id="desc_tutorage"></label>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-9">                        
                    <div class="card">
                        <div class="row">
                            <div class="card-body card-padding">
                                <div class="col-md-12">
                                    <div class="media-demo col-md-12 m-b-20">
                                        <div class="media">                                            
                                            <div class="media-body">
                                                <p class="lead">
                                                    <p class="lead" id="titleone" style="font-size: 24px;"></p>
                                                    <h5 class="lead" id="titletwo" style="font-size: 14px;"></h5>
                                                    <h5 class="lead" id="desc_jenjang" style="font-size: 14px;"></h5>
                                                </p>
                                                <div class="col-md-12" style="margin-bottom: 5%;">
                                                    <div class="col-md-3">                                                        
                                                        <button title="Date" class="btn btn-warning btn-icon waves-effect waves-circle waves-float m-r-10"><i class="zmdi zmdi-calendar"></i></button>
                                                        <small id="desc_date" class="f-14"></small>
                                                    </div>
                                                    <div class="col-md-3">                                                        
                                                        <button title="Time" class="btn btn-info btn-icon waves-effect waves-circle waves-float m-r-10"><i class="zmdi zmdi-time"></i></button>
                                                        <small id="desc_starttime" class="f-14"></small>
                                                        <small id="desc_finishtime" class="f-14"></small>
                                                    </div>
                                                    <div class="col-md-3">                                                        
                                                        <button title="Type Class" class="btn btn-danger btn-icon waves-effect waves-circle waves-float m-r-10"><i class="zmdi zmdi-view-web"></i></button>
                                                        <small id="desc_classtype" class="f-14"></small>
                                                    </div>
                                                    <div class="col-md-3">                                                        
                                                        <button title="Type Template" class="btn bgm-lime btn-icon waves-effect waves-circle waves-float m-r-10"><i class="zmdi zmdi-desktop-windows"></i></button>
                                                        <small id="desc_templatetype" class="f-14"></small>
                                                    </div>
                                                </div>
                                                <button class="buttonjoinn btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> Join</button>
                                            </div>
                                        </div>
                                    </div>                                        
                                </div>
                            </div>
                        </div>
                    </div>

                </div> 
            </div>
                
        </div><!-- akhir container -->            
    </section>

    <!-- Modal Extra Class -->
    <div class="modal fade" id="modal_privateclass" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" align="center">
                    <h4 class="modal-title">
                        <button id="btn_back_private" class="btn bgm-orange" style="position: absolute; display: block;"><i class="zmdi zmdi-close"></i></button>    
                        <button id="btn_back_search_private" class="btn bgm-orange" style="position: absolute; display: block;"><i class="zmdi zmdi-arrow-left"></i></button>
                        Private Class
                </div>
                <div class="modal-body" style="overflow-y: auto; max-height: 550px;">
                        <div id="box_privateclass">
         
                            <div class="row">
                                <!-- <div class="col-md-12"> -->
                                <div class="col-md-12" style="z-index: 5;" id="kotakuntukpencarianprivat">
                                    <div class="card">
                                        <div class="card-header ch-alt">
                                            <h2><?php echo $this->lang->line('titlesearchpriv'); ?></h2>
                                        </div>

                                        <div class="card-body card-padding">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group form-group p-r-15">
                                                        <span class="input-group-addon"><i class="zmdi zmdi-storage"></i></span>
                                                        <select class="select2 form-control" required id="subject_id" style="width: 100%;">
                                                            <?php
                                                                // $id = $this->session->userdata("id_user");
                                                                // $jenjangid = $this->session->userdata('jenjang_id');
                                                                // $allsub = $this->db->query("SELECT ms.*, mj.* FROM master_subject as ms LEFT JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.jenjang_id='$jenjangid' OR ms.jenjang_id LIKE '".$jenjangid."'")->result_array();
                                                                // echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line("selectsubjecttutor").'</option>'; 
                                                                // foreach ($allsub as $row => $v) {                                            
                                                                //     echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_name'].' '.$v['jenjang_level'].'</option>';
                                                                // }
                                                            echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
                                                            $jenjangid = $this->session->userdata('jenjang_id_kids');
                                                            if ($jenjangid == "") {
                                                                $jenjangid = $this->session->userdata('jenjang_id');
                                                            }
                                                            $id = $this->session->userdata("id_user");                                              
                                                            $sub = $this->db->query("SELECT * FROM master_subject WHERE jenjang_id LIKE '%\"$jenjangid\"%' OR jenjang_id='$jenjangid'")->result_array();
                                                            foreach ($sub as $row => $v) {

                                                                $jenjangnamelevel = array();
                                                                $jenjangid = json_decode($v['jenjang_id'], TRUE);
                                                                if ($jenjangid != NULL) {
                                                                    if(is_array($jenjangid)){
                                                                        foreach ($jenjangid as $key => $value) {                                    
                                                                            $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
                                                                            // $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level']; 
                                                                            $jenjangnamelevel[] = $selectnew['jenjang_name'];
                                                                        }   
                                                                    }else{
                                                                        $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
                                                                        // $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];
                                                                        $jenjangnamelevel[] = $selectnew['jenjang_name'];
                                                                    }                                       
                                                                }

                                                                // $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' AND (tb.status='verified' OR tb.status='unverified') order by ms.subject_id ASC")->result_array();                                                
                                                                // foreach ($allsub as $row => $v) {                                            
                                                                    // echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.implode(", ",$jenjangnamelevel).'</option>';
                                                                    echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].'</option>';
                                                                // }
                                                            }
                                                            ?>
                                                        </select>
                                                        <input type="text" name="subjecta" id="subjecton" hidden="true">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="input-group form-group p-r-15">
                                                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                                        <div class="dtp-container fg-line m-l-5" >
                                                            <input type="text" class=" form-control" id="dateon" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>"/>
                                                        </div>
                                                    </div>
                                                </div>  

                                                <div class="col-md-12">                                    
                                                    <div class="input-group form-group p-r-15">
                                                        <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                        <div class="dtp-container fg-line m-l-5" id="tempatwaktuu">
                                                            <input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
                                                        </div>                                            
                                                    </div>
                                                </div>
                                              
                                                <div class="col-md-12">
                                                    <div class="input-group form-group p-r-15">
                                                        <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                                        <div class="dtp-container fg-line">                                                
                                                            <select required class="select2 form-control" required id="duration"  style="width: 100%;">
                                                                <option disabled selected><?php echo $this->lang->line('searchduration'); ?></option>
                                                                <option value="900">15 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="1800">30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="2700">45 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="3600">1 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="5400">1 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="7200">2 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="9000">2 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="10800">3 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="12600">3 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="14400">4 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="16200">4 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="18000">5 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="19800">5 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="21600">6 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="23400">6 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="25200">7 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="27000">7 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="28800">8 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="30600">8 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="32400">9 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="34200">9 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="36000">10 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="37800">10 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="39600">11 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="41400">11 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="43200">12 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="45000">12 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="46800">13 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="48600">13 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="50400">14 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="52200">14 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="54000">15 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="55800">15 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="57600">16 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="59400">16 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="61200">17 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="63000">17 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="64800">18 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="66600">18 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="68400">19 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="70200">19 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="72000">20 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="73800">20 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="75600">21 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="77400">21 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="79200">22 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="81000">22 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="82800">23 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="84600">23 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="86400">24 <?php echo $this->lang->line('jam'); ?></option>
                                                            </select>
                                                            <input type="text" name="durationa" id="durationon" hidden="true">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <button id="button_search" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                                                </div>
                                            </div>

                                        </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-12" id="kotakhasilpencarianprivat" style="display: none;">                        
                                    <div class="card">
                                        
                                        <div class="card-header ch-alt" ng-show="resulte">
                                            <h2><?php echo $this->lang->line('titleresult'); ?> : {{hasil}} </h2>
                                            <div class="pull-right f-14" style="margin-top: -3%;"><label>{{subjects}}</label></div>
                                        </div>
                                        <div class="row">
                                            <div class="card-body card-padding">                                                                
                                                <div class="alert alert-info alert-dismissible text-center" role="alert" ng-show="awals">              
                                                    <label><?php echo $this->lang->line('alertfirst'); ?></label>
                                                </div>
                                                <div class="alert alert-danger alert-dismissible text-center" role="alert" ng-show="firste">              
                                                    <label><?php echo $this->lang->line('nodata'); ?></label>
                                                </div>
                                                <div class="col-md-12" ng-repeat=" x in datas" ng-show="resulte">
                                                    <div class="media-demo col-md-12 m-b-20">
                                                        <div class="media">
                                                            <div class="pull-left col-md-3">                                                        
                                                                <div ng-if="x.status_tutor == 'verified'" class="ribbon"><span>Verified</span></div>                                                
                                                                <!-- <img  class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL?>baru/verifiedtutor.png' style='z-index: 1; position: absolute; height:40px; width:40px; top: 0; left: 250; margin-right: 10%; margin-top: 1%; margin-left:1%;'>                                             -->
                                                                <img class="media-object img-responsive m-t-5" ng-src="{{x.user_image}}" style="height: 100%; width: 100%;" alt="">
                                                            </div>
                                                            <div class="media-body">
                                                                <div class="col-md-4" style="border-right: 1px solid #ececec;">                                                     
                                                                    <p class="f-13"><?php echo $this->lang->line('demandname'); ?> : <br>{{x.user_name}}</p>
                                                                    <p class="f-13" style="margin-top: -5%;"><?php echo $this->lang->line('demandtime'); ?> : <br>
                                                                    <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                                    <label >{{times}}</label>
                                                                    </p>                                                                                                        
                                                                </div> 
                                                                <div class="col-md-5" style="border-right: 1px solid #ececec;">
                                                                    <p class="f-13">
                                                                        <?php echo $this->lang->line('demandprice'); ?> : <br>Rp. {{x.hargaakhir}}
                                                                    </p>                                                    
                                                                </div>                                               
                                                                <div class="col-md-3" id="btn_cari">                         
                                                                    <button gh="{{x.gh}}" harga="{{x.harga_cm}}" hargaakhir="{{x.hargaakhir}}" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" dates="{{dates}}" start_time="{{times}}" subject_id="{{x.subject_id}}" tutor_id="{{x.tutor_id}}" hargatutor="{{x.harga_tutor}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                                                    <button ng-if="x.exact == '0'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" hargatutor="{{x.harga_tutor}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                        
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-link">Save changes</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_groupclass" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" align="center">
                    <h4 class="modal-title">
                        <button id="btn_back_group" class="btn bgm-orange" style="position: absolute; display: block;"><i class="zmdi zmdi-close"></i></button>    
                        <button id="btn_back_search_group" class="btn bgm-orange" style="position: absolute; display: none;"><i class="zmdi zmdi-arrow-left"></i></button>
                        Group Class
                </div>
                <div class="modal-body" style="overflow-y: auto; max-height: 550px;">
                    <div id="box_groupclass">
                        <div class="row">
                            <!-- <div class="col-md-12"> -->
                            <div class="col-md-12" style="z-index: 5;" id="kotakuntukpencariangroup">
                                <div class="card">
                                    <div class="card-header ch-alt">
                                        <h2><?php echo $this->lang->line('titlesearchgrup');?></h2>
                                    </div>

                                    <div class="card-body card-padding">
                                    <form>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group form-group p-r-15">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-storage"></i></span>
                                                    <select class="select2 form-control" required id="subject_idg" style="width: 100%;">
                                                        <?php
                                                            // $id = $this->session->userdata("id_user");
                                                            // $jenjangid = $this->session->userdata('jenjang_id');
                                                            // $allsub = $this->db->query("SELECT ms.*, mj.* FROM master_subject as ms LEFT JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.jenjang_id='$jenjangid' OR ms.jenjang_id LIKE '".$jenjangid."'")->result_array();
                                                            // echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line("selectsubjecttutor").'</option>'; 
                                                            // foreach ($allsub as $row => $v) {                                            
                                                            //     echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_name'].' '.$v['jenjang_level'].'</option>';
                                                            // }
                                                        echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
                                                        $jenjangid = $this->session->userdata('jenjang_id_kids');
                                                        if ($jenjangid == "") {
                                                            $jenjangid = $this->session->userdata('jenjang_id');
                                                        }
                                                        $id = $this->session->userdata("id_user");                                              
                                                        $sub = $this->db->query("SELECT * FROM master_subject WHERE jenjang_id LIKE '%\"$jenjangid\"%' OR jenjang_id='$jenjangid'")->result_array();
                                                        foreach ($sub as $row => $v) {

                                                            $jenjangnamelevel = array();
                                                            $jenjangid = json_decode($v['jenjang_id'], TRUE);
                                                            if ($jenjangid != NULL) {
                                                                if(is_array($jenjangid)){
                                                                    foreach ($jenjangid as $key => $value) {                                    
                                                                        $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
                                                                        // $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level']; 
                                                                        $jenjangnamelevel[] = $selectnew['jenjang_name'];
                                                                    }   
                                                                }else{
                                                                    $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
                                                                    // $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];
                                                                    $jenjangnamelevel[] = $selectnew['jenjang_name'];
                                                                }                                       
                                                            }

                                                            // $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' AND (tb.status='verified' OR tb.status='unverified') order by ms.subject_id ASC")->result_array();                                                
                                                            // foreach ($allsub as $row => $v) {                                            
                                                                // echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.implode(", ",$jenjangnamelevel).'</option>';
                                                            echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].'</option>';
                                                            // }
                                                        }
                                                        ?>
                                                    </select>
                                                    <input type="text" name="subjecta" id="subjectong" hidden="true">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-group form-group p-r-15">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                                    <div class="dtp-container fg-line m-l-5">                                                
                                                        <input type="text" class="form-control" id="dateong" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>"/>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="col-md-12">                                    
                                                <div class="input-group form-group p-r-15">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                    <!-- <div class="dtp-container fg-line m-l-5">
                                                        <input type="text" class="form-control" id="timeeee" placeholder="<?php echo $this->lang->line('searchtime'); ?>" />
                                                    </div> -->
                                                    <div class="dtp-container fg-line m-l-5" id="tempatwaktuuu">
                                                        <input type='text' class='form-control' id='timeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
                                                    </div>                                            
                                                </div>
                                            </div>
                                            <div class="col-md-12" id="durasigroup">
                                                <div class="input-group form-group p-r-15">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                                    <div class="dtp-container fg-line">                                                
                                                        <select required class="select2 form-control" id="durationg" style="width: 100%;" >
                                                            <option disabled selected><?php echo $this->lang->line('searchduration'); ?></option>
                                                            <option value="900">15 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="1800">30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="2700">45 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="3600">1 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="5400">1 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="7200">2 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="9000">2 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="10800">3 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="12600">3 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="14400">4 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="16200">4 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="18000">5 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="19800">5 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="21600">6 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="23400">6 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="25200">7 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="27000">7 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="28800">8 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="30600">8 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="32400">9 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="34200">9 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="36000">10 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="37800">10 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="39600">11 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="41400">11 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="43200">12 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="45000">12 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="46800">13 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="48600">13 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="50400">14 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="52200">14 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="54000">15 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="55800">15 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="57600">16 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="59400">16 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="61200">17 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="63000">17 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="64800">18 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="66600">18 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="68400">19 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="70200">19 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="72000">20 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="73800">20 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="75600">21 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="77400">21 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="79200">22 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="81000">22 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="82800">23 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="84600">23 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="86400">24 <?php echo $this->lang->line('jam'); ?></option>
                                                        </select>
                                                        <input type="text" name="durationa" id="durationong" hidden="true" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button id="button_searchgroup" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                                            </div>
                                        </div>

                                    </div>
                                    </form>
                                </div>
                            </div>

                            <div class="col-md-12" id="kotakhasilpencariangroup" style="display: none;">
                                <div class="card">

                                    <div class="card-header ch-alt" ng-show="resulte">
                                        <h2><?php echo $this->lang->line('titleresult'); ?> : {{ghasil}} </h2>
                                        <div class="pull-right f-14" style="margin-top: -3%;"><label>{{subjects}}</label></div>
                                    </div>

                                    <div class="card-body card-padding" >
                                        <div class="row">
                                            <div class="alert alert-info alert-dismissible text-center" role="alert" ng-if="awals" >              
                                                <label><?php echo $this->lang->line('alertfirst'); ?></label>
                                            </div>
                                            <div class="alert alert-danger alert-dismissible text-center" role="alert" ng-if="firste">                                
                                                <label><?php echo $this->lang->line('nodata'); ?></label>
                                            </div>
                                            <div class="col-md-12" ng-repeat=" x in datasg" ng-show="resulte">
                                                <div class="media-demo col-md-12 m-b-20">
                                                    <div class="media">
                                                        <div class="pull-left col-md-3">                                                    
                                                            <!-- <img ng-if="x.status_tutor == 'verified'" class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL?>baru/verifiedtutor.png' style='z-index: 1; position: absolute; height:40px; width:40px; top: 0; left: 250; margin-right: 10%; margin-top: 1%; margin-left:1%;'> -->
                                                            <div ng-if="x.status_tutor == 'verified'" class="ribbon"><span>Verified</span></div>
                                                            <img class="media-object img-responsive m-t-5" ng-src="{{x.user_image}}" style="height: 100%; width: 100%;" alt="">
                                                        </div>
                                                        <div class="media-body">
                                                            <div class="col-md-4 col-sm-4 col-xs-4" style="border-right: 1px solid #ececec;">                                                     
                                                                <p class="f-13"><?php echo $this->lang->line('demandname'); ?> : <br>{{x.user_name}}</p>
                                                                <p class="f-13" style="margin-top: -5%;"><?php echo $this->lang->line('demandtime'); ?> : <br>
                                                                <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                                <label >{{times}}</label>                                                        
                                                                </p>
                                                                                                                                                                           
                                                            </div>
                                                            <div class="col-md-5 col-sm-5 col-xs-6" style="border-right: 1px solid #ececec;">
                                                                <p class="f-13">
                                                                    <?php echo $this->lang->line('demandprice'); ?> : <br>Rp. {{x.hargaakhir}}
                                                                </p>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3 col-xs-3">                         
                                                                <a class="openmodaladd {{x.tutor_id}} btn bgm-green btn-icon-text btn-block" gh="{{x.gh}}" harga="{{x.harga_cm}}" hargatutor="{{x.harga_tutor}}" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" dates="{{dates}}" start_time="{{times}}" subject_id="{{x.subject_id}}" tutor_id="{{x.tutor_id}}">
                                                                    <i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?>                                                                                        
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                        
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-link">Save changes</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
    <div class="modal " id="modalinvitefriends" tabindex="-1" role="dialog" style="top: 13%;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bgm-cyan">
                    <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    <h4 class="modal-title c-white"><?php echo $this->lang->line('titleinvite');?></h4>
                </div>
                <div class="modal-body m-t-20">
                	<div align="center" id="detailKelas" style="margin-bottom: 20px;">
                		<input type="text" id="id_kelas" name="id_kelas" hidden>
                		<img class="img-rounded" style="height: 80px; width: 80px;" id="imgTutor" src="">
                		<h3 id="namatutor"></h5>
                		<h4 id="subjectkelas"></h4>
                		<h6 id="deskripsiekelas"></h6>
                		<h6 id="waktukelas"></h6>
                	</div>
                	<hr>
                    <form id="invfriend">
                        <div class="col-md-12" style="padding: 0;">
                            <div class="col-md-12">
                                <label class="col-md-12"><?php echo $this->lang->line('findfriends');?></label><br>
                                <div class="col-md-12 m-t-10">                                          
                                    <select required name="tagteman[]" id="tagteman" multiple class="select2 form-control" style="width: 100%;">
                                    	<!-- <option selected="true" value="volvo">Volvo</option> -->
                                    </select>           
                                    <label class="c-red" style="display: none;" id="alertemailempty"><?php echo $this->lang->line('emailkosong');?></label>                     
                                    <label class="c-red" style="display: none;" id="alertemailnotfound"><?php echo $this->lang->line('emailnotfound');?></label>
                                    <label class="c-red" style="display: none;" id="alertemailsendiri"><?php echo $this->lang->line('emailown');?></label>
                                </div>
                            </div>
                            <br>
                            <div class="col-md-1"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btnreqgroupdemand btnInvGroup btn btn-success btn-block m-t-15">Tambah Teman</button>                            
                </div>
            </div>
        </div>
    </div>
    <div class=" modal fade" data-modal-color="blue" id="modal_extraclass" tabindex="-1" role="dialog" >
        <div class="modal-lg modal-dialog">
            <div class="modal-content" style="margin-top: 20%;">
                <div class="modal-header">
                    <h3 class="c-white">
                        <button style="position: fixed;" class="btn btn-danger btn-raise" data-dismiss="modal"><i class="zmdi zmdi-close"></i></button>
                        <label style="margin-left: 5%;"><?php echo $this->lang->line('createextraclass');?></label>
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-xs-12" id="privateclick">
                            <a id="privateclassclick" style="cursor: pointer;">
                                <div class="mini-charts-item bgm-orange" id="kotaknyaprivate">
                                    <div class="clearfix">
                                        <div class="chart stats-bar-2"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>class_icon/Private_revisiw.png" class="img-responsive" style="height: 65px; width: 65px; margin-left: 9px;" alt=""></div>
                                        <div class="count">
                                            <h2 class="m-l-20"><?php echo $this->lang->line('privateclass'); ?></h2>
                                            <small class="m-l-20 m-t-5"><?php echo $this->lang->line('detailprivate'); ?></small>                                            
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        
                        <div class="col-sm-12 col-md-6 col-xs-12" id="groupclick">
                            <a id="groupclassclick" style="cursor: pointer;">
                                <div class="mini-charts-item bgm-orange" id="kotaknyagroup" >
                                    <div class="clearfix">
                                        <div class="chart stats-bar-2"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>class_icon/Group_revisiw.png" class="img-responsive" style=" height: 65px; width: 65px; margin-left: 9px;" alt=""></div>
                                        <div class="count">                                            
                                            <h2 class="m-l-20 m-t-10"><?php echo $this->lang->line('groupclass'); ?></h2>
                                            <small class="m-l-20 m-t-5"><?php echo $this->lang->line('detailgroup'); ?></small>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-link">Save changes</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
    <div class="  modal fade" id="modal_invitation" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="bgm-white modal-body" style=""> 
                <div class="card-header m-t-10">
                    <h2 class=""><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/channelgroup.png" style="height: 25px; width: 25px;"> <label class="m-t-20"><?php echo $this->lang->line('classinvitation'); ?></label></h2>
                </div>
                <table class="displayyy table datainvitation" style="size: 80%;">
                    <tbody id="tempatfriendinvitation">                         
                    </tbody>
                </table>
            </div>
        </div>       
    </div>
    <div class=" modal fade" data-modal-color="blue" id="modal_confirm" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                    <center>
                        <div id='' class="modal-body" style="margin-top: 30%;" ><br>
                           <div class="alert alert-info" style="display: block; ?>" id="">
                                <label id="labelclass"></label>
                            </div>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="buttonok btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                    </div>
                </center>
            </div>
        </div>
    </div>
    <div class=" modal fade" data-modal-color="blue" id="modal_confirm_anak" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                    <center>
                        <div id='' class="modal-body" style="margin-top: 30%;" ><br>
                           <div class="alert alert-info" style="display: block; ?>" id="">
                                <label id="">Kelas tersebut tidak sesuai dengan jenjang Anda</label>
                            </div>
                        </div>
                    <div class="modal-footer">
                    	<button id="btn_openmodal" type="button" class="btn btn-link"  data-dismiss="modal"  style="">Ganti User</button>
                        <button type="button" class="buttonok btn btn-link" data-dismiss="modal" data-="modal" aria-label="Close" style="">OK</button>
                    </div>
                </center>
            </div>
        </div>
    </div>
    <div class=" modal fade" data-modal-color="blue" id="modal_list_anak" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                    <center>
                        <div id='' class="modal-body" style="margin-top: 30%; " ><br>
	                           	<div class="row">
	                           		<div class="col-md-12"><label>Silakan pilih User yang akan bergabung dengan kelas ini:</label></div>
	                           		<!-- <div class="col-md-3"><label>Username:</label></div> -->
	                                <div class="form-group col-xs-12 col-md-12">
	                                    <select  id="nama_anak" required name="nama_anak" class="select2 selectpicker form-control" data-live-search="true"  style="width: 100%;">
	                                      <?php                       

	                                        $parent_id = $this->session->userdata('id_user');
	                                        $parent_jenjang_id = $this->session->userdata('jenjang_id');
	                                        $username = $this->session->userdata('user_name');

	                                        $listanak = $this->db->query("SELECT tpk.kid_id, tpk.jenjang_id, tu.user_name, tu.first_name, tu.last_name, tu.user_image from tbl_profile_kid as tpk INNER JOIN tbl_user as tu where tu.id_user = tpk.kid_id and tpk.parent_id='$parent_id'")->result_array();
	                                        // $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
	                                        // echo "<option id='".$parent_id."' value='".$parent_jenjang_id."' selected='true'>".$username." (Saya sendiri)</option>";
	                                        foreach ($listanak as $row => $d) {
	                                                echo "<option id='".$d['kid_id']."'  value='".$d['jenjang_id']."'>".$d['user_name']."</option>";
	                                        }
	                                        ?>
	                                    </select>
	                                </div>
	                            </div>                           	
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="buttonjoinanak btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                    </div>
                </center>
            </div>
        </div>
    </div>
    <div class="modal" id="modaladdfriends" tabindex="-1" role="dialog" style="top: 13%;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bgm-cyan">
                    <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    <h4 class="modal-title c-white"><?php echo $this->lang->line('titledemandconfrim');?></h4>
                </div>
                <div class="modal-body m-t-20">
                    <form id="frmdata">
                        <div class="col-md-12 c-gray m-10">
                            <label><?php echo $this->lang->line('checkdemand'); ?></label>
                        </div>
                        <div class="col-md-12" style="padding: 0;">
                            <div class="col-md-12">
                                <input type="text" name="data_subject_id" id="data_subject_id" hidden />
                                <input type="text" name="data_start_time" id="data_start_time" hidden/>
                                <input type="text" name="data_avtime_id" id="data_avtime_id" hidden/>
                                <input type="text" name="data_duration" id="data_duration" hidden/>
                                <input type="text" name="data_metod" id="data_metod" hidden/><br>
                                <!-- <input name='ms' class="col-md-8 m-t-5" style="height: 35px;" id="ms"> -->
                                <div class="col-md-12 m-t-10">
                                    <div class="media-demo col-md-12 m-b-20">
                                        <div class="media">
                                            <div class="pull-left">                                                    
                                                <img class="media-object img-responsive m-t-5 imagetutor_private" id="image_group" style="height: 120px; width: 120px;" alt="">
                                            </div>
                                            <div class="media-body">
                                                <div class="col-md-8 c-gray" style="border-right: 1px solid #ececec;">                                                     
                                                    <p class="f-13"><?php echo $this->lang->line('demandname'); ?> : <br><label id="namemodal_group"></label></p>
                                                    <br>
                                                    <p class="f-13" style="margin-top: -5%;"><?php echo $this->lang->line('demandtime'); ?> : <br>
                                                        <label id="timestart_group"></label>                                                  
                                                    </p>                                                                                                        
                                                </div> 
                                                <div class="col-md-4 c-gray">
                                                    <p class="f-13"><?php echo $this->lang->line('demanddate'); ?> : <br>
                                                        <label id="tanggalkelas_group"></label>                                                  
                                                    </p>
                                                    <br>
                                                    <p class="f-13" style="margin-top: -5%;">
                                                        <?php echo $this->lang->line('demandprice'); ?> : <br><label id="hargakelas_group"></label> 
                                                    </p> 
                                                </div>
                                            </div>
                                            <div class="c-gray m-t-5">
                                                <label class="m-t-10"><?php echo $this->lang->line('topikbelajar');?></label><br>                                               
                                                <textarea rows="4" class="form-control" name="topikgroup" id="topikgroup" style="width: 100%; margin-top: 10px; overflow: hidden;" placeholder="<?php echo $this->lang->line('isitopikanda');?>"></textarea>
                                                <label class="c-red" style="display: none;" id="alerttopikgroup" ><?php echo $this->lang->line('topikkosong');?></label>
                                            </div>
                                        </div>
                                    </div>                                                          
                                </div>                                                            
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </form>
                </div>
                <hr>
                <div class="modal-footer">
                    <button type="button" class="btn-req-group-demand btn btn-success btn-block m-t-15" demand-link-group=""><?php echo $this->lang->line('confrimdemand');?></button>                            
                </div>
            </div>
        </div>
    </div>
    <div class="modal" style="margin-top: 4%;" id="modalconfrim" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">                          
                    <button type="button" class="close" data-dismiss="modal"><i class="zmdi zmdi-close"></i></button>
                    <!-- <center> -->
                        <h4 class="modal-title c-black f-20"><?php echo $this->lang->line('titledemandconfrim'); ?></h4>
                    <!-- </center> -->
                    <hr>
                    <div id="showalertpayment" style="display: none;">
                        <div class="alert alert-success f-14" role="alert"><?php echo $this->lang->line('successfullydemand');?></div>    
                    </div>
                    <div id="showdetailtutor">
                        <div class="col-md-12 c-gray m-10">
                            <label><?php echo $this->lang->line('checkdemand'); ?></label>
                        </div>
                        <div class="col-md-12 m-t-10">
                            <div class="media-demo col-md-12 m-b-20">
                                <div class="media">
                                    <div class="pull-left">                                                    
                                        <img class="media-object img-responsive m-t-5 imagetutor_private" id="image" style="height: 120px; width: 120px;" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="col-md-8 c-gray" style="border-right: 1px solid #ececec;">                                                     
                                            <p class="f-13"><?php echo $this->lang->line('demandname'); ?> : <br><label id="namemodal"></label></p>
                                            <br>
                                            <p class="f-13" style="margin-top: -5%;"><?php echo $this->lang->line('demandtime'); ?> : <br>
                                                <label id="timestart"></label>                                                  
                                            </p>                                                                                                        
                                        </div> 
                                        <div class="col-md-4 c-gray">
                                            <p class="f-13"><?php echo $this->lang->line('demanddate'); ?> : <br>
                                                <label id="tanggalkelas"></label>                                                  
                                            </p>
                                            <br>
                                            <p class="f-13" style="margin-top: -5%;">
                                                <?php echo $this->lang->line('demandprice'); ?> : <br><label id="hargakelas"></label> 
                                            </p> 
                                        </div>
                                    </div>
                                    <div class="c-gray m-t-5">                                            
                                        <textarea rows="4" class="form-control" name="topikprivate" id="topikprivate" style="width: 100%;" placeholder="Isi topik yang ingin Anda pelajari ..."></textarea>
                                        <label class="c-red" style="display: none;" id="alerttopik" >Harap isi topik pelajaran terlebih dahulu !</label>
                                    </div>
                                </div>
                            </div>                                                          
                        </div>  
                    </div>
                                                           
                </div> 
                <div class="modal-footer">
                    <!-- <div id="afterchoose" style="display: none;">
                        <button type="button" class="btn-next-demand btn btn-success pull-left"><i class="zmdi zmdi-chevron-left"></i> Lanjut pilih kelas lain</button>
                        <button type="button" class="btn-view-demand btn btn-danger" demand-link=""><?php echo $this->lang->line('confrimpayment');?> <i class="zmdi zmdi-chevron-right"></i></button>  
                    </div> -->
                    <div id="beforechoose">
                        <button type="button" id="getloading" class="btn-req-demand btn btn-success btn-block" demand-link=""><?php echo $this->lang->line('confrimdemand');?></button>
                    </div>
                </div>                                                             
            </div>
        </div>
    </div>
    <!-- End Modal Extra Class -->

</section>
<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
	var cekkotak = 0;
    $("#btntambah").click(function(){
        if (cekkotak == 0) 
        {            
            $("#kotakpilihanteman").append("<div class='col-md-8 m-b-25' style='padding: 0;' id='kotakpilihanteman2'><div class='col-md-12'><label for='nama'>Tambahkan Temanmu melalui Email</label><input type='text' class='form-control typeahead' id='nama' data-provide='typeahead' autocomplete='off'></div></div><div class='col-md-4'><button class='btn btn-info waves-effect btn-sm m-t-20' id='btntambah'><i class='zmdi zmdi-plus'></i></button><button class='btn btn-danger waves-effect btn-sm m-t-20' id='btnkurang'><i class='zmdi zmdi-minus'></i></button></div>");
            cekkotak+1;
        }        
    });

    $("#btn_openmodal").click(function(){
        $("#modal_list_anak").modal('show');
    });  
	var cek_anak = "<?php echo $this->session->userdata('punya_anak');?>";
    var jenjang_anak = "<?php echo $this->session->userdata('jenjang_anak');?>";
    var parent_id = <?php echo $this->session->userdata('id_user');?>;

    var stat_showdetail = "<?php echo $this->session->userdata('stat_showdetail');?>";     
    // alert(jenjang_anak);
    var tokenjwttt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
    var myjenjang = "<?php echo $this->session->userdata('jenjang_id'); ?>";
    var user_utcc = new Date().getTimezoneOffset();
    var tgll = new Date();
    var formattedDatee = moment(tgll).format('YYYY-MM-DD');
    var iduser = "<?php echo $this->session->userdata('id_user_kids'); ?>";
    if (iduser == null || iduser=="") {
        iduser  = "<?php echo $this->session->userdata('id_user'); ?>";
    }
    user_utcc = -1 * user_utcc;  

    if (stat_showdetail!="") {        

        $.ajax({
            url: '<?php echo BASE_URL();?>Rest/get_classdesc/access_token/'+tokenjwttt,
            type: 'POST',
            data: {
                user_utc: user_utcc,
                user_device: 'web',
                date: formattedDatee,
                class_id: stat_showdetail
            },
            success: function(data)
            {                   
                var a = JSON.stringify(data);
                console.warn('data '+a);
                subject_id      = data['data'][0]['subject_id'];
                jenjang_id      = data['data'][0]['jenjang_id'];
                status_all      = data['data'][0]['status_all'];
                subject_name    = data['data'][0]['name'];
                subject_description = data['data'][0]['description'];
                tutor_id        = data['data'][0]['tutor_id'];
                start_time      = data['data'][0]['start_time'];
                finish_time     = data['data'][0]['finish_time'];
                class_type      = data['data'][0]['class_type'];
                template_type   = data['data'][0]['template_type'];
                template        = data['data'][0]['template_type'];
                jenjangnamelevel        = data['data'][0]['jenjangnamelevel'];
                user_name       = data['data'][0]['user_name'];
                email           = data['data'][0]['email'];
                user_gender     = data['data'][0]['user_gender'];
                user_age        = data['data'][0]['user_age'];
                user_image      = data['data'][0]['user_image'];
                dateclass       = data['data'][0]['date'];
                timeshow        = data['data'][0]['time'];
                endtimeshow     = data['data'][0]['endtime'];
                jenjang_idd     = data['data'][0]['jenjang_idd'];
                timeclass       = data['data'][0]['date']+" "+data['data'][0]['time'];
                endtimeclass    = data['data'][0]['enddate']+" "+data['data'][0]['endtime'];
                var all_jenjang = null;

                if (jenjang_idd != null) {
                	
                }
                else{
                	jenjang_idd = jenjang_id;
                }
                
                var userutc     = "<?php echo $this->session->userdata('user_utc');?>";
                var participant_listt = data['data']['participant']['participant'];

                // alert(jenjangnamelevel);
                if (status_all != null) {
                    var desc_jenjang = "Semua Jenjang";
                }
                if (status_all == null) {
                    var desc_jenjang = jenjangnamelevel;   
                }

                if (myjenjang != jenjang_idd && cek_anak !="yes") {
                    // $("#modal_confirm").modal('show');
                	// notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Kelas Tersebut tidak tersedia di jenjang Anda");

                	$("#modal_confirm").modal('show');
                    $("#modal_list_anak").modal('hide');
                    $("#labelclass").text("Kelas tersebut tidak sesuai dengan jenjang Anda");
                    $("#sidebar").css('margin-top','3.5%');
                    $("#kalender").css('display','block');
                    $("#content_classdescription").css('display','none');
                    $("#content_extraclass").css('display','none');
                    $("#content_myclass").css('display','block'); 
                    // $.ajax({
                    //     url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                    //     type: 'POST',
                    //     success: function(data)
                    //     {              
                            
                    //     }
                    // });
                }
                else if (myjenjang != jenjang_idd && cek_anak == "yes") {

                    $.ajax({
                        url: '<?php echo base_url(); ?>Master/checkListKids',
                        type: 'POST',
                        data: {                
                            user_utc : user_utc,
                            device: 'web'
                        },            
                        success: function(data)
                        {         
                            result = JSON.parse(data);                
                            if (result['status'] == true) 
                            {                       
                                for (var i = 0; i < result['message'].length; i++) {
                                    
                                    var kid_id         = result['message'][i]['kid_id'];
                                    var kid_jenjang_id         = result['message'][i]['jenjang_id'];
                                    var kid_first_name         = result['message'][i]['first_name'];
                                    var kid_last_name         = result['message'][i]['last_name'];
                                    var kid_user_image         = result['message'][i]['user_image'];
                                    var kid_user_name         = result['message'][i]['user_name'];

                                    var cek_anak = 0;
                                    if (kid_jenjang_id != jenjang_idd) {
                                        
                                    }
                                    else{
                                        var ada_anak =null;
                                        ada_anak++;
                                    }

                                    
                                }
                                // alert(ada_anak);
                                if (ada_anak == null) {
                                    // alert("Tidak ada anak");

                                    $("#modal_confirm").modal('show');
                                    $("#modal_list_anak").modal('hide');
                                    $("#labelclass").text("Kelas tersebut tidak sesuai dengan jenjang Anda");
                                    $("#sidebar").css('margin-top','3.5%');
                                    $("#kalender").css('display','block');
                                    $("#content_classdescription").css('display','none');
                                    $("#content_extraclass").css('display','none');
                                    $("#content_myclass").css('display','block'); 
                                }
                                else if (ada_anak != null) {
                                    // alert("Ada anak");

                                    $("#modal_list_anak").modal('show');
                                    $("#modal_confirm").modal('hide');
                                    $("#content_myclass").css('display','block');
                                    $("#content_extraclass").css('display','none');
                                    $("#sidebar").css('margin-top','0%');
                                    $("#kalender").css('display','none');
                                    $("#modal_extraclass").modal('hide');
                                    $("#content_classdescription").css('display','none');

                                    // $.ajax({
                                        //     url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                                        //     type: 'POST',
                                        //     success: function(data)
                                        //     {              
                                                
                                        //     }
                                        // });

                                        $('select').on('change', function() {

                                            var kid_jenjang_id =(this.value );

                                            var kid_id = $(this).children(":selected").attr("id");
                                            // alert(id);

                                            // alert(jenjang_idd);
                                            // alert("kid jenjang "+kid_jenjang_id);

                                            $('.buttonjoinanak').click(function(){
                                                
                                                // alert(h);
                                                if (kid_jenjang_id != jenjang_idd) {
                                                    $("#modal_confirm_anak").modal('show');
                                                    $("#modal_list_anak").modal('hide');
                                                    $("#content_myclass").css('display','block');
                                                    $("#content_extraclass").css('display','none');
                                                    $("#sidebar").css('margin-top','0%');
                                                    $("#kalender").css('display','none');
                                                    $("#modal_extraclass").modal('hide');
                                                    $("#content_classdescription").css('display','none');
                                                    // $("#labelclass").text("Kelas tersebut tidak sesuai dengan jenjang Anda");
                                                }
                                                else if (kid_jenjang_id == jenjang_idd) {

                                                    $("#modal_confirm_anak").modal('hide');
                                                    $("#modal_list_anak").modal('hide');
                                                    if (class_type=="multicast") {typeclass="Multicast";}else if(class_type=="private"){typeclass="Private";}else{typeclass="Group";}
                                                    if (template_type=="whiteboard_digital") {template_type="Digital Board";}else if(template_type=="whiteboard_videoboard"){template_type="Video Board";}else{template_type="All Features";}
                                                    $("#desc_tutorname").append("<i class='zmdi zmdi-assignment-account m-r-10'></i> "+user_name);
                                                    $("#desc_tutorgender").append("<i class='zmdi zmdi-male-female m-r-10'></i> "+user_gender);
                                                    $("#desc_tutorage").append("<i class='zmdi zmdi-time-interval m-r-10'></i> "+user_age+" tahun");
                                                    $("#desc_tutorimage").attr('src', '<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>'+user_image);

                                                    $("#titleone").text(subject_name);
                                                    $("#titletwo").text(subject_description);
                                                    $("#desc_jenjang").text(desc_jenjang);
                                                    $("#desc_date").text(dateclass);
                                                    $("#desc_starttime").text(timeshow+" - ");
                                                    $("#desc_finishtime").text(endtimeshow);
                                                    $("#desc_classtype").text(typeclass);
                                                    $("#desc_templatetype").text(template_type);
                                                    $("#nametutor").text(user_name);

                                                    $(".buttonjoinn").attr('link','Transaction/multicast_join/'+stat_showdetail+'?t=kids&child_id='+kid_id);

                                                    $("#content_myclass").css('display','none');
                                                    $("#content_extraclass").css('display','none');
                                                    $("#sidebar").css('margin-top','0%');
                                                    $("#kalender").css('display','none');
                                                    $("#modal_extraclass").modal('hide');
                                                    $("#content_classdescription").css('display','block');
                                                }

                                                else if (myjenjang != jenjang_idd)  {
                                                    $("#modal_confirm_anak").modal('show');
                                                    $("#modal_list_anak").modal('hide');
                                                    $("#content_myclass").css('display','block');
                                                    $("#content_extraclass").css('display','none');
                                                    $("#sidebar").css('margin-top','0%');
                                                    $("#kalender").css('display','none');
                                                    $("#modal_extraclass").modal('hide');
                                                    $("#content_classdescription").css('display','none');
                                                    // $("#labelclass").text("Kelas tersebut tidak sesuai dengan jenjang Anda");
                                                }
                                            });
                                        });

                                        $('.buttonjoinanak').click(function(){
                                                
                                                
                                            if (myjenjang != jenjang_idd)  {
                                                $("#modal_confirm_anak").modal('show');
                                                $("#modal_list_anak").modal('hide');
                                                $("#content_myclass").css('display','block');
                                                $("#content_extraclass").css('display','none');
                                                $("#sidebar").css('margin-top','0%');
                                                $("#kalender").css('display','none');
                                                $("#modal_extraclass").modal('hide');
                                                $("#content_classdescription").css('display','none');
                                                // $("#labelclass").text("Kelas tersebut tidak sesuai dengan jenjang Anda");
                                            }
                                        });


                                }
                            }
                        }
                    });
                	
                }
                else
                {
	                var im_exist = false;                   
	                if(participant_listt != null){
	                    for (var iai = 0; iai < participant_listt.length ;iai++) {                                                              
	                        if (id_user == participant_listt[iai]['id_user']) {
	                            im_exist = true;
	                        }                        
	                    }
	                }

	                if (im_exist) {

                        
	                    notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut',"Anda telah join di kelas "+subject_name+' - '+subject_description);
	                    $("#sidebar").css('margin-top','3.5%');
	                    $("#kalender").css('display','block');
	                    $("#content_classdescription").css('display','none');
	                    $("#content_extraclass").css('display','none');
	                    $("#content_myclass").css('display','block'); 
	                    // $.ajax({
	                    //     url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
	                    //     type: 'POST',
	                    //     success: function(data)
	                    //     {              
	                            
	                    //     }
	                    // });
	                }
	                else
	                {
	                    if (class_type=="multicast") {typeclass="Multicast";}else if(class_type=="private"){typeclass="Private";}else{typeclass="Group";}
	                    if (template_type=="whiteboard_digital") {template_type="Digital Board";}else if(template_type=="whiteboard_videoboard"){template_type="Video Board";}else{template_type="All Features";}
	                    $("#desc_tutorname").append("<i class='zmdi zmdi-assignment-account m-r-10'></i> "+user_name);
	                    $("#desc_tutorgender").append("<i class='zmdi zmdi-male-female m-r-10'></i> "+user_gender);
	                    $("#desc_tutorage").append("<i class='zmdi zmdi-time-interval m-r-10'></i> "+user_age+" tahun");
	                    $("#desc_tutorimage").attr('src', '<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>'+user_image);

	                    $("#titleone").text(subject_name);
	                    $("#titletwo").text(subject_description);
                        $("#desc_jenjang").text(desc_jenjang);
	                    $("#desc_date").text(dateclass);
	                    $("#desc_starttime").text(timeshow+" - ");
	                    $("#desc_finishtime").text(endtimeshow);
	                    $("#desc_classtype").text(typeclass);
	                    $("#desc_templatetype").text(template_type);
	                    $("#nametutor").text(user_name);

	                    $(".buttonjoinn").attr('link','Transaction/multicast_join/'+stat_showdetail+'?t=join');

	                    $("#content_myclass").css('display','none');
	                    $("#content_extraclass").css('display','none');
	                    $("#sidebar").css('margin-top','0%');
	                    $("#kalender").css('display','none');
	                    $("#modal_extraclass").modal('hide');
	                    $("#content_classdescription").css('display','block');
	                }
	            }
            }
        });
        
        $(".buttonjoinn").click(function(){
            var link = "<?php echo base_url();?>"+$(this).attr('link');
            $.ajax({
                url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                type: 'POST',
                success: function(data)
                {              
                    window.location.replace(link);
                }
            });            
        });

        $(".buttonok").click(function(){
            // var link = "<?php echo base_url();?>"+$(this).attr('link');
            $.ajax({
                url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                type: 'POST',
                success: function(data)
                {              
                    // window.location.replace(link);
                }
            });            
        });
    }

    $('#selectkelas').change(function(e){
        var kelas = $("#selectkelas").val();

        if (kelas =="private") {
            $("#xboxprivateclass").css('display','block');
            $("#xboxgroupclass").css('display','none');
            $("#allclass").css('display','none');
        }
        else if (kelas =="group") {
            $("#xboxprivateclass").css('display','none');
            $("#xboxgroupclass").css('display','block');
            $("#allclass").css('display','none');
        }
        else if (kelas =="all") {
            $("#xboxprivateclass").css('display','block');
            $("#xboxgroupclass").css('display','block');
            $("#allclass").css('display','block');
        }
    });
    $("#btn_back_group").click(function(){
        $("#modal_groupclass").modal('hide');
        $("#modal_extraclass").modal('show');
    });
    $("#btn_back_search_group").click(function(){
        $("#kotakuntukpencariangroup").css('display','block');
        $("#kotakhasilpencariangroup").css('display','none');
        $("#btn_back_group").css('display','block');
        $("#btn_back_search_group").css('display','none');
    });
    $("#btn_back_private").click(function(){
        $("#modal_privateclass").modal('hide');
        $("#modal_extraclass").modal('show');
    });
    $("#btn_back_search_private").click(function(){
        $("#kotakuntukpencarianprivat").css('display','block');
        $("#kotakhasilpencarianprivat").css('display','none');
        $("#btn_back_private").css('display','block');
        $("#btn_back_search_private").css('display','none');
    });
    $("#privateclick").click(function(){
        $("#kotakuntukpencarianprivat").css('display','block');
        $("#kotakhasilpencarianprivat").css('display','none');
        // $("#sidebar").css('margin-top','0%');
    	// $("#kalender").css('display','none');
        $("#modal_extraclass").modal('hide');
        // $("#content_myclass").css('display','none');
        $("#content_classdescription").css('display','none');
        // $("#content_extraclass").css('display','block');
        $("#modal_privateclass").modal('show');
        $("#btn_back_private").css('display','block');
        $("#btn_back_search_private").css('display','none');

        $("#kotaknyagroup").removeClass('bgm-orange');
        $("#kotaknyagroup").addClass('bgm-gray');
        $("#kotaknyaprivate").removeClass('bgm-gray');
        $("#kotaknyaprivate").addClass('bgm-orange');

        // $("#box_groupclass").css('display','none');
        $("#privateclick").css('filter','grayscale();');
        $("#groupklik").css('filter','none');
        // $("#box_privateclass").css('display','block');
    });
    $("#groupclassclick").click(function(){
        $("#kotakuntukpencariangroup").css('display','block');
        $("#kotakhasilpencariangroup").css('display','none');
        $("#sidebar").css('margin-top','0%');
    	// $("#kalender").css('display','none');
        $("#modal_extraclass").modal('hide');
        $("#modal_groupclass").modal('show');
        // $("#content_myclass").css('display','none');
        // $("#content_classdescription").css('display','none');
        $("#content_extraclass").css('display','block');
        $("#kotaknyaprivate").removeClass('bgm-orange');
        $("#kotaknyaprivate").addClass('bgm-gray'); 
        $("#kotaknyagroup").removeClass('bgm-gray');
        $("#kotaknyagroup").addClass('bgm-orange');
        // $("#box_privateclass").css('display','none');
        $("#box_groupclass").css('opacity','100');        
        $("#durasigroup").addClass('col-md-12');
        // $("#box_groupclass").css('display','block');
    });
    $('#subject_id').change(function(e){
    $('#subjecton').val($(this).val());
    });
    $('#duration').change(function(e){
        $('#durationon').val($(this).val());
    });
    $('#subject_idg').change(function(e){
        $('#subjectong').val($(this).val());
    });
    $('#durationg').change(function(e){
        $('#durationong').val($(this).val());
    });

    // $("#btn_cari").click(function(){
    //     $("#modal_privateclass").modal('hide');
    // });

    $('#button_search').click(function(){
        // subject_id,dateon,timeeeeeee,duration
        var sbj_id      = $("#subject_id").val();
        var dton        = $("#dateon").val();        
        var drt         = $("#duration").val();        
        if (sbj_id == null) {            
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertsubjectnull'); ?>');    
        }
        else if(dton == ""){
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdatenull'); ?>');
        }
        else if(drt == null){
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdurationnull'); ?>');
        }   
        else
        {   
            $("#kotakhasilpencarianprivat").css('display','block');
            $("#kotakuntukpencarianprivat").css('display','none');
            $("#btn_back_private").css('display','none');
            $("#btn_back_search_private").css('display','block');
            angular.element(document.getElementById('button_search')).scope().getondemandprivate();
        }
    });

    $('#button_searchgroup').click(function(){
        // durationg,timeeeeee,dateong,subject_idg
        var sbj_idg      = $("#subject_idg").val();
        var dtong        = $("#dateong").val();        
        var drtg         = $("#durationg").val();
        if (sbj_idg == null) {            
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertsubjectnull'); ?>');    
        }
        else if(dtong == ""){
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdatenull'); ?>');
        }
        else if(drtg == null){
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdurationnull'); ?>');
        }   
        else
        {
            $("#kotakhasilpencariangroup").css('display','block');
            $("#kotakuntukpencariangroup").css('display','none');
            $("#btn_back_group").css('display','none');
            $("#btn_back_search_group").css('display','block');
            angular.element(document.getElementById('button_search')).scope().getondemandgroup();
        }
    });

    $(document.body).on('click', '.btn-ask' ,function(e){       
        $("#showalertpayment").css('display','none');
        $("#afterchoose").css('display','none');
        $("#beforechoose").css('display','block');
        $("#showdetailtutor").css('display','block');
        $("#topikprivate").val('');
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;

        var subject_id = $(this).attr('subject_id');
        var start_time = $(this).attr('start_time');
        var date = $(this).attr('dates');
        var duration = $(this).attr('duration');    
        var tutor_id = $(this).attr('tutor_id');

        var subject_idg = $(this).attr('subject_idg');
        var start_timeg = $(this).attr('start_timeg');
        var avtime_idg = $(this).attr('avtime_idg');
        var durationg = $(this).attr('durationg'); 

        var user_name = $(this).attr('username');
        var image_user = $(this).attr('image');
        var harga = $(this).attr('harga');
        var hargaakhir = $(this).attr('hargaakhir');
        var gh = $(this).attr('gh');
        var iduser = "<?php echo $this->session->userdata('id_user_kids'); ?>";

        if (iduser == null || iduser=="") {
            iduser  = "<?php echo $this->session->userdata('id_user'); ?>";
        }

        var tokenjwtt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        $('#namemodal').text(user_name);
        $('#timestart').text(start_time);
        $('#tanggalkelas').text(date);
        $('#hargakelas').text("Rp. "+hargaakhir);
        $('.imagetutor_private').attr("src", image_user);        

        if (subject_id == '' && start_time == '' && avtime_id == '' && duration == '') {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Harap isi data dengan lengkap");
        }
        else
        {
            // $.ajax({
            //     url: '<?php echo base_url(); ?>/Rest/isEnoughBalance/access_token/'+tokenjwtt,
            //     type: 'POST',
            //     data: {
            //         id_user: iduser,
            //         gh : gh
            //     },               
            //     success: function(data)
            //     {
            //         balanceuser = data['balance'];

            //         if (data['status'])
            //         {
                         $('.btn-req-demand').attr('demand-link','<?php echo base_url(); ?>/process/demand_me?start_time='+start_time+"&tutor_id="+tutor_id+"&subject_id="+subject_id+"&duration="+duration+"&date="+date+"&user_utc="+user_utc+"&hr="+harga+"&hrt="+hargaakhir+"&id_user="+iduser);
                        $('#modalconfrim').modal('show');
                        $('#modal_privateclass').modal('hide');
            //         } else {
            //             notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
            //         }
            //     }
            // });
        }
    });

    $(document.body).on('click', '.btn-req-demand' ,function(e){
        var topic= $("#topikprivate").val();
        var link = $(this).attr('demand-link');
        link     = link+"&topic="+topic;        
        if (topic == "") {
            $("#alerttopik").css('display','block');
        }
        else
        {             
            $('#modalconfrim').modal('hide');   
            angular.element(document.getElementById('getloading')).scope().loadings();
            $('.btn-req-demand').css('disabled','true');                        
            $.get(link,function(response){
                var a = JSON.parse(response);               
                if (a['status'] == 1) {
                    // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"<?php echo $this->lang->line('successfullydemand');?>");     
                    $('#modalconfrim').modal('show');           
                    $("#beforechoose").css('display','none');
                    $("#showdetailtutor").css('display','none');
                    $("#showalertpayment").css('display','block');
                    $("#afterchoose").css('display','block');
                    setTimeout(function(){
                        location.reload();
                    },3500);
                }
                else
                {
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"<?php echo $this->lang->line('faileddemand');?>");
                    setTimeout(function(){              
                        location.reload();
                    },4000);
                }
                angular.element(document.getElementById('getloading')).scope().loadingsclose();
            });
        }
    }); 

    $(document.body).on('click','.btn-next-demand', function(e){
        $('#modalconfrim').modal("hide");
    }); 

    $(document.body).on('click','.btn-view-demand', function(e){
        location.reload();
    }); 

    $("#box_privateclass").css('opacity','100');
    $("#box_groupclass").css('opacity','0');    
    $("#kotaknyagroup").removeClass('bgm-orange');
    $("#kotaknyagroup").addClass('bgm-gray');   
    $("#btn_extraclass").click(function(){
        $("#modal_extraclass").modal('show');
        // $("#content_myclass").css('display','none');
        // $("#content_extraclass").css('display','block');
    });
    $(".btn_back_home").click(function(){
        $("#sidebar").css('margin-top','3.5%');
        $("#kalender").css('display','block');
        $("#content_classdescription").css('display','none');
        $("#content_myclass").css('display','block');
        $("#content_extraclass").css('display','none');
    }); 
    $(document).ready(function() {

            var date = null;
            var tgll = new Date();  
            var formattedDatee = moment(tgll).format('YYYY-MM-DD'); 
            var a = [];
            var b = [];            
            var c = [];
            var d = [];
            var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";            
            
            setTimeout(function(){
                // $("#kotakhasilpencarianprivat").css('display', 'block');                
                // $("#kotakhasilpencariangroup").css('display', 'block'); 
            },300);

            $( "#subject_id" ).change(function() {
                angular.element(document.getElementById('subject_id')).scope().clearSearch();
            });
            $( "#duration").change(function() {                
                angular.element(document.getElementById('duration')).scope().clearSearch();
            });

            $('#dateon').on('dp.change', function(e) {    
                var tglll = new Date();              
                angular.element(document.getElementById('dateon')).scope().clearSearch();
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tglll.getHours(); 
                // alert(tgl);
                if (hoursnoww == 23) {                    
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                    $("#button_search").attr('disabled');
                    $("#button_searchgroup").attr('disabled');
                }
                else
                {
                    if (date == formattedDatee) {
                        a = [];
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        for (var i = hoursnoww+2; i < 24; i++) {                                                
                            a.push(i);
                        }                        
                        $('#timeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: a,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        b = [];
                        for (var i = 0; i < 24; i++) {                                                
                            b.push(i);
                        }                        
                        $('#timeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: b,
                        });
                    }
                }
            });

            $( "#subject_idg" ).change(function() {
                angular.element(document.getElementById('subject_id')).scope().clearSearch();
            });
            $( "#durationg").change(function() {                
                angular.element(document.getElementById('duration')).scope().clearSearch();
            });

            $('#dateong').on('dp.change', function(e) {
                angular.element(document.getElementById('dateong')).scope().clearSearch();
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tgll.getHours(); 
                // alert(tgl);
                
                if (hoursnoww == 23) {
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                }
                else
                {
                    if (date == formattedDatee) {
                        c = [];
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        for (var i = hoursnoww+2; i < 24; i++) {                                                
                            c.push(i);
                        }                        
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: c,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        d = [];
                        for (var i = 0; i < 24; i++) {                                                
                            d.push(i);
                        }                        
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: d,
                        });
                    }
                }
            });
            var tgll = new Date();
            var formattedDatee = moment(tgll).format('YYYY-MM-DD'); 
            $('#dateon').on('dp.change', function(e) {                  
                angular.element(document.getElementById('dateon')).scope().clearSearch();
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tgll.getHours(); 
                // alert(tgl);
                if (hoursnoww == 23) {                    
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                    $("#button_search").attr('disabled');
                    $("#button_searchgroup").attr('disabled');
                }
                else
                {
                    if (date == formattedDatee) {
                        a = [];
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        for (var i = hoursnoww+2; i < 24; i++) {                                                
                            a.push(i);
                        }                       
                        $('#timeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: a,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        b = [];
                        for (var i = 0; i < 24; i++) {                                                
                            b.push(i);
                        }                       
                        $('#timeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: b,
                        });
                    }
                }
            });

            $('#dateong').on('dp.change', function(e) {                
                angular.element(document.getElementById('dateong')).scope().clearSearch();
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tgll.getHours(); 
                // alert(tgl);
                
                if (hoursnoww == 23) {
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                }
                else
                {
                    if (date == formattedDatee) {
                        c = [];
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        for (var i = hoursnoww+2; i < 24; i++) {                                                
                            c.push(i);
                        }                       
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: c,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        d = [];
                        for (var i = 0; i < 24; i++) {                                                
                            d.push(i);
                        }                       
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: d,
                        });
                    }
                }
            });

            $(function () {
                var tgl = new Date();  
                var formattedDate = moment(tgl).format('YYYY-MM-DD');
                $('#dateon').datetimepicker({  
                    minDate: moment(formattedDate, 'YYYY-MM-DD')
                });
                $('#dateong').datetimepicker({  
                    minDate: moment(formattedDate, 'YYYY-MM-DD')
                }); 
            });

            $('select.select2').each(function(){      
                if($(this).attr('id') == 'tagorang'){
                    $(this).select2({
                      tags:["",],
                      tokenSeparators: [","],
                      maximumSelectionLength: 3
                    }); 
                }  
                else if($(this).attr('id') == 'tagteman'){

                    $(this).select2({
                      tags:["",],
                      tokenSeparators: [","],
                      maximumSelectionLength: 3
                    });
                }                                             
            });

            var namaa = [];
            var ininama;
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/getnameuser',
                type: 'POST',               
                success: function(data)
                {   
                    // var a = JSON.stringify(data['response']);
                    // var b = JSON.parse(a);
                    ininama = data['response'];                    
                    // console.warn(b);
                    // console.warn(b.length);
                    for (i = 0; i < b.length; i++) {
                        var maskid = "";
                        var myemailId =  b[i].email;
                        var prefix= myemailId.substring(0, myemailId .lastIndexOf("@"));
                        var postfix= myemailId.substring(myemailId .lastIndexOf("@"));

                        for(var j=0; j<prefix.length; j++){
                            if(j >= prefix.length - 4 ) {   
                                maskid = maskid + "*";////////
                            }
                            else {
                                maskid = maskid + prefix[j].toString();
                            }
                        }
                        maskid =maskid +postfix;
                        namaa.push(b[i].user_name+" ("+maskid+")");
                    }
                }
            }); 

            $(document).on("click", ".openmodaladd", function () {
                var that = $(this);
                var a = $(this).attr('tutor_id');
                var b = $("."+a).attr('idtutor');
                var c = $(".openmodaladd."+a).attr('tutor_id');
                var iduser = "<?php echo $this->session->userdata('id_user_kids'); ?>";

                if (iduser == null || iduser=="") {
                    iduser  = "<?php echo $this->session->userdata('id_user'); ?>";
                }
                var tokenjwttt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
                var harga = $(this).attr('harga'); 
                var hargatutor = $(this).attr('hargatutor');
                var gh = $(this).attr('gh');
                var date = $(this).attr('dates');

                var user_utc = new Date().getTimezoneOffset();
                user_utc = -1 * user_utc;                                
                
                if (a == c) {
                    var subject_idg = that.attr('subject_id');
                    var start_timeg = that.attr('start_time');
                    var avtime_idg = that.attr('avtime_id');
                    var durationg = that.attr('duration');
                    var user_name = that.attr('username');
                    var image_user = that.attr('image');
                    var tutor_id = c;

                    $('#namemodal_group').text(user_name);
                    $('#timestart_group').text(start_timeg);
                    $('#tanggalkelas_group').text(date);
                    $('#hargakelas_group').text("Rp. "+harga);
                    $('.imagetutor_private').attr("src", image_user);
                    $(".modal-body #data_subject_id").val( subject_idg );
                    $(".modal-body #data_start_time").val( start_timeg );
                    $(".modal-body #data_avtime_id").val( avtime_idg );
                    $(".modal-body #data_duration").val( durationg );
                    $(".modal-body #data_metod").val( 'bayar_sendiri' );                                      
                    
                    $('.btn-req-group-demand').attr('demand-link-group','<?php echo base_url(); ?>process/demand_me_grup?start_time='+start_timeg+"&tutor_id="+tutor_id+"&subject_id="+subject_idg+"&duration="+durationg+"&metod=bayar_sendiri&date="+date+"&user_utc="+user_utc+"&harga="+harga+"&harga_tutor="+hargatutor);
                    $("#modaladdfriends").modal('show');
                    $("#modal_groupclass").modal('hide');
                }   
            });

            $(document.body).on('click', '.btn-req-group-demand' ,function(e){
                var topic = $("#topikgroup").val();               
                if (topic == "") {
                    $("#alertemailnotfound").css('display','none');
                    $("#alertemailempty").css('display','none');
                    $("#alerttopikgroup").css('display','block');                    
                }
                else
                {
                    $('#modaladdfriends').modal('hide');   
                    angular.element(document.getElementById('getloading')).scope().loadings();                  
                    $(".btn-req-group-demand").attr('disabled','true');
                    var link = $(".btn-req-group-demand").attr('demand-link-group')+"&id_friends=''&topic="+topic;
                    $.get(link,function(data){
                        angular.element(document.getElementById('getloading')).scope().loadingsclose();
                        
                        $('#modalconfrim').modal('show');           
                        $("#beforechoose").css('display','none');
                        $("#showdetailtutor").css('display','none');
                        $("#alertemailsendiri").css('display','none');
                        $("#showalertpayment").css('display','block');
                        $("#afterchoose").css('display','block');
                        setTimeout(function(){
                            location.reload();
                        },3500);
                    });
                }
            });   
    });    
    $("#btn_extraclass").click(function(){
        $("#modal_extraclass").modal('show');
        // $("#content_myclass").css('display','none');
        // $("#content_extraclass").css('display','block');
    });
    $(".btn_back_home").click(function(){
        $("#sidebar").css('margin-top','3.5%');
        $("#kalender").css('display','block');
        $("#content_classdescription").css('display','none');
        $("#content_myclass").css('display','block');
        $("#content_extraclass").css('display','none');
    });
    $("#btn_back_group").click(function(){
        $("#modal_groupclass").modal('hide');
        $("#modal_extraclass").modal('show');
    });
    $("#btn_back_search_group").click(function(){
        $("#kotakuntukpencariangroup").css('display','block');
        $("#kotakhasilpencariangroup").css('display','none');
        $("#btn_back_group").css('display','block');
        $("#btn_back_search_group").css('display','none');
    });
    $("#btn_back_private").click(function(){
        $("#modal_privateclass").modal('hide');
        $("#modal_extraclass").modal('show');
    });
    $("#btn_back_search_private").click(function(){
        $("#kotakuntukpencarianprivat").css('display','block');
        $("#kotakhasilpencarianprivat").css('display','none');
        $("#btn_back_private").css('display','block');
        $("#btn_back_search_private").css('display','none');
    });
    $("#privateclick").click(function(){
        $("#kotakuntukpencarianprivat").css('display','block');
        $("#kotakhasilpencarianprivat").css('display','none');
        // $("#sidebar").css('margin-top','0%');
        // $("#kalender").css('display','none');
        $("#modal_extraclass").modal('hide');
        // $("#content_myclass").css('display','none');
        $("#content_classdescription").css('display','none');
        // $("#content_extraclass").css('display','block');
        $("#modal_privateclass").modal('show');
        $("#btn_back_private").css('display','block');
        $("#btn_back_search_private").css('display','none');

        $("#kotaknyagroup").removeClass('bgm-orange');
        $("#kotaknyagroup").addClass('bgm-gray');
        $("#kotaknyaprivate").removeClass('bgm-gray');
        $("#kotaknyaprivate").addClass('bgm-orange');

        // $("#box_groupclass").css('display','none');
        $("#privateclick").css('filter','grayscale();');
        $("#groupklik").css('filter','none');
        // $("#box_privateclass").css('display','block');
    });
    $("#groupclassclick").click(function(){
        $("#kotakuntukpencariangroup").css('display','block');
        $("#kotakhasilpencariangroup").css('display','none');
        $("#sidebar").css('margin-top','0%');
        // $("#kalender").css('display','none');
        $("#modal_extraclass").modal('hide');
        $("#modal_groupclass").modal('show');
        // $("#content_myclass").css('display','none');
        // $("#content_classdescription").css('display','none');
        $("#content_extraclass").css('display','block');
        $("#kotaknyaprivate").removeClass('bgm-orange');
        $("#kotaknyaprivate").addClass('bgm-gray'); 
        $("#kotaknyagroup").removeClass('bgm-gray');
        $("#kotaknyagroup").addClass('bgm-orange');
        // $("#box_privateclass").css('display','none');
        // $("#box_groupclass").css('opacity','100');        
        $("#groupclassclick").css('filter','grayscale();');
        $("#durasigroup").addClass('col-md-12');
        // $("#box_groupclass").css('display','block');
    });
    $('#subject_id').change(function(e){
    $('#subjecton').val($(this).val());
    });
    $('#duration').change(function(e){
        $('#durationon').val($(this).val());
    });
    $('#subject_idg').change(function(e){
        $('#subjectong').val($(this).val());
    });
    $('#durationg').change(function(e){
        $('#durationong').val($(this).val());
    });

    // $("#btn_cari").click(function(){
    //     $("#modal_privateclass").modal('hide');
    // });

    $('#button_search').click(function(){
        // subject_id,dateon,timeeeeeee,duration
        var sbj_id      = $("#subject_id").val();
        var dton        = $("#dateon").val();        
        var drt         = $("#duration").val();        
        if (sbj_id == null) {            
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertsubjectnull'); ?>');    
            $("#modal_privateclass").modal('hide');
        }
        else if(dton == ""){
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdatenull'); ?>');
            $("#modal_privateclass").modal('hide');
        }
        else if(drt == null){
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdurationnull'); ?>');
            $("#modal_privateclass").modal('hide');
        }   
        else
        {   
            $("#kotakhasilpencarianprivat").css('display','block');
            $("#kotakuntukpencarianprivat").css('display','none');
            $("#btn_back_private").css('display','none');
            $("#btn_back_search_private").css('display','block');
            angular.element(document.getElementById('button_search')).scope().getondemandprivate();
        }
    });

    $('#button_searchgroup').click(function(){
        // durationg,timeeeeee,dateong,subject_idg
        var sbj_idg      = $("#subject_idg").val();
        var dtong        = $("#dateong").val();        
        var drtg         = $("#durationg").val();
        if (sbj_idg == null) {            
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertsubjectnull'); ?>');    
            $("#modal_groupclass").modal('hide');
        }
        else if(dtong == ""){
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdatenull'); ?>');
            $("#modal_groupclass").modal('hide');
        }
        else if(drtg == null){
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdurationnull'); ?>');
            $("#modal_groupclass").modal('hide');
        }   
        else
        {
            $("#kotakhasilpencariangroup").css('display','block');
            $("#kotakuntukpencariangroup").css('display','none');
            $("#btn_back_group").css('display','none');
            $("#btn_back_search_group").css('display','block');
            angular.element(document.getElementById('button_search')).scope().getondemandgroup();
        }
    });
</script>
<!-- End Script Extra Class -->

<script type="text/javascript" src="<?php echo base_url();?>aset/js/magicsuggest/magicsuggest.js"></script>    

<script type="text/javascript">
    $(document).ready(function(){
        var kliktgl = null;
        var iduser = "<?php echo $this->session->userdata('id_user_kids'); ?>";

        if (iduser == null || iduser=="") {
            iduser  = "<?php echo $this->session->userdata('id_user'); ?>";
        }
        // var iduser = "<?php echo $this->session->userdata('id_user'); ?>";
        var tgl = new Date();
        
        var formattedDate = moment(tgl).format('YYYY-MM-DD');        
        var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes();  

        var schedule_time = [];
        var nowtime = [];
        var intervalTime = [];
        var finished = [];
        var finish_time = [];
        var threadHandler = [];
        var hour = [];
        var minute = [];
        var second = [];
        var idnya = null;
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";	        
        var user_utc = -1 * new Date().getTimezoneOffset();
        var currentDate = new Date(new Date().getTime());
        var kemarin = new Date(new Date().getTime() -1 * 24 * 60 * 60 * 1000);
        // alert(kemarin);
        var day = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();

        var tgl = new Date();
        var kliktgl = null;
        var iduser = "<?php echo $this->session->userdata('id_user_kids'); ?>";

        if (iduser == null || iduser=="") {
            iduser  = "<?php echo $this->session->userdata('id_user'); ?>";
        }
        
        var formattedDate = moment(tgl).format('YYYY-MM-DD');
        var hari = moment(tgl).format('ddd');        
        var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes();  

        var schedule_time = [];
        var nowtime = [];
        var intervalTime = [];
        var finished = [];
        var finish_time = [];
        var threadHandler = [];
        var hour = [];
        var minute = [];
        var second = [];
        var idnya = null;
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";

        var mydate = new Date();
        var str = month + ' ' + tgl.getFullYear();
         //Variable untuk tampilan tanggal
        var today = new Date(tgl.getTime());
        var tomorrow = new Date(tgl.getTime() + 1 * 24 * 60 * 60 * 1000);
        var twodays = new Date(tgl.getTime() + 2 * 24 * 60 * 60 * 1000);
        var threedays = new Date(tgl.getTime() + 3 * 24 * 60 * 60 * 1000);
        var fourdays = new Date(tgl.getTime() + 4 * 24 * 60 * 60 * 1000);
        var fivedays = new Date(tgl.getTime() + 5 * 24 * 60 * 60 * 1000);
        var sixdays = new Date(tgl.getTime() + 6 * 24 * 60 * 60 * 1000);

        //Variable untuk disamakan dengan format database
        var bln_tgl1 = moment(today).format('YYYY-MM-DD');
        var bln_tgl2 = moment(tomorrow).format('YYYY-MM-DD');
        var bln_tgl3 = moment(twodays).format('YYYY-MM-DD');
        var bln_tgl4 = moment(threedays).format('YYYY-MM-DD');
        var bln_tgl5 = moment(fourdays).format('YYYY-MM-DD');
        var bln_tgl6 = moment(fivedays).format('YYYY-MM-DD');
        var bln_tgl7 = moment(sixdays).format('YYYY-MM-DD');

        //Variable untuk tampilan hari
        var hari1 = moment(today).format('ddd');
        var hari2 = moment(tomorrow).format('ddd');
        var hari3 = moment(twodays).format('ddd');
        var hari4 = moment(threedays).format('ddd');
        var hari5 = moment(fourdays).format('ddd');
        var hari6 = moment(fivedays).format('ddd');
        var hari7 = moment(sixdays).format('ddd');

        //Variable untuk tampilan hari
        var bulan1 = moment(today).format('MMM');
        var bulan2 = moment(tomorrow).format('MMM');
        var bulan3 = moment(twodays).format('MMM');
        var bulan4 = moment(threedays).format('MMM');
        var bulan5 = moment(fourdays).format('MMM');
        var bulan6 = moment(fivedays).format('MMM');
        var bulan7 = moment(sixdays).format('MMM');

        //Variable untuk tampilan bulan
        var bulan = moment(currentDate).format('MMMM - YYYY');
        var tglafter1 = new Date(new Date().getTime() + 0 * 24 * 60 * 60 * 1000);
        var tglafter2 = new Date(new Date().getTime() + 1 * 24 * 60 * 60 * 1000);
        var tglafter3 = new Date(new Date().getTime() + 2 * 24 * 60 * 60 * 1000);
        var tglafter4 = new Date(new Date().getTime() + 3 * 24 * 60 * 60 * 1000);
        var tglafter5 = new Date(new Date().getTime() + 4 * 24 * 60 * 60 * 1000);
        var tglafter6 = new Date(new Date().getTime() + 5 * 24 * 60 * 60 * 1000);
        var tglafter7 = new Date(new Date().getTime() + 6 * 24 * 60 * 60 * 1000);
        var bahasa = "<?php echo $this->session->userdata('lang');?>";
        var code = null;
        var cekk = setInterval(function(){
            code = "<?php echo $this->session->userdata('tutorinclass');?>";            
            cek();
        },500);

        var $subscribeInput = $('input[name="orderKrnj[]"]');

        $subscribeInput.on('click', function(){
            if ( $(this).is(':checked') )
                $("#btnbayar").removeAttr('disabled');
            else
                $("#btnbayar").attr('disabled','disabled');
        });

        $(function() {
            //Tampilan Bulan di Kalender
            $("#bln_ini").html(bulan);

            //Untuk mencocokan dengan yang ada didatabase
            $("#dt").html(tgl.getFullYear() + '-' + (today.getMonth()+1) + '-' + today.getDate());
            $("#dt2").html(tgl.getFullYear() + '-' + (tomorrow.getMonth()+1) + '-' +(tomorrow.getDate()));
            $("#dt3").html(tgl.getFullYear() + '-' + (twodays.getMonth()+1) + '-' +(twodays.getDate()));
            $("#dt4").html(tgl.getFullYear() + '-' + (threedays.getMonth()+1) + '-' +(threedays.getDate()));
            $("#dt5").html(tgl.getFullYear() + '-' + (fourdays.getMonth()+1) + '-' +(fourdays.getDate()));
            $("#dt6").html(tgl.getFullYear() + '-' + (fivedays.getMonth()+1) + '-' +(fivedays.getDate()));
            $("#dt7").html(tgl.getFullYear() + '-' + (sixdays.getMonth()+1) + '-' +(sixdays.getDate()));

            //Tampilan untuk tanggal di Kalender
            $("#d1").html(hari + ', ' +(today.getDate())  + ' ' +bulan1);
            $("#d2").html(hari2 + ', ' +(tomorrow.getDate())  + ' ' +bulan2);
            $("#d3").html(hari3 + ', ' +(twodays.getDate())  + ' ' +bulan3);
            $("#d4").html(hari4 + ', ' +(threedays.getDate())  + ' ' +bulan4);
            $("#d5").html(hari5 + ', ' +(fourdays.getDate())  + ' ' +bulan5);
            $("#d6").html(hari6 + ', ' +(fivedays.getDate())  + ' ' +bulan6);
            $("#d7").html(hari7 + ', ' +(sixdays.getDate())  + ' ' +bulan7);
            
            $("#d1").click(function(){

                $("#d1").css("color","yellow");
                $("#d2").css("color","white");
                $("#d3").css("color","white");
                $("#d4").css("color","white");
                $("#d5").css("color","white");
                $("#d6").css("color","white");
                $("#d7").css("color","white");
                var aj = $("#dt").text();

                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        

                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d3").click(function(){

                $("#d1").css("color","white");
                $("#d2").css("color","white");
                $("#d3").css("color","yellow");
                $("#d4").css("color","white");
                $("#d5").css("color","white");
                $("#d6").css("color","white");
                $("#d7").css("color","white");
                var aj = $("#dt3").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d2").click(function(){

                $("#d1").css("color","white");
                $("#d2").css("color","yellow");
                $("#d3").css("color","white");
                $("#d4").css("color","white");
                $("#d5").css("color","white");
                $("#d6").css("color","white");
                $("#d7").css("color","white");
                var aj = $("#dt2").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d4").click(function(){

                $("#d1").css("color","white");
                $("#d2").css("color","white");
                $("#d3").css("color","white");
                $("#d4").css("color","yellow");
                $("#d5").css("color","white");
                $("#d6").css("color","white");
                $("#d7").css("color","white");
                var aj = $("#dt4").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d5").click(function(){
                $("#d1").css("color","white");
                $("#d2").css("color","white");
                $("#d3").css("color","white");
                $("#d4").css("color","white");
                $("#d5").css("color","yellow");
                $("#d6").css("color","white");
                $("#d7").css("color","white");
                var aj = $("#dt5").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d6").click(function(){
                $("#d1").css("color","white");
                $("#d2").css("color","white");
                $("#d3").css("color","white");
                $("#d4").css("color","white");
                $("#d5").css("color","white");
                $("#d6").css("color","yellow");
                $("#d7").css("color","white");
                var aj = $("#dt6").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d7").click(function(){
                $("#d1").css("color","white");
                $("#d2").css("color","white");
                $("#d3").css("color","white");
                $("#d4").css("color","white");
                $("#d5").css("color","white");
                $("#d6").css("color","white");
                $("#d7").css("color","yellow");
                var aj = $("#dt7").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
        });

        $("#nextbtn").click(function() { 
                
                $("#d1").css("color","yellow");
                $("#d2").css("color","white");
                $("#d3").css("color","white");
                $("#d4").css("color","white");
                $("#d5").css("color","white");
                $("#d6").css("color","white");
                $("#d7").css("color","white");
                currentDate.setDate(currentDate.getDate() + 1);
                var tglafter1 = (currentDate.getTime() + 0 * 24 * 60 * 60 * 1000);
                var tglafter2 = (currentDate.getTime() + 1 * 24 * 60 * 60 * 1000);
                var tglafter3 = (currentDate.getTime() + 2 * 24 * 60 * 60 * 1000);
                var tglafter4 = (currentDate.getTime() + 3 * 24 * 60 * 60 * 1000);
                var tglafter5 = (currentDate.getTime() + 4 * 24 * 60 * 60 * 1000);
                var tglafter6 = (currentDate.getTime() + 5 * 24 * 60 * 60 * 1000);
                var tglafter7 = (currentDate.getTime() + 6 * 24 * 60 * 60 * 1000);
                // var testt = moment(tglafter5).format('DD');
                // alert(testt);

                var bulan = moment(currentDate).format('MMMM - YYYY');

                var tgl1 = moment(tglafter1).format('DD');
                var tgl2 = moment(tglafter2).format('DD');
                var tgl3 = moment(tglafter3).format('DD');
                var tgl4 = moment(tglafter4).format('DD');
                var tgl5 = moment(tglafter5).format('DD');
                var tgl6 = moment(tglafter6).format('DD');
                var tgl7 = moment(tglafter7).format('DD');

                var hari1 = moment(tglafter1).format('ddd');
                var hari2 = moment(tglafter2).format('ddd');
                var hari3 = moment(tglafter3).format('ddd');
                var hari4 = moment(tglafter4).format('ddd');
                var hari5 = moment(tglafter5).format('ddd');
                var hari6 = moment(tglafter6).format('ddd');
                var hari7 = moment(tglafter7).format('ddd');

                var bln1 = moment(tglafter1).format('MM');
                var bln2 = moment(tglafter2).format('MM');
                var bln3 = moment(tglafter3).format('MM');
                var bln4 = moment(tglafter4).format('MM');
                var bln5 = moment(tglafter5).format('MM');
                var bln6 = moment(tglafter6).format('MM');
                var bln7 = moment(tglafter7).format('MM');

                var year1 = moment(tglafter1).format('YYYY');
                var year2 = moment(tglafter2).format('YYYY');
                var year3 = moment(tglafter3).format('YYYY');
                var year4 = moment(tglafter4).format('YYYY');
                var year5 = moment(tglafter5).format('YYYY');
                var year6 = moment(tglafter6).format('YYYY');
                var year7 = moment(tglafter7).format('YYYY');


                //Untuk tampilan di kalender
                var bulan1 = moment(tglafter1).format('MMM');
                var bulan2 = moment(tglafter2).format('MMM');
                var bulan3 = moment(tglafter3).format('MMM');
                var bulan4 = moment(tglafter4).format('MMM');
                var bulan5 = moment(tglafter5).format('MMM');
                var bulan6 = moment(tglafter6).format('MMM');
                var bulan7 = moment(tglafter7).format('MMM');


                $("#bln_ini").html(bulan);

                //Untuk mencocokan dengan yang ada didatabase

                $("#dt").html(year1 + '-' + bln1 + '-' + tgl1);
                $("#dt2").html(year2 + '-' + bln2 + '-' + tgl2);
                $("#dt3").html(year3 + '-' + bln3 + '-' + tgl3);
                $("#dt4").html(year4 + '-' + bln4 + '-' + tgl4);
                $("#dt5").html(year5 + '-' + bln5 + '-' + tgl5);
                $("#dt6").html(year6 + '-' + bln6 + '-' + tgl6);
                $("#dt7").html(year7 + '-' + bln7 + '-' + tgl7);

                //Tampilan untuk tanggal di Kalender
                // $("#d1").html(hari + ', ' +(today.getDate())  + ' ' +bulan1);

                
                $("#d1").html(hari1 + ', ' + tgl1 + ' ' + bulan1);
                $("#d2").html(hari2 + ', ' + tgl2 + ' ' + bulan2);
                $("#d3").html(hari3 + ', ' + tgl3 + ' ' + bulan3);
                $("#d4").html(hari4 + ', ' + tgl4 + ' ' + bulan4);
                $("#d5").html(hari5 + ', ' + tgl5 + ' ' + bulan5);
                $("#d6").html(hari6 + ', ' + tgl6 + ' ' + bulan6);
                $("#d7").html(hari7 + ', ' + tgl7 + ' ' + bulan7);
                var aj = $("#dt").text();
                var cekhari = tgl.getDay();
                // alert(aj);
                
                kliktgl = aj;
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $("d1").addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
        });

        $("#prevbtn").click(function() {
            $("#d1").css("color","yellow");
            $("#d2").css("color","white");
            $("#d3").css("color","white");
            $("#d4").css("color","white");
            $("#d5").css("color","white");
            $("#d6").css("color","white");
            $("#d7").css("color","white");

            currentDate.setDate(currentDate.getDate() -1);
            var tglafter1 = (currentDate.getTime() + 0 * 24 * 60 * 60 * 1000);
            var tglafter2 = (currentDate.getTime() + 1 * 24 * 60 * 60 * 1000);
            var tglafter3 = (currentDate.getTime() + 2 * 24 * 60 * 60 * 1000);
            var tglafter4 = (currentDate.getTime() + 3 * 24 * 60 * 60 * 1000);
            var tglafter5 = (currentDate.getTime() + 4 * 24 * 60 * 60 * 1000);
            var tglafter6 = (currentDate.getTime() + 5 * 24 * 60 * 60 * 1000);
            var tglafter7 = (currentDate.getTime() + 6 * 24 * 60 * 60 * 1000);

            var bulan = moment(currentDate).format('MMMM - YYYY');

            var tgl1 = moment(tglafter1).format('DD');
            var tgl2 = moment(tglafter2).format('DD');
            var tgl3 = moment(tglafter3).format('DD');
            var tgl4 = moment(tglafter4).format('DD');
            var tgl5 = moment(tglafter5).format('DD');
            var tgl6 = moment(tglafter6).format('DD');
            var tgl7 = moment(tglafter7).format('DD');

            var hari1 = moment(tglafter1).format('ddd');
            var hari2 = moment(tglafter2).format('ddd');
            var hari3 = moment(tglafter3).format('ddd');
            var hari4 = moment(tglafter4).format('ddd');
            var hari5 = moment(tglafter5).format('ddd');
            var hari6 = moment(tglafter6).format('ddd');
            var hari7 = moment(tglafter7).format('ddd');

            var bln1 = moment(tglafter1).format('MM');
            var bln2 = moment(tglafter2).format('MM');
            var bln3 = moment(tglafter3).format('MM');
            var bln4 = moment(tglafter4).format('MM');
            var bln5 = moment(tglafter5).format('MM');
            var bln6 = moment(tglafter6).format('MM');
            var bln7 = moment(tglafter7).format('MM');

            var year1 = moment(tglafter1).format('YYYY');
            var year2 = moment(tglafter2).format('YYYY');
            var year3 = moment(tglafter3).format('YYYY');
            var year4 = moment(tglafter4).format('YYYY');
            var year5 = moment(tglafter5).format('YYYY');
            var year6 = moment(tglafter6).format('YYYY');
            var year7 = moment(tglafter7).format('YYYY');


            //Untuk tampilan di kalender
            var bulan1 = moment(tglafter1).format('MMM');
            var bulan2 = moment(tglafter2).format('MMM');
            var bulan3 = moment(tglafter3).format('MMM');
            var bulan4 = moment(tglafter4).format('MMM');
            var bulan5 = moment(tglafter5).format('MMM');
            var bulan6 = moment(tglafter6).format('MMM');
            var bulan7 = moment(tglafter7).format('MMM');


            $("#bln_ini").html(bulan);

            //Untuk mencocokan dengan yang ada didatabase

            $("#dt").html(year1 + '-' + bln1 + '-' + tgl1);
            $("#dt2").html(year2 + '-' + bln2 + '-' + tgl2);
            $("#dt3").html(year3 + '-' + bln3 + '-' + tgl3);
            $("#dt4").html(year4 + '-' + bln4 + '-' + tgl4);
            $("#dt5").html(year5 + '-' + bln5 + '-' + tgl5);
            $("#dt6").html(year6 + '-' + bln6 + '-' + tgl6);
            $("#dt7").html(year7 + '-' + bln7 + '-' + tgl7);

            //Tampilan untuk tanggal di Kalender
            // $("#d1").html(hari + ', ' +(today.getDate())  + ' ' +bulan1);

            $("#d1").addClass("active");
            $("#d1").html(hari1 + ', ' + tgl1 + ' ' + bulan1);
            $("#d2").html(hari2 + ', ' + tgl2 + ' ' + bulan2);
            $("#d3").html(hari3 + ', ' + tgl3 + ' ' + bulan3);
            $("#d4").html(hari4 + ', ' + tgl4 + ' ' + bulan4);
            $("#d5").html(hari5 + ', ' + tgl5 + ' ' + bulan5);
            $("#d6").html(hari6 + ', ' + tgl6 + ' ' + bulan6);
            $("#d7").html(hari7 + ', ' + tgl7 + ' ' + bulan7);
            var aj = $("#dt").text();
            var cekhari = tgl.getDay();
            // alert(aj);
            
            kliktgl = aj;
            if (formattedDate != kliktgl) 
            {                              
                $('.c').removeClass("active");
                $("#d1").addClass("active");
                // alert(aj);
                formattedDate = kliktgl;        
                getschedule(aj);
            }
            else
            {
                formattedDate = moment(tgl).format('YYYY-MM-DD');
            }
        });

        function cek(){
            if (code == "4044") {
                $("#alertalreadyonline").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url(); ?>Rest/clearsession',
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
            else
            {
                $("#alertalreadyonline").css('display','none');
            }
        }

        getschedule(formattedDate);       
        setInterval(function(){  
			// getschedule(formattedDate);       
            // window.location.reload();
		},60000);
        
        function timeToSeconds(time) {
            time = time.split(/:/);
            return time[0] * 3600 + time[1] * 60;
        }  

        function dateTimeToSeconds(datetime){
            dt = datetime.split(" ");
            date = dt[0].split("-");
            time = dt[1].split(":");
            return date[0];
            // return JSON.stringify(dt);
        } 
        $(document).on("click", ".inviteGroup", function () {
            var classid = $(this).data('id');
            var imgTutor = $(this).data('image_tutor');
            var nameTutor = $(this).data('tutorname')
            var subject = $(this).data('subject');
            var descriptionclass = $(this).data('description');
            var timeclass = $(this).data('timeclass');


            $("#imgTutor").attr("src", "<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+imgTutor);
            $("#subjectkelas").text(subject);
            $("#id_kelas").val(classid);
            $("#namatutor").text(nameTutor);
            $("#deskripsiekelas").text(descriptionclass);
            $("#waktukelas").text(timeclass);
            $("#modalinvitefriends").modal('show');
        });
        $(document).on("click", ".btnInvGroup", function () {
            var classid = $("#id_kelas").val();
            var emailfriend = $("#tagteman").val()+ '';                
            var id_user = "<?php echo $this->session->userdata('id_user');?>";
         	$.ajax({
	            url: '<?php echo base_url(); ?>Rest/addParticipant_ToGroup',
	            type: 'POST',   
	            data: {
	                id_user: id_user,
	                email: emailfriend,
	                class_id: classid
	            },             
	            success: function(data)
	            {                   
            	 	var code = data['code'];
            	 	var message = data['message'];
            	 	$("#modalinvitefriends").modal('hide');
	                if (code == 200) {
	                    swal(message, "", "success")

	                    // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Success Join to Class');
	                    setTimeout(function(){
	                        location.reload();
	                    },3000);
	                }
	                else
	                {
	                    swal(message, "", "warning")
	                    setTimeout(function(){
	                        location.reload();
	                    },3000);
	                   
	                }
	            }
	        });

        });

        $(document).on("click", ".datacheck", function () {
            var myBookId = $(this).data('id');
            var ceklink = $(this).data('link');
            var usertype_now = "<?php echo $this->session->userdata('usertype_now');?>";
            var id_user_kids = "<?php echo $this->session->userdata('id_user_kids');?>";
            if (usertype_now == 'student kid') {
                $("#proccess").attr("href", ceklink+myBookId+"?t=kids&child_id="+id_user_kids+"&user_utc="+user_utc);
            }
            else
            {
                $("#proccess").attr("href", ceklink+myBookId+"?t=user"+"&user_utc="+user_utc);
            }
            $(".modal-header #classid").val( myBookId );
        });

        $(document).on("click", ".datacheckk", function () {
            var myBookId = $(this).data('id');
            var ceklink = $(this).data('link');
            var usertype_now = "<?php echo $this->session->userdata('usertype_now');?>";
            var id_user_kids = "<?php echo $this->session->userdata('id_user_kids');?>";
            var lengthParti  = $(this).data('lengthparti');
            var maxlengthParti = $(this).data('maxlengthparti');
            
            if (lengthParti < maxlengthParti) {
                if (usertype_now == 'student kid') {
                    $("#proccessjoin").attr("href", ceklink+myBookId+"?t=kids&child_id="+id_user_kids+"&user_utc="+user_utc);
                }
                else
                {
                    $("#proccessjoin").attr("href", ceklink+myBookId+"?t=user"+"&user_utc="+user_utc);
                }
                $(".modal-header #classid").val( myBookId );
                $("#confrimjoinn").modal("show");
            }
            else
            {
                $("#confrimjoinn").modal('hide');
                notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut',"Maaf Kelas ini sudah penuh!!!");
                setTimeout(function(){
                    location.reload();
                },3500);   
            }

        });

        $(document).on("click", ".datacheck_channel", function () {
            var myBookId = $(this).data('id');
            var ceklink = $(this).data('link');
            $("#proccessjoin").attr("href", ceklink+myBookId+"?t=channel"+"&user_utc="+user_utc);
            $(".modal-header #classid").val( myBookId );
        });

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        function getschedule(aj)
        {

            // Opera 8.0+
            var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

            // Firefox 1.0+
            var isFirefox = typeof InstallTrigger !== 'undefined';

            // Safari 3.0+ "[object HTMLElementConstructor]" 
            var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

            // Internet Explorer 6-11
            var isIE = /*@cc_on!@*/false || !!document.documentMode;

            // Edge 20+
            var isEdge = !isIE && !!window.StyleMedia;

            // Chrome 1+
            var isChrome = !!window.chrome && !!window.chrome.webstore;

            // Blink engine detection
            var isBlink = (isChrome || isOpera) && !!window.CSS;

           /* if (isSafari) {
                var  Date.parse="";
            }
            else{
                var Date.parse= "Date.parse";    
            }*/
            // alert(Date.parse);
            
            // alert(Date.parse);
            $("#liveboxclass").empty();
            $("#xboxclass").empty();
            $("#xboxrecommended").empty();
            $("#xboxmissed").empty();
            $("#xboxprivateclass").empty();
            $("#xboxgroupclass").empty();
            var livecount = 0;
            var waitcount = 0;
            var passcount = 0;
            var privatecount = 0;
            var groupcount = 0;
            //TAMPILAN MISSED CLASS
            var t0 = performance.now();           
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/scheduleone/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    id_user: iduser,
                    user_utc: user_utc,
                    user_device: 'web',
                    date: formattedDate
                },
                success: function(response)
                {   
                	var t1 = performance.now();
                	console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.");
                    var a = JSON.stringify(response);          
                    // console.warn(a);              
                    var jsonPretty = JSON.stringify(JSON.parse(a),null,2);	                    
                    if (response['data'] == null) 
                    {                                                

                        $("#utama4").css('display','block');
                        $("#filterkelas").css('display','none');
                    }                   
                    else
                    {
                        if (response.data.length != null) {
                            
                            // alert(response.data.length);
                            // for (var i = response.data.length-1; i >=0;i--) {
                            for (var i = 0; i < response.data.length; i++) {

                                var visible = response['data'][i]['participant']['visible'];                                
                                var idclass = response['data'][i]['class_id'];
                                if (bahasa == "indonesia") {
                                    var sbjname = response['data'][i]['subject_name'];    
                                }
                                else
                                {
                                    var sbjname = response['data'][i]['english'];    
                                }
                                var desname = response['data'][i]['description'];
                                var desfull = response['data'][i]['description'];
                                var ttrname = response['data'][i]['first_name'];
                                var user_name = response['data'][i]['user_name']
                                var datee   = response['data'][i]['date'];
                                var enddatee= response['data'][i]['enddate'];
                                datee       = moment(datee).format('DD-MM-YYYY');
                                enddatee    = moment(enddatee).format('DD-MM-YYYY');
                                var time    = response['data'][i]['time'];
                                var endtime = response['data'][i]['endtime'];
                                var timeclass    = response['data'][i]['date']+" "+response['data'][i]['time'];
                                var endtimeclass = response['data'][i]['enddate']+" "+response['data'][i]['endtime'];
                                var usrimg  = response['data'][i]['user_image'];       
                                var showtime = moment(timeclass).format('HH:mm'); 
                                var showendtime = moment(endtimeclass).format('HH:mm');         
                                var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                                // var hrgkls  = response['data'][i]['harga'];
                                var hrgkls  = response['data'][i]['harga_cm'];
                                var templatetype  = response['data'][i]['template_type'];
                                var classtype  = response['data'][i]['class_type'];
                                var country = response['data'][i]['country'];
                                desname = desname.substring(0, 33);        
                                var channelstatus = response['data'][i]['channel_status'];  
                                
                                if (classtype == "multicast") 
                                {
                                	var participant_listt = response['data'][i]['participant']['participant'];	
                                    var im_exist = false;
                                    	if(participant_listt != null){
                                    		for (var iai = 0; iai < participant_listt.length ;iai++) {

	                                    		var a = moment(tgl).format('YYYY-MM-DD');
	                                    		if (aj == null) {
	                                    			if (iduser == participant_listt[iai]['id_user']) {
		                                    			im_exist = true;
	                                    			}
	                                    		}
	                                    		else
	                                    		{
	                                    			if (iduser == participant_listt[iai]['id_user']) {
		                                    			im_exist = true;
	                                    			}
	                                    		}
	                                    	}
                                    	}
                                    	
                                        //KOTAK DESIGN 
                                       
                                        var kotak_missedmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"'  onerror="+alt_img+" width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></div></a></div>";

                                        
                                        var kotak_join_missedmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></div></a></div>";


                                    	if(im_exist){
                                    		if (Date.parse(datenow) < Date.parse(timeclass)) 
                                    		{   

                                			}
                                			else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
	                                            
                                			}
                                			else
                                			{       
                                                passcount++;
	                                            $("#xboxmissed").append(kotak_missedmulticast);
                                			}
                                    	}
                                }
                                else if (classtype == "multicast_channel_paid") 
                                {
                                	var participant_listt = response['data'][i]['participant']['participant'];
                                    	var im_exist = false;
                                    	if(participant_listt != null){
                                    		for (var iai = 0; iai < participant_listt.length ;iai++) {
                                    			// alert(participant_listt[iai]['id_user']);
	                                    		var a = moment(tgl).format('YYYY-MM-DD');
	                                    		if (aj == null) {
	                                    			if (iduser == participant_listt[iai]['id_user']) {
		                                    			im_exist = true;
	                                    			}                                                    
	                                    		}
	                                    		else
	                                    		{
	                                    			if (iduser == participant_listt[iai]['id_user']) {
		                                    			im_exist = true;
	                                    			}                                                    
	                                    		}
	                                    	}
                                    	}
                                    	
                                        //KOTAK DESIGN 
                                        
                                        var kotak_missedmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></div></a></div>";
                                        
                                      
                                        var kotak_join_missedchannel = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></div></a></div>";

	                                    if(im_exist){
	                                    	if (channelstatus == 1 || channelstatus == 2) {
	                                    		if (Date.parse(datenow) < Date.parse(timeclass)) 
	                                    		{   
                                                    
	                                			}
	                                			else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
	                                				
	                                			}
	                                			else
	                                			{   
                                                    passcount++;                             				
		                                            $("#xboxmissed").append(kotak_missedmulticast);
	                                			}
	                                    	}
                                    	}
                                        
                                }
                                else if (classtype == "multicast_paid") 
                                {
                                    
                                    var participant_list = response['data'][i]['participant']['participant'];
                                                                    

                                    var number_string = hrgkls.toString(),
                                        sisa    = number_string.length % 3,
                                        rupiah  = number_string.substr(0, sisa),
                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                            
                                    if (ribuan) {
                                        separator = sisa ? '.' : '';
                                        rupiah += separator + ribuan.join('.');
                                    } 
                                    
                                    var kotak_buy_missedmulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><label style='z-index:1; position:absolute; font-size:26px; color:white; top:23%; left:15%; bottom:23%;'>Rp. "+rupiah+"</label><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></div></a></div>";

                                    // alert(participant_list);  
                                    if (participant_list!=null) {
                                        var gueAda = false;
                                        var a = moment(tgl).format('YYYY-MM-DD'); 
                                            
                                        var number_string = hrgkls.toString(),
                                            sisa    = number_string.length % 3,
                                            rupiah  = number_string.substr(0, sisa),
                                            ribuan  = number_string.substr(sisa).match(/\d{3}/g);

                                        if (ribuan) {
                                            separator = sisa ? '.' : '';
                                            rupiah += separator + ribuan.join('.');
                                        }  

                                        
                                        var kotak_missedmulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='lv-img-sm' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></div></a></div>";
                                        
                                        for (var iai = 0; iai < participant_list.length ;iai++) {                                            
                                            
                                             
                                            if (aj == null) {                                             
                                                if (iduser == participant_list[iai]['id_user']) {                           
                                                    var number_string = hrgkls.toString(),
                                                        sisa    = number_string.length % 3,
                                                        rupiah  = number_string.substr(0, sisa),
                                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                                            
                                                    if (ribuan) {
                                                        separator = sisa ? '.' : '';
                                                        rupiah += separator + ribuan.join('.');
                                                    }                                      
                                                    
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                       
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else
                                                    {
                                                        passcount++;
                                                        $("#xboxmissed").append(kotak_missedmulticastpaid);
                                                    }
                                                }
                                                else
                                                {
                                                    var number_string = hrgkls.toString(),
                                                        sisa    = number_string.length % 3,
                                                        rupiah  = number_string.substr(0, sisa),
                                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                                            
                                                    if (ribuan) {
                                                        separator = sisa ? '.' : '';
                                                        rupiah += separator + ribuan.join('.');
                                                    }                                      

                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {        
                                                       
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else
                                                    {
                                                        // passcount++;
                                                        // $("#xboxmissed").append(kotak_buy_missedmulticastpaid);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var a = moment(tgl).format('YYYY-MM-DD');

                                                if (iduser == participant_list[iai]['id_user']) {
                                                    gueAda = true;
                                                }
                                            }
                                        }
                                        if(gueAda) {
                                            if (aj == a) 
                                            {                                                                                                                                 
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {        
                                                   
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                   
                                                }
                                                else
                                                {          
                                                    passcount++;                                                  
                                                    $("#xboxmissed").append(kotak_missedmulticastpaid);
                                                }
                                            }
                                            else
                                            {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {          
                                               
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                   
                                                }
                                                else
                                                {
                                                    passcount++;
                                                    $("#xboxmissed").append(kotak_missedmulticastpaid);
                                                }
                                            }
                                        }else{
                                            var number_string = hrgkls.toString(),
                                                sisa    = number_string.length % 3,
                                                rupiah  = number_string.substr(0, sisa),
                                                ribuan  = number_string.substr(sisa).match(/\d{3}/g);

                                            if (ribuan) {
                                                separator = sisa ? '.' : '';
                                                rupiah += separator + ribuan.join('.');
                                            }                                      

                                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                                            {            
                                            waitcount++;                                            
                                                
                                            }
                                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                
                                            }
                                            else
                                            {
                                                // passcount++;
                                                // $("#xboxmissed").append(kotak_buy_missedmulticastpaid);
                                            }
                                        }
                                    } 
                                    else
                                    {                                        
                                        var number_string = hrgkls.toString(),
                                            sisa    = number_string.length % 3,
                                            rupiah  = number_string.substr(0, sisa),
                                            ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                                
                                        if (ribuan) {
                                            separator = sisa ? '.' : '';
                                            rupiah += separator + ribuan.join('.');
                                        }                                                                                                                

                                        if (Date.parse(datenow) < Date.parse(timeclass)) 
                                        {              
                                      
                                        }
                                        else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                           
                                        }
                                        else
                                        {                 
                                        // passcount++;                                    
                                            // $("#xboxmissed").append(kotak_buy_missedmulticastpaid);
                                        }
                                    }                                 
                                }
                                else if (classtype == "private") 
                                {   
                                    var participant = response['data'][i]['participant']['participant'];

                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);                                    
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (Date.parse(datenow) > Date.parse(timeclass))
                                                {
                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></div></a></div>";
                                                    passcount++;
                                                    $("#xboxmissed").append(kotakprivate);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {

                                               
                                                var kotak_missedprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></div></a></div>";

                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {           
                                                    
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                     
                                                    }
                                                    else
                                                    {             
                                                    passcount++;                                           
                                                        $("#xboxmissed").append(kotak_missedprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
	                                                {	   
	                                                	
	                                                }
	                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                      
	                                                }
	                                                else
	                                                {	        
                                                    passcount++;                                           
	                                                    $("#xboxmissed").append(kotak_missedprivate);
	                                                }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "private_channel") 
                                {                                    
                                    var participant = response['data'][i]['participant']['participant'];                                    

                                    var aca = JSON.stringify(participant);                                    
                                    var jsa = JSON.parse(aca); 
                                                                     
                                    // alert(jsa[0]['id_user']);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');                                        
                                        if (aj == null) {                                        	
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {

                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                   
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                   
                                                }
                                                else
                                                {
                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></div></a></div>";
                                                    passcount++;
                                                    $("#xboxmissed").append(kotakprivate);
                                                }
                                            }
                                        }
                                        else
                                        {                                        	
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {

                                               
                                                var kotak_missedprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></div></a></div>";

                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {            
                                                    
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                       
                                                    }
                                                    else
                                                    {                                         
                                                    passcount++;               
                                                        $("#xboxmissed").append(kotak_missedprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
	                                                {	
	                                                	
	                                                }
	                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                       
	                                                }
	                                                else
	                                                {	                                           
                                                    passcount++;        
	                                                    $("#xboxmissed").append(kotak_missedprivate);
	                                                }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "group") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];
                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);

                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                     
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) || Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    
                                                }
                                                else
                                                {

                                                    var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></div></a></div>";
                                                    passcount++;
                                                    $("#xboxmissed").append(kotakgroup);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                       
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></div></a></div>";
                                                        passcount++;
                                                        $("#xboxmissed").append(kotakprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                       
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                       
                                                    }
                                                    else
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></div></a></div>";
                                                        passcount++;
                                                        $("#xboxmissed").append(kotakprivate);
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "group_channel") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];
                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                     
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) || Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    
                                                }
                                                else
                                                {
                                                    var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></div></a></div>";
                                                    passcount++;
                                                    $("#xboxmissed").append(kotakgroup);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></div></a></div>";
                                                        passcount++;
                                                        $("#xboxmissed").append(kotakprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                       
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        
                                                    }
                                                    else 
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></div></a></div>";
                                                        passcount++;
                                                        $("#xboxmissed").append(kotakprivate);
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }                                                                                            
                                
                            }
                        } 
                    } 
                    // alert(passcount);
                    if (passcount != 0) {
                      $("#utama5").css('display','block');  
                      $("#utama4").css('display','none');
                    }
                    else
                    {

                     $("#utama5").css('display','none');  
                     $("#utama4").css('display','block');  
                    }       
                }
            });
                        
            //TAMPILAN COMING UP DAN LIVE NOW
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/scheduleone/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    id_user: iduser,
                    user_utc: user_utc,
                    user_device: 'web',
                    date: formattedDate
                },
                success: function(response)
                {   
                    var a = JSON.stringify(response);
                    var jsonPretty = JSON.stringify(JSON.parse(a),null,2);
                    // alert(a);
                    if (response['data_private'] == null) {
                        // alert(a);
                       // $("#utama4").css('display','block'); 
                       $("#utamaextraclass").css('display','none');
                       $("#filterkelas").css('display','none');
                    }
                    if (response['data_group'] == null) {
                        // alert(a);
                       $("#utama4").css('display','block'); 
                       $("#filterkelas").css('display','none');
                       $("#utamaextraclass").css('display','none');
                    }

                    if (response['data_private'] != null) {

                      // $("#utama4").css('display','none');
                        
                      if (response.data_private.length != null || response.data_group.length != null) {
                        for (var i = 0; i < response.data_private.length; i++) {
                            
                            var request_id = response['data_private'][i]['request_id'];
                            var avtime_id = response['data_private'][i]['avtime_id'];
                            var id_user_requester = response['data_private'][i]['id_user_requester'];
                            var tutor_id = response['data_private'][i]['tutor_id'];
                            var subject_id = response['data_private'][i]['subject_id'];
                            var topic = response['data_private'][i]['topic'];
                            var date_requested = response['data_private'][i]['date_requested'];
                            var duration_requested = response['data_private'][i]['duration_requested'];
                            var approve = response['data_private'][i]['approve'];
                            var template = response['data_private'][i]['template'];
                            var user_utc = response['data_private'][i]['user_utc'];
                            var datetime = response['data_private'][i]['datetime'];
                            var date_requested_utc = response['data_private'][i]['date_requested_utc'];
                            var country = response['data_private'][i]['country'];
                            var user_name = response['data_private'][i]['user_name'];
                            var user_image = response['data_private'][i]['user_image'];
                            var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                            var subject_name = response['data_private'][i]['subject_name'];
                            day = moment(date_requested_utc).format('dddd');
                            date = moment(date_requested_utc).format('DD MMMM YYYY');
                            time = moment(date_requested_utc).format('HH:mm');

                            if (approve == 0) {
                                var status_tutor ="<small style='margin-top:-1px; color:orange'><?php echo $this->lang->line('waitapprovedtutor');?></small>";

                            }else if (approve == 1) {
                                var status_tutor ="<small style='margin-top:-1px; color:green'><?php echo $this->lang->line('confirmedbytutor');?></small>";
                            }else if (approve == 2){ 
                                var status_tutor ="<small style='margin-top:-1px; color:orange'><?php echo $this->lang->line('waitforpayment');?></small>";
                            }
                            else if (approve == -1){ 
                                var status_tutor ="<small style='margin-top:-1px; color:red'><?php echo $this->lang->line('rejectedbytutor');?></small>";
                            }
                            else { 
                                var status_tutor ="<small style='margin-top:-1px; color:red'><?php echo $this->lang->line('tutornotavailable');?></small>";
                            }
                            
                            var kotak_extrakelasprivate = "<div class='me_timer col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:280px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px;'>"+subject_name+" - "+topic+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+user_name+"</small>"+status_tutor+"<small>"+day+", "+date+" - "+time+"</small></h2></a></div></a></div>";
                            $("#utamaextraclass").css('display','block');
                            $("#utama4").css('display','none');
                            
                            privatecount++;
                            $("#xboxprivateclass").append(kotak_extrakelasprivate);
                        }
                        for (var i = 0; i < response.data_group.length; i++) {
                            
                            var request_id = response['data_group'][i]['request_id'];
                            var avtime_id = response['data_group'][i]['avtime_id'];
                            var id_user_requester = response['data_group'][i]['id_user_requester'];
                            var tutor_id = response['data_group'][i]['tutor_id'];
                            var subject_id = response['data_group'][i]['subject_id'];
                            var topic = response['data_group'][i]['topic'];
                            var date_requested = response['data_group'][i]['date_requested'];
                            var duration_requested = response['data_group'][i]['duration_requested'];
                            var approve = response['data_group'][i]['approve'];
                            var template = response['data_group'][i]['template'];
                            var user_utc = response['data_group'][i]['user_utc'];
                            var datetime = response['data_group'][i]['datetime'];
                            var date_requested_utc = response['data_group'][i]['date_requested_utc'];
                            var country = response['data_group'][i]['country'];
                            var user_name = response['data_group'][i]['user_name'];
                            var user_image = response['data_group'][i]['user_image'];
                            var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                            var subject_name = response['data_group'][i]['subject_name'];
                            day = moment(date_requested_utc).format('dddd');
                            date = moment(date_requested_utc).format('DD MMMM YYYY');
                            time = moment(date_requested_utc).format('HH:mm');
                            var approve = response['data_group'][i]['approve'];
	                        var buttonnih = null;
	                        // var cekstatusdia = response['data_group'][i]['id_friends']['id_friends'];
	                        var myStatus = null;
	                        var statusKelas = null;
	                        var statusteman = null;
	                        // for (var z = 0; z < cekstatusdia.length; z++) {
	                        //     if (cekstatusdia[z]['id_user'] == iduser) 
	                        //     {
	                        //         myStatus = cekstatusdia[z]['status'];

	                        //     }
	                        //     else if (cekstatusdia[z]['id_user'] != iduser) 
	                        //     {
	                        //         statusteman = cekstatusdia[z]['status'];

	                        //     }
	                        // }

                            if (approve == 0 && statusteman != 0) {
                                var status_tutor ="<small style='margin-top:-1px; color:orange'><?php echo $this->lang->line('waitapprovedtutor');?></small>";

                            }
                            else if (approve == 0 && statusteman == 0) {
                                var status_tutor ="<small style='margin-top:-1px; color:orange'><?php echo $this->lang->line('waitapprovedfriend');?></small>";

                            }else if (approve == 1) {
                                var status_tutor ="<small style='margin-top:-1px; color:green'><?php echo $this->lang->line('confirmedbytutor');?></small>";
                            }else if (approve == 2){ 
                                var status_tutor ="<small style='margin-top:-1px; color:orange'><?php echo $this->lang->line('waitforpayment');?></small>";
                            }
                            else if (approve == -1){ 
                                var status_tutor ="<small style='margin-top:-1px; color:red'><?php echo $this->lang->line('rejectedbytutor');?></small>";
                            }
                            else { 
                                var status_tutor ="<small style='margin-top:-1px; color:red'><?php echo $this->lang->line('tutornotavailable');?></small>";
                            }
                            
                            var kotak_extrakelasgroup = "<div class='me_timer col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:280px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px;'>"+subject_name+" - "+topic+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+user_name+"</small>"+status_tutor+"<small>"+day+", "+date+" - "+time+"</small></h2></a></div></a></div>";
                            $("#utamaextraclass").css('display','block');
                            $("#utama4").css('display','none');
                            
                            groupcount++;
                            $("#xboxgroupclass").append(kotak_extrakelasgroup);
                        }
                      }
                    }

                    if (response['data'] == null) 
                    {                                                
                        $("#utama4").css('display','block');
                    }                   
                    else 
                    {
                        if (response.data.length != null) {
                            
                            // alert(response.data.length);
                            for (var i = 0; i < response.data.length; i++) {

                                var visible = response['data'][i]['participant']['visible'];
                                var idclass = response['data'][i]['class_id'];
                                if (bahasa == "indonesia") {
                                    var sbjname = response['data'][i]['subject_name'];    
                                }
                                else
                                {
                                    var sbjname = response['data'][i]['english'];    
                                }
                                var desname = response['data'][i]['description'];
                                var desfull = response['data'][i]['description'];
                                var ttrname = response['data'][i]['first_name'];
                                var user_name = response['data'][i]['user_name']
                                var datee   = response['data'][i]['date'];
                                var enddatee= response['data'][i]['enddate'];
                                datee       = moment(datee).format('DD-MM-YYYY');
                                enddatee    = moment(enddatee).format('DD-MM-YYYY');
                                var time    = response['data'][i]['time'];
                                var endtime = response['data'][i]['endtime'];
                                var timeclass    = response['data'][i]['date']+" "+response['data'][i]['time'];
                                var endtimeclass = response['data'][i]['enddate']+" "+response['data'][i]['endtime'];
                                var showtime = moment(timeclass).format('HH:mm'); 
                                var showendtime = moment(endtimeclass).format('HH:mm'); 
                                
                                var usrimg  = response['data'][i]['user_image'];                
                                // +'?'+new Date().getTime()
                                // var hrgkls  = response['data'][i]['harga'];
                                var hrgkls  = response['data'][i]['harga_cm'];
                                var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                                var templatetype  = response['data'][i]['template_type'];
                                var classtype  = response['data'][i]['class_type'];
                                var country = response['data'][i]['country'];
                                desname = desname.substring(0, 33);        
                                var channelstatus = response['data'][i]['channel_status'];
                                var max_participants = response['data'][i]['max_participants'];
                                
                                if (classtype == "multicast") 
                                {
                                    var participant_listt = response['data'][i]['participant']['participant'];
                                    if (participant_listt == null) {
                                        var lengthParti = 0;
                                    }
                                    else{
                                        var lengthParti = participant_listt.length;
                                    }
                                    var im_exist = false;                                        
                                        if(participant_listt != null){
                                            for (var iai = 0; iai < participant_listt.length ;iai++) {                                                
                                                var a = moment(tgl).format('YYYY-MM-DD');
                                                if (aj == null) {
                                                    if (iduser == participant_listt[iai]['id_user']) {
                                                        im_exist = true;
                                                    }
                                                }
                                                else
                                                {
                                                    if (iduser == participant_listt[iai]['id_user']) {
                                                        im_exist = true;
                                                    }
                                                }
                                            }
                                        }
                                        
                                        //KOTAK DESIGN 
                                        var kotak_comingmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></a></div></a></div>";
                                        var kotak_livemulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='<?php echo base_url(); ?>master/tps_me/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-blue m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></a></div></a></div>";
                                       

                                        var kotak_join_comingmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer;'><a data-id='"+idclass+"' data-lengthParti='"+lengthParti+"' data-maxlengthParti='"+max_participants+"' href='#' class='datacheckk' data-link='<?php echo base_url('Transaction/multicast_join'); ?>/'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><span class='card-header'><h5 class='c-blue m-l-10' style='height:30px; cursor:pointer;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2><button class='btn btn-primary btn-block btn-sm m-t-5'><?php echo $this->lang->line('join'); ?></button></span></div></a></div>";
                                        var kotak_join_livemulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer;'><a data-id='"+idclass+"' data-lengthParti='"+lengthParti+"' data-maxlengthParti='"+max_participants+"' href='#' class='datacheckk' data-link='<?php echo base_url('Transaction/multicast_join'); ?>/'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><span class='card-header'><h5 class='c-blue m-l-10' style='height:30px; cursor:pointer;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2><button class='btn btn-primary btn-block btn-sm m-t-5'><?php echo $this->lang->line('join'); ?></button></span></div></a></div>";
                                       

                                        if(im_exist){
                                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                                            {   
                                                waitcount++;
                                                $("#xboxclass").append(kotak_comingmulticast);
                                            }
                                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                   livecount++;
                                                $("#liveboxclass").append(kotak_livemulticast);
                                            }
                                            else
                                            {       
                                               
                                            }
                                        }else{

                                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                                            {             
                                                waitcount++; 
                                                $("#xboxclass").append(kotak_join_comingmulticast);
                                            }
                                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                livecount++;
                                                $("#liveboxclass").append(kotak_join_livemulticast);
                                            }
                                            else
                                            {
                                               
                                            }
                                        }
                                }
                                else if (classtype == "multicast_channel_paid") 
                                {
                                    var participant_listt = response['data'][i]['participant']['participant'];
                                    var im_exist = false;
                                    if(participant_listt != null){
                                        for (var iai = 0; iai < participant_listt.length ;iai++) {
                                            // alert(participant_listt[iai]['id_user']);
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (aj == null) {
                                                if (iduser == participant_listt[iai]['id_user']) {
                                                    im_exist = true;
                                                }                                                    
                                            }
                                            else
                                            {
                                                if (iduser == participant_listt[iai]['id_user']) {
                                                    im_exist = true;
                                                }                                                    
                                            }
                                        }
                                    }
                                    
                                    //KOTAK DESIGN 
                                    var kotak_comingmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></a></div></a></div>";
                                    var kotak_livemulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='<?php echo base_url(); ?>master/tps_me/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-blue m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></a></div></a></div>";
                                    
                                    
                                    var kotak_join_comingchannel = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer;'><a data-id='"+idclass+"' href='#confrimjoinn' data-toggle='modal' class='datacheck_channel' data-link='<?php echo base_url('Transaction/multicast_join'); ?>/'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><span class='card-header'><h5 class='c-blue m-l-10' style='height:30px; cursor:pointer;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2><button class='btn btn-primary btn-block btn-sm m-t-5'><?php echo $this->lang->line('join'); ?></button></span></div></a></div>";
                                    var kotak_join_livechannel = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer;'><a data-id='"+idclass+"' href='#confrimjoinn' data-toggle='modal' class='datacheck_channel' data-link='<?php echo base_url('Transaction/multicast_join'); ?>/'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><span class='card-header'><h5 class='c-blue m-l-10' style='height:30px; cursor:pointer;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2><button class='btn btn-primary btn-block btn-sm m-t-5'><?php echo $this->lang->line('join'); ?></button></span></div></a></div>";
                                    

                                    if(im_exist){
                                        if (channelstatus == 1 || channelstatus == 2) {
                                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                                            {   
                                                waitcount++;                                            
                                                $("#xboxclass").append(kotak_comingmulticast);
                                            }
                                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                livecount++;
                                                $("#liveboxclass").append(kotak_livemulticast);
                                            }
                                            else
                                            {   
                                               
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (channelstatus == 2) {
                                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                                            {    
                                                waitcount++;                                          
                                                $("#xboxclass").append(kotak_join_comingchannel);
                                            }
                                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                livecount++;
                                                $("#liveboxclass").append(kotak_join_livechannel);
                                            }
                                            else
                                            {          
                                               
                                            }
                                        }
                                    }
                                }
                                else if (classtype == "multicast_paid") 
                                {
                                    var time    = response['data'][i]['time_order'];
                                    var timeclass    = response['data'][i]['date_order']+" "+response['data'][i]['time_order'];
                                    
                                    var participant_list = response['data'][i]['participant']['participant'];                                                                    

                                    var number_string = hrgkls.toString(),
                                        sisa    = number_string.length % 3,
                                        rupiah  = number_string.substr(0, sisa),
                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                            
                                    if (ribuan) {
                                        separator = sisa ? '.' : '';
                                        rupiah += separator + ribuan.join('.');
                                    } 
                                    var kotak_buy_comingmulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer;'><a data-id='"+idclass+"' href='#confrim' data-toggle='modal' class='datacheck' data-link='<?php echo base_url('Transaction/multicast_join'); ?>/'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><label style='z-index:1; position:absolute; font-size:26px; color:white; top:23%; left:15%; bottom:23%; cursor:pointer;'>Rp. "+rupiah+"</label><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><span class='card-header'><h5 class='c-blue m-l-10' style='height:30px; cursor:pointer;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2><button class='btn btn-primary btn-block btn-sm m-t-5'>Beli</button></span></div></a></div>";
                                    var kotak_buy_livemulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer;'><a data-id='"+idclass+"' href='#confrim' data-toggle='modal' class='datacheck' data-link='<?php echo base_url('Transaction/multicast_join'); ?>/'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><label style='z-index:1; position:absolute; font-size:26px; color:white; top:23%; left:15%; bottom:23%; cursor:pointer;'>Rp. "+rupiah+"</label><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><span class='card-header'><h5 class='c-blue m-l-10' style='height:30px; cursor:pointer;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2><button class='btn btn-primary btn-block btn-sm m-t-5'>Beli</button></span></div></a></div>";
                                    

                                    // alert(participant_list);  
                                    if (participant_list!=null) {
                                        var gueAda = false;
                                        var a = moment(tgl).format('YYYY-MM-DD'); 
                                            
                                        var number_string = hrgkls.toString(),
                                            sisa    = number_string.length % 3,
                                            rupiah  = number_string.substr(0, sisa),
                                            ribuan  = number_string.substr(sisa).match(/\d{3}/g);

                                        if (ribuan) {
                                            separator = sisa ? '.' : '';
                                            rupiah += separator + ribuan.join('.');
                                        }  

                                        var kotak_comingmulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></a></div></a></div>"; 
                                        var kotak_livemulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='<?php echo base_url(); ?>master/tps_me/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-blue m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-15px;'>"+ttrname+"</small><small style='margin-top:-6px;'>Harga : "+hrgkls+"</small><small style='margin-top:-1px;'>"+showtime+"  -  "+showendtime+"</small></h2></a></div></a></div>";
                                       
                                        
                                        for (var iai = 0; iai < participant_list.length ;iai++) {                                            
                                            
                                             
                                            if (aj == null) {                                             
                                                if (iduser == participant_list[iai]['id_user']) {                           
                                                    var number_string = hrgkls.toString(),
                                                        sisa    = number_string.length % 3,
                                                        rupiah  = number_string.substr(0, sisa),
                                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                                            
                                                    if (ribuan) {
                                                        separator = sisa ? '.' : '';
                                                        rupiah += separator + ribuan.join('.');
                                                    }                                      
                                                    
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        waitcount++;
                                                        $("#xboxclass").append(kotak_comingmulticastpaid);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        $("#liveboxclass").append(kotak_livemulticastpaid);
                                                    }
                                                    else
                                                    {
                                                        
                                                    }
                                                }
                                                else
                                                {
                                                    var number_string = hrgkls.toString(),
                                                        sisa    = number_string.length % 3,
                                                        rupiah  = number_string.substr(0, sisa),
                                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                                            
                                                    if (ribuan) {
                                                        separator = sisa ? '.' : '';
                                                        rupiah += separator + ribuan.join('.');
                                                    }                                      

                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {        
                                                        waitcount++;                                                
                                                        $("#xboxclass").append(kotak_buy_comingmulticastpaid);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        // livecount++;
                                                        // $("#liveboxclass").append(kotak_buy_livemulticastpaid);
                                                    }
                                                    else
                                                    {
                                                        
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var a = moment(tgl).format('YYYY-MM-DD');

                                                if (iduser == participant_list[iai]['id_user']) {
                                                    gueAda = true;
                                                }
                                            }
                                        }
                                        if(gueAda) {
                                            if (aj == a) 
                                            {                                                                                                                                 
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {        
                                                    waitcount++;                                                    
                                                    $("#xboxclass").append(kotak_comingmulticastpaid);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    livecount++;
                                                    $("#liveboxclass").append(kotak_livemulticastpaid);
                                                }
                                                else
                                                {          
                                                    
                                                }
                                            }
                                            else
                                            {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {          
                                                waitcount++;                                                  
                                                    $("#xboxclass").append(kotak_comingmulticastpaid);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    livecount++;
                                                    $("#liveboxclass").append(kotak_livemulticastpaid);
                                                }
                                                else
                                                {
                                                    
                                                }
                                            }
                                        }else{
                                            var number_string = hrgkls.toString(),
                                                sisa    = number_string.length % 3,
                                                rupiah  = number_string.substr(0, sisa),
                                                ribuan  = number_string.substr(sisa).match(/\d{3}/g);

                                            if (ribuan) {
                                                separator = sisa ? '.' : '';
                                                rupiah += separator + ribuan.join('.');
                                            }                                      

                                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                                            {            
                                            waitcount++;                                            
                                                $("#xboxclass").append(kotak_buy_comingmulticastpaid);
                                            }
                                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                // livecount++;
                                                // $("#liveboxclass").append(kotak_buy_livemulticastpaid);
                                            }
                                            else
                                            {
                                                // passcount++;
                                                // $("#xboxmissed").append(kotak_buy_missedmulticastpaid);
                                            }
                                        }
                                    } 
                                    else
                                    {                                        
                                        var number_string = hrgkls.toString(),
                                            sisa    = number_string.length % 3,
                                            rupiah  = number_string.substr(0, sisa),
                                            ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                                
                                        if (ribuan) {
                                            separator = sisa ? '.' : '';
                                            rupiah += separator + ribuan.join('.');
                                        }                                                                                                                

                                        if (Date.parse(datenow) < Date.parse(timeclass)) 
                                        {              
                                        waitcount++;                              
                                            $("#xboxclass").append(kotak_buy_comingmulticastpaid);
                                        }
                                        else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                            // livecount++;
                                            // $("#liveboxclass").append(kotak_buy_livemulticastpaid);
                                        }
                                        else
                                        {                 
                                        // passcount++;                                    
                                            // $("#xboxmissed").append(kotak_buy_missedmulticastpaid);
                                        }
                                    }                                 
                                }
                                else if (classtype == "private") 
                                {
                                    
                                    var participant = response['data'][i]['participant']['participant'];

                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);                                    
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');

                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            // if (iduser == jsa[iai]['id_user']) {

                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {   

                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></a></div></a></div>";
                                                    waitcount++;
                                                    $("#xboxclass").append(kotakprivate);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {

                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></a></div></a></div>";
                                                    livecount++;
                                                    $("#liveboxclass").append(kotakprivate);
                                                }
                                            // }
                                        }
                                        else
                                        {

                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            // alert(jsa[iai]['id_user']);
                                            if (iduser == jsa[iai]['id_user']) {


                                                var kotak_comingprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></a></div></a></div>";
                                                var kotak_liveprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></a></div></a></div>";
                                                

                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {           
                                                    waitcount++;                                              
                                                        $("#xboxclass").append(kotak_comingprivate);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        $("#liveboxclass").append(kotak_liveprivate);
                                                    }
                                                    else
                                                    {             
                                                    
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {      
                                                        waitcount++;                                                  
                                                        $("#xboxclass").append(kotak_comingprivate);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        $("#liveboxclass").append(kotak_liveprivate);
                                                    }
                                                    else
                                                    {           
                                                   
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "private_channel") 
                                {                                    
                                    var participant = response['data'][i]['participant']['participant'];                                    

                                    var aca = JSON.stringify(participant);                                    
                                    var jsa = JSON.parse(aca); 
                                                                     
                                    // alert(jsa[0]['id_user']);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');                                        
                                        if (aj == null) {                                           
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {

                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></a></div></a></div>";
                                                    waitcount++;
                                                    $("#xboxclass").append(kotakprivate);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></a></div></a></div>";
                                                    livecount++;
                                                    $("#liveboxclass").append(kotakprivate);
                                                }
                                                else
                                                {
                                                    
                                                }
                                            }
                                        }
                                        else
                                        {                                           
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {

                                                var kotak_comingprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></a></div></a></div>";
                                                var kotak_liveprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me_priv/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('privateclass'); ?></small></h2></a></div></a></div>";
                                               

                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {            
                                                    waitcount++;                                             
                                                        $("#xboxclass").append(kotak_comingprivate);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        $("#liveboxclass").append(kotak_liveprivate);
                                                    }
                                                    else
                                                    {                                         
                                                  
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {   
                                                        waitcount++;                                                     
                                                        $("#xboxclass").append(kotak_comingprivate);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        livecount++;
                                                        $("#liveboxclass").append(kotak_liveprivate);
                                                    }
                                                    else
                                                    {                                              
                                                    
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "group") 
                                {


                                    var participant = response['data'][i]['participant']['participant'];
                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);                                        
                                    // alert(jsa);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {

                                                     var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><a data-image_tutor='"+usrimg+"' data-subject='"+sbjname+"' data-description='"+desname+"' data-tutorname='"+user_name+"' data-timeclass='"+showtime+" - "+showendtime+"' data-id='"+idclass+"' href='#invite' data-toggle='modal' class='inviteGroup'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></a></div>";
                                                     waitcount++;
                                                     $("#xboxclass").append(kotakgroup);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) || Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></a></div>";
                                                    livecount++;
                                                    $("#liveboxclass").append(kotakgroup);
                                                }
                                                else
                                                {
                                                   
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {

                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a data-image_tutor='"+usrimg+"' data-subject='"+sbjname+"' data-description='"+desname+"' data-tutorname='"+user_name+"' data-timeclass='"+showtime+" - "+showendtime+"' data-id='"+idclass+"' href='#invite' data-toggle='modal' class='inviteGroup'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></a></div>";
                                                        waitcount++;
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></div></a></div>";
                                                        livecount++;
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                       
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a data-image_tutor='"+usrimg+"' data-subject='"+sbjname+"' data-description='"+desname+"' data-tutorname='"+user_name+"' data-timeclass='"+showtime+" - "+showendtime+"' data-id='"+idclass+"' href='#invite' data-toggle='modal' class='inviteGroup'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></a></div>";
                                                        waitcount++;
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></div></a></div>";
                                                        livecount++;
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                       
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "group_channel") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];
                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                     var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></a></div>";
                                                     waitcount++;
                                                     $("#xboxclass").append(kotakgroup);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) || Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></a></div>";
                                                    livecount++;
                                                    $("#liveboxclass").append(kotakgroup);
                                                }
                                                else
                                                {
                                                    
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></a></div>";
                                                        waitcount++;
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></div></a></div>";
                                                        livecount++;
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                        
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></a></div>";
                                                        waitcount++;
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' onerror="+alt_img+"  width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me_group/"+idclass+"?t="+templatetype+"&p=web'><h5 class='c-white m-l-10' style='height:30px;'>"+sbjname+" <br> "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+showtime+"  -  "+showendtime+" | <?php echo $this->lang->line('groupclass'); ?></small></h2></a></div></div></a></div>";
                                                        livecount++;
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                       
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }                                                                                            
                                
                            }
                        } 
                    } 
                    // alert(waitcount);
                    if (livecount != 0) {
                      $("#utama1").css('display','block');  
                      $("#utama4").css('display','none');
                    }
                    else
                    {
                     $("#utama1").css('display','none');  
                     // $("#utama4").css('display','block'); 
                    }
                    if (waitcount != 0) {
                      $("#utama2").css('display','block');  
                      $("#utama4").css('display','none');
                    }
                    else
                    {
                     $("#utama2").css('display','none');  
                     // $("#utama4").css('display','block'); 
                    }
                }
            });
        }  
    });
</script>

 <script>
    // document.getElementById("btn_extraclass").onmouseover = function() {mouseOver()};
    // document.getElementById("btn_extraclass").onmouseout = function() {mouseOut()};

    function mouseOut() {
        document.getElementById("btn_extraclass").style.right = "-176px";
        document.getElementById("text_extraclass").style.color = "#f49100";
    }

    function mouseOver() {
        document.getElementById("btn_extraclass").style.right = "5px";
        document.getElementById("text_extraclass").style.color = "#ffffff";
    }
</script>
<!-- End Script Rating -->
