<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">

        <?php $this->load->view('inc/side'); ?>
    </aside>
    <section id="content">
    <div class="container" style="position:relative;">
        <div class=" modal fade" data-modal-color="blue" id="complete_modal" tabindex="-1" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content" style="margin-top: 75%;">
                    <div class="modal-header">
                        <h4 class="modal-title"><?php echo $this->lang->line('completeschooltitle');?></h4>
                    </div>
                        <center>
                            <div id='modal_approv' class="modal-body"  ><br>
                               <div class="alert alert-info" style="display: block; ?>" id="alertcomplete_school">
                                    <?php echo $this->lang->line('alertcompleteschool');?>
                                </div>
                            </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                        </div>
                    </center>
                </div>
            </div>
        </div>
        <div class="block-header">
            <h2><?php echo $this->lang->line('profil'); ?></h2>

            <!-- <ul class="actions hidden-xs">
                <li>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('profil'); ?></li>
                        <li class="active"><?php echo $this->lang->line('tab_about'); ?></li>
                    </ol>
                </li>
            </ul> -->
        </div><!-- akhir block header --> 
        <div class="alert alert-success" style="display: none; ?>" id="alertsuccessupdate">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
            Foto akun berhasil diubah
        </div>
        <div class="alert alert-danger" style="display: none; ?>" id="alertsizebesar">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
            Upload gagal, Mohon ukuran file tidak melebihi 2 MB.
        </div>
        
        <div class="bs-item z-depth-5" style="margin-top: 5%;">
            <div class="card" id="profile-main">

                <?php
                $this->load->view('inc/sideprofile');
                ?>           

                <div class="pm-body clearfix">
                    <ul class="tab-nav tn-justified" role="tablist">
                       <?php
                            $status_user = $this->session->userdata('status_user');
                            if ($status_user =="umum") {
                            ?>
                                <li class="waves-effect"><a href="<?php echo base_url('About'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li>
                            <?php
                            }
                            else{
                            ?>
                                <li class="waves-effect"><a href="<?php echo base_url('About'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li>
                                <li id="tab_school" class="active waves-effect"><a href="<?php echo base_url('Profile-School'); ?>"><?php echo $this->lang->line('tab_ps'); ?></a></li>
                            <?php
                            }
                            ?>                            
                    </ul>                       
                    <div class="card-padding card-body">
                        
                        <blockquote class="m-b-5">
                            <p><?php echo $this->lang->line('tab_ps'); ?></p>
                        </blockquote> 
                        <?php

                            $id_user   = $this->session->userdata('id_user'); 
                            $jenjang_id   = $this->session->userdata('jenjang_id_kids'); 
                            if ($jenjang_id == null) {
                                $jenjang_id   = $this->session->userdata('jenjang_id'); 
                            }
                            $get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_id."'")->row_array()['jenjang_name'];
                            // echo $get_jenjangtype;
                            $getdata = $this->db->query("SELECT * FROM tbl_profile_student WHERE student_id='$id_user'")->row_array();
                            $get_school_id = $this->db->query("SELECT school_id FROM `tbl_profile_student` where student_id='$id_user'")->row_array()['school_id'];
                            $cek_school_id = $this->db->query("SELECT school_id FROM `master_school` where school_id='$get_school_id'")->row_array()['school_id'];
                            if ($cek_school_id) {
                                $get_school_name= $this->db->query("SELECT school_name FROM `master_school` where school_id='$cek_school_id'")->row_array()['school_name'];
                                $get_provinsi= $this->db->query("SELECT provinsi FROM `master_school` where school_id='$cek_school_id'")->row_array()['provinsi'];
                                $get_kabupaten= $this->db->query("SELECT kabupaten FROM `master_school` where school_id='$cek_school_id'")->row_array()['kabupaten'];
                                $get_kecamatan= $this->db->query("SELECT kecamatan FROM `master_school` where school_id='$cek_school_id'")->row_array()['kecamatan'];
                                // $get_kelurahan= $this->db->query("SELECT kelurahan FROM `master_school` where school_id='$cek_school_id'")->row_array()['kelurahan'];
                                 $get_school_address= $this->db->query("SELECT school_address FROM `master_school` where school_id='$cek_school_id'")->row_array()['school_address'];
                                 $type = substr($get_jenjangtype, 0, 3);
                            }
                            else{
                                $cek_school_id = $this->db->query("SELECT school_id FROM `master_school_temp` where school_id='$get_school_id'")->row_array()['school_id'];
                                $get_school_name= $this->db->query("SELECT school_name FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['school_name'];
                                $get_provinsi= $this->db->query("SELECT provinsi_id FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['provinsi_id'];
                                $get_kabupaten= $this->db->query("SELECT kabupaten_id FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['kabupaten_id'];
                                $get_kecamatan= $this->db->query("SELECT kecamatan_id FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['kecamatan_id'];
                                // $get_kelurahan= $this->db->query("SELECT kelurahan FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['kelurahan'];
                                 $get_school_address= $this->db->query("SELECT school_address FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['school_address'];
                                 $type = substr($get_jenjangtype, 0, 3);

                            }

                            
                            
                         ?>
                         <?php
                        if ($cek_school_id=='0'|| $cek_school_id==null ) {
                            ?>
                            <div id="editSchool">
                                <form action="<?php echo base_url('/master/saveEditSchoolStudent') ?>" method="post">                             
                                    <?php 
                                    
                                        $provinsi = "";
                                        $d_kabupaten = "";
                                        $d_kecamatan = "";
                                        // $d_kelurahan = "";
                                        $d_schoolname = "";
                                        
                                    ?>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label f-12">Provinsi Sekolah</label>
                                        <!-- <label>aa<?php echo $cek_school_id  ;?></label> -->
                                        <div class="col-sm-9">
                                            <div class="fg-line">                                        
                                                <select id='school_provinsi' class="select2 form-control" name="school_provinsi" data-placeholder="<?php echo $this->lang->line('selectprovinsii'); ?>">                                            
                                                <?php
                                                    
                                                    $prv = $this->Master_model->school_provinsi($provinsi);                                                
                                                    echo '<option disabled="disabled" selected="" value="0">Provinsi</option>'; 
                                                    foreach ($prv as $row => $v) {                                            
                                                        echo '<option value="'.$v['provinsi'].'">'.$v['provinsi'].'</option>';
                                                    }
                                                ?>
                                                </select>
                                            </div>                                           
                                        </div>
                                    </div>                                        
                                    <br><br>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label f-12">Kabupaten Sekolah</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line" id="boxkabupaten">                                        
                                                <select disabled id='school_kabupaten' class="select2 form-control" name="school_kabupaten" data-placeholder="<?php echo $this->lang->line('selectkabupaten'); ?>">
                                                    <option value=""></option>
                                                    <?php
                                                        
                                                        $kbp = $this->Master_model->school_getAllKabupaten($d_kabupaten,$provinsi);
                                                        if($kbp != ''){
                                                            foreach ($kbp as $key => $value) {
                                                                echo "<option value='".$value['kabupaten']."'>".$value['kabupaten']."</option>";
                                                            }
                                                            // echo "<script>pfka = '".$kbp['kabupaten']."'</script>";
                                                        }

                                                    ?>
                                                </select>                                          
                                            </div>
                                        </div>                                    
                                    </div>                                        
                                    <br><br>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label f-12">Kecamatan Sekolah</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">                                       
                                                <select  disabled id='school_kecamatan' name="school_kecamatan" class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkecamatann'); ?>" name="id_kecamatan">
                                                    <option value=''></option>
                                                    <?php
                                                        $kcm = $this->Master_model->school_getAllKecamatan($d_kecamatan,$d_kabupaten);
                                                        if($kcm != ''){
                                                            echo "<option value='".$kcm['kecamatan']."' selected='true'>".$kcm['kecamatan']."</option>";
                                                            echo "<script>pfkec = '".$kcm['kecamatan']."'</script>";

                                                        }

                                                    ?>
                                                </select>
                                                <small id="cek_kecamatan" style="color: red;"></small>
                                            </div>
                                        </div>
                                    </div>                                        
                                    <br><br>                                         
                                    <div class="form-group" id="kotak_School">
                                        <label for="inputEmail3" class="col-sm-3 control-label f-12">Nama Sekolah</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <select disabled id='school_name' name="school_name" required class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectschoolname'); ?>">
                                                    <option value=''></option>
                                                     <?php
                                                            // $name = $this->Master_model->school_getName($d_schoolname,$d_kelurahan);
                                                            if($name == ''){
                                                                echo "<option value='".$name['school_name']."' selected='true'>".$name['school_name']."</option>";
                                                                echo "<script>pfname = '".$name['school_name']."'</script>";
                                                            }

                                                        ?>
                                                </select>
                                                <label class="pull-right" style="cursor: pointer;">Sekolah belum terdaftar ? <a id="addSchool" href="#"> Tambah Sekolah</a></label>
                                                <small id="cek_schoolname" style="color: red;"></small>
                                            </div>
                                        </div>
                                    </div> 
                                    <div id="kotak_addSchool" disabled class="form-group" style="display: none;">
                                    </div>
                                    <br><br>                                         
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('school_address'); ?></label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <textarea name="school_address" id="school_address" rows="3" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typeschooladdress'); ?>" value="<?php echo $get_school_address; ?>"><?php echo $get_school_address; ?></textarea>
                                            </div>
                                        </div>
                                    </div>       
                                    <br><br><br>
                                    <br><br>
                                    <div class="card-padding card-body">                                                                           
                                        <button type="submit" name="simpanedit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>                    
                                    </div>
                                </form>
                            </div>
                        <?php    
                        }
                         else if ($getdata['flag'] == 0 ) {
                            ?>
                            <div id="updateSchool">
                                <!-- <form action="<?php echo base_url('/master/saveEditSchoolStudent') ?>" method="post">                              -->
                                    <?php 
                                    
                                        $provinsi = "";
                                        $d_kabupaten = "";
                                        $d_kecamatan = "";
                                        // $d_kelurahan = "";
                                        $d_schoolname = "";
                                        
                                    ?>
                                    <h4>Pilih Jenjangmu Sekarang</h4>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label f-12"><?php echo $this->lang->line('school_jenjang'); ?></label>
                                        <div class="col-sm-9">
                                            <div class="fg-line" id="kotak_jenjang">
                                                <select required name="jenjang_id"  class="selectALL form-control" id='jenjang_level' data-placeholder="<?php echo $this->lang->line('select_class'); ?>">
                                                    <option value=""></option>
                                                    <?php
                                                        $jen = $this->Master_model->getMasterJenjang();
                                                        if($jen != ''){
                                                            echo "<option value='".$jen['jenjang_id']."' selected='true'>".$jen['jenjang_name'].' '.$jen['jenjang_level']."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <label style="cursor: pointer; color: #2196F3">
                                                <input type="checkbox" value="15" id="check_umum">
                                                Sudah Lulus (Mahasiswa / UMUM)
                                            </label>
                                        </div>
                                    </div><br><br>
                                    <div id="kotak_sekolah">
                                        <br><br>
                                        
                                        <h4>Pilih Alamat Sekolahmu Sekarang</h4>
                                        <div class="pmb-block old_alamat" style="display: none;">
                                            <div class="pmbb-body">
                                                <div class="pmbb-view">
                                                    <dl class="dl-horizontal">
                                                        <dt><?php echo $this->lang->line('school_name'); ?></dt>
                                                        <dd>
                                                            <?php 
                                                                if ($get_school_name == "") {
                                                                    echo $this->lang->line('nousername');
                                                                }
                                                                echo $get_school_name; 
                                                            ?>                                            
                                                        </dd>
                                                    </dl>
                                                    <dl class="dl-horizontal">
                                                        <dt><?php echo $this->lang->line('school_address'); ?></dt>
                                                        <dd>
                                                            <?php 
                                                                if ($get_school_address == "") {
                                                                    echo $this->lang->line('nousername');
                                                                }
                                                                echo $get_school_address; 
                                                            ?>                                            
                                                        </dd>
                                                    </dl>
                                                    
                                                    <dl class="dl-horizontal">
                                                        <dt><?php echo $this->lang->line('selectkecamatann'); ?></dt>
                                                        <dd>
                                                            <?php 
                                                                if ($get_school_name == "") {
                                                                    echo $this->lang->line('nousername');
                                                                }
                                                                echo $get_kecamatan; 
                                                            ?>                                            
                                                        </dd>
                                                    </dl> 
                                                    <?php
                                                        if ($get_provinsi != "Luar Negeri") {
                                                            ?>
                                                            <dl class="dl-horizontal">
                                                                <dt><?php echo $this->lang->line('selectprovinsii'); ?></dt>
                                                                <dd>
                                                                    <?php 
                                                                        if ($get_school_name == "") {
                                                                            echo $this->lang->line('nousername');
                                                                        }
                                                                        echo $get_provinsi; 
                                                                    ?>                                            
                                                                </dd>
                                                            </dl>     
                                                                    <?php
                                                                }
                                                    ?>                                                       
                                                </div>                            
                                            </div>
                                        </div>
                                        <div class="new_alamat">
                                            <div class="form-group " >
                                                <label for="inputEmail3" class="col-sm-3 control-label f-12">Provinsi Sekolah</label>
                                                <!-- <label><?php echo $cek_school_id  ;?></label> -->
                                                <div class="col-sm-9">
                                                    <div class="fg-line">                                        
                                                        <select id='upd_school_provinsi' class="selectupd form-control" name="school_provinsi" data-placeholder="<?php echo $this->lang->line('selectprovinsii'); ?>">                                            
                                                        <?php
                                                            
                                                            $prv = $this->Master_model->school_provinsi($provinsi);                                                
                                                            echo '<option disabled="disabled" selected="" value="0">Provinsi</option>'; 
                                                            foreach ($prv as $row => $v) {                                            
                                                                echo '<option value="'.$v['provinsi'].'">'.$v['provinsi'].'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>                                           
                                                </div>
                                            </div>                                        
                                            <br><br>
                                            <div class="form-group ">
                                                <label for="inputEmail3" class="col-sm-3 control-label f-12">Kabupaten Sekolah</label>
                                                <div class="col-sm-9">
                                                    <div class="fg-line" id="upd_boxkabupaten">                                        
                                                        <select disabled id='upd_school_kabupaten' class="selectupd form-control" name="school_kabupaten" data-placeholder="<?php echo $this->lang->line('selectkabupaten'); ?>">
                                                            <option value=""></option>
                                                            <?php
                                                                
                                                                $kbp = $this->Master_model->school_getAllKabupaten($d_kabupaten,$provinsi);
                                                                if($kbp != ''){
                                                                    foreach ($kbp as $key => $value) {
                                                                        echo "<option value='".$value['kabupaten']."'>".$value['kabupaten']."</option>";
                                                                    }
                                                                    // echo "<script>pfka = '".$kbp['kabupaten']."'</script>";
                                                                }

                                                            ?>
                                                        </select>                                          
                                                    </div>
                                                </div>                                    
                                            </div>                                        
                                            <br><br>
                                            <div class="form-group ">
                                                <label for="inputEmail3" class="col-sm-3 control-label f-12">Kecamatan Sekolah</label>
                                                <div class="col-sm-9">
                                                    <div class="fg-line">                                       
                                                        <select  disabled id='upd_school_kecamatan' name="school_kecamatan" class="selectupd form-control" data-placeholder="<?php echo $this->lang->line('selectkecamatann'); ?>" name="id_kecamatan">
                                                            <option value=''></option>
                                                            <?php
                                                                $kcm = $this->Master_model->school_getAllKecamatan($d_kecamatan,$d_kabupaten);
                                                                if($kcm != ''){
                                                                    echo "<option value='".$kcm['kecamatan']."' selected='true'>".$kcm['kecamatan']."</option>";
                                                                    echo "<script>pfkec = '".$kcm['kecamatan']."'</script>";

                                                                }

                                                            ?>
                                                        </select>
                                                        <small id="upd_cek_kecamatan" style="color: red;"></small>
                                                    </div>
                                                </div>
                                            </div>                                        
                                            <br><br>                                         
                                            <div class="form-group " id="kotak_School">
                                                <label for="inputEmail3" class="col-sm-3 control-label f-12">Nama Sekolah</label>
                                                <div class="col-sm-9">
                                                    <div class="fg-line">
                                                        <select disabled id='upd_school_name' name="school_name" required class="selectupd form-control" data-placeholder="<?php echo $this->lang->line('selectschoolname'); ?>">
                                                            <option value=''></option>
                                                             <?php
                                                                    // $name = $this->Master_model->school_getName($d_schoolname,$d_kelurahan);
                                                                    if($name == ''){
                                                                        echo "<option value='".$name['school_name']."' selected='true'>".$name['school_name']."</option>";
                                                                        echo "<script>pfname = ''</script>";
                                                                    }

                                                                ?>
                                                        </select>
                                                        <label class="pull-right" style="cursor: pointer;">Sekolah belum terdaftar ? <a id="upd_addSchool" href="#"> Tambah Sekolah</a></label>
                                                        <small id="upd_cek_schoolname" style="color: red;"></small>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div id="upd_kotak_addSchool" disabled class="form-group " style="display: none;">
                                            </div>
                                            <br><br>                                         
                                            <div class="form-group ">
                                                <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('school_address'); ?></label>
                                                <div class="col-sm-9">
                                                    <div class="fg-line">
                                                        <textarea name="school_address" id="upd_school_address" rows="3" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typeschooladdress'); ?>" value="<?php echo $get_school_address; ?>"></textarea>
                                                    </div>
                                                </div>
                                            </div> 
                                            <input hidden type="text" name="school_id" id="upd_school_id" value="<?php echo $get_school_id; ?>">
                                        </div>
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <label style="cursor: pointer; color: #2196F3" class=""><input type="checkbox" value="15" id="upd_check_alamat"> Alamat Sekolah masih sama</label>        
                                        </div>  
                                        
                                        <br><br><br>
                                        <br><br>
                                    </div>

                                    <div class="card-padding card-body">                                                                           
                                        <button type="submit" id="btnUpdateJenjang" name="btnUpdateJenjang" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>                    
                                    </div>
                                <!-- </form> -->
                            </div>
                        <?php    
                        }
                        else if ($cek_school_id!='0' && $getdata['flag'] !='0') {
                         ?>
                            <div id="displaySchool">
                                <div class="pmb-block">
                                    <div class="pmbb-body">
                                        <div class="pmbb-view">
                                            <dl class="dl-horizontal">
                                                <dt>Kelas</dt>
                                                <dd>:
                                                    <?php 
                                                    $jenjang_id   = $this->session->userdata('jenjang_id'); 
                                                    $get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_id."'")->row_array()['jenjang_name'];
                                                    $get_jenjanglevel = $this->db->query("SELECT jenjang_level  FROM master_jenjang where jenjang_id='".$jenjang_id."'")->row_array()['jenjang_level'];
                                                    $kelas = $get_jenjanglevel;   
                                                    if ($get_school_name == "") {
                                                        echo $this->lang->line('nousername');
                                                    }
                                                        echo $kelas." ". $get_jenjangtype; 
                                                    ?>                                            
                                                </dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt><?php echo $this->lang->line('school_name'); ?></dt>
                                                <dd>:
                                                    <?php 
                                                        if ($get_school_name == "") {
                                                            echo $this->lang->line('nousername');
                                                        }
                                                        echo $get_school_name; 
                                                    ?>                                            
                                                </dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt><?php echo $this->lang->line('school_address'); ?></dt>
                                                <dd>:
                                                    <?php 
                                                        if ($get_school_address == "") {
                                                            echo $this->lang->line('nousername');
                                                        }
                                                        echo $get_school_address; 
                                                    ?>                                            
                                                </dd>
                                            </dl>
                                            
                                            <dl class="dl-horizontal">
                                                <dt><?php echo $this->lang->line('selectkecamatann'); ?></dt>
                                                <dd>:
                                                    <?php 
                                                        if ($get_school_name == "") {
                                                            echo $this->lang->line('nousername');
                                                        }
                                                        echo $get_kecamatan; 
                                                    ?>                                            
                                                </dd>
                                            </dl> 
                                            <?php
                                                if ($get_provinsi != "Luar Negeri") {
                                                    ?>
                                                    <dl class="dl-horizontal">
                                                        <dt><?php echo $this->lang->line('selectprovinsii'); ?></dt>
                                                        <dd>:
                                                            <?php 
                                                                if ($get_school_name == "") {
                                                                    echo $this->lang->line('nousername');
                                                                }
                                                                echo $get_provinsi; 
                                                            ?>                                            
                                                        </dd>
                                                    </dl>     
                                                            <?php
                                                        }
                                            ?>                                                       
                                        </div>                            
                                    </div>
                                </div>
                            
                            </div>
                         <?php   
                        }
                        ?>
                        <div align="Right" class="m-r-30">
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div><!-- akhir container -->
    </section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    var cek = "<?php echo $this->session->userdata('code_cek');?>";
        if (cek == "777") {
            $("#alert_updatejenjang").modal('show');
            $("#complete_modal").modal('hide');
        }
    var jid = "AAAA";
    var pid = "AAAA";
    jQuery(document).ready(function(){
        
        $('#btnUpdateJenjang').click(function(){
            var school_id = null;
            var id_user = <?php echo $this->session->userdata('id_user');?>;
            var jenjang_old = <?php echo $this->session->userdata('jenjang_id');?>;
            var jenjang_new = document.getElementById('jenjang_level'),
                jenjang_new = jenjang_level.value;
            if (jenjang_new=="") {
                jenjang_new =15;
                school_id=null;
            }
            else{
                school_id = $("#upd_school_id").val();
            }
            // alert(id_user);
            // return false;

            $.ajax({
                url: '<?php echo BASE_URL();?>V1.0.0/changeDataFromStudent',
                type: 'POST',
                data: {
                    id_user : id_user,
                    jenjang_old : jenjang_old,
                    jenjang_new : jenjang_new,
                    school_id : school_id
                },
                success: function(response)
                {
                   var a = JSON.stringify(response);
                   console.log(a);
                    window.location.href = "<?php echo base_url(); ?> ";
                }
            });
        });
        $('#check_umum').bind('change', function (v) {

            if($(this).is(':checked')) {
                $("#check_umum").val('15');
                $("#jenjang_level").val('15');
                $("#kotak_jenjang").css('display','none');
                $("#kotak_sekolah").css('display','none');
                $('#jenjang_level').attr('disabled','');

            } else {
                $('#jenjang_level').removeAttr('disabled','');
                $("#kotak_jenjang").css('display','block');
                $("#kotak_sekolah").css('display','block');
                $("#jenjang_level").empty();
            }
        });
        $('#upd_check_alamat').bind('change', function (v) {

            if($(this).is(':checked')) {
                $(".new_alamat").css('display','none');
                $(".old_alamat").css('display','block');
                // $('.new_alamat').attr('disabled','');

            } else {
                // $(".new_alamat").empty();
                $(".new_alamat").css('display','block');
                $(".old_alamat").css('display','none');
            }
        });
        
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        var code = null;
        var cekk = setInterval(function(){
            code = "<?php echo $this->session->userdata('code');?>";
            cek();
        },500);

        function cek(){
            if (code == "4044") {
                $("#alertsizebesar").css('display','none');
                $("#alertsuccessupdate").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url();?>Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
            else if (code == "4045") {
                $("#alertsuccessupdate").css('display','none');
                $("#alertsizebesar").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url();?>Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
        }

        jQuery('#change-pic').on('click', function(e){
            jQuery('#changePic').show();
            jQuery('#change-pic').hide();
        });
        
        jQuery('#photoimg').on('change', function()   
        { 
            jQuery("#preview-avatar-profile").html('');
            jQuery("#preview-avatar-profile").html('Uploading....');
            jQuery("#cropimage").ajaxForm(
            {
            target: '#preview-avatar-profile',
            success:    function() { 
                    jQuery('img#photo').imgAreaSelect({
                    aspectRatio: '1:1',
                    onSelectEnd: getSizes,
                });
                jQuery('#image_name').val(jQuery('#photo').attr('file-name'));
                }
            }).submit();
        });
    });
    $(function () {
        $('#datetimepicker2').datetimepicker({
            locale: 'id'
        });

        // $('#school_provinsi').change(function(){
        //     var prov = $(this).val();
        //     var placeprov = "<?php echo $this->lang->line('selectprovinsii'); ?>";
        //     $("#boxkabupaten").empty();
        //     var kotakboxprov = "<select id='school_kabupaten' class='select2 form-control' name='school_kabupaten' data-placeholder='"+placeprov+"'>"+isi+"</select>";
        //     $("#boxkabupaten").append(kotakboxprov);
        // });
        $('select.select2').each(function(){            
            if($(this).attr('id') == 'school_provinsi' || $(this).attr('id') == 'school_kabupaten' || $(this).attr('id') == 'school_kecamatan' ||  $(this).attr('id') == 'school_name'){
                var a = null;
                if ($(this).attr('id') == 'school_provinsi') { a = "<?php echo $this->lang->line('chooseyourpropinsi'); ?>";} if ($(this).attr('id') == 'school_kecamatan') { a = "<?php echo $this->lang->line('chooseyourkecamatan'); ?>";} if ($(this).attr('id') == 'school_kabupaten') { a = "<?php echo $this->lang->line('chooseyourkabupaten'); ?>";} 
                // var jid = document.getElementById('jenjang_level'),
                // jid = jid.value;

                $(this).select2({
                    minimumInputLength: 1,
                    placeholder: a,
                    delay: 2000,
                    ajax: {
                        dataType: 'json',
                        type: 'GET',
                        url: '<?php echo base_url(); ?>ajaxer/ajax_indonesia_school/' + $(this).attr('id'),
                        data: function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1,
                                pfp: pfp,
                                pfka: pfka,
                                pfkec: pfkec,
                                pfname: pfname,
                                jid : pid
                            };
                        },
                        processResults: function(data){
                            return {
                                results: data.results,
                                pagination: {
                                    more: data.more
                                }
                            };
                        }
                    }
                });
            }
            else{
              $(this).select2();
            }
        });
        $('select.selectupd').each(function(){            
            if($(this).attr('id') == 'upd_school_provinsi' || $(this).attr('id') == 'upd_school_kabupaten' || $(this).attr('id') == 'upd_school_kecamatan' ||  $(this).attr('id') == 'upd_school_name'){
                var a = null;
                if ($(this).attr('id') == 'upd_school_provinsi') { a = "<?php echo $this->lang->line('chooseyourpropinsi'); ?>";} if ($(this).attr('id') == 'upd_school_kecamatan') { a = "<?php echo $this->lang->line('chooseyourkecamatan'); ?>";} if ($(this).attr('id') == 'upd_school_kabupaten') { a = "<?php echo $this->lang->line('chooseyourkabupaten'); ?>";} 
                // var jid = document.getElementById('jenjang_level'),
                // jid = jid.value;

                $(this).select2({
                    minimumInputLength: 1,
                    placeholder: a,
                    delay: 2000,
                    ajax: {
                        dataType: 'json',
                        type: 'GET',
                        url: '<?php echo base_url(); ?>ajaxer/ajax_indonesia_school_upd/' + $(this).attr('id'),
                        data: function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1,
                                pfp: pfp,
                                pfka: pfka,
                                pfkec: pfkec,
                                pfname: pfname,
                                jid : pid
                            };
                        },
                        processResults: function(data){
                            return {
                                results: data.results,
                                pagination: {
                                    more: data.more
                                }
                            };
                        }
                    }
                });
            }
            else{
              $(this).select2();
            }
        });

         $('#upd_addSchool').click(function(){
            var template_addschool="<label for='school_name' class='col-sm-3 control-label f-12 m-t-10'>Nama Sekolah</label>"+
                "<div class='col-sm-9'>"+
                    "<div class='fg-line'>"+
                        "<textarea name='school_name' id='school_name' required class='input-sm form-control fg-input' placeholder='<?php echo $this->lang->line('typeschoolname'); ?>' ></textarea>"+
                    "</div>"+
                "</div>";
            $("#upd_kotak_addSchool").css("display","block");
            $("#upd_kotak_addSchool").append(template_addschool);
            $("#upd_kotak_School").css("display","none");
            $("#upd_kotak_addSchool").removeAttr('disabled');
        });

        $('#upd_school_provinsi').change(function(){
            var prov = $(this).val();
            var jid = document.getElementById('jenjang_level'),
                jid = jid.value;
            pid = jid;
            pfp = prov;            
            $('#upd_school_kabupaten.select2').select2("val","");
            $("#upd_school_kabupaten").removeAttr('disabled');
            $("#upd_school_kecamatan").attr('disabled','');
            
            $("#upd_school_name").attr('disabled','true');
        });
        $('#upd_school_kabupaten').change(function(){
            var prov = $(this).val();
            jid = document.getElementById('jenjang_level'),
                jid = jid.value;
            pfka = prov;            
            $('#upd_school_kecamatan.select2').select2("val","");
            $("#upd_school_kecamatan").removeAttr('disabled');            
            $("#upd_school_name").attr('disabled','true');
        });
        $('#upd_school_kecamatan').change(function(){
            var prov = $(this).val();
            jid = document.getElementById('jenjang_level'),
            jid = jid.value;
            pfkec = prov;            

            $('#upd_school_name.select2').select2("val","");            
            $("#upd_school_name").removeAttr('disabled');
        });
         $('#upd_school_name').change(function(){
            var prov = $(this).val();   
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>ajaxer/getSchoolAddress/'+prov,
                success:function(datis){
                    var address = $.trim(datis);
                    $('#upd_school_address').val(address);
                }
            });
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>ajaxer/getSchoolID/'+prov,
                success:function(datis){
                    var schoolid = $.trim(datis);
                    $('#upd_school_id').val(schoolid);
                }
            });
        });

        $('#jenjang_level').change(function(){
            
            jid = document.getElementById('jenjang_level'),
            jid = jid.value;
            // alert(jid);
        });

        $('#addSchool').click(function(){
            var template_addschool="<label for='school_name' class='col-sm-3 control-label f-12 m-t-10'>Nama Sekolah</label>"+
                "<div class='col-sm-9'>"+
                    "<div class='fg-line'>"+
                        "<textarea name='school_name' id='school_name' required class='input-sm form-control fg-input' placeholder='<?php echo $this->lang->line('typeschoolname'); ?>' ></textarea>"+
                    "</div>"+
                "</div>";
            $("#kotak_addSchool").css("display","block");
            $("#kotak_addSchool").append(template_addschool);
            $("#kotak_School").css("display","none");
            $("#kotak_addSchool").removeAttr('disabled');
        });

        $('#school_provinsi').change(function(){
            var prov = $(this).val();
            // var jid = document.getElementById('jenjang_level'),
                // jid = jid.value;
            pid = jid;
            pfp = prov;            
            $('#school_kabupaten.select2').select2("val","");
            $("#school_kabupaten").removeAttr('disabled');
            $("#school_kecamatan").attr('disabled','');
            
            $("#school_name").attr('disabled','true');
        });
        $('#school_kabupaten').change(function(){
            var prov = $(this).val();
            // jid = document.getElementById('jenjang_level'),
                // jid = jid.value;
            pfka = prov;            
            $('#school_kecamatan.select2').select2("val","");
            $("#school_kecamatan").removeAttr('disabled');            
            $("#school_name").attr('disabled','true');
        });
        $('#school_kecamatan').change(function(){
            var prov = $(this).val();
            // jid = document.getElementById('jenjang_level'),
            // jid = jid.value;
            pfkec = prov;            

            $('#school_name.select2').select2("val","");            
            $("#school_name").removeAttr('disabled');
        });
        $('#jenjang_level').change(function(){
            
            // jid = document.getElementById('jenjang_level'),
            // jid = jid.value;
            // alert(jid);
        });
        
        
        $('#school_name').change(function(){
            var prov = $(this).val();   
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>ajaxer/getSchoolAddress/'+prov,
                success:function(datis){
                    var address = $.trim(datis);
                    $('#school_address').val(address);
                }
            });
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>ajaxer/getSchoolID/'+prov,
                success:function(datis){
                    var schoolid = $.trim(datis);
                    $('#school_id').val(schoolid);
                }
            });
        });
    });
    $(document).ready(function(){
        // var code_cek = null;
        // var cekk = setInterval(function(){
        //     code_cek = "<?php echo $this->session->userdata('code_cek');?>";
        //     cek();
        // },5000);
        // alert('aasdfsd');
     
        var code_cek = null;
        var cekk = setInterval(function(){
            code_cek = "<?php echo $this->session->userdata('code_cek');?>";
            cek();
        },500);

        // alert('aasdfsd');
        function cek(){
            if (code_cek == "888") {
                $("#complete_modal").modal('show');
                $("#alertcomplete_school").css('display','block');
                // $('#alert_complete_data').css('display','block');
                code_cek == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url(); ?>Rest/clearsession',
                    type: 'POST',
                    data: {
                        cek_code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }

           
            else
            {

            }
            // console.warn(code_cek);
        }

    });
</script>
