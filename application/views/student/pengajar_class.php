<div id="container" class="effect aside-float aside-bright mainnav-lg">
        
    <!--NAVBAR-->
    <!--===================================================-->
    <header id="navbar">
        <?php $this->load->view('inc/navbar_student'); ?>
    </header>
    <!--===================================================-->
    <!--END NAVBAR-->

    <div class="boxed">

        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">
            <div id="page-head">                    
                <div class="text-center pad-btm">
                    <h3>Daftar Pengajar IndiClass</h3>
                    <p>List pengajar <strong class="text-main">Indiclass</strong></p>
                </div>
                <!---------------------------------->
            </div>            
            
            <!--Page content-->
            <!--===================================================-->
            <div id="page-content">
            
                <div class="row spinner-example" id="boxListPengajar">
                    
            
                </div>
            
            </div>
            <!--===================================================-->
            <!--End page content-->

        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->
        
        <!--MAIN NAVIGATION-->
        <!--===================================================-->
        <nav id="mainnav-container">
            <?php $this->load->view('inc/sidebar_student');?>
        </nav>
        <!--===================================================-->
        <!--END MAIN NAVIGATION-->

    </div>

    <!-- FOOTER -->
    <!--===================================================-->
    <footer id="footer">
        <?php $this->load->view('inc/bottom/bottom_student'); ?>
    </footer>
    <!--===================================================-->
    <!-- END FOOTER -->

    <!-- SCROLL PAGE BUTTON -->
    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->
    <div class="mainnav-backdrop"></div>

</div>

<script type="text/javascript">
    
    $(document).ready(function() {

        var access_token    = "<?php echo $this->session->userdata('access_token');?>";
        var id_user         = "<?php echo $this->session->userdata('id_user');?>";
        var boxPengajar     = "";

        function imgError(image) {
            image.onerror = "";
            image.src = "https://cdn.classmiles.com/usercontent/dXNlci9lbXB0eS5qcGc=";
            return true;
        }

        $.ajax({
            url: 'https://classmiles.com/api/v1/allSubject_bytutor/id_user/'+id_user+"/access_token/"+access_token,
            type: 'GET',
            success: function(response)
            { 
                var status  = response['status'];
                if (status) {
                    for (var i = 0; i < response['data'].length; i++) {
                        var tutor_id        = response['data'][i]['tutor_id'];
                        var tutor_name      = response['data'][i]['tutor_name'];
                        var tutor_image     = response['data'][i]['tutor_image'];
                        var tutor_country   = response['data'][i]['tutor_country'];

                        boxPengajar += "<div class='col-sm-4 col-md-3 col-xs-6'>"+
                            "<div class='panel pos-rel'>"+
                                "<div class='pad-all text-center'>"+                                    
                                    "<a href='#'>"+
                                        "<img alt='Profile Picture' class='img-lg img-circle mar-ver' src='https://cdn.classmiles.com/usercontent/"+tutor_image+"' onerror='imgError(this);''>"+
                                        "<p class='text-lg text-semibold mar-no text-main' style='height:35px;'>"+tutor_name+"</p>"+
                                        "<p class='text-sm'>"+tutor_country+"</p>"+                                        
                                    "</a>"+                                    
                                "</div>"+
                            "</div>"+
                        "</div>";
                    }

                    $("#boxListPengajar").append(boxPengajar);
                    
                }
            }
        });

    });

</script>
    
    
    