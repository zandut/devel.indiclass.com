<style type="text/css">
	.select2-container--open{
z-index:9999999
}
</style>
<div id="container" class="effect aside-float aside-bright mainnav-lg mainnav-fixed navbar-fixed footer-fixed">
        
    <!--NAVBAR-->
    <!--===================================================-->
    <header id="navbar">
        <?php $this->load->view('inc/navbar_student'); ?>
    </header>
    <!--===================================================-->
    <!--END NAVBAR-->

    <div class="boxed">

        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">
            <div id="page-head">
                
                <div class="pad-all text-center">
                	<h3>Cari Kelas Privat</h3>
                </div>
            </div>

            
            <!--Page content-->
            <!--===================================================-->
            <div id="page-content" style="z-index: 9999;">

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Kelas Privat</h3>
                    </div>
                    <div class="panel-body">
                    	<div class="alert alert-success" id="alertRequestSuccess" style="display: none;">
		                    <strong>Permintaan Berhasil!</strong> Silahkan Tunggu hingga tutor menjawab permintaan anda.
		                </div>
		                <div class="alert alert-danger" id="alertLewatBatas" style="display: none;">
		                    <strong>Permintaan Anda Gagal!</strong> Silahkan pilih tanggal dan waktu yang lain.
		                </div>
		                <div class="alert alert-danger" id="alertFailed" style="display: none;">
		                    <strong>Permintaan Anda Gagal!</strong> Silahkan pilih tanggal dan waktu yang lain.
		                </div>

                        <div class="row">
	                        <div class="col-sm-3">
	                            <div class="form-group">
	                                <label class="control-label">Pelajaran</label>
	                                <select data-placeholder="Pilih Pelajaran" id="pelajaran" class="form-control select2" tabindex="2">
		                                <option disabled selected>Pilih Pelajaran</option>
		                            </select>
	                            </div>
	                        </div>
	                        <div class="col-sm-3">
	                            <div class="form-group">
	                                <label class="control-label">Tanggal</label>
	                                <div class="input-group date">
	                                    <input type="text" class="form-control" id="dtrq" style="position: relative; z-index:100 !important;">
	                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-sm-2">
	                            <div class="form-group">
	                                <label class="control-label">Waktu</label>
	                                <div class="input-group bootstrap-timepicker timepicker">
							            <input id="tmrq" data-time-format="H:i" type="text" class="form-control input-small">
							            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							        </div>
	                            </div>
	                        </div>
	                        <div class="col-sm-2">
	                            <div class="form-group">
	                                <label class="control-label">Durasi</label>
	                                <select data-placeholder="Pilih Durasi..." id="drrq" class="form-control select2" tabindex="99"  style="z-index: 99999;">
	                                	<option disabled selected>Pilih Durasi</option>
		                                <option value="900">15 Menit</option>
                                        <option value="1800">30 Menit</option>
                                        <option value="2700">45 Menit</option>
                                        <option value="3600">1 Jam</option>
                                        <option value="5400">1 Jam 30 Menit</option>
                                        <option value="7200">2 Jam</option>
                                        <option value="9000">2 Jam 30 Menit</option>
                                        <option value="10800">3 Jam</option>
                                        <option value="12600">3 Jam 30 Menit</option>
                                        <option value="14400">4 Jam</option>
                                        <option value="16200">4 Jam 30 Menit</option>
                                        <option value="18000">5 Jam</option>
                                        <option value="19800">5 Jam 30 Menit</option>
                                        <option value="21600">6 Jam</option>
                                        <option value="23400">6 Jam 30 Menit</option>
                                        <option value="25200">7 Jam</option>
                                        <option value="27000">7 Jam 30 Menit</option>
                                        <option value="28800">8 Jam</option>
                                        <option value="30600">8 Jam 30 Menit</option>
                                        <option value="32400">9 Jam</option>
                                        <option value="34200">9 Jam 30 Menit</option>
                                        <option value="36000">10 Jam</option>
                                        <option value="37800">10 Jam 30 Menit</option>
                                        <option value="39600">11 Jam</option>
                                        <option value="41400">11 Jam 30 Menit</option>
                                        <option value="43200">12 Jam</option>
                                        <option value="45000">12 Jam 30 Menit</option>
                                        <option value="46800">13 Jam</option>
                                        <option value="48600">13 Jam 30 Menit</option>
                                        <option value="50400">14 Jam</option>
                                        <option value="52200">14 Jam 30 Menit</option>
                                        <option value="54000">15 Jam</option>
                                        <option value="55800">15 Jam 30 Menit</option>
                                        <option value="57600">16 Jam</option>
                                        <option value="59400">16 Jam 30 Menit</option>
                                        <option value="61200">17 Jam</option>
                                        <option value="63000">17 Jam 30 Menit</option>
                                        <option value="64800">18 Jam</option>
                                        <option value="66600">18 Jam 30 Menit</option>
                                        <option value="68400">19 Jam</option>
                                        <option value="70200">19 Jam 30 Menit</option>
                                        <option value="72000">20 Jam</option>
                                        <option value="73800">20 Jam 30 Menit</option>
                                        <option value="75600">21 Jam</option>
                                        <option value="77400">21 Jam 30 Menit</option>
                                        <option value="79200">22 Jam</option>
                                        <option value="81000">22 Jam 30 Menit</option>
                                        <option value="82800">23 Jam</option>
                                        <option value="84600">23 Jam 30 Menit</option>
                                        <option value="86400">24 Jam</option>
		                            </select>
	                            </div>
	                        </div>
	                        <div class="col-sm-2">
	                            <div class="form-group">
	                                <label class="control-label" style="color: white">Cari</label>
	                                <button class="btn btn-info btn-block btnSearchPrivate">Cari</button>
	                            </div>
	                        </div>
	                    </div>
                    </div>
                </div>
                

                <div class="panel" id="boxPanelList" style="display: none;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Hasil Pencarian : </h3>
                    </div>
                    <div class="panel-body" id="listTutorPrivate">
                        
                    </div>
                </div>

            </div>
            <!--===================================================-->
            <!--End page content-->

        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->
        
        <!--MAIN NAVIGATION-->
        <!--===================================================-->
        <nav id="mainnav-container">
            <?php $this->load->view('inc/sidebar_student');?>
        </nav>
        <!--===================================================-->
        <!--END MAIN NAVIGATION-->

        <div class="modal" id="modalApproveRequest" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">

	                <!--Modal header-->
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
	                    <h4 class="modal-title">Permintaan Kelas</h4>
	                </div>


	                <!--Modal body-->
	                <div class="modal-body">	                    
	                    <p>Dengan menekan tombol PILIH maka permintaan kelas secara otomatis disampaikan kepada Tutor bersangkutan. Apabila Tutor menerima permintaan Anda tersebut, maka Anda akan menerima Email detail pembayaran kelas yang sudah Anda request.</p>
	                    <br>
	                    <div class="list-group bg-trans">
			                <a href="#" class="list-group-item">
			                    <div class="media-left pos-rel">
			                        <img class="img-circle img-lg" id="photo_desc" alt="Profile Picture">
			                        <i class="badge badge-success badge-stat badge-icon pull-left"></i>
			                    </div>
			                    <div class="media-body">			                        
			                        <div class="row">
			                        	<div class="col-md-12 col-sm-12 col-xs-12">
			                        		<p id="tutor_desc"></p>
			                        	</div>
			                        	<div class="col-md-3 col-sm-3 col-xs-6">
			                        		<p>Tanggal :</p><br>
			                        		<p id="tanggal_desc"></p>
			                        	</div>
			                        	<div class="col-md-3 col-sm-3 col-xs-6">
			                        		<p>Waktu :</p><br>
			                        		<p id="waktu_desc"></p>
			                        	</div>
			                        	<div class="col-md-3 col-sm-3 col-xs-6">
			                        		<p>Durasi :</p><br>
			                        		<p id="durasi_desc"></p>
			                        	</div>
			                        	<div class="col-md-3 col-sm-3 col-xs-6">
			                        		<p>Harga :</p><br>
			                        		<p id="harga_desc"></p>
			                        	</div>
			                        	<div class="col-md-12 col-sm-12 col-xs-12">
			                        		<p>Topik Pelajaran :</p>
			                        		<textarea placeholder="Isi Topik Pelajaran" rows="4" class="form-control" id="topik_pelajaran"></textarea>
			                        	</div>
			                        </div>			                        
			                    </div>
			                </a>
			            </div>	                    
	                </div>

	                <!--Modal footer-->
	                <div class="modal-footer">
	                    <button data-dismiss="modal" class="btn btn-default" type="button">X</button>
	                    <button class="btn btn-primary" id="btnSendRequestPrivate">Kirim Permintaan</button>
	                </div>
	            </div>
	        </div>
	    </div>

    </div>

    <!-- FOOTER -->
    <!--===================================================-->
    <footer id="footer">
        <?php $this->load->view('inc/bottom/bottom_student'); ?>
    </footer>
    <!--===================================================-->
    <!-- END FOOTER -->

    <!-- SCROLL PAGE BUTTON -->
    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->
    <div class="mainnav-backdrop"></div>

</div>

<script type="text/javascript">
	
	$(document).ready(function() {

		var access_token    = "<?php echo $this->session->userdata('access_token');?>";
        var id_user         = "<?php echo $this->session->userdata('id_user');?>";
        var d               = new Date();
        var user_utc 		= "420";

		$("#pelajaran").select2();
		$("#drrq").select2();
		$(".date").datepicker();
		$('#tmrq').timepicker();

		$.ajax({
            url: 'https://classmiles.com/api/v1/allSubject/id_user/'+id_user+'/access_token/'+access_token,
            type: 'GET',   
            success: function(response)
            {
                if (response['status']) {
                    
                    for (var i = 0; i < response['data'].length; i++) {                    	
                    	var subject_id   = response['data'][i]['subject_id'];
                    	var subject_name = response['data'][i]['subject_name'];                    	
                        
                        $("#pelajaran").append("<option value='"+subject_id+"'>"+subject_name+"</option>");                            
                    }
                    
                }
            }
        }); 

		function secondsToHms(d) {
		    d = Number(d);
		    var h = Math.floor(d / 3600);
		    var m = Math.floor(d % 3600 / 60);
		    var s = Math.floor(d % 3600 % 60);

		    var hDisplay = h > 0 ? h + (h == 1 ? " Jam, " : " Jam, ") : "";
		    var mDisplay = m > 0 ? m + (m == 1 ? " Menit, " : " Menit, ") : "";
		    var sDisplay = s > 0 ? s + (s == 1 ? " Detik" : " Detik") : "";
		    return hDisplay + mDisplay + sDisplay; 
		}

        $(document).on("click",".btnSearchPrivate",function() { 
        	var subject_id 		= $("#pelajaran").val();
        	var dtrq			= $("#dtrq").val();
        	var tmrq 			= $("#tmrq").val();
        	var drrq 			= $("#drrq").val();
        	dtrq = dtrq.replace(/(\d\d)\/(\d\d)\/(\d{4})/, "$3-$1-$2");

			var hours = Number(tmrq.match(/^(\d+)/)[1]);
			var minutes = Number(tmrq.match(/:(\d+)/)[1]);
			var AMPM = tmrq.match(/\s(.*)$/)[1];
			if(AMPM == "PM" && hours<12) hours = hours+12;
			if(AMPM == "AM" && hours==12) hours = hours-12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if(hours<10) sHours = "0" + sHours;
			if(minutes<10) sMinutes = "0" + sMinutes;			

        	$.ajax({
	            url: 'https://classmiles.com/api/v1/ajax_get_ondemand',
	            type: 'GET',   
	            data : {
	            	subject_id : subject_id,
	            	dtrq : dtrq,
	            	tmrq : sHours + ":" + sMinutes,
	            	drrq : drrq
	            },
	            success: function(response)
	            {
	            	var listTutorPrivate = "";

	            	if (response['status']) {
	            		$("#listTutorPrivate").empty();
	            		$("#boxPanelList").css('display','block');
	            		for (var i = 0; i < response['data'].length; i++) {
		                	var class_type 	= response['data'][i]['class_type'];

		                	var country 	= response['data'][i]['country'];
		                	var subject_id 	= response['data'][i]['subject_id'];
		                	var subject_name= response['data'][i]['subject_name'];
		                	var tutor_id 	= response['data'][i]['tutor_id'];
		                	var user_image 	= response['data'][i]['user_image'];
		                	var user_name 	= response['data'][i]['user_name'];
		                	var harga_tutor = response['data'][i]['harga_tutor'];
		                	var harga_cm 	= response['data'][i]['harga_cm'];
		                	var hargaakhir 	= response['data'][i]['hargaakhir'];

		                	var duration = secondsToHms(drrq);

		                	listTutorPrivate += "<div class='col-md-3 col-sm-3 col-xs-3'>"+
		                		"<div class='panel' style='border-radius: 10px;'>"+
				                    "<div class='panel-body text-center bg-purple'>"+
				                        "<img alt='Avatar' class='img-lg img-circle img-border mar-btm' src='"+user_image+"'>"+
				                        "<h4 class='text-light' style='height:30px;'>"+user_name+"</h4>"+
				                        "<p>"+subject_name+"</p>"+
				                    "</div>"+
				                    "<div class='list-group bg-trans pad-btm' style='border: solid 1px #ececec;'>"+
				                        "<a class='list-group-item' href='#'><h5 class='label label-info pull-right'>"+dtrq+"</h5><i class='fa fa-calendar icon-lg icon-fw'></i> Tanggal</a>"+
				                        "<a class='list-group-item' href='#'><h5 class='label label-info pull-right'>"+sHours+":"+sMinutes+"</h5><i class='fa fa-clock-o icon-lg icon-fw'></i> Waktu</a>"+
				                        "<a class='list-group-item' href='#'><h5 class='label label-info pull-right'>"+duration+"</h5><i class='fa fa-clock-o icon-lg icon-fw'></i> Durasi</a>"+
				                        "<a class='list-group-item' href='#'><h5 class='label label-info pull-right'>Rp. "+hargaakhir+"</h5><i class='fa fa-dollar icon-lg icon-fw'></i> Harga</a>"+
				                        "<a class='list-group-item'><button class='btn btn-success btn-block btnRequestPrivate' subject_id="+subject_id+" tutor_id="+tutor_id+" hr="+harga_tutor+" hrt="+harga_cm+" photo="+user_image+" tutor_desc="+subject_name+" dtrq="+dtrq+" tmrq="+sHours + ":" + sMinutes+" drrq="+drrq+">Pilih</button></a>"+
				                    "</div>"+				                    
			                    "</div>"+
			                "</div>";
		                }

		                $("#listTutorPrivate").append(listTutorPrivate);
		            }

	            }
	        });

        });

		$(document).on("click",".btnRequestPrivate",function() {
			var subject_id 		= $(this).attr('subject_id');
			var tutor_id 		= $(this).attr('tutor_id');
			var hrt 			= $(this).attr('hrt');
			var hr 				= $(this).attr('hr');
			var photo 			= $(this).attr('photo');
			var dtrq 			= $(this).attr('dtrq');
			var tmrq 			= $(this).attr('tmrq');
			var drrq 			= $(this).attr('drrq');
			var tutor_desc 		= $(this).attr('tutor_desc');

			$("#tutor_desc").html(tutor_desc);
			$("#photo_desc").attr('src',photo);
			$("#tanggal_desc").html(dtrq);
			$("#waktu_desc").html(tmrq);
			$("#durasi_desc").html(secondsToHms(drrq));
			$("#harga_desc").html("Rp. "+hr);
			$("#btnSendRequestPrivate").attr('subject_id',subject_id);
			$("#btnSendRequestPrivate").attr('tutor_id',tutor_id);
			$("#btnSendRequestPrivate").attr('hr',hr);
			$("#btnSendRequestPrivate").attr('hrt',hrt);
			$("#modalApproveRequest").modal("show");
		});

		$(document).on("click","#btnSendRequestPrivate",function() {
			var subject_id 		= $(this).attr('subject_id');
			var tutor_id 		= $(this).attr('tutor_id');
			var hr 				= $(this).attr('hr');
			var hrt 			= $(this).attr('hrt');
			var dtrq			= $("#dtrq").val();
        	var tmrq 			= $("#tmrq").val();
        	var drrq 			= $("#drrq").val();
        	var topic 			= $("#topik_pelajaran").val();

        	dtrq = dtrq.replace(/(\d\d)\/(\d\d)\/(\d{4})/, "$3-$1-$2");

        	var hours = Number(tmrq.match(/^(\d+)/)[1]);
			var minutes = Number(tmrq.match(/:(\d+)/)[1]);
			var AMPM = tmrq.match(/\s(.*)$/)[1];
			if(AMPM == "PM" && hours<12) hours = hours+12;
			if(AMPM == "AM" && hours==12) hours = hours-12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if(hours<10) sHours = "0" + sHours;
			if(minutes<10) sMinutes = "0" + sMinutes;

			$.ajax({
	            url: 'https://classmiles.com/api/v1/requestprivate',
	            type: 'GET',   
	            data : {
	            	subject_id : subject_id,
	            	tutor_id : tutor_id,
	            	id_user : id_user,
	            	start_time : sHours+":"+sMinutes,
	            	date : dtrq,
	            	user_utc : '420',
	            	duration : drrq,
	            	topic : topic,
	            	hr : hr,
	            	hrt : hrt,
	            	user_device : 'web'
	            },
	            success: function(response)
	            {
	            	$("#modalApproveRequest").modal("hide");
	            	window.scrollTo(0,0);
	                if (response['status']) {
	                    
	                    $("#alertRequestSuccess").css('display','block');
	                    setTimeout(function(){
                            location.reload();
                        },2000);
	                    
	                }
	                else
	                {
	                	if (response['code'] == '402') {
	                		$("#alertLewatBatas").css('display','block');
		                    setTimeout(function(){
	                            location.reload();
	                        },2000);
	                	}
	                	else
	                	{
	                		$("#alertFailed").css('display','block');
		                    setTimeout(function(){
	                            location.reload();
	                        },2000);
	                	}
	                }
	            }
	        });
		});
	});

</script>

    
    
    
