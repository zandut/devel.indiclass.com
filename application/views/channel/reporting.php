<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('channel/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sidechannel'); ?>
    </aside>

    <section id="content" style="margin-top: 5%;">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>List Penggunaan Point</h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul> -->
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card">  
                <div class="card-header"></div>              
                <div class="card-body card-padding">
                    <div class="row"> 
                        <div class="table-responsive">
                            <br>
                            <table id="list" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="bgm-teal c-white" style="width: 10px">No</th>
                                        <th class="bgm-teal c-white" style="width: 15px">Nama Class</th>
                                        <th class="bgm-teal c-white" style="width: 30px">Description</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Tutor</th>                                        
                                        <th class="bgm-teal c-white" style="width: 20px">Waktu Mulai</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Waktu Akhir</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Durasi Class</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Jumlah Peserta</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Tipe Class</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Point Awal</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Point Terpakai</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Point Akhir</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody id="box_listpoint">
                                           
                                </tbody>
                            </table> 
                        </div>                           
                    </div>                                               
                </div>                                             
            </div> 

            <!-- Modal TAMBAH -->  
            <div class="modal fade" id="modalTambah" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Tambah Akun Rekening</h4>
                            <hr>
                        </div>                        
                        <div class="modal-body">
                            <div class="card">
                            <div class="row p-15">                                 
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nama Akun</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="namaakun" id="namaakun" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nomer Rekening</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="norekening" id="norekening" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nama Bank</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <div class="select">
                                                    <select required name="namabank" id="namabank" class="selectpicker" data-live-search="true" style="z-index: 10;">
                                                    <option disabled selected>Pilih Bank</option>   
                                                    <?php
                                                       $allbank = $this->db->query("SELECT * FROM master_bank")->result_array();
                                                       foreach ($allbank as $row => $v) {
                                                            echo '<option value="'.$v['bank_id'].'">'.$v['bank_name'].'</option>';
                                                        }
                                                    ?>                                                    
                                                    </select>
                                                </div>

                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Cabang</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="cabangbank" id="cabangbank" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn bgm-green" id="simpandatarekening">Simpan</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>                              

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Hapus Akun Bank</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>
                            <input type="text" name="id_rekeningg" id="id_rekeningg" class="c-black" value=""/>
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" id="hapusdatarekening">Ya</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </section>

</section>
<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){
        var channelid = "<?php echo $this->session->userdata('channel_id');?>";
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        $.ajax({
            url: '<?php echo base_url(); ?>Rest/getReportingPoint/access_token/'+tokenjwt,
            type: 'POST',
            data: {
                channel_id: channelid,
                type: 'channel'
            },
            success: function(response)
            {   
                var a = JSON.stringify(response);
                var jsonPretty = JSON.stringify(JSON.parse(a),null,2);                      
                // $("#aaa").text(jsonPretty);  
                // alert(response['data'][1]['class_id']);

                if (response['data'] == null) 
                {                                                
                }                   
                else
                {
                    var no = 0;
                    if (response.data.length != null) {
                        for (var i = response.data.length-1; i >=0;i--) {

                            var name                = response['data'][i]['name'];
                            var description         = response['data'][i]['description'];
                            var user_name           = response['data'][i]['user_name'];
                            var start_time          = response['data'][i]['start_time'];
                            start_time = moment(start_time).format('HH:ss');
                            var finish_time         = response['data'][i]['finish_time'];
                            finish_time = moment(finish_time).format('HH:ss');
                            var duration            = response['data'][i]['duration'];
                            var participant_listt   = response['data'][i]['participant']['participant'];
                            var count_student       = participant_listt.length;
                            var class_type          = response['data'][i]['class_type'];
                            var last_point          = response['data'][i]['last_point'];
                            var amount              = response['data'][i]['amount'];
                            var new_point           = response['data'][i]['new_point'];
                            var created_at          = response['data'][i]['created_at'];    
                            var created_at_point    = response['data'][i]['created_at_point'];    
                           
                            var hours = Math.floor(duration / 3600);
                            var min = Math.floor((duration - (hours*3600)) / 60);
                            var seconds = Math.floor(duration % 60);

                            var value = "";
                            if(min > 0) {
                                if(min == 1) {
                                   value = " Menit ";
                                } else {
                                   value = " Menit " + value;
                                }
                                value = min + value;
                            }

                            if(hours > 0) {
                                if(hours == 1) {
                                   value = " Jam " + value;
                                } else {
                                   value = " Jam " + value;
                                }
                                value = hours + value;
                            }    

                            no += 1;
                            if (class_type == "multicast_channel_paid") {
                                class_type = "Multicast";
                            }
                            else if (class_type == "private_channel") {
                                class_type = "Private";
                            }
                            else
                            {
                                class_type = "Group";
                            }

                            var kotak = "<tr><td>"+no+"</td><td>"+name+"</td><td>"+description+"</td><td>"+user_name+"</td><td>"+start_time+"</td><td>"+finish_time+"</td><td>"+value+"</td><td>"+count_student+"</td><td>"+class_type+"</td><td>"+last_point+"</td><td>"+amount+"</td><td>"+new_point+"</td><td>"+created_at_point+"</td></tr>";
                            
                            $("#list tbody").append(kotak);
                            $("#list" ).DataTable();
                        }
                    }
                }
            }
        }); 
    });
</script>
