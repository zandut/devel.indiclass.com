
<div id="loader">
    <div class="page-loader" style="background-color:#1274b3;">
        <div class="preloader pl-xl pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Class Miles . . .</p>
        </div>
    </div>
</div>

<header hidden id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #2196f3;">
    
    <ul class="header-inner container">
        <li>
            <a href="#" class="m-r-20"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" alt=""></a>
        </li>
        <li class="pull-right">
            <a href="#" class="m-l-10"><img id="channel_logo" style='height: 50px;' alt=""></a>
        </li>  
    </ul>
    
    <!-- Top Search Content -->
    <div id="top-search-wrap">
        <div class="tsw-inner">
            <i id="top-search-close" class="zmdi zmdi-arrow-left"></i>
            <input type="text" placeholder="Search Class in here">            
        </div>
    </div>

    <!-- <div class="container">        
        <nav class="ha-menu" style="background-color: rgba(33, 150, 243, 0.6); color: gray; width: 100%; padding: 0; margin: 0;">
            <ul>                    
                <li class="active waves-effect"> <a href="#">Channel Description</a></li>
                <li class="waves-effect pull-right active"> <a href="#" id="channel_name"></a></li>
            </ul>
        </nav>        
    </div> -->

    <div>        


</header>

<style type="text/css">


#desc_channellogo{
    margin-top: 0%;
    width: 330px;
    height: 330px;
}
#channel_mobile{
    display: none;
}

#boxblue{
    margin-left: -9%; 
    width: 118%;
}
#kolom_about{
    margin-top: -13%;
}

p.test {
    white-space: : pre;
}
input[readonly] {
 background-color: #dddddd;
}
img {
    background-color: white;
    border: 1px solid #ddd;
    border-radius: 1px;
    padding: 3px;
}
/*body{
    background-color: white;
}*/

p.uppercase {
    text-transform: uppercase;
}
    html{
        overflow-x: hidden;
    }
    #widgetright{
        margin-top: -25%;
    }
    #titlesubject{
        margin-top: -47%;
        margin-bottom: 20%;
        background-color: rgba(238, 238, 238, 0);
        background-color: white; padding: 1%; background:rgba(0,0,0,0.3); border-radius: 0px;
        border: none;
    }
    #boxblue{
        height: 350px; /*background-color: rgba(33, 150, 243, 0.77);*/ padding: 0; margin-bottom: 1.5%; width: 100%; display: block;
        background-repeat: no-repeat; background-size: 100%;
    }
    #titleone{
        color: white;
    }
    #titletwo{
        color: white;
    }
     #data_channel{
        color: white;
    }
     #address_channel{
        color: white;
    }
    #desc_date{
        color: #ffffff;
    }
    #desc_starttime{
        color: #ffffff;
    }
    #desc_finishtime{
        color: #ffffff;
    }
    #desc_classtype{
        color: #ffffff;
    }
    #desc_templatetype{
        color: #ffffff;
    }   
    @media screen and (max-width: 1001px)
    {
        #widgetright{
            margin-top: 20%;
        }
        #boxblue{
            display: inline;
        }
        #titlesubject{
            padding: 3%;
            margin-top: -50%;
            margin-bottom: 3%;
            background-color: #ffffff;
            background-color: white; padding: 1%; background:rgba(0,0,0,0.3); border-radius: 5px;
        }
        #desc_channellogo{
            margin-top: -195%;
        }
        #kolom_about{
            margin-top: 0%;
            margin-bottom: 2%;
        }
        #titleone{
            color: #ffffff;
        }
        #titletwo{
            color: #ffffff;
        }
        #desc_date{
            color: gray;
        }
        #desc_starttime{
            color: gray;
        }
        #desc_finishtime{
            color: gray;
        }
        #desc_classtype{
            color: gray;
        }
        #desc_templatetype{
            color: gray;
        }
    }
     @media screen and (max-width: 991px)
    {
        #boxblue{

        }
        #channel_mobile{
            display: none;
        }
        #channel_web{
            display: inline;
        }

        #boxblue{
            height: 250px;
        }
        #titleone{
            /*text-align: center;*/
        }
        #titlesubject{
            /*text-align: center;*/
            padding: 3%;
            margin-top: -30%;
            /*height: 260px;*/
            width: 100%;
            margin-bottom: 3%;
            margin-left: -1%;
            color: black;
            /*background-color: rgba(238, 238, 238, 0);*/
            background-color: white; padding: 1%; background:rgba(0,0,0,0.0); border-radius: 5px;
            border: none;
        }
        #kolom_about{
            width: 100%;
            margin-top: 0%;
            margin-left: 0%;
            margin-bottom: 2%;
        }    
        #desc_channellogo{
            margin-top: -60%;
            width: 20%;
            height: auto;
        }
        #titleone{
            color: black;
            font-size: 26px;
        }
        #titletwo{
            color: black;
        }
        #data_channel{
            color: black;
        }
        #address_channel{
            color: black;
        }
    }
    @media screen and (max-width: 767px)
    {
        #boxblue{

        }
        #channel_mobile{
            display: none;
        }
        #channel_web{
            display: inline;
        }

        #boxblue{
            height: 150px;
        }
        #titleone{
            /*text-align: center;*/
        }
        #titlesubject{
            /*text-align: center;*/
            padding: 3%;
            margin-top: -30%;
            /*height: 260px;*/
            width: 100%;
            margin-bottom: 3%;
            margin-left: -1%;
            color: black;
            /*background-color: rgba(238, 238, 238, 0);*/
            background-color: white; padding: 1%; background:rgba(0,0,0,0.0); border-radius: 5px;
            border: none;
        }
        #kolom_about{
            width: 100%;
            margin-top: 0%;
            margin-left: 0%;
            margin-bottom: 2%;
        }    
        #desc_channellogo{
            margin-top: -60%;
            width: 20%;
            height: auto;
        }
        #titleone{
            color: black;
            font-size: 26px;
        }
        #titletwo{
            color: black;
        }
        #data_channel{
            color: black;
        }
        #address_channel{
            color: black;
        }
    }
    @media screen and (max-width: 736px)
   {
        #detail_channel{
            margin-top: -22.5%;
            margin-left: 8%;
        }
        #channel_mobile{
            display: inline;
        }
    }
    @media screen and (max-width: 732px)
   {
        #detail_channel{
            margin-top: -23%;
            margin-left: 8%;
        }
        #channel_mobile{
            display: inline;
        }
        #boxblue{
            height: 150px;
        }
        #titleone{
            /*text-align: center;*/
        }
        #titlesubject{
            /*text-align: center;*/
            padding: 3%;
            margin-top: -30%;
            /*height: 260px;*/
            width: 100%;
            margin-bottom: 3%;
            margin-left: -1%;
            color: black;
            /*background-color: rgba(238, 238, 238, 0);*/
            background-color: white; padding: 1%; background:rgba(0,0,0,0.0); border-radius: 5px;
            border: none;
        }
        #kolom_about{
            width: 100%;
            margin-top: 0%;
            margin-left: 0%;
            margin-bottom: 2%;
        }    
        #desc_channellogo{
            margin-top: -60%;
            width: 20%;
            height: auto;
        }
        #titleone{
            color: black;
            font-size: 26px;
        }
        #titletwo{
            color: black;
        }
        #data_channel{
            color: black;
        }
        #address_channel{
            color: black;
        }
    }
    @media screen and (max-width: 667px)
    {
        #detail_channel{
            margin-top: -25 %;
            margin-left: 8%;
        }
        #channel_mobile{
            display: inline;
        }
        #boxblue{
            height: 150px;
        }
        #titleone{
            /*text-align: center;*/
        }
        #titlesubject{
            /*text-align: center;*/
            padding: 3%;
            margin-top: -30%;
            /*height: 260px;*/
            width: 100%;
            margin-bottom: 3%;
            margin-left: -1%;
            color: black;
            /*background-color: rgba(238, 238, 238, 0);*/
            background-color: white; padding: 1%; background:rgba(0,0,0,0.0); border-radius: 5px;
            border: none;
        }
        #kolom_about{
            width: 100%;
            margin-top: 0%;
            margin-left: 0%;
            margin-bottom: 2%;
        }    
        #desc_channellogo{
            margin-top: -60%;
            width: 20%;
            height: auto;
        }
        #titleone{
            color: black;
            font-size: 26px;
        }
        #titletwo{
            color: black;
        }
        #data_channel{
            color: black;
        }
        #address_channel{
            color: black;
        }
    }
    @media screen and (max-width: 640px)
    {   
        #detail_channel{
            margin-top: -26%;
            margin-left: 8%;
        }
        #channel_mobile{
            display: inline;
        }
        #boxblue{
            height: 150px;
        }
        #titleone{
            /*text-align: center;*/
        }
        #titlesubject{
            /*text-align: center;*/
            padding: 3%;
            margin-top: -35%;
            /*height: 260px;*/
            width: 100%;
            margin-bottom: 3%;
            margin-left: -1%;
            color: black;
            /*background-color: rgba(238, 238, 238, 0);*/
            background-color: white; padding: 1%; background:rgba(0,0,0,0.0); border-radius: 5px;
            border: none;
        }
        #kolom_about{
            width: 100%;
            margin-top: 0%;
            margin-left: 0%;
            margin-bottom: 2%;
        }    
        #desc_channellogo{
            margin-top: -60%;
            width: 20%;
            height: auto;
        }
        #titleone{
            color: black;
            font-size: 26px;
        }
        #titletwo{
            color: black;
        }
        #data_channel{
            color: black;
        }
        #address_channel{
            color: black;
        }
    }
    @media screen and (max-width: 568px)
    {
        #detail_channel{
            margin-top: -30%;
            margin-left: 10%;
        }
        #channel_mobile{
            display: inline;
        }
        #boxblue{
            height: 150px;
        }
        #titleone{
            /*text-align: center;*/
        }
        #titlesubject{
            /*text-align: center;*/
            padding: 3%;
            margin-top: -35%;
            /*height: 260px;*/
            width: 100%;
            margin-bottom: 3%;
            margin-left: -1%;
            color: black;
            /*background-color: rgba(238, 238, 238, 0);*/
            background-color: white; padding: 1%; background:rgba(0,0,0,0.0); border-radius: 5px;
            border: none;
        }
        #kolom_about{
            width: 100%;
            margin-top: 0%;
            margin-left: 0%;
            margin-bottom: 2%;
        }    
        #desc_channellogo{
            margin-top: -60%;
            width: 20%;
            height: auto;
        }
        #titleone{
            color: black;
            font-size: 26px;
        }
        #titletwo{
            color: black;
        }
        #data_channel{
            color: black;
        }
        #address_channel{
            color: black;
        }
    }
    
    @media screen and (max-width: 425px)

    {
        #detail_channel{
            margin-top: -41%;
            margin-left: 15%;
        }
        #channel_mobile{
            display: inline;
        }
        #boxblue{
            height: 100px;
        }
        #titleone{
            /*text-align: center;*/
        }
        #titlesubject{
            /*text-align: center;*/
            padding: 3%;
            margin-top: -40%;
            /*height: 260px;*/
            width: 100%;
            margin-bottom: 3%;
            margin-left: -1%;
            color: black;
            /*background-color: rgba(238, 238, 238, 0);*/
            background-color: white; padding: 1%; background:rgba(0,0,0,0.0); border-radius: 5px;
            border: none;
        }
        #kolom_about{
            width: 100%;
            margin-top: 0%;
            margin-left: 0%;
            margin-bottom: 2%;
        }    
        #desc_channellogo{
            margin-top: -60%;
            width: 20%;
            height: auto;
        }
        #titleone{
            color: black;
            font-size: 26px;
        }
        #titletwo{
            color: black;
        }
        #data_channel{
            color: black;
        }
        #address_channel{
            color: black;
        }
    }
     @media screen and (max-width: 414px)
    {
        #detail_channel{
            margin-top: -44%;
            margin-left: 15%;
        }
    }
    @media screen and (max-width: 412px)
    {
        #detail_channel{
            margin-top: -43%;
            margin-left: 15%;
        }
        #channel_mobile{
            display: inline;
        }
        #boxblue{
            height: 150px;
        }
        #titleone{
            /*text-align: center;*/
        }
        #titlesubject{
            /*text-align: center;*/
            padding: 3%;
            margin-top: -40%;
            /*height: 260px;*/
            width: 100%;
            margin-bottom: 3%;
            margin-left: -1%;
            color: black;
            /*background-color: rgba(238, 238, 238, 0);*/
            background-color: white; padding: 1%; background:rgba(0,0,0,0.0); border-radius: 5px;
            border: none;
        }
        #kolom_about{
            width: 100%;
            margin-top: 0%;
            margin-left: 0%;
            margin-bottom: 2%;
        }    
        #desc_channellogo{
            margin-top: -60%;
            width: 20%;
            height: auto;
        }
        #titleone{
            color: black;
            font-size: 26px;
        }
        #titletwo{
            color: black;
        }
        #data_channel{
            color: black;
        }
        #address_channel{
            color: black;
        }
    }
    @media screen and (max-width: 375px)
    {
        #detail_channel{
            margin-top: -49%;
            margin-left: 15%;
        }
    }

     @media screen and (max-width: 360px)
    {
        #detail_channel{
            margin-top: -50%;
            margin-left: 15%;
        }
        #channel_mobile{
            display: inline;
        }
        #boxblue{
            height: 120px;
        }
        #titleone{
            /*text-align: center;*/
        }
        #titlesubject{
            /*text-align: center;*/
            padding: 3%;
            margin-top: -40%;
            /*height: 260px;*/
            width: 100%;
            margin-bottom: 3%;
            margin-left: -1%;
            color: black;
            /*background-color: rgba(238, 238, 238, 0);*/
            background-color: white; padding: 1%; background:rgba(0,0,0,0.0); border-radius: 5px;
            border: none;
        }
        #kolom_about{
            width: 100%;
            margin-top: 0%;
            margin-left: 0%;
            margin-bottom: 2%;
        }    
        #desc_channellogo{
            margin-top: -60%;
            width: 20%;
            height: auto;
        }
        #titleone{
            color: black;
            font-size: 26px;
        }
        #titletwo{
            color: black;
        }
        #data_channel{
            color: black;
        }
        #address_channel{
            color: black;
        }
    }
     @media screen and (max-width: 320px)
    {
        #detail_channel{
            margin-top: -59%;
            margin-left: 17%;
        }
        #channel_mobile{
            display: inline;
        }
        #boxblue{
            height: 100px;
        }
        #titleone{
            /*text-align: center;*/
        }
        #titlesubject{
            /*text-align: center;*/
            padding: 3%;
            margin-top: -40%;
            /*height: 260px;*/
            width: 100%;
            margin-bottom: 3%;
            margin-left: -1%;
            color: black;
            /*background-color: rgba(238, 238, 238, 0);*/
            background-color: white; padding: 1%; background:rgba(0,0,0,0.0); border-radius: 5px;
            border: none;
        }
        #kolom_about{
            width: 100%;
            margin-top: 0%;
            margin-left: 0%;
            margin-bottom: 2%;
        }    
        #desc_channellogo{
            margin-top: -60%;
            width: 20%;
            height: auto;
        }
        #titleone{
            color: black;
            font-size: 26px;
        }
        #titletwo{
            color: black;
        }
        #data_channel{
            color: black;
        }
        #address_channel{
            color: black;
        }
    }

</style>

<section id="content" style=" margin-top: 0%; overflow-x: hidden;">
    
    <div class="row">
       

    </div>

    <div class="container c-alt" style="background-color: white; ">        
        <div class=" c-alt col-md-12 col-xs-12 col-lg-12" id="boxblue" style="margin-left: -9%; width: 118%;  ">    
        </div>
        <div class="row ">            
            
            <div class="col-md-4 col-sm-12 col-xs-12" id="widgetright">
                <div class=" blog-post">
                    <div class="bp-header" style=" ">
                        <img  data-target="#modalPicts" data-toggle="modal" id="desc_channellogo" alt="" style="cursor: pointer; border-color: black;  ">
                    </div>              
                </div> 

                <div class="card picture-list" style="display: none;">
                    <div class="card-header">
                        <h2>More photo tutor <small></small></h2>
                    </div>
        
                    <div class="pl-body">
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/4.jpg" alt="">
                            </a>
                        </div>
        
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/228.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/308.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/290.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/213000.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/214000.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/52.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/47.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/321.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/37.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/95.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="">
                                <img src="../aset/img/user/32.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>

                                                         
            </div>
            <div class="modal fade" id="modalPict" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <center><img id="view_channellogo" alt="" style=" height: 500px; width: 500px; border-color: black;  "></center>
                </div>
            </div>

            <div class="col-md-8 col-sm-12 col-xs-12">

                
                <div class="block-header">          
                    <!-- <h2>Requirements <small>Access to a computer with an internet connection.</small></h2>                   -->
                </div>
                <br><br>
                <div class="col-sm-12" id="titlesubject" style="">
                    <h1 class="m-l-10 m-r-10 text-uppercase" id="titleone"></h1>
                    <p class="m-l-10 m-r-10" id="titletwo"></p>
                    <div class="col-sm-12" style="">
                        <div class="col-sm-1" style="">
                            <ul class="list-inline list-unstyled" id="data_channel" style="">
                                    <button title="Email" class="btn btn-danger btn-icon waves-effect waves-circle waves-float m-r-10"><i class="zmdi zmdi-email"></i></button>
                            </ul>
                            <ul class="list-inline list-unstyled" id="">
                                    <button title="Phone Number" class=" btn btn-success btn-icon waves-effect waves-circle waves-float m-r-10"><i class="zmdi zmdi-phone"></i></button>                                            
                            </ul>
                            <ul class="list-inline list-unstyled" id="address_channel">
                                    <button title="Address Channel" class=" btn btn-info btn-icon waves-effect waves-circle waves-float"><i class="zmdi zmdi-home"></i></button>
                            </ul>
                        </div>
                        <div class="col-sm-11" style="" id="detail_channel">
                            <ul class="list-inline list-unstyled" id="data_channel" style="">
                                <li class="m-r-10 m-t-10">
                                    <small id="desc_channelemail" class=" f-14"></small>  
                                </li>
                            </ul>
                            <ul class="list-inline list-unstyled" id="address_channel">
                                <li class="m-t-20" style="">
                                    <small id="desc_channelphone" class="f-14"></small>                         
                                </li>                                                
                            </ul>
                            <ul class="list-inline list-unstyled" id="address_channel">
                                <li class="m-t-15">
                                    <small class="f-14" id="desc_channeladdress"></small>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>  
            <div class="  col-md-12 col-sm-12 col-xs-12" id="kolom_about">
                <h3 class="m-l-10 m-r-10">Tentang Channel</h3>
                <textarea readonly class="form-control" style="border-color: white; cursor: normal; background-color: white; height: 300px;" rows="5" placeholder="" name="channel_about" id="aboutchannel" ></textarea>
                <!-- <p class="test m-l-10 m-r-10 text-justify" id="about_channel"><?php echo nl2br($channel_about); ?></p> -->
                <hr>
            </div>                          
                
            </div>
            
            <div class="hidden-xs col-md-12 col-sm-12 col-xs-12" id="channel_web">
                
                <!-- <div class="card wall-posting p-15">
                    <div class="row" style="background-color: #f9f9f9;">
                        <div class="card-header">
                            <h2>Classes by <label id="namachannel"></label></h2>
                            <hr>
                        </div>
                        <div class="card-body card-padding" id="boxclass_more">  
                            <a class='"+show_classid+"'>
                                <div class='col-md-3 col-sm-6 p-t-5'>
                                    <div id='best-selling' class='dash-widget-item waves-effect z-depth-1' style='cursor: pointer;'>
                                        <div class='dash-widget-header'>
                                            <img class='' src='../aset/img/user/"+show_user_image+"' style='z-index: 1; position: absolute; margin: 3%; height: 40px; width: 40px; top: 5; right: 0;'>
                                            <img src='../aset/img/headers/BGclassroom-01.png'>
                                            <div class='dash-widget-title'>"+show_template_type+"</div>
                                            <div class='main-item'>
                                                <h2>"+show_harga+"</h2>

                                            </div>
                                        </div>
                                        <div class='listview p-15'>
                                            <img class="pull-right" id="kocakk" src='../aset/img/icons/share-variant.png' style='height: 25px; width: 25px;'/>
                                            <div class='f-14 c-gray'>"+show_user_name+"</div><br>
                                            <small class='f-18 c-black'>"+show_subject_name+" - </small><br>
                                            <small class='f-18 c-black'>"+show_description+"</small>
                                        </div>
                                        <div class='p-10' style='bottom: 0; position: absolute; left: 0;'>asdf</div>
                                    </div>
                                </div>
                            </a>  

                        </div>
                    </div>
                </div> -->
                <div class="card p-l-15 p-r-15" id="utama1">
                    <div class="row" style="background-color: white;"> 

                        <div class="card-header">
                            <h2><img src="../aset/img/baru/liveclass-04.png" style="height: 25px; width: 25px;"> <label class="c-red">Live Now</label></h2>
                            <hr>
                        </div>
                        <div class="card-body" style="margin-top: -2%;">
                            <div class="col-lg-12" id="liveboxclass">                           
                            </div>                      
                        </div>
                    </div>
                </div>
                <div class="card p-l-15 p-r-15" id="utama2">
                    <div class="row" style="background-color: white;">
                        <div class="card-header">
                            <h2><img src="../aset/img/baru/comingup-04.png" style="height: 25px; width: 25px;"> <label class="c-bluegray"> Coming Up </label></h2>
                            <hr>
                        </div>
                        <div class="card-body" style="margin-top: -2%;">
                            <div class="col-lg-12" id="xboxclass">
                            </div>                    
                        </div>
                    </div>
                </div>
                <div class="card p-l-15 p-r-15" id="utama5">
                    <div class="row" style="background-color: white;">
                        <div class="card-header">
                            <h2><img src="../aset/img/baru/missedclass-04.png" style="height: 25px; width: 25px;"> <label class="c-bluegray"> History </label></h2>
                            <hr>
                        </div>
                        <div class="card-body" style="margin-top: -2%;">
                            <div class="col-lg-12" id="xboxmissed">                        
                            </div>                    
                        </div>
                    </div>
                </div>
                <div class="card p-l-15 p-r-15 p-t-5 p-b-5" id="utama4" style="display: none;">
                    <div class="row" style="background-color: white;">                
                        <div class="card-header" style="margin-top: 0%;">                        
                            <div class="alert alert-danger text-center f-14" role="alert"><?php echo $this->lang->line('no_class');?>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="" class="col-md-12 col-sm-12 col-xs-12" id="channel_mobile">
                
                <!-- <div class="card wall-posting p-15">
                    <div class="row" style="background-color: #f9f9f9;">
                        <div class="card-header">
                            <h2>Classes by <label id="namachannel"></label></h2>
                            <hr>
                        </div>
                        <div class="card-body card-padding" id="boxclass_more">  
                            <a class='"+show_classid+"'>
                                <div class='col-md-3 col-sm-6 p-t-5'>
                                    <div id='best-selling' class='dash-widget-item waves-effect z-depth-1' style='cursor: pointer;'>
                                        <div class='dash-widget-header'>
                                            <img class='' src='../aset/img/user/"+show_user_image+"' style='z-index: 1; position: absolute; margin: 3%; height: 40px; width: 40px; top: 5; right: 0;'>
                                            <img src='../aset/img/headers/BGclassroom-01.png'>
                                            <div class='dash-widget-title'>"+show_template_type+"</div>
                                            <div class='main-item'>
                                                <h2>"+show_harga+"</h2>

                                            </div>
                                        </div>
                                        <div class='listview p-15'>
                                            <img class="pull-right" id="kocakk" src='../aset/img/icons/share-variant.png' style='height: 25px; width: 25px;'/>
                                            <div class='f-14 c-gray'>"+show_user_name+"</div><br>
                                            <small class='f-18 c-black'>"+show_subject_name+" - </small><br>
                                            <small class='f-18 c-black'>"+show_description+"</small>
                                        </div>
                                        <div class='p-10' style='bottom: 0; position: absolute; left: 0;'>asdf</div>
                                    </div>
                                </div>
                            </a>  

                        </div>
                    </div>
                </div> -->
                <div class="card p-l-15 p-r-15" id="utama1_m">
                    <div class="row" style="background-color: white;"> 

                        <div class="">
                            <h2><img src="../aset/img/baru/liveclass-04.png" style="height: 25px; width: 25px;"> <label class="c-red">Live Now</label></h2>
                            <hr>
                        </div>
                        <div class="" style="margin-top: -2%;">
                            <div class="col-lg-12" id="liveboxclass_m">                           
                            </div>                      
                        </div>
                    </div>
                </div>
                <div class="card p-l-15 p-r-15" id="utama2_m">
                    <div class="row" style="background-color: white;">
                        <div class="">
                            <h2><img src="../aset/img/baru/comingup-04.png" style="height: 25px; width: 25px;"> <label class="c-bluegray"> Coming Up </label></h2>
                            <hr>
                        </div>
                        <div class="" style="margin-top: -2%;">
                            <div class="col-lg-12" id="xboxclass_m">
                            </div>                    
                        </div>
                    </div>
                </div>
                <div class="card p-l-15 p-r-15" id="utama5_m">
                    <div class="row" style="background-color: white;">
                        <div class="">
                            <h2><img src="../aset/img/baru/missedclass-04.png" style="height: 25px; width: 25px;"> <label class="c-bluegray"> History </label></h2>
                            <hr>
                        </div>
                        <div class="" style="margin-top: -2%;">
                            <div class="col-lg-12" id="xboxmissed_m">                        
                            </div>                    
                        </div>
                    </div>
                </div>
                <div class="card p-l-15 p-r-15 p-t-5 p-b-5" id="utama4_m" style="display: none;">
                    <div class="row" style="background-color: white;">                
                        <div class="card-header" style="margin-top: 0%;">                        
                            <div class="alert alert-danger text-center f-14" role="alert"><?php echo $this->lang->line('no_class');?>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="alertnotlogin" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title c-black">Pemberitahuan</h4>
                        <hr>
                        <p><label class="c-gray f-15">Anda harus masuk terlebih dahulu dan menjadi murid di Classmiles</label></p>
                        <div class="col-md-6">
                        <button id="kelogin" class="btn bgm-blue btn-block" style="height: 50px;">Login</button>
                        </div>
                        <div class="col-md-6">
                        <button id="keregister" class="btn btn-danger btn-block" style="height: 50px;">Register</button>
                        </div>
                    </div>                                                
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                        <a id="proccessjoin" href=""><button type="button" class="btn bgm-green" id="confrimjoin">Ya</button></a>
                    </div>  -->                       
                </div>
            </div>
        </div>

        <div class="modal" style="margin-top: 5%;" id="sharelink_show" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><i class="zmdi zmdi-close"></i></button>
                        <center>
                            <h3 class="modal-title c-gray f-28">Share Classroom Public Link</h3>
                        </center>
                        <hr>
                        <div class="col-md-12 c-black m-10">
                            <label>Invite others to join your classroom by sharing below URL link. Attach in email or paste it through WhatsApp.

                            Or share your classroom directly to:</label>
                        </div>
                        <div class="col-md-12 m-t-10">
                            <div class="col-md-1">
                                <button class="btn btn-default btn-icon"  style="margin-left: -5px; cursor: default;"><i class="zmdi zmdi-link"></i></button>
                            </div>
                            <div class="col-md-9 c-black">                                
                                <p>
                                    <input type="text" class="form-control p-20" readonly name="linkclass" id="linkclass" placeholder="Your link class is here" style="background-color: rgba(96, 124, 138, 0.25); border-radius: 9px;">
                                </p>
                            </div>
                            <div class="col-md-2">
                                <button class="btn bgm-green btn-block" id="copylink" style="padding: 10px;"><!-- <i class="zmdi zmdi-copy"></i> --> Copy</button>
                            </div>
                            <!-- <div class="col-md-12">
                                <button class="btn bgm-green btn-block" id="dc" style="padding: 10px;">Decript</button>
                            </div> -->
                        </div>  

                        <div class="col-md-12">
                            <hr>
                            <div class="col-md-12 c-gray">
                                <label>Share your link class To:</label>
                            </div>
                            <div class="col-md-12 m-t-15">
                                <div class="col-md-2 col-xs-3">
                                    <a class="fb-share"><img src="../aset/img/social/facebook-128.png" target="_blank" style="height: 48px; width: 48px;"></a>
                                </div>
                                <div class="col-md-2 col-xs-3">
                                    <a class="twitter customer share" title="Twitter share" target="_blank"><img src="../aset/img/social/twitter-128.png" style="height: 48px; width: 48px;"></a>
                                </div>                                
                            </div>
                        </div>                  
                    </div> 
                    <div class="modal-footer"></div>                                                                   
                </div>
            </div>
        </div>

    </div>

</section>

<script type="text/javascript">
    var busy = false;
    var limit = 8
    var offset = 0;

    $(document).ready(function(){

        var tgl = new Date();
        var formattedDate = moment(tgl).format('YYYY-MM-DD');
        var channel_link    = "<?php echo $this->session->userdata('channel_n');?>";    
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";  
        $.ajax({
            url: 'https://classmiles.com/Rest/ChannelDetails/access_token/'+tokenjwt,
            type: 'POST',
            data: {
                channel_link: channel_link
            },
            success: function(response)
            { 
                if (response['code'] == 300) {
                    window.location.replace('https://classmiles.com/');
                }
                else
                {
                    var channel_id          = response['data']['channel_id'];
                    var channel_name        = response['data']['channel_name'];
                    var channel_description = response['data']['channel_description'];
                    var channel_about       = response['data']['channel_about'];
                    var channel_phone       = response['data']['channel_country_code']+' '+response['data']['channel_callnum'];
                    var channel_email       = response['data']['channel_email'];
                    var channel_address     = response['data']['channel_address'];
                    var channel_logo        = response['data']['channel_logo']+'?'+new Date().getTime();
                    var channel_banner      = response['data']['channel_banner']+'?'+new Date().getTime();
                    var cek_logo            = response['data']['channel_logo'];
                    var cek_banner          = response['data']['channel_banner'];


                    $("#channel_name").text(channel_name);
                    // $("#boxblue").css('background','url("../aset/img/channel/banner/'+channel_banner+'")');
                    // $("#boxblue").css('height','300px');
                    
                    
                    $("#desc_channeladdress").text(channel_address);
                    $("#titleone").text(channel_name);
                    $("#titletwo").text(channel_description);
                    $("#desc_channelphone").text('+'+channel_phone);
                    $("#desc_channelemail").text(channel_email);
                    $("#aboutchannel").text(channel_about);
                    $("#namachannel").text(channel_name);
                    
                    Historyclass(channel_id);
                    Showclass(channel_id);


                    if (cek_logo == null && cek_banner == null) {
                        $("#channel_logo").attr('src','../aset/img/channel/logo/empty.jpg');
                        $("#desc_channellogo").attr('src', '../aset/img/channel/logo/empty.jpg');
                        $("#view_channellogo").attr('src', '../aset/img/channel/logo/empty.jpg');
                        $("#boxblue").css('background','url("../aset/img/channel/banner/empty.jpg')+'?'+new Date().getTime();
                        $("#boxblue").css('background-size','100% 100%');
                        $("#boxblue").css('background-repeat','no-repeat');
                        $("#boxblue").css('position','relative'); 

                    }
                    else if (cek_logo != null && cek_banner == null) {
                        $("#channel_logo").attr('src','../aset/img/channel/logo/'+channel_logo);
                        $("#desc_channellogo").attr('src', '../aset/img/channel/logo/'+channel_logo)+'?'+new Date().getTime();
                        $("#view_channellogo").attr('src', '../aset/img/channel/logo/'+channel_logo)+'?'+new Date().getTime();
                        $("#boxblue").css('background','url("../aset/img/channel/banner/empty.jpg')+'?'+new Date().getTime();
                        $("#boxblue").css('background-size','100% 100%');
                        $("#boxblue").css('background-repeat','no-repeat');
                        $("#boxblue").css('position','relative'); 

                    }
                    else if (cek_logo == null && cek_banner != null) {
                        $("#channel_logo").attr('src','../aset/img/channel/logo/empty.jpg');
                        $("#desc_channellogo").attr('src', '../aset/img/channel/logo/empty.jpg');
                        $("#view_channellogo").attr('src', '../aset/img/channel/logo/empty.jpg');
                        $("#boxblue").css('background','url("../aset/img/channel/banner/'+channel_banner)+'?'+new Date().getTime();
                        $("#boxblue").css('background-size','100% 100%');
                        $("#boxblue").css('background-repeat','no-repeat');
                        $("#boxblue").css('position','relative'); 
                        

                    }
                     else if (cek_logo != null && cek_banner != null) {
                        $("#channel_logo").attr('src','../aset/img/channel/logo/'+channel_logo);
                        $("#desc_channellogo").attr('src', '../aset/img/channel/logo/'+channel_logo)+'?'+new Date().getTime();
                        $("#view_channellogo").attr('src', '../aset/img/channel/logo/'+channel_logo)+'?'+new Date().getTime();
                        $("#boxblue").css('background','url("../aset/img/channel/banner/'+channel_banner)+'?'+new Date().getTime();
                        $("#boxblue").css('background-size','100% 100%');
                        $("#boxblue").css('background-repeat','no-repeat');
                        $("#boxblue").css('position','relative'); 
                        
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        });
                function Showclass(channel_id)
        {   
           
            var show_linkclass = null;
            var user_utc = new Date().getTimezoneOffset();            
            user_utc = -1 * user_utc;
            var date = new Date();
            date.setDate(date.getDate() + 7);            
            // var dateMsg = date.getDate()+'-'+ (date.getMonth()+1) +'-'+date.getFullYear();            
            var dateMsg = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
            dateMsg = moment(dateMsg).format('YYYY-MM-DD');  

            // Get data for History Classes
            var dateOld = new Date();
            dateOld.setDate(dateOld.getDate() - 7);            
            // var dateMsg = date.getDate()+'-'+ (date.getMonth()+1) +'-'+date.getFullYear();            
            var dateMsgOld = dateOld.getFullYear()+'-'+(dateOld.getMonth()+1)+'-'+dateOld.getDate();
            dateMsgOld = moment(dateMsgOld).format('YYYY-MM-DD');  


            // alert(formattedDate);
            // alert(dateMsgOld);
            $.ajax({
                url: 'https://classmiles.com/Rest/getChannelClasses?channel_id='+channel_id+'&start_date='+formattedDate+'&end_date='+dateMsg+'&user_utc='+user_utc+'&user_device=web/access_token/'+tokenjwt,
                type: 'GET',
                success: function(data)
                {            
                    console.log("ini data"+ JSON.stringify(data));
                    if (data['code'] == 200) {
                        for (var i = data.response.length-1; i >=0;i--) {
                            var key = i;                                                    
                            var show_classid            = data['response'][i]['class_id'];
                            // get_encrypt(show_classid); 
                            var show_subject_id     = data['response'][i]['subject_id'];
                            var show_tutor_id       = data['response'][i]['tutor_id'];
                            var show_user_name      = data['response'][i]['user_name'];
                            var show_subject_name   = data['response'][i]['subject_name'];
                            var show_template_type  = data['response'][i]['template_type'];
                            var show_class_type     = data['response'][i]['class_type'];
                            var show_description    = data['response'][i]['description'];
                            var show_harga          = data['response'][i]['harga'];
                            var show_user_image     = data['response'][i]['user_image'];
                            var show_time           = data['response'][i]['time'];
                            var show_endtime        = data['response'][i]['endtime'];
                            var show_date           = moment(data['response'][i]['date']).format('DD-MM-YYYY');
                            var show_enddate        = data['response'][i]['enddate'];

                            var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes();  
                            var timeclass    = data['response'][i]['date']+" "+data['response'][i]['time'];
                            var endtimeclass = data['response'][i]['enddate']+" "+data['response'][i]['endtime'];

                            var t1 = null;

                            if (show_template_type == "whiteboard_digital") {
                                show_template_type = "Digital Board";
                            }
                            else if (show_template_type == "whiteboard_videoboard") {
                                show_template_type = "Video Board";
                            }
                            else
                            {
                                show_template_type = "No Whiteboard";
                            }

                            if (show_harga == null) {
                                show_harga = "Free class";
                            }
                            // console.log(response);
                            if ( data['response'] == null) 
                            {                                                
                                $("#utama1").css('display','none');
                                $("#utama2").css('display','none');
                                $("#utama3").css('display','none');
                                $("#utama5").css('display','none');
                                $("#utama4").css('display','block');
                            }                   
                            else
                            {
                                $("#utama4").css('display','none');
                                $("#utama1").css('display','block');
                                $("#utama3").css('display','block');
                                $("#utama5").css('display','block');
                                $("#utama2").css('display','block');
                            }
                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                            {                                      
                                 
                                 var boxclass = "<a class='"+show_classid+"'><div class='col-md-3 col-sm-3 p-t-5'><div id='best-selling' class='dash-widget-item waves-effect z-depth-1' style='cursor: pointer;'><div class='dash-widget-header'><img style='filter: grayscale(0%); width:100%; height:200px;'  src='../aset/img/user/"+show_user_image+"'><div class='dash-widget-title'></div><div class='main-item'><h2>"+show_harga+"</h2></div></div><div class='listview p-15'></a><img class='pull-right' src='../aset/img/icons/share-variant.png' id='kocak' class='kocak' data-classid='"+show_classid+"' title='Share Class' style='cursor:pointer; z-index: 99; height: 25px; width: 25px;'/><a class='"+show_classid+"'><div class='f-14 c-gray'>"+show_user_name+"</div><br><small class='f-18 c-black'>"+show_subject_name+" - </small><br><small class='f-18 c-black'>"+show_description+"</small></div></div></div></a>";
                                $("#xboxclass").append(boxclass);
                            }
                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                 var boxclass = "<a class='"+show_classid+"'><div class='col-md-3 col-sm-3 p-t-5'><div id='best-selling' class='dash-widget-item waves-effect z-depth-1' style='cursor: pointer;'><div class='dash-widget-header'><img  style='filter: grayscale(0%); width:100%; height:200px;' src='../aset/img/user/"+show_user_image+"'><div class='dash-widget-title'></div><div class='main-item'><h2>"+show_harga+"</h2></div></div><div class='listview p-15'></a><img class='pull-right' src='../aset/img/icons/share-variant.png' id='kocak' class='kocak' data-classid='"+show_classid+"' title='Share Class' style='cursor:pointer; z-index: 99; height: 25px; width: 25px;'/><a class='"+show_classid+"'><div class='f-14 c-gray'>"+show_user_name+"</div><br><small class='f-18 c-black'>"+show_subject_name+" - </small><br><small class='f-18 c-black'>"+show_description+"</small></div></div></div></a>";
                                $("#liveboxclass").append(boxclass);
                            }                                    
                            // else 
                            // {
                            //      var boxclass = "<a class='"+show_classid+"'><div class='col-md-3 col-sm-6 p-t-5'><div id='best-selling' class='dash-widget-item waves-effect z-depth-1' style='cursor: pointer;'><div class='dash-widget-header'><img src='../aset/img/user/"+show_user_image+"' style='filter: grayscale(100%); width:100%; height:200px;' ><div class='dash-widget-title'>"+show_template_type+"</div><div class='main-item'><h2>"+show_harga+"</h2></div></div><div class='listview p-15'></a><a class='"+show_classid+"'><div class='f-14 c-gray'>"+show_user_name+"</div><br><small class='f-18 c-black'>"+show_subject_name+" - </small><br><small class='f-18 c-black'>"+show_description+"</small></div></div></div></a>";
                            //     $("#xboxmissed").append(boxclass);                                        
                            // }                                
                            // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
                            $("#boxclass_more").append(boxclass);

                            get_encrypt(show_classid);

                        }
                    }
                    else{
                        var boxclass = '<center><div class="alert alert-danger f-14" role="alert">Tidak ada kelas lain.</div></center>';                                
                            // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
                            $("#boxclass_more").append(boxclass);
                    }
                }
            });

            // $.ajax({
            //     cache: false,
            //     url: 'https://classmiles.com/Rest/getChannelClasses?channel_id='+channel_id+'&end_date='+formattedDate+'&start_date='+dateMsgOld+'&user_utc='+user_utc+'&user_device=web/access_token/'+tokenjwt,
            //     type: 'GET',
            //     success: function(data)
            //     {            
            //     console.log("ini data"+ JSON.stringify(data));
            //     if (data['code'] == 200) {

                    
            //         for (var i = 0; i< data.response.length;i++) {
                        
            //             // var x = i - 8;
            //             // alert (i);    
            //             // if (i === 8) { break; }
            //             var key = i;                                                    
            //             var show_classid            = data['response'][i]['class_id'];
            //             // get_encrypt(show_classid); 
            //             var show_subject_id     = data['response'][i]['subject_id'];
            //             var show_tutor_id       = data['response'][i]['tutor_id'];
            //             var show_user_name      = data['response'][i]['user_name'];
            //             var show_subject_name   = data['response'][i]['subject_name'];
            //             var show_template_type  = data['response'][i]['template_type'];
            //             var show_class_type     = data['response'][i]['class_type'];
            //             var show_description    = data['response'][i]['description'];
            //             var show_harga          = data['response'][i]['harga'];
            //             var show_user_image     = data['response'][i]['user_image'];
            //             var show_time           = data['response'][i]['time'];
            //             var show_endtime        = data['response'][i]['endtime'];
            //             var show_date           = moment(data['response'][i]['date']).format('DD-MM-YYYY');
            //             var show_enddate        = data['response'][i]['enddate'];

            //             var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes();  
            //             var timeclass    = data['response'][i]['date']+" "+data['response'][i]['time'];
            //             var endtimeclass = data['response'][i]['enddate']+" "+data['response'][i]['endtime'];

            //             var t1 = null;

            //             if (show_template_type == "whiteboard_digital") {
            //                 show_template_type = "Digital Board";
            //             }
            //             else if (show_template_type == "whiteboard_videoboard") {
            //                 show_template_type = "Video Board";
            //             }
            //             else
            //             {
            //                 show_template_type = "No Whiteboard";
            //             }

            //             if (show_harga == null) {
            //                 show_harga = "Free class";
            //             }
            //             // console.log(response);
            //             if ( data['response'] == null) 
            //             {                                                
            //                 $("#utama1").css('display','none');
            //                 $("#utama2").css('display','none');
            //                 $("#utama3").css('display','none');
            //                 $("#utama5").css('display','none');
            //                 $("#utama4").css('display','block');
            //             }                   
            //             else
            //             {
            //                 $("#utama4").css('display','none');
            //                 $("#utama1").css('display','block');
            //                 $("#utama3").css('display','block');
            //                 $("#utama5").css('display','block');
            //                 $("#utama2").css('display','block');
            //             }   
            //                 var boxclass = "<a class=''><div class='col-md-3 col-sm-3 p-t-5'><div id='best-selling' class='dash-widget-item waves-effect z-depth-1' style=''><div class='dash-widget-header'><img src='../aset/img/user/"+show_user_image+"' style='filter: grayscale(100%); width:100%; height:200px;' ><div class='dash-widget-title'>"+show_template_type+"</div><div class='main-item'><h2>"+show_harga+"</h2></div></div><div class='listview p-15'></a><a class=''><div class='f-14 c-gray'>"+show_user_name+"</div><br><small class='f-18 c-black'>"+show_subject_name+" - </small><br><small class='f-18 c-black'>"+show_description+"</small></div></div></div></a>";
                              
            //                 $("#xboxmissed").append(boxclass);                                        
                              
            //             // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
            //             // $("#boxclass_more").append(boxclass);

            //             get_encrypt(show_classid);

            //             }
            //         }
            //         else{
            //             var boxclass = '<center><div class="alert alert-danger f-14" role="alert">Tidak ada kelas lain.</div></center>';                                
            //                 // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
            //                 $("#boxclass_more").append(boxclass);
            //         }
            //     }
            // });

            $.ajax({
                url: 'https://classmiles.com/Rest/getChannelClasses?channel_id='+channel_id+'&start_date='+formattedDate+'&end_date='+dateMsg+'&user_utc='+user_utc+'&user_device=web/access_token/'+tokenjwt,
                type: 'GET',
                success: function(data)
                {            
                    console.log("ini data"+ JSON.stringify(data));
                    if (data['code'] == 200) {
                        for (var i = data.response.length-1; i >=0;i--) {
                            var key = i;                                                    
                            var show_classid            = data['response'][i]['class_id'];
                            // get_encrypt(show_classid); 
                            var show_subject_id     = data['response'][i]['subject_id'];
                            var show_tutor_id       = data['response'][i]['tutor_id'];
                            var show_user_name      = data['response'][i]['user_name'];
                            var show_subject_name   = data['response'][i]['subject_name'];
                            var show_template_type  = data['response'][i]['template_type'];
                            var show_class_type     = data['response'][i]['class_type'];
                            var show_description    = data['response'][i]['description'];
                            var show_harga          = data['response'][i]['harga'];
                            var show_user_image     = data['response'][i]['user_image'];
                            var show_time           = data['response'][i]['time'];
                            var show_endtime        = data['response'][i]['endtime'];
                            var show_date           = moment(data['response'][i]['date']).format('DD-MM-YYYY');
                            var show_enddate        = data['response'][i]['enddate'];

                            var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes();  
                            var timeclass    = data['response'][i]['date']+" "+data['response'][i]['time'];
                            var endtimeclass = data['response'][i]['enddate']+" "+data['response'][i]['endtime'];

                            var t1 = null;

                            if (show_template_type == "whiteboard_digital") {
                                show_template_type = "Digital Board";
                            }
                            else if (show_template_type == "whiteboard_videoboard") {
                                show_template_type = "Video Board";
                            }
                            else
                            {
                                show_template_type = "No Whiteboard";
                            }

                            if (show_harga == null) {
                                show_harga = "Free class";
                            }
                            // console.log(response);
                            if ( data['response'] == null) 
                            {                                                
                                $("#utama1_m").css('display','none');
                                $("#utama2_m").css('display','none');
                                $("#utama3_m").css('display','none');
                                $("#utama5_m").css('display','none');
                                $("#utama4_m").css('display','block');
                            }                   
                            else
                            {
                                $("#utama4_m").css('display','none');
                                $("#utama1_m").css('display','block');
                                $("#utama3_m").css('display','block');
                                $("#utama5_m").css('display','block');
                                $("#utama2_m").css('display','block');
                            }
                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                            {                                      
                                 
                                 var boxclass = "<a class='"+show_classid+"'><div class='col-xs-12 ' ><div id='best-selling'  class='' style=''><div class='col-xs-5'><img class='img-rounded' z-index: -1 src='../aset/img/user/"+show_user_image+"' style='filter: grayscale(00%); width:100%;' ><div class='main-item'><h5 class='' style='text-align: center;'>"+show_harga+"</h5></div></div><div class='listview' style='height:130px;'></a><a class=''><div class='f-12 c-gray'>"+show_user_name+"</div><br><small class='f-15 c-black'>"+show_subject_name+" - </small><br><small class='f-14 c-black'>"+show_description+"</small></div></div></div></a>";
                                $("#xboxclass_m").append(boxclass);
                            }
                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                 var boxclass = "<a class='"+show_classid+"'><div class='col-xs-12 ' ><div id='best-selling'  class='' style=''><div class='col-xs-5'><img class='img-rounded' z-index: -1 src='../aset/img/user/"+show_user_image+"' style='filter: grayscale(00%); width:100%;' ><div class='main-item'><h5 class='' style='text-align: center;'>"+show_harga+"</h5></div></div><div class='listview' style='height:130px;'></a><a class=''><div class='f-12 c-gray'>"+show_user_name+"</div><br><small class='f-15 c-black'>"+show_subject_name+" - </small><br><small class='f-14 c-black'>"+show_description+"</small></div></div></div></a>";
                                $("#liveboxclass_m").append(boxclass);
                            }                                    
                            // else 
                            // {
                            //      var boxclass = "<a class='"+show_classid+"'><div class='col-md-3 col-sm-6 p-t-5'><div id='best-selling' class='dash-widget-item waves-effect z-depth-1' style='cursor: pointer;'><div class='dash-widget-header'><img src='../aset/img/user/"+show_user_image+"' style='filter: grayscale(100%); width:100%; height:200px;' ><div class='dash-widget-title'>"+show_template_type+"</div><div class='main-item'><h2>"+show_harga+"</h2></div></div><div class='listview p-15'></a><a class='"+show_classid+"'><div class='f-14 c-gray'>"+show_user_name+"</div><br><small class='f-18 c-black'>"+show_subject_name+" - </small><br><small class='f-18 c-black'>"+show_description+"</small></div></div></div></a>";
                            //     $("#xboxmissed").append(boxclass);                                        
                            // }                                
                            // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
                            $("#boxclass_more_m").append(boxclass);

                            get_encrypt(show_classid);

                        }
                    }
                    else{
                        var boxclass = '<center><div class="alert alert-danger f-14" role="alert">Tidak ada kelas lain.</div></center>';                                
                            // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
                            $("#boxclass_more").append(boxclass);
                    }
                }
            });

            // $.ajax({
            //     url: 'https://classmiles.com/Rest/getChannelClasses?channel_id='+channel_id+'&end_date='+formattedDate+'&start_date='+dateMsgOld+'&user_utc='+user_utc+'&user_device=web/access_token/'+tokenjwt,
            //     type: 'GET',
            //     success: function(data)
            //     {            
            //     console.log("ini data"+ JSON.stringify(data));
            //     if (data['code'] == 200) {

                    
            //         for (var i = 0; i< data.response.length;i++) {
                        
            //             // var x = i - 8;
            //             // alert (i);    
                        
            //             var key = i;                                                    
            //             var show_classid            = data['response'][i]['class_id'];
            //             // get_encrypt(show_classid); 
            //             var show_subject_id     = data['response'][i]['subject_id'];
            //             var show_tutor_id       = data['response'][i]['tutor_id'];
            //             var show_user_name      = data['response'][i]['user_name'];
            //             var show_subject_name   = data['response'][i]['subject_name'];
            //             var show_template_type  = data['response'][i]['template_type'];
            //             var show_class_type     = data['response'][i]['class_type'];
            //             var show_description    = data['response'][i]['description'];
            //             var show_harga          = data['response'][i]['harga'];
            //             var show_user_image     = data['response'][i]['user_image'];
            //             var show_time           = data['response'][i]['time'];
            //             var show_endtime        = data['response'][i]['endtime'];
            //             var show_date           = moment(data['response'][i]['date']).format('DD-MM-YYYY');
            //             var show_enddate        = data['response'][i]['enddate'];

            //             var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes();  
            //             var timeclass    = data['response'][i]['date']+" "+data['response'][i]['time'];
            //             var endtimeclass = data['response'][i]['enddate']+" "+data['response'][i]['endtime'];

            //             var t1 = null;

            //             if (show_template_type == "whiteboard_digital") {
            //                 show_template_type = "Digital Board";
            //             }
            //             else if (show_template_type == "whiteboard_videoboard") {
            //                 show_template_type = "Video Board";
            //             }
            //             else
            //             {
            //                 show_template_type = "No Whiteboard";
            //             }

            //             if (show_harga == null) {
            //                 show_harga = "Free class";
            //             }
            //             // console.log(response);
            //             if ( data['response'] == null) 
            //             {                                                
            //                 $("#utama1_m").css('display','none');
            //                 $("#utama2_m").css('display','none');
            //                 $("#utama3_m").css('display','none');
            //                 $("#utama5_m").css('display','none');
            //                 $("#utama4_m").css('display','block');
            //             }                   
            //             else
            //             {
            //                 $("#utama4_m").css('display','none');
            //                 $("#utama1_m").css('display','block');
            //                 $("#utama3_m").css('display','block');
            //                 $("#utama5_m").css('display','block');
            //                 $("#utama2_m").css('display','block');
            //             }   
            //                 var boxclass = "<a class=''><div class='col-xs-6 '><div id='best-selling' class='dash-widget-item' style=''><div class='dash-widget-header'><img src='../aset/img/user/"+show_user_image+"' style='filter: grayscale(100%); width:100%;' ><div class='dash-widget-title'></div><div class='main-item'><h5 class='c-white'>"+show_harga+"</h5></div></div><div class='listview p-15'></a><a class=''><div class='f-12 c-gray'>"+show_user_name+"</div><br><small class='f-15 c-black'>"+show_subject_name+" - </small><br><small class='f-14 c-black'>"+show_description+"</small></div></div></div></a>";
                              
            //                 $("#xboxmissed_m").append(boxclass);                                        
                              
            //             // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
            //             // $("#boxclass_more").append(boxclass);

            //             get_encrypt(show_classid);

            //             }
            //         }
            //         else{
            //             var boxclass = '<center><div class="alert alert-danger f-14" role="alert">Tidak ada kelas lain.</div></center>';                                
            //                 // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
            //                 $("#boxclass_more").append(boxclass);
            //         }
            //     }
            // });
        }

        function Historyclass(channel_id, lim, off)
        {   
           
            var show_linkclass = null;
            var user_utc = new Date().getTimezoneOffset();            
            user_utc = -1 * user_utc;
            var date = new Date();

            date.setDate(date.getDate() + 7);            
            // var dateMsg = date.getDate()+'-'+ (date.getMonth()+1) +'-'+date.getFullYear();            
            var dateMsg = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
            dateMsg = moment(dateMsg).format('YYYY-MM-DD');  

            // Get data for History Classes
            var dateOld = new Date();
            dateOld.setDate(dateOld.getDate() - 7);            
            // var dateMsg = date.getDate()+'-'+ (date.getMonth()+1) +'-'+date.getFullYear();            
            var dateMsgOld = dateOld.getFullYear()+'-'+(dateOld.getMonth()+1)+'-'+dateOld.getDate();
            dateMsgOld = moment(dateMsgOld).format('YYYY-MM-DD');  


            // alert(formattedDate);
            // alert(dateMsgOld);

            $.ajax({
                cache: false,
                url: 'https://classmiles.com/Rest/getChannelClasses?channel_id='+channel_id+'&end_date='+formattedDate+'&start_date='+dateMsgOld+'&user_utc='+user_utc+'&user_device=web/access_token/'+tokenjwt,
                type: 'GET',
                data: "limit=" + lim + "&offset=" + off,
                success: function(data)
                {            
                console.log("ini data"+ JSON.stringify(data));
                if (data['code'] == 200) {

                    
                    for (var i = 0; i< data.response.length;i++) {
                        
                        // var x = i - 8;
                        // alert (i);    
                        if (i === 8) { break; }
                        var key = i;                                                    
                        var show_classid            = data['response'][i]['class_id'];
                        // get_encrypt(show_classid); 
                        var show_subject_id     = data['response'][i]['subject_id'];
                        var show_tutor_id       = data['response'][i]['tutor_id'];
                        var show_user_name      = data['response'][i]['user_name'];
                        var show_subject_name   = data['response'][i]['subject_name'];
                        var show_template_type  = data['response'][i]['template_type'];
                        var show_class_type     = data['response'][i]['class_type'];
                        var show_description    = data['response'][i]['description'];
                        var show_harga          = data['response'][i]['harga'];
                        var show_user_image     = data['response'][i]['user_image'];
                        var show_time           = data['response'][i]['time'];
                        var show_endtime        = data['response'][i]['endtime'];
                        var show_date           = moment(data['response'][i]['date']).format('DD-MM-YYYY');
                        var show_enddate        = data['response'][i]['enddate'];

                        var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes();  
                        var timeclass    = data['response'][i]['date']+" "+data['response'][i]['time'];
                        var endtimeclass = data['response'][i]['enddate']+" "+data['response'][i]['endtime'];

                        var t1 = null;

                        if (show_template_type == "whiteboard_digital") {
                            show_template_type = "Digital Board";
                        }
                        else if (show_template_type == "whiteboard_videoboard") {
                            show_template_type = "Video Board";
                        }
                        else
                        {
                            show_template_type = "No Whiteboard";
                        }

                        if (show_harga == null) {
                            show_harga = "Free class";
                        }
                        // console.log(response);
                        if ( data['response'] == null) 
                        {                                                
                            $("#utama1").css('display','none');
                            $("#utama2").css('display','none');
                            $("#utama3").css('display','none');
                            $("#utama5").css('display','none');
                            $("#utama4").css('display','block');
                        }                   
                        else
                        {
                            $("#utama4").css('display','none');
                            $("#utama1").css('display','block');
                            $("#utama3").css('display','block');
                            $("#utama5").css('display','block');
                            $("#utama2").css('display','block');
                        }   
                            var boxclass = "<a class=''><div class='col-md-3 col-sm-3 p-t-5'><div id='best-selling' class='dash-widget-item waves-effect z-depth-1' style=''><div class='dash-widget-header'><img src='../aset/img/user/"+show_user_image+"' style='filter: grayscale(100%); width:100%; height:200px;' ><div class='dash-widget-title'></div><div class='main-item'><h2>"+show_harga+"</h2></div></div><div class='listview p-15'></a><a class=''><div class='f-14 c-gray'>"+show_user_name+"</div><br><small class='f-18 c-black'>"+show_subject_name+" - </small><br><small class='f-18 c-black'>"+show_description+"</small></div></div></div></a>";
                              
                            $("#xboxmissed").append(boxclass);                                        
                              
                        // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
                        // $("#boxclass_more").append(boxclass);

                        get_encrypt(show_classid);

                        }
                    }
                    else{
                        var boxclass = '<center><div class="alert alert-danger f-14" role="alert">Tidak ada kelas lain.</div></center>';                                
                            // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
                            $("#boxclass_more").append(boxclass);
                    }
                }
            });

            $.ajax({
                url: 'https://classmiles.com/Rest/getChannelClasses?channel_id='+channel_id+'&end_date='+formattedDate+'&start_date='+dateMsgOld+'&user_utc='+user_utc+'&user_device=web/access_token/'+tokenjwt,
                type: 'GET',
                success: function(data)
                {            
                console.log("ini data"+ JSON.stringify(data));
                if (data['code'] == 200) {

                    
                    for (var i = 0; i< data.response.length;i++) {
                        
                        // var x = i - 8;
                        // alert (i);    
                        
                        var key = i;                                                    
                        var show_classid            = data['response'][i]['class_id'];
                        // get_encrypt(show_classid); 
                        var show_subject_id     = data['response'][i]['subject_id'];
                        var show_tutor_id       = data['response'][i]['tutor_id'];
                        var show_user_name      = data['response'][i]['user_name'];
                        var show_subject_name   = data['response'][i]['subject_name'];
                        var show_template_type  = data['response'][i]['template_type'];
                        var show_class_type     = data['response'][i]['class_type'];
                        var show_description    = data['response'][i]['description'];
                        var show_harga          = data['response'][i]['harga'];
                        var show_user_image     = data['response'][i]['user_image'];
                        var show_time           = data['response'][i]['time'];
                        var show_endtime        = data['response'][i]['endtime'];
                        var show_date           = moment(data['response'][i]['date']).format('DD-MM-YYYY');
                        var show_enddate        = data['response'][i]['enddate'];

                        var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes();  
                        var timeclass    = data['response'][i]['date']+" "+data['response'][i]['time'];
                        var endtimeclass = data['response'][i]['enddate']+" "+data['response'][i]['endtime'];

                        var t1 = null;

                        if (show_template_type == "whiteboard_digital") {
                            show_template_type = "Digital Board";
                        }
                        else if (show_template_type == "whiteboard_videoboard") {
                            show_template_type = "Video Board";
                        }
                        else
                        {
                            show_template_type = "No Whiteboard";
                        }

                        if (show_harga == null) {
                            show_harga = "Free class";
                        }
                        // console.log(response);
                        if ( data['response'] == null) 
                        {                                                
                            $("#utama1_m").css('display','none');
                            $("#utama2_m").css('display','none');
                            $("#utama3_m").css('display','none');
                            $("#utama5_m").css('display','none');
                            $("#utama4_m").css('display','block');
                        }                   
                        else
                        {
                            $("#utama4_m").css('display','none');
                            $("#utama1_m").css('display','block');
                            $("#utama3_m").css('display','block');
                            $("#utama5_m").css('display','block');
                            $("#utama2_m").css('display','block');
                        }   
                            var boxclass = "<a class=''><div class='col-xs-12 ' ><div id='best-selling'  class='' style=''><div class='col-xs-5'><img class='img-rounded' z-index: -1 src='../aset/img/user/"+show_user_image+"' style='filter: grayscale(100%); width:100%;' ><div class='main-item'><h5 class='' style='text-align: center;'>"+show_harga+"</h5></div></div><div class='listview' style='height:130px;'></a><a class=''><div class='f-12 c-gray'>"+show_user_name+"</div><br><small class='f-15 c-black'>"+show_subject_name+" - </small><br><small class='f-14 c-black'>"+show_description+"</small></div></div></div></a>";
                              
                            $("#xboxmissed_m").append(boxclass);                                        
                              
                        // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
                        // $("#boxclass_more").append(boxclass);

                        get_encrypt(show_classid);

                        }
                    }
                    else{
                        var boxclass = '<center><div class="alert alert-danger f-14" role="alert">Tidak ada kelas lain.</div></center>';                                
                            // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
                            $("#boxclass_more").append(boxclass);
                    }
                }
            });
        }

        $(document).ready(function() {
        // start to load the first set of data
        if (busy == false) {
          busy = true;
          // start to load the first set of data
          Historyclass(limit, offset);
        }


        $(window).scroll(function() {
          // make sure u give the container id of the data to be loaded in.
          if ($(window).scrollTop() + $(window).height() > $("#xboxmissed").height() && !busy) {
            busy = true;
            offset = limit + offset;

            // this is optional just to delay the loading of data
            setTimeout(function() { displayRecords(limit, offset); }, 500);

            // you can remove the above code and can use directly this function
            // displayRecords(limit, offset);

          }
        });

      });
        function get_encrypt(show_classid)
        {            
            $.ajax({
                url: '<?php echo base_url(); ?>Tools/get_c/' + show_classid,
                type: 'GET',
                /*data: {
                    class_id: classid
                },*/
                success: function(data)
                {    
                    
                    data = data.trim();                                          
                    c = data;   
                    
                    link = "<?php echo base_url();?>class?c="+data;                                        
                    $("."+show_classid).attr('href',link);                                        
                }
            });
        }

        $(document).on("click","#kocak",function() {
            var classid  = $(this).data('classid');
            
            $.ajax({
                url: '<?php echo base_url(); ?>Tools/get_c/' + classid,
                type: 'GET',
                /*data: {
                    class_id: classid
                },*/
                success: function(data)
                {              
                    data = data.trim();      
                    c = data;
                    link = "<?php echo base_url();?>class?c="+data;
                    $("#linkclass").val(link);                    
                    $(".fb-share").attr('href','https://www.facebook.com/sharer/sharer.php?u='+link);
                    $(".twitter").attr('href', 'https://twitter.com/share?url='+link+'&text=Ini adalah percobaan share ke twitter!');
                    $("#sharelink_show").modal("show");
                }
            });
        });

        $(document).on('click', "#copylink", function(){
            copyToClipboard(document.getElementById("linkclass"));
            notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut','Success copy link');
        });

        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);
            
            // copy the selection
            var succeed;
            try {
                  succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }
            
            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }

        $('.fb-share').click(function(e) {
            e.preventDefault();
            window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
            return false;
        });

        $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
            // Prevent default anchor event
            e.preventDefault();
            
            // Set values for window
            intWidth = intWidth || '500';
            intHeight = intHeight || '400';
            strResize = (blnResize ? 'yes' : 'no');

            // Set title and open popup with focus on it
            var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,            
                objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
        }
        
        $(document).ready(function ($) {
            $('.customer.share').on("click", function(e) {
                $(this).customerPopup(e);
            });
        });

    });
     
</script>
<!-- <script>  
 $(document).ready(function(){  
      load_data();  
      function load_data(page)  
      {  
           $.ajax({  
                url: '<?php echo base_url(); ?>ajaxer/getMyListChannel',  
                method:"POST",  
                data:{page:page},  
                success:function(data){  
                     $('#xboxmissed').html(data);  
                }  
           })  
      }  
      $(document).on('click', '.pagination_link', function(){  
           var page = $(this).attr("id");  
           load_data(page);  
      });  
 });  
 </script>  -->