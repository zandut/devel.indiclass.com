<div class="site_preloader flex_center">
    <div class="site_preloader_inner">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
    </div>
</div>

<div class="styler">
    <div class="single_styler clearfix sticky_header">
        <h4>Header style</h4>
        <select class="wide">
            <option value="static">Enable header top</option>
            <option value="diseable_ht">Diseable header top</option>
        </select>
    </div>
    <div class="single_styler primary_color clearfix">
        <h4>Primary Color</h4>
        <div class="clearfix">
            <input id="primary_clr" type="text" class="form-control color_input" value="#f9bf3b" />
            <label class="form-control" for="primary_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler secound_color clearfix">
        <h4>Secoundary Color</h4>
        <div class="clearfix">
            <input id="secound_clr" type="text" class="form-control color_input" value="#19b5fe" />
            <label class="form-control" for="secound_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler layout_mode clearfix">
        <h4>Layout Mode</h4>
        <select class="wide">
            <option value="wide">Wide</option>
            <option value="boxed">Boxed</option>
            <option value="wide_box">Wide Boxed</option>
        </select>
    </div>
    <div class="layout_mode_dep">
        <div class="single_styler body_bg_style clearfix">
            <h4>Body Background style</h4>
            <select class="wide">
                <option value="img_bg">Image Background</option>
                <option value="solid">Solid Color</option>
            </select>
        </div>
        <div class="single_styler body_solid_bg clearfix">
            <h4>Select Background Color</h4>
            <div class="clearfix">
                <input id="body_bg_clr" type="text" class="form-control color_input" value="#d3d3d3" />
                <label class="form-control" for="body_bg_clr"><i class="fa fa-angle-down"></i></label>
            </div>
        </div>
      
    </div>
</div>
   
<div class="main_wrap">
    <?php $this->load->view('inc/navbar');?>
    <div class="breadcrumb_area">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Pengajar</h1>
                    <!-- <ul class="brc">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li><a href="http://rltclass.com.sg">Home</a></li>
                        <li><a href="#">Tutor</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
    <!-- 04. cta_area -->
    <div class="cta_area wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="cta">
                        <h2>Mulai karir anda dengan Fisipro</h2>
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    <a href="<?php echo BASE_URL();?>DaftarPengajar" class="button hvr-bounce-to-right pbg"><i class="fa fa-long-arrow-right"></i> Daftar Sekarang</a>
                </div>
            </div>
        </div>
    </div>
    <!-- 04. /cta_area -->
     <div class="work_area sp">
        <div class="container">
            <div class="row masonry-wrap" id="kotak_tutor">
            </div>
        </div>
    </div>

</div>
   

<script type="text/javascript">
	$.ajax({
            url: '<?php echo AIR_API;?>tutorList',
            type: 'POST',
            data: {      
            	channel_id : 52,         
                device: 'web'
            },            
            success: function(data)
            {         
            	var a = JSON.stringify(data);
            	// alert(a);
            	            
                if (data['status'] == true) 
                {                       
                    for (var i = 0; i < data.data.length; i++) {
                        var user_image        	= data['data'][i]['user_image'];
                        var user_name        	= data['data'][i]['user_name'];
                        var self_description        	= data['data'][i]['self_description'];
                        var tutor_image 		="https://cdn.classmiles.com/usercontent/"+user_image;
                        var list_tutor			="<div class='col-md-3 col-sm-6 single_work masonry-grid wow fadeInUp'>"+
    					    "<div>"+
    					        "<a href='#' class='work_img'>"+
    					            "<img style='height:265px; width: auto;' src='"+tutor_image+"' alt='>"+
    					        "</a>"+
    					        "<div class='work_content'>"+
    					            "<a href='#' class='h4'>"+user_name+"</a>"+
    					            // "<span>"+self_description+"</span>"+
    					        "</div>"+
    					    "</div>"+
    					"</div>";
						$("#kotak_tutor").append(list_tutor);
                        
                    }
                }
         
            }
        });
</script>