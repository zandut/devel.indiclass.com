<div class="d-flex flex-column h-100">
    
    <!-- Navbar -->
        <?php $this->load->view('inc/navbar_tutor');?>
    <!-- // END Navbar -->

    <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
        <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
            <div class="container">

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="instructor-dashboard.html">Home</a></li>
                    <li class="breadcrumb-item"><a href="instructor-courses.html">Courses</a></li>
                    <li class="breadcrumb-item active">Search</li>
                </ol>
                <div class="media">
                    <div class="media-body">
                        <div class="row">
                            <h1 class="page-heading h2">
                                Detail Payment
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="card">
                        <div class="page-heading text-center" style="margin-top: 3%;">
                                    <h2>History Payments</h2>
                                    <p class="lead">List history payment class private or group.</p>
                                    <hr>
                                </div>
                        <div class="card-body card-padding">
                            <div id="tables_reportPayment"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php $this->load->view('inc/sidebar_tutor');?>

        <div class="modal  fade" id="modal_alert" style="margin-top: 10%;"  data-backdrop="static" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>The Rote Less Travelled</b></h3>
                    </div>
                    <div class="modal-body">
                        <label>You successfully joined the class</label>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        // $(".select2").select2();
        var tgl = new Date(); 
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc; 
        var access_token = "<?php echo $this->session->userdata('access_token');?>";
        var iduser = "<?php echo $this->session->userdata('id_user');?>";
        var dataSet = [];

        $.ajax({
            url: 'https://rltclass.com.sg/Rest/paypalReport/access_token/'+access_token,
            type: 'POST',
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                
                if (code == 200) {                     
                    for (var i = 0; i < response.data.length; i++) {
                        var payment_id  = response['data'][i]['payment_id'];
                        var user_id = response['data'][i]['user_id'];
                        var product_id  = response['data'][i]['product_id'];
                        var txn_id  = response['data'][i]['txn_id'];
                        var payment_gross   = response['data'][i]['payment_gross'];
                        var currency_code   = response['data'][i]['currency_code'];
                        var payer_email = response['data'][i]['payer_email'];
                        var payment_status  = response['data'][i]['payment_status'];
                        var id_package  = response['data'][i]['id_package'];
                        var name_package    = response['data'][i]['name_package'];
                        var credit_package  = response['data'][i]['credit_package'];
                        var price_package   = response['data'][i]['price_package'];
                        

                        dataSet.push([i+1, txn_id, name_package, credit_package, payment_gross, currency_code, payer_email, payment_status]);
                    }                        

                    $('#tables_reportPayment').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="reportList"></table>' );
             
                    $('#reportList').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Transaction ID"},
                            { "title": "Package Name"},
                            { "title": "Package Credit"},
                            { "title": "Payment Gross"},
                            { "title": "Currency Code"},
                            { "title": "Payer Email"},
                            { "title": "Payment Status"}
                                                        
                        ]
                    });
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
                else
                {
                    $('#tables_reportPayment').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="reportList"></table>' );
             
                    $('#reportList').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Transaction ID"},
                            { "title": "Package Name"},
                            { "title": "Package Credit"},
                            { "title": "Payment Gross"},
                            { "title": "Currency Code"},
                            { "title": "Payer Email"},
                            { "title": "Payment Status"}
                             
                        ]
                    });
                }
            }
        });
            
    });
</script>
