<div class="d-flex flex-column h-100">
    
    <!-- Navbar -->
        <?php $this->load->view('inc/navbar_tutor');?>
    <!-- // END Navbar -->

    <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
        <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
            <div class="container">

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="instructor-dashboard.html">Home</a></li>
                    <li class="breadcrumb-item active">Package</li>
                </ol>

                <div class="page-heading text-center" style="margin-top: 5%;">
                    <h2>"Select the Package"</h2>
                    <p class="lead">Choose your package to buy class private or group.</p>
                    <hr>
                </div>

                <div class="jumbotron" id="kotak_info" style="display: none;">
                    <div class="media align-items-center">
                        <div class="media-left">
                            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/coins.png" alt="" width="80">
                        </div>
                        <div class="media-body media-middle">
                            <h4 class="mb-0">Package <span id="detail_package"></span> | $ <span id="detail_price"></span></h4>
                            <p class="text-muted mb-0"></p>
                            <p class="text-muted mb-0" id="detail_credit"></p>
                        </div>
                        <div class="media-footer">
                            <a id="lnk_click" style="cursor: pointer; text-decoration: none;" class="btn btn-success">Pay Now</a>
                        </div>
                    </div>
                </div>

                <div class="row" id="list_credit">
                                        
                </div>
            </div>        

        </div>

        <div class="modal  fade" id="modal_alert" style="margin-top: 10%;"  data-backdrop="static" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>The Rote Less Travelled</b></h3>
                    </div>
                    <div class="modal-body">
                        <label>You successfully unattended the class</label>
                    </div>                
                </div>
            </div>
        </div>
        <?php $this->load->view('inc/sidebar_tutor');?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        // $(".select2").select2();
        var tgl = new Date(); 
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc; 
        var access_token = "<?php echo $this->session->userdata('access_token');?>";
        var iduser = "<?php echo $this->session->userdata('id_user');?>";
        var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
        var member_ship = "<?php echo $this->session->userdata('member_ship');?>";

        $.ajax({
            url: '<?php echo base_url();?>Rest/list_package/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response);  
                var code = response['code'];   
                var color_orange = "#F7CA18";
                var color_green  = "#00E640";
                var color_red    = "#F22613";
                if (code == 200) {
                    for (var i = 0; i < response.data.length; i++) {
                        var id_package = response['data'][i]['id_package'];
                        var name_package = response['data'][i]['name_package'];
                        var credit_package = response['data'][i]['credit_package'];
                        var price_package_basic = response['data'][i]['price_package_basic'];
                        var price_package_premium = response['data'][i]['price_package_premium'];
                        if (i == 0) { color = color_orange; }else if(i == 1){ color = color_green;}else{ color = color_red;}
                        if(member_ship == "basic"){
                            var kotak_credit =                             
                            "<div class='col-md-4'>"+
                                "<div class='card text-center'>"+
                                    "<div style='height: 4px; background-color: "+color+";'></div>"+
                                    "<div class='card-header'>"+name_package+"</div>"+
                                    "<div class='card-body'>"+
                                        "<h4 class='card-title' style='margin-top: 5%;'>Price Package</h4>"+
                                        "<div class='row' style='margin-top: 6%; margin-bottom: 6%;'>"+
                                            "<div class='col-md-6'>"+
                                                "<p><h5>Price Basic</h5></p>"+
                                                "<p><h5>$ "+price_package_basic+"</h5></p>"+
                                            "</div>"+
                                            "<div class='col-md-6'>"+
                                                "<p><strong><h5>Price Premium</h5></strong></p>"+
                                                "<p><strong><h5>$ "+price_package_premium+"</h5></strong></p>"+
                                            "</div>"+
                                        "</div>"+
                                        "<p>Total "+credit_package+" Credits</p>"+
                                    "</div>"+
                                    "<div class='card-footer text-muted'>"+
                                        "<a href='#' data-credit='"+credit_package+"' data-price='"+price_package_basic+"' data-idpackage='"+id_package+"' data-namepackage='"+name_package+"' class='btn btn-primary btn-block btn_buy'>Buy Package</a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
                        }
                        else
                        {
                            var kotak_credit =                             
                            "<div class='col-md-4'>"+
                                "<div class='card text-center'>"+
                                    "<div style='height: 4px; background-color: "+color+";'></div>"+
                                    "<div class='card-header'>"+name_package+"</div>"+
                                    "<div class='card-body'>"+
                                        "<h4 class='card-title' style='margin-top: 5%;'>Price Package</h4>"+
                                        "<div class='row' style='margin-top: 6%; margin-bottom: 6%;'>"+                                            
                                            "<div class='col-md-12'>"+
                                                // "<p><strong><h5>Price Premium</h5></strong></p>"+
                                                "<p><strong><h5>$ "+price_package_premium+"</h5></strong></p>"+
                                            "</div>"+
                                        "</div>"+
                                        "<p>Total "+credit_package+" Credits</p>"+
                                    "</div>"+
                                    "<div class='card-footer text-muted'>"+
                                        "<a href='#' data-credit='"+credit_package+"' data-price='"+price_package_premium+"' data-idpackage='"+id_package+"' data-namepackage='"+name_package+"' class='btn btn-primary btn-block btn_buy'>Buy Package</a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
                        }                           

                        $("#list_credit").append(kotak_credit);
                    }
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
            }
        });


        $(document.body).on('click', '.btn_buy' ,function(e){
           var id_package = $(this).data('idpackage');
            var name_package = $(this).data('namepackage');   
            var total_credit = $(this).data('credit')
            var price_package = $(this).data('price');       
            if (iduser == null || iduser =='') {
                $("#modal_alert_login").modal('show');
                // $("#modal_buypackage").modal('show');
            }
            else
            {   
                $("#kotak_info").css('display','block');
                $('#total_credit').val(total_credit);
                $('#namepackage').val(name_package);
                $('#detail_package').text(name_package);
                $('#detail_credit').text(total_credit+" Credits");
                $('#detail_price').text(price_package);
                $('#item_price').val(price_package);
                $('#item_total').val(price_package);
                $('#item_description').val('Package '+name_package);
                $('#lnk_click').attr('href','<?php echo BASE_URL();?>products/buy_package/'+id_package);
            }
        });
            

    });
</script>
