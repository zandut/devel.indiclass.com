<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2>Result Quiz</h2>				
			</div> <!-- akhir block header    -->        
			<br>			

            <?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>

			<!-- disini untuk dashboard murid -->               
			<div class="card" style="margin-top: 2%;">
				<div class="card-header">
					<div class="pull-left"><h2>List Student</h2></div>                    
				</div>
				<div class="row" style="margin-top: 2%;">
					<div class="card-body card-padding">
						<div class="table-responsive">							
							<table id="" class="display table table-hover table-bordered data" cellspacing="0" width="100%">
						        <thead>
						        	<?php
										$tutor_id		= $this->session->userdata('id_user');
										$select_all 	= $this->db->query("SELECT tq.quiz_id, tq.id_user, tu.user_name, mb.name, mb.description, ms.subject_name FROM tbl_quiz as tq INNER JOIN master_banksoal as mb ON tq.bsid=mb.bsid INNER JOIN tbl_user as tu ON tu.id_user=tq.id_user INNER JOIN master_subject as ms ON ms.subject_id=tq.subject_id WHERE mb.tutor_id='$tutor_id'")->result_array();
									?>
						            <tr valign="middle">
						                <th><center>#</center></th>
						                <th><center>Name Student</center></th>						                
						                <th><center>Quiz Name</center></th>
						                <th><center>Quiz Description</center></th>
						                <th><center>Quiz Lesson</center></th>
						                <th><center>Action</center></th>
						            </tr>
						            
						        </thead>
						        <tbody>
						        	<?php
									$no=1;
									foreach ($select_all as $row => $v) {
										// if($row>0)break;
										$quiz_id 		= $v['quiz_id'];
										$id_user 		= $v['id_user'];
										$user_name 		= $v['user_name'];
										$name 			= $v['name'];
										$description 	= $v['description'];
										$subject_name 	= $v['subject_name'];										
										?>
							            <tr align="center">
							                <td><?php echo($no); ?></td>
							                <td><?php echo $user_name; ?></td>
							                <td><?php echo $name;?></td>						                
							                <td><?php echo $description; ?></td>
							                <td><?php echo $subject_name; ?></td>
							                <td>							                	
							                	<a href="<?php echo base_url();?>Tutor/QuizResultStudent?qd=<?php echo $quiz_id;?>" class="viewResult btn btn-block btn-success"><i class="zmdi zmdi-camera-bw"></i> View</a>           
							                </td>
							            </tr>
							            <?php
							            $no++;
							        }
							        ?>
						        </tbody>
						    </table>

						</div>
					</div>
				</div>
			</div>

		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	$('a[title]').tooltip();

	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        },
        "aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
        "iDisplayLength": 5
    } );
    $('.data').DataTable();
	
</script>
<script type="text/javascript">
    $(document).ready(function() {
	    $(function () {
            var tgl = new Date();  
            var formattedDate = moment(tgl).format('YYYY-MM-DD');         
            
            $('#validity').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });

            $('#invalidity').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });
            
        });

        $(document).on("click", ".viewDetailResult", function () {        	
	        var myBookId = $(this).attr('quiz_id');
	        $('#deletequiz').attr('uid',myBookId);
	        $('#modalconfrim_deletearchive').modal('show');
	    });  

	    $('#deletequiz').click(function(e){
            var soal_uid = $(this).attr('uid');            
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deleteQuestionArchive",
                type:"POST",
                data: {
                    soal_uid: soal_uid
                },
                success: function(data){
                	var response = JSON.parse(data);                	
                    // alert("Berhasil menghapus data siswa");
                    $('#modalconfrim_deletearchive').modal('hide');
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully delete");
                    setTimeout(function(){
                        location.reload(); 
                    },2000);
                                        
                } 
            });
        });
	});
</script>