<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2><?php echo $this->lang->line('reportteaching'); ?></h2>				
			</div> <!-- akhir block header    -->        
			<br>			

			<div class="row"> 
                <div class="col-sm-4 col-md-4">
                    <div class="card pt-inner" style="border-radius: 10px;">
                        <div class="pti-header bgm-orange">
                        	<?php 
                        		$tutorcount 	= $this->session->userdata('id_user');
                        		$totalkelas		= $this->db->query("SELECT count(*) as total FROM master_banksoal WHERE tutor_id='$tutorcount'")->row_array();
                        	?>
                            <h2><?php echo $totalkelas['total']; ?> </h2>                        	
                            <div class="ptih-title">Total Quiz</div>
                        </div>                               
                    </div>
                </div>                                 
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>

			<!-- disini untuk dashboard murid -->               
			<div class="card" style="margin-top: 2%;">
				<div class="card-header">
					<div class="pull-left"><h2>Quiz</h2></div>
                    <div class="pull-right"><a data-toggle="modal" href="#modaladdsubject"><button class="btn btn-success"><i class="zmdi zmdi-plus"></i> New Quiz</button></a></div>
				</div>
				<div class="row" style="margin-top: 2%;">
					<div class="card-body card-padding">
						<div class="table-responsive">
							<table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
								<thead>
									<?php
									$tutor_id		= $this->session->userdata('id_user');
									$select_all 	= $this->db->query("SELECT ms.subject_name, mj.jenjang_name, mj.jenjang_level, mbs.* FROM master_banksoal as mbs INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id) On mbs.subject_id=ms.subject_id WHERE mbs.tutor_id='$tutor_id'")->result_array();
									?>
									<tr>
										<th data-column-id="no">No</th>
										<th>Quiz Name</th>
										<th>Quiz Description</th>
										<th>Quiz Subject</th>
										<th>Quiz Jenjang</th>
										<th>Start Quiz Date</th>
										<th>Finish Date Quiz</th>
										<th>Duration</th>
										<th>Type</th>
										<th></th>
										<th>Options</th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>  
									<?php
									$no=1;
									foreach ($select_all as $row => $v) {	
										$quiz_name 		= $v['name'];
										$quiz_desc 		= $v['description'];
										$subject_id		= $v['subject_id'];
										$subject_name 	= $v['subject_name'];
										$jenjang 		= $v['jenjang_level'].' - '.$v['jenjang_name'];
										$duration 		= $v['duration'];
									    $hours 			= floor($duration / 3600);
									    $mins 			= floor($duration / 60 % 60);
									    $secs 			= floor($duration % 60);
									    $timeFormat 	= sprintf('%02d:%02d', $hours, $mins);
										$type 			= $v['template_type'];

										$user_utc		= $this->session->userdata('user_utc');
										$server_utcc    = $this->Rumus->getGMTOffset();				
										$intervall      = $user_utc - $server_utcc;				
							            $validity       = DateTime::createFromFormat ('Y-m-d H:i:s',$v['validity']);
							            $validity->modify("+".$intervall ." minutes");
							            $validity       = $validity->format('d-m-Y H:i:s');
							            $invalidity     = DateTime::createFromFormat ('Y-m-d H:i:s',$v['invalidity']);
							            $invalidity->modify("+".$intervall ." minutes");
							            $invalidity     = $invalidity->format('d-m-Y H:i:s');	
							            if ($type == "essay") {
	            							$type = "Essay";
	            						}					
	            						else if($type == "multiple_choice"){
	            							$type = "Multiple Choise";
	            						}	
										?>								
										<tr>
											<td><?php echo($no); ?></td>
											<td><?php echo $quiz_name; ?></td>
											<td><?php echo $quiz_desc; ?></td>
											<td><?php echo $subject_name; ?></td>
											<td><?php echo $jenjang; ?></td>
											<td><?php echo $validity; ?></td>
											<td><?php echo $invalidity; ?></td>
											<td><?php echo $timeFormat.' Hours'; ?></td>
											<td><?php echo $type; ?></td>
											<td>
                                                <a href="<?php echo base_url(); ?>Tutor/EditQuiz?bsid=<?php echo $v['bsid'] ?>"><button class="btn bgm-bluegray" title="Edit Quiz"><i class="zmdi zmdi-edit"></i> Edit</button></a>
                                            </td>
                                            <td>
                                                <a data-toggle="modal" rtp="<?php echo $v['bsid'] ?>" class="deletequiz" data-target-color="green" href="#modalDelete"><button class="btn bgm-red" title="Delete Quiz"><i class="zmdi zmdi-delete"></i> Delete</button></a>
                                            </td>                                      
                                            <td>
                                                <a href="<?php echo base_url();?>tutor/QuestionView?bsid=<?php echo $v['bsid'];?>"><button class="btn bgm-lightblue" title=""><i class="zmdi zmdi-delete"></i> View Question</button></a>
                                            </td>
										</tr>
										<?php 
										$no++;
									}
									?>		
								</tbody>   
							</table>   
						</div>
					</div>
				</div>
			</div>

			<!-- Modal TAMBAH -->  
            <div class="modal" id="modaladdsubject" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                            <h4 class="modal-title c-white">New Quiz</h4>
                        </div>
                        <div class="modal-body m-t-20">
                            <!-- <form method="post" action="<?php echo base_url(); ?>process/QuizAddNew" enctype="multipart/form-data"> -->
	                            <div class="row p-15">    
	                                
	                                <div class="col-sm-12 col-md-12 col-xs-12">
	                                	<label class="fg-label">Quiz Name</label>
	                                    <div class="form-group fg-float">
	                                        <div class="fg-line">
	                                            <input type="text" name="name" id="name_quiz" required class="input-sm form-control fg-input">                                          
	                                        </div>
	                                    </div>
	                                </div>  

	                                <div class="col-sm-12 col-md-12 col-xs-12">
	                                	<label class="fg-label">Quiz Description</label>
	                                    <div class="form-group fg-float">
	                                        <div class="fg-line">
	                                            <textarea name="description" id="description_quiz" class="input-sm form-control fg-input" rows="4"></textarea>                                            
	                                        </div>
	                                    </div>
	                                </div>                                

	                                <div class="col-md-12 m-t-5">        
	                                <label class="fg-label">Jenjang</label>                                                                                   
	                                    <div class="form-group">                                    
	                                        <div class="fg-line">
	                                            <select class='select2 form-control col-md-12 input-sm fg-input' required id="subject_id_quiz" name="subject_id" style="width: 100%;">
			                                        <?php
			                                        	$id = $this->session->userdata("id_user");
			                                            $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' order by ms.subject_id ASC")->result_array();
			                                            echo '<option disabled="disabled" selected="" value="">Pilih Pelajaran</option>'; 
			                                            foreach ($allsub as $row => $v) {                                            
			                                                echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_level'].' '.$v['jenjang_name'].'</option>';
			                                            }
			                                        ?>
			                                    </select>                                              
	                                        </div>
	                                    </div>
	                                </div>

	                                <div class="col-sm-12 col-md-12 col-xs-12">
	                                	<label class="fg-label">Start Quiz Date</label>
	                                    <div class="form-group fg-float">                                    	
	                                        <div class="fg-line">
	                                            <input type="text" class=" form-control" id="validity_quiz" name="validity" data-date-format="DD-MM-YYYY" placeholder="Select start date"/>
	                                        </div>
	                                    </div>
	                                </div> 

	                                <div class="col-sm-12 col-md-12 col-xs-12">
	                                	<label class="fg-label">Finish Quiz Date</label>
	                                    <div class="form-group fg-float">                                	
	                                        <div class="fg-line">
	                                            <input type="text" class=" form-control" id="invalidity_quiz" name="invalidity" data-date-format="DD-MM-YYYY" placeholder="Select finish date"/>
	                                        </div>
	                                    </div>
	                                </div>	                                

	                                <div class="col-sm-12 col-md-12 col-xs-12">
	                                	<label class="fg-label">Duration</label>
	                                    <div class="form-group">                                	
	                                        <div class="fg-line">
	                                            <select required class="select2 form-control col-md-12 input-sm fg-input" id="duration_quiz" name="duration" style="width: 100%;">
	                                                <option disabled selected value=""><?php echo $this->lang->line('searchduration'); ?></option>
	                                                <option value="900">15 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="1800">30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="2700">45 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="3600">1 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="5400">1 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="7200">2 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="9000">2 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="10800">3 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="12600">3 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="14400">4 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="16200">4 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="18000">5 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="19800">5 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="21600">6 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="23400">6 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="25200">7 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="27000">7 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="28800">8 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="30600">8 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="32400">9 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="34200">9 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="36000">10 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="37800">10 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="39600">11 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="41400">11 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="43200">12 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="45000">12 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="46800">13 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="48600">13 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="50400">14 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="52200">14 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="54000">15 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="55800">15 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="57600">16 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="59400">16 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="61200">17 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="63000">17 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="64800">18 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="66600">18 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="68400">19 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="70200">19 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="72000">20 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="73800">20 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="75600">21 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="77400">21 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="79200">22 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="81000">22 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="82800">23 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="84600">23 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="86400">24 <?php echo $this->lang->line('jam'); ?></option>
	                                            </select>
	                                        </div>
	                                    </div>
	                                </div>

	                                <input type="text" id="template_type_quiz" name="template_type" value="multiple_choice" hidden="true">

	                                <div class="col-md-12 m-t-20">
	                                    <button id="saveQuiz" class="btn btn-success btn-block">Simpan</button>
	                                </div>

	                            </div>
                            <!-- </form> -->
                            <br>
                        </div>
                    </div>
                </div>
            </div>    

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Delete Quiz</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="deletequiz" rtp="">Ya</button>                            
                        </div>                        
                    </div>
                </div>
            </div>  

		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	$('a[title]').tooltip();

	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();
	
</script>
<script type="text/javascript">
    $(document).ready(function() {
	    $(function () {
            var tgl = new Date();  
            var formattedDate = moment(tgl).format('YYYY-MM-DD');         
            
            $('#validity_quiz').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });

            $('#invalidity_quiz').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });
            
        });

	    $(document).on('click', "#saveQuiz", function(e){
	    	var name_quiz			= $("#name_quiz").val();
	    	var description_quiz	= $("#description_quiz").val();
	    	var subject_id_quiz		= $("#subject_id_quiz").val();
	    	var validity_quiz		= $("#validity_quiz").val();
	    	var invalidity_quiz		= $("#invalidity_quiz").val();
	    	var duration_quiz		= $("#duration_quiz").val();
	    	var template_type_quiz	= $("#template_type_quiz").val();

	    	if (name_quiz == "" || description_quiz == "" || subject_id_quiz == "" || validity_quiz == "" || invalidity_quiz == "" || duration_quiz == null || template_type_quiz == null) {
	    		notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Terdapat data kosong, harap isi dengan lengkap!");
	    	}
	    	else
	    	{
	    		$.ajax({
	                url :"<?php echo base_url() ?>Process/QuizAddNew",
	                type:"POST",
	                data: {
	                    name: name_quiz,
	                    description: description_quiz,
	                    subject_id: subject_id_quiz,
	                    validity: validity_quiz,
	                    invalidity: invalidity_quiz,
	                    duration: duration_quiz,
	                    template_type: template_type_quiz
	                },
	                success: function(data){
	                	var response = JSON.parse(data);
	                	if (response['status'] == 1) {
	                		$("#modaladdsubject").modal('hide');
	                		notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
	                		setTimeout(function(){
		                        location.reload(); 
		                    },3500);
	                	}
	                	else
	                	{
	                		notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',response['message']);
	                		setTimeout(function(){
		                        location.reload(); 
		                    },3500);
	                	}
	                }
	            });
	    	}
	    });

        $(document).on("click", ".deletequiz", function () {
	        var myBookId = $(this).attr('rtp');
	        $('#deletequiz').attr('rtp',myBookId);
	    });  

	    $('#deletequiz').click(function(e){
            var rtp = $(this).attr('rtp');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deleteQuiz",
                type:"POST",
                data: {
                    rtp: rtp
                },
                success: function(data){
                	var response = JSON.parse(data);                	
                    // alert("Berhasil menghapus data siswa");
                    $('#modalDelete').modal('hide');
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully delete Quiz");
                    setTimeout(function(){
                        location.reload(); 
                    },2000);
                                        
                } 
            });
        });
	});
</script>