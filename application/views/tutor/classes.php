<div class="d-flex flex-column h-100">
    
    <!-- Navbar -->
        <?php $this->load->view('inc/navbar_tutor');?>
    <!-- // END Navbar -->

    <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
        <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
            <div class="container">

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="instructor-dashboard.html">Home</a></li>
                    <li class="breadcrumb-item active">Courses</li>
                </ol>
                <h1 class="page-heading h2">Manage Courses</h1>
                <div class="card-columns">

                    <div class="col-md-12">
                        <div class="row" id="box_listclasses">

                        </div>
                    </div>

                </div>

                <div id="box_listclassesnull"></div>
            </div>        

        </div>

        <div class="modal  fade" id="modal_alert" style="margin-top: 10%;"  data-backdrop="static" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>The Rote Less Travelled</b></h3>
                    </div>
                    <div class="modal-body">
                        <label>You successfully unattended the class</label>
                    </div>                
                </div>
            </div>
        </div>
        <?php $this->load->view('inc/sidebar_tutor');?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        // $(".select2").select2();
        var tgl = new Date(); 
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc; 
        var access_token = "<?php echo $this->session->userdata('access_token');?>";
        var iduser = "<?php echo $this->session->userdata('id_user');?>";

        $.ajax({
            url: '<?php echo AIR_API;?>channel_classList/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id : '46',
                user_utc : user_utc
            },
            success: function(response)
            {
                var a = JSON.stringify(response);
                var box_listclasses = "";
                var box_listclassesnull = "";
                if (response['code'] == -400) {
                    window.location.href="<?php echo BASE_URL();?>Admin/Logout";
                }
                else
                {
                    if (response['data'] == "" || response['data'] == null) {
                        box_listclassesnull += 
                        "<div class='col-md-12'>"+
                            "<div class='card bg-info text-white text-center'>"+
                                "<div class='card-body'>"+
                                    "<p>No Class</p>"+
                                "</div>"+
                            "</div>"+
                        "</div>";
                        $("#box_listclasses").append(box_listclassesnull);
                    }
                    else
                    {
                        var notnull = null;
                        for (var i = 0; i < response['data'].length; i++) {
                            var class_id   = response['data'][i]['class_id'];
                            var user_name   = response['data'][i]['user_name'];
                            var user_image  = response['data'][i]['user_image'];
                            var name        = response['data'][i]['name'];
                            var description = response['data'][i]['description'];
                            var start_time  = response['data'][i]['start_time'];
                            var finish_time = response['data'][i]['finish_time'];
                            var class_type  = response['data'][i]['class_type'];
                            var template_type = response['data'][i]['template_type'];
                            var link = response['data'][i]['url'];
                            var showtime    = moment(start_time).format('DD-MM-YYYY HH:mm'); 
                            var showendtime = moment(finish_time).format('DD-MM-YYYY HH:mm');
                            if (template_type == "all_featured"){ template_type = "All Features";}
                            var im_exist = null;
                            var participant_listt = response['data'][i]['participant']['participant'];
                            if(participant_listt != null){
                                for (var iai = 0; iai < participant_listt.length ;iai++) {

                                    var a = moment(tgl).format('YYYY-MM-DD');
                                    if (iduser == participant_listt[iai]['id_user']) {
                                        im_exist = true;
                                        notnull = true;
                                    }
                                }
                            }

                            if (im_exist) {
                                box_listclasses += 
                                "<div class='col-md-6 col-sm-6 col-xs-6'>"+
                                    "<div class='card'>"+
                                        "<div class='card-header bg-white text-center'>"+
                                            "<h4 class='card-title'><a href='student-take-course.html'>"+name+"</a></h4>"+
                                        "</div>"+
                                        "<a href='student-take-course.html'>"+
                                            "<center>"+
                                                "<img onerror='this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+user_image+"' alt='' style='width:100%;'>"+
                                            "</center>"+
                                        "</a>"+
                                        "<div class='card-body'>"+
                                            "<small class='text-muted'>"+user_name+"</small><br> "+description+"<br>"+
                                            "<span class='badge badge-primary '>"+class_type+"</span> | "+
                                            "<span class='badge badge-primary '>"+template_type+"</span>"+
                                        "</div>"+
                                        "<div class=card-footer>"+
                                            "<button class='btn btn-block btn-danger btnUnAttend' data-class_id='"+class_id+"' style='cursor:pointer;'>Unjoin</button>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>";
                            }
                            else
                            {
                                box_listclassesnull += 
                                "<div class='col-md-12'>"+
                                    "<div class='card bg-info text-white text-center'>"+
                                        "<div class='card-body'>"+
                                            "<p>No Class</p>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>";
                            }
                            
                        }   
                        if (notnull == null) {
                            $("#box_listclassesnull").append(box_listclassesnull);
                        }   
                        else
                        {   
                            $("#box_listclasses").append(box_listclasses);
                        }
                    }
                }
            }
        });

        $(document.body).on('click', '.btnUnAttend' ,function(e){
            var class_id = $(this).data('class_id');            
            // if (member_ship == "") {
            //     window.location.href="<?php echo BASE_URL();?>Student/MemberTier";
            // }
            // else
            // {
                $.ajax({
                    url: '<?php echo AIR_API;?>channel_unjoinClass/access_token/'+access_token,
                    type: 'POST',
                    data: {
                        class_id : class_id,
                        id_user : iduser
                    },
                    success: function(response)
                    {
                        if (response['code'] == -400) {
                            window.location.href="<?php echo BASE_URL();?>Admin/Logout";
                        }
                        else
                        {
                            $("#modal_alert").modal("show");
                            setTimeout(function(){
                                location.reload();
                            },3000);
                        }
                    }
                });
            // }
        });
            

    });
</script>
