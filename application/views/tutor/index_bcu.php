<!-- <div id="loader">
    <div class="page-loader" style="background-color:#1274b3;">
        <div class="preloader pl-xl pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Class Miles . . .</p>
        </div>
    </div>
</div> -->
<header id="header-2" class="clearfix" style="z-index: 10; position: fixed; width: 100%;" data-current-skin="<?php echo $this->session->userdata('color'); ?>">
    
        <style type="text/css">
            /* scroller browser */
        ::-webkit-scrollbar {
            width: 9px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
            -webkit-border-radius: 7px;
            border-radius: 7px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            -webkit-border-radius: 7px;
            border-radius: 7px;
            background: #a6a5a5;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
        }
        ::-webkit-scrollbar-thumb:window-inactive {
            background: rgba(0,0,0,0.4); 
        }
        </style>
        <ul class="header-inner clearfix">
            <li id="menu-trigger" data-trigger="#sidebar" >
                <div class="line-wrap">
                    <div class="line top"></div>
                    <div class="line center"></div>
                    <div class="line bottom"></div>
                </div>
            </li>

            <li>
                <a href="#" class="m-l-20"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" alt=""></a>
            </li> 
            <li class="pull-right hidden-xs">
                <ul class="top-menu">
                    <!-- <li id="toggle-width">
                        <div class="toggle-switch">
                            <input id="tw-switch" type="checkbox" hidden="hidden">
                            <label for="tw-switch" class="ts-helper"></label>
                        </div>
                    </li> -->
                    
                    <!-- <li id="top-search">
                        <a href=""><i class="tm-icon zmdi zmdi-search"></i></a>
                    </li> -->

                    <li class="dropdown" id="notif" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;" id_user="<?php echo $this->session->userdata('id_user');?>">
                        <a data-toggle="dropdown" href="" ng-init="getnotif()">
                            <i class="tm-icon zmdi zmdi-notifications"></i>
                            <i class='tmn-counts bara' ng-if="nes > '0'">{{nes}}</i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg pull-right" >
                            <div class="listview" id="notifications" >
                                <div class="lv-header">
                                    <?php echo $this->lang->line('notification') ?>

                                    <ul class="actions" data-toggle="tooltip" title="Read all" data-placement="right">
                                        <li class="dropdown">   
                                            <a class="readsa" style="cursor: pointer;" id_user='<?php echo $this->session->userdata('id_user');?>'>
                                                <i class="zmdi zmdi-check-all" ></i>
                                            </a>                                                                  
                                        </li>
                                    </ul>
                                </div>                        
                                <div class="lv-body" id="scol" style="overflow-y:auto; height:350px; " when-scrolled="loadData()" style="cursor: pointer;">
                                    <div ng-repeat='x in dataf' style="cursor: pointer;">
                                        <a class="lv-item tif" ng-if="x.read_status == '1'" hrefa="{{x.link}}" notif_id="{{x.notif_id}}">
                                            <div class="media" data-toggle="tooltip" title="">
                                                <div class="pull-left">
                                                    <img width="65px" height="65px" style="border-radius: 5px;" src="<?php echo base_url('aset/img/user/'.$this->session->userdata('user_image')); ?>"> 
                                                </div>
                                                <div class="media-body" style="overflow:hidden; word-wrap: break-word;">
                                                    <div style="font-size: 11px; color:black;">{{x.notif}}</div>                                            
                                                </div>
                                                <div class="media-footer">
                                                    <small class="c-black" style="position:absolute; bottom:0px; margin-bottom: 1px; margin-top: auto;">{{x.interval_time}}</small>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="lv-item tif" style="background: #ECF0F1;" ng-if="x.read_status == '0'" hrefa="{{x.link}}" notif_id="{{x.notif_id}}" >
                                            <div class="media">
                                                <div class="pull-left">
                                                    <img width="65px" height="65px" src="<?php echo base_url('aset/img/user/'.$this->session->userdata('user_image')); ?>"> 
                                                </div>
                                                <div class="media-body" style="overflow:hidden; word-wrap: break-word;">
                                                    <div style="font-size: 11px; color:black;">{{x.notif}}</div>                                            
                                                </div>
                                                <div class="media-footer">
                                                    <small class="c-black" style="position:absolute; bottom:0px; margin-bottom: 1px; margin-top: auto;">{{x.interval_time}}</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div ng-show="lodingnav">
                                        <center><div class="preloader pl-lg">
                                            <svg class="pl-circular" viewBox="25 25 50 50">
                                                <circle class="plc-path" cx="50" cy="50" r="20" />
                                            </svg>
                                        </div></center>
                                    </div>  
                                    <!-- <a class="lv-item" style="background: #ECF0F1;">
                                        <div class="media" data-toggle="tooltip" data-placement="bottom" title="Please, complete your profile for tutor registration approval">
                                            <div class="pull-left">
                                                <img class="lv-img-sm" src="<?php echo base_url('aset/img/user/'.$this->session->userdata('user_image')); ?>">                                        
                                            </div>
                                            <div class="media-body">
                                                <div class="lv-title">Administrator</div>
                                                <small class="lv-small">Please, complete your profile for tutor registration approval</small>
                                                <small class="c-black">34 Menit yang lalu</small>
                                            </div>                                    
                                        </div>
                                    </a> -->
                                </div>                     

                            </div>

                        </div>
                    </li>     

                    <li class="dropdown" style="height: 35px;">
                        <a data-toggle="dropdown" href="">
                             
                            <?php
                                $patokan =  $this->lang->line('profil');

                                if ($patokan == "Profile") {
                                    ?>
                                    <img class="m-t-5" width="35px" height="20px" style="border-radius:15%;" src="<?php echo base_url('aset/img/language/flaginggris.png'); ?>"                
                                    <?php
                                }
                                else if ($patokan == "Data Diri"){
                                    ?>
                                    <img class="m-t-5" width="35px" height="20px" style="border-radius:15%;" src="<?php echo base_url('aset/img/language/flagindo.png'); ?>"                
                                    <?php
                                }
                            ?>                           
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg pull-right">
                            <div class="listview" id="notifications">

                                <div class="lv-header">
                                    <?php echo $this->lang->line('chooselanguage'); ?>                                                            
                                </div>

                                <div class="lv-body" id="checklang">                            
                                    <a class="lv-item" href="<?php echo base_url('set_lang/english'); ?>" id="btn_setenglish">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img width="35px" height="20px" style="border-radius:15%;" src="<?php echo base_url(); ?>aset/img/language/flaginggris.png" alt="">
                                            </div>
                                            <div class="media-body">
                                                <div class="lv-title">English</div>                                                    
                                            </div>
                                        </div>
                                    </a>  
                                    <a class="lv-item" href="<?php echo base_url('set_lang/indonesia'); ?>" id="btn_setindonesia">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img width="35px" height="20px" style="border-radius:15%;" src="<?php echo base_url(); ?>aset/img/language/flagindo.png" alt="">
                                            </div>
                                            <div class="media-body">
                                                <div class="lv-title">Bahasa Indonesia</div>                                                    
                                            </div>
                                        </div>
                                    </a>         
                                    <!-- <a class="lv-item" href="<?php echo base_url('set_lang/arab'); ?>">
                                        <div class="media">
                                            <div disabled="true" class="pull-left">
                                                <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/language/saudi_arabia.png" alt="">
                                            </div>
                                            <div class="media-body">
                                                <div class="lv-title">Arabic</div>                                                    
                                            </div>
                                        </div>
                                    </a> -->                                      
                                </div>                                                
                            </div>

                        </div>
                    </li>

                    <li class="dropdown hidden-xs">
                        <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                        <ul class="dropdown-menu dm-icon pull-right">
                           <!--  <li class="skin-switch hidden-xs">
                                <span class="ss-skin bgm-lightblue" data-skin="lightblue" id="lightblue"></span>
                                <span class="ss-skin bgm-bluegray" data-skin="bluegray" id="bluegray"></span>
                                <span class="ss-skin bgm-cyan" data-skin="cyan" id="cyan"></span>
                                <span class="ss-skin bgm-teal" data-skin="teal" id="teal"></span>
                                <span class="ss-skin bgm-orange" data-skin="orange" id="orange"></span>
                                <span class="ss-skin bgm-blue" data-skin="blue" id="blue"></span>
                            </li> -->
                            <li class="hidden-xs">
                                <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i><?php echo $this->lang->line('togglefullscreen'); ?></a>
                            </li>
                            <li class="divider hidden-xs"></li>
                            
                            <li>
                                <a onclick="signOut()" data-action="" href="<?php echo base_url('/logout'); ?>"><i class="zmdi zmdi-time-restore"></i><?php echo $this->lang->line('logout'); ?></a>
                            </li>
                        </ul>
                    </li>
                   <!--  <li class="hidden-xs" id="chat-trigger" data-trigger="#chat">
                        <a href=""><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>
                    </li> -->
                </ul>
            </li>
        </ul>

        <div>
        <?php
            $now = date('N');
            if(isset($page) && $page == 0){
                $nextMon = date('Y-m-d', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));    
            }else if(isset($page) && $page == 1){
                $nextMon = date('Y-m-d', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
                $nextMon = date('Y-m-d', strtotime('+7 day', strtotime($nextMon)));
            }
            $oneWeek = date('Y-m-d', strtotime('+6 day', strtotime($nextMon)));
            $nextMon = DateTime::createFromFormat('Y-m-d',$nextMon);
            $oneWeek = DateTime::createFromFormat('Y-m-d',$oneWeek);

            $timeArray = array();

            $arrayFul;
            $stime = 0;
            for($x=0;$x<7;$x++){
                $timeArray[$x]["day"] =  date('D, d M', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));
                $timeArray[$x]["date"] = date('Y-m-d', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));                

                for ($i=0; $i < 24; $i++) {
                    $timeArray[$x][$i] = $stime;
                    $stime+=3600;
                }
                $stime = 0;
            }


        ?> 
        <center>
            <nav class="ha-menu">
                <ul>
                    <?php
                        $this->session->set_userdata('cektglnih', date("Y-m-d")); 
                        $no = 1;
                        foreach ($timeArray as $key => $value) {
                            // echo "<th style='text-align:center; font-size:12px;'>".$value['day']."</th>";
                            if ($value['date'] == date("Y-m-d")) {
                                echo "<li class='bbb".$no." c waves-effect active' tglskrng='".$value['date']."'><a>".$value['day']."</a></li>";
                            }
                            else
                            {
                                echo "<li class='bbb".$no." c waves-effect' tglskrng='".$value['date']."'><a>".$value['day']."</a></li>";
                            }
                            $no++;
                        }

                    ?>
                    <!-- <li class="waves-effect"> <a href="#">Senin</a></li>
                    <li class="active waves-effect"> <a href="#">Selasa</a></li>
                    <li class="waves-effect"><a href="#">Rabu</a></li>
                    <li class="waves-effect"> <a href="#">Kamis</a></li>
                    <li class="waves-effect"> <a href="#">Jumat</a></li>
                    <li class="waves-effect"> <a href="#">Sabtu</a></li>
                    <li class="waves-effect"> <a href="#">Minggu</a></li> -->
                </ul>
            </nav>
            </center>
        </div>
        <div class="pull-right">
            
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#btn_setindonesia').click(function(e){
                    e.preventDefault();
                    $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
                });
                $('#btn_setenglish').click(function(e){
                    e.preventDefault();
                    $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
                });
            });
        </script>

</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 4.5%; z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>


    <section id="content" style="margin-top: 5%;">

    <!-- <textarea rows="20" style="width: 100%;" id="aaa"></textarea>
    <div id="bbb"></div> -->

        <div class="container">                
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="block-header">
                <h2><?php echo $this->lang->line('home'); 
                    ?></h2>

                <ul class="actions">
                    <li>
                        <ol class="breadcrumb">
                        </ol>
                    </li>
                </ul>
            </div> <!-- akhir block header    -->                   
            
            <div class="card p-l-15 p-r-15" id="utama1">
                <div class="row">

                    <div class="card-header">
                        <h2><i class="zmdi zmdi-cast-connected"></i> <label class="c-red">Live Now</label></h2>

                        <hr>
                    </div>

                    <div class="card-body" style="margin-top: -2%;">
                        <div class="col-lg-12" id="liveboxclass">                           

                        </div>                      
                    </div>

                </div>
            </div>

            <div class="card p-l-15 p-r-15" id="utama2">
                <div class="row">

                    <div class="card-header">
                        <h2><i class="zmdi zmdi-open-in-browser"></i> <label class="c-bluegray"> Semua Kelas </label></h2>

                        <hr>
                    </div>

                    <div class="card-body" style="margin-top: -2%;">
                        <div class="col-lg-12" id="xboxclass">                          

                        <!-- <div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><div class='card picture-list'><div class='card blog-post' style='height:280px;'><img src='../aset/img/widgets/preview.jpg' width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me/"+idclass+"'><h2 class='m-l-10'>"+sbjname+" - "+desname+"<small>"+ttrname+"</small><small>"+time+"-"+endtime+" | "+datee+"</small></h2><button class='btn btn-success btn-block' style='margin-top: 11px;'>Masuk</button></a></div></div>
                        </div> -->                      
                    </div>

                </div>
            </div>

            

        </div>
        <div class="card p-l-15 p-r-15 p-t-5 p-b-5" id="utama4" style="display: none;">
                <div class="row">
                
                    <div class="card-header" style="margin-top: 0%;">
                        
                        <div class="alert alert-danger text-center f-14" role="alert">Tidak ada kelas hari ini</div>

                    </div>

                </div>
            </div>
    </div>
</section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function(){

        var kliktgl = null;
        var iduser = "<?php echo $this->session->userdata('id_user'); ?>";
        var tgl = new Date();
        
        var formattedDate = moment(tgl).format('YYYY-MM-DD');
        var datenow = tgl.getHours()+":"+tgl.getMinutes();  

        function timeToSeconds(time) {
            time = time.split(/:/);
            return time[0] * 3600 + time[1] * 60;
        }   

        $(".c").click(function(){
            var aj = $(this).attr('tglskrng');
            var ck = $(this).attr('hariini');
            kliktgl = aj;
            if (formattedDate != kliktgl) 
            {                              
                $('.c').removeClass("active");
                $(this).addClass("active");
                formattedDate = kliktgl;        
                getschedule();
            }
            else
            {
                formattedDate = moment(tgl).format('YYYY-MM-DD');
            }
            
        });
        getschedule();

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        function getschedule()
        {
            $("#liveboxclass").empty();
            $("#xboxclass").empty();
            $.ajax({
                url: 'https://classmiles.com/Rest/scheduleonetutor',
                type: 'POST',
                data: {
                    id_user: iduser,
                    date: formattedDate
                },
                success: function(response)
                {   
                    var a = JSON.stringify(response);
                    var jsonPretty = JSON.stringify(JSON.parse(a),null,2);  
                    // alert(jsonPretty);   
                    // $("#aaa").text(jsonPretty);  
                    // alert(response['data'][1]['class_id']);

                    if (response['data'] == null) 
                    {                                                
                        $("#utama1").css('display','none');
                        $("#utama2").css('display','none');
                        $("#utama4").css('display','block');
                    }                   
                    else
                    {
                        $("#utama4").css('display','none');
                        $("#utama1").css('display','block');
                        $("#utama2").css('display','block');
                        if (response.data.length != null) {
                            for (var i = response.data.length-1; i >=0;i--) {

                                var idclass = response['data'][i]['class_id'];
                                var sbjname = response['data'][i]['subject_name'];
                                var desname = response['data'][i]['description'];
                                var desfull = response['data'][i]['description'];
                                var ttrname = response['data'][i]['user_name'];
                                var datee   = response['data'][i]['date'];
                                datee = moment(datee).format('DD-MM-YYYY');
                                var time    = response['data'][i]['start_time'];
                                var endtime = response['data'][i]['finish_time'];                       
                                desname = desname.substring(0, 33);                               
                                    
                                    if (timeToSeconds(datenow) < timeToSeconds(time)) 
                                    {
                                        var kotak = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:240px;'><img src='../aset/img/widgets/preview.jpg' width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+time+"  -  "+endtime+"</small></h2></a></div></a></div>";
                                        $("#xboxclass").append(kotak);
                                    }
                                    else if (timeToSeconds(time) > timeToSeconds(datenow) || timeToSeconds(datenow) < timeToSeconds(endtime)) {
                                        var kotak = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='<?php echo base_url(); ?>master/tps_me/"+idclass+"'><div class='card picture-list'><div class='card blog-post' style='height:240px;'><img src='../aset/img/headers/4.png' width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='<?php echo base_url(); ?>master/tps_me/"+idclass+"'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+time+"  -  "+endtime+"</small></h2></a></div></a></div>";
                                        $("#liveboxclass").append(kotak);
                                    }
                                    else
                                    {
                                        var kotak = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:240px;'><img src='../aset/img/widgets/preview.jpg' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+time+"  -  "+endtime+"</small></h2></div></a></div>";
                                        $("#xboxclass").append(kotak);                                        
                                    }                                                                                                                                                          
                            }                          

                        } 
                    }      
                }
            });
        }         
    });
</script>                               