<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->
<style type="text/css">

        input::-webkit-input-placeholder {
    color: grey !important;
    }
    input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}
</style>

<script type="text/javascript">
           function hanyaAngka(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
          return true;
        }
</script>

<script type="text/javascript">
    $(document).ready(function(){
        
       
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        // var email = "<?php echo $this->session->userdata('email');?>";
        // var number = "<?php echo $this->session->userdata('number');?>";
        
        // if (tokenjwt == null) {
        //     $.ajax({
        //         url:'<?php echo base_url();?>Rest/login',
        //         type:'POST',
        //         data:{
        //             email: email,
        //             password: number
        //         },
        //         success:function(response){
        //             console.warn(response);
        //             var access_token = response['access_token'];                     
        //             window.localStorage.setItem('access_token_jwt', access_token);
        //         }
        //     });
        // }

        jQuery('#provinsi_id').change(function(){
           if($('#user_birthplace').val() == ('')){
                $('#cek_tempat').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');  
               
            }
            if($('#user_numberid').val() == ('')){
                $('#cek_nomorid').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');  
               
            }
           if($('#user_address').val() == ('')){
                $('#cek_alamat').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
               
            }
            else{
                $('#cek_tempat').html('').css('color', 'red');
                $('#cek_alamat').html('').css('color', 'red');
                $('#cek_nomorid').html('').css('color', 'red');
            }
        });
        $('#user_birthplace').on('keyup',function(){
            if($('#user_birthplace').val() != ('')){
                $('#cek_tempat').html('').css('color', 'red');
            }else{
                $('#cek_tempat').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
            }
        });
        $('#user_address').on('keyup',function(){
            if($('#user_address').val() != ('')){
                $('#cek_alamat').html('').css('color', 'red');
            }else{
                $('#cek_alamat').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
            }
        });
        $('#user_numberid').on('keyup',function(){
            if($('#user_numberid').val() != ('')){
                $('#cek_nomorid').html('').css('color', 'red');
            }else{
                $('#cek_nomorid').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
            }
        });
        $('#place_experience1').on('keyup',function(){
            if($('#place_experience1').val() != ('')){ 
             $('.buttonFinish').removeClass('disabled');
            }else{
                $('.buttonFinish').addClass('disabled');
            }
        });
    })
</script>
<script type="text/javascript">
    var counter = 1;
    var limit = 10;
    function addInput(divName){
         
              var newdiv = document.createElement('div');
              newdiv.innerHTML =
            "<h3>Data Tambahan</h3><br>"+
            "<div class='form-group fg-float'>"+
            "<div class='fg-line'>"+
            "<input data-toggle='tooltip' data-placement='bottom' placeholder='<?php echo $this->lang->line('educational_competence'); ?>' title='Ex: Computer Science, Biology, etc.' type='text' required class='form-control col-md-7 col-xs-12' name='educational_competence' id='educational_competence1'></div>"+
            "</div>"+
            "<div class='form-group fg-float'>"+
            "<div class='fg-line'>"+
            "<input data-toggle='tooltip' data-placement='bottom' placeholder='<?php echo $this->lang->line('educational_institution'); ?>' title='Ex: Your school or university name, etc.' type='text' required class='form-control col-md-7 col-xs-12' name='educational_institution' id='educational_institution1'>"+
            "</div>"+
            "<div class='form-group fg-float'>"+
            "<div class='fg-line'>"+
            "<input data-toggle='tooltip' data-placement='bottom' placeholder='<?php echo $this->lang->line('institution_address'); ?>' title='Ex: Jakarta, Bandung, etc.'  type='text' rows='5' required class='form-control col-md-7 col-xs-12' name='institution_address' id='institution_address1'>"+
            "</div>"+
            "<div class='form-group fg-float'>"+
            "<div class='fg-line'>"+
            "<input data-toggle='tooltip' data-placement='bottom' placeholder='<?php echo $this->lang->line('graduation_year'); ?>' title='Ex: 2015, 2016 etc.' type='text' onkeypress='return hanyaAngka(event)' required class='form-control col-md-7 col-xs-12 yearly' minlength='4' maxlength='4' name='graduation_year' id='graduation_year1'>"+
            "</div>";
              document.getElementById(divName).appendChild(newdiv);
              counter++;
         
    }
    function Remove(form_id) {
        if (window.confirm('Remove this data?')) {
            var f = document.getElementById(edukasi_form);
            f.parentNode.removeChild(f);
        }
    }
    </script>
    <style type="text/css">
        @media screen  (min-width : 1000px) {
        #modal_approv{
            margin-top: 75%;
        }
    }
</style>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/sidetutor'); ?>
    </aside>


    <section id="content">
        <div class="container">
            <div class=" modal fade" data-modal-color="blue" id="approval_modal" tabindex="-1" data-backdrop="static" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                            <center>
                                <div id='modal_approv' class="modal-body" style="margin-top: 75%;" ><br>
                                   <div class="alert alert-info" style="display: block; ?>" id="alertapprovaltutor">
                                        <?php echo $this->lang->line('completetutorapproval');?>
                                    </div>
                                </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
            <div class="modal fade" data-modal-color="red"  data-backdrop="static" id="modal_foto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-body" style="margin-top: 75%;" > 
                            <br>
                            <div class="alert alert-danger" style="display: none; ?>" id="alert_ukuran"><center>Maksimal file 2 MB,<br>Ukuran foto harap 512 x 512 pixels atau lebih</center>
                            </div>
                            <div class="alert alert-danger" style="display: none; ?>" id="alert_wajah"><center><?php echo $this->lang->line('fotowajah'); ?></center>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" data-modal-color="red"  data-backdrop="static" id="valid_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                            <div class="modal-body" style="margin-top: 75%;" > 
                            <center><br>
                             <div class="alert alert-danger" style="display: block; ?>" id="">
                                 Please complete your data
                            </div>
                                  
                            </div>
                             <div class="modal-footer">
                                <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button></center>
                            </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" data-modal-color="red"  data-backdrop="static" id="nointernet_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                            <div class="modal-body" style="margin-top: 75%;" > 
                            <center><br>
                             <div class="alert alert-danger" style="display: block; ?>" id="">
                                 fail to connect, please check your connection settings!!!
                            </div>
                                  
                            </div>
                             <div class="modal-footer">
                                <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button></center>
                            </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" data-modal-color="teal"  data-backdrop="static" id="modal_submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                            <div class="modal-body" style="margin-top: 75%;" > 
                            <br>
                             <div class="" style="display: block; ?>" id="">
                                 <?php echo $this->lang->line('requestsent'); ?>
                            </div>
                                  
                            </div>
                             <div class="modal-footer">
                                <button id="btn_submit_ok" type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="x_panel">
                        <div class="x_title">
                            <h3><?php echo $this->lang->line('approval'); ?><small class="hidden m-l-10">Tutor</small></h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content" >
                            <!-- Smart Wizard -->                        
                            <div id="wizard" class="form_wizard wizard_horizontal">
                                <ul class="hidden-xs wizard_steps">
                                    <li>
                                        <a href="#step-1">
                                            <span class="step_no">1</span>
                                            <span class="step_descr">
                                                <?php echo $this->lang->line('step'); ?> 1<br />
                                                <small><?php echo $this->lang->line('account');?></small>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-2">
                                            <span class="step_no">2</span>
                                            <span class="step_descr">
                                                <?php echo $this->lang->line('step'); ?> 2<br />
                                                <small><?php echo $this->lang->line('profil');?></small>
                                            </span>
                                        </a>
                                    </li>                 
                                </ul>
                                <div id="step-1">
                                    <?php 
                                    $id_user = $this->session->userdata('id_user');
                                    $v = $this->db->query("SELECT ts. * , tpt. * 
                                        FROM tbl_profile_tutor AS tpt
                                        INNER JOIN tbl_user AS ts ON tpt.tutor_id = ts.id_user
                                        WHERE ts.id_user =  '$id_user'")->row_array();
                                       $first_name = $v['first_name'];
                                       $last_name = $v['last_name'];
                                       $email_addr = $v['email'];
                                       $user_ktp = $v['user_ktp'];
                                       $user_birthplace = $v['user_birthplace'];
                                       $user_birthdate = $v['user_birthdate'];
                                       $user_callnum = $v['user_callnum'];
                                       $user_gender = $v['user_gender'];
                                       $user_religion = $v['user_religion'];
                                       $user_nationality = $v['user_nationality'];
                                       $id_provinsi = $v['id_provinsi'];
                                       $id_kabupaten = $v['id_kabupaten'];
                                       $id_kecamatan = $v['id_kecamatan'];
                                       $id_desa = $v['id_desa'];
                                       $kodepos = $v['kodepos'];
                                       $user_address = $v['user_address'];
                                       $user_city = $v['user_city'];
                                       $user_distrik = $v['user_distrik'];
                                       $user_subdistrik = $v['user_subdistrik'];
                                       ;

                                       if ($user_nationality !="Indonesia"){
                                        ?>
                                            <style type="text/css">
                                            .indo{
                                            display:none;
                                            }
                                            .non_indo{
                                            display:inline;
                                            }
                                            </style>
                                        <?php
                                       }
                                      else{
                                        ?>
                                            <style type="text/css">
                                            .indo{
                                            display:inline;
                                            }.non_indo{
                                            display:none;
                                            }</style>
                                        <?php
                                        }
                                    ?>

                                    <form class="form-horizontal" id="approvalForm_account" method="POST" action="<?php echo base_url('master/tutor_submitApproval/'.$this->session->userdata('id_user')); ?>">    
                                    
                                        <div style="" class="col-sm-2">                                         
                                        </div>
                                        <div class=" col-sm-8">
                                            <div class="form-group">
                                                <label style="text-align: left;" class="col-sm-4 control-label"><?php echo $this->lang->line('numberid'); ?> (<?php echo $this->lang->line('selectnumberid'); ?>) </label>
                                                <div class="col-sm-8">
                                                    <div class="dtp-container fg-line m-l-5">                                                
                                                        <input placeholder="<?php echo $this->lang->line('typeyournumberid'); ?>" maxlength="16" type="text" name="user_ktp" id="user_ktp" required class="form-control" value="<?php echo $user_ktp; ?>">
                                                    </div>
                                                    <small id="cek_nomorid" style="color: red;"></small>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label style="text-align: left;" class="col-sm-4 control-label"><?php echo $this->lang->line('placeofbirth'); ?></label>
                                                <div class="col-sm-8">
                                                    <div class="dtp-container fg-line m-l-5">                                                
                                                        <input placeholder="<?php echo $this->lang->line('typeyourbirthplace'); ?>" type="text" name="user_birthplace" id="user_birthplace" required class="form-control" value="<?php echo $user_birthplace; ?>">
                                                    </div>
                                                    <small id="cek_tempat" style="color: red;"></small>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label style="text-align: left;" class="col-sm-4 control-label"><?php echo $this->lang->line('select_religion'); ?></label>
                                                <div class="col-sm-8">
                                                    <?php 
                                                        $flag = $user_religion;
                                                    ?>
                                                    <select style="width: 100%;" name="user_religion" required class="select2 form-control">
                                                        <option disabled selected><?php echo $this->lang->line('chooseyourreligion'); ?></option>
                                                        <option value="Islam" <?php if($flag=="Islam"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Islam'); ?></option>
                                                        <option value="Kristen Protestan" <?php if($flag=="Kristen Protestan"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Protestan'); ?></option>
                                                        <option value="Kristen Katolik" <?php if($flag=="Kristen Katolik"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Katolik'); ?></option>
                                                        <option value="Hindu" <?php if($flag=="Hindu"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Hindu'); ?></option>
                                                        <option value="Buddha" <?php if($flag=="Buddha"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Buddha'); ?></option>
                                                        <option value="Konghucu" <?php if($flag=="Konghucu"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Konghucu'); ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label style="text-align: left;" class="col-sm-4 control-label"><?php echo $this->lang->line('home_address'); ?></label>
                                                <div class="col-sm-8">
                                                    <div class="dtp-container fg-line m-l-5">                                                
                                                        <input placeholder="<?php echo $this->lang->line('typeyouraddress'); ?>" type="text" name="user_address" id="user_address" rows="5" required class="form-control" value="<?php echo $user_address; ?>">
                                                    </div>
                                                    <small id="cek_alamat" style="color: red;"></small>
                                                </div>
                                            </div>
                                            <div class="form-group non_indo">
                                                <label style="text-align: left;" class="col-sm-4 control-label">City</label>
                                                <div class="col-sm-8">
                                                    <div class="dtp-container fg-line m-l-5">                                                
                                                        <input placeholder="Type Your City name here..." type="text" name="user_city" id="user_city" rows="5" required class="form-control" value="<?php echo $user_city; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group non_indo">
                                                <label style="text-align: left;" class="col-sm-4 control-label">District</label>
                                                <div class="col-sm-8">
                                                    <div class="dtp-container fg-line m-l-5">                                                
                                                         <input placeholder="Type Your District name here..." type="text" name="user_distrik" id="user_distrik" rows="5" required class="form-control" value="<?php echo $user_distrik; ?>">    
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group non_indo">
                                                <label style="text-align: left;" class="col-sm-4 control-label">Sub-district</label>
                                                <div class="col-sm-8">
                                                    <div class="dtp-container fg-line m-l-5">                                                
                                                         <input placeholder="Type Your Sub-district name here..." type="text" name="user_subdistrik" id="user_subdistrik" rows="5" required class="form-control" value="<?php echo $user_subdistrik; ?>">     
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group indo">
                                                <label style="text-align: left;" class="col-sm-4 control-label"><?php echo $this->lang->line('selectprovinsii'); ?></label>
                                                <div class="col-sm-8">
                                                    <select style="width: 100%;" id='provinsi_id' class="select2 form-control" name="id_provinsi">
                                                        <option disabled selected><?php echo $this->lang->line('chooseyourpropinsi'); ?></option>
                                                        <option value=""></option>
                                                            <?php
                                                                $prv = $this->Master_model->provinsi($id_provinsi);
                                                                if($prv != ''){
                                                                    echo "<option value='".$prv['provinsi_id']."' selected='true'>".$prv['provinsi_name']."</option>";
                                                                    echo "<script>pfp = '".$prv['provinsi_id']."'</script>";
                                                                }

                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group indo">
                                                <label style="text-align: left;" class="col-sm-4 control-label"><?php echo $this->lang->line('selectkabupatenn'); ?></label>
                                                <div class="col-sm-8">
                                                   <select id='kabupaten_id' name="id_kabupaten" class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkabupatenn'); ?>" >
                                                        <option disabled selected><?php echo $this->lang->line('chooseyourkabupaten'); ?></option>
                                                        <option value=""></option>
                                                        <?php
                                                            $kbp = $this->Master_model->getAllKabupaten($id_kabupaten,$id_provinsi);
                                                            if($kbp != ''){
                                                                echo "<option value='".$kbp['kabupaten_id']."' selected='true'>".$kbp['kabupaten_name']."</option>";
                                                                echo "<script>pfka = '".$kbp['kabupaten_id']."'</script>";
                                                            }

                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group indo">
                                                <label style="text-align: left;" class="col-sm-4 control-label"><?php echo $this->lang->line('selectkecamatann'); ?></label>
                                                <div class="col-sm-8">
                                                   <select  id='kecamatan_id' class="select2 form-control" data-placeholder="<?php echo $this->lang->line('chooseyourkecamatan'); ?>" name="id_kecamatan">
                                                        <option value=''></option>
                                                        <option disabled selected><?php echo $this->lang->line('selectkecamatann'); ?></option>
                                                        <?php
                                                            $kcm = $this->Master_model->getAllKecamatan($id_kecamatan,$id_kabupaten);
                                                            if($kcm != ''){
                                                                echo "<option value='".$kcm['kecamatan_id']."' selected='true'>".$kcm['kecamatan_name']."</option>";
                                                                echo "<script>pfkec = '".$kcm['kecamatan_id']."'</script>";

                                                            }

                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group indo">
                                                <label style="text-align: left;" class="col-sm-4 control-label"><?php echo $this->lang->line('selectkelurahann'); ?></label>
                                                <div class="col-sm-8">
                                                   <select id='desa_id' class="select2 form-control" data-placeholder="<?php echo $this->lang->line('chooseyourkelurahan'); ?>" name="id_desa">
                                                        <option value=''></option>
                                                        <option disabled selected><?php echo $this->lang->line('selectkelurahann'); ?></option>
                                                         <?php
                                                                $des = $this->Master_model->getAllKelurahan($id_desa,$id_kecamatan);
                                                                if($des != ''){
                                                                    echo "<option value='".$des['desa_id']."' selected='true'>".$des['desa_name']."</option>";
                                                                }

                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label style="text-align: left;" class="col-sm-4 control-label"><?php echo $this->lang->line('postalcode'); ?></label>
                                                <div class="col-sm-8">
                                                    <div class="dtp-container fg-line m-l-5">                                                
                                                         <input placeholder="<?php echo $this->lang->line('postalcode'); ?>" onkeypress="return hanyaAngka(event)" type="text" name="kodepos" id="kodepos" rows="5" required class="form-control col-md-7 col-xs-12" value="<?php echo $kodepos; ?>">  
                                                    </div>
                                                    <small id="cek_kodepos" style="color: red;"></small>  
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <input hidden="" value="2" id="cek_foto" type="" name="">
                                                <label style="text-align: left;" class="col-sm-4 control-label"><?php echo $this->lang->line('photo'); ?></label>
                                                <div class="col-sm-8">
                                                    <button style="width: 150px;" type="button" id="unggahFoto" name="unggahFoto" class="btn btn-info active"><?php echo $this->lang->line('photoupload'); ?></button>
                                                    <label class="hidden-xs" style="">&nbsp;&nbsp;<?php echo $this->lang->line('or'); ?>&nbsp;&nbsp;</label>
                                                    <button style=" width: 150px;" type="button" id="gunakanCamera" class="btn btn-default" ><?php echo $this->lang->line('usecamera'); ?></button> 
                                                    <div id="my_camera" class="m-t-15" style="display:block">
                                                    </div>

                                                    <div id="pics_div" style="display: none;" class="m-t-10">
                                                      <button style="width: 157px;" type="button" class="btn btn-info" id="take_pics"><?php echo $this->lang->line('capture'); ?></button>

                                                      <button style="width: 157px;" type="button" class="btn btn-default" disabled="true" id="retake_pics" ><?php echo $this->lang->line('retakecapture'); ?></button>
                                                    </div>

                                                    <div class="m-t-10" id="upload_foto" style="">
                                                        <?php
                                                            $iduser = $this->session->userdata('id_user');
                                                            $get_image = $this->db->query("SELECT * FROM tbl_user WHERE id_user='".$iduser."'")->row_array();
                                                        ?>
                                                        <input type="file"  id="imgfile" name="imgfile" accept="image/*" data-classButton="btn btn-default" data-classInput="form-control inline input-s">
                                                         
                                                        <img src="<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=" class="m-t-10" id="imgprev" src="#" alt="your image" style="width:320px;height:320px;">
                                                        <textarea  id="imgdata"  name="imgdata" hidden></textarea>
                                                    </div> 
                                                </div>
                                            </div>





                                        </div>         
                                                                                                      
                                         <div style="" class="col-sm-3">                                         
                                        </div>                           
                                    </form>
                                </div>
                                <div id="step-2">

                                    <form class="form-horizontal form-label-left" id="approvalForm_profile">
                                        <legend><h4><?php echo $this->lang->line('latbelpend'); ?></h4></legend>
                                        <table class="table" >
                                            <!-- <thead>
                                                <th width="250"><?php echo $this->lang->line('educational_level'); ?></th>
                                                <th><?php echo $this->lang->line('educational_competence'); ?></th>
                                                <th><?php echo $this->lang->line('educational_institution'); ?></th>
                                                <th><?php echo $this->lang->line('institution_address'); ?></th>
                                                <th><?php echo $this->lang->line('graduation_year'); ?></th>
                                            </thead> -->
                                            <tbody id="body_tableaccount">
                                                <tr>
                                                    <td>
                                                        <!-- <input type="text" required class="form-control col-md-7 col-xs-12" name="educational_level" id="educational_level1"> -->
                                                        <select required class="form-control select2 col-md-7 col-xs-12" name="educational_level" id="educational_level1" data-placeholder="<?php echo $this->lang->line('select_level'); ?>">
                                                            <option value=""></option>
                                                        </select>
                                                        <input data-toggle="tooltip" data-placement="bottom" placeholder="  <?php echo $this->lang->line('educational_competence'); ?>" title="Ex: Computer Science, Biology, etc." type="text" required class="form-control col-md-7 col-xs-12" name="educational_competence" id="educational_competence1">
                                                        <br>
                                                        <input data-toggle="tooltip" data-placement="bottom" placeholder="  <?php echo $this->lang->line('educational_institution'); ?>" title="Ex: Your school or university name, etc." type="text" required class="form-control col-md-7 col-xs-12" name="educational_institution" id="educational_institution1">
                                                        <br>
                                                        <input data-toggle="tooltip" data-placement="bottom" placeholder="  <?php echo $this->lang->line('institution_address'); ?>" title="Ex: Jakarta, Bandung, etc." type="text" rows="5" required class="form-control col-md-7 col-xs-12" name="institution_address" id="institution_address1">
                                                        <br>
                                                        <input data-toggle="tooltip" data-placement="bottom" placeholder="  <?php echo $this->lang->line('graduation_year'); ?>" title="Ex: 2015, 2016 etc." type='text' required class="form-control col-md-7 col-xs-12 yearly" onkeypress="return hanyaAngka(event)" minlength="4" maxlength="4" name="graduation_year" id="graduation_year1">
                                                        <input type="checkbox" id="check_graduated" value="oke" name="check_graduated"> <?php echo $this->lang->line('not_graduated'); ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <th><button class="btn btn-primary" type="button" id="btn_appendAccount"><?php echo $this->lang->line('tambahdata'); ?></button> <button class="btn btn-danger" type="button" id="btn_eraseAccount"><?php echo $this->lang->line('hapusdata'); ?></button></th>
                                            </tfoot>
                                        </table><br><br><br>
                                        <legend><h4><?php echo $this->lang->line('pengalaman_mengajar'); ?></h4></legend>
                                        <table class="table ">
                                            <!-- <thead>
                                                <th><?php echo $this->lang->line('description_experience'); ?></th>
                                                <th><?php echo $this->lang->line('year_experience'); ?></th>
                                                <th><?php echo $this->lang->line('place_experience'); ?></th>
                                                <th><?php echo $this->lang->line('information_experience'); ?></th>
                                            </thead> -->
                                            <tbody id="body_tableprofile">
                                                <tr>
                                                    <td>
                                                        <input data-toggle="tooltip" data-placement="bottom" title="Ex: Teaching SD etc." placeholder="  <?php echo $this->lang->line('description_experience'); ?>" type="text" required class="form-control col-md-7 col-xs-12" name="description_experience" id="description_experience1">  
                                                        <br><br>
                                                        <div style="">
                                                            <div><label class="m-t-5"><?php echo $this->lang->line('year_experience'); ?></th></label>  </div>                                                                                                          
                                                            <div class="col-md-3" >

                                                                <input data-toggle="tooltip" minlength="4" maxlength="4" data-placement="bottom" title="Ex: 2014." type='text' onkeypress="return hanyaAngka(event)" required class="form-control col-md-7 col-xs-12 yearlyy cek_error1" placeholder="<?php echo $this->lang->line('from'); ?>" name="year_experience1" id="year_experience1">
                                                            </div>                                                        
                                                            <div class="col-md-3">
                                                                <input data-toggle="tooltip" minlength="4" maxlength="4" data-placement="bottom" title="Ex: 2016." type='text' onkeypress="return hanyaAngka(event)" required class="form-control col-md-7 col-xs-12 yearlyy cek_error2" placeholder="<?php echo $this->lang->line('to'); ?>" name="year_experience2" id="year_experience2">
                                                            </div>

                                                            <br><br>
                                                        </div>

                                                        <input data-toggle="tooltip" data-placement="bottom" title="Ex: Jakarta, Surabaya, etc." placeholder="  <?php echo $this->lang->line('place_experience'); ?>" type="text" rows="5" required class="form-control col-md-7 col-xs-12" name="place_experience" id="place_experience1">
                                                        <br><br>
                                                        <input data-toggle="tooltip" data-placement="bottom" title="Ex: Pass." type="text" placeholder="  <?php echo $this->lang->line('information_experience'); ?>" rows="5" required class="form-control col-md-7 col-xs-12" name="information_experience" id="information_experience1">
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                            <th>
                                                <button class="btn btn-primary" type="button" id="btn_appendProfile"><?php echo $this->lang->line('tambahdata'); ?></button>
                                                <button class="btn btn-danger" type="button" id="btn_eraseProfile"><?php echo $this->lang->line('hapusdata'); ?></button></th>
                                            </tfoot>
                                        </table>
                                        <br><br><br>
                                    </form>
                                  <!-- <div class="m-t-20">
                                        <button class="btn bgm-teal btn-block" type="submit" id="submiit_sementara">Lewati Menu Ini</button><br><br><br>
                                    </div> --> 
                                    <div class="modal fade" style="cursor: progress;" id="modal_loading" tabindex="-1" data-backdrop="static" role="dialog">
                                        <div class="modal-dialog" role="document" style="background:rgba(0,00,00,0.0);">   
                                            <div style="margin-top: 30%;">
                                                <center>
                                                   <img style="width: 20%" src="https://www.silver-peak.com/sites/all/themes/silverpeak/images/loader.gif"><br>
                                                   <label style="color: white;">Loading...</label>
                                                </center>
                                            </div>
                                        </div>
                                    </div> 
                                </div>                        

                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- akhir container -->
    </section>            
</section>

    <footer id="footer">
        <?php $this->load->view('inc/footer'); ?>
    </footer>

<script type="text/javascript">
    $(".cek_error2").on('keyup',function(){
        if(parseFloat($(".cek_error1").val()) > parseFloat($(".cek_error2").val())){
            $(".cek_error2").css("color","red");
            // $('.buttonFinish').addClass('disabled'); 
        }
        else {
           $(".cek_error2").css("color","black");
           // $('.buttonFinish').removeClass('disabled');  
        }
    });

    $(".cek_error1").on('keyup',function(){
    if(parseFloat($(".cek_error1").val()) > parseFloat($(".cek_error2").val()))
    {
        $(".cek_error2").css("color","red");
        // $('.buttonFinish').addClass('disabled');
        
    }
    else {
       $(".cek_error2").css("color","black");
       // $('.buttonFinish').removeClass('disabled');
       
    }
    });
    $('yearlyy').on('keypress',function(e){
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
    });
</script>
<script type="text/javascript">
      $("#submiit_sementara").click(function(){
        // $('#approvalForm_account').submit();
        var o = {};
        var a = $('#approvalForm_account').serializeArray();
        $.each(a, function() {
          if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
              o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
          } else {
            o[this.name] = this.value || '';
          }
        });
        var y = {};
        var b = $('#approvalForm_profile').serializeArray();
        $.each(b, function() {
          if (y[this.name] !== undefined) {
            if (!y[this.name].push) {
              y[this.name] = [y[this.name]];
            }
            y[this.name].push(this.value || '');
          } else {
            y[this.name] = this.value || '';
          }
        });
        $.ajax({
          url: '<?php echo base_url(); ?>master/tutor_tempApproval/<?php echo $this->session->userdata('id_user'); ?>',
          type: 'POST',
          data: {
            'form_account': JSON.stringify(o),
            'form_profile': JSON.stringify(y)
          },
          success: function(hasil){
            location.href='<?php echo base_url(); ?>tutor';
          }
        });
      });
</script>
<script type="text/javascript">
    $(document).ready(function(){

        var lang_from = '<?php echo $this->lang->line('from'); ?>';
        var lang_to = '<?php echo $this->lang->line('to'); ?>';
        var total_bodyAccount = 0;
        var data_tambahan = 0;
        var delete_btn = document.getElementById('btn_eraseAccount');

        $('#check_graduated').bind('change', function (v) {

                if($(this).is(':checked')) {
                    $("#graduation_year1").css('display','none');
                    $("#graduation_year1").val('0');

                } else {
                    $("#graduation_year1").css('display','inline');
                    $("#graduation_year1").val('');
                }
            });

        $('#btn_appendAccount').click(function(){

            total_bodyAccount++;
            data_tambahan++;
            $('#body_tableaccount').append('<tr id="tr_bodyaccount_'+total_bodyAccount+'">'+
                // '<td><input type="text" required class="form-control col-md-7 col-xs-12" name="educational_level"></td>'+
                '<td><h4>Data Tambahan ( '+data_tambahan+' )</h4><br><select required class="form-control select2 col-md-7 col-xs-12" name="educational_level" data-placeholder="<?php echo $this->lang->line('select_level'); ?>">'+
                '<option value=""></option>'+
                '</select>'+
                '<input type="text" placeholder="  <?php echo $this->lang->line('educational_competence'); ?>" required class="form-control col-md-7 col-xs-12" name="educational_competence">'+
                '<input type="text" placeholder="  <?php echo $this->lang->line('educational_institution'); ?>" required class="form-control col-md-7 col-xs-12" name="educational_institution">'+
                '<input type="text" placeholder="  <?php echo $this->lang->line('institution_address'); ?>" rows="5" required class="form-control col-md-7 col-xs-12" name="institution_address">'+
                '<input type="text" placeholder="<?php echo $this->lang->line('graduation_year'); ?>" required class="form-control date-picker col-md-7 col-xs-12" onkeypress="return hanyaAngka(event)" data-date-format="YYYY" name="graduation_year" id="graduation_'+total_bodyAccount+'">'+
                '<input type="checkbox" id="check_graduated'+total_bodyAccount+'" value="oke" name="check_graduated"> <?php echo $this->lang->line('not_graduated'); ?></td>'+
                '</tr>');

            $('#check_graduated'+total_bodyAccount+'').bind('change', function (v) {

                if($(this).is(':checked')) {
                    $("#graduation_"+total_bodyAccount).css('display','none');
                    $("#graduation_"+total_bodyAccount).val('0');

                } else {
                    $("#graduation_"+total_bodyAccount).css('display','inline');
                    $("#graduation_"+total_bodyAccount).val('0');
                }
            });


            $('select.select2').each(function(){
                if($(this).attr('name') == "educational_level"){
                    $(this).select2({
                          delay: 2000,
                          ajax: {
                              dataType: 'json',
                              type: 'GET',
                              url: '<?php echo base_url(); ?>ajaxer/getJenjangLevel',
                              data: function (params) {
                                  return {
                                      term: params.term,
                                      page: params.page || 1
                                  };
                              },
                              processResults: function(data){
                                return {
                                  results: data.results,
                                  pagination: {
                                    more: data.more
                                }
                            };
                        }
                    }
                });    
                }
                
            });

        });
        $('#btn_eraseAccount').click(function(){
            if(total_bodyAccount>0){
                $('#tr_bodyaccount_'+total_bodyAccount).remove();
                total_bodyAccount--;
                data_tambahan--;    
            }

        });
        var total_bodyProfile = 0;
        $('#btn_appendProfile').click(function(){
            total_bodyProfile++;
           $('#body_tableprofile').append('<tr id="tr_bodyprofile_'+total_bodyProfile+'">'+
                '<td><h4>Pengalaman Lain ( '+total_bodyProfile+' )</h4><input type="text" placeholder="<?php echo $this->lang->line('description_experience'); ?>" required class="form-control col-md-7 col-xs-12" name="description_experience">'+
                '<div>'+
                '<div><label class="m-t-5"><?php echo $this->lang->line('year_experience'); ?></th></label> </div>'+
                '<div class="col-md-3">'+
                '<input type="text"  required class="form-control date-picker col-md-7 col-xs-12 yearly  " id="cek_error1_'+total_bodyProfile+'" data-date-format="YYYY" onkeypress="return hanyaAngka(event)" placeholder="<?php echo $this->lang->line('from'); ?>" name="year_experience1">'+
                '</div>'+
                '<div class="col-md-3">'+
                '<input type="text"   required class="form-control date-picker col-md-7 col-xs-12 yearly " id="cek_error2_'+total_bodyProfile+'" data-date-format="YYYY" onkeypress="return hanyaAngka(event)" placeholder="<?php echo $this->lang->line('to'); ?>" name="year_experience2">'+
                '</div></div>'+
                '<input type="text" rows="5" placeholder="<?php echo $this->lang->line('place_experience'); ?>" required class=" form-control col-md-7 col-xs-12" name="place_experience">'+
                '<input type="text" rows="5" placeholder="<?php echo $this->lang->line('information_experience'); ?>" required class="form-control col-md-7 col-xs-12" name="information_experience"></td>'+
                '</tr>');
        });
        // imginstance.setSelection(0, 0, 512, 512, true);
        $('#btn_eraseProfile').click(function(){
            if(total_bodyProfile>0){
                $('#tr_bodyprofile_'+total_bodyProfile).remove();
                total_bodyProfile--;
            }

        });

        $(document).on("keypress", ".yearly", function() {
            $('#cek_error1_'+total_bodyProfile).on('keypress',function(e){
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        });

 /*       var imginstance = $('img#imgprev').imgAreaSelect({
            handles: true,
            instance: true,
            aspectRatio: '1:1',
            x1: 0,
            y1: 0,
            x2: 250,
            y2: 250,
            onSelectEnd: someFunction
        });
        function someFunction(img,selection) {
            alert(img);
        }*/

    });

    Webcam.set({
        width: 320,
        height: 240,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
</script>
<script type="text/javascript">
       $(document).ready(function(){
            var code = null;
            var cekk = setInterval(function(){
                code = "<?php echo $this->session->userdata('code');?>";
                cek();
            },500);
            var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
            function cek(){
                if (code == "291") {
                    $("#approval_modal").modal('show');
                    code == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>Rest/clearsession/access_token/'+tokenjwt,
                        type: 'POST',
                        data: {
                            code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
                console.warn(code);
            }
        });           
</script>