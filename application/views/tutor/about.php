<!-- <header id="header" class="clearfix" data-current-skin="blue"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">

        <?php $this->load->view('./inc/sidetutor'); ?>
    </aside>

    <section id="content">
        <div class="container">

            <div class="block-header">
                <h2><?php echo $this->lang->line('profil'); ?></h2>

                <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_about'); ?></li>
                        </ol>
                    </li>
                </ul>
            </div><!-- akhir block header -->     
           
            <div class="alert alert-success" style="display: none; ?>" id="alertsuccessupdate">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                Foto akun berhasil diubah
            </div>
            <div class="alert alert-danger" style="display: none; ?>" id="alertsizebesar">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                Upload gagal, Mohon ukuran file tidak melebihi 2 MB.
            </div>       
            <div class="bs-item z-depth-5" style="margin-top: 5%;">
                <div class="card" id="profile-main">

                    <?php
                    $this->load->view('./inc/sideprofile');
                    ?>

                    <div class="pm-body clearfix">
                        <ul class="tab-nav tn-justified" role="tablist">
                            <li class="active waves-effect"><a href="<?php echo base_url('tutor/about'); ?>"><?php echo $this->lang->line('tab_about'); ?></a></li>
                            <li class="waves-effect"><a href="<?php echo base_url('tutor/profile_account'); ?>"><?php echo $this->lang->line('tab_account'); ?></a></li> 
                            <li class="waves-effect"><a href="<?php echo base_url('tutor/profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li>
                            <!-- <li class="waves-effect"><a href="<?php echo base_url('tutor/profile_forgot'); ?>"><?php echo $this->lang->line('tab_cp'); ?></a></li> -->                                      
                        </ul>                            

                        <div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-equalizer m-r-5"></i> <?php echo $this->lang->line('summary'); ?></h2>
                            </div>
                            <div class="pmbb-body p-l-30">
                                <!-- Disini Bio tutor -->
                                <div class="pmbb-view">
                                    <?php echo $this->db->query("SELECT self_description FROM tbl_profile_tutor WHERE tutor_id='".$this->session->userdata('id_user')."'")->row_array()['self_description']; ?>
                                </div>
                            </div>
                        </div>

                        <div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('basic_info'); ?></h2>
                            </div>

                            <div class="pmbb-body p-l-30">
                                <div class="pmbb-view">
                                    <dl class="dl-horizontal">
                                        <dt><?php echo $this->lang->line('full_name'); ?></dt>
                                        <dd>
                                            <?php 
                                                if ($profile['user_name'] == "") {
                                                    echo $this->lang->line('nousername');
                                                }
                                                echo $profile['user_name']; 
                                            ?>  
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt><?php echo $this->lang->line('gender'); ?></dt>
                                        <dd>
                                            
                                            <?php 
                                                if ($this->session->userdata('jenis_kelamin') == "") {
                                                    echo $this->lang->line('nogender');
                                                }
                                                if ($this->session->userdata('jenis_kelamin') == "Male"){
                                                    echo $this->lang->line('male');
                                                }
                                                if($this->session->userdata('jenis_kelamin') == "Female"){
                                                    echo $this->lang->line('women');
                                                }
                                            ?>  
                                        </dd>

                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt><?php echo $this->lang->line('birthday'); ?></dt>
                                        <dd>
                                            <?php 
                                                if ($this->session->userdata('tanggal_lahir') == "") {
                                                    echo $this->lang->line('nobirthday');
                                                }
                                                echo $this->session->userdata('tanggal_lahir'); 
                                            ?> 
                                        </dd>
                                    </dl> 
                                    <dl class="dl-horizontal">
                                        <dt><?php echo $this->lang->line('age'); ?></dt>
                                        <dd>
                                            <?php 
                                                if ($this->session->userdata('umur') == "") {
                                                    echo $this->lang->line('age');
                                                }
                                                echo $this->session->userdata('umur').' '.$this->lang->line('yearsold'); 
                                            ?> 
                                        </dd>
                                    </dl>                               
                                </div>                         
                            </div>
                        </div>

                        <div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-phone m-r-5"></i> <?php echo $this->lang->line('contact_info'); ?></h2>
                            </div>
                            <div class="pmbb-body p-l-30">
                                <div class="pmbb-view">
                                    <dl class="dl-horizontal">
                                        <dt><?php echo $this->lang->line('mobile_phone'); ?></dt>
                                        <dd>
                                            <?php 
                                                if ($this->session->userdata('no_hp') == "") {
                                                    echo $this->lang->line('nonumber');
                                                }
                                                echo "+".$this->session->userdata('kode_area').$this->session->userdata('no_hp'); 
                                            ?> 
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt><?php echo $this->lang->line('email_address'); ?></dt>
                                        <dd>
                                            <?php 
                                                if ($this->session->userdata('email') == "") {
                                                    echo $this->lang->line('noemail');
                                                }
                                                echo $this->session->userdata('email'); 
                                            ?> 
                                        </dd>
                                    </dl>
                                <!-- <dl class="dl-horizontal">
                                    <dt>Twitter</dt>
                                    <dd>@malinda</dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Skype</dt>
                                    <dd>malinda.hollaway</dd>
                                </dl> -->
                            </div>

                            
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div><!-- akhir container -->
</section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    var code = null;
        var cekk = setInterval(function(){
            code = "<?php echo $this->session->userdata('code');?>";
            cek();
        },500);
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        function cek(){
            if (code == "4044") {
                $("#alertsizebesar").css('display','none');
                $("#alertsuccessupdate").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url(); ?>Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
            else if (code == "4045") {
                $("#alertsuccessupdate").css('display','none');
                $("#alertsizebesar").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url(); ?>Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
        }
</script>