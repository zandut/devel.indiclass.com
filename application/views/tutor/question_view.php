<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2>Question View</h2>				
			</div> <!-- akhir block header    -->        
			<br>			

            <?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>

			<?php
        		if (!isset($_GET['bsid'])) {
        			redirect('/');
        		}
        		else
        		{
        			$bsid 			= $_GET['bsid'];
					$tutor_id		= $this->session->userdata('id_user');
					$select 		= $this->db->query("SELECT mbs.name, ms.subject_name, mj.jenjang_name, mj.jenjang_level, mbs.description, mbs.template_type FROM master_banksoal as mbs INNER JOIN master_subject as ms ON mbs.subject_id=ms.subject_id INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE mbs.bsid='$bsid' AND mbs.tutor_id='$tutor_id'")->row_array();
			?>

			<div class="card">
                <div class="card-header ch-alt text-center">
                    <h2><?php echo $select['name'].', '.$select['subject_name'].' '.$select['jenjang_name'].'-'.$select['jenjang_level']; ?></h2>
                </div>
                
                <div class="card-body card-padding">     
                    <div class="clearfix"></div>
                    
                    <div class="row p-10" style="word-wrap: break-word;">
                    	<p><?php echo $select['description'];?></p>					                      
                    </div>
                                        
                </div>                                
            </div>
            <?php 
	        }
	        ?>

	        <?php 
	        	$bsidd 		= $_GET['bsid'];
	        	$tutor_idd	= $this->session->userdata('id_user');
	        	$select = $this->db->query("SELECT * FROM master_banksoal WHERE bsid='$bsidd' AND tutor_id='$tutor_idd'")->row_array();
	        	$listtt = $select['list_soal'];
	        	$jlist = json_decode($listtt, true);	        	
	        ?>
			<!-- disini untuk dashboard murid -->               
			<div class="card" style="margin-top: 2%;">
				<div class="card-header">
					<div class="pull-left"><h2>List Question</h2></div>
					<div class="pull-right">
						<button class="btn btn-success" data-toggle="modal" data-target="#modaladdsubject"><i class="zmdi zmdi-plus"></i> Add Quiz from archive</button>
					</div>                    
				</div>
				<div class="row" style="margin-top: 2%;">
					<div class="card-body card-padding">
						<div class="table-responsive">							
							<table id="" class="display table table-hover table-bordered data" cellspacing="0" width="100%">
						        <thead>
						        	<?php
						        		if (!isset($_GET['bsid'])) {
						        			redirect('/');
						        		}
						        		else
						        		{
						        			$bsid 			= $_GET['bsid'];
											$tutor_id		= $this->session->userdata('id_user');
											// $select_all 	= $this->db->query("SELECT mbs.name, mbs.description, mbs.template_type, mbd.* FROM master_banksoal as mbs INNER JOIN master_banksoal_detailTEMP mbd ON mbs.bsid=mbd.bsid WHERE mbd.bsid='$bsid' AND mbs.tutor_id='$tutor_id'")->result_array();
									?>
						            <tr valign="middle">
						                <th rowspan="2" width="5%"><center>#</center></th>
						                <th colspan="3" width="90%"><center>Quiz</center></th>						                
						                <th rowspan="2" width="5%" style="border-right: 1px solid #eeeeee;"><center>Options</center></th>
						            </tr>
						            <tr>
						                <th width="10%"><center>Question</center></th>
						                <th width="20%"><center>Option</center></th>
						                <th width="20%" style="border-right: 1px solid #eeeeee;"><center>Answer</center></th>
						            </tr>
						        </thead>
						        <tbody>
						        	<?php
									$no=1;
									foreach ($jlist as $row => $v) {
										$getdet 		= $this->db->query("SELECT * FROM master_banksoal_archive WHERE soal_uid='$v'")->row_array();
										$soal_uid 		= $getdet['soal_uid'];
										$question 		= $getdet['text_soal'];
										$options 		= $getdet['options'];
										$answer_value 	= $getdet['answer_value'];
										$options_type 	= $getdet['options_type'];
										$j = 1;
										
										?>										
							            <tr align="center">
							                <td><?php echo($no); ?></td>
							                <td style="word-wrap: break-word;"><?php echo $question; ?></td>
							                <td id="optionnnn">
							                	<?php 
							                		$obj = json_decode($options, true);
							                		if ($options_type == "images") {								                			
							                			?>
							                			<table class="table bgm-gray table-bordered">
							                                <thead>
							                                    <tr>
							                                    	<th>#</th>								                                        
							                                        <th>Choices</th>
							                                        <th>Correct</th>								                                        
							                                    </tr>
							                                </thead>
							                                <tbody>
							                                	<?php
							                                	$no_image = 1;	
							                                	if (is_array($obj)) {					                			
											                		foreach ($obj as $key => $value) {
											                			$imguri = $value['imguri'];
											                			$choices_options = $value['value'];
											                			$answer_checked = "";
											                			if($answer_value == $choices_options){
												                			 $answer_checked = "checked";
												                		}
												                		?>
									                                    <tr>		
									                                    	<td><?php echo($no_image); ?></td>						                                        
									                                        <td><img src='<?php echo $imguri;?>' style='width:150px; height:150px;'/></td>
									                                        <td>
									                                        	<div class="checkbox m-b-15">
																	                <label>
																	                    <!-- <input type="radio" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?>> -->
																	                    <input type="checkbox" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?> disabled>
																	                    <i class="input-helper"></i>
																	                </label>
																	            </div>
																	       	</td>								                                        
									                                    </tr>
									                                    <?php
									                                    $no_image++;
									                                }
									                            }
									                            else
									                            {
									                            	?>
									                            	<tr>
									                					<td>-</td>
									                					<td>-</td>
									                					<td>-</td>
									                				</tr>
									                            	<?php
									                            }
									                            ?>
							                                </tbody>
							                            </table>
							                			<?php	
							                		}
							                		else
							                		{							                			
							                			?>
							                			<table class="table bgm-gray table-bordered">
							                                <thead>
							                                    <tr>	
							                                    	<th>#</th>
							                                        <th>Choices</th>
							                                        <th>Correct</th>								                                        
							                                    </tr>
							                                </thead>
							                                <tbody>
							                                	<?php
							                                	$no_text = 1;
							                                	if (is_array($obj)) {
										                			foreach ($obj as $key => $value) {
											                			$text = $value['value'];
											                			$answer_value_text = "";
											                			if($answer_value == $text){
												                			 $answer_value_text = "checked";
												                		}
												                	?>
								                                    <tr>
								                                    	<td><?php echo($no_text); ?></td>								                                        
								                                        <td><label class="lead f-16"><?php echo $value['value'];?></label></td>
								                                        <td>
								                                        	<div class="checkbox m-b-15">
																                <label>
																                    <!-- <input type="radio" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?>> -->
																                    <input type="checkbox" name="an_<?php echo $soal_uid;?>" <?php echo $answer_value_text;?> disabled>
																                    <i class="input-helper"></i>
																                </label>
																            </div>
																       	</td>								                                        
								                                    </tr>
								                                    <?php
								                                    $no_text++;
									                				}
									                			}
									                			else
									                			{
									                				?>
									                				<tr>
									                					<td>-</td>
									                					<td>-</td>
									                					<td>-</td>
									                				</tr>
									                				<?php
									                			}
								                				?>
							                                </tbody>
							                            </table>
							                			<?php							                			
							                		}
							                	?>
							                </td>
							                <td><?php echo $answer_value; ?></td>
							                <td>
							                	<!-- <a href="#"><button class="btn bgm-green btn-block" title="Edit Quiz"><i class="zmdi zmdi-edit"></i> Edit Question</button></a>
							                	<br><br> -->
							                	<a uid="<?php echo $v;?>" bsid="<?php echo $bsid;?>" class="confrimDelete"><button class="btn bgm-red btn-block" title="Delete Quiz"><i class="zmdi zmdi-delete"></i> Delete</button></a>
                                                                                                
							                </td>							                
							            </tr>
							            <?php
							            $no++;
							        }
							        }
							        ?>
						        </tbody>
						    </table>

						</div>
					</div>
				</div>
			</div>

			<!-- Modal TAMBAH -->  
            <div class="modal" id="modaladdsubject" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                            <h4 class="modal-title c-white">List Question</h4>
                        </div>
                        <div class="modal-body m-t-20" id="topModal">
                            <div class="alert alert-success" role="alert" id="alertsuccessAdd" style="display: none;"></div>
                            <div class="alert alert-warning" role="alert" id="alertwarningAdd" style="display: none;"></div>
                            <div class="alert alert-danger" role="alert" id="alertfailedAdd" style="display: none;"></div>

                            <div class="card-body">
                            	<div role="tabpanel">
                                <ul class="tab-nav" role="tablist">
                                    <li class="active"><a href="#home11" aria-controls="home11" role="tab" data-toggle="tab" aria-expanded="true">My Question</a></li>
                                    <li class=""><a href="#profile11" aria-controls="profile11" role="tab" data-toggle="tab" aria-expanded="false">Other Archive Question</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home11">
	                                    <div class="row">
	                                        <div class="table-responsive">
	                                        	<table class="table table-bordered table-hover" cellspacing="0" width="100%" id="mylistquestion">
					                                <thead>
					                                	<?php
															$tutor_id		= $this->session->userdata('id_user');
															$select_allmy 	= $this->db->query("SELECT * FROM master_banksoal_archive WHERE tutor_creator='$tutor_id'")->result_array();
														?>
					                                    <tr valign="middle">
											                <th rowspan="2" width="5%"><center>#</center></th>
											                <th colspan="3" width="50%"><center>Quiz</center></th>						                
											                <th rowspan="2" width="10%" style="border-right: 1px solid #eeeeee;"><center>Options</center></th>										                
											            </tr>
											            <tr>
											                <th width="10%"><center>Question</center></th>
											                <th width="20%"><center>Option</center></th>
											                <th width="20%" style="border-right: 1px solid #eeeeee;"><center>Answer</center></th>
											            </tr>
					                                </thead>
					                                <tbody>
					                                	<?php
														$no=1;
														foreach ($select_allmy as $row => $v) {
															// if($row>0)break;
															$soal_uid 		= $v['soal_uid'];
															$question 		= $v['text_soal'];
															$options 		= $v['options'];
															$answer_value 	= $v['answer_value'].$soal_uid;
															$options_type 	= $v['options_type'];										
															$j = 1;
															?>
						                                	<tr>
						                                		<td><?php echo($no); ?></td>
												                <td><?php echo $question; ?></td>
												                <td id="optionnnn">
												                	<?php 
												                		if (is_array($options)) {
													                		$obj = json_decode($options, true);
													                		if ($options_type == "images") {								                			
													                			?>
													                			<table class="table bgm-gray table-bordered">
													                                <thead>
													                                    <tr>
													                                    	<th>#</th>								                                        
													                                        <th>Choices</th>
													                                        <th>Correct</th>								                                        
													                                    </tr>
													                                </thead>
													                                <tbody>
													                                	<?php
													                                	$no_image = 1;						                			
																                		foreach ($obj as $key => $value) {
																                			$imguri = $value['imguri'];
																                			$choices_options = $value['value'].$soal_uid;
																                			$answer_checked = "";
																                			if($answer_value == $choices_options){
																	                			 $answer_checked = "checked";
																	                		}
																	                		?>
														                                    <tr>		
														                                    	<td><?php echo($no_image); ?></td>						                                        
														                                        <td><img src='<?php echo $imguri;?>' style='width:100px; height:100px;'/></td>
														                                        <td>
														                                        	<div class="checkbox m-b-15">
																						                <label>
																						                    <!-- <input type="radio" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?>> -->
																						                    <input type="checkbox" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?> disabled>
																						                    <i class="input-helper"></i>
																						                </label>
																						            </div>
																						       	</td>								                                        
														                                    </tr>
														                                    <?php
														                                    $no_image++;
														                                }
														                                ?>
													                                </tbody>
													                            </table>
													                			<?php	
													                		}
													                		else
													                		{							                			
													                			?>
													                			<table class="table bgm-gray table-bordered">
													                                <thead>
													                                    <tr>	
													                                    	<th>#</th>
													                                        <th>Choices</th>
													                                        <th>Correct</th>								                                        
													                                    </tr>
													                                </thead>
													                                <tbody>
													                                	<?php
													                                	$no_text = 1;
															                			foreach ($obj as $key => $value) {
																                			$text = $value['value'].$soal_uid;
																                			$answer_value_text = "";
																                			if($answer_value == $text){
																	                			 $answer_value_text = "checked";
																	                		}
																	                	?>
													                                    <tr>
													                                    	<td><?php echo($no_text); ?></td>								                                        
													                                        <td><label class="lead f-16"><?php echo $value['value'];?></label></td>
													                                        <td>
													                                        	<div class="checkbox m-b-15">
																					                <label>
																					                    <!-- <input type="radio" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?>> -->
																					                    <input type="checkbox" name="an_<?php echo $soal_uid;?>" <?php echo $answer_value_text;?> disabled>
																					                    <i class="input-helper"></i>
																					                </label>
																					            </div>
																					       	</td>								                                        
													                                    </tr>
													                                    <?php
													                                    $no_text++;
														                				}
														                				?>
													                                </tbody>
													                            </table>
													                			<?php							                			
													                		}
													                	}
													                	else
													                	{
													                		echo "-";
													                	}
												                	?>
												                </td>
												                <td><?php echo $v['answer_value']; ?></td>	
												                <td><button class="btn btn-default btn-block choose_mysoaluid" data-uidsoal="<?php echo $soal_uid;?>">Pilih</button></td>				                                		
						                                	</tr>
						                                	<?php
						                                	$no++;
						                                	}
						                                ?>
					                                </tbody>
					                            </table>
	                                        </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="profile11">
                                    	<div class="row">
	                                        <table class="table table-bordered table-hover" cellspacing="0" width="100%" id="otherlistquestion">
				                                <thead>
				                                	<?php
														$tutor_id		= $this->session->userdata('id_user');
														$select_allmy 	= $this->db->query("SELECT * FROM master_banksoal_archive WHERE tutor_creator!='$tutor_id' AND flag=1")->result_array();
													?>
				                                    <tr valign="middle">
										                <th rowspan="2" width="5%"><center>#</center></th>
										                <th colspan="3" width="50%"><center>Quiz</center></th>						                
										                <th rowspan="2" width="10%" style="border-right: 1px solid #eeeeee;"><center>Options</center></th>										                
										            </tr>
										            <tr>
										                <th width="10%"><center>Question</center></th>
										                <th width="20%"><center>Option</center></th>
										                <th width="20%" style="border-right: 1px solid #eeeeee;"><center>Answer</center></th>
										            </tr>
				                                </thead>
				                                <tbody>
				                                	<?php
													$no=1;
													foreach ($select_allmy as $row => $v) {
														// if($row>0)break;
														$soal_uid 		= $v['soal_uid'];
														$question 		= $v['text_soal'];
														$options 		= $v['options'];
														$answer_value 	= $v['answer_value'].$soal_uid;
														$options_type 	= $v['options_type'];										
														$j = 1;
														?>
					                                	<tr>
					                                		<td><?php echo($no); ?></td>
											                <td><?php echo $question; ?></td>
											                <td id="optionnnn">
											                	<?php 
											                		if (is_array($options)) {
												                		$obj = json_decode($options, true);
												                		if ($options_type == "images") {								                			
												                			?>
												                			<table class="table bgm-gray table-bordered">
												                                <thead>
												                                    <tr>
												                                    	<th>#</th>								                                        
												                                        <th>Choices</th>
												                                        <th>Correct</th>								                                        
												                                    </tr>
												                                </thead>
												                                <tbody>
												                                	<?php
												                                	$no_image = 1;						                			
															                		foreach ($obj as $key => $value) {
															                			$imguri = $value['imguri'];
															                			$choices_options = $value['value'].$soal_uid;
															                			$answer_checked = "";
															                			if($answer_value == $choices_options){
																                			 $answer_checked = "checked";
																                		}
																                		?>
													                                    <tr>		
													                                    	<td><?php echo($no_image); ?></td>						                                        
													                                        <td><img src='<?php echo $imguri;?>' style='width:100px; height:100px;'/></td>
													                                        <td>
													                                        	<div class="checkbox m-b-15">
																					                <label>
																					                    <!-- <input type="radio" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?>> -->
																					                    <input type="checkbox" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?> disabled>
																					                    <i class="input-helper"></i>
																					                </label>
																					            </div>
																					       	</td>								                                        
													                                    </tr>
													                                    <?php
													                                    $no_image++;
													                                }
													                                ?>
												                                </tbody>
												                            </table>
												                			<?php	
												                		}
												                		else
												                		{							                			
												                			?>
												                			<table class="table bgm-gray table-bordered">
												                                <thead>
												                                    <tr>	
												                                    	<th>#</th>
												                                        <th>Choices</th>
												                                        <th>Correct</th>								                                        
												                                    </tr>
												                                </thead>
												                                <tbody>
												                                	<?php
												                                	$no_text = 1;
														                			foreach ($obj as $key => $value) {
															                			$text = $value['value'].$soal_uid;
															                			$answer_value_text = "";
															                			if($answer_value == $text){
																                			 $answer_value_text = "checked";
																                		}
																                	?>
												                                    <tr>
												                                    	<td><?php echo($no_text); ?></td>								                                        
												                                        <td><label class="lead f-16"><?php echo $value['value'];?></label></td>
												                                        <td>
												                                        	<div class="checkbox m-b-15">
																				                <label>
																				                    <!-- <input type="radio" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?>> -->
																				                    <input type="checkbox" name="an_<?php echo $soal_uid;?>" <?php echo $answer_value_text;?> disabled>
																				                    <i class="input-helper"></i>
																				                </label>
																				            </div>
																				       	</td>								                                        
												                                    </tr>
												                                    <?php
												                                    $no_text++;
													                				}
													                				?>
												                                </tbody>
												                            </table>
												                			<?php							                			
												                		}
												                	}
												                	else
												                	{
												                		echo "-";
												                	}
											                	?>
											                </td>
											                <td><?php echo $v['answer_value']; ?></td>	
											                <td><button class="btn btn-default btn-block choose_othersoaluid" data-uidsoal="<?php echo $soal_uid;?>">Pilih</button></td>				                                		
					                                	</tr>
					                                	<?php
					                                	$no++;
					                                	}
					                                ?>
				                                </tbody>
				                            </table>
				                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>    

            <!-- Modal DELETE -->  
            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="modalconfrim_deletearchive" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Delete Quiz</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="deletequiz" uid="" bsid="">Ya</button>                            
                        </div>                        
                    </div>
                </div>
            </div>    

		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	$('a[title]').tooltip();

	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        },
        "aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
        "iDisplayLength": 5
    } );
    $('.data').DataTable();
    $('#mylistquestion').DataTable({
    	"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
    	"iDisplayLength": 5
    });
    $('#otherlistquestion').DataTable({
    	"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
    	"iDisplayLength": 5
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {
	    $(function () {
            var tgl = new Date();  
            var formattedDate = moment(tgl).format('YYYY-MM-DD');         
            
            $('#validity').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });

            $('#invalidity').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });
            
        });

        $(document).on("click", ".confrimDelete", function () {        	
	        var myBookId = $(this).attr('uid');
	        var bsid = $(this).attr('bsid');
	        $('#deletequiz').attr('uid',myBookId);
	        $('#deletequiz').attr('bsid',bsid);
	        $('#modalconfrim_deletearchive').modal('show');
	    });  

        $(document).on("click", ".choose_mysoaluid", function(){        	
        	var soal_uid 	= $(this).data('uidsoal');
        	var bsid 		= "<?php echo $_GET['bsid'];?>";        	

        	$.ajax({
                url :"<?php echo base_url() ?>Process/addchooseArchiveQuiz",
                type:"POST",
                data: {
                    soal_uid: soal_uid,
                    bsid: bsid
                },
                success: function(data){
                	var response = JSON.parse(data);  
                	$("#modaladdsubject").modal("hide");
					if (response['status'] == 1) {   
	        			notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
	        			setTimeout(function(){
	        				location.reload();
	        			},2000);
                    }
                    else if(response['status'] == 2)
                    {
	        			notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',response['message']);	
                    }
                    else
                    {
	        			notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',response['message']);
                    }          
                } 
            });
        });

        $(document).on("click", ".choose_othersoaluid", function(){        	
        	var soal_uid 	= $(this).data('uidsoal');
        	var bsid 		= "<?php echo $_GET['bsid'];?>"; 

        	$.ajax({
                url :"<?php echo base_url() ?>Process/addchooseArchiveQuiz",
                type:"POST",
                data: {
                    soal_uid: soal_uid,
                    bsid: bsid
                },
                success: function(data){
                	var response = JSON.parse(data);  
                	$("#modaladdsubject").modal("hide");
					if (response['status'] == 1) {   
	        			notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
	        			setTimeout(function(){
	        				location.reload();
	        			},2000);
                    }
                    else if(response['status'] == 2)
                    {
	        			notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',response['message']);	
                    }
                    else
                    {
	        			notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',response['message']);
                    }          
                } 
            });
        });

	    $('#deletequiz').click(function(e){
            var soal_uid = $(this).attr('uid');
            var bsid = $(this).attr('bsid');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deleteQuestionQuiz",
                type:"POST",
                data: {
                    soal_uid: soal_uid,
                    bsid:bsid
                },
                success: function(data){
                	var response = JSON.parse(data);                	
                    // alert("Berhasil menghapus data siswa");
                    $('#modalconfrim_deletearchive').modal('hide');
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully delete");
                    setTimeout(function(){
                        location.reload(); 
                    },2000);
                                        
                } 
            });
        });
	});
</script>