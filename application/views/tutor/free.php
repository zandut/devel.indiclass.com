<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2>Atur Kelas Free</h2>

				<!-- <ul class="actions hidden-xs">
					<li>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url();?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>                            
							<li class="active"><?php echo $this->lang->line('selectsubjecttutor'); ?></li>
						</ol>
					</li>
				</ul> -->
			</div> <!-- akhir block header    -->        
			<br>
			<?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>
            <div class="alert alert-danger" id="kotakalerttutor" style="display: none;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->lang->line('alertlimakali');?>
            </div>
			<!-- disini untuk dashboard murid -->               
			<div class="card col-md-12">
                <form class="form-horizontal" role="form">
                    <div class="card-header">
                        <h2>Atur Kelas Free <small>Mengatur Jadwal Kelas Gratis</small><small class="c-red" style="font-style: italic;"><b>* Harap memilih mata pelajaran yang ingin anda buat kelas di menu "<a href="<?php echo base_url();?>tutor/choose">Mata Pelajaran</a>". Untuk memunculkan di menu pilihan mata pelajaran dibawah ini.</b></small></h2><hr>                        
                    </div>
                    
                    <div class="card-body card-padding">

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Kuota Participant</label>
                            <div class="col-sm-6">
                                <div class="dtp-container fg-line">                                                
                                    <select class='select2 form-control' required id="kuota_participants">
                                        <?php
                                            echo '<option disabled="disabled" selected="" value="0">Pilih Kuota</option>'; 
                                            $id = $this->session->userdata("id_user");
                                            $sub = $this->db->query("SELECT * FROM tbl_multicast_volume WHERE id_tutor='$id' AND type='open_multicast' AND status=1")->result_array();
                                            foreach ($sub as $row => $v) {
                                                $price          = number_format($v['price'], 0, ".", ".");;
                                                $volume         = $v['volume'];
                                                $user_utc       = $v['utc'];
                                                $datenow        = $v['date_request'];
                                                // $server_utc     = $this->Rumus->getGMTOffset();
                                                // $interval       = $user_utc - $server_utc;
                                                // $date_requested = DateTime::createFromFormat('Y-m-d H:i:s', $datenow );
                                                // $date_requested->modify("+".$interval ." minutes");     
                                                // $date_requested = $date_requested->format('Y-m-d H:i:s');

                                                echo '<option value="'.$v['idv'].'">'.$datenow.' | Rp. '.$price.' - '.$volume.' Participant</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Tanggal</label>
                        	<div class="col-sm-6">
                                <div class="dtp-container fg-line m-l-5">                                                
                                    <input id="tanggal" type='text' required class="form-control"  data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Waktu</label>
                            <div class="col-sm-6">
                                <div class="dtp-container fg-line m-l-5" id="tempatwaktuu">
                                    <input type='text' class='form-control' id='timeeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />                                
                                </div>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Durasi</label>
                            <div class="col-sm-6">
                            	<div class="dtp-container fg-line">   
                                    <?php 
                                        $status = $this->session->userdata('status');                                        
                                        if ($status == 2) {
                                            ?>
                                    <select class='select2 form-control' required id="durasi">
                                        <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                        <option value="900">15 Minutes</option>
                                    </select>
                                            <?php       
                                        }
                                        else
                                        {
                                    ?>
                                    <select class='select2 form-control' required id="durasi">
                                        <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                        <option value="900">15 Minutes</option>
                                        <option value="1800">30 Minutes</option>
                                        <option value="2700">45 Minutes</option>
                                        <option value="3600">1 Hours</option>
                                    </select>  
                                        <?php 
                                        }
                                    ?>                                                           
                                </div>
                            </div>
                        </div> 

                        <!-- <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Template</label>
                            <div class="col-sm-6">
                            	<div class="dtp-container fg-line">                                                
                                    <select class='select2 form-control' required id="template">
                                        <option disabled selected>Pilih Template</option>
                                        <option value="whiteboard_digital">Multicast Whiteboard Digital</option>
                                        <option value="whiteboard_videoboard">Multicast Whiteboard VideoBoard</option>
                                        <option value="whiteboard_no">Multicast No Whiteboard</option>
                                        <option value="all_featured">Multicast All Features</option>
                                    </select>                                                             
                                </div>
                            </div>
                        </div>  -->

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Pelajaran</label>
                            <div class="col-sm-6">
                            	<div class="dtp-container fg-line">                                                
                                    <select class='select2 form-control' required id="pelajaran">
                                        <?php
                                            echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
                                        	$id = $this->session->userdata("id_user");
                                            $sub = $this->db->query("SELECT * FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id=ms.subject_id WHERE tb.id_user='$id' AND tb.tutor_id='0' AND (tb.status='verified' OR tb.status='requested' OR tb.status='unverified')")->result_array();
                                            foreach ($sub as $row => $v) {

                                                $jenjangnamelevel = array();
                                                $jenjangid = json_decode($v['jenjang_id'], TRUE);
                                                if ($jenjangid != NULL) {
                                                    if(is_array($jenjangid)){
                                                        foreach ($jenjangid as $key => $value) {                                    
                                                            $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();
                                                            
                                                            if ($selectnew['jenjang_level']=='0') {
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
                                                            }
                                                            else{
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
                                                            }

                                                            
                                                        }   
                                                    }else{
                                                        $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
                                                        if ($selectnew['jenjang_level']=='0') {
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
                                                            }
                                                            else{
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
                                                            }                                                         
                                                    }                                       
                                                }

                                                // $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' AND (tb.status='verified' OR tb.status='unverified') order by ms.subject_id ASC")->result_array();                                                
                                                // foreach ($allsub as $row => $v) {                                            
                                                    echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.implode(", ",$jenjangnamelevel).'</option>';
                                                // }
                                            }
                                        ?>
                                    </select>                                                               
                                </div>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Topik Pelajaran</label>
                            <div class="col-sm-6">
                            	<div class="dtp-container fg-line">                                                                                
                                    <!-- <input type="text" name="topikpelajaran" class="form-control" required placeholder="Masukan Topik pelajaran"> -->
                                    <textarea rows="5" name="topikpelajaran" id="topikpelajaran" class="form-control" required placeholder="Masukan Topik Pelajaran"></textarea>        
                                </div>
                            </div>
                        </div> 
                        <input type="text" name="user_utc" id="user_utc" value="<?php echo $this->session->userdata('user_utc'); ?>" hidden>                   

                        <br><hr>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-7">
                                <!-- <a class="waves-effect c-white btn btn-primary btn-block simpanprice"><i class="zmdi zmdi-face-add"></i>Buat Kelas</a> -->
                                <button type="submit" class="waves-effect c-white btn btn-primary btn-block" id="simpankelas">Buat Kelas</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>

		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

$(document).ready(function(){

    var tgll = new Date();  
    var formattedDatee = moment(tgll).format('YYYY-MM-DD');
    var a = [];
    var b = [];            
    var c = [];
    var d = [];

    $(function () {
        var tgl = new Date();  
        var formattedDate = moment(tgl).format('YYYY-MM-DD');         
        
        $('#tanggal').datetimepicker({  
            minDate: moment(formattedDate, 'YYYY-MM-DD')
        });
        
    });

    $('#tanggal').on('dp.change', function(e) {
        date = moment(e.date).format('YYYY-MM-DD');                
        var hoursnoww = tgll.getHours(); 
        // alert(tgl);
        if (date == formattedDatee) {
            a = [];
            $("#tempatwaktuu").html("");
            $("#tempatwaktuu").append("<input type='text' class='form-control' id='time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
            $("#time").removeAttr('disabled');
            for (var i = hoursnoww+1; i < 24; i++) {                                                
                a.push(i);
            }
            console.warn(a);
            $('#time').datetimepicker({                    
                sideBySide: true, 
                showClose: true,                   
                format: 'HH:mm',
                stepping: 15,
                enabledHours: a,
            });
        }
        else
        {
            $("#tempatwaktuu").html("");
            $("#tempatwaktuu").append("<input type='text' class='form-control' id='time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
            $("#time").removeAttr('disabled');
            b = [];
            for (var i = 0; i < 24; i++) {                                                
                b.push(i);
            }
            console.warn(b);
            $('#time').datetimepicker({                    
                sideBySide: true,                    
                format: 'HH:mm',
                showClose: true,
                stepping: 15,
                enabledHours: b,
            });
        }
        
    });

	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();
	
	$("#simpankelas").click(function(e){
        $("#simpankelas").attr('disabled', true);
		var rate_value = null;
		// var isChecked = $('.aba').prop('checked');
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
		var idtutorni = "<?php echo $this->session->userdata('id_user');?>";
        var kuota_participants = $("#kuota_participants").val();
		var tanggal = $("#tanggal").val();		
		var time = $("#time").val()+":00";
		var durasi = $("#durasi").val();
		var template = 'all_featured';
		var pelajaran = $("#pelajaran").val();
		var topikpelajaran = $("#topikpelajaran").val();
		var user_utc = $("#user_utc").val();        
        var status = "<?php echo $this->session->userdata('status');?>";
        if (kuota_participants==null) {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Jika tidak terdapat pilihan Kuota Participant. Mohon untuk meminta kelas percobaan di menu.");
        }
        else if (tanggal=="") {
        	notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih tanggal kelas terlebih dahulu!!!");
        }
        else if (time=="undefined:00") {
        	notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih waktu kelas terlebih dahulu!!!");
        }
        else if (durasi == null) {
        	notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih durasi kelas terlebih dahulu!!!");
        }
        else if (template == null) {
        	notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih template kelas terlebih dahulu!!!");
        }
        else if (pelajaran == null) {
        	notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih pelajaran kelas terlebih dahulu!!!");
        }
        else if (topikpelajaran == "") {
        	notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon mengisi topik pelajarn kelas terlebih dahulu!!!");
        }
        else
        {
			$.ajax({
				url: '<?php echo base_url(); ?>Rest/createclass_free/access_token/'+tokenjwt,
				type: 'POST',
				data: {
					id_tutor: idtutorni,
                    kuota_participants: kuota_participants,
					tanggal: tanggal,
					time: time,
					durasi: durasi,
					template: template,
					pelajaran: pelajaran,
					topikpelajaran: topikpelajaran,
					user_device: 'web',
					user_utc: user_utc,
	                status: status
				},
				success: function(response)
				{					
					// alert(response['code']);
					if (response['code'] == "200") {
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Menambah Kelas");
						setTimeout(function(){
							window.location.replace("<?php echo base_url();?>");
						},1000);
					}
	                else if (response['code'] == "405") {
	                    // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal, Sebelum mendapat persetujuan dari pihak kami, anda hanya diperbolehkan membuat kelas sebanyak 5 kali.");
	                    // setTimeout(function(){
	                    //     location.reload();
	                    // },10000);
	                    $(window).scrollTop(0);
	                    $("#kotakalerttutor").css('display','block');
	                    $("#simpankelas").removeAttr('disabled');
	                    // setTimeout(function(){
	                    //     location.reload();
	                    // },10000);
	                }else if(response['code'] == '403') {
	                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', '<?php echo $this->lang->line("alertalreadyclass");?>');
	                    $("#simpankelas").removeAttr('disabled');
	                    // setTimeout(function(){
	                    //     location.reload();
	                    // },2000);
	                }
	                else if(response['code'] == '402') {
	                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', '<?php echo $this->lang->line("alertdibawahwaktu");?>');
	                    $("#simpankelas").removeAttr('disabled');
	                    // setTimeout(function(){
	                    //     location.reload();
	                    // },2000);
	                }
					else
					{
						notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal Menambah Kelas");
						setTimeout(function(){
							location.reload();
						},2000);
					}
				}
			});
		}
	});
	});	
	
</script>