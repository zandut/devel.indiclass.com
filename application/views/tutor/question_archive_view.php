<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2>Question View</h2>				
			</div> <!-- akhir block header    -->        
			<br>			

            <?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>

			<!-- disini untuk dashboard murid -->               
			<div class="card" style="margin-top: 2%;">
				<div class="card-header">
					<div class="pull-left"><h2>List Question Archive</h2></div>                    
				</div>
				<div class="row" style="margin-top: 2%;">
					<div class="card-body card-padding">
						<div class="table-responsive">							
							<table id="" class="display table table-hover table-bordered data" cellspacing="0" width="100%">
						        <thead>
						        	<?php
										$tutor_id		= $this->session->userdata('id_user');
										$select_all 	= $this->db->query("SELECT * FROM master_banksoal_archive WHERE tutor_creator='$tutor_id'")->result_array();
									?>
						            <tr valign="middle">
						                <th rowspan="2" width="5%"><center>#</center></th>
						                <th colspan="3" width="50%"><center>Quiz</center></th>						                
						                <th rowspan="2" width="5%" style="border-right: 1px solid #eeeeee;"><center>Options</center></th>
						            </tr>
						            <tr>
						                <th width="10%"><center>Question</center></th>
						                <th width="20%"><center>Option</center></th>
						                <th width="20%" style="border-right: 1px solid #eeeeee;"><center>Answer</center></th>
						            </tr>
						        </thead>
						        <tbody>
						        	<?php
									$no=1;
									foreach ($select_all as $row => $v) {
										// if($row>0)break;
										$soal_uid 		= $v['soal_uid'];
										$question 		= $v['text_soal'];
										$options 		= $v['options'];
										$answer_value 	= $v['answer_value'].$soal_uid;
										$options_type 	= $v['options_type'];										
										$j = 1;
										?>
							            <tr align="center">
							                <td><?php echo($no); ?></td>
							                <td><?php echo $question; ?></td>
							                <td id="optionnnn">
							                	<?php 
							                		$obj = json_decode($options, true);
							                		if ($options_type == "images") {								                			
							                			?>
							                			<table class="table bgm-gray table-bordered">
							                                <thead>
							                                    <tr>
							                                    	<th>#</th>								                                        
							                                        <th>Choices</th>
							                                        <th>Correct</th>								                                        
							                                    </tr>
							                                </thead>
							                                <tbody>
							                                	<?php
							                                	$no_image = 1;						                			
										                		foreach ($obj as $key => $value) {
										                			$imguri = $value['imguri'];
										                			$choices_options = $value['value'].$soal_uid;
										                			$answer_checked = "";
										                			if($answer_value == $choices_options){
											                			 $answer_checked = "checked";
											                		}
											                		?>
								                                    <tr>		
								                                    	<td><?php echo($no_image); ?></td>						                                        
								                                        <td><img src='<?php echo $imguri;?>' style='width:100px; height:100px;'/></td>
								                                        <td>
								                                        	<div class="checkbox m-b-15">
																                <label>
																                    <!-- <input type="radio" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?>> -->
																                    <input type="checkbox" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?> disabled>
																                    <i class="input-helper"></i>
																                </label>
																            </div>
																       	</td>								                                        
								                                    </tr>
								                                    <?php
								                                    $no_image++;
								                                }
								                                ?>
							                                </tbody>
							                            </table>
							                			<?php	
							                		}
							                		else
							                		{							                			
							                			?>
							                			<table class="table bgm-gray table-bordered">
							                                <thead>
							                                    <tr>	
							                                    	<th>#</th>
							                                        <th>Choices</th>
							                                        <th>Correct</th>								                                        
							                                    </tr>
							                                </thead>
							                                <tbody>
							                                	<?php
							                                	$no_text = 1;
									                			foreach ($obj as $key => $value) {
										                			$text = $value['value'].$soal_uid;
										                			$answer_value_text = "";
										                			if($answer_value == $text){
											                			 $answer_value_text = "checked";
											                		}
											                	?>
							                                    <tr>
							                                    	<td><?php echo($no_text); ?></td>								                                        
							                                        <td><label class="lead f-16"><?php echo $value['value'];?></label></td>
							                                        <td>
							                                        	<div class="checkbox m-b-15">
															                <label>
															                    <!-- <input type="radio" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?>> -->
															                    <input type="checkbox" name="an_<?php echo $soal_uid;?>" <?php echo $answer_value_text;?> disabled>
															                    <i class="input-helper"></i>
															                </label>
															            </div>
															       	</td>								                                        
							                                    </tr>
							                                    <?php
							                                    $no_text++;
								                				}
								                				?>
							                                </tbody>
							                            </table>
							                			<?php							                			
							                		}
							                	?>
							                </td>
							                <td><?php echo $v['answer_value']; ?></td>							                
							                <td>
							                	<a href="#"><button class="btn bgm-green btn-block" title="Edit Quiz"><i class="zmdi zmdi-setting"></i> Set Global</button></a>
							                	<br><br>
							                	<a uid="<?php echo $v['soal_uid'];?>" class="confrimDelete"><button class="btn bgm-red btn-block confrimDelete" title="Delete Quiz"><i class="zmdi zmdi-delete"></i> Delete</button></a>
                                                                                                
							                </td>							                
							            </tr>
							            <?php
							            $no++;
							        }
							        ?>
						        </tbody>
						    </table>

						</div>
					</div>
				</div>
			</div>

			<!-- Modal TAMBAH -->  
            <div class="modal" id="modaladdsubject" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                            <h4 class="modal-title c-white">New Quiz</h4>
                        </div>
                        <div class="modal-body m-t-20">
                            <form method="post" action="<?php echo base_url(); ?>process/QuizAddNew" enctype="multipart/form-data">
	                            <div class="row p-15">    
	                                
	                                <div class="col-sm-12 col-md-12 col-xs-12">
	                                	<label class="fg-label">Quiz Name</label>
	                                    <div class="form-group fg-float">
	                                        <div class="fg-line">
	                                            <input type="text" name="name" required class="input-sm form-control fg-input">                                          
	                                        </div>
	                                    </div>
	                                </div>  

	                                <div class="col-sm-12 col-md-12 col-xs-12">
	                                	<label class="fg-label">Quiz Description</label>
	                                    <div class="form-group fg-float">
	                                        <div class="fg-line">
	                                            <textarea name="description" class="input-sm form-control fg-input" rows="4"></textarea>                                            
	                                        </div>
	                                    </div>
	                                </div>                                

	                                <div class="col-md-12 m-t-5">        
	                                <label class="fg-label">Jenjang</label>                                                                                   
	                                    <div class="form-group">                                    
	                                        <div class="fg-line">
	                                            <select class='select2 form-control col-md-12 input-sm fg-input' required id="subject_id" name="subject_id" style="width: 100%;">
			                                        <?php
			                                        	$id = $this->session->userdata("id_user");
			                                            $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' order by ms.subject_id ASC")->result_array();
			                                            echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
			                                            foreach ($allsub as $row => $v) {                                            
			                                                echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_level'].' '.$v['jenjang_name'].'</option>';
			                                            }
			                                        ?>
			                                    </select>                                              
	                                        </div>
	                                    </div>
	                                </div>

	                                <div class="col-sm-12 col-md-12 col-xs-12">
	                                	<label class="fg-label">Start Quiz Date</label>
	                                    <div class="form-group fg-float">                                    	
	                                        <div class="fg-line">
	                                            <input type="text" class=" form-control" id="validity" name="validity" data-date-format="DD-MM-YYYY" placeholder="Select start date"/>
	                                        </div>
	                                    </div>
	                                </div> 

	                                <div class="col-sm-12 col-md-12 col-xs-12">
	                                	<label class="fg-label">Finish Quiz Date</label>
	                                    <div class="form-group fg-float">                                	
	                                        <div class="fg-line">
	                                            <input type="text" class=" form-control" id="invalidity" name="invalidity" data-date-format="DD-MM-YYYY" placeholder="Select finish date"/>
	                                        </div>
	                                    </div>
	                                </div>	                                

	                                <div class="col-sm-12 col-md-12 col-xs-12">
	                                	<label class="fg-label">Duration</label>
	                                    <div class="form-group">                                	
	                                        <div class="fg-line">
	                                            <select required class="select2 form-control col-md-12 input-sm fg-input" id="duration" name="duration" style="width: 100%;">
	                                                <option disabled selected><?php echo $this->lang->line('searchduration'); ?></option>
	                                                <option value="900">15 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="1800">30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="2700">45 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="3600">1 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="5400">1 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="7200">2 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="9000">2 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="10800">3 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="12600">3 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="14400">4 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="16200">4 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="18000">5 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="19800">5 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="21600">6 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="23400">6 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="25200">7 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="27000">7 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="28800">8 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="30600">8 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="32400">9 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="34200">9 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="36000">10 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="37800">10 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="39600">11 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="41400">11 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="43200">12 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="45000">12 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="46800">13 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="48600">13 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="50400">14 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="52200">14 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="54000">15 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="55800">15 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="57600">16 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="59400">16 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="61200">17 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="63000">17 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="64800">18 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="66600">18 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="68400">19 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="70200">19 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="72000">20 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="73800">20 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="75600">21 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="77400">21 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="79200">22 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="81000">22 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="82800">23 <?php echo $this->lang->line('jam'); ?></option>
	                                                <option value="84600">23 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
	                                                <option value="86400">24 <?php echo $this->lang->line('jam'); ?></option>
	                                            </select>
	                                        </div>
	                                    </div>
	                                </div>

	                                <div class="col-sm-12 col-md-12 col-xs-12">
	                                	<label class="fg-label">Type</label>
	                                    <div class="form-group">                                	
	                                        <div class="fg-line">
	                                           	<select required class="select2 form-control col-md-12 input-sm fg-input" id="template_type" name="template_type" style="width: 100%;">
	                                                <option disabled selected>Choose type</option>
	                                                <option value="multiple_choice">Multiple Choice</option>
	                                                <option value="essay">Essay</option>
	                                            </select>
	                                        </div>
	                                    </div>
	                                </div>                                 

	                                <div class="col-md-12 m-t-20">
	                                    <button class="btn btn-success btn-block">Simpan</button>
	                                </div>

	                            </div>
                            </form>
                            <br>
                        </div>
                    </div>
                </div>
            </div>    

            <!-- Modal DELETE -->  
            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="modalconfrim_deletearchive" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Delete Quiz</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="deletequiz" uid="">Ya</button>                            
                        </div>                        
                    </div>
                </div>
            </div>  

		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	$('a[title]').tooltip();

	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        },
        "aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
        "iDisplayLength": 5
    } );
    $('.data').DataTable();
	
</script>
<script type="text/javascript">
    $(document).ready(function() {
	    $(function () {
            var tgl = new Date();  
            var formattedDate = moment(tgl).format('YYYY-MM-DD');         
            
            $('#validity').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });

            $('#invalidity').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });
            
        });

        $(document).on("click", ".confrimDelete", function () {        	
	        var myBookId = $(this).attr('uid');
	        $('#deletequiz').attr('uid',myBookId);
	        $('#modalconfrim_deletearchive').modal('show');
	    });  

	    $('#deletequiz').click(function(e){
            var soal_uid = $(this).attr('uid');            
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deleteQuestionArchive",
                type:"POST",
                data: {
                    soal_uid: soal_uid
                },
                success: function(data){
                	var response = JSON.parse(data);                	
                    // alert("Berhasil menghapus data siswa");
                    $('#modalconfrim_deletearchive').modal('hide');
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully delete");
                    setTimeout(function(){
                        location.reload(); 
                    },2000);
                                        
                } 
            });
        });
	});
</script>