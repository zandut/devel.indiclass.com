<link rel="stylesheet" href="<?php echo base_url(); ?>aset/css/jslider.css" type="text/css">

<style type="text/css" media="screen">
	.layout { margin-bottom: 10%; font-family: Georgia, serif;}
	.layout-slider { margin-bottom: 60px; width: 50%; }
	.layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
	.layout-slider-settings pre { font-family: Courier; }
</style>

<script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jslider/tmpl.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jslider/jquery.dependClass-0.1.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jslider/draggable-0.1.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jslider/jquery.slider.js"></script>
<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>


<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<?php if(isset($mes_alert)){ ?>
			<div class="alert alert-<?php echo $mes_alert; ?>" style="display:<?php echo $mes_display; ?>">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<i class="fa fa-ban-circle"></i><?php echo $mes_message; ?>
			</div>
			<?php } ?>

			<div class="block-header">
				<h2><?php echo $this->lang->line('setavailabilitytime'); ?></h2>

				<ul class="actions hidden-xs">
					<li>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>
							<li class="active"><?php echo $this->lang->line('setavailabilitytime'); ?></li>
						</ol>
					</li>
				</ul>
			</div> <!-- akhir block header    -->                      

			<div class="card" style="margin-top: 5%;">

				<!-- INI MULAI TAB -->
				<div class="card-body card-padding">

						<div class="tab-content">
							<?php
							$now = 7-(date('N')-1);
							$nextMon = date('Y-m-d', strtotime('+'.$now.' day', strtotime(date('Y-m-d'))));
							$oneWeek = date('Y-m-d', strtotime('+6 day', strtotime($nextMon)));
							$nextMon = DateTime::createFromFormat('Y-m-d',$nextMon);
							$oneWeek = DateTime::createFromFormat('Y-m-d',$oneWeek);

							$timeArray = array();

							$arrayFul;
							$stime = 0;
							for($x=0;$x<7;$x++){
								$timeArray[$x]["day"] =  date('D, d M', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));
								$timeArray[$x]["date"] = date('Y-m-d', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));
								$query_checker[$x] = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id=".$this->session->userdata('id_user')." AND date='".$timeArray[$x]["date"]."'")->result_array();

								for ($i=0; $i < 24; $i++) {
									$timeArray[$x][$i] = $stime;
									$stime+=3600;
								}
								$stime = 0;
							}

							?>

							<!-- INI TAB SATU -->
								<div class="card-header">
									<div class="row">
										<div class="table-responsive">

											<table class="table">
												<thead>
													<th width="100">No</th>
													<th width="350"><?php echo $this->lang->line('title_pg_subject'); ?></th>
													<th width="250"><?php echo $this->lang->line('subdisc'); ?></th> 
												</thead>
												<tbody>
													<?php
													$fdate = $timeArray[0]["date"];
													echo '<input type="text" style="display: none" id="fdate" value="'.$fdate.'"/>';

													$my_subjects = $this->Rumus->subdMe($this->session->userdata('id_user'),$fdate);
													echo '<input type="text" style="display: none" id="jml_pel" value="'.count($my_subjects).'"/>';
													foreach ($my_subjects as $key => $value) {
														echo '<input type="text" style="display: none" id="subject_id_'.$key.'" value="'.$value['subject_id'].'"/>';
														echo '<tr>
														<td>'.($key+1).'</td>
														<td>
															<label><h2>'.$value['subject_name'].'</h2></label>
														</td>
														<td>
															<textarea class="form-control" id="subdiscussion_'.$key.'" rows="5" placeholder="Contoh : Bangun ruang, Aljabar, dll.">'.$value['isi'].'</textarea>
														</td>
													</tr>';
												}
												?>
											</tbody>

										</table>

									</div>                                                  
									<div class="col-md-12">
										<button aria-controls="setsub" id="btn_setsub" role="tab" data-toggle="tab" class="btn btn-primary btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('button_save'); ?></button>
									</div>
								</div>
							</div>
						</div>


				</div>
			</section>
		</section>

		<footer id="footer">
			<?php $this->load->view('inc/footer'); ?>
		</footer>    
		<script type="text/javascript">
			$(document).ready(function(){
				/*$('.btn-next').click(function(e){
					$('#setsub').removeClass('active');
					$('#setsub_li').removeClass('active');

					$('#settime').addClass('active');
					$('#settime_li').addClass('active');
				});
				$('.btn-prev').click(function(e){
					$('#setsub').addClass('active');
					$('#setsub_li').addClass('active');

					$('#settime').removeClass('active');
					$('#settime_li').removeClass('active');
				});*/
				$('#btn_setsub').click(function(){
					var jml_pel = $('#jml_pel').val();
					var fdate = $('#fdate').val();
					var subject_id = "";
					var subdiscussion = "";
					for (var i = 0; i < jml_pel; i++) {
						subject_id = $('#subject_id_'+i).val();
						subdiscussion = $('#subdiscussion_'+i).val();
						$.ajax({
							url: '<?php echo base_url(); ?>/process/save_subd?subject_id='+subject_id+'&fdate='+fdate,
							type: 'POST',
							data: {
								subdiscussion: subdiscussion
							},
							success: function(data){
								data = JSON.parse(data);
								if(data['status'] == 1){
									notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',data['message']);
								}else{
									notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
								}
							}
						})
					}
				});

				/*var vuvu = $('#myTab').tabs();
				var slide_show = 0;
				vuvu.tabs({
					activate: function(event,ui){
						if(ui.newTab.index() == 1 && slide_show == 0){
							slide_show = 1;
							jQuery(".sliding").slider({
								from: 0, 
								to: 1440, 
								step: 15, 
								dimension: "", 
								scale: ["00:00", "01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00","24:00"], 
								limits: false, 
								calculate: function(value){
									var hours = Math.floor( value / 60 );
									var mins = ( value - hours*60 );
									return (hours < 10 ? "0"+hours : hours) + ":" + ( mins == 0 ? "00" : mins );
								}
							});
							$('.ly-sliding').each(function(index,b){
								var that = $(this);
								if($(this).attr('setted') == '1'){
									that.attr('data-toggle','tooltip');
									that.attr('data-placement','bottom');
									that.attr('title','Cannot change, it has been used.');
									$('[data-toggle="tooltip"]').tooltip(); 

									// alert('yes');
									$(this).find('.jslider-pointer').css('pointer-events','none');
								}
							});
							// $('div .jslider-pointer').css('pointer-events','none');
						}
					}
				});*/
			});

			

		</script>