<div class="d-flex flex-column h-100">
    
    <!-- Navbar -->
        <?php $this->load->view('inc/navbar_tutor');?>
    <!-- // END Navbar -->

    <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
            <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
                <div class="container">

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="">Beranda</a></li>
                        <li class="breadcrumb-item active">Data Diri</li>
                    </ol>

                    <div class="card">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="#first" data-toggle="tab">Data Diri</a>
                            </li>
                        </ul>
                        <div class="tab-content card-body">
                            <div class="tab-pane active" id="first">
                                <form action="#" class="form-horizontal">
                                    <div class="form-group row">
                                        <label for="avatar" class="col-sm-3 c ol-form-label">Foto</label>
                                        <div class="col-sm-9">
                                            <img src="" onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" id="profile_image" style="width: 200px; height: 200px;">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 col-form-label">Nama Lengkap</label>
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" placeholder="Isi nama lengkap" disabled id="profile_fullname">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-3 col-form-label">Email</label>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1">
                                                    <i class="material-icons md-18 text-muted">mail</i>
                                                </span>
                                                <input type="text" class="form-control" placeholder="Email Address" disabled id="profile_email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="website" class="col-sm-3 col-form-label">Tempat & Tanggal Lahir </label>
                                        <div class="col-sm-6 col-md-6">
                                        	<div class="row">
                                            	<div class="col-md-6">
		                                            <div class="input-group">
		                                                <span class="input-group-addon" id="basic-addon2">
		                                                    <i class="material-icons md-18 text-muted">room</i>
		                                                </span>
		                                                <input type="text" class="form-control" placeholder="" disabled id="profile_place">
		                                            </div>
		                                        </div>
		                                        <div class="col-md-6">
		                                        	<div class="input-group">
		                                                <span class="input-group-addon" id="basic-addon2">
		                                                    <i class="material-icons md-18 text-muted">calendar_today</i>
		                                                </span>
		                                                <input type="text" class="form-control" placeholder="" disabled id="profile_date">
		                                            </div>
		                                        </div>
		                                    </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="col-sm-3 col-form-label">Umur</label>
                                        <div class="col-sm-6 col-md-4">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon3">
                                                    <i class="material-icons md-18 text-muted">assignment_ind</i>
                                                </span>
                                                <input type="text" class="form-control" placeholder="Age" disabled id="profile_age">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                        <div class="col-sm-6 col-md-4">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon3">
                                                    <i class="material-icons md-18 text-muted">supervised_user_circle</i>
                                                </span>
                                                <input type="text" class="form-control" placeholder="Gender" disabled id="profile_gender">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="col-sm-3 col-form-label">Alamat </label>
                                        <div class="col-sm-6 col-md-4">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon3">
                                                    <i class="material-icons md-18 text-muted">account_balance</i>
                                                </span>
                                                <input type="text" class="form-control" placeholder="Home Address " disabled id="profile_home">
                                            </div>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>
                      
                        </div>
                    </div>

                </div>
            </div>

            <div class="animated fadeIn modal fade" style="margin-top: 15%; color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
			    <div class="modal-dialog" >
			        <div class="modal-content" id="modal_konten">
			            <div class="modal-body" align="center">
			                <label id="text_modal">Halloo</label><br>
			                <button   id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
			            </div>
			        </div>
			    </div>
			</div>
        <?php $this->load->view('inc/sidebar_tutor');?>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {        
        var tgl = new Date(); 
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc; 
        var access_token = "<?php echo $this->session->userdata('access_token');?>";
        var iduser = "<?php echo $this->session->userdata('id_user');?>";
        var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
        var member_ship = "<?php echo $this->session->userdata('member_ship');?>";
        var dataSet = [];

        // var status_member = null;
        if (member_ship == 'basic') {
            $("#klik_premium").removeAttr('disabled');
        }
        else
        {
            $("#klik_premium").attr('disabled','true');
        }

        $.ajax({
            url: '<?php echo AIR_API;?>detail_student/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id : '46',
                id_user : iduser
            },
            success: function(response)
            {
                if (response['code'] == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
                var a = JSON.stringify(response);
                // alert(response['data']['user_name']);

                var user_name   	= response['data']['user_name'];
                var email   		= response['data']['email'];
                var user_address   	= response['data']['user_address'];
                var user_age   		= response['data']['user_age'];
                var user_gender   	= response['data']['user_gender'];
                var user_birthdate  = response['data']['user_birthdate'];
                var user_birthplace = response['data']['user_birthplace'];
                var user_image  	= response['data']['user_image'];
                var kotak_image 	= "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+user_image;
                $("#profile_fullname").val(user_name);
                $("#profile_place").val(user_birthplace);
                $("#profile_date").val(user_birthdate);
                $("#profile_age").val(user_age);
                $("#profile_gender").val(user_gender);
                $("#profile_home").val(user_address);
                $("#profile_email").val(email);
                $("#profile_image").attr('src',kotak_image);

            }

        });


        //TOP UP CREDITS
        $.ajax({
            url: '<?php echo base_url();?>Rest/list_package/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response);  
                var code = response['code'];   
                var color_orange = "#F7CA18";
                var color_green  = "#00E640";
                var color_red    = "#F22613";
                if (code == 200) {
                    for (var i = 0; i < response.data.length; i++) {
                        var id_package = response['data'][i]['id_package'];
                        var name_package = response['data'][i]['name_package'];
                        var credit_package = response['data'][i]['credit_package'];
                        var price_package_basic = response['data'][i]['price_package_basic'];
                        var price_package_premium = response['data'][i]['price_package_premium'];
                        if (i == 0) { color = color_orange; }else if(i == 1){ color = color_green;}else{ color = color_red;}
                        if(member_ship == "basic"){
                            var kotak_credit =                             
                            "<div class='col-md-4'>"+
                                "<div class='card text-center'>"+
                                    "<div style='height: 4px; background-color: "+color+";'></div>"+
                                    "<div class='card-header'>"+name_package+"</div>"+
                                    "<div class='card-body'>"+
                                        "<h4 class='card-title' style='margin-top: 5%;'>Price Package</h4>"+
                                        "<div class='row' style='margin-top: 6%; margin-bottom: 6%;'>"+
                                            "<div class='col-md-6'>"+
                                                "<p><h5>Price Basic</h5></p>"+
                                                "<p><h5>$ "+price_package_basic+"</h5></p>"+
                                            "</div>"+
                                            "<div class='col-md-6'>"+
                                                "<p><strong><h5>Price Premium</h5></strong></p>"+
                                                "<p><strong><h5>$ "+price_package_premium+"</h5></strong></p>"+
                                            "</div>"+
                                        "</div>"+
                                        "<p>Total "+credit_package+" Credits</p>"+
                                    "</div>"+
                                    "<div class='card-footer text-muted'>"+
                                        "<a href='#' data-credit='"+credit_package+"' data-price='"+price_package_basic+"' data-idpackage='"+id_package+"' data-namepackage='"+name_package+"' class='btn btn-primary btn-block btn_buy'>Buy Package</a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
                        }
                        else
                        {
                            var kotak_credit =                             
                            "<div class='col-md-4'>"+
                                "<div class='card text-center'>"+
                                    "<div style='height: 4px; background-color: "+color+";'></div>"+
                                    "<div class='card-header'>"+name_package+"</div>"+
                                    "<div class='card-body'>"+
                                        "<h4 class='card-title' style='margin-top: 5%;'>Price Package</h4>"+
                                        "<div class='row' style='margin-top: 6%; margin-bottom: 6%;'>"+                                            
                                            "<div class='col-md-12'>"+
                                                // "<p><strong><h5>Price Premium</h5></strong></p>"+
                                                "<p><strong><h5>$ "+price_package_premium+"</h5></strong></p>"+
                                            "</div>"+
                                        "</div>"+
                                        "<p>Total "+credit_package+" Credits</p>"+
                                    "</div>"+
                                    "<div class='card-footer text-muted'>"+
                                        "<a href='#' data-credit='"+credit_package+"' data-price='"+price_package_premium+"' data-idpackage='"+id_package+"' data-namepackage='"+name_package+"' class='btn btn-primary btn-block btn_buy'>Buy Package</a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
                        }                           

                        $("#list_credit").append(kotak_credit);
                    }
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
            }
        });
		
		$(document.body).on('click', '.btn_buy' ,function(e){
           var id_package = $(this).data('idpackage');
            var name_package = $(this).data('namepackage');   
            var total_credit = $(this).data('credit')
            var price_package = $(this).data('price');       
            if (iduser == null || iduser =='') {
                $("#modal_alert_login").modal('show');
                // $("#modal_buypackage").modal('show');
            }
            else
            {   
                $("#kotak_info").css('display','block');
                $('#total_credit').val(total_credit);
                $('#namepackage').val(name_package);
                $('#detail_package').text(name_package);
                $('#detail_credit').text(total_credit+" Credits");
                $('#detail_price').text(price_package);
                $('#item_price').val(price_package);
                $('#item_total').val(price_package);
                $('#item_description').val('Package '+name_package);
                $('#lnk_click').attr('href','<?php echo BASE_URL();?>products/buy_package/'+id_package);
            }
        });

		$(".btn_member").click(function(){            
            var status_member= $(this).data('select_member');
            $.ajax({
                url: '<?php echo AIR_API;?>success_upgrade/access_token/'+access_token,
                type: 'POST',
                data: {
                    channel_id : '46',
                    id_user : iduser,
                    member_select : status_member
                },
                success: function(response)
                {
                    if (response['code'] == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                    else if (response['code']== 200) {
                        $.ajax({
                            url: '<?php echo BASE_URL();?>First/setMembership',
                            type: 'POST',
                            data: {
                                status_member:status_member
                            },
                            success: function(response)
                            {
                                $("#modal_package").modal('hide');
                                $("#modal_alert").modal('show');
                                $("#modal_konten").css('background-color','#32c787');
                                $("#text_modal").html("Successfully Upgrade Membership");
                                $("#button_ok").click( function(){
                                    location.reload();
                                });
                            }
                        });                            
                    }

                }
            });            
        });


		$.ajax({
            url: 'https://rltclass.com.sg/Rest/myPaypalReport/access_token/'+access_token,
            data:{
            	user_id : iduser
            },
            type: 'POST',
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                
                if (code == 200) {                     
                    for (var i = 0; i < response.data.length; i++) {
                        var payment_id  = response['data'][i]['payment_id'];
                        var user_id = response['data'][i]['user_id'];
                        var product_id  = response['data'][i]['product_id'];
                        var txn_id  = response['data'][i]['txn_id'];
                        var payment_gross   = response['data'][i]['payment_gross'];
                        var currency_code   = response['data'][i]['currency_code'];
                        var payer_email = response['data'][i]['payer_email'];
                        var payment_status  = response['data'][i]['payment_status'];
                        var id_package  = response['data'][i]['id_package'];
                        var name_package    = response['data'][i]['name_package'];
                        var credit_package  = response['data'][i]['credit_package'];
                        var price_package   = response['data'][i]['price_package'];
                        

                        dataSet.push([i+1, txn_id, name_package, credit_package, payment_gross, currency_code, payer_email, payment_status]);
                    }                        

                    $('#tables_reportPayment').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="reportList"></table>' );
             
                    $('#reportList').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Transaction ID"},
                            { "title": "Package Name"},
                            { "title": "Package Credit"},
                            { "title": "Payment Gross"},
                            { "title": "Currency Code"},
                            { "title": "Payer Email"},
                            { "title": "Payment Status"}
                                                        
                        ]
                    });
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
                else
                {
                    $('#tables_reportPayment').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="reportList"></table>' );
             
                    $('#reportList').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Transaction ID"},
                            { "title": "Package Name"},
                            { "title": "Package Credit"},
                            { "title": "Payment Gross"},
                            { "title": "Currency Code"},
                            { "title": "Payer Email"},
                            { "title": "Payment Status"}
                             
                        ]
                    });
                }
            }
        });

    });
</script>