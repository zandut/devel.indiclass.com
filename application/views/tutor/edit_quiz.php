<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sidetutor'); ?>
    </aside>

    <section id="content">
        <div class="container invoice">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Edit Quiz</h2>
            </div>
                    
            <?php
                $bsid               = $_GET['bsid'];
                $getquiz            = $this->db->query("SELECT * FROM master_banksoal WHERE bsid='$bsid'")->row_array();
                if (empty($getquiz)) {
                    header("Location: ".BASE_URL()."Tutor/Quiz");
                }
                else
                {
                    $validity           = $getquiz['validity'];
                    $invalidity         = $getquiz['invalidity'];
                    $server_utcc        = $this->Rumus->getGMTOffset();
                    $user_utc           = $this->session->userdata('user_utc');
                    $intervall          = $user_utc - $server_utcc;
                    $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$validity);
                    $tanggal->modify("+".$intervall ." minutes");
                    $tanggal            = $tanggal->format('d-m-Y H:i:s');
                    $vtanggal           = DateTime::createFromFormat ('Y-m-d H:i:s',$invalidity);
                    $vtanggal->modify("+".$intervall ." minutes");
                    $vtanggal           = $vtanggal->format('d-m-Y H:i:s');
                
            ?>
            <div class="card">
                <div class="card-header ch-alt">
                    <div class="pull-right f-14">Quiz Name : <?php echo $getquiz['name']; ?></div>
                    <div class="pull-left f-14"><a href="<?php echo base_url();?>tutor/Quiz"><button class="btn btn-danger"><i class="zmdi zmdi-arrow-left"></i> Back</button></a></div><br>
                </div>
                
                <div class="card-body card-padding" style="background: white;">                
                    <div class="row">                        
                        <!-- <form method="post" action="<?php echo base_url('process/QuizEdit'); ?>" enctype="multipart/form-data"> -->
                            <div class="col-md-3 m-t-25"></div>
                            <div class="col-md-6 m-t-25 m-b-25" style="border: 1px solid gray; border-radius: 5px; padding: 15px;">
                                
                                <input type="text" name="bsid" id="bsid" value="<?php echo $bsid; ?>" hidden/>                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Quiz Name</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="name" id="name_quiz" value="<?php echo $getquiz['name'] ?>" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Quiz Description</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <textarea name="description" id="description_quiz" class="input-sm form-control fg-input" rows="5"><?php echo $getquiz['description'];?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br><br><br><br>                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12">Jenjang</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="subject_id" id="subject_id_quiz" class="select2 col-md-6 form-control">
                                            <?php
                                            	$tutorid = $getquiz['tutor_id'];
                                                $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$tutorid' order by ms.subject_id ASC")->result_array();
                                                foreach ($allsub as $row => $d) {
                                                    if($d['subject_id'] == $getquiz['subject_id']){
                                                        echo "<option value='".$d['subject_id']."' selected='true'>".$d['subject_name']." - ".$d['jenjang_level']." ".$d['jenjang_name']."</option>";
                                                    }
                                                    if ($d['subject_id'] != $getquiz['subject_id']) {
                                                        echo "<option value='".$d['subject_id']."'>".$d['subject_name']." - ".$d['jenjang_level']." ".$d['jenjang_name']."</option>";
                                                    }                                                            
                                                }
                                            ?> 
                                            </select>
                                        </div>
                                    </div>
                                </div>                                
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Start Quiz Date</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" class="form-control" id="validityy" name="validity" data-date-format="DD-MM-YYYY" placeholder="Select Start date" value="<?php echo $tanggal;?>"/>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Finish Quiz Date</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" class="form-control" id="invalidity" name="invalidity" data-date-format="DD-MM-YYYY" placeholder="Select finish date" value="<?php echo $vtanggal;?>"/>
                                        </div>
                                    </div>
                                </div> 
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Duration</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required class="select2 form-control col-md-12 input-sm fg-input" id="duration" name="duration" style="width: 100%;">
                                                <option disabled selected><?php echo $this->lang->line('searchduration'); ?></option>
                                                <option value="900" <?php if($getquiz['duration']=="900"){ echo "selected='true'";} ?>>15 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="1800" <?php if($getquiz['duration']=="1800"){ echo "selected='true'";} ?>>30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="2700" <?php if($getquiz['duration']=="2700"){ echo "selected='true'";} ?>>45 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="3600" <?php if($getquiz['duration']=="3600"){ echo "selected='true'";} ?>>1 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="5400" <?php if($getquiz['duration']=="5400"){ echo "selected='true'";} ?>>1 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="7200" <?php if($getquiz['duration']=="7200"){ echo "selected='true'";} ?>>2 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="9000" <?php if($getquiz['duration']=="9000"){ echo "selected='true'";} ?>>2 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="10800" <?php if($getquiz['duration']=="10800"){ echo "selected='true'";} ?>>3 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="12600" <?php if($getquiz['duration']=="12600"){ echo "selected='true'";} ?>>3 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="14400" <?php if($getquiz['duration']=="14400"){ echo "selected='true'";} ?>>4 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="16200" <?php if($getquiz['duration']=="16200"){ echo "selected='true'";} ?>>4 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="18000" <?php if($getquiz['duration']=="18000"){ echo "selected='true'";} ?>>5 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="19800" <?php if($getquiz['duration']=="19800"){ echo "selected='true'";} ?>>5 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="21600" <?php if($getquiz['duration']=="21600"){ echo "selected='true'";} ?>>6 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="23400" <?php if($getquiz['duration']=="23400"){ echo "selected='true'";} ?>>6 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="25200" <?php if($getquiz['duration']=="25200"){ echo "selected='true'";} ?>>7 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="27000" <?php if($getquiz['duration']=="27000"){ echo "selected='true'";} ?>>7 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="28800" <?php if($getquiz['duration']=="28800"){ echo "selected='true'";} ?>>8 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="30600" <?php if($getquiz['duration']=="30600"){ echo "selected='true'";} ?>>8 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="32400" <?php if($getquiz['duration']=="32400"){ echo "selected='true'";} ?>>9 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="34200" <?php if($getquiz['duration']=="34200"){ echo "selected='true'";} ?>>9 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="36000" <?php if($getquiz['duration']=="36000"){ echo "selected='true'";} ?>>10 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="37800" <?php if($getquiz['duration']=="37800"){ echo "selected='true'";} ?>>10 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="39600" <?php if($getquiz['duration']=="39600"){ echo "selected='true'";} ?>>11 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="41400" <?php if($getquiz['duration']=="41400"){ echo "selected='true'";} ?>>11 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="43200" <?php if($getquiz['duration']=="43200"){ echo "selected='true'";} ?>>12 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="45000" <?php if($getquiz['duration']=="45000"){ echo "selected='true'";} ?>>12 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="46800" <?php if($getquiz['duration']=="46800"){ echo "selected='true'";} ?>>13 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="48600" <?php if($getquiz['duration']=="48600"){ echo "selected='true'";} ?>>13 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="50400" <?php if($getquiz['duration']=="50400"){ echo "selected='true'";} ?>>14 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="52200" <?php if($getquiz['duration']=="52200"){ echo "selected='true'";} ?>>14 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="54000" <?php if($getquiz['duration']=="54000"){ echo "selected='true'";} ?>>15 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="55800" <?php if($getquiz['duration']=="55800"){ echo "selected='true'";} ?>>15 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="57600" <?php if($getquiz['duration']=="57600"){ echo "selected='true'";} ?>>16 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="59400" <?php if($getquiz['duration']=="59400"){ echo "selected='true'";} ?>>16 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="61200" <?php if($getquiz['duration']=="61200"){ echo "selected='true'";} ?>>17 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="63000" <?php if($getquiz['duration']=="63000"){ echo "selected='true'";} ?>>17 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="64800" <?php if($getquiz['duration']=="64800"){ echo "selected='true'";} ?>>18 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="66600" <?php if($getquiz['duration']=="66600"){ echo "selected='true'";} ?>>18 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="68400" <?php if($getquiz['duration']=="68400"){ echo "selected='true'";} ?>>19 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="70200" <?php if($getquiz['duration']=="70200"){ echo "selected='true'";} ?>>19 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="72000" <?php if($getquiz['duration']=="72000"){ echo "selected='true'";} ?>>20 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="73800" <?php if($getquiz['duration']=="73800"){ echo "selected='true'";} ?>>20 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="75600" <?php if($getquiz['duration']=="75600"){ echo "selected='true'";} ?>>21 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="77400" <?php if($getquiz['duration']=="77400"){ echo "selected='true'";} ?>>21 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="79200" <?php if($getquiz['duration']=="79200"){ echo "selected='true'";} ?>>22 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="81000" <?php if($getquiz['duration']=="81000"){ echo "selected='true'";} ?>>22 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="82800" <?php if($getquiz['duration']=="82800"){ echo "selected='true'";} ?>>23 <?php echo $this->lang->line('jam'); ?></option>
                                                <option value="84600" <?php if($getquiz['duration']=="84600"){ echo "selected='true'";} ?>>23 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                <option value="86400" <?php if($getquiz['duration']=="86400"){ echo "selected='true'";} ?>>24 <?php echo $this->lang->line('jam'); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>                                                          
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Type</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required class="select2 form-control col-md-12 input-sm fg-input" id="template_type" name="template_type" style="width: 100%;">
                                                <option disabled selected>Choose type</option>
                                                <option value="multiple_choice" <?php if($getquiz['template_type']=="multiple_choice"){ echo "selected='true'";} ?>>Multiple Choice</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <br><br><br>
                                <button id="editQuiz" class="btn btn-md c-white btn-block m-t-10 bgm-blue" style="border: 1px solid gray;">Edit</button>
                            </div>
                            <div class="col-md-3 m-t-25"></div>
                                                    
                        <!-- </form> -->

                    </div>
                </div>
                
            </div>
            <?php
                }
            ?>
        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function() {
	    $(function () {
            var tgl = new Date();  
            var formattedDate = moment(tgl).format('YYYY-MM-DD');         
            
            $('#validityy').datetimepicker({  
                
            });

            $('#invalidity').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });
            
        });

        $(document).on('click', "#editQuiz", function(e){
            $("#editQuiz").attr('disabled','disabled');
            var bsid                = $("#bsid").val();
            var name_quiz           = $("#name_quiz").val();
            var description_quiz    = $("#description_quiz").val();
            var subject_id_quiz     = $("#subject_id_quiz").val();
            var validity_quiz       = $("#validityy").val();
            var invalidity_quiz     = $("#invalidity").val();
            var duration_quiz       = $("#duration").val();
            var template_type_quiz  = $("#template_type").val();

            if (name_quiz == "" || description_quiz == "" || subject_id_quiz == "" || validity_quiz == "" || invalidity_quiz == "" || duration_quiz == null || template_type_quiz == null) {
                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Terdapat data kosong, harap isi dengan lengkap!");
            }
            else
            {
                $.ajax({
                    url :"<?php echo base_url() ?>Process/QuizEdit",
                    type:"POST",
                    data: {
                        bsid: bsid,
                        name: name_quiz,
                        description: description_quiz,
                        subject_id: subject_id_quiz,
                        validity: validity_quiz,
                        invalidity: invalidity_quiz,
                        duration: duration_quiz,
                        template_type: template_type_quiz
                    },
                    success: function(data){
                        var response = JSON.parse(data);
                        if (response['status'] == 1) {
                            $("#modaladdsubject").modal('hide');
                            notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
                            setTimeout(function(){
                                window.location.replace('<?php echo base_url();?>Tutor/Quiz');
                            },2000);
                        }
                        else
                        {
                            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',response['message']);
                            setTimeout(function(){
                                window.location.replace('<?php echo base_url();?>Tutor/Quiz');
                            },2000);
                        }
                    }
                });
            }
        });
	});
</script>