<style type="text/css">
	.toggle-switch .ts-label {
		min-width: 130px;
	}
	html, body {
      /*margin: 0px;
      padding: 0px;*/
      height: 100%;
  }
</style>
<body onload="init();">
	<header id="header" class="clearfix" data-current-skin="blue">
		<?php $this->load->view('inc/navbar');

		?>
	</header>
	<section id="main" data-layout="layout-1" style="overflow: auto;">
		<aside id="sidebar" class="sidebar c-overflow">
			<?php $this->load->view('./inc/sidetutor'); ?>
		</aside>
		<section id="content">
			<div class="container">
				<div class="block-header">
					<h2><?php echo $this->lang->line('setavailabilitytime'); ?></h2>

					<ul class="actions hidden-xs">
						<li>
							<ol class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>
								<li class="active"><?php echo $this->lang->line('setavailabilitytime'); ?></li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</section>
	</section>
	<form action="" method="post" target="hidden_frame" accept-charset="utf-8">
		<!-- <input type="text" name="data" value="" id="data"> -->
		<textarea hidden name="data" value="" id="data"></textarea>
	</form>
	<style type="text/css">
		.dhx_cal_container{
			overflow: inherit;
			overflow-x: inherit;
			overflow-y: inherit;
		}
	</style>
	<div style="height: 1154px">
		<div id="scheduler_here" class="dhx_cal_container" style='margin-left: auto; margin-right: auto; width:85%; height:100%; margin-bottom: 10%; margin-top: -5%;'>
			<div class="dhx_cal_navline">
				<div class="dhx_cal_date"></div>
			</div>
			<div class="dhx_cal_header">
			</div>
			<div class="dhx_cal_data">
			</div>
		</div>
	</div>
	<div style=" margin-left: 6%; width: 85%; margin-right: 35%; margin-top: 1%; ">
		<button class="btn btn-primary btn-block m-l-20 m-r-20" id="btn_settime" style="padding-top: 10px; padding-bottom: 10px;">Simpan</button>
		<!-- <button class="btn btn-primary btn-block m-l-20 m-r-20" onclick="show()">Show</button> -->
	</div>
	<div class="modal" id="deletedata" tabindex="-1" role="dialog" style="top: 40%;" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
            <br><br>
                <div class="modal-body text-center" style="background-color: #ffffff;">
                    <div class="m-b-20">
	                    <p><label class="f-16" style="font-family: arial;">Are you sure delete?</label></p>
	                    <button class="btn btn-default m-r-10" type="button" data-dismiss="modal">Cancel</button>
	                    <button class="btn btn-danger m-l-10" type="button" id="delete">Yes, Delete it</button><br>
                    </div>
                </div>           
            </div>
        </div>
    </div>
	<script type="text/javascript" charset="utf-8">
		function init() {
			scheduler.config.multi_day = true;

			scheduler.config.lightbox.sections = [

			];

			scheduler.config.full_day = true;

			scheduler.config.xml_date = "%Y-%m-%d %H:%i";
			scheduler.config.api_date = "%Y-%m-%d %H:%i";
			scheduler.config.details_on_create = true;
			scheduler.config.details_on_dblclick = true;		

			scheduler.attachEvent("onBeforeLightbox", function (id){
				var event = scheduler.getEvent(id);
				event.text = "";
				return true;
			});
			var now = new Date();
			now.setDate(now.getDate()+7);
			var monday = new Date();
			monday.setDate(monday.getDate()+7-(now.getDay()-1));
			var sunday = new Date();
			sunday.setDate(sunday.getDate()+7+(7-now.getDay()));
			monday = monday.getFullYear()+"-"+pad(monday.getMonth()+1,2)+"-"+pad(monday.getDate(),2);
			sunday = sunday.getFullYear()+"-"+pad(sunday.getMonth()+1,2)+"-"+pad(sunday.getDate(),2);

			scheduler.init("scheduler_here",now,"week");
			// scheduler.parse(small_events_list, "json");

			function block_readonly(id){
				if (!id) return true;
				return !this.getEvent(id).readonly;
			}

			scheduler.attachEvent("onBeforeDrag",block_readonly);
			scheduler.attachEvent("onClick", function(id){
				if (this.getEvent(id).readonly)
					return false;
				master_id = id;
				$('#deletedata').modal('show');
			});

			$('#delete').click(function(){
				// alert(master_id);
				$('#deletedata').modal('hide');
				$.get('<?php echo base_url(); ?>process/delete_lavtime?lavtime_id='+master_id,function(data){
					data = JSON.parse(data);
					if(data['status'] == 1){
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',data['message']);
						scheduler.deleteEvent(master_id);
					}else{
						notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
					}

				});
			});

			$.ajax({
				url: '<?php echo base_url(); ?>ajaxer/ajax_lavtimeMe',
				method: 'POST',
				data: {
					start_date: monday,
					end_date: sunday
				},
				success: function(data){
					data = JSON.parse(data);
					$.each(data['data'],function(index, value){
						if(value['setted'] == 0){
							scheduler.parse([
							{ id:value['lavtime_id'], start_date: value['date']+" "+value['start_time'], end_date: value['date']+" "+value['stop_time'], text:""}
							],"json");
						}else{
							scheduler.parse([
							{ id:value['lavtime_id'], start_date: value['date']+" "+value['start_time'], end_date: value['date']+" "+value['stop_time'], text:"", readonly:true}
							],"json");
						}
						
					});
				}
			})

		}

		function showasli(){
			alert(scheduler.toJSON());
		}
		function show() {
			alert(scheduler.toJSON());
			$('#data').html(scheduler.toJSON());
		}
		function save() {
			var form = document.forms[0];
			form.action = "./inc/json_writer.php";
			form.elements.data.value = scheduler.toJSON();
			form.submit();
		}
		function pad (str, max) {
		  str = str.toString();
		  return str.length < max ? pad("0" + str, max) : str;
		}
	</script>
	<script type="text/javascript">
		$('#btn_settime').click(function(){
			var data = scheduler.toJSON();
			// alert(data);
			/*var a = $('#time_form').serializeArray();
			var o = {};

			$.each(a, function() {
				if (o[this.name] !== undefined) {
					if (!o[this.name].push) {
						o[this.name] = [o[this.name]];
					}
					o[this.name].push(this.value || '');
				} else {
					o[this.name] = this.value || '';
				}
			});*/

			$.ajax({
				url: '<?php echo base_url(); ?>/process/save_lavtime',
				type: 'POST',
				data: {
					'data': data
				},
				success: function(data){
					data = JSON.parse(data);
					if(data['status'] == 1){
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',data['message']);
						location.reload();
					}else{
						notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
					}
				}
			});
		});
	</script>

