<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>

</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2><?php echo $this->lang->line('reportteaching'); ?></h2>				
			</div> <!-- akhir block header    -->        
			<br>
			<?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>

			<div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="card pt-inner" style="border-radius: 10px;">
                        <div class="pti-header bgm-blue">
                        	<?php 
                        		$idpocket 		= $this->session->userdata('id_user');
                        		$active_balance	= $this->db->query("SELECT active_balance FROM uangsaku WHERE id_user='$idpocket'")->row_array()['active_balance'];
                        		$active_balance = number_format($active_balance, 0, ".", ".");
                        	?>
                            <h2><small>Rp </small> <?php echo $active_balance; ?> </h2>
                            <div class="ptih-title">Saldo Anda</div>
                        </div>                               
                    </div>
                </div>  
                <div class="col-sm-4 col-md-4">
                    <div class="card pt-inner" style="border-radius: 10px;">
                        <div class="pti-header bgm-orange">
                        	<?php 
                        		$tutorcount 	= $this->session->userdata('id_user');
                        		$totalkelas		= $this->db->query("SELECT count(*) as total FROM tbl_class as tc LEFT JOIN tbl_order_detail as tcd on tc.class_id=tcd.class_id LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id WHERE tc.tutor_id='$tutorcount' AND tc.class_type!='multicast_channel_paid' AND tc.class_type!='private_channel' AND tc.class_type!='group_channel'")->row_array();
                        	?>
                            <h2><?php echo $totalkelas['total']; ?> </h2>                        	
                            <div class="ptih-title">Total kelas</div>
                        </div>                               
                    </div>
                </div> 
                <div class="col-sm-4 col-md-4">
                    <div class="card pt-inner" style="borderr-radius: 10px;">
                        <div class="pti-header bgm-purple">
                        	<?php 
                        		$tutorcount 	= $this->session->userdata('id_user');
                        		$totalpending	= $this->db->query("SELECT * FROM tbl_class as tc LEFT JOIN tbl_order_detail as tcd on tc.class_id=tcd.class_id LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id WHERE tc.tutor_id='$tutorcount' AND tc.class_type!='multicast_channel_paid' AND tc.class_type!='private_channel' AND tc.class_type!='group_channel'")->result_array();
                        		$countpending   = 0;
                        		foreach ($totalpending as $v) {
                        			$order_id 	= $v['order_id'];
                        			$d 			= $this->db->query("SELECT order_status FROM tbl_order where order_id='$order_id'")->row_array();
                        			if (!empty($d)) {
                        				$status = $d['order_status'];
                        				if ($status != 1) {
                        					$countpending = $countpending+1;
                        				}                        				
                        			}
                        			else
                        			{
                        				$countpending = $countpending+1;
                        			}
                        		}
                        	?>
                            <h2><?php echo $countpending; ?> </h2>
                            <div class="ptih-title">Pending Payment</div>
                        </div>                               
                    </div>
                </div>                
            </div>

            <div class="card" style="margin-top: 2%;">
            	<div class="card-body p-15">
            		<div class="row">
            			<form role="form" action="<?php  echo base_url(); ?>tutor/ReportTeaching" method="get" >
		            		<div class="col-md-12 col-xs-12">
		            			<div class="col-md-3">
		            				<label>Mata Pelajaran</label>
		            				<select class="select2 form-control" id="subject" name="subject">
		                                <?php
                                        	$id = $this->session->userdata("id_user");
                                            $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' order by ms.subject_id ASC")->result_array();
                                            echo '<option selected="" value="">Semua Pelajaran</option>'; 
                                            foreach ($allsub as $row => $v) {                                            
                                                echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_level'].' '.$v['jenjang_name'].'</option>';
                                            }
                                        ?>
		                            </select>
		            			</div>
		            			<div class="col-md-3">
		            				<label>Bulan</label>
		            				<select class="select2 form-control" id="month" name="month">
		            					<option selected value=''>Semua <?php echo $this->lang->line('monthbirth'); ?></option>
		                                <option value="1">Januari</option>
		                                <option value="2">Februari</option>
		                                <option value="3">Maret</option>
		                                <option value="4">April</option>
		                                <option value="5">Mei</option>
		                                <option value="6">Juni</option>
		                                <option value="7">Juli</option>
		                                <option value="8">Agustus</option>
		                                <option value="9">September</option>
		                                <option value="10">Oktober</option>
		                                <option value="11">November</option>
		                                <option value="12">Desember</option>
		                            </select>
		            			</div>
		            			<div class="col-md-3">
		            				<label>Tahun</label>
		            				<select name="years" id="years" class="select2 form-control">
	                                    <option selected value=''>Semua <?php echo $this->lang->line('yearbirth'); ?></option>
	                                    <?php 
	                                    for ($i=2017; $i <= 2018; $i++) {
	                                        ?>                                                 
	                                        <option value="<?php echo $i; ?>" <?php if(isset($form_cache) && $form_cache['tahun_lahir'] == $i){ echo 'selected="true"';} ?>><?php echo $i; ?></option>
	                                        <?php 
	                                    } 
	                                    ?>
	                                </select>
		            			</div>
		            			<div class="col-md-3">
		            				<label></label>
		            				<button class="btn bgm-bluegray waves-effect btn-block">Cari</button>
		            			</div>
		            		</div>
	            		</form>
            		</div>
            	</div>
            </div>

			<!-- disini untuk dashboard murid -->               
			<div class="card" style="margin-top: 2%;">
				<div class="card-header"></div>
				<div class="card-body card-padding">
				<div class="row">
				<div class="table-responsive">
					<table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
						<thead>
							<?php
							$tutor_id		= $this->session->userdata('id_user');
							$search_sub 	= null;
							$search_month 	= null;
							$search_years 	= null;
							$otherwhere 	= null;
							if (isset($_GET['subject']) || isset($_GET['month']) || isset($_GET['years'])) {
								$search_sub 	= $_GET['subject'];
								$search_month 	= $_GET['month'];
								$search_years 	= $_GET['years'];
								
								if ($search_sub == "" || $search_month == "" || $search_years == "") {
									$otherwhere = "";
								}
								if (!empty($search_sub) && !empty($search_month) && !empty($search_years)) {
									$otherwhere .= "AND tc.subject_id='".$search_sub."' AND MONTH(tc.start_time) = '".$search_month."' AND YEAR(tc.start_time) = '".$search_years."'";
								}
								if (!empty($search_month) && !empty($search_years)) {
									$otherwhere .= "AND MONTH(tc.start_time) = '".$search_month."' AND YEAR(tc.start_time) = '".$search_years."'";
								}
								if (!empty($search_sub) && !empty($search_month)) {
									$otherwhere .= "AND tc.subject_id='".$search_sub."' AND MONTH(tc.start_time) = '".$search_month."'";
								}
								if (!empty($search_sub) && !empty($search_years)) {
									$otherwhere .= "AND tc.subject_id='".$search_sub."' AND YEAR(tc.start_time) = '".$search_years."'";	
								}
								if(!empty($search_sub)){
									$otherwhere .= " AND tc.subject_id='".$search_sub."'";
								}
								if(!empty($search_month)){							
									$otherwhere .= "AND MONTH(tc.start_time) = '".$search_month."'";
								}
								if(!empty($search_years)){									
									$otherwhere .= "AND YEAR(tc.start_time) = '".$search_years."'";
								}																							
								else
								{
									$otherwhere .= "";
								}
							}							
							
							$select_all 	= $this->db->query("SELECT tcd.order_id, tcd.final_price, tcd.final_pricetutor, tcp.harga, tc.* FROM tbl_class as tc LEFT JOIN tbl_order_detail as tcd on tc.class_id=tcd.class_id LEFT JOIN tbl_class_price as tcp ON tcp.class_id=tc.class_id WHERE tc.tutor_id='$tutor_id' AND tc.class_type!='multicast_channel_paid' AND tc.class_type!='private_channel' AND tc.class_type!='group_channel' $otherwhere")->result_array();
							?>
							<tr>
								<th data-column-id="no">No</th>
								<th>Date Time</th>
								<th>Subject</th>
								<th>Desc</th>
								<th>Level</th>
								<th>Duration</th>
								<th>Price</th>
								<th>Type</th>
								<th>Paid</th>
							</tr>
						</thead>
						<tbody>  
							<?php
							$no=1;
							foreach ($select_all as $row => $v) {								
								$subject_id		= $v['subject_id'];
								$data_subject 	= $this->db->query("SELECT * FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.subject_id='$subject_id'")->row_array();
								$duration 		= strtotime($v['finish_time']) - strtotime($v['start_time']);
								$convertduration= gmdate("H:i", $duration). ' hour';
								$price          = $v['final_price'] != null ? $v['final_pricetutor'] : $v['harga'];
								$typeprice      = $v['final_price'] != null ? 'demand' : 'paid';
								$converprice 	= number_format($price, 0, ".", ".");
								$type 			= null;
								$user_utc		= $this->session->userdata('user_utc');
								$server_utcc    = $this->Rumus->getGMTOffset();				
								$intervall      = $user_utc - $server_utcc;				
					            $datetime       = DateTime::createFromFormat ('Y-m-d H:i:s',$v['start_time']);
					            $datetime->modify("+".$intervall ." minutes");
					            $datetime       = $datetime->format('d-m-Y H:i:s');
								$datepaid 		= $this->db->query("SELECT approval_time FROM tbl_order WHERE order_id='$v[order_id]'")->row_array()['approval_time'];
								if (!empty($datepaid)) {																															           
						            $tanggal        = DateTime::createFromFormat ('Y-m-d H:i:s',$datepaid);
						            $tanggal->modify("+".$intervall ." minutes");
						            $tanggal        = $tanggal->format('Y-m-d H:i:s');
					        	}
					        	else
					        	{
					        		$tanggal 	= "-";
					        	}

								if($v['class_type'] == 'private'){ $type='Private'; }else if($v['class_type'] == 'group'){ $type='Group'; } else if($v['class_type'] == 'multicast_paid'){ $type='Multicast Paid'; } else { $type='-';}
								?>								
								<tr>
									<td><?php echo($no); ?></td>
									<td><?php echo $datetime; ?></td>
									<td><?php echo $data_subject['subject_name']; ?></td>
									<td><?php echo($v['description']); ?></td>
									<td><?php echo $data_subject['jenjang_name'].' - '.$data_subject['jenjang_level']; ?></td>
									<td><?php echo $convertduration; ?></td>
									<td><?php echo 'Rp '.$price; ?></td>
									<td><?php echo $type; ?></td>
									<td><?php echo $tanggal; ?></td>
								</tr>
								<?php 
								$no++;
							}
							?>		
						</tbody>   
					</table>   
				</div>
				</div>
				</div>
			</div>
		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	/*$(document).on('click', "a.choose_ajax", function(e) {
		e.preventDefault();
		$(this).removeClass('choose_ajax');
    	alert('ke choose');
	});
	$(document).on('click', "a.unchoose_ajax", function() {
    	alert('ke unchoose');
	});*/
	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();

	$(document).on('click', "a.choose_ajax", function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax');
					that.attr('href',"<?php echo base_url('/master/unchoosesub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('tidakikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		
	});
	$(document).on('click','a.unchoose_ajax', function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block choose_ajax');
					that.attr('href',"<?php echo base_url('/master/choosesub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('ikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		
	});
	
</script>