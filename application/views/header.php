<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="SkfZM0Q0l2y7V6v4Vxxugw86hU81Uh15QpgIvi1IJ6o" />
    <title>FisiPro</title>
        
    <link rel="shortcut icon" href="https://rltclass.com.sg/assets/landing/img/rlt_ico.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/preload.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/plugins.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/style.light-blue-500.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/width-boxed.min.css" id="ms-boxed" disabled="">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">

  </head>
  
  <style type="text/css">
      /* scroller browser */
      ::-webkit-scrollbar {
          width: 5px;
      }

      /* Track */
      ::-webkit-scrollbar-track {
          -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
          -webkit-border-radius: 7px;
          border-radius: 7px;
      }

      /* Handle */
      ::-webkit-scrollbar-thumb {
          -webkit-border-radius: 7px;
          border-radius: 7px;
          background: #a6a5a5;
          -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
      }
      ::-webkit-scrollbar-thumb:window-inactive {
          background: rgba(0,0,0,0.4); 
      }
  </style>
  <body >
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery/dist/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="<?php echo base_url(); ?>aset/landing_page/js/plugins.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/landing_page/js/app.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>aset/sel2/js/select2.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

  