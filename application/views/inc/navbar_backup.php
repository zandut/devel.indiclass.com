<!-- <style type="text/css">
    /* scroller browser */
::-webkit-scrollbar {
    width: 8px;
}

/* Track */
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
    -webkit-border-radius: 7px;
    border-radius: 7px;
}

/* Handle */
::-webkit-scrollbar-thumb {
    -webkit-border-radius: 7px;
    border-radius: 7px;
    background: #a6a5a5;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
}
::-webkit-scrollbar-thumb:window-inactive {
    background: rgba(0,0,0,0.4); 
}
</style> -->
<ul class="header-inner" >

    <li id="menu-trigger" data-trigger="#sidebar">
        <div class="line-wrap">
            <div class="line top"></div>
            <div class="line center"></div>
            <div class="line bottom"></div>
        </div>
    </li>

    <li class="hidden-xs">
        <?php if ($this->session->userdata('status')==1) { ?>
            <a href="<?php echo base_url(); ?>" class="m-l-10"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/logoclass6.png" alt=""></a>            
        <?php }?>
        <?php if ($this->session->userdata('status')== 0) { ?>
            <a href="#" class="m-l-10"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/logoclass6.png" alt=""></a>            
        <?php }?>
        <?php if ($this->session->userdata('status')== 2) { ?>
            <a href="#" class="m-l-10"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/logoclass6.png" alt=""></a>            
        <?php }?>
        <?php if ($this->session->userdata('status')== 3) { ?>
            <a href="#" class="m-l-10"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/logoclass6.png" alt=""></a>            
        <?php }?>        
        
    </li>

    <li class="pull-right">
        <ul class="top-menu">
            <!-- <li id="toggle-width">
                <div class="toggle-switch">
                    <input id="tw-switch" type="checkbox" hidden="hidden">
                    <label for="tw-switch" class="ts-helper"></label>
                </div>
            </li> -->
            
            <?php
                $getusertype_id = $this->session->userdata('usertype_id');                
                $status = $this->session->userdata('status');
                if ($getusertype_id == "tutor" && $status == "1") {                                
            ?>
            <li>
                <a href='#' id="btn_callsupport"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/callsupport.png" width="35" height="35" title="Call Support"></a>
            </li>
            <?php 
            }
            ?>

            <?php
                $getusertype_id = $this->session->userdata('usertype_id');
                $getiduser = $this->session->userdata('id_user');
                $status = $this->session->userdata('status');
                if ($getusertype_id == "student" || $getusertype_id == "student kid" && $status == "1") {
            ?>
            <li class="dropdown" title="Invoice">
                <a data-toggle="dropdown" href="">
                    <i class="tm-icon zmdi zmdi-file-text"></i>                    
                </a>
                <div class="dropdown-menu dropdown-menu-lg pull-right">
                    <div class="listview">
                        <div class="lv-header">
                            List Invoice
                            <br>
                            <small style="color: #6C7A89; text-transform: lowercase;">(Klik untuk detail)</small>
                        </div>
                        
                            <div class="lv-body" style="overflow-y:auto; height:250px; ">                           
                                <?php
                                    $myid               = $this->session->userdata('id_user');
                                    $allchild   = $this->db->query("SELECT tu.*, tpk.* FROM tbl_profile_kid as tpk INNER JOIN tbl_user as tu ON tpk.kid_id=tu.id_user WHERE tpk.parent_id='$myid'")->result_array();
                                    foreach ($allchild as $row => $child) {
                                        $getdatainvoice   = $this->db->query("SELECT * FROM tbl_invoice as ti INNER JOIN tbl_order_detail as tod ON ti.order_id=tod.order_id INNER JOIN tbl_order as tor ON tor.order_id=tod.order_id WHERE tor.buyer_id='$myid' || tor.buyer_id='$child[id_user]' ORDER BY tor.order_status DESC")->result_array();
                                        if (empty($getdatainvoice)) {
                                            ?>
                                            <div class="lv-item">
                                                <div class="checkbox" style="background-color:#EEEEEE;  cursor:not-allowed;">
                                                    <div class="media" style="border: 1px solid #F2F1EF; padding: 5px; border-radius: 5px; border-collapse: inherit;">                                        
                                                        <div class="media-body">
                                                            <label>Tidak ada Invoice</label>                                     
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        else
                                        {
                                            foreach ($getdatainvoice as $row => $v) {
                                                $invoice_id     = $v['invoice_id'];
                                                $product_id     = $v['product_id'];
                                                $order_id       = $v['order_id'];
                                                $order_status   = $v['order_status'];
                                                $type           = $v['type'];

                                                if ($type == "multicast") {                                                                                        
                                                    $getdetailinvoice = $this->db->query("SELECT tod.*, tr.subject_id as tr_subject_id, tr.tutor_id as tr_tutor_id, tr.approve as tr_approve, trg.subject_id as trg_subject_id, trg.tutor_id as trg_tutor_id, trg.approve as trg_approve, tc.subject_id as trm_subject_id, tc.tutor_id as trm_tutor_id, trm.status as trm_approve, ti.invoice_id FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN tbl_request_multicast as trm ON trm.class_id=tod.product_id LEFT JOIN tbl_invoice as ti ON ti.order_id=tod.order_id INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id WHERE tod.order_id='$order_id' AND tr.request_id='$product_id' OR trg.request_id='$product_id' OR trm.class_id='$product_id' AND ti.invoice_id='$invoice_id'")->row_array();  

                                                    $subject_id     = $getdetailinvoice['tr_subject_id'] != null ? $getdetailinvoice['tr_subject_id'] : ( $getdetailinvoice['trg_subject_id'] != null ? $getdetailinvoice['trg_subject_id'] : $getdetailinvoice['trm_subject_id']);
                                                    $tutor_id       = $getdetailinvoice['tr_tutor_id'] != null ? $getdetailinvoice['tr_tutor_id'] : ( $getdetailinvoice['trg_tutor_id'] != null ? $getdetailinvoice['trg_tutor_id'] : $getdetailinvoice['trm_tutor_id']);
                                                    $status         = $getdetailinvoice['tr_approve'] != null ? $getdetailinvoice['tr_approve'] : ( $getdetailinvoice['trg_approve'] != null ? $getdetailinvoice['trg_approve'] : $getdetailinvoice['trm_approve']);

                                                    $subject_name   = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
                                                    $data_tutor     = $this->db->query("SELECT user_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
                                                    $tutor_name     = $data_tutor['user_name'];
                                                    $tutor_image    = $data_tutor['user_image'];                                              
                                                }
                                                else
                                                {
                                                    $product_id     = $v['product_id'];
                                                    
                                                    $getdetailinvoice = $this->db->query("SELECT tod.*, tr.subject_id as tr_subject_id, tr.tutor_id as tr_tutor_id, tr.approve as tr_approve, trg.subject_id as trg_subject_id, trg.tutor_id as trg_tutor_id, trg.approve as trg_approve, tc.subject_id as trm_subject_id, tc.tutor_id as trm_tutor_id, trm.status as trm_approve, ti.invoice_id FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN tbl_request_multicast as trm ON trm.class_id=tod.product_id LEFT JOIN tbl_invoice as ti ON ti.order_id=tod.order_id INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id WHERE tod.order_id='$order_id' AND tr.request_id='$product_id' OR trg.request_id='$product_id' OR trm.class_id='$product_id' AND ti.invoice_id='$invoice_id'")->row_array();  

                                                    $subject_id     = $getdetailinvoice['tr_subject_id'] != null ? $getdetailinvoice['tr_subject_id'] : ( $getdetailinvoice['trg_subject_id'] != null ? $getdetailinvoice['trg_subject_id'] : $getdetailinvoice['trm_subject_id']);
                                                    $tutor_id       = $getdetailinvoice['tr_tutor_id'] != null ? $getdetailinvoice['tr_tutor_id'] : ( $getdetailinvoice['trg_tutor_id'] != null ? $getdetailinvoice['trg_tutor_id'] : $getdetailinvoice['trm_tutor_id']);
                                                    $status         = $getdetailinvoice['tr_approve'] != null ? $getdetailinvoice['tr_approve'] : ( $getdetailinvoice['trg_approve'] != null ? $getdetailinvoice['trg_approve'] : $getdetailinvoice['trm_approve']);

                                                    $subject_name   = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
                                                    $data_tutor     = $this->db->query("SELECT user_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
                                                    $tutor_name     = $data_tutor['user_name'];
                                                    $tutor_image    = $data_tutor['user_image'];  
                                                }

                                                $subject_id     = $getdetailinvoice['tr_subject_id'] != null ? $getdetailinvoice['tr_subject_id'] : ( $getdetailinvoice['trg_subject_id'] != null ? $getdetailinvoice['trg_subject_id'] : $getdetailinvoice['trm_subject_id']);
                                                $tutor_id       = $getdetailinvoice['tr_tutor_id'] != null ? $getdetailinvoice['tr_tutor_id'] : ( $getdetailinvoice['trg_tutor_id'] != null ? $getdetailinvoice['trg_tutor_id'] : $getdetailinvoice['trm_tutor_id']);
                                                $status         = $getdetailinvoice['tr_approve'] != null ? $getdetailinvoice['tr_approve'] : ( $getdetailinvoice['trg_approve'] != null ? $getdetailinvoice['trg_approve'] : $getdetailinvoice['trm_approve']);

                                                $subject_name   = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
                                                $data_tutor     = $this->db->query("SELECT user_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
                                                $tutor_name     = $data_tutor['user_name'];
                                                $tutor_image    = $data_tutor['user_image'];
                                                // if ($order_status == 2) {                                                                                            
                                                ?>                                
                                                <div class="lv-item" title="Klik untuk detail" style="cursor: pointer; border: 1px solid #F2F1EF; padding: 10px; border-radius: 5px; border-collapse: inherit; margin: 5px;">
                                                    <a class="lv-item" href="https://devel.classmiles.com/DetailPayment?order_id=<?php echo $order_id; ?>">
                                                    <div class="checkbox">
                                                        <div class="media">                                        
                                                            <div class="media-body">                                                        
                                                                <!-- <div class="pull-left" style="height: 60px;">
                                                                    <img class="lv-img-sm" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$tutor_image;?>" alt="" style="width: 40px; height: 40px;">
                                                                </div> -->
                                                                <div class="pull-left" style="border-bottom: 1px solid #EEEEEE; margin-bottom: 5px;">
                                                                    <div class="lv-title c-gray f-13"><b><?php echo $getdetailinvoice['invoice_id'];?></b></div>
                                                                    <small><i><?php echo $getdetailinvoice['created_at'];?></i></small>
                                                                </div>
                                                                <div class="pull-left" style="height: 50px;">
                                                                    <div class="lv-title"><?php echo $tutor_name;?></div>
                                                                    <small class="lv-small"><?php echo $subject_name; ?></small>
                                                                </div>
                                                                <div class="pull-left" style="<?php if($status==2){ echo "display:block;";} else if($status==1){ echo 'display: block'; }else { echo "display:none;"; }?> margin-left: -13%; width: 100%;">
                                                                    <label class="f-12 <?php if($status==2){ echo 'c-orange';}else{ echo 'c-gray';} ?>"><i><?php if($status==2){ echo "Status : Menunggu pembayaran anda"; }else if($status==1){ echo "Status : selesai"; }?></i></label>
                                                                </div>                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </a>
                                                </div>
                                                <?php
                                                // }
                                            }
                                        }
                                    }
                                ?>
                            </div>
                            <!-- style="<?php if($status !=1){ echo "background-color:#EEEEEE;  cursor:not-allowed;"; }?>" href="<?php if($status ==1){ echo 'https://classmiles.com/DetailPayment?order_id='.$order_id.'&u=372';}else { echo '#';}?>" -->
                            <!-- <button class="lv-footer btn btn-success btn-block btn-lg c-white f-14">Bayar</button> -->
                    </div>
                </div>
            </li>
            <?php 
            }
            ?>

            <?php
                $getusertype_id = $this->session->userdata('usertype_id');
                $getiduserr = $this->session->userdata('id_user');
                $status = $this->session->userdata('status');
                if ($getusertype_id == "student" || $getusertype_id == "student kid" && $status == "1") {  
                    $jumlah     = $this->db->query("SELECT count(*) as jumlah FROM tbl_keranjang as tk LEFT JOIN tbl_request as tr ON tk.request_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON tk.request_grup_id=trg.request_id LEFT JOIN  tbl_request_multicast as trm ON tk.request_multicast_id=trm.request_id WHERE tk.id_user='$getiduserr' AND tr.approve=2 OR trg.approve=2 OR trm.status=2")->row_array()['jumlah'];                              
            ?>
            <li class="dropdown" title="Keranjang">
                <a data-toggle="dropdown" href="">
                    <i class="tm-icon zmdi zmdi-shopping-cart"></i>
                    <i class="tmn-counts" style="<?php if($jumlah!=1){ echo 'display: none';} else{ echo "display: block;";}?>"><?php echo $jumlah;?></i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg pull-right">
                    <div class="listview">
                        <div class="lv-header">
                            List Order Class
                        </div>
                        <form action="<?php echo base_url(); ?>process/submitOrder" method="post">
                            <div class="lv-body" style="overflow-y:auto; height:250px; ">                           
                                <?php
                                    $top                = 0;
                                    $myid               = $this->session->userdata('id_user');
                                    // $getdatakeranjang   = $this->db->query("SELECT tk.*, tclass.class_id, tr.approve as tr_approve, trg.approve as trg_approve, trm.status as trm_approve, tr.subject_id as tr_subject_id, trg.subject_id as trg_subject_id, tclass.subject_id as trm_subject_id,  tr.tutor_id as tr_tutor_id, trg.tutor_id as trg_tutor_id, tclass.tutor_id as trm_tutor_id, tr.topic as tr_topic, trg.topic as trg_topic, tclass.description as trm_topic, tr.date_requested as tr_date_requested, trg.date_requested as trg_date_requested, trm.created_at as trm_date_requested FROM tbl_keranjang as tk LEFT JOIN tbl_request as tr ON tk.request_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON tk.request_grup_id=trg.request_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tclass ON trm.class_id=tclass.class_id) ON tk.request_multicast_id=trm.request_id WHERE tk.id_user='$myid'")->result_array();
                                    $allchild   = $this->db->query("SELECT tu.*, tpk.* FROM tbl_profile_kid as tpk INNER JOIN tbl_user as tu ON tpk.kid_id=tu.id_user WHERE tpk.parent_id='$myid'")->result_array();
                                    foreach ($allchild as $row => $child) {                                        
                                        if ($top == 0) {
                                            $getdatakeranjang   = $this->db->query("SELECT tk.*, tclass.class_id, tr.approve as tr_approve, trg.approve as trg_approve, trm.status as trm_approve, tr.subject_id as tr_subject_id, trg.subject_id as trg_subject_id, tclass.subject_id as trm_subject_id,  tr.tutor_id as tr_tutor_id, trg.tutor_id as trg_tutor_id, tclass.tutor_id as trm_tutor_id, tr.topic as tr_topic, trg.topic as trg_topic, tclass.description as trm_topic, tr.date_requested as tr_date_requested, trg.date_requested as trg_date_requested, trm.created_at as trm_date_requested FROM tbl_keranjang as tk LEFT JOIN tbl_request as tr ON tk.request_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON tk.request_grup_id=trg.request_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tclass ON trm.class_id=tclass.class_id) ON tk.request_multicast_id=trm.request_id WHERE tk.id_user='$child[id_user]' || tk.id_user='$myid'")->result_array();
                                            $top    = $top+1;
                                            if (empty($getdatakeranjang)) {
                                                ?>
                                                <div class="lv-item">
                                                    <div class="checkbox" style="background-color:#EEEEEE;  cursor:not-allowed;">
                                                        <div class="media" style="border: 1px solid #F2F1EF; padding: 5px; border-radius: 5px; border-collapse: inherit;">                                        
                                                            <div class="media-body">
                                                                <label>Tidak ada order kelas</label>                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                        else
                                        {
                                            $getdatakeranjang   = $this->db->query("SELECT tk.*, tclass.class_id, tr.approve as tr_approve, trg.approve as trg_approve, trm.status as trm_approve, tr.subject_id as tr_subject_id, trg.subject_id as trg_subject_id, tclass.subject_id as trm_subject_id,  tr.tutor_id as tr_tutor_id, trg.tutor_id as trg_tutor_id, tclass.tutor_id as trm_tutor_id, tr.topic as tr_topic, trg.topic as trg_topic, tclass.description as trm_topic, tr.date_requested as tr_date_requested, trg.date_requested as trg_date_requested, trm.created_at as trm_date_requested FROM tbl_keranjang as tk LEFT JOIN tbl_request as tr ON tk.request_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON tk.request_grup_id=trg.request_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tclass ON trm.class_id=tclass.class_id) ON tk.request_multicast_id=trm.request_id WHERE tk.id_user='$child[id_user]'")->result_array();
                                            
                                        }
                                                                       
                                        // else
                                        // {
                                            foreach ($getdatakeranjang as $row => $v) {
                                                $subject_id     = $v['tr_subject_id'] != null ? $v['tr_subject_id'] : ( $v['trg_subject_id'] != null ? $v['trg_subject_id'] : $v['trm_subject_id'] );
                                                $tutor_id       = $v['tr_tutor_id'] != null ? $v['tr_tutor_id'] : ( $v['trg_tutor_id'] != null ? $v['trg_tutor_id'] : $v['trm_tutor_id'] );
                                                $status         = $v['tr_approve'] != null ? $v['tr_approve'] : ( $v['trg_approve'] != null ? $v['trg_approve'] : $v['trm_approve'] );
                                                $topic          = $v['tr_topic'] != null ? $v['tr_topic'] : ( $v['trg_topic'] != null ? $v['trg_topic'] : $v['trm_topic'] );
                                                $date_requested = $v['tr_date_requested'] != null ? $v['tr_date_requested'] : ( $v['trg_date_requested'] != null ? $v['trg_date_requested'] : $v['trm_date_requested'] );

                                                $subject_name   = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
                                                $data_tutor     = $this->db->query("SELECT user_name,first_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
                                                $tutor_name     = $data_tutor['first_name'];
                                                $tutor_image    = $data_tutor['user_image'];
                                                $request_id     = $v['request_id'];
                                                $request_grup_id= $v['request_grup_id'];
                                                $harga          = $v['price'];
                                                $hargaprice     = number_format($harga, 0, ".", ".");
                                            ?>                                
                                            <div class="lv-item" style="<?php if($status !=2){ echo "background-color:#EEEEEE;  cursor:not-allowed;"; }?>border: 1px solid #F2F1EF; padding: 5px; border-radius: 5px; border-collapse: inherit; margin: 5px;">
                                                <!-- <a class="lv-item" href=""> -->
                                                <div class="checkbox">
                                                    <div class="media" >                                        
                                                        <div class="media-body">
                                                            <div class="pull-left" style="margin-top: 3%;">
                                                                <label class="checkbox">
                                                                    <input type="checkbox" <?php echo $status != 2 ? 'disabled' : '';  ?> id="checkbox<?php echo $v['krnj_id'];?>" name='orderKrnj[]' value="<?php echo $v['krnj_id'];?>" class="booking">
                                                                    <i class="input-helper"></i>                                                    
                                                                </label>
                                                            </div>
                                                            <div class="pull-left">
                                                                <img class="lv-img-sm" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$tutor_image;?>" alt="" style="width: 40px; height: 40px;">
                                                            </div>
                                                            <div class="pull-left m-l-10 row" style="word-wrap: break-word;">
                                                                <div class="lv-title"><?php echo $tutor_name.' - Rp. '.$hargaprice;?></div>
                                                                <small class="lv-small" style="word-wrap: break-word;"><p><?php echo $subject_name; ?></p></small>
                                                                <!-- <small class="lv-small"><?php echo ", Rp ".$hargaprice; ?></small> -->
                                                            </div>
                                                            <!-- <div class="pull-right" style="height: 30px; margin-left: -7%; margin-top: 5%;">
                                                                <label>Rp. <?php echo $hargaprice;?></label>
                                                            </div> -->
                                                            <div class="col-md-12" style="<?php if($status==2){ echo "display:block;";} else if($status==1){ echo 'display: block'; } else if($status==0){ echo 'display: block'; } else if ($status==-2){ echo "display: none"; } else { echo "display:none;"; }?> height: 35px; margin-left: -8%; margin-top: 5%; width: 100%;">
                                                                <label class="f-12 c-gray"><i><?php if($status==0){ echo "Status : Menunggu persetujuan tutor"; }else if($status==2){ echo "Status : Proses pemilihan pembayaran"; } else if ($status==-2){ echo "Status: Pembelian expired"; }?></i></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- </a> -->
                                            </div>
                                            <?php
                                            }
                                        // }
                                    }
                                ?>
                            </div>
                            <!-- <a class="lv-footer btn btn-success btn-lg c-white f-14" id="gotoorder">Bayar</a> -->
                            <!-- <input type="submit" name="" value="Bayar" class="btn btn-success bgm-green btn-lg f-14 c-white"> -->
                            <button class="lv-footer btn btn-success btn-block btn-lg c-white f-14" id="btnbayar" disabled="false">Lanjutkan pembayaran</button>
                        </form>  
                    </div>
                </div>
            </li>
            <?php 
            }
            ?>

            <li class="dropdown" id="notif" title="Notifikasi" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;" id_user="<?php echo $this->session->userdata('id_user');?>">
                <a data-toggle="dropdown" href="" ng-init="getnotif()">
                    <i class="tm-icon zmdi zmdi-notifications"></i>
                    <i class='tmn-counts bara' ng-if="nes > '0'"><span ng-bind="nes"></span></i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg pull-right" >
                    <div class="listview" id="notifications" >
                        <div class="lv-header">
                            <?php echo $this->lang->line('notification') ?>

                            <ul class="actions" data-toggle="tooltip" title="Read all" data-placement="right">
                                <li class="dropdown">   
                                    <a class="readsa" style="cursor: pointer;" id_user='<?php echo $this->session->userdata('id_user');?>'>
                                        <i class="zmdi zmdi-check-all" ></i>
                                    </a>                                                                  
                                </li>
                            </ul>
                        </div>                        
                        <div class="lv-body" id="scol" style="overflow-y:auto; height:350px; " when-scrolled="loadData()" style="cursor: pointer;">
                            <div ng-repeat='x in dataf' style="cursor: pointer;">
                                <a class="lv-item tif" ng-if="x.read_status == '1'" hrefa="{{x.link}}" notif_id="{{x.notif_id}}">
                                    <div class="media" data-toggle="tooltip" title="">
                                        <div class="pull-left">
                                            <img width="65px" height="65px" style="border-radius: 5px;" src="<?php $this->session->userdata('user_image'); ?>"> 
                                        </div>
                                        <div class="media-body" style="overflow:hidden; word-wrap: break-word;">
                                            <div style="font-size: 11px; color:black;">{{x.notif}}</div>                                            
                                        </div>
                                        <div class="media-footer">
                                            <small class="c-black" style="position:absolute; bottom:0px; margin-bottom: 1px; margin-top: auto;">{{x.interval_time}}</small>
                                        </div>
                                    </div>
                                </a>
                                <a class="lv-item tif" style="background: #ECF0F1;" ng-if="x.read_status == '0'" hrefa="{{x.link}}" notif_id="{{x.notif_id}}" >
                                    <div class="media">
                                        <div class="pull-left">
                                            <img width="65px" height="65px" src="<?php $this->session->userdata('user_image'); ?>"> 
                                        </div>
                                        <div class="media-body" style="overflow:hidden; word-wrap: break-word;">
                                            <div style="font-size: 11px; color:black;">{{x.notif}}</div>                                            
                                        </div>
                                        <div class="media-footer">
                                            <small class="c-black" style="position:absolute; bottom:0px; margin-bottom: 1px; margin-top: auto;">{{x.interval_time}}</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div ng-show="lodingnav">
                                <center><div class="preloader pl-lg">
                                    <svg class="pl-circular" viewBox="25 25 50 50">
                                        <circle class="plc-path" cx="50" cy="50" r="20" />
                                    </svg>
                                </div></center>
                            </div>  
                        </div>                     

                    </div>

                </div>
            </li>            

            <li class="dropdown" style="height: 35px;">
                <a data-toggle="dropdown" href="">
                     
                    <?php
                        $patokan =  $this->lang->line('profil');

                        if ($patokan == "Profile") {
                            ?>
                            <img class="m-t-5" width="35px" height="20px" style="border-radius:15%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'language/flaginggris.png'; ?>" >               
                            <?php
                        }
                        else if ($patokan == "Data Diri"){
                            ?>
                            <img class="m-t-5" width="35px" height="20px" style="border-radius:15%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'language/flagindo.png'; ?>" >               
                            <?php
                        }
                    ?>                           
                </a>
                <div class="dropdown-menu dropdown-menu-lg pull-right">
                    <div class="listview" id="notifications">

                        <div class="lv-header">
                            <?php echo $this->lang->line('chooselanguage'); ?>                                                            
                        </div>

                        <div class="lv-body" id="checklang">                            
                            <a class="lv-item" href="<?php echo base_url('set_lang/english'); ?>" id="btn_setenglish">
                                <div class="media">
                                    <div class="pull-left">
                                        <img width="35px" height="20px" style="border-radius:15%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>language/flaginggris.png" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">English</div>                                                    
                                    </div>
                                </div>
                            </a>  
                            <a class="lv-item" href="<?php echo base_url('set_lang/indonesia'); ?>" id="btn_setindonesia">
                                <div class="media">
                                    <div class="pull-left">
                                        <img width="35px" height="20px" style="border-radius:15%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>language/flagindo.png" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">Bahasa Indonesia</div>                                                    
                                    </div>
                                </div>
                            </a>         
                            <!-- <a class="lv-item" href="<?php echo base_url('set_lang/arab'); ?>">
                                <div class="media">
                                    <div disabled="true" class="pull-left">
                                        <img class="lv-img-sm" src="<?php echo base_url(); ?>language/saudi_arabia.png" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">Arabic</div>                                                    
                                    </div>
                                </div>
                            </a> -->                                      
                        </div>                                                
                    </div>

                </div>
            </li>

            <li class="dropdown">
                <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                <ul class="dropdown-menu dm-icon pull-right">
                   <!--  <li class="skin-switch hidden-xs">
                        <span class="ss-skin bgm-lightblue" data-skin="lightblue" id="lightblue"></span>
                        <span class="ss-skin bgm-bluegray" data-skin="bluegray" id="bluegray"></span>
                        <span class="ss-skin bgm-cyan" data-skin="cyan" id="cyan"></span>
                        <span class="ss-skin bgm-teal" data-skin="teal" id="teal"></span>
                        <span class="ss-skin bgm-orange" data-skin="orange" id="orange"></span>
                        <span class="ss-skin bgm-blue" data-skin="blue" id="blue"></span>
                    </li> -->
                    <li class="hidden-xs">
                        <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i><?php echo $this->lang->line('togglefullscreen'); ?></a>
                    </li>
                    <li class="divider hidden-xs"></li>
                    
                    <li>
                        <a onclick="signOut()" data-action="" href="<?php echo base_url('/logout'); ?>"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/keluar1-01.svg" style="margin-left: -7%; margin-right: 6%; height: 23px; width: 23px;">  <?php echo $this->lang->line('logout'); ?></a>
                    </li>
                </ul>
            </li>
           <!--  <li class="hidden-xs" id="chat-trigger" data-trigger="#chat">
                <a href=""><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>
            </li> -->
        </ul>
    </li>
</ul>
<!-- Top Search Content -->
<!-- <div id="top-search-wrap">
    <div class="tsw-inner">
        <i id="top-search-close" class="zmdi zmdi-arrow-left"></i>
        <input type="text">
    </div>
</div> -->

<script type="text/javascript">
    $(document).ready(function(){

        var $subscribeInput = $('input[name="orderKrnj[]"]');

        $subscribeInput.on('click', function(){
            if ( $(this).is(':checked') )
                $("#btnbayar").removeAttr('disabled');
            else
                $("#btnbayar").attr('disabled','disabled');
        });

        $('#btn_setindonesia').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
        });
        $('#btn_setenglish').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
        });

        $('#lightblue').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','lightblue'); ?>',function(){  });
        });
        $('#bluegray').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','bluegray'); ?>',function(){ location.reload(); });                       
        });
        $('#cyan').click(function(e){   
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','cyan'); ?>',function(){ location.reload(); });           
        });
        $('#teal').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','teal'); ?>',function(){ location.reload(); });           
        });
        $('#orange').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','orange'); ?>',function(){ location.reload(); });         
        });
        $('#blue').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','blue'); ?>',function(){ location.reload(); });           
        });
        $('#btn_callsupport').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url(); ?>process/callsupport',function(ret){
                ret = JSON.parse(ret);
                if(ret['status'] == true){
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',ret['message']);
                }else{
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',ret['message']);
                }
                

            });
        });
    });
</script>