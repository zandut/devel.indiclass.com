
<div class="pm-overview c-overflow">

	<div class="pmo-pic"  style="cursor:pointer;">
		<div class="p-relative" data-target="#modalColor" data-toggle="modal">
			<a>				
				<img class="img-responsive" src="<?php echo base_url('aset/img/user/'.$dataimage['user_image']); ?>" > 
			</a>

			<div class="pmop-edit" style="cursor:pointer;">
				<i class="zmdi zmdi-camera"></i> <span class="hidden-xs" >Update Profile Picture</span>
			</div>
		</div>

		<!-- <div id="userpic" class="userpic">
			<div class="js-preview userpic__preview" id="kode"></div>
			<center><div class="btn btn-success js-fileapi-wrapper">
				<div class="js-browse">
					<span class="btn-txt">Choose</span>
					<input type="file" name="filedata"/>
				</div>
				<div class="js-upload" style="display: none;">
					<div class="progress progress-success"><div class="js-progress bar"></div></div>
					<span class="btn-txt">Uploading</span>
				</div>
			</div></center>
		</div>

		<script>

			examples.push(function (){


				$('#userpic').fileapi({
					url: 'http://rubaxa.org/FileAPI/server/ctrl.php',
					accept: 'image/*',
					imageSize: { minWidth: 512, minHeight: 512 },
					elements: {
						active: { show: '.js-upload', hide: '.js-browse' },
						preview: {
							el: '.js-preview',
							width: 200,
							height: 200
						},
						progress: '.js-progress'
					},
					onSelect: function (evt, ui){
						var file = ui.files[0];

						if( !FileAPI.support.transform ) {
							alert('Your browser does not support Flash :');
						}
						else if( file ){
							$("#popup").css('display','none');
							$('#popup').modal({
								closeOnEsc: true,
								closeOnOverlayClick: false,
								onOpen: function (overlay){
									$(overlay).on('click', '.js-upload', function (){
										$.modal().close();
										$('#userpic').fileapi('upload');																				
									});

									$('.js-img', overlay).cropper({
										file: file,
										bgColor: '#000',
										maxSize: [$(window).width()-100, $(window).height()-100],
										minSize: [512, 512],
										selection: '90%',
										onSelect: function (coords){
											$('#userpic').fileapi('crop', file, coords);							
										}
									});
								}
							}).open();

						}
					}
				});
			});
		</script>
		<div id="popup" class="popup" style="position:absolute; z-index:10; top:10%; display: none; ">
			<div class="popup__body" style="width:600px; height:600px;"><div class="js-img" id="js_img"></div></div>
			<div style="margin: 0 0 5px; text-align: center;">
				<div class="js-upload btn btn_browse btn_browse_small" id="save_img">Save</div>
			</div>
		</div> -->
		
	</div>

	<div class="pmo-block pmo-contact hidden-xs">
		<h2><?php echo $this->lang->line('contact'); ?></h2>

		<ul>
			<li><i class="zmdi zmdi-phone"></i> <?php echo $this->session->userdata('no_hp'); ?></li>
			<li><i class="zmdi zmdi-email"></i> <?php echo $this->session->userdata('email'); ?></li>                                    
			<li>
				<i class="zmdi zmdi-pin"></i>
				<address class="m-b-0 ng-binding">
					<?php 
					$address = $this->session->userdata('address');
					if ($address == "") {
						echo "No address";
					}
					echo $this->session->userdata('address');
					?>
				</address>
			</li>            
		</ul>

	</div>

	<div id="btn-color-targets">
	<div class="modal fade" data-modal-color="" id="modalColor" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:30%;">
        <div class="modal-content">
        <form id="form1">
            <div class="modal-header">
                <center><h3 class="modal-title" style="color:gray;">Select your photo</h3>
                <hr>
                </center>
            </div>
            <div class="modal-body">
                	<center>
						
					<input type="file" name="inputFile" id="inputFile" class="filestyle" data-buttonBefore="true">	
					<img src="<?php echo base_url('aset/img/user/'.$dataimage['user_image']);?>" width="360" height="360" id="viewgambar" class="m-t-20 m-b-20">
					</center>
					<script>
					function readURL(input) {
					        if (input.files && input.files[0]) {
					            var reader = new FileReader();

					            reader.onload = function (e) {
					                $('#viewgambar').attr('src', e.target.result);
					            }

					            reader.readAsDataURL(input.files[0]);
					        }
					    }

					    $("#inputFile").change(function () {
					        readURL(this);
					    });

					    $(":file").filestyle({
					            buttonBefore: true
					        });
					</script>  	
            </div>
            <div class="progress progress-striped active m-t-10" style="height:18px;">
				<div class="progress-bar" style="width:0%; height:150px;"></div>
			</div>
            <div class="modal-footer bgm-teal">
                <button type="submit" class="btn btn-info upload" id="save" name="save">Save</button>                
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
    </div>
    </div>

</div>

<script type="text/javascript">
	
	jQuery(document).ready(function(){

		// PROGRESS BAR
		$(document).on('submit','form',function(e){
			e.preventDefault();

			$form = $(this);

			uploadImage($form);

		});

		function uploadImage($form){
			$form.find('.progress-bar').removeClass('progress-bar-success')
										.removeClass('progress-bar-danger');

			var formdata = new FormData($form[0]); //formelement
			var request = new XMLHttpRequest();

			//progress event...
			request.upload.addEventListener('progress',function(e){
				var percent = Math.round(e.loaded/e.total * 100);
				$form.find('.progress-bar').width(percent+'%').html(percent+'%');
			});

			//progress completed load event
			request.addEventListener('load',function(e){
				
				$form.find('.progress-bar').addClass('progress-bar-success').html('Upload Completed...');
				// alert('Profile Completed Change');
				setTimeout(function(){
					window.location.reload();
		        }, 1000);
			});

			request.open('post', '<?php echo base_url(); ?>Master/update_photo');
			request.send(formdata);

			$form.on('click','.cancel',function(){
				request.abort();

				$form.find('.progress-bar')
					.addClass('progress-bar-danger')
					.removeClass('progress-bar-success')
					.html('upload aborted...');
			});

		}

		$('body').on('click', '#btn-color-targets > .btn', function(){
            var color = $(this).data('target-color');
            $('#modalColor').attr('data-modal-color', color);
        });

		jQuery('#change-pic').on('click', function(e){
	        jQuery('#changePic').show();
			jQuery('#change-pic').hide();
	        
	    });
		
		jQuery('#photoimg').on('change', function()   
		{ 
			jQuery("#preview-avatar-profile").html('');
			jQuery("#preview-avatar-profile").html('Uploading....');
			jQuery("#cropimage").ajaxForm(
			{
			target: '#preview-avatar-profile',
			success:    function() { 
					jQuery('img#photo').imgAreaSelect({
					aspectRatio: '1:1',
					onSelectEnd: getSizes,
				});
				jQuery('#image_name').val(jQuery('#photo').attr('file-name'));
				}
			}).submit();

		});
		
		jQuery('#btn-crop').on('click', function(e){
	    e.preventDefault();
	    params = {
	            targetUrl: '<?php echo base_url(); ?>/First/post_profile/save',
	            action: 'save',
	            x_axis: jQuery('#hdn-x1-axis').val(),
	            y_axis : jQuery('#hdn-y1-axis').val(),
	            x2_axis: jQuery('#hdn-x2-axis').val(),
	            y2_axis : jQuery('#hdn-y2-axis').val(),
	            thumb_width : jQuery('#hdn-thumb-width').val(),
	            thumb_height:jQuery('#hdn-thumb-height').val()
	        };

	        saveCropImage(params);
	    });
	    
	 
	    
	    function getSizes(img, obj)
	    {
	        var x_axis = obj.x1;
	        var x2_axis = obj.x2;
	        var y_axis = obj.y1;
	        var y2_axis = obj.y2;
	        var thumb_width = obj.width;
	        var thumb_height = obj.height;
	        if(thumb_width > 0)
	            {

	                jQuery('#hdn-x1-axis').val(x_axis);
	                jQuery('#hdn-y1-axis').val(y_axis);
	                jQuery('#hdn-x2-axis').val(x2_axis);
	                jQuery('#hdn-y2-axis').val(y2_axis);
	                jQuery('#hdn-thumb-width').val(thumb_width);
	                jQuery('#hdn-thumb-height').val(thumb_height);
	                
	            }
	        else
	            alert("Please select portion..!");
	    }
	    
	    function saveCropImage(params) {
	    jQuery.ajax({
	        url: params['targetUrl'],
	        cache: false,
	        dataType: "html",
	        data: {
	            action: params['action'],
	            id: jQuery('#hdn-profile-id').val(),
	             t: 'ajax',
	                                w1:params['thumb_width'],
	                                x1:params['x_axis'],
	                                h1:params['thumb_height'],
	                                y1:params['y_axis'],
	                                x2:params['x2_axis'],
	                                y2:params['y2_axis'],
									image_name :jQuery('#image_name').val()
	        },
	        type: 'Post',
	       // async:false,
	        success: function (response) {
	                jQuery('#changePic').hide();
					jQuery('#change-pic').show();
	                jQuery(".imgareaselect-border1,.imgareaselect-border2,.imgareaselect-border3,.imgareaselect-border4,.imgareaselect-border2,.imgareaselect-outer").css('display', 'none');
	                
	                jQuery("#avatar-edit-img").attr('src', response);
	                jQuery("#preview-avatar-profile").html('');
	                jQuery("#photoimg").val('');
	        },
	        error: function (xhr, ajaxOptions, thrownError) {
	            alert('status Code:' + xhr.status + 'Error Message :' + thrownError);
	        }
	    });
	    }
		});

</script>
