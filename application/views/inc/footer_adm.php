        <!-- Javascript -->
        <!-- Vendors -->
        <!-- <script src="../assets/admin/vendors/bower_components/jquery/dist/jquery.min.js"></script> -->
        

        <script src="../aset/channel/vendors/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="../aset/channel/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../aset/channel/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="../aset/channel/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="../aset/channel/vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>
        <script src="../aset/channel/vendors/bower_components/Waves/dist/waves.min.js"></script>

        <!-- <script src="../aset/channel/vendors/bower_components/flot/jquery.flot.js"></script> -->
        <!-- <script src="../aset/channel/vendors/bower_components/flot/jquery.flot.resize.js"></script> -->
        <!-- <script src="../aset/channel/vendors/bower_components/flot.curvedlines/curvedLines.js"></script> -->
        <!-- <script src="../aset/channel/vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="../aset/channel/vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script> -->
        <!-- <script src="../aset/channel/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script> -->
        <!-- <script src="../aset/channel/vendors/bower_components/salvattore/dist/salvattore.min.js"></script> -->
        <script src="../aset/channel/vendors/jquery.sparkline/jquery.sparkline.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
        <script src="../aset/channel/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>

        <!-- Charts and maps-->
        <script src="../aset/channel/demo/js/flot-charts/curved-line.js"></script>
        <script src="../aset/channel/demo/js/flot-charts/line.js"></script>
        <script src="../aset/channel/demo/js/flot-charts/chart-tooltips.js"></script>
        <script src="../aset/channel/demo/js/other-charts.js"></script>
        <script src="../aset/channel/demo/js/jqvmap.js"></script>
        <script src="../aset/channel/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
        <script src="../aset/channel/vendors/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>        

        <!-- App functions and actions -->
        <script src="../aset/channel/js/app.min.js"></script>

        <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> -->
         <!-- Vendors: Data tables -->
        
        <script type="text/javascript" src="https://infra.clarin.eu/content/libs/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
        <script src="../aset/channel/vendors/bower_components/flatpickr/dist/flatpickr.min.js"></script>
        <script src="../aset/channel/js/bootstrap-material-datetimepicker.js"></script>
        <script type="text/javascript" src="../aset/channel/js/bootstrap-filestyle.min.js"> </script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fecha/2.3.3/fecha.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script type="text/javascript">
            function notify(from, align, icon, type, animIn, animOut, message){
                $.notify({
                    icon: icon,
                    title: '',
                    message: message,
                    url: ''
                },{
                    element: 'body',
                    type: type,
                    allow_dismiss: true,
                    placement: {
                        from: from,
                        align: align
                    },
                    offset: {
                        x: 20,
                        y: 90
                    },
                    spacing: 10,
                    z_index: 9999,
                    delay: 3500,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: false,
                    animate: {
                        enter: animIn,
                        exit: animOut
                    },
                    icon_type: 'class',
                    template:   '<div data-notify="container" class="alert alert-dismissible alert-{0} alert--notify" role="alert">' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '<button type="button" aria-hidden="true" data-notify="dismiss" class="alert--notify__close">X</button>' +
                    '</div>'
                });
            }
        </script>   

          
    </body>
</html>