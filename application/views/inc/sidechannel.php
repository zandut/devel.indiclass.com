        <?php 
            $channel_id = $this->session->userdata('channel_id');
            $channel_logo = $this->session->userdata('channel_logo');
            $get_image = $this->db->query("SELECT * FROM master_channel WHERE channel_id='".$channel_id."'")->row_array();
        ?>
<style type="text/css">
/*    .profile-menu > a {
  display: block;
  height: 129px;
  margin-bottom: 5px;
  width: 100%;
  background: url(img/widgets/preview.jpg) no-repeat left top;
  background-size: 100%;
}*/
</style>
<div class="profile-menu">
    <a href="">

        <div class="profile-pic">
            <img class="img-responsive" src="<?php if ($channel_logo =null){ echo base_url('/aset/img/channel/logo/empty.jpg'); } else { echo base_url('/aset/img/channel/logo/'.$get_image['channel_logo'].'?'.time()); } ?>" >
        </div>

        <div class="profile-info">

            <?php 
                $idadmin = $this->session->userdata('id_user');
                $cek     = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$idadmin'")->result_array();
                foreach ($cek as $row => $a) {
                    echo $a['user_name'];
                }
            ?>        
        
            <i class="zmdi zmdi-caret-down"></i>
        </div>
    </a>

    <ul class="main-menu">
        <li>
            <a href="<?php echo base_url('/logout'); ?>"><i class="zmdi zmdi-time-restore"></i> <?php echo $this->lang->line('logout'); ?></a>
        </li>
    </ul>
</div>
<ul class="main-menu">
    <li class="    
        <?php if($sideactive=="dashboard"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>Channel/dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a>
    </li>

    <li class="    
        <?php if($sideactive=="tutorchannel"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>Channel/Tutor"><i class="zmdi zmdi-accounts-list-alt"></i> Tutor Channel</a>
    </li>

    <li class="    
        <?php if($sideactive=="studentchannel"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>Channel/Student"><i class="zmdi zmdi-account-circle"></i>  Student Channel</a>
    </li>

    <li class="    
        <?php if($sideactive=="listclasschannel"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>Channel/ListClass"><i class="zmdi zmdi-account"></i>  List Class</a>
    </li>

    <li class="    
        <?php if($sideactive=="reporting"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>Channel/ReportPoint"><i class="zmdi zmdi-assignment"></i>  Reporting</a>
    </li>
    <li class="    
        <?php if($sideactive=="setting"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>Channel/setting"><i class="zmdi zmdi-settings"></i>  Setting Channel</a>
    </li>
</ul>