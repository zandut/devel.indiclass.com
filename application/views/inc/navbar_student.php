<div id="navbar-container" class="boxed">
    <!--Brand logo & name-->
    <!--================================-->
    <div class="navbar-header">
        <a href="<?php echo BASE_URL();?>/Siswa/KelasSaya" class="navbar-brand">
            <img src="../assets/indiclass/img/logo_atas.png" style="padding: 15px;" alt="Nifty Logo" class="brand-icon">
            <div class="brand-title">
                <span class="brand-text">Indiclass</span>
            </div>
        </a>
    </div>
    <!--================================-->
    <!--End brand logo & name-->

    <!--Navbar Dropdown-->
    <!--================================-->
    <div class="navbar-content">
        <ul class="nav navbar-top-links">

            <!--Navigation toogle button-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <li class="tgl-menu-btn">
                <a class="mainnav-toggle" href="#">
                    <i class="fa fa-navicon"></i>
                </a>
            </li>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End Navigation toogle button-->

        </ul>
        <ul class="nav navbar-top-links">

            <!--User dropdown-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <li>
                <a href="<?php echo BASE_URL();?>Siswa/Logout" class="text-right">
                    <span class="ic-user pull-right">                        
                        <i class="fa fa-sign-out" title="Keluar"></i>                        
                    </span>
                </a>

                <!-- <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right panel-default">
                    <ul class="head-list">
                        <li>
                            <a href="<?php echo BASE_URL();?>Siswa/DataDiri"><i class="fa fa-user icon-lg icon-fw"></i> Profile</a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL();?>Siswa/Logout"><i class="fa fa-sign-out icon-lg icon-fw"></i> Logout</a>
                        </li>
                    </ul>
                </div> -->
            </li>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End user dropdown-->

        </ul>
    </div>
    <!--================================-->
    <!--End Navbar Dropdown-->
</div>