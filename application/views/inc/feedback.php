<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
</head>
 
<body>
  	
  	<div class="modal" id="modalconfrim" style="margin-top: 1%;" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">	                    	
                    <!-- <button type="button" class="close" data-dismiss="modal"><i class="zmdi zmdi-close"></i></button> -->
                    <center>
                        <h2 class="lead modal-title c-black" style="font-size: 24px;" id="titlefeedback">Apa kelas telah terlaksana ?</h2>                        
                    </center>     
                    <hr>

                	<div id="show_choosecase">                		
                        <div class="col-md-12 c-gray m-10">                            
                            <center>                            	
                            	<img class="media-object img-responsive" id="image" style="height: 130px; width: 130px;" alt="" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/rating.png">                            	
                        	</center>
                        
                            <div class="media-demo col-md-12 m-b-20">
                            	<center><label class="m-l-20 f-18 c-gray lead"></label></center>
                                <div class="media">
                                	<p class="c-gray f-19 lead">Bahasa Indonesia</p>    
                                    <p class="c-gray f-15 lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <div class="col-md-6 f-15">
                                        <button title="Type Class" class="btn btn-danger btn-icon waves-effect waves-circle waves-float m-r-10"><i class="zmdi zmdi-view-web"></i></button>
                                        Private Class
                                    </div>
                                    <div class="col-md-6 f-15">
                                        <button title="Date" class="btn btn-warning btn-icon waves-effect waves-circle waves-float m-r-10"><i class="zmdi zmdi-calendar"></i></button>
                                        2018-02-06 13:30:00
                                    </div>
                                </div>                                
                            </div>                                                        	                           
                        </div>                          
                    </div>
                    <div id="show_caseno" style="display: none;">
                        <center>                        
                            <img class="lv-img m-b-10" id="image" style="height: 130px; width: 130px;" alt="" src="http://www.asterfone.com/icons/complaint.png">
                        </center>
                        <p class="c-gray f-15 m-t-20">Masalah yang anda terima : </p>
                        <div class="radio m-b-15 c-gray f-14">
                            <label>
                                <input type="radio" name="answer_komplain" value="1">
                                <i class="input-helper"></i>    
                                Tutor tidak menguasai materi                   
                            </label>
                        </div>
                        <div class="radio m-b-15 c-gray f-14">
                            <label>
                                <input type="radio" name="answer_komplain" value="2">
                                <i class="input-helper"></i>   
                                Kualitas video tidak jernih/jelas                    
                            </label>
                        </div>
                        <div class="radio m-b-15 c-gray f-14">
                            <label>
                                <input type="radio" name="answer_komplain" value="3">
                                <i class="input-helper"></i>
                                Tutor tidak lucu                       
                            </label>
                        </div>
                        <div class="radio m-b-15 c-gray f-14">
                            <label>
                                <input type="radio" name="answer_komplain" value="lainnya">
                                <i class="input-helper"></i>  
                                Lainnya                     
                            </label>
                        </div>
                        <div style="margin-top: 3%; display: none;" id="box_morecomplain">
                            <textarea rows="4" class="form-control fg-input p-10 f-14 lead" name="more_complain" placeholder="Isi keluhan anda" style="border: 1px solid #EEEEEE;"></textarea>
                        </div>
                    </div>
                    <div id="show_caseyes" style="display: none;">
                        <center>                     
                            <p>
                                <img class="lv-img" id="image" style="height: 130px; width: 130px;" alt="" src="https://cdn.classmiles.com/usercontent/dXNlci9ad21UdFAySC5qcGc=">
                            </p>
                            <p class="c-gray f-20 lead">
                                Robith Ritz
                            </p>
                            <div id="rateYo"></div>
                            <!-- <button id="getRating" >Get Rating</button> -->
                            <div style="margin-top: 3%;">
                                <textarea rows="4" class="form-control fg-input p-10 f-14 lead" placeholder="Isi ulasan anda" style="border: 1px solid #EEEEEE;"></textarea>
                            </div>                            
                        </center>
                        <p class="c-gray f-14">
                            <label class="c-gray f-15 m-b-10">Please give us more information</label>
                            <br>
                            <label class="checkbox checkbox-inline m-r-20 m-b-10">
                                <input type="checkbox" name='ratingus[]' value="1">   
                                <i class="input-helper"></i>    
                                Gambar jernih/jelas/tidak putus
                            </label>
                            <br>
                            <label class="checkbox checkbox-inline m-r-20 m-b-10">
                                <input type="checkbox" name='ratingus[]' value="2">                                
                                <i class="input-helper"></i>
                                Tutor menguasai materi
                            </label>
                            <br>
                            <label class="checkbox checkbox-inline m-r-20">
                                <input type="checkbox" name='ratingus[]' value="3">
                                <i class="input-helper"></i>
                                Tutor menawan
                            </label>
                        </p>
                    </div>                                 
                </div> 
                <div class="modal-footer">
                    <div class="footer_showcase">
                    	<div class="col-md-6">
                        	<button id="btn_caseno" type="button" class="btn btn-danger btn-block">Tidak</button>
                        </div>
                        <div class="col-md-6">
                        	<button id="btn_caseyes" type="button" class="btn btn-success btn-block">Ya</button>
                        </div>
                    </div>
                    <div class="footer_showyes" style="display: none;">                                                
                            <button id="btn_submit" type="button" class="btn btn-success btn-block">Kirim</button>                        
                    </div>
                    <div class="footer_showno" style="display: none;">                                                
                            <button id="btn_submit" type="button" class="btn btn-success btn-block">Kirim Komplain</button>                        
                    </div>
                </div>                                                             
            </div>
        </div>
    </div>

  	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
</body>
<script type="text/javascript">
	$(function () {
		var rateYo = $("#rateYo").rateYo({
            starWidth: "30px",
            fullStar: true,
            spacing: "5px",
            onSet: function (rating, rateYoInstance) {
                alert("Rating is set to: " + rating);
            }
        });
        rateYo.rateYo("option", "onChange", function () {
            /* get the rated fill at the current point of time */
            var ratedFill = rateYo.rateYo("option", "ratedFill");        
        });
        
        /* set the option `multiColor` to show Multi Color Rating */
        rateYo.rateYo("option", "multiColor", true);

		$("#getRating").click(function () {
			/* get rating */
			var rating = rateYo.rateYo("rating");
			window.alert("Its " + rating + " Yo!");
		}); 
	});


    $("#modalconfrim").modal('show');
    $("#btn_caseno").click(function(){
        $("#titlefeedback").text();
        $("#titlefeedback").text("Buka Komplain");

        $(".footer_showcase").css('display','none');
        $(".footer_showyes").css('display','none');
        $("#show_choosecase").css('display','none');
        $("#show_caseyes").css('display','none');
        $("#show_caseno").css('display','block');
        $(".footer_showno").css('display','block');
    });

    $("#btn_caseyes").click(function(){
        $("#titlefeedback").text();
        $("#titlefeedback").text("Mohon tinjau pengalaman Anda");

        $(".footer_showcase").css('display','none');
        $(".footer_showno").css('display','none');
        $("#show_choosecase").css('display','none');
        $("#show_caseno").css('display','none');
        $("#show_caseyes").css('display','block');
        $(".footer_showyes").css('display','block');
    });

    $('input[type=radio][name=answer_komplain]').change(function() {
        if (this.value == "lainnya") {
            $("#box_morecomplain").css('display','block');
        }
        else
        {
            $("#box_morecomplain").css('display','none');
        }
    });
</script>

