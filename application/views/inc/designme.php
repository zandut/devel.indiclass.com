 <style type="text/css">
	/*  bhoechie tab */
	div.bhoechie-tab-container{
		background-color: #ffffff;
		padding: 0 !important;
		border-radius: 4px;
		-moz-border-radius: 4px;
		border:1px solid #ddd;
		margin-top: 20px;
		-webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
		box-shadow: 0 6px 12px rgba(0,0,0,.175);
		-moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
		background-clip: padding-box;
		opacity: 0.97;
		filter: alpha(opacity=97);
	}
	div.bhoechie-tab-menu{
		padding-right: 0;
		padding-left: 0;
		padding-bottom: 0;
	}
	div.bhoechie-tab-menu div.list-group{
	  	margin-bottom: 0;
	}
	div.bhoechie-tab-menu div.list-group>a{
	  	margin-bottom: 0;
	}
	div.bhoechie-tab-menu div.list-group>a .glyphicon,
	div.bhoechie-tab-menu div.list-group>a .fa {
	  	color: #5A55A3;
	}
	div.bhoechie-tab-menu div.list-group>a:first-child{
	  	border-top-right-radius: 0;
	  	-moz-border-top-right-radius: 0;
	}
	div.bhoechie-tab-menu div.list-group>a:last-child{
	  	border-bottom-right-radius: 0;
	  	-moz-border-bottom-right-radius: 0;
	}
	div.bhoechie-tab-menu div.list-group>a.active,
	div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
	div.bhoechie-tab-menu div.list-group>a.active .fa{
	  	background-color: #5A55A3;
	  	background-image: #5A55A3;
	  	color: #ffffff;
	}
	div.bhoechie-tab-menu div.list-group>a.active:after{
		content: '';
		position: absolute;
		left: 100%;
		top: 50%;
		margin-top: -13px;
		border-left: 0;
		border-bottom: 13px solid transparent;
		border-top: 13px solid transparent;
		border-left: 10px solid #5A55A3;
	}
	div.bhoechie-tab-content{
		background-color: #ffffff;
		/* border: 1px solid #eeeeee; */
		padding-left: 20px;
		padding-top: 10px;
	}
	div.bhoechie-tab div.bhoechie-tab-content:not(.active){
	  	display: none;
	}
</style>
<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2><?php echo $this->lang->line('reportteaching'); ?></h2>				
			</div> <!-- akhir block header    -->        
			<br>
			<?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>
			
			<div class="container">
			    <div class="row">
			        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
			            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bhoechie-tab-menu">
			              <div class="list-group">
			                <a href="#" class="list-group-item active text-center">
			                  <h4 class="glyphicon glyphicon-plane"></h4><br/>List Quiz
			                </a>
			                <a href="#" class="list-group-item text-center">
			                  <h4 class="glyphicon glyphicon-road"></h4><br/>Train
			                </a>
			                <a href="#" class="list-group-item text-center">
			                  <h4 class="glyphicon glyphicon-home"></h4><br/>Hotel
			                </a>
			                <a href="#" class="list-group-item text-center">
			                  <h4 class="glyphicon glyphicon-cutlery"></h4><br/>Restaurant
			                </a>
			              </div>
			            </div>
			            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab">
			                <!-- flight section -->
			                <div class="bhoechie-tab-content active">
			                    <div class="table-responsive">
									<table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
										<thead>
											<?php
											$tutor_id		= $this->session->userdata('id_user');
											$select_all 	= $this->db->query("SELECT ms.subject_name, mj.jenjang_name, mj.jenjang_level, mbs.* FROM master_banksoal as mbs INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id) On mbs.subject_id=ms.subject_id WHERE mbs.tutor_id='$tutor_id'")->result_array();
											?>
											<tr>
												<th data-column-id="no">No</th>
												<th>Quiz Name</th>
												<th width="1%">Quiz Description</th>
												<th>Quiz Subject</th>
												<th>Quiz Jenjang</th>
												<th>Quiz Date</th>
												<th>Expired Date Quiz</th>
												<th>Duration</th>
												<th>Type</th>
												<th>Options</th>
											</tr>
										</thead>
										<tbody>  
											<?php
											$no=1;
											foreach ($select_all as $row => $v) {	
												$quiz_name 		= $v['name'];
												$quiz_desc 		= $v['description'];
												$subject_id		= $v['subject_id'];
												$subject_name 	= $v['subject_name'];
												$jenjang 		= $v['jenjang_level'].' - '.$v['jenjang_name'];
												$duration 		= $v['duration'];
												$type 			= $v['template_type'];

												$user_utc		= $this->session->userdata('user_utc');
												$server_utcc    = $this->Rumus->getGMTOffset();				
												$intervall      = $user_utc - $server_utcc;				
									            $validity       = DateTime::createFromFormat ('Y-m-d H:i:s',$v['validity']);
									            $validity->modify("+".$intervall ." minutes");
									            $validity       = $validity->format('d-m-Y H:i:s');
									            $invalidity     = DateTime::createFromFormat ('Y-m-d H:i:s',$v['invalidity']);
									            $invalidity->modify("+".$intervall ." minutes");
									            $invalidity     = $invalidity->format('d-m-Y H:i:s');								
												?>								
												<tr>
													<td><?php echo($no); ?></td>
													<td><?php echo $quiz_name; ?></td>
													<td><?php echo $quiz_desc; ?></td>
													<td><?php echo $subject_name; ?></td>
													<td><?php echo $jenjang; ?></td>
													<td><?php echo $validity; ?></td>
													<td><?php echo $invalidity; ?></td>
													<td><?php echo $duration; ?></td>
													<td><?php echo $type; ?></td>
													<td>-</td>
												</tr>
												<?php 
												$no++;
											}
											?>		
										</tbody>   
									</table>   
								</div>
			                </div>
			                <!-- train section -->
			                <div class="bhoechie-tab-content">
			                    <center>
			                      <h1 class="glyphicon glyphicon-road" style="font-size:12em;color:#55518a"></h1>
			                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
			                      <h3 style="margin-top: 0;color:#55518a">Train Reservation</h3>
			                    </center>
			                </div>
			    
			                <!-- hotel search -->
			                <div class="bhoechie-tab-content">
			                    <center>
			                      <h1 class="glyphicon glyphicon-home" style="font-size:12em;color:#55518a"></h1>
			                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
			                      <h3 style="margin-top: 0;color:#55518a">Hotel Directory</h3>
			                    </center>
			                </div>
			                <div class="bhoechie-tab-content">
			                    <center>
			                      <h1 class="glyphicon glyphicon-cutlery" style="font-size:12em;color:#55518a"></h1>
			                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
			                      <h3 style="margin-top: 0;color:#55518a">Restaurant Diirectory</h3>
			                    </center>
			                </div>
			                <div class="bhoechie-tab-content">
			                    <center>
			                      <h1 class="glyphicon glyphicon-credit-card" style="font-size:12em;color:#55518a"></h1>
			                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
			                      <h3 style="margin-top: 0;color:#55518a">Credit Card</h3>
			                    </center>
			                </div>
			            </div>
			        </div>
			  	</div>
			</div>
				          
            <div class="card" style="margin-top: 2%;">
            	<div class="card-body p-15">
            		<div class="row">
            			<form role="form" action="<?php  echo base_url(); ?>tutor/ReportTeaching" method="get" >
		            		<div class="col-md-12 col-xs-12">
		            			<div class="col-md-3">
		            				<label>Mata Pelajaran</label>
		            				<select class="select2 form-control" id="subject" name="subject">
		                                <?php
                                        	$id = $this->session->userdata("id_user");
                                            $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' order by ms.subject_id ASC")->result_array();
                                            echo '<option selected="" value="">Semua Pelajaran</option>'; 
                                            foreach ($allsub as $row => $v) {                                            
                                                echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_level'].' '.$v['jenjang_name'].'</option>';
                                            }
                                        ?>
		                            </select>
		            			</div>		            			
		            			<div class="col-md-3">
		            				<label></label>
		            				<button class="btn bgm-bluegray waves-effect btn-block">Cari</button>
		            			</div>
		            		</div>
	            		</form>
            		</div>
            	</div>
            </div>

			<!-- disini untuk dashboard murid -->               
			<div class="card" style="margin-top: 2%;">
				<div class="card-header">
					<button class="pull-right btn-success"></button>
				</div>
				<div class="card-body card-padding">
				<div class="row">
				<div class="table-responsive">
					<table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
						<thead>
							<?php
							$tutor_id		= $this->session->userdata('id_user');
							$select_all 	= $this->db->query("SELECT ms.subject_name, mj.jenjang_name, mj.jenjang_level, mbs.* FROM master_banksoal as mbs INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id) On mbs.subject_id=ms.subject_id WHERE mbs.tutor_id='$tutor_id'")->result_array();
							?>
							<tr>
								<th data-column-id="no">No</th>
								<th>Quiz Name</th>
								<th>Quiz Description</th>
								<th>Quiz Subject</th>
								<th>Quiz Jenjang</th>
								<th>Quiz Date</th>
								<th>Expired Date Quiz</th>
								<th>Duration</th>
								<th>Type</th>
								<th>Options</th>
							</tr>
						</thead>
						<tbody>  
							<?php
							$no=1;
							foreach ($select_all as $row => $v) {	
								$quiz_name 		= $v['name'];
								$quiz_desc 		= $v['description'];
								$subject_id		= $v['subject_id'];
								$subject_name 	= $v['subject_name'];
								$jenjang 		= $v['jenjang_level'].' - '.$v['jenjang_name'];
								$duration 		= $v['duration'];
								$type 			= $v['template_type'];

								$user_utc		= $this->session->userdata('user_utc');
								$server_utcc    = $this->Rumus->getGMTOffset();				
								$intervall      = $user_utc - $server_utcc;				
					            $validity       = DateTime::createFromFormat ('Y-m-d H:i:s',$v['validity']);
					            $validity->modify("+".$intervall ." minutes");
					            $validity       = $validity->format('d-m-Y H:i:s');
					            $invalidity     = DateTime::createFromFormat ('Y-m-d H:i:s',$v['invalidity']);
					            $invalidity->modify("+".$intervall ." minutes");
					            $invalidity     = $invalidity->format('d-m-Y H:i:s');							
								?>								
								<tr>
									<td><?php echo($no); ?></td>
									<td><?php echo $quiz_name; ?></td>
									<td><?php echo $quiz_desc; ?></td>
									<td><?php echo $subject_name; ?></td>
									<td><?php echo $jenjang; ?></td>
									<td><?php echo $validity; ?></td>
									<td><?php echo $invalidity; ?></td>
									<td><?php echo $duration; ?></td>
									<td><?php echo $type; ?></td>
									<td>-</td>
								</tr>
								<?php 
								$no++;
							}
							?>		
						</tbody>   
					</table>   
				</div>
				</div>
				</div>
			</div>
		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	/*$(document).on('click', "a.choose_ajax", function(e) {
		e.preventDefault();
		$(this).removeClass('choose_ajax');
    	alert('ke choose');
	});
	$(document).on('click', "a.unchoose_ajax", function() {
    	alert('ke unchoose');
	});*/
	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();

	$(document).on('click', "a.choose_ajax", function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax');
					that.attr('href',"<?php echo base_url('/master/unchoosesub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('tidakikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		
	});
	$(document).on('click','a.unchoose_ajax', function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block choose_ajax');
					that.attr('href',"<?php echo base_url('/master/choosesub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('ikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		
	});
	
</script>
<script type="text/javascript">
    $(document).ready(function() {
	    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
	        e.preventDefault();
	        $(this).siblings('a.active').removeClass("active");
	        $(this).addClass("active");
	        var index = $(this).index();
	        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
	        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
	    });
	});
</script>