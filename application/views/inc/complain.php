<style type="text/css">
    .bp-header:hover
    {
        cursor: pointer;
    }
    .ci-avatar img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .ci-avatar:hover img {
        opacity: 0.6;
    }
</style>
<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php 
            if ($this->session->userdata('usertype_id') == 'tutor') {
                $this->load->view('./inc/sidetutor');    
            }
            else
            {
                $this->load->view('inc/side');
            }
            ?>
	</aside>

	<section id="content">
        <div class="container">
            <div class="block-header">
                <h2>Komplain</h2>
            </div>
        
            <div class="card m-b-0" id="messages-main">
                
                <div class="ms-menu bgm-white">
                    <div class="ms-block" style="background-color: #f8f8f8;">
                        <div class="ms-user">                            
                            <div class="f-20" style="margin-top: -4%; margin-bottom: 4%;">Komplain</div>
                        </div>
                    </div>                                    
                    
                    <div class="listview lv-user" id="box_listusercomplain" style="height: 500px; max-height: 500px; overflow-y: auto;">                                                

                    </div>
                    
                </div>
                
                <div class="ms-body">
                    <div class="listview lv-message">
                        <div class="lv-header-alt clearfix">
                            <div id="ms-menu-trigger">
                                <div class="line-wrap">
                                    <div class="line top"></div>
                                    <div class="line center"></div>
                                    <div class="line bottom"></div>
                                </div>
                            </div>

                            <div class="lvh-label">
                                <div class="lv-avatar pull-left">
                                    <img src="" alt="" id="display_image">
                                </div>
                                <span class="c-black" id="display_name"></span><span class="c-gray" id="display_subjectname"></span>
                                
                            </div>                  
                            <div class="lv-actions actions pull-right" id="box_completecomplain" style="display: none;"><button class="btn btn-success" classid="" id="komplain_selesai"><i class="zmdi zmdi-check-all"></i> Selesai</button></div>                                  

                        </div>
                        
                        <div class="lv-body" style="height: 500px; max-height: 500px; overflow-y: auto;" id="box_chatting">                            
                                                                            
                        </div>
                        
                        <div class="lv-footer ms-reply" id="footer_submit" style="display: none;">
                            <div id="footer_0" style="display: none;">
                                <input type="text" id="class_id_selected" hidden>
                                <textarea  placeholder="Diskusikan masalah anda disini..." id="submit_textwrittercomplain"></textarea>                            
                                <button><i class="zmdi zmdi-mail-send" id="submit_chatcomplain"></i></button>
                            </div>
                            <div id="footer_1" style="display: none;">
                                <center>
                                    <label class="c-black f-18 lead">Komplain masalah anda telah selesai. Terima kasih</label>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

</section>

<script type="text/javascript">
    $(document).ready(function() {
	    $(function () {
            var tgl = new Date();  
            var formattedDate = moment(tgl).format('YYYY-MM-DD');         
            
            $('#validity_quiz').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });

            $('#invalidity_quiz').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });
            
        });

        var nowactive = null;
        var url = null;
        var usertype = "<?php echo $this->session->userdata('usertype_id');?>"; 
        var user_utc = "<?php echo $this->session->userdata('user_utc');?>";      
        var student_id = null;  
        var active_list = null;
        if (usertype == 'student') {
            url = "<?php echo base_url(); ?>Rating/polling_resolution/";
        }
        else if(usertype == 'tutor'){
            url = "<?php echo base_url(); ?>Rating/polling_resolution/tutor"
        }
        $.ajax({
            url: url,
            data: {
                user_utc : user_utc
            },
            type: 'POST',                
            success: function(response)
            {       
                result = JSON.parse(response);
                console.warn(result);
                var a               = 0;
                var box_empty       = "<div class='lv-item media' style='margin-top: 5%;'><center><img src='https://cdn.classmiles.com/sccontent/baru/no_message_3.png' alt='' style='height: 300px; width: 300px;'><br><label class='f-18 lead c-gray m-t-20'>Pilih Diskusi terlebih dahulu</label></center></div>";
                var box_listempty   = "<div class='lv-item media' style='margin-top: 50%;'><center><img src='https://cdn.classmiles.com/sccontent/baru/no_list.png' alt='' style='height: 100px; width: 100px;'><br><label class='f-16 lead c-gray m-t-20'>Tidak ada komplain</label></center></div>"
                $("#box_chatting").append(box_empty);
                if (result == "") {
                    $("#box_listusercomplain").append(box_listempty);    
                }
                else
                {
                    for (var i = result.length-1; i >=0;i--) {
                        var class_id        = result[i]['class_id'];                        
                        var resolution_status= result[i]['resolution_status'];
                        var last_update     = result[i]['last_update'];
                        var user_name       = result[i]['user_name'];
                        var user_image      = result[i]['user_image'];
                        var subject_name    = result[i]['subject_name'];
                        var description     = result[i]['description'];
                        student_id          = result[i]['id_user'];                    
                        // var alt_img         = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+user_image;
                        var active          = "background-color: rgba(228, 241, 254, 0.3);";                        
                        var top             = null;
                        var cselesai        = null;
                        var status          = null;
                        var color           = null;
                        var ccolor          = null;
                        if(a == 0){a=1; top="m-t-10";}else{top=null;}
                        if(resolution_status==1){ cselesai= "background-color: rgba(228, 241, 254, 0.3);"; status='Selesai'; color = '#2ECC71'; ccolor='#00E640'}else{ cselesai = null; status="Proses"; color='#CF000F'; ccolor='#F22613'}
                        var box_listusercomplain = 
                        "<div class='lv-item media ci-avatar "+top+"' id='box_"+i+"' data-classid='"+class_id+"' data-iduser='"+student_id+"' style='cursor: pointer; border-bottom: 1px solid #EEEEEE;'><div class='pull-left'><img class='lv-img-sm' src='"+user_image+"' alt=''></div><div class='media-body'><div class='lv-title'>"+user_name+"</div><div class='p-5 c-white f-10' style='display: inline-block; background-color:"+ccolor+"'>"+status+"</div><div class='lv-small m-t-5'>"+subject_name+"</div><div class='lv-small'>"+description+"</div><small class='c-gray pull-left'>"+last_update+"</small></div></div>";                        

                        $("#box_listusercomplain").append(box_listusercomplain);
                    }   
                }             
            }
        });

        function get_resolution(class_id,id_user) {
            $.ajax({
                url: '<?php echo base_url(); ?>Rating/get_resolution/',
                type: 'POST',
                data: {
                    class_id : class_id,                    
                    id_user : id_user,
                    user_utc : user_utc
                },                
                success: function(response)
                {       
                    result = JSON.parse(response);                    
                    var resolution_status = result['resolution_status'];
                    var subject_name = result['subject_name'];
                    var description = result['description'];
                    var user_name = result['user_name'];
                    var user_image = result['user_image'];                    
                    var jenjang = result['jenjang_name']+' - '+result['jenjang_level'];
                    $("#display_image").attr('src','<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>'+user_image);
                    $("#display_name").text(user_name+' | ');
                    $("#display_subjectname").text(subject_name+' - '+jenjang);                    
                    if (resolution_status == 0) {
                        if (usertype == 'student') {
                            $("#footer_1").css('display','none');
                            $("#box_completecomplain").css('display','block');
                            $("#footer_submit").css('display','block');                        
                            $("#footer_0").css('display','block');    
                        }
                        else
                        {
                            $("#footer_1").css('display','none');                            
                            $("#footer_submit").css('display','block');                        
                            $("#footer_0").css('display','block');
                        }
                    }
                    else
                    {
                        $("#footer_0").css('display','none');
                        $("#box_completecomplain").css('display','none');
                        $("#footer_submit").css('display','block');                        
                        $("#footer_1").css('display','block');
                    }
                    for (var i = 0; i < result['resolution_text'].length; i++) {
                        var writer_id       = result['resolution_text'][i]['writer_id'];
                        var writer_name     = result['resolution_text'][i]['writer_name'];
                        var writer_image    = result['resolution_text'][i]['writer_image'];
                        var writer_text     = result['resolution_text'][i]['writer_text'];
                        var writer_time     = result['resolution_text'][i]['writer_time'];
                        
                        if (usertype == 'student') {
                            if (writer_id == id_user) {
                                var box_chatstudent = 
                                "<div class='lv-item media right'><div class='lv-avatar pull-right'><img src='"+writer_image+"' alt=''></div><div class='media-body'><div class='ms-item'>"+writer_text+"</div><small class='ms-date'><small><i class='zmdi zmdi-account-box-o'></i> Anda</small> | <i class='zmdi zmdi-time'></i> "+writer_time+"</small></div></div>";
                                $("#box_chatting").append(box_chatstudent);
                            }
                            else
                            {
                                var box_chattutor = 
                                "<div class='lv-item media'><div class='lv-avatar pull-left'><img src='"+writer_image+"' alt=''></div><div class='media-body'><div class='ms-item'>"+writer_text+"</div><small class='ms-date'><small><i class='zmdi zmdi-account-box-o'></i> "+writer_name+"</small> | <i class='zmdi zmdi-time'></i> "+writer_time+"</small></div></div>";
                                $("#box_chatting").append(box_chattutor);
                            }   
                        }
                        else
                        {
                            if (writer_id == id_user) {
                                var box_chatstudent = 
                                "<div class='lv-item media'><div class='lv-avatar pull-left'><img src='"+writer_image+"' alt=''></div><div class='media-body'><div class='ms-item'>"+writer_text+"</div><small class='ms-date'><small><i class='zmdi zmdi-account-box-o'></i> "+writer_name+"</small> | <i class='zmdi zmdi-time'></i> "+writer_time+"</small></div></div>";
                                $("#box_chatting").append(box_chatstudent);
                            }
                            else
                            {
                                var box_chattutor = 
                                
                                "<div class='lv-item media right'><div class='lv-avatar pull-right'><img src='"+writer_image+"' alt=''></div><div class='media-body'><div class='ms-item'>"+writer_text+"</div><small class='ms-date'><small><i class='zmdi zmdi-account-box-o'></i> Anda</small> | <i class='zmdi zmdi-time'></i> "+writer_time+"</small></div></div>";
                                $("#box_chatting").append(box_chattutor);
                            }   
                        }
                                 
                    }
                    var d = $('#box_chatting');
                    d.scrollTop(d.prop("scrollHeight"));
                }
            });
        }

	    $(document).on('click', ".ci-avatar", function(e){
            $("#box_chatting").empty();
            var class_id    = $(this).data("classid");
            var id_user    = $(this).data("iduser");
            active_list = $(this).attr('id');            
            $(".ci-avatar").css('background-color','');
            $("#"+active_list).css('background-color','background-color: rgba(228, 241, 254, 0.3);');
            // if(resolution_status==1){ cselesai= "background-color: rgba(228, 241, 254, 0.3);"; status='Selesai'; color = '#2ECC71';}else{ cselesai = null; status="Proses"; color='#CF000F';}
            $("#class_id_selected").val(class_id);
             
            if (usertype=='student') {                
                $("#komplain_selesai").attr('classid',class_id);
            }
            get_resolution(class_id,id_user);
	    });

        $(document).on("click", "#submit_chatcomplain", function () {	        
            var class_id = $("#class_id_selected").val();
            var id_user = student_id;
            var writer_id = "<?php echo $this->session->userdata('id_user');?>";
            var writer_text = $("#submit_textwrittercomplain").val();
            
            $.ajax({
                url: '<?php echo base_url(); ?>Rating/submit_resolution/',
                type: 'POST',    
                data: {
                    class_id : class_id,                    
                    id_user : id_user,
                    writer_id : writer_id,
                    writer_text : writer_text,
                    user_utc : user_utc
                }, 
                success: function(response)
                {
                    var sdata = JSON.parse(response);
                    if (sdata['status'] == 1) {                        
                        $("#class_id_selected").val(class_id);
                        
                        var writer_id = sdata['data']['writer_id'];
                        var writer_name = sdata['data']['writer_name'];
                        var writer_text = sdata['data']['writer_text'];
                        var writer_time = sdata['data']['writer_time'];
                        var writer_image = sdata['data']['writer_image'];
                        if (usertype == 'student') {
                            if (writer_id == id_user) {
                                var box_chatstudent = 
                                "<div class='lv-item media right'><div class='lv-avatar pull-right'><img src='"+writer_image+"' alt=''></div><div class='media-body'><div class='ms-item'>"+writer_text+"</div><small class='ms-date'><small><i class='zmdi zmdi-account-box-o'></i> Anda</small> | <i class='zmdi zmdi-time'></i> "+writer_time+"</small></div></div>";
                                $("#box_chatting").append(box_chatstudent);
                            }
                            else
                            {
                                var box_chattutor = 
                                "<div class='lv-item media'><div class='lv-avatar pull-left'><img src='"+writer_image+"' alt=''></div><div class='media-body'><div class='ms-item'>"+writer_text+"</div><small class='ms-date'><small><i class='zmdi zmdi-account-box-o'></i> "+writer_name+"</small> | <i class='zmdi zmdi-time'></i> "+writer_time+"</small></div></div>";
                                $("#box_chatting").append(box_chattutor);
                            }   
                        }
                        else
                        {
                            if (writer_id == id_user) {
                                var box_chatstudent = 
                                "<div class='lv-item media'><div class='lv-avatar pull-left'><img src='"+writer_image+"' alt=''></div><div class='media-body'><div class='ms-item'>"+writer_text+"</div><small class='ms-date'><small><i class='zmdi zmdi-account-box-o'></i> "+writer_name+"</small> | <i class='zmdi zmdi-time'></i> "+writer_time+"</small></div></div>";
                                $("#box_chatting").append(box_chatstudent);
                            }
                            else
                            {
                                var box_chattutor = 
                                
                                "<div class='lv-item media right'><div class='lv-avatar pull-right'><img src='"+writer_image+"' alt=''></div><div class='media-body'><div class='ms-item'>"+writer_text+"</div><small class='ms-date'><small><i class='zmdi zmdi-account-box-o'></i> Anda</small> | <i class='zmdi zmdi-time'></i> "+writer_time+"</small></div></div>";
                                $("#box_chatting").append(box_chattutor);
                            }   
                        }

                        var d = $('#box_chatting');
                        d.scrollTop(d.prop("scrollHeight"));
                        $("#submit_textwrittercomplain").val("");
                    }
                    else
                    {
                        // $("#modalRating").modal('hide');
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Terjadi kesalahan!");
                        setTimeout(function(){
                            $("#modalRating").modal('show');
                        },1500);
                    }
                }
            });	        
	    });  

        $(document).on("click", "#komplain_selesai", function () {           
            var class_id = $(this).attr("classid");
            var id_user = student_id;            
            
            $.ajax({
                url: '<?php echo base_url(); ?>Rating/complain_solved/',
                type: 'POST',    
                data: {
                    class_id : class_id,                    
                    id_user : id_user,
                    user_utc : user_utc
                }, 
                success: function(response)
                {
                    var sdata = JSON.parse(response);
                    if (sdata['status'] == 1) {      
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Komplain masalah anda telah selesai. Terima kasih");
                        setTimeout(function(){
                            location.reload();
                        },2000);                  
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Terjadi kesalahan!");
                        setTimeout(function(){
                            $("#modalRating").modal('show');
                        },1500);
                    }
                }
            });
        });
	});
</script>