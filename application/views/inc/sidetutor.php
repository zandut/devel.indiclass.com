<?php 
$state = $this->session->userdata('status');
?>
<div class="profile-menu">
    <a href="">
        <?php
            $iduser = $this->session->userdata('id_user');
            $get_image = $this->db->query("SELECT * FROM tbl_user WHERE id_user='".$iduser."'")->row_array();
        ?>
        <div class="profile-pic">
            <img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" src="<?php  echo $this->session->userdata('user_image'); ?>" alt="">
        </div>

        <div class="profile-info">
            <?php echo $get_image['user_name'];
            ?>        
        
            <i class="zmdi zmdi-caret-down"></i>
        </div>
    </a>

    <ul class="main-menu">
        <!-- <li style="display: <?php if($state == 3){ echo "none"; }else if ($state == 2){ echo "none"; } else if ($state == 0){ echo "none"; } else{ echo "inline"; } ?>">
            <a href="<?php echo base_url(); ?>tutor/about"><i class="zmdi zmdi-account"></i> <?php echo $this->lang->line('viewprofile'); ?></a>
        </li>         -->
        <li>
            <a href="<?php echo base_url('/logout'); ?>"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/keluar1-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('logout'); ?></a>
        </li>
    </ul>
</div>
<ul class="main-menu">
    
    <li style="display: <?php if($state == 1){ echo "inline"; } else if($state==2){ echo "inline";} else { echo "none"; }  ?>" class="<?php if($sideactive=="hometutor"){ echo "active";} else{ } ?>">
        <a style="font-size: 14px;" href="<?php echo base_url(); ?>tutor"><i class="zmdi zmdi-home"></i>  <?php echo $this->lang->line('home'); ?></a>
    </li>
    <li style="display: <?php if($state == 1){ echo "inline"; } else if($state==2){ echo "inline";} else { echo "none"; }  ?>" class="<?php if($sideactive=="choosingsubjecttutor"){ echo "sub-menu active";} else if ($sideactive=="subjectadd_choose"){ echo "sub-menu active";} else{ echo "sub-menu"; } ?>">
        <a style="font-size: 14px;"  href=""><i class="zmdi zmdi-dns"></i><?php echo $this->lang->line('subject'); ?></a>
        <ul>
            <li><a class="<?php if($sideactive=="choosingsubjecttutor"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('tutor/choose'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('subject'); ?></a></li>
            <li><a class="<?php if($sideactive=="subjectadd_choose"){ echo "active"; } else { echo ""; } ?>" style="display: <?php if ($state==2) { echo "none"; } else { echo "inline"; } ?>" href="<?php echo base_url('tutor/subject'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;Harga</a></li>
        </ul>
    </li>
    <li style="display: <?php if($state == 1){ echo "inline"; } else if($state==2){ echo "inline";} else { echo "none"; }  ?>" class="<?php if($sideactive=="freeclass"){ echo "sub-menu active";} else if ($sideactive=="paidclass"){ echo "sub-menu active";} else{ echo "sub-menu"; } ?>">
        <a style="font-size: 14px;"  href=""><i class="zmdi zmdi-timer"></i><?php echo $this->lang->line('aturkelas'); ?></a>
        <ul>
            <li><a class="<?php if($sideactive=="freeclass"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('tutor/free'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('gratis'); ?></a></li>
            <li><a class="<?php if($sideactive=="paidclass"){ echo "active"; }else { echo "";} ?>" style="display: <?php if ($state==2) { echo "none"; } else { echo "inline"; } ?>" href="<?php echo base_url('tutor/paid'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('berbayar'); ?></a></li>
            <!-- <li style="display: <?php if($state == 1){ echo "inline"; } else if($state==2){ echo "inline";} else { echo "none"; }  ?>" class="<?php if($sideactive=="freeclass"){ echo "sub-menu active";} else if ($sideactive=="paidclass"){ echo "sub-menu active";} else{ echo "sub-menu"; } ?>">
                <a href="">&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('kelaspribadi'); ?></a>
                <ul>
                    
                </ul>
            </li> -->
            <!-- <li style="display: <?php if($state == 1){ echo "inline"; }else { echo "none"; }  ?>" class="<?php if($sideactive=="privatechannel"){ echo "sub-menu active";} else if ($sideactive=="groupchannel"){ echo "sub-menu active";}else if ($sideactive=="multicastchannel"){ echo "sub-menu active";} else{ echo "sub-menu"; } ?>">
            <a href="">&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('kelaschannel'); ?></a>
                <ul>
                    <li><a class="<?php if($sideactive=="multicastchannel"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('tutor/multicastchannel'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Multicast</a></li>  
                    <li><a class="<?php if($sideactive=="privatechannel"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('tutor/privatechannel'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('privateclass'); ?></a></li>
                    <li><a class="<?php if($sideactive=="groupchannel"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('tutor/groupchannel'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('grupclass'); ?></a></li>
                </ul>
            </li> -->
        </ul>
    </li>
    <li style="display: <?php if($state == 1){ echo "inline"; } else if($state==2){ echo "inline";} else { echo "none"; }  ?>" class="<?php if($sideactive=="buy_class_credit"){ echo "active";} else{ } ?>">
        <a style="font-size: 14px;" href="<?php echo base_url(); ?>Tutor/BuyClassCredit"><i class="zmdi zmdi-shopping-basket"></i>  <?php echo $this->lang->line('buy_class_credit'); ?></a>
    </li>
    <li style="display: <?php if($state == 1){ echo "inline"; } else if($state==2){ echo "inline";} else { echo "none"; }  ?>" class="<?php if($sideactive=="request_trial_class"){ echo "active";} else{ } ?>">
        <a style="font-size: 14px;" href="<?php echo base_url(); ?>Tutor/RequestTrialClass"><i class="zmdi zmdi-shopping-cart-plus"></i>  <?php echo $this->lang->line('request_trial_class'); ?></a>
    </li>
    <?php
        $iduser = $this->session->userdata('id_user');
        $checkterdaftar = $this->db->query("SELECT * FROM master_channel_tutor WHERE tutor_id='".$iduser."'")->row_array();
        if (empty($checkterdaftar)) {
            
        }
        else
        {
    ?>
    <?php
    }
    ?> 

    <li style="display: none;" class="    
        <?php if($sideactive=="setavailabilitytime"){ echo "active";} else{            
        } ?>"> <a style="font-size: 14px;"  href="<?php echo base_url(); ?>tutor/availability"><i class="zmdi zmdi-view-list-alt"></i>  <?php echo $this->lang->line('setavailabilitytime'); ?></a>
    </li>
    <!-- <li style="display: <?php if($state == 1){ echo "inline"; }else { echo "none"; }  ?>" class="<?php if($sideactive=="availabilitytime"){ echo "sub-menu active";} else if ($sideactive=="availabilitysubd"){ echo "sub-menu active";} else{ echo "sub-menu"; } ?>">
        <a href=""><i class="zmdi zmdi-view-list-alt"></i> <?php echo $this->lang->line('setavailabilitytime'); ?></a>
        <ul>
            <li><a class="<?php if($sideactive=="availabilitysubd"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('tutor/availabilitysubd'); ?>">Sub Bahasan</a></li>
            <li><a class="<?php if($sideactive=="availabilitytime"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('tutor/availabilitytime'); ?>">Waktu</a></li>
        </ul>
    </li> -->
    <li style="display: <?php if($state == 1){ echo "inline"; }else { echo "none"; }  ?>" class="<?php if($sideactive=="approvalondemand"){ echo "sub-menu active";} else if ($sideactive=="approvalondemandgroup"){ echo "sub-menu active";} else { echo "sub-menu"; } ?>">
        <a style="font-size: 14px;"  href="<?php echo base_url(); ?>tutor/approval_ondemand"><i class="zmdi zmdi-assignment-check"></i>  <?php echo $this->lang->line('approvalondemand'); ?></a>
        <ul>
            <li><a class="<?php if($sideactive=="approvalondemand"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('tutor/approval_ondemand'); ?>">&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('privateclass'); ?></a></li>
            <li><a class="<?php if($sideactive=="approvalondemandgroup"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('tutor/approval_ondemand_group'); ?>">&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('groupclass'); ?></a></li>
        </ul>
    </li>    
    <li style="display: <?php if($state == 1){ echo "inline"; } else if($state==2){ echo "inline";} else { echo "none"; }  ?>" class="
        <?php if($sideactive=="filemanager"){echo "active";}else{
        } ?>"><a style="font-size: 14px;"  href="<?php echo base_url(); ?>tutor/FileManager"><i class="zmdi zmdi-file-text"></i> File Manager</a>
    </li>
    <li style="display: <?php if($state == 1){ echo "inline"; } else if($state==2){ echo "inline";} else { echo "none"; }  ?>" class="
        <?php if($sideactive=="profiletutor"){echo "active";}else{
        } ?>"><a style="font-size: 14px;"  href="<?php echo base_url(); ?>tutor/about"><i class="zmdi zmdi-account-circle"></i> <?php echo $this->lang->line('tab_account'); ?></a>
    </li>                 
</ul>