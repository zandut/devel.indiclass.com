<div class="profile-menu">
    <a href="">
        <div class="profile-pic">
            <img src="<?php echo $this->session->userdata('user_image');?>" > 
        </div>

        <div class="profile-info">
            <?php echo $this->session->userdata('user_name'); ?>
        
            <i class="zmdi zmdi-caret-down"></i>
        </div>
    </a>
</div>
<ul class="main-menu">
    <!-- <li class="    
        <?php if($sideactive=="datauangsaku"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>finance/DataTransaction"><i class="zmdi zmdi-accounts-list"></i>  Data Transaction</a>
    </li> -->
    <li  class="<?php if($sideactive=="datauangsaku"){ echo "sub-menu active";} else if($sideactive=="confrimpending"){ echo "sub-menu active";} else if($sideactive=="listinvoice"){ echo "sub-menu active";} else if($sideactive=="confrimationpayment"){ echo "sub-menu active";} else if($sideactive=="listpaymenttutor"){ echo "sub-menu active";} else{ echo "sub-menu";           
            } ?>">
        <a href=""><i class="zmdi zmdi-assignment-o"></i> Finance</a>
        <?php
        $useremail = $this->session->userdata('email');
            if ($useremail == "cust_finance@meetaza.com") {
                ?>
                <ul>
                    <!-- <li class="<?php if($sideactive=="datauangsaku"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>finance/DataTransaction">Data Transaction</a></li> -->
                    <li class="<?php if($sideactive=="confrimpending"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>finance/ConfrimPending">Inbound</a></li>
                    <li class="<?php if($sideactive=="confrimationpayment"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>finance/ConfrimationPayment">Confirmation Payment</a></li>
                    <li class="<?php if($sideactive=="listinvoice"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>finance/ListInvoice">Active Invoice</a></li>
                    <li class="<?php if($sideactive=="listpackageinvoice"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>finance/ListPackageInvoice">Package Invoice</a></li>
                    <li class="<?php if($sideactive=="invoiceexpired"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>finance/InvoiceExpired">Expired Invoice</a></li>
                </ul>
                <?php
            }
            else{
                ?>
                <ul>
                    <li class="<?php if($sideactive=="listpaymenttutor"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>finance/paymentTutor">Payment Tutor</a></li>          
                </ul>
                <?php

            }
        ?>
    </li>

    <!-- <li class="<?php if($sideactive=="confrimuangsaku"){ echo "sub-menu active";} else if($sideactive=="confrimondemand"){ echo "sub-menu active";} else if($sideactive=="confrimmulticast"){ echo "sub-menu active";} else{ echo "sub-menu";           
            } ?>">
        <a href=""><i class="zmdi zmdi-assignment-o"></i> Class Confrimation</a>
        <ul>
            <li class="<?php if($sideactive=="confrimuangsaku"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>finance/ConfrimTransaction">Topup Saldo</a></li>
            <li class="<?php if($sideactive=="confrimmulticast"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>finance/ConfrimMulticast"> Multicast Paid</a></li>
            <li class="<?php if($sideactive=="confrimondemand"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>finance/ConfrimOnDemand"> Private / Group</a></li>
        </ul>
    </li> -->
    <!-- <li class="    
        <?php if($sideactive=="confrimuangsaku"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>finance/ConfrimTransaction"><i class="zmdi zmdi-assignment-o"></i>  Confrim Top Up</a>
    </li>
    <li class="    
        <?php if($sideactive=="confrimondemand"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>finance/ConfrimOnDemand"><i class="zmdi zmdi-assignment-o"></i>  Confrim On Demand </a>
    </li> -->
    <li style="display: none;" class="    
        <?php if($sideactive=="alltutor"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>finance/ListTutor"><i class="zmdi zmdi-accounts-list"></i>  List Tutor</a>
    </li>
    <li style="display: none;">
        <a href="<?php echo base_url('/logout'); ?>"><i class="zmdi zmdi-time-restore"></i> <?php echo $this->lang->line('logout'); ?></a>
    </li>  

</ul>