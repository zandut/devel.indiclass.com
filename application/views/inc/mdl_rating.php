<div class="modal" id="modalRating" style="margin-top: 1%;" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">                          

                <button style="display: none; position: absolute;" id="btn_back" class="btn-link" ><i style="font-size: 20px;" class="zmdi zmdi-arrow-left"></i></button>
                <center>
                    <h2 class="lead modal-title c-black" style="font-size: 24px;" id="titlefeedback">Apa kelas telah terlaksana ?</h2>                        
                </center>     
                <hr>
                <input type="text" class="c-gray" id="class_id" hidden>
                <input type="text" class="c-gray" id="class_ack" hidden>
                <div id="show_choosecase">                      
                    <div class="col-md-12 c-gray m-10">                            
                        <center>                                
                            <img class="media-object img-responsive" id="image" style="height: 130px; width: 130px;" alt="" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/rating.png">                                                             
                        </center>
                    
                        <div class="media-demo col-md-12 m-b-20">
                            <center><label class="m-l-20 f-18 c-gray lead"></label></center>
                            <div class="media">
                                <p class="c-gray f-19 lead" id="title_subjectname"></p>    
                                <p class="c-gray f-15 lead" id="title_description"></p>
                                <div class="col-md-6 f-15 m-t-10">
                                    <button title="Type Class" class="btn btn-danger btn-icon waves-effect waves-circle waves-float m-r-10"><i class="zmdi zmdi-view-web"></i></button>
                                    <label id="title_type"></label>
                                </div>
                                <div class="col-md-6 f-15 m-t-10">
                                    <button title="Date" class="btn btn-warning btn-icon waves-effect waves-circle waves-float m-r-10"><i class="zmdi zmdi-calendar"></i></button>
                                    <label id="title_date"></label>
                                </div>
                            </div>                                
                        </div>                                                                                     
                    </div>                          
                </div>
                <div id="show_caseno" style="display: none;">
                    <center>                        
                        <div class="alert alert-danger text-center f-13" role="alert" style="display: none;" id="alert_writter_text">Mohon memilih permasalahan komplain anda!</div>
                        <img class="lv-img m-b-10" id="image" style="height: 130px; width: 130px;" alt="" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>/baru/Complainn.png">
                        <p class="c-gray f-20 lead m-t-10" id="no_tutorname">
                            
                        </p>
                        <p class="c-gray f-14 lead" id="no_detailclass">
                            
                        </p> 
                    </center>
                    <p class="c-gray f-15 m-t-20">Masalah yang anda terima : </p>
                    <div class="radio m-b-15 c-gray f-14">
                        <label>
                            <input type="radio" name="answer_komplain" value="Tutor tidak hadir">
                            <i class="input-helper"></i>    
                            Tutor tidak hadir                  
                        </label>
                    </div>
                    <div class="radio m-b-15 c-gray f-14">
                        <label>
                            <input type="radio" name="answer_komplain" value="Tidak bisa masuk kelas">
                            <i class="input-helper"></i>   
                            Tidak bisa masuk kelas                  
                        </label>
                    </div>                        
                    <div class="radio m-b-15 c-gray f-14">
                        <label>
                            <input type="radio" name="answer_komplain" value="lainnya">
                            <i class="input-helper"></i>  
                            Lainnya                     
                        </label>
                    </div>
                    <div style="margin-top: 3%; display: none;" id="box_morecomplain">
                        <textarea rows="4" class="form-control fg-input p-10 f-14 lead" name="more_complain" id="more_complain" placeholder="Isi keluhan anda" style="border: 1px solid #EEEEEE;"></textarea>
                    </div>
                </div>
                <div id="show_caseyes" style="display: none;">
                    <center>
                        <div class="alert alert-danger text-center f-13" role="alert" style="display: none;" id="alert_class_rate">Mohon tentukan rating dengan memilih bintang!</div>
                        <div class="alert alert-danger text-center f-13" role="alert" style="display: none;" id="alert_class_review">Mohon mengisi ulasan!</div>
                        <div class="alert alert-danger text-center f-13" role="alert" style="display: none;" id="alert_chk">Harap pilih salah satu informasi tambahan!</div>
                        <p>
                            <img class="lv-img" id="yes_imagetutor" style="height: 130px; width: 130px;" alt="" src="">
                        </p>
                        <p class="c-gray f-20 lead" id="yes_tutorname">
                            
                        </p>
                        <p class="c-gray f-14 lead" id="yes_detailclass">
                            
                        </p>
                        <div id="rateTutor"></div>
                        <!-- <button id="getRating" >Get Rating</button> -->
                        <div style="margin-top: 3%;">
                            <textarea id="class_review" rows="4" class="form-control fg-input p-10 f-14 lead" placeholder="Isi ulasan anda" style="border: 1px solid #EEEEEE;"></textarea>
                        </div>                            
                    </center>
                    <p class="c-gray f-14" id="parentId">
                        <label class="c-gray f-15 m-b-10">Please give us more information</label>
                        <br>
                        <label class="checkbox checkbox-inline m-r-20 m-b-10">
                            <input type="checkbox" name='ratingus[]' class="ratingus" value="Gambar jernih/jelas/tidak putus">   
                            <i class="input-helper"></i>    
                            Gambar jernih/jelas/tidak putus
                        </label>
                        <br>
                        <label class="checkbox checkbox-inline m-r-20 m-b-10">
                            <input type="checkbox" name='ratingus[]' class="ratingus" value="Tutor menguasai materi">                                
                            <i class="input-helper"></i>
                            Tutor menguasai materi
                        </label>
                        <br>
                        <label class="checkbox checkbox-inline m-r-20">
                            <input type="checkbox" name='ratingus[]' class="ratingus" value="Tutor menawan">
                            <i class="input-helper"></i>
                            Tutor menawan
                        </label>
                    </p>
                </div>                                 
            </div> 
            <div class="modal-footer">
                <div class="footer_showcase">
                    <div class="col-md-6">
                        <button id="btn_caseno" type="button" class="btn btn-danger btn-block">Tidak</button>
                    </div>
                    <div class="col-md-6">
                        <button id="btn_caseyes" type="button" class="btn btn-success btn-block">Ya</button>
                    </div>
                </div>
                <div class="footer_showyes" style="display: none;">                                                
                        <button id="btn_submitrating" type="button" class="btn btn-success btn-block">Kirim</button>                        
                </div>
                <div class="footer_showno" style="display: none;">                                                
                        <button id="btn_submitcomplain" type="button" class="btn btn-success btn-block">Kirim Komplain</button>                        
                </div>
            </div>                                                             
        </div>
    </div>
</div>