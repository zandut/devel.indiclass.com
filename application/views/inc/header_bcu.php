<!Doctype html>
<!--Conditionals for IE9 Support-->
<!--[if IE 9]><html lang="en" class="ie ie9"><![endif]-->
<html  ng-app="cmAir">
  <head>
    <meta charset="utf-8">
    <title>The Rote Less Travelled</title>
    <meta name="description" content="The Rote Less Travelled" />
	<meta name="keywords" content="channel, class, classmiles, The Rote Less Travelled" />
	<meta name="author" content="Ramdhani" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!--Favicon-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="http://rlt.com.sg/wp-content/uploads/2015/07/rlt-fav.ico" type="image/x-icon">
    <!--Google Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <!--Bootstrap 3.3.2-->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <!--Icon Fonts-->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/icon-moon.css" rel="stylesheet" media="screen">
    <!--Animations-->
    <link href="assets/css/animate.css" rel="stylesheet" media="screen">
    <!--Theme Styles-->
    <link href="assets/css/theme-styles.css" rel="stylesheet" media="screen">
    <link href="assets/fonts/icomoon.woff" rel="stylesheet" media="screen">
    <!--Color Schemes-->
    <link class="color-scheme" href="assets/css/colors/color-default.css" rel="stylesheet" media="screen">
    <!--Modernizr-->
	<script src="assets/js/libs/modernizr.custom.js"></script>

    <!--Adding Media Queries and Canvas Support for IE8-->
    <!--[if lt IE 9]>
      <script src="js/plugins/respond.min.js"></script>
      <script src="js/plugins/excanvas.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css">    
    <!-- LOAD ANGULAR -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/api_air.js"></script> -->    
  </head>

  