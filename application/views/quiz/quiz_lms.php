<?php
    require_once("curl.php");
    // $newdata = array( 
    //    'username'  => $this->session->userdata('user_name'),
    //    'firstname'  => $this->session->userdata('user_name'),
    //    'lastname'  => $this->session->userdata('user_name'),
    //    'email'     => $this->session->userdata('email'), 
    //    'logged_in' => TRUE
    // ); 
    // $this->session->set_userdata($newdata);
    $user = $this->session->userdata('user_name');
    $name = explode(' ',$user,2);
    $username = str_replace(' ', '',strtolower($user));
    $firstname = str_replace(' ', '',$name[0]); 
//    $firstname = 'asd'; 
    $lastname = str_replace(' ', '',$name[1]);
//    $lastname = 'asdasd';
    $useremail = $this->session->userdata('email');

function getloginurl($useremail, $firstname, $lastname, $username) {
    require_once('curl.php');
    $token        = 'dd4a8aff41d413305809c45536abd3af';
    $domainname   = 'https://classmiles.com/lms';
    $functionname = 'auth_userkey_request_login_url';

    $param = [
        'user' => [
            'firstname' => $firstname, // You will not need this parameter, if you are not creating/updating users
            'lastname'  => $lastname, // You will not need this parameter, if you are not creating/updating users
            'username'  => $username, 
            'email'     => $useremail
            // "customfields" => array ( // If you have custom fields in your system.
            //     array(
            //         "type" => "birthdate",
            //         "value" => strtotime("01/01/1990")
            //         ),
            //     array(
            //         "type" => "something_else",
            //         "value" => "0"
            //         )
            //     )
        ]
    ];

    $serverurl = $domainname . '/webservice/rest/server.php' . '?wstoken=' . $token . '&wsfunction=' . $functionname . '&moodlewsrestformat=json';
    $curl = new curl; // The required library curl can be obtained from https://github.com/moodlehq/sample-ws-clients 

    try {
        $resp     = $curl->post($serverurl, $param);
        $resp     = json_decode($resp);
        if ($resp && !empty($resp->loginurl)) {
            $loginurl = $resp->loginurl;        
        }
    } catch (Exception $ex) {
        return false;
    }

    if (!isset($loginurl)) {
        return false;
    }

    $path = '';
    if ($loginurl) {
        $path = '&wantsurl=' . urlencode("$domainname/");
    }
  header('Location: '.$loginurl . $path);
 // return $loginurl . $path;
   

}

echo getloginurl($useremail, $firstname, $lastname , $username);
?>