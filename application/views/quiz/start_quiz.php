<style type="text/css">
    html,body{
        /*background-color: white;*/
        background-color: #f5f5f0;
    }
.sidenav {
    height: 100%;
    width: 18%;
    position: fixed;
    z-index: 1;
    top: 0;
    right: 0;
    background-color: white;
    overflow-x: hidden;
    padding-top: 100px;
}

.sidenav a {
    padding: 6px 8px 6px 16px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: white;
}

.sidenav a:hover {
    color: #f1f1f1;
}


@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}
</style>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->

</head>
<body>
    <?php
        $bsid                   = $_GET['bsid'];
        $getdetails_courses     = $this->db->query("SELECT ms.subject_name, mj.jenjang_name, mj.jenjang_level, mbs.* FROM master_banksoal as mbs INNER JOIN master_subject as ms ON mbs.subject_id=ms.subject_id INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE mbs.bsid='$bsid'")->row_array(); 
        if (empty($getdetails_courses)) {
            redirect('/Quiz');
        }
        else
        {
            $name               = $getdetails_courses['name'];
            $subject_name       = $getdetails_courses['subject_name'];
            $desc               = $getdetails_courses['description'];
            $seconds            = $getdetails_courses['duration'];
            $hours              = floor($seconds / 3600);
            $mins               = floor($seconds / 60 % 60);
            $secs               = floor($seconds % 60);
            $duration           = sprintf('%02d:%02d', $hours, $mins);
            $list_soal          = json_decode($getdetails_courses['list_soal'],true);
            $jumlah_soal        = count($list_soal);
    ?>

<div class="sidenav" style="display: none; padding-left: 10px;">
  <h5>Deskripsi = <label><?php echo $desc;?></label></h5>
  <h5>Jumlah Soal = <label id="detail_jml_soal"></label></h5>
  <h5>Durasi Pengerjaan = <label><?php echo $duration;?> Jam</label></h5>
  <h5>Sisa Waktu = <label id="sisa_waktu"></label></h5>
  <h5>Sumber Soal = <label>Tim Classmiles.com</label></h5>
</div>
<section id="main" data-layout="layout-1" class="">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        
        <div style="display: none; background-color: white; " class="container" id="header_soal">

            <div class="col-md-2 col-xs-2">
            </div>
            <div class="col-md-8 col-xs-8" style=""> 
                <div class="row" style="">                                                     

                    <div class="card-body" style="">                                 
                        <div class="media">
                            <div class="clearfix"></div>
                            <div class="media-body rating-list">
                                <center>
                                    <h1 style="color: #6C7A89; font-size: 22pt;" class="lead"><?php echo $name;?></h1>
                                    <hr>
                                </center>

                                <div class="card-body card-padding">
                                    <p style="margin-top: 5%;"></p>
                                    <p style="margin-top: 1%;">
                                        
                                        Deskripsi<br>
                                        <label class="lead c-black"><?php echo $desc;?></label>
                                        <br>

                                        Jumlah Soal<br>
                                        <label class="lead c-black"><?php echo $jumlah_soal;?></label>
                                        <br>

                                        Durasi Pengerjaan<br>
                                        <label class="lead c-black"><?php echo $duration;?> Jam</label>
                                        <br>

                                        Sumber Soal<br>
                                        <label class="lead c-black">Tim Classmiles.com</label>
                                        <br>

                                        <button onclick="startQuiz.start();" id="display_soal" class="startTime btn btn-primary btn-block m-t-20">Mulai</button>
                                    </p>
                                </div>
                            </div>                                           
                        </div>                        
                    </div>

                </div>

            </div>              
            <div class="col-md-2 col-xs-2">
            </div>
        </div>

        <div style="display: none; background-color: white; " class="container" id="result_quiz">

            <div class="col-md-2 col-xs-2">
            </div>
            <div class="col-md-8 col-xs-8" style=""> 
                <div class="row" style="">                                                     

                    <div class="card-body" style="">                                 
                        <div class="media">
                            <div class="clearfix"></div>
                            <div class="media-body rating-list">
                                <center>
                                    <h1 style="color: #6C7A89; font-size: 22pt;" class="lead">Result Quiz <?php echo $subject_name;?></h1>
                                    <hr>
                                </center>

                                <div class="card-body card-padding">
                                    <p style="margin-top: 5%;"></p>
                                    <p style="margin-top: 1%;">
                                        
                                        Deskripsi<br>
                                        <label class="lead c-black"><?php echo $desc;?></label>
                                        <br>                                        

                                        Durasi Pengerjaan<br>
                                        <label class="lead c-black"><?php echo $duration;?> Jam</label>
                                        <br>

                                        Sumber Soal<br>
                                        <label class="lead c-black">Tim Classmiles.com</label>
                                        <br>
                                        <hr>
                                        <div class="col-md-12" align="center">
                                            <h1><i class="zmdi zmdi-mood"></i> Quiz Telah Selesai, Terima Kasih <i class="zmdi zmdi-mood"></i> </h1>
                                        </div>
                                        <div id="kotak_resutl" style="display: none;">
                                            <div class="col-md-12">
                                                <div class="col-md-4" align="center" style="border-right:1px solid #acac86;">
                                                    <label>
                                                        <img src="https://cdn.classmiles.com/sccontent/baru/JumlahSoal.png" style="height: 20%; width: 20%;">
                                                        <h1 id="result_jumlahsoal" style="color: blue;"></h1> Jumlah Soal
                                                    </label>
                                                </div>
                                                <div class="col-md-4" align="center" style="">
                                                    <label>
                                                        <img src="https://cdn.classmiles.com/sccontent/baru/JawabanBenar.png" style="height: 20%; width: 20%;">
                                                        <h1 id="result_totalbenar" style="color: green;"></h1> Benar
                                                    </label>
                                                </div>
                                                <div class="col-md-4" align="center" style="border-left:1px solid #acac86;">
                                                    <label>
                                                        <img src="https://cdn.classmiles.com/sccontent/baru/JawabanSalah.png" style="height: 20%; width: 20%;">
                                                        <h1 id="result_totalsalah" style="color: red;"></h1> Salah 
                                                    </label>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12">
                                                <hr>
                                                <center>
                                                    <label>
                                                        <h1 style="color: #95A5A6;">Score Anda</h1>
                                                        <h1 id="result_score" style="color: #6C7A89;">100</h1>
                                                    </label>
                                                </center>
                                                <hr>
                                            </div> 
                                        </div>

                                        <button class="reload btn bgm-gray btn-block m-t-20">Kembali</button>

                                    </p>
                                </div>
                            </div>                                           
                        </div>                        
                    </div>

                </div>

            </div>              
            <div class="col-md-2 col-xs-2">
            </div>
        </div>

        <div class="container" style="display: none;" id="soal_quiz">
            <div class="card col-md-9" style="padding: 0;">
                <div class="col-md-4" align="center" style="border-right:1px solid #acac86;">
                    <label>                        
                        <h1 id="jmlh_soal"></h1>Jumlah Soal
                    </label>
                </div>
                <div class="col-md-4" align="center" style="">
                    <label style="display: none;">
                        <h1 id="sisa_soal"></h1>Sisa Soal
                    </label>
                </div>
                <div class="col-md-4" align="center" style="border-left:1px solid #acac86;">
                    <label>
                        <h1 id="blm_terjawab" style="color: red;"></h1>Yang belum terjawab
                    </label>
                </div>
            </div>
            <div class="card col-md-9" style="padding-left: 0; padding-right: 0;">
                <div class="col-md-12" id="show_soal" style="padding-top: 20px;">
                        <h4 id="text_soal"></h4>
                        <hr>
                </div>
                <br><br>
                <div class="col-md-12" id="show_jawaban">
                </div>
                <div style="display: none;" class="col-md-12" id="kotak_essay">
                    <div class="form-group">
                        <div class="fg-line">
                            <textarea id="jawaban_essay" class="form-control" rows="5" placeholder="Silakan ketik jawaban Anda disini..."></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" id="show_action" style="padding-bottom: 10px;padding-top: 10px; background-color: #d6d6c2" align="right">
                    <button id="button_next"  class="next_soal btn btn-success btn-icon-text">Lanjut&nbsp;&nbsp;<i class="zmdi zmdi-mail-send"></i></button>
                </div>
            </div>
        </div>
        <div class=" modal fade"  id="temp_modal" tabindex="-1" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content" id="modal_konten" style="background-color: #32c787">
                    <center>
                            <div id='' class="modal-body" style="margin-top: 75%;" ><br>
                               <div class="" style="color: white; display: block; ?>" id="alertbody">
                                    <label id="text_modal">Waktu Anda Habis dan Quiz telah selesai, Terimakasih telah menyelesaikan Quiz ini. Jawaban Anda akan kami berikan kepada Tutor untuk diperiksa</label>
                                </div>
                            </div>
                        <div class="modal-footer">
                            <button  id="button_ok" type="submit" data-dismiss="modal" class="button_ok btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                        </div>
                    </center>
                </div>
            </div>
        </div>

        <?php 
        }
        ?>

    </section>
</section>

<footer id="footer" class="" style="z-index: 2;">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function() {
        $("#soal_quiz").css('display','block');
        $(".sidenav").css('display','block');
        function getURLParameter(name) {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
        }
        var bsid = getURLParameter("bsid");           
        var id_user = "<?php echo $this->session->userdata('id_user');?>";  
        // var id_user = 37;  
        var tgl = new Date();
        var tgll = moment(tgl).format('YYYY-MM-DD HH:mm:ss');
        var jawaban_dipilih = null;
        var uid = null;
        var optional_type = null;
        var options_type = null;
        var text_soal = null;
        var tutor_creator = null;
        var image_soal = null;
        var pilihan = null;
        var optionss = null;
        var belum_terjawab = null;
        var jumlah_soal = null;
        var quiz_id = null;
        var sisa_soal = null;
        var finish_status = null;
        var durasi = null;
        var total_benar = null;
        var total_salah = null;
        var score       = null;
        var jam_skrg = null;
        var jam_mulai = null;
        var x = null;
        var y = null;
        var time_left = null;
        var timer = null;
        var seconds = null;
        var secondsx = null;
        var secondsy = null;
        var days        = null;
        var hoursLeft   = null;
        var hours       = null;
        var minutesLeft = null;
        var minutes     = null;
        // $("#header_soal").css('display','none');
                $(".sidenav").css('display','block');
                // $("#soal_quiz").css('display','block');    
                $("#show_jawaban").empty();
                $("#blm_terjawab").empty();
                $("#sisa_soal").empty();
                $("#jmlh_soal").empty();
                $("#text_soal").empty();
                $.ajax({
                    url: '<?php echo base_url(); ?>V1.0.0/startQuiz',
                    type: 'POST',       
                    data: {
                        bsid:bsid,
                        id_user:id_user,
                        time_start:tgll
                    },         
                    success: function(response)
                    {            
                        var a = JSON.stringify(response);
                        console.log(a);
                        // return false;
                        var jsonPretty = JSON.stringify(JSON.parse(a),null,2);
                        belum_terjawab = response.belum_terjawab;
                        jumlah_soal = response.jumlah_soal;
                        quiz_id = response.quiz_id;
                        sisa_soal = response.sisa_soal;
                        finish_status = response.finish;
                        durasi = response.duration;
                        if (finish_status == false) {
                            uid = response.getDetail['soal_uid'];
                            options_type = response.getDetail['options_type'];

                            text_soal = response.getDetail['text_soal'];
                            tutor_creator = response.getDetail['tutor_creator'];
                            image_soal = response.getDetail['image_soal'];
                            pilihan = "";
                            optionss = response.getDetail['options'];
                            if (options_type !="essay") {
                                $("#kotak_essay").css('display','none');
                                $("#show_jawaban").css('display','block');
                                for (var i = 0; i < optionss.length; i++) {
                                    var index = 65;
                                    var alpabet = String.fromCharCode(index+i);

                                    // console.log(optionss[i]['value']);
                                    var jawaban = optionss[i]['value'];                                                
                                    pilihan += "<div class='col-md-12 radio_jawaban' id='"+jawaban+"'> "
                                        +"<div class='radio m-b-15' >"
                                            +"<label class='plh_jawab'>"+alpabet+". "
                                                +"<input type='radio' name='answer' value='"+jawaban+"'>"
                                                +"<i class='input-helper'></i>"+jawaban+""
                                            +"</label>"
                                        +"</div>"
                                    +"</div><br>";                        
                                }
                                $("#show_jawaban").empty();
                                $("#show_jawaban").append(pilihan);
                                $(".radio_jawaban").click(function(){
                                    jawaban_dipilih = this.id;
                                    $('#button_next').removeAttr('disabled'); 
                                });
                            }
                            else if (options_type == "essay") {
                                $("#kotak_essay").css('display','block');
                                $("#show_jawaban").css('display','none');
                                $("#jawaban_essay").empty();
                            }
                            
                        }
                        else{              
                            total_benar = response.total_benar;
                            total_salah = response.total_salah;
                            score       = response.score;
                            $("#kotak_essay").css('display','none');
                            $("#soal_quiz").css('display','none');
                            $(".sidenav").css('display','none');
                            $("#result_quiz").css('display','block');
                            $("#result_jumlahsoal").text(jumlah_soal);
                            $("#result_totalbenar").text(total_benar);
                            $("#result_totalsalah").text(total_salah);
                            $("#result_score").text(score);
                        }
                        $("#blm_terjawab").text(belum_terjawab);
                        $("#sisa_soal").text(sisa_soal);
                        $("#jmlh_soal").text(jumlah_soal);
                        $("#detail_jml_soal").text(jumlah_soal);
                        $("#text_soal").html(text_soal);                
                    }
                });
        var startQuiz = {
            start : function(){
                // $("#header_soal").css('display','none');
                $(".sidenav").css('display','block');
                // $("#soal_quiz").css('display','block');    
                $("#show_jawaban").empty();
                $("#blm_terjawab").empty();
                $("#sisa_soal").empty();
                $("#jmlh_soal").empty();
                $("#text_soal").empty();
                $.ajax({
                    url: '<?php echo base_url(); ?>V1.0.0/startQuiz',
                    type: 'POST',       
                    data: {
                        bsid:bsid,
                        id_user:id_user,
                        time_start:tgll
                    },         
                    success: function(response)
                    {            
                        var a = JSON.stringify(response);
                        console.log(a);
                        // return false;
                        var jsonPretty = JSON.stringify(JSON.parse(a),null,2);
                        belum_terjawab = response.belum_terjawab;
                        jumlah_soal = response.jumlah_soal;
                        quiz_id = response.quiz_id;
                        sisa_soal = response.sisa_soal;
                        finish_status = response.finish;
                        durasi = response.duration;
                        if (finish_status == false) {
                            uid = response.getDetail['soal_uid'];
                            options_type = response.getDetail['options_type'];

                            text_soal = response.getDetail['text_soal'];
                            tutor_creator = response.getDetail['tutor_creator'];
                            image_soal = response.getDetail['image_soal'];
                            pilihan = "";
                            optionss = response.getDetail['options'];
                            if (options_type !="essay") {
                                $("#kotak_essay").css('display','none');
                                $("#show_jawaban").css('display','block');
                                for (var i = 0; i < optionss.length; i++) {
                                    var index = 65;
                                    var alpabet = String.fromCharCode(index+i);

                                    // console.log(optionss[i]['value']);
                                    var jawaban = optionss[i]['value'];                                                
                                    pilihan += "<div class='col-md-12 radio_jawaban' id='"+jawaban+"'> "
                                        +"<div class='radio m-b-15' >"
                                            +"<label class='plh_jawab'>"+alpabet+". "
                                                +"<input type='radio' name='answer' value='"+jawaban+"'>"
                                                +"<i class='input-helper'></i>"+jawaban+""
                                            +"</label>"
                                        +"</div>"
                                    +"</div><br>";                        
                                }
                                $("#show_jawaban").empty();
                                $("#show_jawaban").append(pilihan);
                                $(".radio_jawaban").click(function(){
                                    jawaban_dipilih = this.id;
                                    $('#button_next').removeAttr('disabled'); 
                                });
                            }
                            else if (options_type == "essay") {
                                $("#kotak_essay").css('display','block');
                                $("#show_jawaban").css('display','none');
                                $("#jawaban_essay").empty();
                            }
                            
                        }
                        else{              
                            total_benar = response.total_benar;
                            total_salah = response.total_salah;
                            score       = response.score;
                            $("#kotak_essay").css('display','none');
                            $("#soal_quiz").css('display','none');
                            $(".sidenav").css('display','none');
                            $("#result_quiz").css('display','block');
                            $("#result_jumlahsoal").text(jumlah_soal);
                            $("#result_totalbenar").text(total_benar);
                            $("#result_totalsalah").text(total_salah);
                            $("#result_score").text(score);
                        }
                        $("#blm_terjawab").text(belum_terjawab);
                        $("#sisa_soal").text(sisa_soal);
                        $("#jmlh_soal").text(jumlah_soal);
                        $("#detail_jml_soal").text(jumlah_soal);
                        $("#text_soal").html(text_soal);                
                    }
                });
            }
        };
        $.ajax({
            url: '<?php echo base_url(); ?>V1.0.0/startQuiz',
            type: 'POST',       
            data: {
                bsid:bsid,
                id_user:id_user,
                time_start:tgll
            },         
            success: function(response)
            {            
                var a = JSON.stringify(response);
                console.log(a);
                // return false;
                var jsonPretty = JSON.stringify(JSON.parse(a),null,2);
                
                belum_terjawab = response.belum_terjawab;
                jumlah_soal = response.jumlah_soal;
                quiz_id = response.quiz_id;
                time_start = response.time_start;
                sisa_soal = response.sisa_soal;
                finish_status = response.finish;
                durasi = response.duration;


                jam_skrg = moment(tgl).format('HH:mm:ss');
                jam_mulai = moment(time_start).format('HH:mm:ss');
                x = jam_skrg.split(':'); // split it at the colons
                y = jam_mulai.split(':'); // split it at the colons

                // minutes are worth 60 seconds. Hours are worth 60 minutes.
                secondsx = (+x[0]) * 60 * 60 + (+x[1]) * 60 + (+x[2]); 
                secondsy = (+y[0]) * 60 * 60 + (+y[1]) * 60 + (+y[2]); 
                
                


                function startTimer(duration, display) {
                    time_left = durasi - (secondsx - secondsy);
                    // timer = duration, minutes, seconds;
                    setInterval(function () {

                        days        = Math.floor(time_left/24/60/60);
                        hoursLeft   = Math.floor((time_left) - (days*86400));
                        hours       = Math.floor(hoursLeft/3600);
                        minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                        minutes     = Math.floor(minutesLeft/60);
                        var remainingSeconds = time_left % 60;
                        if (remainingSeconds < 10) {
                            remainingSeconds = "0" + remainingSeconds; 
                        }
                        if (hours < 10) {
                            hours = "0" + hours; 
                        }
                        if (minutes < 10) {
                            minutes = "0" + minutes; 
                        }
                        // alert(days + ":" + hours + ":" + minutes + ":" + remainingSeconds);
                        // alert(time_left);
                        if (time_left < 0) {
                            $("#temp_modal").modal('show');     
                            document.getElementById('sisa_waktu').innerHTML = "Waktu Habis";
                            $(".button_ok").click(function(){
                                window.location.href = '<?php echo base_url();?>';
                            });
                        }
                        else if (time_left == 0) {
                            // clearInterval(countdownTimer);
                            document.getElementById('sisa_waktu').innerHTML = "Waktu Habis";
                            $("#temp_modal").modal('show');
                            $(".button_ok").click(function(){
                                window.location.href = '<?php echo base_url();?>';
                            });
                            
                        } else {
                            document.getElementById('sisa_waktu').innerHTML = hours + ":" + minutes + ":" + remainingSeconds;
                            time_left--;
                        }

                    }, 1000);
                }

                jQuery(function ($) {
                        display = $('#sisa_awaktu');
                    startTimer(time_left, display);
                });     
            }
        });
        $('.next_soal').click(function(){
        if (options_type == "essay") {
            jawaban_dipilih = $('#jawaban_essay').val();    
            $("#jawaban_essay").val('');
            $("#jawaban_essay").empty();
        }
        $.ajax({
            url: '<?php echo base_url(); ?>V1.0.0/saveQuizFromStudent',
            type: 'POST',       
            data: {
                bsid:bsid,
                id_user:id_user,
                quiz_id : quiz_id,
                soal_uid : uid,
                answer_select : jawaban_dipilih
            },         
            success: function(response)
            {
                $(document).ready(function(){
                    // $('.display_soal').trigger('click');
                    startQuiz.start();
                });
            }
        });   
    });
    $('.startTime').click(function(){          

        $("#header_soal").css('display','none');       
        $.ajax({
            url: '<?php echo base_url(); ?>V1.0.0/startQuiz',
            type: 'POST',       
            data: {
                bsid:bsid,
                id_user:id_user,
                time_start:tgll
            },         
            success: function(response)
            {            
                var a = JSON.stringify(response);
                console.log(a);
                // return false;
                var jsonPretty = JSON.stringify(JSON.parse(a),null,2);
                
                belum_terjawab = response.belum_terjawab;
                jumlah_soal = response.jumlah_soal;
                quiz_id = response.quiz_id;
                time_start = response.time_start;
                sisa_soal = response.sisa_soal;
                finish_status = response.finish;
                durasi = response.duration;


                jam_skrg = moment(tgl).format('HH:mm:ss');
                jam_mulai = moment(time_start).format('HH:mm:ss');
                x = jam_skrg.split(':'); // split it at the colons
                y = jam_mulai.split(':'); // split it at the colons

                // minutes are worth 60 seconds. Hours are worth 60 minutes.
                secondsx = (+x[0]) * 60 * 60 + (+x[1]) * 60 + (+x[2]); 
                secondsy = (+y[0]) * 60 * 60 + (+y[1]) * 60 + (+y[2]); 
                
                


                function startTimer(duration, display) {
                    time_left = durasi - (secondsx - secondsy);
                    // timer = duration, minutes, seconds;
                    setInterval(function () {

                        days        = Math.floor(time_left/24/60/60);
                        hoursLeft   = Math.floor((time_left) - (days*86400));
                        hours       = Math.floor(hoursLeft/3600);
                        minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                        minutes     = Math.floor(minutesLeft/60);
                        var remainingSeconds = time_left % 60;
                        if (remainingSeconds < 10) {
                            remainingSeconds = "0" + remainingSeconds; 
                        }
                        if (hours < 10) {
                            hours = "0" + hours; 
                        }
                        if (minutes < 10) {
                            minutes = "0" + minutes; 
                        }
                        // alert(days + ":" + hours + ":" + minutes + ":" + remainingSeconds);
                        // alert(time_left);
                        if (time_left < 0) {
                            $("#temp_modal").modal('show');     
                            document.getElementById('sisa_waktu').innerHTML = "Waktu Habis";
                            $(".button_ok").click(function(){
                                window.location.href = '<?php echo base_url();?>';
                            });
                        }
                        else if (time_left == 0) {
                            // clearInterval(countdownTimer);
                            document.getElementById('sisa_waktu').innerHTML = "Waktu Habis";
                            $("#temp_modal").modal('show');
                            $(".button_ok").click(function(){
                                window.location.href = '<?php echo base_url();?>';
                            });
                            
                        } else {
                            document.getElementById('sisa_waktu').innerHTML = hours + ":" + minutes + ":" + remainingSeconds;
                            time_left--;
                        }

                    }, 1000);
                }

                jQuery(function ($) {
                        display = $('#sisa_awaktu');
                    startTimer(time_left, display);
                });     
            }
        });
    });
    $(".reload").click(function(){
        window.location.replace('<?php echo base_url();?>Quiz');
    });
    });
    
</script>
    