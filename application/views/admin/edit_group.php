<main class="main">

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>Edit Group</h1>
            <small>Details of the group list in your channel</small>

            <div class="actions">
                <a data-toggle="modal" href="#modalAddGroup"><button class="btn btn-success">+ Add Participants</button></a>
            </div>
        </header>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label>Name group</label>
                                <input type="text" class="form-control" id="group_name">                        
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-success btn-lg" id="saveNameGroup">Save Name Group</button>
                        </div>
                    </div>
                    <div class="row table-responsive">
                        <div id="tables_listGroup"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" >
                <div class="modal-content" id="modal_konten">
                    <div class="modal-body" align="center">
                        <label id="text_modal">Halloo</label><br>
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal modalAddTutor -->  
        <div class="modal fade show" id="modalAddGroup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">                        
                        <h4 class="modal-title c-white pull-left">Add Participants Group</h4>
                        <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    </div>
                    <div class="modal-body m-t-20">
                        <form id="frmdata">
                            <div class="col-md-12" style="padding: 0;">
                                <div class="col-md-12">
                                    <label>Participants Group</label><br>                                    
                                    <div class="col-md-12 m-t-10">                                          
                                        <select required name="taggroup[]" id="taggroup" multiple class="select2 taggroup" style="width: 100%;">                                                
                                        </select>
                                        <p style="color: red; display: none;" id="alertemptytag">Mohon memilih partisipan yang ingin ditambah terlebih dahulu!!!</p>                                    
                                    </div>
                                </div>                                
                            </div>
                        </form>
                        <br>
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-success waves-effect btn-block p-10" style="margin-top: 2%;" id="AddParticipantsGroup">Save</button>                            
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade show" id="modalDeleteGroup" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Delete Participants</b></h3>
                    </div>
                    <div class="modal-body">                        
                        <label class="c-gray f-15">Are you sure you want to delete?</label>                         
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-danger waves-effect" id="proses_hapusgroup" data-id="" rtp="">Yes</button>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>      

<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var group_id = "<?php echo $_GET['idg'];?>";    
    var dataSet = [];
    var user_utc = new Date().getTimezoneOffset();
        user_utc = 1 * user_utc;

    $(document).ready(function() {

        $('#select_listtutor').select2();
        var group_participants = [];
        $.ajax({
            url: '<?php echo AIR_API;?>listGroupById/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id,
                group_id : group_id,
                user_utc: user_utc
            },
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                  
                if (code == 200) {                    
                    var group_id        = response['data']['group_id'];
                    var group_name      = response['data']['group_name'];
                    var new_arr         = response['data']['new_arr'];
                    var participant     = response['data']['participant'];
                    group_participants  = response['data']['group_participants'];

                    for (var i = 0; i < new_arr.length; i++) {  
                        var id_user = new_arr[i]['id_user'];
                        var name    = new_arr[i]['nama'];
                        var email   = new_arr[i]['email'];
                        dataSet.push([i+1, name, email, "<button class='deleteGroup btn waves-effect' style='background-color: #D91E18;' data-groupid='"+group_id+"' data-iduser='"+id_user+"' title='Delete Group'><i class='zmdi zmdi-delete zmdi-hc-fw' style='color:#fff;'></i></button>"]);                         
                    }

                    $("#group_name").val(group_name);

                                           

                    $('#tables_listGroup').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="listGroup"></table>' );
             
                    $('#listGroup').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Name"},
                            { "title": "Email"},
                            { "title": "Action"},                            
                        ]
                    });
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
                else
                {
                    $('#tables_listGroup').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="listGroup"></table>' );
             
                    $('#listGroup').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Name Group"},
                            { "title": "Participant"},
                            { "title": "Action"},                            
                        ]
                    });
                }
            }
        }); 
        
        $(document).on('click', '.deleteGroup', function(){
            var groupid    = $(this).data('groupid');
            var iduser    = $(this).data('iduser');
            console.log(group_participants);
            for (var i = 0; i < group_participants.length; i++) {                
                if (iduser == group_participants[i]) {
                    group_participants.splice(i, 1);                    
                    break;
                }
            }            
            $('#proses_hapusgroup').attr('rtp',groupid);
            $("#modalDeleteGroup").modal("show");
        });

        $("select.select2").select2({
            delay: 2000,
            maximumSelectionLength: 1,
            ajax: {
                dataType: 'json',
                type: 'GET',
                url: '<?php echo AIR_API;?>getListNameGroupNotLike/access_token/'+access_token+'?channel_id='+channel_id+'&group_id='+group_id,
                data: function (params) {
                    return {
                      term: params.term,
                      page: params.page || 1
                    };
                },
                processResults: function(data){
                    return {
                        results: data.results,
                        pagination: {
                            more: data.more
                        }                       
                    };
                }                   
            }
        });   

        $(document).on("click", "#saveNameGroup", function(){
            $('#modalAddGroup').modal('hide');
            var group_name          = $("#group_name").val();
            
            $.ajax({
                url : '<?php echo AIR_API;?>updateGroupName/access_token/'+access_token,
                type:"post",
                data: {
                    group_id  : group_id,
                    group_name: group_name,                    
                    channel_id: channel_id
                },
                success: function(response){             
                    var code = response['code'];
                    if (code == 200) {
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully Change Group Name");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                        /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Successfully Added Group');
                        setTimeout(function(){                            
                            location.reload();
                        },2000);*/
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                    else
                    {
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#text_modal").html("Please complete the data !!!");
                        $("#button_ok").click( function(){
                            $("#modalAddGroup").modal('show');
                        });
                        /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','There is an error!!!');
                        setTimeout(function(){
                            location.reload();
                        },2000);*/
                    }
                }
            });
        });

        $(document).on("click", "#proses_hapusgroup", function(){   
            $('#modalDeleteGroup').modal('hide');
            var group_id = $(this).attr('rtp');
            $.ajax({
                url :"<?php echo AIR_API;?>updateGroupParticipants/access_token/"+access_token,
                type:"post",
                data: {
                    group_id: group_id,
                    group_participants : group_participants,
                    channel_id: channel_id
                },
                success: function(response){   
                    var code = response['code'];
                    if (code == 200) {
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully Removing Participants Group");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                        /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Successfully Added Group');
                        setTimeout(function(){                            
                            location.reload();
                        },2000);*/
                    }   
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                    else{                        
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#text_modal").html("There is an error!!!");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                        /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','There is an error!!!');
                        setTimeout(function(){
                            location.reload();
                        },2000);*/
                    }                  
                } 
            });
        });

        $(document).on("click", "#AddParticipantsGroup", function(){   
            
            var taggroup = $("#taggroup").val();   
            if (taggroup == "") {
                $("#alertemptytag").css('display','block');
            }
            else
            {
                $('#modalAddGroup').modal('hide');
                $("#alertemptytag").css('display','none');        
                for (var i = 0; i < group_participants.length; i++) {
                    var id = group_participants[i];                    
                    if (id != taggroup) {                        
                        group_participants.push(taggroup[0]);
                        break;                         
                    }
                }
                $.ajax({
                    url :"<?php echo AIR_API;?>updateGroupParticipants/access_token/"+access_token,
                    type:"post",
                    data: {
                        group_id: group_id,
                        group_participants : group_participants,
                        channel_id: channel_id
                    },
                    success: function(response){   
                        var code = response['code'];
                        if (code == 200) {
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#32c787');
                            $("#text_modal").html("Successfully Add Participants Group");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                        }   
                        else if (code == -400) {
                            window.location.href='<?php echo base_url();?>Admin/Logout';
                        }
                        else{                        
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#ff3333');
                            $("#text_modal").html("There is an error!!!");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                        }                  
                    } 
                });
            }
        });

    });
</script>  