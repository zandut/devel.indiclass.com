<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Reschedule Class</h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul> -->
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>            

            <div class="card m-t-20 p-20" style="">
                <div class="block-header">
                        <h2>Display : <b id="status_class">All Date</b></h2>
                    
                        <ul class="actions">
                            <li class="dropdown">
                                <a href="" data-toggle="dropdown">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                    
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a id="btn_all" href="">All Date</a>
                                    </li>
                                    <li>
                                        <a id="btn_today" href="">Today</a>
                                    </li>
                                    <li>
                                        <a id="btn_week" href="">This Week</a>
                                    </li>
                                    <li>
                                        <a id="btn_month" href="">This Month</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    
                    </div>


                <div id="class_all" class="row" style="display: block; margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $now = (date('Y-m-d H:i:s'));
                                    $select_all = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_monthly = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE MONTH(`finish_time`) = MONTH(CURDATE()) AND YEAR(`finish_time`) = YEAR(CURRENT_DATE()) and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_weekly = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE YEARWEEK(`finish_time`, 1) = YEARWEEK(CURDATE(), 1) and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_daily = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE DATE(`finish_time`) = CURDATE() and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    ?>
                                    <tr>
                                        <th data-column-id="no">No</th>
                                        <th data-column-id="class_id">Class ID</th>
                                        <th data-column-id="type_class">Type of Class</th>
                                        <th data-column-id="name">Subject Name</th>
                                        <th data-column-id="name_tutor">Tutor Name</th>                                        
                                        <th data-column-id="description">Description</th>
                                        <th data-column-id="participant">Number of Participant</th>
                                        <th data-column-id="classdate">Class Date</th>
                                        <th data-column-id="start_time">Start Time</th>
                                        <th data-column-id="end_time">End Time</th>
                                        <th data-column-id="aksi" style="text-align: center;" >Action</th>

                                    </tr>
                                </thead>
                                <tbody id="" style="">  
                                    <?php
                                    $no=1;
                                    foreach ($select_all as $row => $v) {
                                        $user_utc   = $this->session->userdata('user_utc');
                                        $server_utc = $this->Rumus->getGMTOffset();
                                        $decode = json_decode($v['participant'],true);
                                        if(isset($decode['participant'])){
                                            $length = count($decode['participant']);
                                            $participant = $length;
                                        }
                                        else
                                        {
                                            $participant = 0;
                                        }   
                                        $interval   = $user_utc - $server_utc;                                        
                                        $class_type = $v['class_type'];
                                        if ($class_type == "multicast") {
                                            $text_classtype = "Multicast";
                                        }else if ($class_type == "multicast_channel_paid") {
                                            $text_classtype = "Multicast Paid Channel";
                                        }else if ($class_type == "multicast_paid") {
                                            $text_classtype = "Multicast Paid";
                                        }else if ($class_type == "private") {
                                            $text_classtype = "Private";
                                        }else if ($class_type == "private_channel") {
                                            $text_classtype = "Private Channel";
                                        }else if ($class_type == "group") {
                                            $text_classtype = "Group";
                                        }else if ($class_type == "group_channel") {
                                            $text_classtype = "Group Channel";
                                        }
                                        $start_time = DateTime::createFromFormat('Y-m-d H:i:s',$v['start_time']);
                                        $start_time->modify("+$interval minute");
                                        $start_time = $start_time->format('H:i');

                                        $finish_time = DateTime::createFromFormat('Y-m-d H:i:s',$v['finish_time']);
                                        $finish_time->modify("+$interval minute");
                                        $finish_time = $finish_time->format('H:i');

                                        $start_date = DateTime::createFromFormat('Y-m-d H:i:s',$v['start_time']);
                                        $start_date->modify("+$interval minute");
                                        $start_date = $start_date->format('d M Y');

                                        $finish_date = DateTime::createFromFormat('Y-m-d H:i:s',$v['finish_time']);
                                        $finish_date->modify("+$interval minute");
                                        $finish_date = $finish_date->format('d M Y');

                                        if ($start_date == $finish_date) {
                                            $dateclass = $start_date;
                                        }
                                        else{
                                            $dateclass = $start_date." - ".$finish_date;
                                        }
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['class_id']; ?></td>
                                            <td><?php echo $text_classtype;?></td>
                                            <td><?php echo($v['name']); ?></td>
                                            <td><?php echo($v['user_name']); ?></td>                                            
                                            <td><?php echo($v['description']); ?></td>  
                                            <td><?php echo($participant);?></td>
                                            <td><?php echo($dateclass);?></td>
                                            <td><?php echo($start_time); ?></td> 
                                            <td><?php echo($finish_time); ?></td>
                                            <td>
                                                <a data-toggle="modal" description="<?php echo $v['description']; ?>" rtp="<?php echo $v['class_id']; ?>" subject="<?php echo $v['name']; ?>" end="<?php echo $v['finish_time']; ?>" tutor="<?php echo $v['user_name']; ?>" start="<?php echo $v['start_time']; ?>" class="btneditWaktu" data-target-color="green" href="#modalEditWaktu"><button class="btn bgm-bluegray" title="Ubah Waktu Kelas"><i class="zmdi zmdi-edit"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>
                            </table>                           
                        </div>
                    </div>
                </div> 
                <div id="class_today" class="row" style="display: none; margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $now = (date('Y-m-d H:i:s'));
                                    $select_all = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_monthly = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE MONTH(`finish_time`) = MONTH(CURDATE()) AND YEAR(`finish_time`) = YEAR(CURRENT_DATE()) and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_weekly = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE YEARWEEK(`finish_time`, 1) = YEARWEEK(CURDATE(), 1) and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_daily = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE DATE(`finish_time`) = CURDATE() and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    ?>
                                    <tr>
                                        <th data-column-id="no">No</th>
                                        <th data-column-id="class_id">Class ID</th>
                                        <th data-column-id="type_class">Type of Class</th>
                                        <th data-column-id="name">Subject Name</th>
                                        <th data-column-id="name_tutor">Tutor Name</th>                                        
                                        <th data-column-id="description">Description</th>
                                        <th data-column-id="participant">Number of Participant</th>
                                        <th data-column-id="start_time">Start Time</th>
                                        <th data-column-id="end_time">End Time</th>
                                        <th data-column-id="aksi" style="text-align: center;" >Action</th>

                                    </tr>
                                </thead>
                                <tbody id="" style="">  
                                    <?php
                                    $no=1;
                                    foreach ($select_daily as $row => $v) {
                                        $user_utc   = $this->session->userdata('user_utc');
                                        $server_utc = $this->Rumus->getGMTOffset();
                                        $decode = json_decode($v['participant'],true);
                                        if(isset($decode['participant'])){
                                            $length = count($decode['participant']);
                                            $participant = $length;
                                        }
                                        else
                                        {
                                            $participant = 0;
                                        }   
                                        $interval   = $user_utc - $server_utc;                                        
                                        $class_type = $v['class_type'];
                                        if ($class_type == "multicast") {
                                            $text_classtype = "Multicast";
                                        }else if ($class_type == "multicast_channel_paid") {
                                            $text_classtype = "Multicast Paid Channel";
                                        }else if ($class_type == "multicast_paid") {
                                            $text_classtype = "Multicast Paid";
                                        }else if ($class_type == "private") {
                                            $text_classtype = "Private";
                                        }else if ($class_type == "private_channel") {
                                            $text_classtype = "Private Channel";
                                        }else if ($class_type == "group") {
                                            $text_classtype = "Group";
                                        }else if ($class_type == "group_channel") {
                                            $text_classtype = "Group Channel";
                                        }

                                        $start_time = DateTime::createFromFormat('Y-m-d H:i:s',$v['start_time']);
                                        $dateclass = DateTime::createFromFormat('Y-m-d H:i:s', $v['start_time']);
                                        $start_time->modify("+$interval minute");
                                        $start_time = $start_time->format('H:i');

                                        $finish_time = DateTime::createFromFormat('Y-m-d H:i:s',$v['finish_time']);
                                        $finish_time->modify("+$interval minute");
                                        $finish_time = $finish_time->format('H:i');
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['class_id']; ?></td>
                                            <td><?php echo $text_classtype;?></td>
                                            <td><?php echo($v['name']); ?></td>
                                            <td><?php echo($v['user_name']); ?></td>                                            
                                            <td><?php echo($v['description']); ?></td>  
                                            <td><?php echo($participant);?></td>
                                            <td><?php echo($start_time); ?></td> 
                                            <td><?php echo($finish_time); ?></td>
                                            <td>
                                                <a data-toggle="modal" description="<?php echo $v['description']; ?>" rtp="<?php echo $v['class_id']; ?>" subject="<?php echo $v['name']; ?>" end="<?php echo $v['finish_time']; ?>" tutor="<?php echo $v['user_name']; ?>" start="<?php echo $v['start_time']; ?>" class="btneditWaktu" data-target-color="green" href="#modalEditWaktu"><button class="btn bgm-bluegray" title="Ubah Waktu Kelas"><i class="zmdi zmdi-edit"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>
                            </table>                           
                        </div>
                    </div>
                </div>
                <div id="class_week" class="row" style="display: none; margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $now = (date('Y-m-d H:i:s'));
                                    $select_all = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_monthly = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE MONTH(`finish_time`) = MONTH(CURDATE()) AND YEAR(`finish_time`) = YEAR(CURRENT_DATE()) and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_weekly = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE YEARWEEK(`finish_time`, 1) = YEARWEEK(CURDATE(), 1) and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_daily = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE DATE(`finish_time`) = CURDATE() and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    ?>
                                    <tr>
                                        <th data-column-id="no">No</th>
                                        <th data-column-id="class_id">Class ID</th>
                                        <th data-column-id="type_class">Type of Class</th>
                                        <th data-column-id="name">Subject Name</th>
                                        <th data-column-id="name_tutor">Tutor Name</th>                                        
                                        <th data-column-id="description">Description</th>
                                        <th data-column-id="participant">Number of Participant</th>
                                        <th data-column-id="dateclass">Class Date</th>
                                        <th data-column-id="start_time">Start Time</th>
                                        <th data-column-id="end_time">End Time</th>
                                        <th data-column-id="aksi" style="text-align: center;" >Action</th>

                                    </tr>
                                </thead>
                                <tbody id="" style="">  
                                    <?php
                                    $no=1;
                                    foreach ($select_weekly as $row => $v) {
                                        $user_utc   = $this->session->userdata('user_utc');
                                        $server_utc = $this->Rumus->getGMTOffset();
                                        $decode = json_decode($v['participant'],true);
                                        if(isset($decode['participant'])){
                                            $length = count($decode['participant']);
                                            $participant = $length;
                                        }
                                        else
                                        {
                                            $participant = 0;
                                        }   
                                        $interval   = $user_utc - $server_utc;                                        
                                        $class_type = $v['class_type'];
                                        if ($class_type == "multicast") {
                                            $text_classtype = "Multicast";
                                        }else if ($class_type == "multicast_channel_paid") {
                                            $text_classtype = "Multicast Paid Channel";
                                        }else if ($class_type == "multicast_paid") {
                                            $text_classtype = "Multicast Paid";
                                        }else if ($class_type == "private") {
                                            $text_classtype = "Private";
                                        }else if ($class_type == "private_channel") {
                                            $text_classtype = "Private Channel";
                                        }else if ($class_type == "group") {
                                            $text_classtype = "Group";
                                        }else if ($class_type == "group_channel") {
                                            $text_classtype = "Group Channel";
                                        }

                                        $start_time = DateTime::createFromFormat('Y-m-d H:i:s',$v['start_time']);
                                        $dateclass = DateTime::createFromFormat('Y-m-d H:i:s', $v['start_time']);
                                        $start_time->modify("+$interval minute");
                                        $start_time = $start_time->format('H:i');

                                        $finish_time = DateTime::createFromFormat('Y-m-d H:i:s',$v['finish_time']);
                                        $finish_time->modify("+$interval minute");
                                        $finish_time = $finish_time->format('H:i');

                                        $start_date = DateTime::createFromFormat('Y-m-d H:i:s',$v['start_time']);
                                        $start_date->modify("+$interval minute");
                                        $start_date = $start_date->format('d M Y');

                                        $finish_date = DateTime::createFromFormat('Y-m-d H:i:s',$v['finish_time']);
                                        $finish_date->modify("+$interval minute");
                                        $finish_date = $finish_date->format('d M Y');

                                        if ($start_date == $finish_date) {
                                            $dateclass = $start_date;
                                        }
                                        else{
                                            $dateclass = $start_date." - ".$finish_date;
                                        }
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['class_id']; ?></td>
                                            <td><?php echo $text_classtype;?></td>
                                            <td><?php echo($v['name']); ?></td>
                                            <td><?php echo($v['user_name']); ?></td>                                            
                                            <td><?php echo($v['description']); ?></td>  
                                            <td><?php echo($participant);?></td>
                                            <td><?php echo($dateclass);?></td>
                                            <td><?php echo($start_time); ?></td> 
                                            <td><?php echo($finish_time); ?></td>
                                            <td>
                                                <a data-toggle="modal" description="<?php echo $v['description']; ?>" rtp="<?php echo $v['class_id']; ?>" subject="<?php echo $v['name']; ?>" end="<?php echo $v['finish_time']; ?>" tutor="<?php echo $v['user_name']; ?>" start="<?php echo $v['start_time']; ?>" class="btneditWaktu" data-target-color="green" href="#modalEditWaktu"><button class="btn bgm-bluegray" title="Ubah Waktu Kelas"><i class="zmdi zmdi-edit"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>
                            </table>                           
                        </div>
                    </div>
                </div>
                <div id="class_month" class="row" style="display: none; margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $now = (date('Y-m-d H:i:s'));
                                    $select_all = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_monthly = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE MONTH(`finish_time`) = MONTH(CURDATE()) AND YEAR(`finish_time`) = YEAR(CURRENT_DATE()) and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_weekly = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE YEARWEEK(`finish_time`, 1) = YEARWEEK(CURDATE(), 1) and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    $select_daily = $this->db->query("SELECT tc.*, tu.user_name FROM tbl_class as tc INNER JOIN tbl_user as tu WHERE DATE(`finish_time`) = CURDATE() and tc.tutor_id=tu.id_user and finish_time > '$now' order by start_time ASC")->result_array();
                                    ?>
                                    <tr>
                                        <th data-column-id="no">No</th>
                                        <th data-column-id="class_id">Class ID</th>
                                        <th data-column-id="type_class">Type of Class</th>
                                        <th data-column-id="name">Subject Name</th>
                                        <th data-column-id="name_tutor">Tutor Name</th>                                        
                                        <th data-column-id="description">Description</th>
                                        <th data-column-id="participant">Number of Participant</th>
                                        <th data-column-id="dateclass">Class Date</th>
                                        <th data-column-id="start_time">Start Time</th>
                                        <th data-column-id="end_time">End Time</th>
                                        <th data-column-id="aksi" style="text-align: center;" >Action</th>

                                    </tr>
                                </thead>
                                <tbody id="" style="">  
                                    <?php
                                    $no=1;
                                    foreach ($select_monthly as $row => $v) {
                                        $user_utc   = $this->session->userdata('user_utc');
                                        $server_utc = $this->Rumus->getGMTOffset();
                                        $decode = json_decode($v['participant'],true);
                                        if(isset($decode['participant'])){
                                            $length = count($decode['participant']);
                                            $participant = $length;
                                        }
                                        else
                                        {
                                            $participant = 0;
                                        }   
                                        $interval   = $user_utc - $server_utc;                                        
                                        $class_type = $v['class_type'];
                                        if ($class_type == "multicast") {
                                            $text_classtype = "Multicast";
                                        }else if ($class_type == "multicast_channel_paid") {
                                            $text_classtype = "Multicast Paid Channel";
                                        }else if ($class_type == "multicast_paid") {
                                            $text_classtype = "Multicast Paid";
                                        }else if ($class_type == "private") {
                                            $text_classtype = "Private";
                                        }else if ($class_type == "private_channel") {
                                            $text_classtype = "Private Channel";
                                        }else if ($class_type == "group") {
                                            $text_classtype = "Group";
                                        }else if ($class_type == "group_channel") {
                                            $text_classtype = "Group Channel";
                                        }

                                        $start_time = DateTime::createFromFormat('Y-m-d H:i:s',$v['start_time']);
                                        $dateclass = DateTime::createFromFormat('Y-m-d H:i:s', $v['start_time']);
                                        $start_time->modify("+$interval minute");
                                        $start_time = $start_time->format('H:i');

                                        $finish_time = DateTime::createFromFormat('Y-m-d H:i:s',$v['finish_time']);
                                        $finish_time->modify("+$interval minute");
                                        $finish_time = $finish_time->format('H:i');

                                        $start_date = DateTime::createFromFormat('Y-m-d H:i:s',$v['start_time']);
                                        $start_date->modify("+$interval minute");
                                        $start_date = $start_date->format('d M Y');

                                        $finish_date = DateTime::createFromFormat('Y-m-d H:i:s',$v['finish_time']);
                                        $finish_date->modify("+$interval minute");
                                        $finish_date = $finish_date->format('d M Y');

                                        if ($start_date == $finish_date) {
                                            $dateclass = $start_date;
                                        }
                                        else{
                                            $dateclass = $start_date." - ".$finish_date;
                                        }
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['class_id']; ?></td>
                                            <td><?php echo $text_classtype;?></td>
                                            <td><?php echo($v['name']); ?></td>
                                            <td><?php echo($v['user_name']); ?></td>                                            
                                            <td><?php echo($v['description']); ?></td>  
                                            <td><?php echo($participant);?></td>
                                            <td><?php echo($dateclass); ?></td>
                                            <td><?php echo($start_time); ?></td> 
                                            <td><?php echo($finish_time); ?></td>
                                            <td>
                                                <a data-toggle="modal" description="<?php echo $v['description']; ?>" rtp="<?php echo $v['class_id']; ?>" subject="<?php echo $v['name']; ?>" end="<?php echo $v['finish_time']; ?>" tutor="<?php echo $v['user_name']; ?>" start="<?php echo $v['start_time']; ?>" class="btneditWaktu" data-target-color="green" href="#modalEditWaktu"><button class="btn bgm-bluegray" title="Ubah Waktu Kelas"><i class="zmdi zmdi-edit"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>
                            </table>                           
                        </div>
                    </div>
                </div>
                                            
            </div> 

            <!-- Modal TAMBAH -->  
            <div class="modal" id="modaladdstudent" tabindex="-1" role="dialog" style="top: 13%;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                            <h4 class="modal-title c-white">Tambah Akun</h4>
                        </div>
                        <div class="modal-body m-t-20">
                            <form method="post" action="<?php echo base_url('Process/add_channel'); ?>" enctype="multipart/form-data">
                            <div class="row p-15">    
                                
                                <div class="col-sm-12 col-md-12 col-xs-12">                                                    
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="text" name="channel_name" required class="input-sm form-control fg-input">
                                            <label class="fg-label">Nama Channel</label>
                                        </div>
                                    </div>
                                </div>                                

                                <div class="col-sm-12 m-t-5">                                                                                           
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="email" name="channel_email" required class="input-sm form-control fg-input">
                                            <label class="fg-label">Email</label>
                                        </div>
                                    </div>
                                </div>  

                                <div class="col-sm-12 m-t-5">                                                                                           
                                    <div class="form-group fg-float">
                                        <div class="fg-line">                                            
                                            <textarea rows="4" name="channel_address" required class="input-sm form-control fg-input"></textarea>
                                            <label class="fg-label">Alamat</label>
                                        </div>
                                    </div>
                                </div>                                
                                
                                <div class="col-sm-5 m-t-10">
                                    <div class="fg-line">                                                       
                                        <select required name="channel_country_code" class="select2 form-control">
                                            <?php
                                                $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                                foreach ($allsub as $row => $d) {
                                                    if($d['phonecode'] == $alluser['user_countrycode']){
                                                        echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                    }
                                                    if ($d['phonecode'] != $alluser['user_countrycode']) {
                                                        echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                    }                                                            
                                                }
                                            ?>                                        
                                        </select>
                                    </div>
                                    <!-- <span class="zmdi zmdi-flag form-control-feedback"></span> -->
                                </div>
                                <div class="col-sm-7 m-t-10">
                                    <div class="fg-line">
                                        <input type="text" id="no_hape" minlength="9" maxlength="13" name="channel_callnum" required class="form-control " placeholder="No Handphone">
                                    </div>
                                </div>
                                <div class="col-md-12 m-t-20">
                                    <button class="btn btn-success btn-block">Simpan</button>
                                </div>
                            </div>
                            </form>
                            <br>
                        </div>
                    </div>
                </div>
            </div>                             

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalEditWaktu" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form role="form" action="#" method="post" novalidate>
                            <div class="modal-header">
                                <h4 class="modal-title c-black">Ubah Waktu Kelas</h4>
                                <hr>
                                <div class="row" >
                                    <div class="col-sm-3">
                                        <label class="c-black f-15" >Subject </label><br>
                                        <label class="c-black f-15" >Description </label><br>
                                        <label class="c-black f-15" >Tutor </label><br>
                                    </div>
                                    <div class="col-sm-8">
                                        <label class="c-black f-15"> : </label>&nbsp;<label class="c-black f-15" id="txt_subject"></label><br>
                                        <label class="c-black f-15"> : </label>&nbsp;<label class="c-black f-15" id="txt_description"></label><br>
                                        <label class="c-black f-15"> : </label>&nbsp;<label class="c-black f-15" id="txt_nametutor"></label><br>                                        
                                    </div>                                    
                                    <br>
                                    <br>
                                    <br>
                                    
                                    <div class="col-md-6 m-t-5">
                                        <p class="c-black f-500" style="margin: 0;">Start Date</p>
                                        <div class="form-group fg-float">
                                            <div class="fg-float">
                                                <input id="tanggal" type='text' required class="form-control fg-input"  data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 m-t-5">
                                        <p class="c-black f-500" style="margin: 0;">Start Time</p>
                                        <div class="form-group fg-float">
                                            <div class="fg-float" id="tempatwaktuu">
                                                <input type='text' class='form-control' id='timeeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 m-t-5">
                                        <p class="c-black f-500" style="margin: 0;">Finish Date</p>
                                        <div class="form-group fg-float">
                                            <div class="fg-float">
                                                <input id="tanggal_finish" type='text' required class="form-control fg-input"  data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 m-t-5">
                                        <p class="c-black f-500" style="margin: 0;">Finish Time</p>
                                        <div class="form-group fg-float">
                                            <div class="fg-float" id="tempatwaktuuu">
                                                <input type='text' class='form-control' id='timeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
                                            </div>
                                        </div>
                                    </div>                                            
    
                                </div>
                            
                                
                            </div>                                                
                            <div class="modal-footer">
                                <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                                <button type="button" class="btn bgm-green" id="ubahDataClass">Ubah</button>
                                <!-- <button type="submit" class="btn bgm-green" data-id="" id="editWaktuKelas" rtp="">Ubah</button> -->
                                
                            </div>  
                        </form>                      
                    </div>
                </div>
            </div>



        </div>
    </section>

</section>

<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){

        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc; 

        var tgl = new Date();  
        var formattedDate = moment(tgl).format('YYYY-MM-DD');         
        
        $('#tanggal').datetimepicker({  
            minDate: moment(formattedDate, 'YYYY-MM-DD')
        });

        $('#tanggal').on('dp.change', function(e) {
            date = moment(e.date).format('YYYY-MM-DD');                
            var hoursnoww = tgl.getHours(); 
            // alert(tgl);
            if (date == formattedDate) {
                a = [];
                $("#tempatwaktuu").html("");
                $("#tempatwaktuu").append("<input type='text' class='form-control' id='time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                $("#time").removeAttr('disabled');
                for (var i = hoursnoww+1; i < 24; i++) {                                                
                    a.push(i);
                }
                console.warn(a);
                $('#time').datetimepicker({                    
                    sideBySide: true, 
                    showClose: true,                   
                    format: 'HH:mm',
                    stepping: 15,
                    enabledHours: a,
                });
            }
            else
            {
                $("#tempatwaktuu").html("");
                $("#tempatwaktuu").append("<input type='text' class='form-control' id='time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                $("#time").removeAttr('disabled');
                b = [];
                for (var i = 0; i < 24; i++) {                                                
                    b.push(i);
                }
                console.warn(b);
                $('#time').datetimepicker({                    
                    sideBySide: true,                    
                    format: 'HH:mm',
                    showClose: true,
                    stepping: 15,
                    enabledHours: b,
                });
            }            
        });

        $('#tanggal_finish').datetimepicker({  
            minDate: moment(formattedDate, 'YYYY-MM-DD')
        });

        $('#tanggal_finish').on('dp.change', function(e) {
            date = moment(e.date).format('YYYY-MM-DD');                
            var hoursnoww = tgl.getHours(); 
            // alert(tgl);
            if (date == formattedDate) {
                a = [];
                $("#tempatwaktuuu").html("");
                $("#tempatwaktuuu").append("<input type='text' class='form-control' id='time_finish' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                $("#time_finish").removeAttr('disabled');
                for (var i = hoursnoww+1; i < 24; i++) {                                                
                    a.push(i);
                }
                console.warn(a);
                $('#time_finish').datetimepicker({                    
                    sideBySide: true, 
                    showClose: true,                   
                    format: 'HH:mm',
                    stepping: 15,
                    enabledHours: a,
                });
            }
            else
            {
                $("#tempatwaktuuu").html("");
                $("#tempatwaktuuu").append("<input type='text' class='form-control' id='time_finish' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                $("#time_finish").removeAttr('disabled');
                b = [];
                for (var i = 0; i < 24; i++) {                                                
                    b.push(i);
                }
                console.warn(b);
                $('#time_finish').datetimepicker({                    
                    sideBySide: true,                    
                    format: 'HH:mm',
                    showClose: true,
                    stepping: 15,
                    enabledHours: b,
                });
            }            
        });
        
        $('.btneditWaktu').click(function(e){
            var class_id = $(this).attr('rtp');
            var subject = $(this).attr('subject');
            var tutor = $(this).attr('tutor');
            var description = $(this).attr('description');
            var start = $(this).attr('start');
            var end = $(this).attr('end');

            $("#txt_nametutor").text(tutor);
            $("#txt_subject").text(subject);
            $("#txt_description").text(description);
            $("#editstart_time").val(start);
            $("#editfinish_time").val(end);
            $("#ubahDataClass").attr('class_id', class_id);
            
        });
        $('#btn_today').click(function(e){
            var sekarang = moment(tgl).format('DD, MMM YYYY');    
            $("#status_class").text("Today ("+sekarang+")");
            $("#class_today").css('display','block');
            $("#class_week").css('display','none');
            $("#class_month").css('display','none');
            $("#class_all").css('display','none');
        });
        $('#btn_week').click(function(e){
            $("#status_class").text("This Week");
            $("#class_today").css('display','none');
            $("#class_week").css('display','block');
            $("#class_month").css('display','none');
            $("#class_all").css('display','none');
        });
        $('#btn_month').click(function(e){
            var bulan = moment(tgl).format('MMMM YYYY');    
            $("#status_class").text("This Month ("+bulan+")");
            $("#class_today").css('display','none');
            $("#class_week").css('display','none');
            $("#class_month").css('display','block');
            $("#class_all").css('display','none');
        });
        $('#btn_all').click(function(e){
            $("#status_class").text("All Date");
            $("#class_today").css('display','none');
            $("#class_week").css('display','none');
            $("#class_month").css('display','none');
            $("#class_all").css('display','block');
        });

        $(document).on("click", "#ubahDataClass", function(){ 
            var class_id        = $(this).attr('class_id');
            var edt_start       = $("#tanggal").val();
            var edt_starttime   = $("#time").val();
            var edt_finish      = $("#tanggal_finish").val();
            var edt_finishtime  = $("#time_finish").val();
            
            var date_start      = edt_start+' '+edt_starttime+':00';
            var date_finish     = edt_finish+' '+edt_finishtime+':00';
            $("#modalEditWaktu").modal("hide");
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/modifyDateTimeFromAdmin',
                type: 'POST',
                data: {
                    class_id: class_id,
                    date_start: date_start,
                    date_finish: date_finish,
                    user_device: 'web',
                    user_utc: user_utc
                },
                success: function(response)
                {                   
                    // alert(response['code']);
                    if (response['code'] == "200") {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Mengubah data Kelas");
                        setTimeout(function(){
                            location.reload();
                        },4000);
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Waktu sudah terisi! Mohon pilih waktu lain");
                        setTimeout(function(){
                            location.reload();
                        },4000);
                    }
                }
            });
        });
    });
 
    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable(); 
</script>

