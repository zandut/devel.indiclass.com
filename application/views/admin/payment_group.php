<style type="text/css">
    .hover11 img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .hover11:hover img {
        opacity: 0.5;
    }
</style>
<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Payment Group</h2>
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>            

            <div class="card m-t-20 p-20" style="">
                <div class="card-header">
                    <div class="pull-left"><h2>List Payment</h2></div>                    
                <br><br>
                <hr>

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="dataaa" class="display table table-striped table-bordered dataa" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $select_all = $this->db->query("SELECT ms.subject_name, tu.user_name, tu.email, tr.* FROM (tbl_request_grup as tr INNER JOIN master_subject as ms ON tr.subject_id=ms.subject_id) INNER JOIN tbl_user as tu ON tu.id_user=tr.tutor_id WHERE approve=2 or approve=1 ORDER BY request_id DESC")->result_array();
                                    ?>
                                    <tr>
                                        <th data-column-id="no">No</th>
                                        <th data-column-id="classid">Request ID</th>
                                        <th data-column-id="name">Subject Name</th>
                                        <th data-column-id="name">Topic</th> 
                                        <th data-column-id="jenjang">Tutor Name</th>  
                                        <th data-column-id="userrequest">User Request</th>
                                        <th data-column-id="starttime">Date Request</th>
                                        <th data-column-id="starttime">Time Request</th>
                                        <th data-column-id="finishtime">Duration</th>
                                        <th data-column-id="finishtime">Harga</th>
                                        <th data-column-id="aksi" style="text-align: right;" >Action</th>

                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    foreach ($select_all as $row => $v) {
                                        $iduserrequester = $v['id_user_requester'];
                                        $subjectid       = $v['subject_id'];
                                        $tutorid         = $v['tutor_id'];
                                        $datauser        = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequester'")->row_array()['user_name'];
                                        $dataprice       = $this->db->query("SELECT * FROM tbl_service_price WHERE class_type='private' AND subject_id='$subjectid' AND tutor_id='$tutorid'")->row_array()['harga_cm'];
                                        $durasi             = $v['duration_requested'];
                                        $hargaakhir         = ($durasi/900)*$dataprice;

                                        $user_utc           = $this->session->userdata('user_utc');
                                        $dataprice          = number_format($dataprice, 0, ".", ".");
                                        $hargaakhir         = number_format($hargaakhir, 0, ".", ".");
                                        $server_utcc        = $this->Rumus->getGMTOffset();
                                        $intervall          = $user_utc - $server_utcc;
                                        $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$v['date_requested']);
                                        $tanggal->modify("+".$intervall ." minutes");
                                        $tanggal            = $tanggal->format('Y-m-d H:i:s');
                                        $datelimit          = date_create($tanggal);
                                        $dateelimit         = date_format($datelimit, 'd/m/y');
                                        $harilimit          = date_format($datelimit, 'd');
                                        $hari               = date_format($datelimit, 'l');
                                        $tahunlimit         = date_format($datelimit, 'Y');
                                        $waktulimit         = date_format($datelimit, 'H:s');   
                                        $datelimit          = $dateelimit;
                                        $sepparatorlimit    = '/';
                                        $partslimit         = explode($sepparatorlimit, $datelimit);
                                        $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                                        $seconds            = $v['duration_requested'];
                                        $hours              = floor($seconds / 3600);
                                        $mins               = floor($seconds / 60 % 60);
                                        $secs               = floor($seconds % 60);
                                        $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['request_id']; ?></td>
                                            <td><?php echo $v['subject_name']; ?></td>
                                            <td><?php echo $v['topic']; ?></td>
                                            <td><?php echo $v['user_name']; ?></td>
                                            <td><?php echo $datauser; ?></td>                                            
                                            <td><?php echo $hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit ?></td>
                                            <td><?php echo $waktulimit;?></td>
                                            <td><?php echo $durationrequested." Jam" ?></td>
                                            <td><?php echo "Rp. ".$hargaakhir;?></td>
                                            <?php 
                                            if ($v['approve'] == -1) {
                                                ?>
                                                <td>
                                                    <button disabled class="btn btn-success" title="Confirm Transaction"><i class="zmdi zmdi-check"></i></button>
                                                </td>
                                               
                                                <?php
                                            }
                                            else if($v['approve'] == 1){
                                                ?>
                                                <td>
                                                    <button disabled class="btn btn-info" title="Sudah Terkonfirmasi"><i class="zmdi zmdi-badge-check"></i></button>
                                                </td>
                                                
                                                <?php
                                            }
                                            else
                                            {
                                                ?>
                                                <td>
                                                    <a data-toggle="modal" rtp="<?php echo $v['request_id'] ?>" tutor_id="<?php echo $v['tutor_id'];?>" requester="<?php echo $v['id_user_requester'];?>" href="#modalConfirm" class="confirmtransaction"><button class="btn btn-success" title="Confirm Transaction"><i class="zmdi zmdi-check"></i></button></a>
                                                </td>
                                                <!-- <td>
                                                    <a data-toggle="modal" rtp="<?php echo $v['request_id'] ?>" class="hapustransaksi" data-target-color="green" href="#modalDelete"><button class="btn btn-danger" title="Reject Transaction"><i class="zmdi zmdi-block"></i></button></a>
                                                </td> -->
                                                <?php
                                            }
                                            ?>
                                            
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div>                                 

            <!-- Modal CONFIRM -->  
            <div class="modal" id="modalConfirm" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 10%;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Konfirmasi Transaksi</h4>
                            <hr>
                            <p><label class="c-gray f-15">Harap periksa kembali data transaksi bank sebelum konfirmasi. Jika sudah silahkan klik tombol Konfirmasi</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="konfirmasidatatransaksi" rtp="">Konfirmasi</button>                            
                        </div>                        
                    </div>
                </div>
            </div>     

            <!-- Modal DELETE -->  
            <div class="modal" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 10%;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Hapus Transaksi</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="hapusdatatransaksi" rtp="">Ya</button>                            
                        </div>                        
                    </div>
                </div>
            </div>            

        </div>
    </section>

</section>

<script type="text/javascript">

    $(document).ready(function(){

        $('#konfirmasidatatransaksi').click(function(e){
            var rtp = $(this).attr('rtp');
            var tutor_id = $(this).attr('tutor_id');
            var requester = $(this).attr('requester');            
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/ConfirmPaymentGroup",
                type:"POST",
                data: {
                    rtp: rtp,
                    tutor_id:tutor_id,
                    id_user:requester
                },
                success: function(response){             
                    // alert("Berhasil menghapus data siswa");
                    $('#modalDelete').modal('hide');
                    $("#modalConfirm").modal('hide');
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Confirm Transaction");
                    setTimeout(function(){
                        location.reload(); 
                    },2000);
                                        
                } 
            });
        });

        $('#hapusdatatransaksi').click(function(e){
            var rtp = $(this).attr('rtp');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deletePaymentMulticast",
                type:"POST",
                data: {
                    rtp: rtp
                },
                success: function(html){             
                    // alert("Berhasil menghapus data siswa");
                    $('#modalDelete').modal('hide');
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Success reject Transaction");
                    setTimeout(function(){
                        location.reload(); 
                    },2000);
                                        
                } 
            });
        });
    });

    $(document).on("click", ".hapustransaksi", function () {
         var myBookId = $(this).attr('rtp');
         $('#hapusdatatransaksi').attr('rtp',myBookId);
    });   

    $(document).on("click", ".confirmtransaction", function () {
         var myBookId = $(this).attr('rtp');
         var tutor_id = $(this).attr('tutor_id');
         var requester = $(this).attr('requester');
         var class_id = $(this).attr('class_id');
         $('#konfirmasidatatransaksi').attr('rtp',myBookId);
         $('#konfirmasidatatransaksi').attr('tutor_id',tutor_id);
         $('#konfirmasidatatransaksi').attr('requester',requester);
         $('#konfirmasidatatransaksi').attr('class_id',class_id);

    });

    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.dataaa').DataTable(); 

   
</script>

