<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style=" z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Activity Channel</h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul> -->
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card">  
                <div class="card-header">
                    <div class="pull-left"><h2>Activity</h2></div>
                    <!-- <div class="pull-right"><a data-toggle="modal" href="#topup"><button class="btn btn-success">+ Topup Point</button></a></div> -->
                    <br><br>
                </div>              
                <div class="card-body card-padding">
                    <div class="row"> 
                        <div class="table-responsive">
                            <br>
                            <table id="list" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="bgm-teal c-white" style="width: 10px"><center>No</center></th>
                                        <th class="bgm-teal c-white" style="width: 15px"><center>Channel Name</center></th>
                                        <th class="bgm-teal c-white" style="width: 15px"><center>Channel Email</center></th>
                                        <th class="bgm-teal c-white" style="width: 50px"><center>Activity</center></th>
                                        <th class="bgm-teal c-white" style="width: 80px"></th>
                                        <th class="bgm-teal c-white" style="width: 10px"><center>IP Access</center></th>
                                        <th class="bgm-teal c-white" style="width: 20px"><center>Date</center></th>
                                        <th class="bgm-teal c-white" style="width: 20px"><center>Time</center></th>
                                    </tr>
                                </thead>
                                <tbody id="box_listpoint">
                                           
                                </tbody>
                            </table> 
                        </div>                           
                    </div>                                               
                </div>                                             
            </div> 

        </div>
    </section>

</section>
<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){
        // var channelid = "<?php echo $this->session->userdata('channel_id');?>";
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        $.ajax({
            url: '<?php echo base_url(); ?>Rest/activityChannel/access_token/'+tokenjwt,
            type: 'POST',
            success: function(response)
            {   
                var a = JSON.stringify(response);
                var jsonPretty = JSON.stringify(JSON.parse(a),null,2);                      
                // $("#aaa").text(jsonPretty);  
                // alert(response['data'][1]['class_id']);

                if (response['data'] == null) 
                {                                                
                }                   
                else
                {
                    var no = 0;
                    if (response.data.length != null) {
                        for (var i = response.data.length-1; i >=0;i--) {

                            var channel_name          = response['data'][i]['channel_name'];
                            var channel_email         = response['data'][i]['channel_email'];
                            var action          	  = response['data'][i]['action'];
                            var ip_from           	  = response['data'][i]['ip_from'];
                            var created_at            = response['data'][i]['created_at'];
                            var data         		  = response['data'][i]['data'];
                            var created_at_point      = response['data'][i]['created_at_point'];
                            var time_created_at       = response['data'][i]['time_created_at'];
                            var user_name 			  = null;
                            if (data != null) {
                            	var profileusername	  = response['data'][i]['profile'];
                            	for (var iai = 0; iai < profileusername.length ;iai++) {
                            		user_name 	  = profileusername[iai]['user_name'];
                            		// alert(user_name);
                            	}
                            }
                            else
                            {
                            	user_name = " - ";
                            }
                                                          
                            no += 1;

                            var kotak = "<tr><td><center>"+no+"</center></td><td><center>"+channel_name+"</center></td><td><center>"+channel_email+"</center></td><td><center>"+action+"</center></td><td><center>"+user_name+"</center></td><td><center>"+ip_from+"</center></td><td><center>"+created_at_point+"</center></td><td><center>"+time_created_at+"</center></td></tr>";
                            
                            $("#list tbody").append(kotak);
                            $("#list" ).DataTable();
                        }
                    }
                }
            }
        });

        $("#savetopup").click(function(){
            var channelid = $("#channelname").val();
            var amountpoint = $("#amountpoint").val();
            
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/topupPointChannel/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    channel_id: channelid,
                    amountpoint: amountpoint
                },
                success: function(response)
                {
                    var stat = response['code'];
                    if (stat == 200) {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Topup point success");
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Failed topup point");
                        setTimeout(function(){
                            location.reload();
                        },2000);
                    }
                }
            });
        });

        $('#amountpoint').on('keypress',function(e){
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                $("#numberonly").css('display', 'block');
                return false;
            }
        });
    });
</script>
