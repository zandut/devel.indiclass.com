<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sideadmin'); ?>
	</aside>

	<section id="content">
		<div class="container">            
			<div class="block-header">
				<h2>Evaluator</h2>
			</div>

			<div class="card m-t-20 p-20" style="">
                <div class="row" style="margin-top: 2%; overflow-y: auto;">                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>                                        
                                        <th>Photo</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    foreach ($alldatatutor as $row => $v) { 
                                    	$idkabupaten 	= $v['id_kabupaten'];
                                    	$namakab 		= $this->db->query("SELECT kabupaten_name FROM master_kabupaten WHERE kabupaten_id='$idkabupaten'")->row_array()['kabupaten_name'];
                                    	$foto  			= $v['user_image'];                                    	                                    	
                                        ?>
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['user_name']; ?></td>
                                            <td><?php echo $v['email']; ?></td>
                                            <td>
		                                    	<img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$foto; ?>" class="pv-main" style='width: 100px; height: 100px;' alt="">
                                            </td>
                                            <td>
                                                <a class="card profile-tutor" href="<?php echo base_url();?>admin/ViewEvaluator?id=<?php echo $v['id_user'];?>"><button class="btn btn-default" title="Detail tutor"><i class="zmdi zmdi-search"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div>
            </div> 		

		</div>
	</section>

</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	$('.card.profile-tutor').click(function(){
		var ids = $(this).attr('id');
		ids = ids.substr(5,10);		
		$.ajax({
			url: '<?php echo base_url();?>Admin/getSubjectTutor',
			data: {
                tutor_id: ids
            },            
			type: 'POST',
			success: function(response){
				$('#dataSubject tbody').empty();
				response = JSON.parse(response);
				if(response['status'] == 1){
					for (var i = 0; i < response['data'].length; i++) {
						var subject_name 	= response['data'][i]['subject_name'];
						var jenjang_name 	= response['data'][i]['jenjang_name'];
						var jenjang_level	= response['data'][i]['jenjang_level'];
						var box = '<tr>'+
							'<td>'+(i+1)+'</td>'+
							'<td>'+subject_name+'</td>'+
							'<td>'+jenjang_name+'</td>'+
							'<td>'+jenjang_level+'</td>'+
							'<td></td>'+
							'</tr>';
						$('#dataSubject tbody').append(box);
						$('.dataSubject').DataTable();
					}
					$("#descTutor").modal('show');
					// $("#data").DataTable();
				}
			}
		});
		
	});

	$('table.display').DataTable({
        fixedHeader: {
            header: true,
            footer: true
        }
    });

    $('.data').DataTable(); 

</script>