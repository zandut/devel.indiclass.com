<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style=" z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>List Point Channel</h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul> -->
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card">  
                <div class="card-header">
                    <div class="pull-left"><h2>List</h2></div>
                    <div class="pull-right"><a data-toggle="modal" href="#topup"><button class="btn btn-success">+ Topup Quota</button></a></div>
                    <br><br>
                </div>              
                <div class="card-body card-padding">
                    <div class="row"> 
                        <div class="table-responsive">
                            <br>
                            <table id="list" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="bgm-teal c-white" style="width: 10px">No</th>
                                        <th class="bgm-teal c-white" style="width: 15px">Channel Name</th>
                                        <th class="bgm-teal c-white" style="width: 10px">Quota</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Validity Period</th>
                                        <th class="bgm-teal c-white" style="width: 10px">Channel Email</th>
                                        <th class="bgm-teal c-white" style="width: 30px">Status Package</th>                                        
                                        <th class="bgm-teal c-white" style="width: 10px">Action</th>   
                                    </tr>
                                </thead>
                                <tbody id="box_listpoint">
                                           
                                </tbody>
                            </table> 
                        </div>                           
                    </div>                                               
                </div>                                             
            </div> 

            <!-- Modal TAMBAH -->  
            <div class="modal" id="topup" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Topup Quota</h4>
                            <hr>
                        </div>                        
                        <div class="modal-body">
                            <div class="card">
                            <div class="row p-15">   

                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Channel Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <div class="select">
                                                    <select required name="channelname" id="channelname" class="selectpicker" data-live-search="true" style="z-index: 10;">
                                                    <option disabled selected>Pilih Channel</option>   
                                                    <?php
                                                       $allchannel = $this->db->query("SELECT mc.* FROM  master_channel as mc")->result_array();
                                                       foreach ($allchannel as $row => $v) {
                                                            echo '<option value="'.$v['channel_id'].'">'.$v['channel_name'].'</option>';
                                                        }
                                                    ?>                                                    
                                                    </select>
                                                </div>

                                            </div>                                                
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Amount Quota</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="amountpoint" id="amountpoint" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" maxlength="5" /> 
                                                <label class="c-red" id="numberonly" style="display: none;">Please enter number only</label>   
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn bgm-green" id="savetopup">Topup</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>                              

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Hapus Akun Bank</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>
                            <input type="text" name="id_rekeningg" id="id_rekeningg" class="c-black" value=""/>
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" id="hapusdatarekening">Ya</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalConfirm" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Confirm Acitvate Package</h4>
                            <hr>
                            <div class="row col-md-12 col-sm-12 col-xs-12'">
                                <label hidden="" class="c-black" id="det_channel_id"></label>
                                <label hidden="" class="c-black" id="det_pid"></label>
                                <label hidden="" class="c-black" id="det_start_periode"></label>
                                <label hidden="" class="c-black" id="det_end_periode"></label>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <label class="c-black">Channel Name</label><br>
                                    <label class="c-black">Total Quota</label><br>
                                    <label class="c-black">Activate Period</label>
                                    <label class="c-black">Invoice Payment</label>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <label class="c-black"> : <b class="c-black" id="det_channel_name"> Kolaborasi Indonesia</b></label><br>
                                    <label class="c-black"> : <b class="c-black" id="det_total_quota"> 30</b></label><br>
                                    <label class="c-black"> : <b class="c-black" id="det_periode"> June - 2018</b></label><br>
                                    <label class="c-black"> : <b class="c-black" id="det_invoice_ch"> CH0080102A</b></label>
                                </div>
                            </div>
                            <br>
                            <p><label class="c-black f-16">Do You want to activate this Package?</label></p>
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn bgm-green" id="act_activate">Yes</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="animated fadeIn modal fade" style="color: white; margin-top: 15%" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" >
                    <div class="modal-content" id="modal_konten">
                        <div class="modal-body" align="center">
                            <label id="text_modal">Halloo</label><br>
                            <button   id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

</section>
<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){
        // var channelid = "<?php echo $this->session->userdata('channel_id');?>";
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        $.ajax({
            url: '<?php echo base_url(); ?>Rest/getListKuotaChannel/access_token/'+tokenjwt,
            type: 'POST',
            success: function(response)
            {   
                var a = JSON.stringify(response);
                var jsonPretty = JSON.stringify(JSON.parse(a),null,2);                      
                // $("#aaa").text(jsonPretty);  
                // alert(response['data'][1]['class_id']);

                if (response['data'] == null) 
                {                                                
                }                   
                else
                {
                    var no = 0;
                    if (response.data.length != null) {
                        for (var i = response.data.length-1; i >=0;i--) {
                            // if (response['data'][i]['status_active'] == 1) {
                                var channel_id          = response['data'][i]['channel_id'];
                                var pid          = response['data'][i]['pid'];
                                var channel_name          = response['data'][i]['channel_name'];
                                var status_active           = response['data'][i]['status_active']
                                var quota_channel          = response['data'][i]['kuota'];
                                var channel_email         = response['data'][i]['channel_email'];
                                var start_date         = response['data'][i]['start_date'];
                                var start = moment(start_date).format("YYYY-MM");
                                var end_date         = response['data'][i]['end_date'];
                                var end = moment(end_date).format("YYYY-MM");

                                
                                var start_periode = moment(start_date).format("MMMM - YYYY");
                                var end_periode = moment(end_date).format("MMMM - YYYY");
                                if (start_periode == end_periode) {
                                    var periode = start_periode;
                                }
                                else if (start_periode != end_periode) {
                                    var periode = start_periode +" - "+end_periode;
                                }
                                if (status_active == -1 ) {
                                    status_active ="<label style='color:red'>Expired</label>";
                                    var action ="<a disabled='true' data-toggle='modal'   chid='"+channel_id+"' class='' data-target-color='green' href='#'><button disabled='true' class='btn bgm-bluegray' title='Activate Package'><i class='zmdi zmdi-edit'></i></button></a>";
                                }else if (status_active == 1 ) {
                                    status_active ="<label style='color:green'>Active</label>";
                                    var action ="<a disabled='true' data-toggle='modal'   chid='"+channel_id+"' class='' data-target-color='green' href='#'><button disabled='true' class='btn bgm-bluegray' title='Activate Package'><i class='zmdi zmdi-edit'></i></button></a>";
                                }
                                else{
                                    status_active ="<label style='color:orange'>Not Active</label>";   
                                    var action ="<a data-toggle='modal' pid='"+pid+"' chname='"+channel_name+"'  quota='"+quota_channel+"' start='"+start+"' end='"+end+"' periode='"+periode+"' chid='"+channel_id+"' class='act_topup' data-target-color='green' href='#modalConfirm'><button class='act_topup btn bgm-bluegray' title='Activate Package'><i class='zmdi zmdi-edit'></i></button></a>";

                                }

                                // var channel_number        = response['data'][i]['channel_country_code']+" "+response['data'][i]['channel_callnum'];                              
                                no += 1;

                                var kotak = "<tr><td>"+no+"</td><td>"+channel_name+"</td><td>"+quota_channel+"</td><td>"+periode+"</td><td>"+channel_email+"</td><td>"+status_active+"</td><td>"+action+"</td></tr>";
                                
                                $("#list tbody").append(kotak);
                                $("#list" ).DataTable();
                                $('.act_topup').click(function(e){
                                    var dt_chid = $(this).attr('chid');
                                    var dt_pid = $(this).attr('pid');
                                    var dt_quota = $(this).attr('quota');
                                    var dt_start = $(this).attr('start');
                                    var dt_end = $(this).attr('end');
                                    var dt_periode = $(this).attr('periode');
                                    var dt_chname = $(this).attr('chname');

                                    $("#det_start_periode").text(dt_start);
                                    $("#det_end_periode").text(dt_end);
                                    $("#det_channel_id").text(dt_chid);
                                    $("#det_pid").text(dt_pid);
                                    $("#det_channel_name").text(dt_chname);
                                    $("#det_periode").text(dt_periode);
                                    $("#det_total_quota").text(dt_quota);
                                    // $("#det_invoice_ch").text(channel_name);
                                   
                                });
                            // }
                        }
                    }
                }
            }
        });



        $("#act_activate").click(function(){
            var act_channel_id = $("#det_channel_id").text();
            var act_pid = $("#det_pid").text();
            var act_quota = $("#det_total_quota").text();
            var act_start_date = $("#det_start_periode").text();
            var act_end_date = $("#det_end_periode").text();
            // alert(act_channel_id);
            // alert(act_quota);
            // alert(act_start_date);
            // alert(act_end_date);
            $.ajax({
                    url : 'https://air.classmiles.com/v1/change_actIn_package/access_token/'+tokenjwt,
                    type:"post",
                    data: {
                        channel_id: act_channel_id,
                        pid : act_pid,
                    },
                    success: function(response){             
                        var code = response['code'];
                        if (code == 200) {
                            $("#modalConfirm").modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#32c787');
                            $("#text_modal").html("Successfully Activate this Package");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                        }
                        else if (code == -400) {
                            window.location.href='<?php echo base_url();?>Admin/Logout';
                        }
                        else if (code == -100) {
                            $("#modalConfirm").modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#ff3333');
                            $("#text_modal").html("Can not Activate this package. This package is already active");
                            $("#button_ok").click( function(){
                                $("#modalConfirm").modal('show');
                                $("#modal_alert").modal('hide');
                            });
                        }
                        else{                        
                            $("#modal_alert").modal('show');
                            $("#modalConfirm").modal('hide');
                            $("#text_modal").html("There is an error!!!");
                            $("#modal_konten").css('background-color','#ff3333');
                            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','There is an error!!!');
                            $("#button_ok").click( function(){
                                location.reload();
                            });

                        } 
                    }
                });
            
        });

        $("#asavetopup").click(function(){
            var channelid = $("#channelname").val();
            var amountpoint = $("#amountpoint").val();
            
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/topupPointChannel/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    channel_id: channelid,
                    amountpoint: amountpoint
                },
                success: function(response)
                {
                    var stat = response['code'];
                    if (stat == 200) {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Topup point success");
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Failed topup point");
                        setTimeout(function(){
                            location.reload();
                        },2000);
                    }
                }
            });
        });

        $('#amountpoint').on('keypress',function(e){
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                $("#numberonly").css('display', 'block');
                return false;
            }
        });
    });
</script>
