<main class="main">

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <div class="content__inner content__inner--lg">
            <header class="content__title">
                <h1>Setting</h1>
                <small>Mengubah data data channel</small>               
            </header>

            <div class="card">
                <div class="card-block">
                    <h4 class="card-block__title mb-4">Mengubah banner channel</h4>

                    <img src="" id="bannerChannel" onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" alt="" style="height: 370px; width: 100%; background-color: #ececec;">
                    <a href="#" class="zmdi zmdi-camera profile__img__edit" data-target="#modalBanner" data-toggle="modal" style="margin-top: 5%; margin-left: 2%;"></a>
                </div>
            </div>

            <div class="card profile">
                
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <img src="" onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" id="logoChannel" alt="" style="height: 35%; width: 100%; background-color: #ececec;">

                            <a href="#" class="zmdi zmdi-camera profile__img__edit"></a>

                            <div class="col-md-12 col-xs-12 p-0" style="margin-top: 5%;">
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary btn-block waves-effect" id="simpanLogoChannel" data-target="#modalLogo" data-toggle="modal"><i class="zmdi zmdi-camera"></i> Ganti Logo Channel</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-xs-8">  
                            <div class="row p-10">                          
                                <div class="col-md-12 col-xs-12">
                                    <h1 class="card-block__title" style="font-size: 18px; margin-bottom: 3%;">Detail Akun Channel : </h1>
                                    <div class="form-group" id="formName">
                                        <h3 class="card-block__title">Nama Channel</h3>
                                        <input type="text" class="form-control" id="nameChannel" required placeholder="Isi nama channel disini">
                                        <i class="form-group__bar"></i>
                                    </div> 
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group" id="formDeskripsi">
                                        <h3 class="card-block__title">Deskripsi Channel</h3>
                                        <textarea class="form-control" rows="5" id="deskripsiChannel" required placeholder="Isi deskripsi channel disini"></textarea>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group" id="formChannel>
                                        <h3 class="card-block__title">Email Channel</h3>
                                        <input type="text" class="form-control" id="emailChannel" required placeholder="Isi email channel disini">
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h3 class="card-block__title">Kontak Channel</h3>
                                </div>                                
                                <div class="col-md-3 col-xs-3">
                                    <div class="form-group" id="formKode">
                                        <select class="select2 form-control" required required id="kodetelponChannel" style="width: 100%;">
                                            <option disabled="disabled" selected="" value="0">Pilih Kode Area</option>                            
                                        </select> 
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <div class="form-group" id="formNomer">                                        
                                        <input type="text" class="form-control" id="nomerChannel" required placeholder="Isi nomer telepon channel disini">
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group" id="formAlamat">
                                        <h3 class="card-block__title">Alamat Channel</h3>
                                        <textarea class="form-control" rows="5" id="alamatChannel" required placeholder="Isi alamat channel disini"></textarea>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group" id="formLink">     
                                        <h3 class="card-block__title">Link Channel : https://classmiles.com/channel/</h3>                                   
                                        <input type="text" class="form-control" id="linkChannel" required placeholder="Isi link channel disini">
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>                                
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group" id="formTentang">
                                        <h3 class="card-block__title">Tentang Channel</h3>
                                        <textarea class="form-control" rows="5" id="tentangChannel" required placeholder="Isi tentang channel disini"></textarea>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-block waves-effect" id="simpanChannel">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            
            <div class="modal " data-modal-color="" id="modalLogo" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <form id="form3" method="post" enctype="multipart/form-data">
                        <div class="modal-header">
                            <center>
                                <h4 class="modal-title" style="color:gray; margin-bottom:-20px;"><?php echo $this->lang->line('changelogo');?></h4>                
                            </center>
                        </div>                        
                        <div class="modal-body">
                            <center>                        
                                <input type="file" required name="inputFile1" id="inputFile1" class="filestyle" accept="image/x-png,image/gif,image/jpeg" data-placeholder="No Selected" style="margin-left:10px;">  
                                <img src="" onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" style='width: 256px; height: 256px; margin-top: 5%; background-color: #ececec;' id="viewgambar">
                            </center>                            
                            <h4 class="c-gray">Note: </h4>
                            <ul class="c-red">
                                <li><?php echo $this->lang->line('maxsizelogo');?></li>
                                <li><?php echo $this->lang->line('sizelogo');?></li>                   
                            </ul>
                            
                            <script>
                                function readURL(input) {
                                    if (input.files && input.files[0]) {
                                        var reader = new FileReader();

                                        reader.onload = function (e) {
                                            $('#viewgambar').attr('src', e.target.result);
                                        }

                                        reader.readAsDataURL(input.files[0]);
                                    }
                                }

                                $("#inputFile1").change(function () {                                    
                                    readURL(this);
                                });
                     
                            </script>   
                        </div>
                        <div class="progress progress-striped active m-t-10" style="height:18px;">
                            <div class="progress-bar" style="width:0%; height:100px;"></div>
                        </div>
                        <div class="modal-footer" style="background-color: #009688;">
                            <button type="button" class="btn btn-link" style="background-color: #fff; margin-top: 3%;" data-dismiss="modal">cancel</button>
                            <button type="button" class="btn btn-info upload" id="save_logo" name="save_logo" style="margin-top: 3%;"><?php echo $this->lang->line('upload');?></button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal " data-modal-color="" id="modalBanner" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                    <form id="form4" method="post" enctype="multipart/form-data">
                        <div class="modal-header">
                            <center>
                                <h4 class="modal-title" style="color:gray; margin-bottom:-20px;">Change Banner</h4>                
                            </center>
                        </div>                        
                        <div class="modal-body">
                            <center>                        
                                <input type="file" required name="inputFile2" id="inputFile2" class="filestyle" accept="image/x-png,image/gif,image/jpeg" data-placeholder="No Selected" style="margin-left:10px;">  
                                <img src="" onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" style='width: 100%; height: 256px; margin-top: 5%; margin-bottom: 5%; background-color: #ececec;' id="viewgambar1">
                            </center>                            
                            <h4 class="c-gray">Note: </h4>
                            <ul class="c-red">
                                <li><?php echo $this->lang->line('maxsizelogo');?></li>
                                <li>Size of logo must be 1268 x 720 pixels or bigger.</li>                   
                            </ul>
                            
                            <script>
                                function readURL2(input) {
                                    if (input.files && input.files[0]) {
                                        var reader = new FileReader();

                                        reader.onload = function (e) {
                                            $('#viewgambar1').attr('src', e.target.result);
                                        }

                                        reader.readAsDataURL(input.files[0]);
                                    }
                                }

                                $("#inputFile2").change(function () {                                    
                                    readURL2(this);
                                });
                      
                            </script>   
                        </div>
                        <div class="progress progress-striped active m-t-10" style="height:18px;">
                            <div class="progress-bar" style="width:0%; height:100px;"></div>
                        </div>
                        <div class="modal-footer" style="background-color: #009688;">
                            <button type="button" class="btn btn-link" style="background-color: #fff; margin-top: 3%;" data-dismiss="modal">cancel</button>
                            <button type="button" class="btn btn-info upload" id="save_banner" name="save_banner" style="margin-top: 3%;"><?php echo $this->lang->line('upload');?></button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>

    </section>
</main>        
<script type="text/javascript">
    $('#kodetelponChannel').select2();
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";    
    $.ajax({
        url: '<?php echo AIR_API;?>channel_getTentang/access_token/'+access_token,
        type: 'POST',
        data: {
            channel_id: channel_id
        },
        success: function(response)
        {
            var code    = response['code'];

            if (code == 200) {
                var channel_name            = response['data']['channel_name'];
                var channel_description     = response['data']['channel_description'];
                var channel_about           = response['data']['channel_about'];
                var channel_country_code    = response['data']['channel_country_code'];
                var channel_callnum         = response['data']['channel_callnum'];
                var channel_email           = response['data']['channel_email'];
                var channel_address         = response['data']['channel_address'];
                var channel_logo            = response['data']['channel_logo'];
                var channel_banner          = response['data']['channel_banner'];
                var channel_link            = response['data']['channel_link'];
                var imgLogo                 = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+channel_logo;
                var imgBanner               = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+channel_banner;                
                
                $("#nameChannel").val(channel_name);
                $("#deskripsiChannel").val(channel_description);
                $("#emailChannel").val(channel_email);
                $("#nomerChannel").val(channel_callnum);
                $("#alamatChannel").val(channel_address);
                $("#linkChannel").val(channel_link);
                $("#tentangChannel").val(channel_about);
                $("#logoChannel").attr('src',imgLogo);
                $("#viewgambar").attr('src',imgLogo);
                $("#bannerChannel").attr('src',imgBanner);
                $("#viewgambar1").attr('src',imgBanner);

                $.ajax({
                    url: '<?php echo AIR_API;?>countryCode/access_token/'+access_token,
                    type: 'POST',
                    success: function(response)
                    {
                        var a = JSON.stringify(response);
                        if (response['code'] == -400) {
                            window.location.href='<?php echo base_url();?>Admin/Logout'
                        }
                        for (var i = 0; i < response.data.length; i++) {
                            var nicename = response['data'][i]['nicename'];
                            var phonecode = response['data'][i]['phonecode'];                            
                            if (channel_country_code == phonecode) {
                                $("#kodetelponChannel").append("<option selected value='"+phonecode+"'>"+nicename+" +"+phonecode+"</option>");
                            }
                            else
                            {
                                $("#kodetelponChannel").append("<option value='"+phonecode+"'>"+nicename+" +"+phonecode+"</option>");
                            }
                        }
                    }
                });
            }   
            else if (code == -400) {
                window.location.href='<?php echo base_url();?>Admin/Logout';
            }
            else
            {
                window.location.href='<?php echo base_url();?>Admin/Logout';
            }            
            
        }
    });

    $("#simpanChannel").click(function(){
        var nameChannel         = $("#nameChannel").val();
        var deskripsiChannel    = $("#deskripsiChannel").val();        
        var kodetelponChannel   = $("#kodetelponChannel").val();
        var emailChannel        = $("#emailChannel").val();
        var nomerChannel        = $("#nomerChannel").val();
        var alamatChannel       = $("#alamatChannel").val();
        var linkChannel         = $("#linkChannel").val();
        var tentangChannel      = $("#tentangChannel").val();
        if (nameChannel == ""|| deskripsiChannel==""||emailChannel==""||kodetelponChannel==null||emailChannel==""||nomerChannel==""||alamatChannel==""||linkChannel==""||tentangChannel=="") {
            alert('Mohon untuk mengisi Nama Channel');
        }
        else
        {
            $.ajax({
                url: '<?php echo AIR_API;?>channel_saveTentang/access_token/'+access_token,
                type: 'POST',
                data: {
                    channel_id: channel_id,
                    channel_name: nameChannel,
                    channel_email: emailChannel,
                    channel_description: deskripsiChannel,
                    channel_about: tentangChannel,
                    channel_country_code: kodetelponChannel,
                    channel_callnum: nomerChannel,                    
                    channel_address: alamatChannel,
                    channel_link: linkChannel
                },
                success: function(response)
                {                    
                    if (response['code'] == 200) {                        
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda berhasil mengubah data Channel");
                        setTimeout(function(){
                            location.reload();
                        },1500);
                    }
                    else if(response['code'] == -300){
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Terjadi kesalahan mengubah data, silahkan coba kembali");
                        setTimeout(function(){
                            location.reload();
                        },1500);
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                    else
                    {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                }
            });
        }        
    });

    $("#save_logo").click(function(){
        data = new FormData();
        data.append('file', $('#inputFile1')[0].files[0]);

        var imgname  =  $('input[type=file]').val();
        var size  =  $('#inputFile1')[0].files[0].size;

        var ext =  imgname.substr( (imgname.lastIndexOf('.') +1) );
        $.ajax({
            url: '<?php echo AIR_API;?>channel_updateLogo/access_token/'+access_token,
            type: 'POST',
            data: data,            
            enctype: 'multipart/form-data',
            processData: false,  // tell jQuery not to process the data
            contentType: false,
            success: function(response)
            {
                var code = response['code'];
                if (code == 200) {
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Berhasil mengubah Logo');
                    setTimeout(function(){                            
                        location.reload();
                    },2000);
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
                else
                {
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','Terjadi Kesalahan, Silahkan coba kembali.');
                    setTimeout(function(){
                        location.reload();
                    },2000);
                }
            }
        });
    });

    $("#save_banner").click(function(){
        data = new FormData();
        data.append('file', $('#inputFile2')[0].files[0]);

        var imgname  =  $('input[type=file]').val();
        var size  =  $('#inputFile2')[0].files[0].size;

        var ext =  imgname.substr( (imgname.lastIndexOf('.') +1) );
        $.ajax({
            url: '<?php echo AIR_API;?>channel_updateBanner/access_token/'+access_token,
            type: 'POST',
            data: data,            
            enctype: 'multipart/form-data',
            processData: false,  // tell jQuery not to process the data
            contentType: false,
            success: function(response)
            {
                var code = response['code'];
                if (code == 200) {
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Berhasil mengubah Banner');
                    setTimeout(function(){                            
                        location.reload();
                    },2000);
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
                else
                {
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','Terjadi Kesalahan, Silahkan coba kembali.');
                    setTimeout(function(){
                        location.reload();
                    },2000);
                }
            }
        });        
    });        

</script>