<main class="main">

    <header class="header">
        <?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>List Request Join Student</h1>
            <small>Detail list of students listed on your channel!</small>      
        </header>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">
                    <div class="row table-responsive">
                        <div id="tables_listStudent"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" >
                <div class="modal-content" id="modal_konten">
                    <div class="modal-body" align="center">
                        <label id="text_modal">Halloo</label><br>
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade show" id="modalDeleteStudent" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Delete Student Account</b></h3>
                    </div>
                    <div class="modal-body">                        
                        <label class="c-gray f-15">Are you sure you want to delete?</label>                         
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                        <button type="button" data-dismiss="modal" class="btn btn-danger waves-effect" id="proses_hapusdatauser" data-id="" rtp="">Yes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalStatusStudent" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Change Status</b></h3>
                    </div>
                    <div class="modal-body" id="body_status">                  
                        

                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>        
<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = [];

    $(document).ready(function() {
        $.ajax({
            url: '<?php echo AIR_API;?>listStudentJoinChannel/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response);  
                var code = response['code'];                
                if (code == 200) {
                    for (var i = 0; i < response.data.length; i++) {
                        var uid = response['data'][i]['uid'];
                        var channel_id = response['data'][i]['channel_id'];
                        var id_user = response['data'][i]['id_user'];
                        var created_at = response['data'][i]['created_at'];
                        var user_name = response['data'][i]['user_name'];
                        var email = response['data'][i]['email'];
                        var user_callnum = response['data'][i]['user_callnum'];
                        var user_gender = response['data'][i]['user_gender'];
                        var user_address = response['data'][i]['user_address'];
                        var status = response['data'][i]['status'];
                        if (status =="inactive"){
                            status = "<label style='color:red;'>INCATIVE</label>";
                        }
                        else if (status =="active") {
                            status = "<label style='color:green;'>ACTIVE</label>";
                        }
                        var action = "<button class='change_status btn waves-effect' style='background-color: #607D8B;' data-iduser='"+id_user+"'><i class='zmdi zmdi-settings' style='color:#fff;'></i></button>";
                        dataSet.push([i+1, user_name, email, user_callnum, user_gender, user_address, action],);
                    }

                    $('#tables_listStudent').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="studentList"></table>' );
             
                    $('#studentList').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Name of Student"},
                            { "title": "Email"},
                            { "title": "No Telp"},
                            { "title": "Gender"},
                            { "title": "Address"},                            
                            { "title": "Action" }
                        ]
                    });  
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
                else
                {
                    $('#tables_listStudent').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="studentList"></table>' );
             
                    $('#studentList').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Name of Student"},
                            { "title": "Email"},
                            { "title": "No Telp"},
                            { "title": "Gender"},
                            { "title": "Address"},                            
                            { "title": "Action" }
                        ]
                    }); 
                }
            }
        });             

        $(document).on('click', '.change_status', function(){
            $("#body_status").empty();
            var iduser    = $(this).data('iduser');
            $("#modalStatusStudent").modal("show");
            // alert(iduser);
            $.ajax({
                url: '<?php echo AIR_API;?>listStudentJoinChannel/access_token/'+access_token,
                type: 'POST',
                data: {
                    channel_id: channel_id
                },
                success: function(response)
                {
                    var a = JSON.stringify(response);  
                    var code = response['code'];                
                    if (code == 200) {
                        for (var i = 0; i < response.data.length; i++) {
                            var uid = response['data'][i]['uid'];
                            var channel_id = response['data'][i]['channel_id'];
                            var id_user = response['data'][i]['id_user'];
                            var status = response['data'][i]['flag_membership'];
                            if (status=="0" && id_user == iduser) {
                                var button_status = "<center>"+
                                "<div class='row col-md-12' style='margin-top: 5%;'>"+
                                    "<div class='col-md-6'>"+
                                        "<button type='button' class='btn btn-lg btn-success waves-effect' id='activate' iduser='"+id_user+"'>Approve</button>"+
                                    "</div>"+
                                    "<div class='col-md-6'>"+
                                        "<button type='button' disabled='true' class='btn btn-lg btn-danger waves-effect' id='inactive' iduser='"+id_user+"' >Reject</button>"+
                                    "</div>"+
                                "</div>"+
                            "</center> ";
                                $("#body_status").append(button_status);
                            }
                            else if (status=="1" && id_user == iduser) {
                                var button_status = "<center>"+
                                "<div class='row col-md-12' style='margin-top: 5%;'>"+
                                    "<div class='col-md-6'>"+
                                        "<button type='button' disabled='true' class='btn btn-lg btn-success waves-effect' id='activate' iduser='"+id_user+"'>Approve</button>"+
                                    "</div>"+
                                    "<div class='col-md-6'>"+
                                        "<button type='button' class='btn btn-lg btn-danger waves-effect' id='inactive' iduser='"+id_user+"' >Reject</button>"+
                                    "</div>"+
                                "</div>"+
                            "</center> ";
                                $("#body_status").append(button_status);
                            }
                            else{
                                
                            }
                        }

                      
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                    else
                    {
                       
                    }
                }
            }); 
        });
        $(document).on('click', '.action_student', function(){
            var id_user    = $(this).data('iduser');
            $('#proses_hapusdatauser').attr('rtp',id_user);
            $("#modalDeleteStudent").modal("show");
        });
        $(document).on('click', '#activate', function(){
            var id_user = $(this).attr('iduser');
             $.ajax({
                url: '<?php echo AIR_API;?>change_join_student/access_token/'+access_token,
                type: 'POST',
                data: {
                    id_user: id_user,
                    channel_id: channel_id
                },
                success: function(response)
                {
                    var a = JSON.stringify(response);  
                    var code = response['code'];
                    $("#body_status").empty();
                    var button_status = "<center>"+
                            "<div class='row col-md-12' style='margin-top: 5%;'>"+
                                "<div class='col-md-6'>"+
                                    "<button type='button' disabled='true' class='btn btn-lg btn-success waves-effect' id='activate' iduser='"+id_user+"'>Approve</button>"+
                                "</div>"+
                                "<div class='col-md-6'>"+
                                    "<button type='button'  class='btn btn-lg btn-danger waves-effect' id='inactive' iduser='"+id_user+"' >Reject</button>"+
                                "</div>"+
                            "</div>"+
                        "</center> ";
                        $("#body_status").append(button_status);
                        
                        $("#modalStatusStudent").modal("hide");
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully activate");
                        $("#button_ok").click( function(){
                            location.reload();
                        });

                }
            });
        });
        $(document).on('click', '#inactive', function(){
            var id_user = $(this).attr('iduser');
            $.ajax({
                url: '<?php echo AIR_API;?>change_join_student/access_token/'+access_token,
                type: 'POST',
                data: {
                    id_user: id_user,
                    channel_id: channel_id
                },
                success: function(response)
                    {
                        var a = JSON.stringify(response);  
                        var code = response['code'];
                        $("#body_status").empty();
                        var button_status = "<center>"+
                                "<div class='row col-md-12' style='margin-top: 5%;'>"+
                                    "<div class='col-md-6'>"+
                                        "<button type='button'  class='btn btn-lg btn-success waves-effect' id='activate' iduser='"+id_user+"'>Approve</button>"+
                                    "</div>"+
                                    "<div class='col-md-6'>"+
                                        "<button type='button' disabled='true'  class='btn btn-lg btn-danger waves-effect' id='inactive' iduser='"+id_user+"' >Reject</button>"+
                                    "</div>"+
                                "</div>"+
                            "</center> ";
                            $("#body_status").append(button_status);
                            
                            $("#modal_alert").modal('show');
                            $("#modalStatusStudent").modal("hide");
                            $("#modal_konten").css('background-color','#32c787');
                            $("#text_modal").html("Successfully deactivate");
                            $("#button_ok").click( function(){
                                location.reload();
                            });

                    }
            });
        });

            $('#proses_hapusdatauser').click(function(e){
                var rtp = $(this).attr('rtp');
                //menggunakan fungsi ajax untuk pengambilan data
                $.ajax({
                    url :"<?php echo AIR_API;?>deleteStudent/access_token/"+access_token,
                    type:"post",
                    data: {
                        rtp: rtp,
                        channel_id: channel_id
                    },
                    success: function(response){   
                        var code = response['code'];
                        if (code == 200) {

                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#32c787');
                            $("#text_modal").html("Successfully deleted Student data");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                            /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully deleted Student data");
                            $('#modalDeleteStudent').modal('hide');
                            setTimeout(function(){
                                window.location.reload();
                            },2000);*/
                        }   
                        else if (code == -400) {
                            window.location.href='<?php echo base_url();?>Admin/Logout';
                        }
                        else{
                            $('#modalDeleteStudent').modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#ff3333');
                            $("#text_modal").html("There is an error!!!");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                            /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"There is an error!!!");
                            setTimeout(function(){
                                window.location.reload();
                            },2000);*/
                        }                  
                    } 
                });
            });

        $("select.select2").select2({            
            delay: 2000,                    
            maximumSelectionLength: 1,                  
            ajax: {
                dataType: 'json',
                type: 'GET',
                url: '<?php echo AIR_API;?>getStudentName/access_token/'+access_token+'?channel_id='+channel_id,
                data: function (params) {
                    return {
                      term: params.term,
                      page: params.page || 1
                    };
                },
                processResults: function(data){
                    return {
                        results: data.results,
                        pagination: {
                            more: data.more
                        }                       
                    };
                }                   
            }
        });  

        $('#saveStudent').click(function(e){           
            var idtage = $("#tagorang").val()+ '';
            if (idtage == null || idtage == "") {
                        $('#modalAddStudent').modal('hide');
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#text_modal").html("Please select at least 1 name");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                    }
            else{
                $.ajax({
                    url : '<?php echo AIR_API;?>addStudentChannel/access_token/'+access_token,
                    type:"post",
                    data: {
                        channel_id: channel_id,
                        id_user: idtage
                    },
                    success: function(response){          
                        var code = response['code'];
                        if (code == 200) {
                            $('#modalAddStudent').modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#32c787');
                            $("#text_modal").html("Successfully add Student data");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                           /* notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully add Student data");
                            setTimeout(function(){
                                window.location.reload();
                            },2000);*/
                        }
                        else if (code == -400) {
                            window.location.href='<?php echo base_url();?>Admin/Logout';
                        }
                        else
                        {
                            $('#modalAddStudent').modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#ff33333');
                            $("#text_modal").html("There is an error!!!");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                            /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"There is an error!!!");
                            setTimeout(function(){
                                window.location.reload();
                            },2000);*/
                        }
                        
                    } 
                });
            }
        });

    });
</script>