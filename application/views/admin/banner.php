<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sideadmin'); ?>
		<?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
		?>
	</aside>

	<section id="content">
		<div class="container">            
			<div class="block-header">
				<h2>Banner</h2>				
			</div>      		

			<div class="card m-t-20 p-20" style="">				
                <div class="row" style="overflow-y: auto;">
                	
                	<div class="card-header">
						<div class="col-md-12">
							<div class="col-md-9 col-sm-9 col-xs-9">
								<h3 style="margin-top: -1%;">List Banner</h3>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-3">
								<a href="#modaladdBanner" data-toggle="modal" style="margin-top: -20px;" class="btn btn-primary pull-right"><i class="zmdi zmdi-plus"></i> Tambah Banner</a>
							</div>
						</div>
					</div>

                    <div class="col-md-12">
                        <div class="card-body card-padding" style="background-color:#EEEEEE;">                          
                            <div id="tables_listBanner" class="table-responsive"></div>                      
                        </div>
                    </div>
                </div> 
                                            
            </div>

		</section>
	</section>

	<div class="modal fade" id="modaladdBanner">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <!-- <div class="modal-header">		            	
	                <h3 class="modal-title pull-left" style="color: #000;"><b>Add User</b></h3>		                
	            </div> -->
	            <div class="modal-body">
		            <div class="job-postdetails">
						<div class="row">
							<fieldset>
								<div class="section postdetails">
									<h4>Tambah Banner<span class="pull-right"><button type="button" class="btn btn-link waves-effect" data-dismiss="modal">X</button></span></h4>
									<hr>										
									<div class="row form-group" style="margin-top: 5%;">
										<label class="col-sm-3 label-title">Banner<span class="required">*</span></label>
										<div class="col-sm-9">
											<input type="file" class="form-control" id="banner" required accept="image/x-png,image/gif,image/jpeg">
											<label style="color: red; font-size: 14px; display: none;" id="alertPhoto">Silahkan pilih banner!!!</label>
										</div>
									</div>
								</div><!-- postdetails -->																																		
								<div class="checkbox section agreement" style="margin-top: 3%;">										
									<button type="submit" class="btn btn-sm btn-primary btn-block" id="actionAddBanner">Simpan</button>
								</div><!-- section -->									
							</fieldset>								
						</div>
					</div>
	            </div>
	        </div>
	    </div>
	</div>

	<footer id="footer">
		<?php $this->load->view('inc/footer'); ?>
	</footer>
	<script type="text/javascript">

		var dataSet = [];

		$.ajax({
            url : "<?php echo BASE_URL();?>Rest/listBanner",
            type: "GET",
            success: function(response){   
            	var code = response['code'];            	
            	if (code == 200) {

            		for (var i = 0; i < response['data'].length; i++) {
            			var id 		= response['data'][i]['id'];
            			var banner 	= response['data'][i]['banner'];

            			var imgBanner = "<img src='"+banner+"' style='height:200px; width:auto;'/>";
            			var action 	= "<button class='btn btn-danger btnDeleteBanner' data-id='"+id+"'>Hapus</button>"
            			dataSet.push([i+1, imgBanner, action ]);
            		}
            		
            		$('#tables_listBanner').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="listBanner"></table>' );
             
                    $('#listBanner').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Banner"},
                            { "title": "Aksi" }
                        ]
                    });
            	}
            	else
                {
                    $('#tables_listBanner').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="listBanner"></table>' );
             
                    $('#listBanner').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Banner"},
                            { "title": "Aksi" }
                        ]
                    });
                }
            	
            }
        });

		$(document).on('click', '#actionAddBanner', function(){
			var file        = document.getElementById('banner').files[0];

			if(file == null){				
				$("#alertPhoto").css('display','block');
			}
			else
			{
				$("#alertPhoto").css('display','none');
				$("#modaladdBanner").modal("hide");
				var form        = new FormData();
	            form.append('image', file);
	            var filename    = file.name;
		        var filesize    = file.size;
				
				$.ajax({
	                url: '<?php echo BASE_URL();?>Rest/UploadImageUser',
	                cache: false,
	                contentType: false,
	                processData: false,
	                data: form,
	                type: "post",
	                success: function(response) {
	                    var code = response['code'];
	                    var link = response['data'];
	                    if (code == 200) {
	                    	notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Berhasil mengupload banner');
		                    setTimeout(function(){
		                    	window.location.reload();
		                    },2500);
	                    }
	                    else
	                	{
	                		notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Terjadi Kesalahan!!!");		
		                    setTimeout(function(){
		                    	window.location.reload();
		                    },2500);
	                	}
	                }
	            });

			}
		});

		$(document).on('click', '.btnDeleteBanner', function(){
			var id = $(this).data('id');
			
			var r = confirm("Apakah anda yakin ingin menghapus banner ini?");
            if (r == true) {
				$.ajax({
	                url :"<?php echo BASE_URL();?>Rest/deleteBanner",
	                type:"post",
	                data: {
	                    id: id
	                },
	                success: function(response){   
	                	var code = response['code'];
	                	if (code == 200) {
	                		notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Berhasil menghapus banner');
		                    setTimeout(function(){
		                    	window.location.reload();
		                    },2500);
	                	}
	                	else
	                	{
	                		notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Terjadi Kesalahan!!!");		
		                    setTimeout(function(){
		                    	window.location.reload();
		                    },2500);
	                	}
	                }
	            });
			}

		});

		$('table.display').DataTable( {
	        fixedHeader: {
	            header: true,
	            footer: true
	        }
	    } );
	    $('.data').DataTable(); 
	</script>