<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sideadmin'); ?>
		<?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
		?>
	</aside>

	<section id="content">
		<div class="container">            
			<div class="block-header">
				<h2><?php echo $this->lang->line('adminalltutor');?></h2>
			</div>      		

			<div class="card m-t-20 p-20" style="">

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12">
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Jenis kelamin</th>
                                        <th>Umur</th>
                                        <th>Kota asal</th>
                                        <th>No HP</th>
                                        <th>Referral Code</th>
                                        <th>Photo</th>    
                                        <th>Status</th>
                                        <th>Date register</th>
                                        <th>Actived Status</th>
                                        <th>Action</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
         // 							function UR_exists($url){
									//    $headers=get_headers($url);
									//    return stripos($headers[0],"200 OK")?true:false;
									// }
                                    foreach ($alldatatutor as $row => $v) { 
                                    	$idkabupaten 	= $v['id_kabupaten'];
                                    	$namakab 		= $this->db->query("SELECT kabupaten_name FROM master_kabupaten WHERE kabupaten_id='$idkabupaten'")->row_array()['kabupaten_name'];
                                    	$foto  			= $v['user_image'];
                                    	$image 			= CDN_URL.USER_IMAGE_CDN_URL.$v['user_image'];
                                    	$referralid		= $v['referral_id'];
                                    	$referralcode	= $this->db->query("SELECT referral_code FROM master_referral WHERE referral_id='$referralid'")->row_array()['referral_code'];
                                    	$codestatus 	= $v['status'];

                                    	$user_utc 			= "420";
                                    	$server_utcc        = $this->Rumus->getGMTOffset();
							            $intervall          = $user_utc - $server_utcc;
							            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$v['created_at']);
							            $tanggal->modify("+".$intervall ." minutes");
							            $tanggal            = $tanggal->format('Y-m-d H:i:s');
							            $datelimit          = date_create($tanggal);
							            $dateelimit         = date_format($datelimit, 'd/m/y');
							            $harilimit          = date_format($datelimit, 'd');
							            $hari               = date_format($datelimit, 'l');
							            $tahunlimit         = date_format($datelimit, 'Y');
							            $waktulimit         = date_format($datelimit, 'H:s');   
							            $datelimit          = $dateelimit;
							            $sepparatorlimit    = '/';
							            $partslimit         = explode($sepparatorlimit, $datelimit);
							            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                                    	if ($codestatus == 0) {
                                    		$status 	= "Menunggu verifikasi email";
                                    	}
                                    	else if($codestatus == 3){
                                    		$status 	= "Tutor belum mengisi data diri";
                                    	}
                                    	else if($codestatus == 2){
                                    		$status 	= "Menunggu persetujuan classmiles";
                                    	}
                                    	else if($codestatus == 1)
                                    	{
                                    		$status 	= "Tutor telah disetujui";
                                    	}
                                    	else if($codestatus == -1){
                                    		$status 	= "Tutor telah ditolak";
                                    	}
                                    	else{
                                    		$status 	= "Tutor terkena suspend";
                                    	}

                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['user_name']; ?></td>
                                            <td><?php echo $v['email']; ?></td>
                                            <td><?php echo $v['user_gender']; ?></td>
                                            <td><?php echo $v['user_age']; ?></td>
                                            <td><?php echo $namakab; ?></td>
                                            <td><?php echo "+".$v['user_countrycode'].$v['user_callnum']; ?></td>
                                            <td align="center"><?php if($referralcode==""){ echo "-"; }else{echo $referralcode;}?></td>
                                            <td>                                            	
                                    			<img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$foto; ?>" class="pv-main" style='width: 100px; height: 100px;' alt="">
                                            </td>
                                            <td><?php echo $status; ?></td>
                                            <td><?php echo $hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit; ?></td>
                                            <td>
                                            	<?php
                                            		if ($v['flag'] == 0) {
                                            			echo "<button disabled='true' class='btn btn-danger'>Not Active</button>";
                                            		}
                                            		else
                                            		{
                                            			echo "<button disabled='true' class='btn btn-success'>Active</button>";
                                            		}
                                            	?>
                                            </td>
                                            <td>
                                                <a data-toggle="modal" class="card profile-tutor" href="#descTutor" id="card_<?php echo $v['id_user']; ?>"><button class="btn btn-default" title="Detail tutor"><i class="zmdi zmdi-search"></i></button></a>
                                            </td>
                                            <td>
                                            	<a class="card active-tutor" idtutor="<?php echo $v['id_user']; ?>" flg="<?php echo $v['flag'];?>"><button class="btn btn-default" title="Active tutor"><i class="zmdi zmdi-settings"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div> 		

			<!-- Modal Default -->	
			<div class="modal fade" id="descTutor" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">														
							<div class="pull-left">
								<h3 class="modal-title">Profile Tutor</h3>
							</div>
							<div class="pull-right">
								<!-- <button disabled="disable" class="bgm-orange c-white" ><i class="zmdi zmdi-face-add"></i> <?php //echo $this->lang->line('followed'); ?></button> -->
								<!--<button class="btn bgm-deeporange"><i class="zmdi zmdi-face-add"></i>Unfollow</button>-->
								<button type="button" class="btn bgm-gray" data-dismiss="modal">X</button>
							</div>							
							<hr style="margin-top:6%;">
							
						</div>
						<div class="modal-body">
							<div class="col-sm-12">
								<div class="col-sm-4">
									<img id="modal_image" src="<?php echo base_url('aset/img/user/47.jpg') ?>" width="95%" height="180%" alt="">
								</div>
								<div class="col-sm-8" style="margin-top:-20px;">
									<!-- <h3><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('summary'); ?></h3>
									<label id="modal_self_desc"></label> -->
									<h4><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('basic_info'); ?></h4>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('full_name'); ?></dt>
										<dd id="modal_user_name"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('placeofbirth'); ?></dt>
										<dd id="modal_birthplace"></dd>
									</dl> 
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('birthday'); ?></dt>
										<dd id="modal_birthdate"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('age'); ?></dt>
										<dd id="modal_age"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('mobile_phone'); ?></dt>
										<dd id="modal_callnum"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('gender'); ?></dt>
										<dd id="modal_gender"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('religion'); ?></dt>
										<dd id="modal_religion"></dd>
									</dl>
								</div>
							</div>

							<div class="col-sm-12 table-responsive">
								<br><br>
								<h4><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('latbelpend'); ?></h4>
								<table class="table table-bordered table-hover">
									<thead>
										<th><?php echo $this->lang->line('educational_level'); ?></th>
										<th><?php echo $this->lang->line('educational_competence'); ?></th>
										<th><?php echo $this->lang->line('educational_institution'); ?></th>
										<th><?php echo $this->lang->line('institution_address'); ?></th>
										<th><?php echo $this->lang->line('graduation_year'); ?></th>
									</thead>
									<tbody id="modal_eduback">
										
									</tbody>
								</table>

								<br/>
								<h4><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('pengalaman_mengajar'); ?></h4>
								<table class="table table-bordered table-hover">
									<thead>
										<th><?php echo $this->lang->line('description_experience'); ?></th>
										<th colspan="2"><?php echo $this->lang->line('year_experience'); ?></th>
										<th><?php echo $this->lang->line('place_experience'); ?></th>
										<th><?php echo $this->lang->line('information_experience'); ?></th>
									</thead>
									<tbody id="modal_teachexp">
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tbody>
								</table>
							</div>
							<div class="col-md-12">
								<h4><i class="zmdi zmdi-comment m-r-5"></i> Kesimpulan anda :</h4>								
								<textarea class="form-control" disabled rows="6" name="conclusion" id="conclusion" placeholder="isi disini" style="overflow-y: auto; font-size: 13px;"></textarea>
							</div>
						</div>
						<br>  
						<div class="modal-footer m-r-30" >
							
						</div>    				
					</div>
				</div>
			</div>

			<div class="modal fade" id="activeTutor" tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 15%;">
	            <div class="modal-dialog">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <h3 class="modal-title pull-left" style="color: #000;"><b>Change Status</b></h3>
	                    </div>
	                    <div class="modal-body" id="body_status">
	                    	<center>
	                    		<div class="row col-md-12" style="margin-top: 5%;">
	                    			<div class="col-md-6">
	                    				<button type="button" class="btn btn-lg btn-success waves-effect" id="activate" iduser="" flag="1">ACTIVATE</button>
	                    			</div>
	                    			<div class="col-md-6">
	                    				<button type="button" class="btn btn-lg btn-danger waves-effect" id="inactive" iduser="" flag="0">NOT ACTIVATE</button>
	                    			</div>
	                    		</div>
	                    	</center> 
	                    </div>
	                    <div class="modal-footer" style="background-color: #e5e5e5;">
	                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
	                    </div>
	                </div>
	            </div>
	        </div>

		</section>
	</section>

	<footer id="footer">
		<?php $this->load->view('inc/footer'); ?>
	</footer>
	<script type="text/javascript">
		$('.card.profile-tutor').click(function(){
			var ids = $(this).attr('id');
			ids = ids.substr(5,10);
			$.get('<?php echo base_url(); ?>first/getTutorProfile/'+ids,function(hasil){
				hasil = JSON.parse(hasil);
				$('.modal_approve').attr('id',ids);
				$('.modal_decline').attr('id',ids);
				$('#modal_image').attr('src','<?php echo base_url(); ?>aset/img/user/'+hasil['user_image']);
				$('#modal_self_desc').html(hasil['self_description']);
				$('#modal_user_name').html(hasil['user_name']);
				$('#modal_birthplace').html(hasil['user_birthplace']);
				$('#modal_birthdate').html(hasil['user_birthdate']);
				$('#modal_age').html(hasil['user_age']);
				$('#modal_callnum').html(hasil['user_callnum']);
				$('#modal_gender').html(hasil['user_gender']);
				$('#modal_religion').html(hasil['user_religion']);
				$('#conclusion').html(hasil['conclusion']);

				$('#modal_eduback').html('');
				$('#modal_teachexp').html('');
				if(hasil['education_background'] != ''){
					for (var i = 0; i<hasil['education_background'].length; i++) {
						$('#modal_eduback').append('<tr>'+
							'<td>'+hasil['education_background'][i]['educational_level']+'</td>'+
							'<td>'+hasil['education_background'][i]['educational_competence']+'</td>'+
							'<td>'+hasil['education_background'][i]['educational_institution']+'</td>'+
							'<td>'+hasil['education_background'][i]['institution_address']+'</td>'+
							'<td>'+hasil['education_background'][i]['graduation_year']+'</td>'+
							'</tr>');
					}
				}
				if(hasil['teaching_experience'] != ''){
					for (var i = 0; i<hasil['teaching_experience'].length; i++) {
						$('#modal_teachexp').append('<tr>'+
							'<td>'+hasil['teaching_experience'][i]['description_experience']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['year_experience1']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['year_experience2']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['place_experience']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['information_experience']+'</td>'+
							// '<td>'+hasil['teaching_experience'][i]['graduation_year']+'</td>'+
							'</tr>');
					}
				}
				$('#modalDefault').modal('show');
			});

			$('.modal_approve').click(function(){
				$('.modal_approve').css('disabled','true');
				var newids = $(this).attr('id');
				var conclusion = $("#conclusion").val();
				$.ajax({
		            url: '<?php echo base_url(); ?>first/approveTheTutor',
		            type: 'GET',   
		            data: {
		                id_user: newids,
		                text: conclusion
		            },             
		            success: function(data)
		            { 
		            	$("#descTutor").modal("hide");
		            	notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Approve Tutor success");
                        setTimeout(function(){
                            window.location.reload();
                        },3000);
		            }
	        	});
				// $.get('<?php echo base_url(); ?>first/approveTheTutor?id_user='+newids+'&text='+conclusion,function(s){
					
				// });
			});
			$('.modal_decline').click(function(){
				$('.modal_decline').css('disabled','true');
				var newids = $(this).attr('id');
				var conclusion = $("#conclusion").val();
				$.ajax({
		            url: '<?php echo base_url(); ?>first/declineTheTutor',
		            type: 'GET',   
		            data: {
		                id_user: newids,
		                text: conclusion
		            },             
		            success: function(data)
		            { 
		            	$("#descTutor").modal("hide");
		            	notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Reject Tutor success");
                        setTimeout(function(){
                            window.location.reload();
                        },3000);
		            }
	        	});
			});			
		});

		$(document).on('click', '.active-tutor', function(){
			var idtutor = $(this).attr('idtutor');
			var flag = $(this).attr('flg');
			var message = "";
			if (flag == 0) {
				$("#activate").removeAttr('disabled');
				$("#inactive").attr('disabled','true');
				$("#activate").attr('iduser',idtutor);				
			}
			else
			{
				$("#inactive").removeAttr('disabled');
				$("#activate").attr('disabled','true');
				$("#inactive").attr('iduser',idtutor);				
			}			
			$("#activeTutor").modal("show");
			

		});

		$(document).on('click', '#activate', function(){
			var idtutor = $(this).attr('iduser');
			var flag = $(this).attr('flag');
			$.ajax({
                url :"<?php echo BASE_URL();?>Rest/actInTutorFromAdmin",
                type:"post",
                data: {
                    tutor_id: idtutor,
                    flag: flag
                },
                success: function(response){   
                	var code = response['code'];
                	var message = response['message'];
                	if (code == 200) {
                		notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',message);
	                    $('#modalDeleteTutor').modal('hide');
	                    setTimeout(function(){
	                    	window.location.reload();
	                    },2500);
                	}
                	else
                	{
                		notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Terjadi Kesalahan!!!");
	                    $('#activeTutor').modal('hide');
	                    setTimeout(function(){
	                    	window.location.reload();
	                    },2500);
                	}
                }
            });
		});

		$(document).on('click', '#inactive', function(){
			var idtutor = $(this).attr('iduser');
			var flag = $(this).attr('flag');
			$.ajax({
                url :"<?php echo BASE_URL();?>Rest/actInTutorFromAdmin",
                type:"post",
                data: {
                    tutor_id: idtutor,
                    flag: flag
                },
                success: function(response){   
                	var code = response['code'];
                	if (code == 200) {
                		notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',message);
	                    $('#modalDeleteTutor').modal('hide');
	                    setTimeout(function(){
	                    	window.location.reload();
	                    },2500);
                	}
                	else
                	{
                		notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Terjadi Kesalahan!!!");
	                    $('#activeTutor').modal('hide');
	                    setTimeout(function(){
	                    	window.location.reload();
	                    },2500);
                	}
                }
            });
		});

		$('table.display').DataTable( {
	        fixedHeader: {
	            header: true,
	            footer: true
	        }
	    } );
	    $('.data').DataTable(); 
	</script>