<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sideadmin'); ?>
	</aside>

	<section id="content">
		<div class="container">            
			<div class="block-header">
				<h2>Evaluator</h2>
			</div>

			<div class="card m-t-20 p-20" style="">
                <div class="row" style="margin-top: 2%; overflow-y: auto;">                
                    <div class="col-md-12" >
                    	<div class="card-header">
                    		<h2><a href="<?php echo base_url();?>admin/Evaluator"><button class="btn btn-danger"><i class="zmdi zmdi-arrow-left"></i> Kembali</button></a></h2>
                    	</div>
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                       <th>No</th>
                                        <th>Pelajaran</th>
                                        <th>Tingkatan Sekolah</th>
                                        <th>Tingkatan Kelas</th>
                                        <th>Status</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    if (!isset($_GET['id'])) {
                                    	echo $_GET['id'];
                                    }
                                    else
                                    {
	                                    $tutor_id 	= $_GET['id'];
								        $getBooking = $this->db->query("SELECT tb.*, ms.subject_name, mj.jenjang_name, mj.jenjang_level FROM tbl_booking as tb INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id) ON tb.subject_id=ms.subject_id WHERE tb.id_user='$tutor_id' AND ( tb.status='verified' OR tb.status='rejected' ) ORDER BY datetime DESC")->result_array();
								        if (empty($getBooking)) {
								        	?>
								        	<tr>
								        		<td colspan="5"><center>Tidak ada data</center></td>
								        	</tr>
								        	<?php
								        }
	                                    foreach ($getBooking as $row => $v) { 
	                                    	$subject_name 	= $v['subject_name'];
	                                    	$jenjang_name  	= $v['jenjang_name'];
	                                    	$jenjang_level  = $v['jenjang_level'];
	                                    	if ($v['status']=='verified') {
	                                    		$status = 'Aktif';
	                                    	}
	                                    	else if($v['status']=='rejected')
	                                    	{
	                                    		$status = 'Tidak aktif';
	                                    	}
	                                        ?>
	                                        <tr>
	                                            <td><?php echo($no); ?></td>
	                                            <td><?php echo $subject_name; ?></td>
	                                            <td><?php echo $jenjang_name; ?></td>
	                                            <td><?php echo $jenjang_level; ?></td>
	                                            <td><?php echo $v['status'];?></td>                                            
	                                            <td>
	                                            	<center>
	                                            	<?php if($status == 'Aktif'){?>
	                                                	<a class="card profile-tutor" uid="<?php echo $v['uid']; ?>" action='reject'><button class="btn btn-danger" title="Detail tutor"> Reject</button></a>
	                                                <?php 
	                                            	} else{
	                                            	?>
	                                                	<a class="card profile-tutor" uid="<?php echo $v['uid']; ?>" action='active'><button class="btn btn-success" title="Detail tutor"> Active</button></a>
	                                                	</center>
	                                                <?php
	                                            	}
	                                            	?>
	                                            </td>
	                                        </tr>
	                                        <?php 
	                                        $no++;
	                                    }
                                	}
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div>
            </div> 		
			
		</div>
	</section>

</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	$('.card.profile-tutor').click(function(){
		var ids 	= $(this).attr('uid');
		var action 	= $(this).attr('action');
		if (action == 'reject') {
			$.ajax({
				url: '<?php echo base_url();?>Admin/reject_vts',
				data: {
	                booking_id: ids
	            },            
				type: 'GET',
				success: function(response){					
					response = JSON.parse(response);
					if(response['status']){
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Reject");
						setTimeout(function(){
	                        window.location.reload();
	                    },1500);
					}
					else
					{
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Failed Reject");
						setTimeout(function(){
	                        window.location.reload();
	                    },1500);	
					}
				}
			});
		}
		else
		{
			if (action == 'active') {
			$.ajax({
				url: '<?php echo base_url();?>Admin/approve_vts',
				data: {
	                booking_id: ids
	            },            
				type: 'GET',
				success: function(response){					
					response = JSON.parse(response);
					if(response['status']){
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Approve");
						setTimeout(function(){
	                        window.location.reload();
	                    },1500);
					}
					else
					{
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Failed Approve");
						setTimeout(function(){
	                        window.location.reload();
	                    },1500);	
					}
				}
			});
		}
		}
	});

	$('table.display').DataTable({
        fixedHeader: {
            header: true,
            footer: true
        }
    });

    $('.data').DataTable(); 

</script>