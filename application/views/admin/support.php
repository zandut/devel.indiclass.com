<style type="text/css">
    .loading {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        background: rgba(50, 50, 50, 0.7);
        z-index: 10;
        display: none;
    }
    .gif{
        top: 150px;
        margin-top: 170px;
        z-index: 999;
    }


</style>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

     ?>
</header>
<div class="loading">
    <center>
    <img class="gif" src="<?php echo base_url();?>aset/img/clock-loading.gif" width="400px">
    </center>
</div>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/sideadmin'); ?>
        <?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
        ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="block-header">
                <h2>Support</h2>

                <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>admin"><?php echo $this->lang->line('homeadminn'); ?></a></li>
                            <li class="active">Support </li>
                            <li id="checksupport"><?php echo $this->session->userdata('checksupport');?> </li>
                        </ol>
                    </li>
                </ul>
            </div> <!-- akhir block header    --> 

            <div class="card col-md-12" style="margin-top: 4%;">
                <div class="card-header">
                    <h2>List Tutor Support</h2>
                </div>

                <div class="card-body card-padding">
                    <div class="row">

                        <div class="col-md-12 table-responsive">
                            <table class="display table table-striped table-bordered data">
                                <thead>
                                    <tr>   
                                        <th>No</th>                              
                                        <th hidden>Request id</th>
                                        <th hidden>tutor id</th>
                                        <th>Tutor request</th>
                                        <th>Type</th>
                                        <th>Time</th>                                                                            
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                
                                <tbody> 
                                    <?php
                                        $no = 1;                                        
                                        // $query2 = $this->db->query("SELECT lm.payment_type, lm.status_message, lo.* FROM log_midtrans_transaction as lm INNER JOIN log_order as lo ON lm.order_id=lo.order_id WHERE lo.id_user='$user' ORDER BY lo.timestamp DESC")->result_array();
                                        $query2 = $this->db->query("SELECT tu.user_name, ts.* FROM tbl_support as ts INNER JOIN tbl_user as tu ON ts.id_tutor=tu.id_user WHERE ts.status!='-1' ORDER BY timestamp DESC")->result_array();
                                        if (empty($query2)) {
                                            ?>
                                        <tr>
                                            <td colspan='8'><center>Tidak ada request support</center></td>
                                        </tr>
                                        <?php
                                        }
                                        else
                                        {     
                                        foreach ($query2 as $row => $v) {                                            
                                          
                                            $date = date_create($v['timestamp']);
                                            $datee = date_format($date, 'd/m/y');
                                            $hari = date_format($date, 'd');
                                            $tahun = date_format($date, 'Y');
                                            $waktu = date_format($date, 'H:i:s');
                                                                                    
                                            $date = $datee;
                                            $sepparator = '/';
                                            $parts = explode($sepparator, $date);
                                            $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));
                                    ?>                               
                                    <tr>       
                                        <td><?php echo($no); ?></td>
                                        <td hidden id="idrequestt"><?php echo($v['requestid']);?></td>
                                        <td hidden id="idttrr"><?php echo($v['id_tutor']);?></td>                         
                                        <th><?php echo($v['user_name']); ?></th>
                                        <th><?php echo($v['type']); ?></th>
                                        <th><?php echo $hari.' '. $bulan. ' '.$tahun.' '.$waktu ?></th>
                                        <td>
                                            <?php 
                                                if($v['status'] == '0'){
                                            ?>
                                                <!-- <a href="<?php echo base_url(); ?>approve_support?idrequest=<?php echo($v['requestid']);?>&idttr=<?php echo($v['id_tutor']); ?>"> -->
                                                    <button class="apr_req btn btn-success m-l-5 btn-sm" requestidd="<?php echo($v['requestid']);?>" idttrr="<?php echo($v['id_tutor']); ?>">Call Tutor</button>
                                                    
                                                <!-- </a> -->
                                            <?php
                                                }
                                                else
                                                {
                                            ?>
                                                <!-- <a href="<?php echo base_url(); ?>admin/ApproveSupport"> -->
                                                    <button class="btn btn-danger m-l-5 btn-sm">Passed</button>
                                                <!-- </a> -->
                                            <?php
                                                }
                                            ?>                                           
                                        </td>
                                    </tr>
                                    <?php
                                        $no++;
                                        }                                         
                                    } 
                                    ?>                                                                  
                                </tbody>
                                
                            </table>                            
                        </div>                        

                    </div>
                </div>
            </div>

        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<!-- <div class="page-loader bgm-blue">
    <div class="preloader pls-white pl-xxl">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p class="c-white f-14">Please wait...</p>
    </div>
</div> -->



<script type="text/javascript">
    $(document).ready(function() {

        $('table.display').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        
        $('.data').DataTable();
        
        $('.apr_req').click(function(e){            
            var idrequest = $(".apr_req").attr('requestidd');
            $.ajax({                                
                url: '<?php echo base_url(); ?>admin/approve_support',
                type: 'POST', // performing a POST request
                data : {
                    idr: idrequest,
                },                  
                success: function(data){                    
                    // window.location.reload();
                    data = JSON.parse(data);
                    var times = 0;
                    if (data['status'] == true) {
                        $(".loading").css('display','block');
                        // $('.loading').fadeIn('fast').delay(15000).fadeOut('fast');
                        $('.loading').fadeIn('fast');
                        var timer1 = setInterval(function() {
                            $.ajax({
                                url: '<?php echo base_url(); ?>Admin/checked?idr='+idrequest, 
                                success: function(data) {
                                    // alert('a');
                                    data = JSON.parse(data);
                                    if(data['status'] == 1){
                                        clearInterval(timer1);                                     
                                        window.location.replace(data['link']);
                                    }else{

                                        times++;
                                        if(times >=60){
                                            clearInterval(timer1);
                                            // alert('error');
                                            $('.loading').fadeOut('fast');
                                            notify('top','center','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
                                            $.ajax({
                                                url: '<?php echo base_url(); ?>Admin/sendaction?idr='+idrequest,
                                                success: function(data) {
                                                    data = JSON.parse(data);
                                                    if(data['status'] == 1){
                                                        window.location.reload();
                                                    }
                                                    else
                                                    {
                                                        window.location.reload();
                                                    }
                                                }                                                
                                            });
                                            
                                        }
                                        // window.location.replace("https://classmiles.com/admin/support");
                                    }
                                }                            
                              });
                        }, 1000);
                        // $(".loading").css('display','block');
                        
                    }
                } 
            });            
        });

        /*setTimeout(function(){
           window.location.reload(1);
        }, 15000);*/
        // var checksupport = "<?php echo $this->session->userdata('checksupport'); ?>";        
        // if (checksupport == "penuh") {                        
        //     (function worker() {
        //       $.ajax({
        //         url: '<?php echo base_url(); ?>Admin/checked', 
        //         success: function(data) {
        //             // alert('a');
        //             data = JSON.parse(data);
        //             if(data['status'] == 1){
        //                 // window.location.replace("https://classmiles.com/admin/support");
        //             }else{
        //                 window.location.replace("https://facebook.com/");
        //             }
        //         },
        //         complete: function() {
        //           // Schedule the next request when the current one's complete
        //           setTimeout(worker, 3000);
        //         }
        //       });
        //     })();

        //     $(".loading").css('display','block');
        //     $('.loading').fadeIn('fast').delay(15000).fadeOut('fast');
        // }


        // $("#loader").show();
        // $(".loading").fadeOut("slow");
            
    } );
</script>
    