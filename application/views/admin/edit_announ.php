<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

     ?>
</header>

<section id="main">

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2><?php echo $this->lang->line('accountlist'); ?></h2>
                <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul>
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card m-t-20 p-20" style="">
                <div class="card-header">
                    <div class="pull-left"><h2>Edit Announcement</h2></div>
                    <div class="pull-right"><a href="<?php echo base_url();?>admin/Announcement"><button class="btn btn-danger"><i class="zmdi zmdi-arrow-right"></i> Batal</button></a></div>
                <br><br>
                <hr>

                <div class="row" style="margin-top: 5%;">
                <?php
                    $idannoun = $_GET['id'];                    
                    $getannoun = $this->db->query("SELECT * FROM tbl_announcement WHERE id='$idannoun'")->result_array();
                    if (empty($getannoun)) {
                        ?>
                    <center>Tidak ada Data</center>
                    <?php
                    }
                    else
                    {                        
                        foreach ($getannoun as $row => $v) {                            
                        ?>
                    <div class="col-md-2"></div>                        
                    <div class="col-md-8" >
                        <div class="card">
                            <form method="post" action="<?php echo base_url('Process/edittext_announ'); ?>" enctype="multipart/form-data">
                            <div class="row p-15">    
                                
                                <input type="text" name="id_announ" id="id_announ" class="c-black" value="<?php echo $_GET['id']; ?>" hidden/>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Text</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">                                                
                                                <textarea class="form-control fg-input input-sm" rows="6" name="text_announ" id="text_announ" style="border: 1px solid #BDBDBD; padding: 3px;"><?php echo $v['text_announ']; ?></textarea>      
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>                                
                                <div class="col-md-12 m-t-20">
                                    <button class="btn btn-success btn-block">Simpan</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <?php 
                        }                       
                    }
                    ?>                                 

                </div> 
                                            
            </div> 

            
            <!-- Modal EDIT -->  
            <div class="modal fade" id="modalEdit" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Edit Akun Rekening</h4>
                            <hr>
                        </div>                        
                        <div class="modal-body">
                            <div class="card">
                                <div class="row p-15">   
                                    <input type="text" name="id_rekening" id="id_rekening" class="c-black" value=""/>                                     
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nama Akun</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group fg-float" style="margin-left: 4%;">                       
                                                <div class="fg-line c-gray">
                                                    <input type="text" name="editnamaakun" id="editnamaakun" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nomer Rekening</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group fg-float" style="margin-left: 4%;">                       
                                                <div class="fg-line c-gray">
                                                    <input type="text" name="editnorekening" id="editnorekening" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nama Bank</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group fg-float" style="margin-left: 4%;">                       
                                                <div class="fg-line c-gray">
                                                    <div class="select">
                                                        <select required name="editnamabank" id="editnamabank" class="selectpicker" data-live-search="true" style="z-index: 10;">
                                                        <?php
                                                           $allbank = $this->db->query("SELECT * FROM master_bank")->result_array();
                                                           foreach ($allbank as $row => $v) {
                                                                echo '<option value="'.$v['bank_id'].'">'.$v['bank_name'].'</option>';
                                                            }
                                                        ?>                                                    
                                                        </select>
                                                    </div>
   
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Cabang</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group fg-float" style="margin-left: 4%;">                       
                                                <div class="fg-line c-gray">
                                                    <input type="text" name="editcabangbank" id="editcabangbank" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn bgm-green" id="editdatarekening">Simpan</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>

        </div>
    </section>

</section>
<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){
        $('#simpandatarekening').click(function(e){
            var namaakun = $("#namaakun").val();
            var norekening = $("#norekening").val();
            var namabank = $("#namabank").val();
            var cabangbank = $("#cabangbank").val();
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/addrekening",
                type:"post",
                data: "namaakun="+namaakun+"&norekening="+norekening+"&namabank="+namabank+"&cabangbank="+cabangbank,
                success: function(html){             
                    alert("Berhasil Menambahkan akun rekening bank");
                    $('#modalTambah').modal('hide');
                    location.reload();                     
                } 
            });
         });

        $('#hapusdatarekening').click(function(e){
            var id_rekening = $("#id_rekeningg").val();
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deleterekening",
                type:"post",
                data: "id_rekening="+id_rekening,
                success: function(html){             
                    alert("Berhasil menghapus data rekening");
                    $('#modalDelete').modal('hide');
                    location.reload();                     
                } 
            });
        });
    });
</script>
