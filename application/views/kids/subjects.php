<style type="text/css">
    .bp-header:hover
    {
        cursor: pointer;
    }
    .ci-avatar img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .ci-avatar:hover img {
        opacity: 0.6;
    }
</style>
<div ng-show="loding" class="page-loader" style="background-color:black; opacity: 0.5;">
    <div class="preloader pl-lg pls-white">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p style="margin-left: -18px;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
    </div>
</div>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); 
            $id_user = $this->session->userdata('id_user');
        ?>
    </aside>
    <section id="content">

        <div class="container">

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="tn-justified f-16 row" style="background-color: white;" id="choose_child">
				<div class="lv-header hidden-xs" style="background-color: white;">
					<div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;"><?php echo $this->lang->line('lihatpelajarankids'); ?></div>
					<small class="text-lowercase" id="chooseanak">( <?php echo $this->lang->line('pilihanak'); ?> )</small>					
        		</div><br>

				<div class="card-body card-padding p-l-20 p-r-20">
					<div class="contacts clearfix row">
						<?php 
							$parentid 	= $this->session->userdata('id_user');
							$allchild 	= $this->db->query("SELECT tu.*, tpk.* FROM tbl_profile_kid as tpk INNER JOIN tbl_user as tu ON tpk.kid_id=tu.id_user WHERE tpk.parent_id='$parentid'")->result_array();
							foreach ($allchild as $key => $child) {					
							?>
		                        <div class="col-md-3 kotakchild" id="ktk_<?php echo $key;?>" data-id="<?php echo $child['id_user'];?>" data-jenjang="<?php echo $child['jenjang_id'];?>" data-name="<?php echo $child['user_name'];?>">
		                            <div class="c-item kotakchild">
		                                <a href="" class="ci-avatar kotakchild">
		                                    <img src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$child['user_image'];?>" alt="">
		                                </a>        
		                                <div class="c-info kotakchild">
		                                    <strong><?php echo $child['user_name'];?></strong>
		                                    <small><?php echo $child['user_gender'].', '.$child['user_birthplace'].$child['user_birthdate'] ?></small><br>
		                                </div>
		                            </div>
		                        </div>
                        	<?php
                    		}
                    	?>
					</div>
				</div>
			</div>

			<div id="hiddensubject" style="display: none;">
				<div class="tn-justified f-16 row text-center m-l-5" style="background-color: white; width: 98%;">
					<div class="lv-header hidden-xs" style="background-color: white;">
						<div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;"><?php echo $this->lang->line('lihatpelajarankids'); ?></div>
						<small class="text-lowercase" id="levelregister">( <?php echo $this->lang->line('pilihpelajaran'); ?> )</small>
	        		</div>
        		</div>

	            <div class="block-header" style=" margin-top: 3%;">
	                <h2><?php echo $this->lang->line('title_pg_subject'); ?></h2>
	                <ul class="actions hidden-xs">
	                    <li>
	                        <ol class="breadcrumb">
                                <li><a class="f-16 c-bluegray" style="margin-top: -3%; cursor: default;" href="#"><?php echo $this->lang->line('kidsselected'); ?> : <label id="namadipilih"></label> . <label class="c-orange" style="cursor: pointer;" id="chooseother"><?php echo $this->lang->line('pilihyanglain'); ?></label></a></li>
                            </ol>
	                    </li>
	                </ul>
	            </div>
	            
	            <div class="alert alert-info text-center" id="nosubjectchild" style="display: none;"><?php echo $this->lang->line('no_subjectskids'); ?>
	            </div>

	            <div class="card" id="notutorchild" style="display: none;">
	                <div class="lv-header-alt clearfix m-b-5">
	                    <h2 class="lvh-label f-16">subject_name</h2>	                           
	                    <div class="lvh-search">
	                        <input type="text" placeholder="Search typing..." class="lvhs-input">
	                        
	                        <i class="lvh-search-close">&times;</i>
	                    </div>
	                </div>                        
	                
	                <div class="card-body card-padding">
	                	<center>
	        				<div class=""><?php echo $this->lang->line('no_tutor_here').' subject_name'; ?></div>
	        			</center>
	        		</div>
	        	</div>
	            
	            <div class="card" style="display: none;" id="listtutorchild">
	                
	                
	            </div>

            </div>

        </div>  

        <!-- Modal Default -->  
        <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style=" background: #f7f7f7;">
                    <div class="modal-header bgm-teal">                                                     
                        <div class="pull-left">
                            <h3 class="modal-title c-white"><?php echo $this->lang->line('profil'); ?></h3>
                            
                        </div>
                        <div class="pull-right">                                        
                            <button type="button" class="btn bgm-white" data-dismiss="modal">X</button>
                        </div>
                    </div>

                    <div class="modal-body" style="margin-top:30px;">
                        <div class="col-sm-12">

                            <div class="col-sm-4">
                                <img class="z-depth-3-bottom" id="modal_image" width="100%" height="100%" alt=""><br><br>
                                <div class="card" style="padding: 10px;">
                                    <label style="font-size:15px;"><?php echo $this->lang->line('tutorname');?></label>
                                    <hr style="margin-top: -1px;">
                                    <p style="font-size:13px; margin-top: 5px; overflow:hidden; word-wrap: break-word;" id="modal_user_name"></p>                                                                                       
                                </div>
                                <div class="card" style="padding: 10px; margin-top: -15px;">
                                    <label style="font-size:15px;"><?php echo $this->lang->line('gender');?></label>
                                    <hr style="margin-top: -1px;">
                                    <label style="font-size:13px; margin-top: -5px;"  id="modal_gender"></label>
                                </div>
                            </div>

                            <div class="col-sm-8" style="margin-top:0px;">                                              
                                <div class="col-sm-12">
                                    <div class="card" style="padding: 10px; margin-top: 10px;">
                                        <label style="font-size:16px;"><?php echo $this->lang->line('description_tutor');?></label>
                                        <hr style="margin-top: -1px;">
                                        <label id="modal_self_desc" style="width:100%; text-align:justify;">
                                        </label><br><br>
                                    </div>
                                </div>
                                <div class="col-lg-12 m-l-25">
                                    <div class="col-sm-1" style="margin-left:-20px;">
                                        <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/topi.png" width="30px" height="30px" alt="">
                                    </div>
                                    <div id="modal_eduback" class="col-sm-5 m-t-5">                                                 
                                    </div>
                                    <!-- <div class="col-sm-1" style="margin-left:-30px;">
                                        <img src="<?php echo base_url('aset/img/icons/waktu.png').'?'.time() ?>" width="27px" height="27px" alt="">
                                    </div>
                                    <div id="modal_teachexp" class="col-sm-6 m-t-5">
                                    </div> -->
                                </div>                                                                                  

                                <div class="col-lg-12 m-t-10">
                                    <div class="col-sm-12 m-t-5">
                                        <div class="table-responsive">      
                                        <center>
                                            <table style="height:10px;" class="table">
                                                <thead>
                                                    <center>    
                                                        <th class="bgm-teal c-white">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $this->lang->line('competency');?></th>
                                                    <th class="bgm-teal c-white"></th>                                                              
                                                    </center>
                                                </thead>
                                                <tbody id="modal_competency" style="height: 10px;">                                                 
                                                </tbody>
                                            </table>
                                        </center>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- AKHIR col-sm-8 -->

                        </div><!-- AKHIR col-sm-12 -->
                    </div><!-- AKHIR modal-body -->

                    <div class="modal-footer m-r-30" >
                    </div>                  
                    <br><br><br>

                </div><!-- AKHIR modal-content -->
            </div><!-- AKHIR modal-dialog -->
        </div>
        <!-- AKHIR modal fade -->

    </section>
</section>
                
<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>                

<script type="text/javascript">
    
    var child_id = null;
    $('.kotakchild.col-md-3').click(function(){    	
    	$("#listtutorchild").empty();
    	var ktk 	= $(this).attr('id');    	
		child_id 	= $(this).data('id');		
		jenjang_id 	= $(this).data('jenjang');
		var username= $(this).data('name');
		angular.element(document.getElementById(ktk)).scope().loadings();    	
	
		$("#jenjangchild").text(jenjang_id);
		$("#choose_child").css('display','none');
		$("#hiddensubject").css('display','block');
		$("#namadipilih").text(username);				
		$.ajax({
			url: '<?php echo base_url();?>Master/listSubjectChild',
			type: 'GET',
			data: {
				child_id: child_id,
				jenjang_id: jenjang_id
			},
			success: function(response)
			{									
				result = JSON.parse(response);
				data = JSON.stringify(response);
				console.warn(data);
				result_code = result['code'];							
				if (result_code == 1041) {
					$("#notutorchild").css('display','none');
					$("#listtutorchild").css('display','none');
					$("#nosubjectchild").css('display','block');				
				}
				else if(result_code == 1042)
				{
					$("#nosubjectchild").css('display','none');
					$("#listtutorchild").css('display','none');
					$("#notutorchild").css('display','block');
				}	
				else
				{
					for (var i = result.data.length-1; i >=0;i--) {
						var subject_id 	= result['data'][i]['subject_id'];
						var subject_name= result['data'][i]['subject_name'];
						var iconsub 	= result['data'][i]['icon'];
						var cdnicon 	= "<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>class_icon/"+iconsub;
						var langbahasa	= "<?php echo $this->session->userdata('lang');?>";								
						var bahasa 		= null;
						if (langbahasa=='english') {
							bahasa 		= result['data'][i]['english'];
						}
						else{
							bahasa 		= result['data'][i]['indonesia'];	
						}
						var enter 		= "<br><br>";

						// console.warn('cdnicon : '+cdnicon);
						var boxlistsubject = "<div class='lv-header-alt clearfix m-b-5' style='margin-bottom:3%;'><h2 class='lvh-label f-16'><img class='m-r-5' src='"+cdnicon+"' style='height: 25px; width: 25px;'>"+bahasa+"</h2></div>";
						var boxtutorsubject = "<div class='card-body card-padding'><div class='contacts clearfix row' id='kotaklisttutorrr'>";
												
						var tutor_list = result['data'][i]['tutors'];									

						for (var iai = 0; iai < tutor_list.length ;iai++) {										
							var tutor_id 	= tutor_list[iai]['tutor_id'];
							var tutor_name 	= tutor_list[iai]['tutor_name'];
							var tutor_image = tutor_list[iai]['tutor_image'];
							var tutor_country 	= tutor_list[iai]['tutor_country'];
							var following 	= tutor_list[iai]['following'];
							var status_tutor 	= tutor_list[iai]['status_tutor'];
							var kotak_ver = "";
							if (status_tutor=='verified') {
								kotak_ver = "<div class='ribbon'><span>Verified</span></div>";
							}
							else
							{
								kotak_ver = "";
							}
							var imagetutor 	= "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+tutor_image;
							var buttonfollow = null;
							if (following==0) {
								buttonfollow = "<div class='c-footer'><button id='buttonfollow' subject_id='"+subject_id+"' tutorid='"+tutor_id+"' subject_namee='"+subject_name+"' class='waves-effect bgm-green c-white btn btn-success btn-block follow_ajax f-12'><?php echo $this->lang->line('follow'); ?></button></div>";
							}
							else
							{
								buttonfollow = "<div class='c-footer'><button id='buttonunfollow' subject_id='"+subject_id+"' tutorid='"+tutor_id+"' subject_name='"+subject_name+"' class='waves-effect bgm-red c-white btn btn-danger btn-block unfollow_ajax f-12'><?php echo $this->lang->line('unfollow'); ?></button></div>";
							}
							var kotaklisttutor = "<div class='col-md-2 col-sm-4 col-xs-6'><div class='c-item'>"+kotak_ver+"<a href='#' class='ci-avatar showModal' id='card_"+tutor_id+"'><img src='"+imagetutor+"' style='height: 22vh; width: 100%;' alt=''></a><div class='c-info'><strong>"+tutor_name+"</strong></div>"+buttonfollow+"</div></div>";

							boxtutorsubject += kotaklisttutor;
						}
						$("#listtutorchild").append(boxlistsubject+boxtutorsubject+"</div>"+"</div>");
					}
					angular.element(document.getElementById(ktk)).scope().loadingsclose();
					$("#nosubjectchild").css('display','none');
					$("#notutorchild").css('display','none');
					$("#listtutorchild").css('display','block');
				}					
				$("#choose_child").css('display','none');
				$("#hiddensubject").css('display','block');
				$("#namadipilih").text(username);				
			}
		});					
	});

	$("#chooseother").click(function(){					
		$("#hiddensubject").css('display','none');
		$("#namadipilih").text("");
		$("#choose_child").css('display','block');
		child_id = null;
	});

    $(document).on('click', ".unfollow_ajax", function(e){
		e.preventDefault();     
    	var id_user 	= child_id;
    	var subject_id 	= $(this).attr('subject_id');
    	var tutor_id 	= $(this).attr('tutorid');
    	var subject_name= $(this).attr('subject_name');
    	var that 		= $(this);
    	$.ajax({
			url: '<?php echo base_url();?>Master/unsavebooking',
			type: 'GET',
			data: {
				id_user: id_user,
				tutor_id: tutor_id,
				subject_id: subject_id,
				subject_name: subject_name
			},
			success: function(data)
			{ 
				data = JSON.parse(data);
				if(data['code'] == 1){
					notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block follow_ajax f-12');
					that.html('<?php echo $this->lang->line('follow'); ?>');				
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
      	});  
    });

    $(document).on('click', ".follow_ajax", function(e){
		e.preventDefault();
    	var id_user 	= child_id;
    	var subject_id 	= $(this).attr('subject_id');
    	var tutor_id 	= $(this).attr('tutorid');
    	var subject_namee= $(this).attr('subject_namee');
    	var that 		= $(this);
    	$.ajax({
			url: '<?php echo base_url();?>Master/savebooking',
			type: 'GET',
			data: {
				id_user: id_user,
				tutor_id: tutor_id,
				subject_id: subject_id,
				subject_namee: subject_namee
			},
			success: function(data)
			{ 		
				data = JSON.parse(data);		
				if(data['code'] == 1){
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-red c-white btn btn-danger btn-block unfollow_ajax f-12');
					that.html('<?php echo $this->lang->line('unfollow'); ?>');								
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
      	}); 
    });

    $('.showModal').click(function(){
        var ids = $(this).attr('id');
        alert('a'+ids);
        ids = ids.substr(5,10);
        $.get('<?php echo base_url(); ?>master/onah?id_user='+ids,function(hasil){
            hasil = JSON.parse(hasil);
            $('.modal_approve').attr('id',ids);
            $('.modal_decline').attr('id',ids);
            $('#modal_image').attr('src','<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>'+hasil['user_image']);            
            if (hasil['self_description'] == '') {
                $('#modal_self_desc').html("<?php echo $this->lang->line('nodescription'); ?>");
            }
            else
            {
                $('#modal_self_desc').html(hasil['self_description']);
            }
            $('#modal_user_name').html(hasil['user_name']);
            $('#modal_birthplace').html(hasil['user_birthplace']);
            $('#modal_birthdate').html(hasil['user_birthdate']);
            $('#modal_age').html(hasil['user_age']);
            $('#modal_callnum').html(hasil['user_callnum']);
            // $('#modal_gender').html(hasil['user_gender']);
            // alert(hasil['user_gender']);
            if (hasil['user_gender'] == 'Male') {
                $('#modal_gender').html("<?php echo $this->lang->line('male');?>");
            }
            else if(hasil['user_gender'] == 'Female')
            {
                $('#modal_gender').html("<?php echo $this->lang->line('women');?>");
            }
            $('#modal_religion').html(hasil['user_religion']);
            $('#modal_competency').html(hasil['competency']);
            $('#modal_eduback').html("Pendidikan Terakhir "+hasil['last_education']);
            // $('#modal_teachexp').html("Pengalaman Mengajar "+hasil['year_experience'] +" Tahun" );
            if(hasil['competency'] != ''){
                for (var i = 0; i<hasil['competency'].length; i++) {
                    $('#modal_competency').append('<tr>'+
                        '<td>'+hasil['competency'][i]['subject_name']+'</td>'+
                        '<td>'+hasil['competency'][i]['jenjang_name']+'  '+hasil['competency'][i]['jenjang_level']+'</td>'+                     
                        // '<td>'+hasil['education_background'][i]['institution_address']+'</td>'+
                        // '<td>'+hasil['education_background'][i]['graduation_year']+'</td>'+
                        '</tr>');
                }
            }
            $('#modalDefault').modal('show');
        });
        $('.modal_approve').click(function(){
            var newids = $(this).attr('id');
            $.get('<?php echo base_url(); ?>first/approveTheTutor/'+newids,function(s){
                location.reload();
            });
        });
        $('.modal_decline').click(function(){
            var newids = $(this).attr('id');
            $.get('<?php echo base_url(); ?>first/declineTheTutor/'+newids,function(s){
                location.reload();
            });
        });

        $("#show1").click(function(){            
            $("#contoh2").css('display','none');
            $("#contoh1").css('display','block');
        });
        $("#show2").click(function(){            
            $("#contoh1").css('display','none');
            $("#contoh2").css('display','block');    
        });

    });

    
</script>
