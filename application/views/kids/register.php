<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container invoice">

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="block-header" style="margin-bottom: 50px;">
                <!-- <h2>Daftar Anak</h2> -->
            </div>
                                
            <div class="card">
                <div class="card-header ch-alt">
                    <div class="pull-left f-16"><?php echo $this->lang->line('registerkid')?></div><br>
                    <!-- <div class="pull-right f-14" style="margin-top: -10px;"><?php echo $this->lang->line('transaksi'); ?> </div><br> -->
                </div>
                
                <div class="card-body card-padding" style="background: white;">                
                    <div class="row">                        
                        <form method="post" action="<?php echo base_url('Kids/registerkids'); ?>" enctype="multipart/form-data">
                            
                            <div class="col-md-2 m-t-25"></div>
                            <div class="col-md-8 m-t-25 m-b-25" style="border: 1px solid gray; border-radius: 5px; padding: 15px;">

                                <input type="text" name="email" style="display: none;" required class="input-sm form-control fg-input" value="<?php echo $this->session->userdata('email'); ?>"/>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('username'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="username" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typeusername'); ?>"/>
                                        </div>
                                    </div>
                                </div>  
                                <br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('password'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="password" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typepassword'); ?>"/>
                                        </div>
                                        <label class="c-red m-t-5 f-12">( <?php echo $this->lang->line('aksesloginkid'); ?> )</label>
                                    </div>
                                </div>                         
                                
                                <div class="form-group" style="margin-top: 11%;">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('first_name'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="first_name" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typefirstname'); ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('last_name'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="last_name" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typelastname'); ?>"/>
                                        </div>
                                    </div>
                                </div>   
                                <br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('birthday'); ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"></span>
                                        <div class="col-sm-4 m-t-10" style="margin-left: -7%;">
                                            <div class="fg-line">
                                                <select name="tanggal_lahir" id="tanggal_lahir" class="select2 form-control">
                                                        <option disabled selected><?php echo $this->lang->line('datebirth'); ?></option>
                                                        <?php 
                                                        if(isset($alluser)){
                                                            $tgl = substr($alluser['user_birthdate'], 8,2);
                                                            $bln = substr($alluser['user_birthdate'], 5,2);
                                                            $thn = substr($alluser['user_birthdate'], 0,4);
                                                        }

                                                            for ($date=01; $date <= 31; $date++) {
                                                                echo '<option value="'.$date.'" ';
                                                                if(isset($alluser) && $tgl == $date){ echo "selected='true'";}
                                                                echo '>'.$date.'</option>';
                                                            }
                                                        ?>  
                                                    </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 m-t-10">
                                            <div class="fg-line">
                                                <select name="bulan_lahir" class="select2 form-control">
                                                    <option disabled selected><?php echo $this->lang->line('monthbirth'); ?></option>
                                                    <option value="01" <?php if(isset($alluser) && $bln == "01"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jan'); ?></option>
                                                    <option value="02" <?php if(isset($alluser) && $bln == "02"){ echo "selected='true'";} ?>><?php echo $this->lang->line('feb'); ?></option>
                                                    <option value="03" <?php if(isset($alluser) && $bln == "03"){ echo "selected='true'";} ?>><?php echo $this->lang->line('mar'); ?></option>
                                                    <option value="04" <?php if(isset($alluser) && $bln == "04"){ echo "selected='true'";} ?>><?php echo $this->lang->line('apr'); ?></option>
                                                    <option value="05" <?php if(isset($alluser) && $bln == "05"){ echo "selected='true'";} ?>><?php echo $this->lang->line('mei'); ?></option>
                                                    <option value="06" <?php if(isset($alluser) && $bln == "06"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jun'); ?></option>
                                                    <option value="07" <?php if(isset($alluser) && $bln == "07"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jul'); ?></option>
                                                    <option value="08" <?php if(isset($alluser) && $bln == "08"){ echo "selected='true'";} ?>><?php echo $this->lang->line('ags'); ?></option>
                                                    <option value="09" <?php if(isset($alluser) && $bln == "09"){ echo "selected='true'";} ?>><?php echo $this->lang->line('sep'); ?></option>
                                                    <option value="10" <?php if(isset($alluser) && $bln == "10"){ echo "selected='true'";} ?>><?php echo $this->lang->line('okt'); ?></option>
                                                    <option value="11" <?php if(isset($alluser) && $bln == "11"){ echo "selected='true'";} ?>><?php echo $this->lang->line('nov'); ?></option>
                                                    <option value="12" <?php if(isset($alluser) && $bln == "12"){ echo "selected='true'";} ?>><?php echo $this->lang->line('des'); ?></option>  
                                                </select> 
                                            </div>
                                            <!-- <span class="zmdi zmdi-triangle-down form-control-feedback"></span> -->
                                        </div>
                                        <div class="col-sm-4 m-t-10">                                   
                                            <div class="fg-line">
                                                <select name="tahun_lahir" id="tahun_lahir" class="select2 form-control">
                                                    <option disabled selected><?php echo $this->lang->line('yearbirth'); ?></option>
                                                    <?php 
                                                        for ($i=1960; $i <= 2016; $i++) {                                                                            
                                                            echo '<option value="'.$i.'" ';
                                                            if(isset($alluser) && $thn == $i){ echo "selected='true'";}
                                                            echo '>'.$i.'</option>';
                                                        } 
                                                    ?>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('placeofbirth'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="user_birthplace" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typebirthplace'); ?>"/>
                                        </div>
                                    </div>
                                </div>   
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('gender'); ?></label>
                                    <div class="col-sm-9 m-t-10">
                                        <div class="fg-line">
                                            <label class="radio radio-inline ">
                                                <input type="radio" name="user_gender" value="Male">
                                                <i class="input-helper"></i><?php echo $this->lang->line('male'); ?>  
                                            </label>
                                            
                                            <label class="radio radio-inline " >
                                                <input type="radio" name="user_gender" value="Female">
                                                <i class="input-helper"></i><?php echo $this->lang->line('women'); ?>  
                                            </label>
                                        </div>
                                    </div>
                                </div>   
                                <br><br>
                                <!-- <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('school_name'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="school_name" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typeschoolname'); ?>"/>
                                        </div>
                                    </div>
                                </div>   --> 
                                <br><br>
                                <?php 
                                    $provinsi = "";
                                    $d_kabupaten = "";
                                    $d_kecamatan = "";
                                    // $d_kelurahan = "";
                                    $d_schoolname = "";
                                ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('school_jenjang'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="jenjang_id"  class="selectSD form-control" id='jenjang_level' data-placeholder="<?php echo $this->lang->line('select_class'); ?>">
                                                <option value=""></option>
                                                <?php
                                                    $jen = $this->Master_model->getMasterJenjang();
                                                    if($jen != ''){
                                                        echo "<option value='".$jen['jenjang_id']."' selected='true'>".$jen['jenjang_name'].' '.$jen['jenjang_level']."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>   
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12">Provinsi Sekolah</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">                                        
                                            <select id='school_provinsi' class="select2 form-control" name="school_provinsi" data-placeholder="<?php echo $this->lang->line('selectprovinsii'); ?>">                                            
                                            <?php
                                                
                                                $prv = $this->Master_model->school_provinsi($provinsi);                                                
                                                echo '<option disabled="disabled" selected="" value="0">Provinsi</option>'; 
                                                foreach ($prv as $row => $v) {                                            
                                                    echo '<option value="'.$v['provinsi'].'">'.$v['provinsi'].'</option>';
                                                }
                                            ?>
                                            </select>
                                        </div>                                           
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group" >
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12">Kabupaten Sekolah</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line" id="boxkabupaten">                                        
                                            <select disabled id='school_kabupaten' class="select2 form-control" name="school_kabupaten" data-placeholder="<?php echo $this->lang->line('selectkabupaten'); ?>">
                                                <option value=""></option>
                                                <?php
                                                    
                                                    $kbp = $this->Master_model->school_getAllKabupaten($d_kabupaten,$provinsi);
                                                    if($kbp != ''){
                                                        foreach ($kbp as $key => $value) {
                                                            echo "<option value='".$value['kabupaten']."'>".$value['kabupaten']."</option>";
                                                        }
                                                        // echo "<script>pfka = '".$kbp['kabupaten']."'</script>";
                                                    }

                                                ?>
                                            </select>                                            
                                        </div>
                                    </div>                                    
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12">Kecamatan Sekolah</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">                                       
                                            <select  disabled id='school_kecamatan' class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkecamatann'); ?>" name="id_kecamatan">
                                                <option value=''></option>
                                                <?php
                                                    $kcm = $this->Master_model->school_getAllKecamatan($d_kecamatan,$d_kabupaten);
                                                    if($kcm != ''){
                                                        echo "<option value='".$kcm['kecamatan']."' selected='true'>".$kcm['kecamatan']."</option>";
                                                        echo "<script>pfkec = '".$kcm['kecamatan']."'</script>";

                                                    }

                                                ?>
                                            </select>
                                            <small id="cek_kecamatan" style="color: red;"></small>
                                        </div>
                                    </div>
                                </div> 
                                <br><br> 
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12">Nama Sekolah</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select disabled id='school_name' name="school_name" required class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkelurahann'); ?>" name="id_desa">
                                                <option value=''></option>
                                                 <?php
                                                        // $name = $this->Master_model->school_getName($d_schoolname,$d_kelurahan);
                                                      

                                                        // $name = $this->db->query("SELECT school_name FROM master_school group by school_name ASC")->result_array();
                                                       
                                                       
                                                        
                                                        if($name == ''){
                                                            echo "<option value='".$name['school_name']."' selected='true'>".$name['school_name']."</option>";
                                                            echo "<script>pfname = '".$name['school_name']."'</script>";
                                                        }

                                                    ?>
                                            </select>
                                            <small id="cek_schoolname" style="color: red;"></small>
                                        </div>
                                    </div>
                                </div>  
                                <br><br> 
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('school_address'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <textarea name="school_address" id="school_address" rows="3" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typeschooladdress'); ?>"></textarea>
                                        </div>
                                    </div>
                                </div>      
                                <br><br><br>
                                


                                <button class="btn btn-md c-white btn-block m-t-10 bgm-blue" id="kotak" style="border: 1px solid gray;"><?php echo $this->lang->line('register'); ?></button>
                            </div>
                            <div class="col-md-2 m-t-25"></div>
                                                    
                        </form>

                    </div>
                </div>
                
                <!-- <footer class="p-20">
                    <a href="<?php echo base_url();?>first/topup"><button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;">Konfirmasi</button></a>
                </footer> -->
            </div>
            
        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function(){
        $('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $("#nominal").keyup(function() {
          var clone = $(this).val();
          var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
          $("#nominalsave").val(cloned);
        });

    });

</script>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datetimepicker({
            locale: 'id'
        });

        // $('#school_provinsi').change(function(){
        //     var prov = $(this).val();
        //     var placeprov = "<?php echo $this->lang->line('selectprovinsii'); ?>";
        //     $("#boxkabupaten").empty();
        //     var kotakboxprov = "<select id='school_kabupaten' class='select2 form-control' name='school_kabupaten' data-placeholder='"+placeprov+"'>"+isi+"</select>";
        //     $("#boxkabupaten").append(kotakboxprov);
        // });
        $('select.select2').each(function(){            
            if($(this).attr('id') == 'school_provinsi' || $(this).attr('id') == 'school_kabupaten' || $(this).attr('id') == 'school_kecamatan'  || $(this).attr('id') == 'school_name'){
                var a = null;
                if ($(this).attr('id') == 'school_provinsi') { a = "<?php echo $this->lang->line('chooseyourpropinsi'); ?>";} if ($(this).attr('id') == 'school_kecamatan') { a = "<?php echo $this->lang->line('chooseyourkecamatan'); ?>";} if ($(this).attr('id') == 'school_kabupaten') { a = "<?php echo $this->lang->line('chooseyourkabupaten'); ?>";}
                $(this).select2({
                    minimumInputLength: 1,
                    placeholder: a,
                    delay: 2000,
                    ajax: {
                        dataType: 'json',
                        type: 'GET',
                        url: '<?php echo base_url(); ?>ajaxer/ajax_indonesia_school_SD/' + $(this).attr('id'),
                        data: function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1,
                                pfp: pfp,
                                pfka: pfka,
                                pfkec: pfkec,
                                pfname: pfname
                            };
                        },
                        processResults: function(data){
                            return {
                                results: data.results,
                                pagination: {
                                    more: data.more
                                }
                            };
                        }
                    }
                });
            }
            else{
              $(this).select2();
            }
        });

        $('#school_provinsi').change(function(){
            var prov = $(this).val();
            pfp = prov;            
            $('#school_kabupaten.select2').select2("val","");
            $("#school_kabupaten").removeAttr('disabled');
            $("#school_kecamatan").attr('disabled','');
            $("#school_name").attr('disabled','true');
        });
        $('#school_kabupaten').change(function(){
            var prov = $(this).val();
            pfka = prov;            
            $('#school_kecamatan.select2').select2("val","");
            $("#school_kecamatan").removeAttr('disabled');            
            $("#school_name").attr('disabled','true');
        });
        $('#school_kecamatan').change(function(){
            var prov = $(this).val();
            pfkec = prov;            
            $('#school_name.select2').select2("val","");
            $("#school_name").removeAttr('disabled');
        });

        $('#school_name').change(function(){
            var prov = $(this).val();            
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>ajaxer/getSchoolAddress/'+prov,
                success:function(datis){
                    var address = $.trim(datis);
                    $('#school_address').val(address);
                }
            });
        });
    });
</script>
