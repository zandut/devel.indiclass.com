<div id="loader">
    <div class="page-loader" style="background-color:#1274b3;">
        <div class="preloader pl-xl pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Class Miles . . .</p>
        </div>
    </div>
</div>
<header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #2196f3;">
    
        <style type="text/css">
            /* scroller browser */
        ::-webkit-scrollbar {
            width: 9px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
            -webkit-border-radius: 7px;
            border-radius: 7px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            -webkit-border-radius: 7px;
            border-radius: 7px;
            background: #a6a5a5;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
        }
        ::-webkit-scrollbar-thumb:window-inactive {
            background: rgba(0,0,0,0.4); 
        }
        </style>
        <ul class="header-inner clearfix m-l-10 m-r-10">
            <li id="menu-trigger" data-trigger="#sidebar" >
                <div class="line-wrap">
                    <div class="line top"></div>
                    <div class="line center"></div>
                    <div class="line bottom"></div>
                </div>
            </li>

            <li>
                <a href="#" class="m-l-10"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/logoclass6.png" alt=""></a>
            </li> 
            <li class="pull-right hidden-xs">
                <ul class="top-menu">
                    <?php
                        $getusertype_id = $this->session->userdata('usertype_id');
                        $getiduser = $this->session->userdata('id_user');
                        $status = $this->session->userdata('status');
                        if ($getusertype_id == "student" || $getusertype_id == "student kid" && $status == "1") {
                            $jumlah = $this->db->query("SELECT count(*) as total FROM tbl_order WHERE buyer_id='$getiduser' AND order_status=2")->row_array()['total'];
                    ?>
                    <li class="dropdown" title="Invoice">
                        <a data-toggle="dropdown" href="">
                            <i class="tm-icon zmdi zmdi-file-text"></i>  
                            <i class="tmn-counts" style="<?php if($jumlah>0){ echo 'display: block';} else{ echo "display: none;";}?>"><?php echo $jumlah;?></i>                  
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg pull-right">
                            <div class="listview">
                                <div class="lv-header">
                                    <?php echo $this->lang->line('listinvoice');?>
                                    <br>
                                    <small style="color: #6C7A89; text-transform: lowercase;">(<?php echo $this->lang->line('listdetailinvoice');?>)</small>
                                </div>
                                
                                    <div class="lv-body" style="overflow-y:auto; height:250px; ">                           
                                        <?php
                                            $myid               = $this->session->userdata('id_user');           
                                            $anak               = $this->db->query("SELECT * FROM tbl_profile_kid WHERE parent_id='$myid'")->result_array();
                                            $q_anak = "";
                                            if(!empty($anak)){
                                                foreach ($anak as $key => $value) {
                                                    $q_anak.= " OR tor.buyer_id='".$value['kid_id']."' ";
                                                }
                                            }
                                                
                                            $getdatainvoice   = $this->db->query("SELECT * FROM tbl_invoice as ti INNER JOIN tbl_order as tor ON ti.order_id=tor.order_id WHERE tor.buyer_id='$myid' $q_anak ORDER BY tor.order_status DESC")->result_array();
                                            if (empty($getdatainvoice)) {
                                                ?>
                                                <div class="lv-item">
                                                    
                                                    <center class="m-t-15" style="margin-right: 20px;">
                                                        <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>icons/Invoice.png" style="margin-left:25px; width: 75%; height: 170px;">
                                                    
                                                        <div class="checkbox m-t-20" style="cursor:not-allowed;">
                                                            <div class="media" style="padding: 5px; border-radius: 5px; border-collapse: inherit;">                                        
                                                                <div class="media-body">
                                                                    <label class="f-14"><?php echo $this->lang->line('noinvoice');?></label>                                     
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </center>
                                                </div>
                                                <?php
                                            }
                                            else
                                            {

                                                foreach ($getdatainvoice as $row => $v) {
                                                    $invoice_id     = $v['invoice_id'];
                                                    // $product_id     = $v['product_id'];
                                                    $order_id       = $v['order_id'];
                                                    $order_status   = $v['order_status'];
                                                    $payment_type   = $v['payment_type'];

                                                    $get_orderdetail = $this->db->query("SELECT tod.*, tr.subject_id as tr_subject_id, tr.tutor_id as tr_tutor_id, tr.approve as tr_approve, trg.subject_id as trg_subject_id, trg.tutor_id as trg_tutor_id, trg.approve as trg_approve, tc.subject_id as trm_subject_id, tc.tutor_id as trm_tutor_id, trm.status as trm_approve FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id  WHERE tod.order_id='$order_id'")->result_array(); 
                                                    
                                                    $harga          = $v['total_price'];
                                                    $hargaprice     = number_format($harga, 0, ".", ".");
                                                    // if ($order_status == 2) {
                                                    if ($payment_type == null) {
                                                        $link       = "https://devel.classmiles.com/DetailOrder?order_id=".$order_id;
                                                    }
                                                    else if($payment_type == "bank_transfer"){
                                                        $link       = "https://devel.classmiles.com/DetailPayment?order_id=".$order_id;
                                                    }
                                                    ?>                                
                                                    <div class="lv-item z-depth-1-bottom" title="Klik untuk detail" style="cursor: pointer; border: 1px solid #F2F1EF; padding: 10px; border-radius: 5px; border-collapse: inherit; margin: 5px;">
                                                        <a class="lv-item" href="<?php echo $link; ?>">
                                                        <div class="checkbox">
                                                            <div class="media">                                        
                                                                <div class="media-body">                                                        
                                                                    <!-- <div class="pull-left" style="height: 60px;">
                                                                        <img class="lv-img-sm" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$tutor_image;?>" alt="" style="width: 40px; height: 40px;">
                                                                    </div> -->
                                                                    <div class="pull-left" style="border-bottom: 1px solid #EEEEEE; margin-bottom: 5px;">
                                                                        <div class="lv-title c-gray f-13"><b><?php echo $invoice_id;?></b></div>
                                                                        <small><?php echo $v['created_at'].' Rp '.$hargaprice;?></small>
                                                                    </div>

                                                                    <?php 
                                                                        $status = null;
                                                                        foreach ($get_orderdetail as $key => $value) {
                                                                            $subject_id     = $value['tr_subject_id'] != null ? $value['tr_subject_id'] : ( $value['trg_subject_id'] != null ? $value['trg_subject_id'] : $value['trm_subject_id']);
                                                                            $tutor_id       = $value['tr_tutor_id'] != null ? $value['tr_tutor_id'] : ( $value['trg_tutor_id'] != null ? $value['trg_tutor_id'] : $value['trm_tutor_id']);
                                                                            $status         = $value['tr_approve'] != null ? $value['tr_approve'] : ( $value['trg_approve'] != null ? $value['trg_approve'] : $value['trm_approve']);

                                                                            $datasub   = $this->db->query("SELECT ms.subject_name, mj.jenjang_name, mj.jenjang_level FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.subject_id='$subject_id'")->row_array();
                                                                            $subject_name   = $datasub['subject_name'];
                                                                            $jenjang_data   = $datasub['jenjang_name'].' '.$datasub['jenjang_level'];
                                                                            $data_tutor     = $this->db->query("SELECT first_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
                                                                            $tutor_name     = $data_tutor['first_name'];
                                                                            $tutor_image    = $data_tutor['user_image'];
                                                                            ?>
                                                                        <div class="pull-left" style="height: 50px; padding : 5px; background-color: rgba(236, 236, 236, 0.20); width: 100%;">
                                                                            <div class="lv-title"><?php echo $tutor_name;?></div>
                                                                            <small class="lv-small"><?php echo $subject_name.' - '.$jenjang_data; ?></small>                                                                    
                                                                        </div>
                                                                        <?php 
                                                                        }
                                                                    ?>      
                                                                    <div class="pull-left" style="<?php if($order_status==2){ echo "display:block;";} else if($order_status==1){ echo 'display: block'; }else { echo "display:none;"; }?> width: 100%; border-top: 1px solid #EEEEEE; margin-top: 2%;">
                                                                        <button class="btn <?php if($order_status==2){ echo "btn-warning";} else if($order_status==1){ echo 'btn-success'; }else { echo "btn-default"; }?> btn-block f-10"><i><?php if($order_status==2){ echo "Menunggu pembayaran anda"; }else if($order_status==1){ echo "Selesai"; }?></i></button>
                                                                    </div>                                       
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </a>
                                                    </div>
                                                    <?php
                                                    // }
                                                }
                                            }
                                            
                                        ?>
                                    </div>
                                    <!-- style="<?php if($status !=1){ echo "background-color:#EEEEEE;  cursor:not-allowed;"; }?>" href="<?php if($status ==1){ echo 'https://classmiles.com/DetailPayment?order_id='.$order_id.'&u=372';}else { echo '#';}?>" -->
                                    <!-- <button class="lv-footer btn btn-success btn-block btn-lg c-white f-14">Bayar</button> -->
                            </div>
                        </div>
                    </li>
                    <?php 
                    }
                    ?>

                    <?php
                        $getusertype_id = $this->session->userdata('usertype_id');
                        $myid = $this->session->userdata('id_user');
                        $status = $this->session->userdata('status');
                        $anak   = $this->db->query("SELECT * FROM tbl_profile_kid WHERE parent_id='$myid'")->result_array();
                        $q_anak = "";
                        if(!empty($anak)){
                            foreach ($anak as $key => $value) {
                                $q_anak.= " OR tk.id_user='".$value['kid_id']."' ";
                            }
                        }
                        if ($getusertype_id == "student" || $getusertype_id == "student kid" && $status == "1") {  
                            $jumlah     = $this->db->query("SELECT count(*) as jumlah FROM tbl_keranjang as tk LEFT JOIN tbl_request as tr ON tk.request_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON tk.request_grup_id=trg.request_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tclass ON trm.class_id=tclass.class_id) ON tk.request_multicast_id=trm.request_id WHERE tk.id_user='$myid' $q_anak AND tr.approve=2 OR trg.approve=2 OR trm.status=2")->row_array()['jumlah'];                              
                    ?>
                    <li class="dropdown" title="Keranjang">
                        <a data-toggle="dropdown" href="">
                            <i class="tm-icon zmdi zmdi-shopping-cart"></i>
                            <i class="tmn-counts" style="<?php if($jumlah>0){ echo 'display: block';} else{ echo "display: none;";}?>"><?php echo $jumlah;?></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg pull-right">
                            <div class="listview">
                                <div class="lv-header">
                                    <?php echo $this->lang->line('keranjangorder');?>
                                </div>
                                <form action="<?php echo base_url(); ?>process/submitOrder" method="post">
                                    <div class="lv-body" style="overflow-y:auto; height:250px; ">                           
                                        <?php
                                            $top                = 0;
                                            $myid               = $this->session->userdata('id_user');                                    
                                            $getdatakeranjang   = $this->db->query("SELECT tk.*, tclass.class_id, tr.approve as tr_approve, trg.approve as trg_approve, trm.status as trm_approve, tr.subject_id as tr_subject_id, trg.subject_id as trg_subject_id, tclass.subject_id as trm_subject_id,  tr.tutor_id as tr_tutor_id, trg.tutor_id as trg_tutor_id, tclass.tutor_id as trm_tutor_id, tr.topic as tr_topic, trg.topic as trg_topic, tclass.description as trm_topic, tr.date_requested as tr_date_requested, trg.date_requested as trg_date_requested, trm.created_at as trm_date_requested FROM tbl_keranjang as tk LEFT JOIN tbl_request as tr ON tk.request_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON tk.request_grup_id=trg.request_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tclass ON trm.class_id=tclass.class_id) ON tk.request_multicast_id=trm.request_id WHERE tk.id_user='$myid' $q_anak")->result_array();
                                            if (empty($getdatakeranjang)) {
                                                ?>
                                                <div class="lv-item">
                                                    <center class="m-t-15" style="margin-right: 30px;">
                                                        <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>icons/KeranjangKosong.png" style="width: 75%; height: 170px;">
                                                    
                                                        <div class="checkbox m-l-10 m-t-20" style="cursor:not-allowed;">
                                                            <div class="media" style="padding: 5px; border-radius: 5px; border-collapse: inherit;">                                        
                                                                <div class="media-body">

                                                                    <label class="f-14"><?php echo $this->lang->line('noorder_class');?></label>                                     
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </center>
                                                </div>
                                                <?php
                                            }                         
                                            else
                                            {
                                                $numItems = count($getdatakeranjang);
                                                foreach ($getdatakeranjang as $row => $v) {
                                                    $subject_id     = $v['tr_subject_id'] != null ? $v['tr_subject_id'] : ( $v['trg_subject_id'] != null ? $v['trg_subject_id'] : $v['trm_subject_id'] );
                                                    $tutor_id       = $v['tr_tutor_id'] != null ? $v['tr_tutor_id'] : ( $v['trg_tutor_id'] != null ? $v['trg_tutor_id'] : $v['trm_tutor_id'] );
                                                    $status         = $v['tr_approve'] != null ? $v['tr_approve'] : ( $v['trg_approve'] != null ? $v['trg_approve'] : $v['trm_approve'] );
                                                    $topic          = $v['tr_topic'] != null ? $v['tr_topic'] : ( $v['trg_topic'] != null ? $v['trg_topic'] : $v['trm_topic'] );
                                                    $date_requested = $v['tr_date_requested'] != null ? $v['tr_date_requested'] : ( $v['trg_date_requested'] != null ? $v['trg_date_requested'] : $v['trm_date_requested'] );

                                                    $subject_name   = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
                                                    $data_tutor     = $this->db->query("SELECT user_name,first_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
                                                    $tutor_name     = $data_tutor['first_name'];
                                                    $tutor_image    = $data_tutor['user_image'];
                                                    $request_id     = $v['request_id'];
                                                    $request_grup_id= $v['request_grup_id'];
                                                    $harga          = $v['price'];
                                                    $hargaprice     = number_format($harga, 0, ".", ".");
                                                    $a = $v['id_user'] == $myid ? 'ortu' : 'anak';
                                                ?>                                
                                                <div class="lv-item" style="<?php if($status !=2){ echo "background-color:#EEEEEE;  cursor:not-allowed;"; }?>border: 1px solid #F2F1EF; padding: 5px; border-radius: 5px; border-collapse: inherit; margin: 5px;">
                                                    <!-- <a class="lv-item" href=""> -->
                                                    <div class="checkbox">
                                                        <div class="media" >                                        
                                                            <div class="media-body">
                                                                <div class="pull-left" style="margin-top: 3%;">
                                                                    <label class="checkbox">
                                                                        <input type="checkbox" <?php echo $status != 2 ? 'disabled' : '';  ?> id="checkbox<?php echo $v['krnj_id'];?>" name='orderKrnj[]' value="<?php echo $v['krnj_id'];?>" class="booking">
                                                                        <i class="input-helper"></i>                                                    
                                                                    </label>
                                                                </div>
                                                                <div class="pull-left">
                                                                    <img class="lv-img-sm" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$tutor_image;?>" alt="" style="width: 40px; height: 40px;">
                                                                </div>
                                                                <div class="pull-left m-l-10 row" style="word-wrap: break-word;">
                                                                    <div class="lv-title"><?php echo $tutor_name.' - Rp. '.$hargaprice;?></div>
                                                                    <small class="lv-small" style="word-wrap: break-word;"><p><?php echo $subject_name; ?></p></small>
                                                                    <!-- <small class="lv-small"><?php echo ", Rp ".$hargaprice; ?></small> -->
                                                                </div>
                                                                <!-- <div class="pull-right" style="height: 30px; margin-left: -7%; margin-top: 5%;">
                                                                    <label>Rp. <?php echo $hargaprice;?></label>
                                                                </div> -->
                                                                <div class="col-md-12" style="<?php if($status==2){ echo "display:block;";} else if($status==1){ echo 'display: block'; } else if($status==0){ echo 'display: block'; } else if ($status==-2){ echo "display: none"; } else { echo "display:none;"; }?> height: 35px; margin-left: -8%; margin-top: 5%; width: 100%;">
                                                                    <label class="f-12 c-gray"><i><?php if($status==0){ echo "Status : Menunggu persetujuan tutor"; }else if($status==2){ echo "Status : Proses pemilihan pembayaran"; } else if ($status==-2){ echo "Status: Pembelian expired"; }?></i></label>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- </a> -->                                            
                                                </div>
                                                <?php 
                                                    if(++$row === $numItems) {
                                                        echo "<br><br>";
                                                    }                                        
                                                } 
                                                ?>
                                                <button class="lv-footer btn btn-success btn-block btn-lg c-white f-14" style="position: absolute; bottom: 0;" id="btnbayar" disabled="false">Lanjutkan</button>
                                                <?php                                
                                            }
                                        ?>
                                    </div>                          
                                    
                                </form>  
                            </div>
                        </div>
                    </li>
                    <?php 
                    }
                    ?>

                    <li class="dropdown" id="notif" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;" id_user="<?php echo $this->session->userdata('id_user');?>">
                        <a data-toggle="dropdown" href="" ng-init="getnotif()">
                            <i class="tm-icon zmdi zmdi-notifications"></i>
                            <i class='tmn-counts bara' ng-if="nes > '0'">{{nes}}</i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg pull-right" >
                            <div class="listview" id="notifications" >
                                <div class="lv-header">
                                    <?php echo $this->lang->line('notification') ?>

                                    <ul class="actions" data-toggle="tooltip" title="Read all" data-placement="right">
                                        <li class="dropdown">   
                                            <a class="readsa" style="cursor: pointer;" id_user='<?php echo $this->session->userdata('id_user');?>'>
                                                <i class="zmdi zmdi-check-all" ></i>
                                            </a>                                                                  
                                        </li>
                                    </ul>
                                </div>                        
                                <div class="lv-body" id="scol" style="overflow-y:auto; height:350px; " when-scrolled="loadData()" style="cursor: pointer;">
                                    <div ng-repeat='x in dataf' style="cursor: pointer;">
                                        <a class="lv-item tif" ng-if="x.read_status == '1'" hrefa="{{x.link}}" notif_id="{{x.notif_id}}">
                                            <div class="media" data-toggle="tooltip" title="">
                                                <div class="pull-left">
                                                    <img width="65px" height="65px" style="border-radius: 5px;" src="<?php echo $this->session->userdata('user_image'); ?>"> 
                                                </div>
                                                <div class="media-body" style="overflow:hidden; word-wrap: break-word;">
                                                    <div style="font-size: 11px; color:black;">{{x.notif}}</div>                                            
                                                </div>
                                                <div class="media-footer">
                                                    <small class="c-black" style="position:absolute; bottom:0px; margin-bottom: 1px; margin-top: auto;">{{x.interval_time}}</small>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="lv-item tif" style="background: #ECF0F1;" ng-if="x.read_status == '0'" hrefa="{{x.link}}" notif_id="{{x.notif_id}}" >
                                            <div class="media">
                                                <div class="pull-left">
                                                    <img width="65px" height="65px" src="<?php echo $this->session->userdata('user_image'); ?>"> 
                                                </div>
                                                <div class="media-body" style="overflow:hidden; word-wrap: break-word;">
                                                    <div style="font-size: 11px; color:black;">{{x.notif}}</div>                                            
                                                </div>
                                                <div class="media-footer">
                                                    <small class="c-black" style="position:absolute; bottom:0px; margin-bottom: 1px; margin-top: auto;">{{x.interval_time}}</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div ng-show="lodingnav">
                                        <center><div class="preloader pl-lg">
                                            <svg class="pl-circular" viewBox="25 25 50 50">
                                                <circle class="plc-path" cx="50" cy="50" r="20" />
                                            </svg>
                                        </div></center>
                                    </div>  
                                    <!-- <a class="lv-item" style="background: #ECF0F1;">
                                        <div class="media" data-toggle="tooltip" data-placement="bottom" title="Please, complete your profile for tutor registration approval">
                                            <div class="pull-left">
                                                <img class="lv-img-sm" src="<?php echo base_url('aset/img/user/'.$this->session->userdata('user_image')); ?>">                                        
                                            </div>
                                            <div class="media-body">
                                                <div class="lv-title">Administrator</div>
                                                <small class="lv-small">Please, complete your profile for tutor registration approval</small>
                                                <small class="c-black">34 Menit yang lalu</small>
                                            </div>                                    
                                        </div>
                                    </a> -->
                                </div>                     

                            </div>

                        </div>
                    </li>     

                    <li class="dropdown" style="height: 35px;">
                        <a data-toggle="dropdown" href="">
                             
                            <?php
                                $patokan =  $this->lang->line('profil');

                                if ($patokan == "Profile") {
                                    ?>
                                    <img class="m-t-5" width="35px" height="20px" style="border-radius:15%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>language/flaginggris.png?>"                
                                    <?php
                                }
                                else if ($patokan == "Data Diri"){
                                    ?>
                                    <img class="m-t-5" width="35px" height="20px" style="border-radius:15%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>language/flagindo.png?>"                
                                    <?php
                                }
                            ?>                           
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg pull-right">
                            <div class="listview" id="notifications">

                                <div class="lv-header">
                                    <?php echo $this->lang->line('chooselanguage'); ?>                                                            
                                </div>

                                <div class="lv-body" id="checklang">                            
                                    <a class="lv-item" href="<?php echo base_url('set_lang/english'); ?>" id="btn_setenglish">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img width="35px" height="20px" style="border-radius:15%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>language/flaginggris.png" alt="">
                                            </div>
                                            <div class="media-body">
                                                <div class="lv-title">English</div>                                                    
                                            </div>
                                        </div>
                                    </a>  
                                    <a class="lv-item" href="<?php echo base_url('set_lang/indonesia'); ?>" id="btn_setindonesia">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img width="35px" height="20px" style="border-radius:15%;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>language/flagindo.png" alt="">
                                            </div>
                                            <div class="media-body">
                                                <div class="lv-title">Bahasa Indonesia</div>                                                    
                                            </div>
                                        </div>
                                    </a>         
                                    <!-- <a class="lv-item" href="<?php echo base_url('set_lang/arab'); ?>">
                                        <div class="media">
                                            <div disabled="true" class="pull-left">
                                                <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/language/saudi_arabia.png" alt="">
                                            </div>
                                            <div class="media-body">
                                                <div class="lv-title">Arabic</div>                                                    
                                            </div>
                                        </div>
                                    </a> -->                                      
                                </div>                                                
                            </div>

                        </div>
                    </li>

                    <li class="dropdown hidden-xs">
                        <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                        <ul class="dropdown-menu dm-icon pull-right">
                           <!--  <li class="skin-switch hidden-xs">
                                <span class="ss-skin bgm-lightblue" data-skin="lightblue" id="lightblue"></span>
                                <span class="ss-skin bgm-bluegray" data-skin="bluegray" id="bluegray"></span>
                                <span class="ss-skin bgm-cyan" data-skin="cyan" id="cyan"></span>
                                <span class="ss-skin bgm-teal" data-skin="teal" id="teal"></span>
                                <span class="ss-skin bgm-orange" data-skin="orange" id="orange"></span>
                                <span class="ss-skin bgm-blue" data-skin="blue" id="blue"></span>
                            </li> -->
                            <li class="hidden-xs">
                                <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i><?php echo $this->lang->line('togglefullscreen'); ?></a>
                            </li>
                            <li class="divider hidden-xs"></li>
                            
                            <li>
                                <a onclick="signOut()" data-action="" href="<?php echo base_url('/logout'); ?>"><i class="zmdi zmdi-time-restore"></i><?php echo $this->lang->line('logout'); ?></a>
                            </li>
                        </ul>
                    </li>
                   <!--  <li class="hidden-xs" id="chat-trigger" data-trigger="#chat">
                        <a href=""><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>
                    </li> -->
                </ul>
            </li>
        </ul>

        <div>        
        <?php
            $now = date('N');
            if(isset($page) && $page == 0){
                $nextMon = date('Y-m-d', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));    
            }else if(isset($page) && $page == 1){
                $nextMon = date('Y-m-d', strtotime('-'.($now-1).' day', strtotime(date('Y-m-d'))));
                $nextMon = date('Y-m-d', strtotime('+7 day', strtotime($nextMon)));
            }
            $oneWeek = date('Y-m-d', strtotime('+6 day', strtotime($nextMon)));
            $nextMon = DateTime::createFromFormat('Y-m-d',$nextMon);
            $oneWeek = DateTime::createFromFormat('Y-m-d',$oneWeek);

            $timeArray = array();

            $arrayFul;
            $stime = 0;
            for($x=0;$x<7;$x++){
                $timeArray[$x]["day"] =  date('D, d M', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));
                $timeArray[$x]["date"] = date('Y-m-d', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));                

                for ($i=0; $i < 24; $i++) {
                    $timeArray[$x][$i] = $stime;
                    $stime+=3600;
                }
                $stime = 0;
            }


        ?> 
         <center>
            <nav class="ha-menu" style="background-color: #2090ea; width: 100%; padding: 0; margin: 0;">
                <div hidden  style="size: 50%;">
                    <?php echo "<a style=' font-size: 10px' id='dt' class='c waves-effect active ' ></a>"; ?>
                    <?php echo "<a style='font-size: 10px' id='dt2' class='c waves-effect active ' ></a>"; ?>
                    <?php echo "<a style='font-size: 10px' id='dt3' class='c waves-effect active ' ></a>"; ?>
                    <?php echo "<a style='font-size: 10px' id='dt4' class='c waves-effect active ' ></a>"; ?>
                    <?php echo "<a style='font-size: 10px' id='dt5' class='c waves-effect active ' ></a>"; ?>
                    <?php echo "<a style='font-size: 10px' id='dt6' class='c waves-effect active ' ></a>"; ?>
                    <?php echo "<a style='font-size: 10px' id='dt7' class='c waves-effect active ' ></a>"; ?>
                </div>
                <ul>
                    <?php

                        echo "<li class=' waves-effect '><a id='prevbtn' ><i class='zmdi zmdi-chevron-left zmdi-hc-fw'></i></a></li>";
                        echo "<li class='c waves-effect active '><a id='d1' ></a></li>";
                        echo "<li class='c waves-effect '><a id='d2' ></a></li>";
                        echo "<li class='c waves-effect '><a id='d3' ></a></li>";
                        echo "<li class='c waves-effect '><a id='d4' ></a></li>";
                        echo "<li class='c waves-effect '><a id='d5' ></a></li>";
                        echo "<li class='c waves-effect '><a id='d6' ></a></li>";
                        echo "<li class='c waves-effect '><a id='d7' ></a></li>";
                        echo "<li class=' waves-effect '><a id='nextbtn' ><i class='zmdi zmdi-chevron-right zmdi-hc-fw'></i></a></li>";
                    ?>
                </ul>
            </nav>
        </center>
        </div>
        <div class="pull-right">
            
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#btn_setindonesia').click(function(e){
                    e.preventDefault();
                    $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
                });
                $('#btn_setenglish').click(function(e){
                    e.preventDefault();
                    $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
                });
            });
        </script>

</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 6; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" style="margin-top: 4%;">

        <div class="container">                
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>            
            
            <div>
                <div class="tn-justified f-16 row text-center m-l-5" style="background-color: white; width: 98%;">
                    <div class="lv-header hidden-xs" style="background-color: white;">
                        <div class="c-teal judul_register m-t-10 m-b-10 text-uppercase" style="font-weight: bold; font-family: 'Trebuchet MS';"><?php echo $this->lang->line('jadwalkelas'); ?> <label id="namechild"></label></div>
                        <small class="text-lowercase" id="levelregister">( <?php echo $this->lang->line('pilihkids'); ?> )</small>
                    </div>
                </div>
                <br><br>    
                <div class="block-header">
                    <h2><?php echo $this->lang->line('home'); ?></h2>

                    <ul class="actions hidden-xs">
                        <li>
                            <ol class="breadcrumb">
                                <li><a class="f-16 c-bluegray" style="margin-top: -3%; cursor: default;" href="#"><?php echo $this->lang->line('kidsselected'); ?> : <label id="namadipilih"></label> . <label class="c-orange" style="cursor: pointer;" id="chooseother"><?php echo $this->lang->line('pilihyanglain'); ?></label></a></li>
                            </ol>
                        </li>
                    </ul>
                </div>
            
                <div class="card p-l-15 p-r-15" id="utama1">
                    <div class="row">

                        <div class="card-header">
                            <h2><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/liveclass-04.png" style="height: 25px; width: 25px;"> <label class="c-red"><?php echo $this->lang->line('livenow'); ?></label></h2>
                            <hr>
                        </div>
                        <div class="card-body" style="margin-top: -2%;">
                            <div class="col-lg-12" id="liveboxclass">                           
                            </div>                      
                        </div>
                    </div>
                </div>

                <div class="card p-l-15 p-r-15" id="utama2">
                    <div class="row">
                        <div class="card-header">
                            <h2><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/comingup-04.png" style="height: 25px; width: 25px;"> <label class="c-bluegray"><?php echo $this->lang->line('comingup'); ?></label></h2>
                            <hr>
                        </div>
                        <div class="card-body" style="margin-top: -2%;">
                            <div class="col-lg-12" id="xboxclass">
                            </div>                    
                        </div>
                    </div>
                </div>

                <div class="card p-l-15 p-r-15" id="utama5">
                    <div class="row">
                        <div class="card-header">
                            <h2><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/missedclass-04.png" style="height: 25px; width: 25px;"> <label class="c-bluegray"><?php echo $this->lang->line('missed'); ?></label></h2>
                            <hr>
                        </div>
                        <div class="card-body" style="margin-top: -2%;">
                            <div class="col-lg-12" id="xboxmissed">                        
                            </div>                    
                        </div>
                    </div>
                </div>

                <div class="card p-l-15 p-r-15 p-t-5 p-b-5" id="utama4" style="display: none;">
                    <div class="row">                
                        <div class="card-header" style="margin-top: 0%;">                        
                            <div class="alert alert-danger text-center f-14" role="alert"><?php echo $this->lang->line('no_class');?>                            
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal CONFRIM -->  
                <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="confrim" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title c-black">Konfirmasi Pembelian</h4>
                                <hr>
                                <p><label class="c-gray f-15">Apakah anda ingin membeli kelas ini ???</label></p>
                                <input type="text" name="classid" id="classid" class="c-black" value="" hidden />
                            </div>                                                
                            <div class="modal-footer">
                                <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                                <a id="proccess" href=""><button type="button" class="btn bgm-green" id="confrimbuy">Ya</button></a>
                            </div>                        
                        </div>
                    </div>
                </div>

                <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="confrimjoinn" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title c-black">Konfirmasi Join</h4>
                                <hr>
                                <p><label class="c-gray f-15">Apakah anda ingin join kelas ini ???</label></p>
                                <input type="text" name="classid" id="classid" class="c-black" value="" hidden />
                            </div>                                                
                            <div class="modal-footer">
                                <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                                <a id="proccessjoin" href=""><button type="button" class="btn bgm-green" id="confrimjoin">Ya</button></a>
                            </div>                        
                        </div>
                    </div>
                </div>

                <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="sharelink_show" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title c-black">Share link your Class</h4>
                                <hr>
                                <p><label class="c-gray f-15">Apakah anda ingin join kelas ini ???</label></p>
                                <input type="text" name="classid" id="classid" class="c-black" value="" hidden />
                            </div>                                                
                            <div class="modal-footer">
                                <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                                <a id="proccessjoin" href=""><button type="button" class="btn bgm-green" id="confrimjoin">Ya</button></a>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function(){

        var schedule_time = [];
        var nowtime = [];
        var intervalTime = [];
        var finished = [];
        var finish_time = [];
        var threadHandler = [];
        var hour = [];
        var minute = [];
        var second = [];
        var idnya = null;
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        console.warn(tokenjwt);
        var user_utc = -1 * new Date().getTimezoneOffset();
        var currentDate = new Date(new Date().getTime());
        var kemarin = new Date(new Date().getTime() -1 * 24 * 60 * 60 * 1000);
        // alert(kemarin);
        var day = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();

        var tgl = new Date();
        var kliktgl = null;
        var iduser = "<?php echo $this->session->userdata('id_user'); ?>";
        alert(iduser);
        
        
        var formattedDate = moment(tgl).format('YYYY-MM-DD');
        var hari = moment(tgl).format('ddd');        
        var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes();  

        var mydate = new Date();
        var str = month + ' ' + tgl.getFullYear();
         //Variable untuk tampilan tanggal
        var today = new Date(tgl.getTime());
        var tomorrow = new Date(tgl.getTime() + 1 * 24 * 60 * 60 * 1000);
        var twodays = new Date(tgl.getTime() + 2 * 24 * 60 * 60 * 1000);
        var threedays = new Date(tgl.getTime() + 3 * 24 * 60 * 60 * 1000);
        var fourdays = new Date(tgl.getTime() + 4 * 24 * 60 * 60 * 1000);
        var fivedays = new Date(tgl.getTime() + 5 * 24 * 60 * 60 * 1000);
        var sixdays = new Date(tgl.getTime() + 6 * 24 * 60 * 60 * 1000);

        //Variable untuk disamakan dengan format database
        var bln_tgl1 = moment(today).format('YYYY-MM-DD');
        var bln_tgl2 = moment(tomorrow).format('YYYY-MM-DD');
        var bln_tgl3 = moment(twodays).format('YYYY-MM-DD');
        var bln_tgl4 = moment(threedays).format('YYYY-MM-DD');
        var bln_tgl5 = moment(fourdays).format('YYYY-MM-DD');
        var bln_tgl6 = moment(fivedays).format('YYYY-MM-DD');
        var bln_tgl7 = moment(sixdays).format('YYYY-MM-DD');

        //Variable untuk tampilan hari
        var hari1 = moment(today).format('ddd');
        var hari2 = moment(tomorrow).format('ddd');
        var hari3 = moment(twodays).format('ddd');
        var hari4 = moment(threedays).format('ddd');
        var hari5 = moment(fourdays).format('ddd');
        var hari6 = moment(fivedays).format('ddd');
        var hari7 = moment(sixdays).format('ddd');

        //Variable untuk tampilan hari
        var bulan1 = moment(today).format('MMM');
        var bulan2 = moment(tomorrow).format('MMM');
        var bulan3 = moment(twodays).format('MMM');
        var bulan4 = moment(threedays).format('MMM');
        var bulan5 = moment(fourdays).format('MMM');
        var bulan6 = moment(fivedays).format('MMM');
        var bulan7 = moment(sixdays).format('MMM');

        //Variable untuk tampilan bulan
        var bulan = moment(currentDate).format('MMMM - YYYY');
        var tglafter1 = new Date(new Date().getTime() + 0 * 24 * 60 * 60 * 1000);
        var tglafter2 = new Date(new Date().getTime() + 1 * 24 * 60 * 60 * 1000);
        var tglafter3 = new Date(new Date().getTime() + 2 * 24 * 60 * 60 * 1000);
        var tglafter4 = new Date(new Date().getTime() + 3 * 24 * 60 * 60 * 1000);
        var tglafter5 = new Date(new Date().getTime() + 4 * 24 * 60 * 60 * 1000);
        var tglafter6 = new Date(new Date().getTime() + 5 * 24 * 60 * 60 * 1000);
        var tglafter7 = new Date(new Date().getTime() + 6 * 24 * 60 * 60 * 1000);

        var $subscribeInput = $('input[name="orderKrnj[]"]');
        $subscribeInput.on('click', function(){
            if ( $(this).is(':checked') )
                $("#btnbayar").removeAttr('disabled');
            else
                $("#btnbayar").attr('disabled','disabled');
        });

        $(function() {
            //Tampilan Bulan di Kalender
            $("#bln_ini").html(bulan);

            //Untuk mencocokan dengan yang ada didatabase
            $("#dt").html(tgl.getFullYear() + '-' + (today.getMonth()+1) + '-' + today.getDate());
            $("#dt2").html(tgl.getFullYear() + '-' + (tomorrow.getMonth()+1) + '-' +(tomorrow.getDate()));
            $("#dt3").html(tgl.getFullYear() + '-' + (twodays.getMonth()+1) + '-' +(twodays.getDate()));
            $("#dt4").html(tgl.getFullYear() + '-' + (threedays.getMonth()+1) + '-' +(threedays.getDate()));
            $("#dt5").html(tgl.getFullYear() + '-' + (fourdays.getMonth()+1) + '-' +(fourdays.getDate()));
            $("#dt6").html(tgl.getFullYear() + '-' + (fivedays.getMonth()+1) + '-' +(fivedays.getDate()));
            $("#dt7").html(tgl.getFullYear() + '-' + (sixdays.getMonth()+1) + '-' +(sixdays.getDate()));

            //Tampilan untuk tanggal di Kalender
            $("#d1").html(hari + ', ' +(today.getDate())  + ' ' +bulan1);
            $("#d2").html(hari2 + ', ' +(tomorrow.getDate())  + ' ' +bulan2);
            $("#d3").html(hari3 + ', ' +(twodays.getDate())  + ' ' +bulan3);
            $("#d4").html(hari4 + ', ' +(threedays.getDate())  + ' ' +bulan4);
            $("#d5").html(hari5 + ', ' +(fourdays.getDate())  + ' ' +bulan5);
            $("#d6").html(hari6 + ', ' +(fivedays.getDate())  + ' ' +bulan6);
            $("#d7").html(hari7 + ', ' +(sixdays.getDate())  + ' ' +bulan7);

            $("#d1").click(function(){

                $("#d1").css("color","yellow");
                $("#d2").css("color","white");
                $("#d3").css("color","white");
                $("#d4").css("color","white");
                $("#d5").css("color","white");
                $("#d6").css("color","white");
                $("#d7").css("color","white");
                var aj = $("#dt").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d3").click(function(){

                $("#d1").css("color","white");
                $("#d2").css("color","white");
                $("#d3").css("color","yellow");
                $("#d4").css("color","white");
                $("#d5").css("color","white");
                $("#d6").css("color","white");
                $("#d7").css("color","white");
                var aj = $("#dt3").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d2").click(function(){

                $("#d1").css("color","white");
                $("#d2").css("color","yellow");
                $("#d3").css("color","white");
                $("#d4").css("color","white");
                $("#d5").css("color","white");
                $("#d6").css("color","white");
                $("#d7").css("color","white");
                var aj = $("#dt2").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d4").click(function(){

                $("#d1").css("color","white");
                $("#d2").css("color","white");
                $("#d3").css("color","white");
                $("#d4").css("color","yellow");
                $("#d5").css("color","white");
                $("#d6").css("color","white");
                $("#d7").css("color","white");
                var aj = $("#dt4").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d5").click(function(){
                $("#d1").css("color","white");
                $("#d2").css("color","white");
                $("#d3").css("color","white");
                $("#d4").css("color","white");
                $("#d5").css("color","yellow");
                $("#d6").css("color","white");
                $("#d7").css("color","white");
                var aj = $("#dt5").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d6").click(function(){
                $("#d1").css("color","white");
                $("#d2").css("color","white");
                $("#d3").css("color","white");
                $("#d4").css("color","white");
                $("#d5").css("color","white");
                $("#d6").css("color","yellow");
                $("#d7").css("color","white");
                var aj = $("#dt6").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
            $("#d7").click(function(){
                $("#d1").css("color","white");
                $("#d2").css("color","white");
                $("#d3").css("color","white");
                $("#d4").css("color","white");
                $("#d5").css("color","white");
                $("#d6").css("color","white");
                $("#d7").css("color","yellow");
                var aj = $("#dt7").text();
                kliktgl = aj;
                // alert(aj);
                if (formattedDate != kliktgl) 
                {                              
                    $('.c').removeClass("active");
                    $(this).addClass("active");
                    // alert(aj);
                    formattedDate = kliktgl;        
                    getschedule(aj);
                }
                else
                {
                    formattedDate = moment(tgl).format('YYYY-MM-DD');
                }
            });
        });
        $("#nextbtn").click(function() { 
            $("#d1").css("color","yellow");
            $("#d2").css("color","white");
            $("#d3").css("color","white");
            $("#d4").css("color","white");
            $("#d5").css("color","white");
            $("#d6").css("color","white");
            $("#d7").css("color","white");
            currentDate.setDate(currentDate.getDate() + 1);
            var tglafter1 = (currentDate.getTime() + 0 * 24 * 60 * 60 * 1000);
            var tglafter2 = (currentDate.getTime() + 1 * 24 * 60 * 60 * 1000);
            var tglafter3 = (currentDate.getTime() + 2 * 24 * 60 * 60 * 1000);
            var tglafter4 = (currentDate.getTime() + 3 * 24 * 60 * 60 * 1000);
            var tglafter5 = (currentDate.getTime() + 4 * 24 * 60 * 60 * 1000);
            var tglafter6 = (currentDate.getTime() + 5 * 24 * 60 * 60 * 1000);
            var tglafter7 = (currentDate.getTime() + 6 * 24 * 60 * 60 * 1000);
            // var testt = moment(tglafter5).format('DD');
            // alert(testt);

            var bulan = moment(currentDate).format('MMMM - YYYY');

            var tgl1 = moment(tglafter1).format('DD');
            var tgl2 = moment(tglafter2).format('DD');
            var tgl3 = moment(tglafter3).format('DD');
            var tgl4 = moment(tglafter4).format('DD');
            var tgl5 = moment(tglafter5).format('DD');
            var tgl6 = moment(tglafter6).format('DD');
            var tgl7 = moment(tglafter7).format('DD');

            var hari1 = moment(tglafter1).format('ddd');
            var hari2 = moment(tglafter2).format('ddd');
            var hari3 = moment(tglafter3).format('ddd');
            var hari4 = moment(tglafter4).format('ddd');
            var hari5 = moment(tglafter5).format('ddd');
            var hari6 = moment(tglafter6).format('ddd');
            var hari7 = moment(tglafter7).format('ddd');

            var bln1 = moment(tglafter1).format('MM');
            var bln2 = moment(tglafter2).format('MM');
            var bln3 = moment(tglafter3).format('MM');
            var bln4 = moment(tglafter4).format('MM');
            var bln5 = moment(tglafter5).format('MM');
            var bln6 = moment(tglafter6).format('MM');
            var bln7 = moment(tglafter7).format('MM');

            var year1 = moment(tglafter1).format('YYYY');
            var year2 = moment(tglafter2).format('YYYY');
            var year3 = moment(tglafter3).format('YYYY');
            var year4 = moment(tglafter4).format('YYYY');
            var year5 = moment(tglafter5).format('YYYY');
            var year6 = moment(tglafter6).format('YYYY');
            var year7 = moment(tglafter7).format('YYYY');


            //Untuk tampilan di kalender
            var bulan1 = moment(tglafter1).format('MMM');
            var bulan2 = moment(tglafter2).format('MMM');
            var bulan3 = moment(tglafter3).format('MMM');
            var bulan4 = moment(tglafter4).format('MMM');
            var bulan5 = moment(tglafter5).format('MMM');
            var bulan6 = moment(tglafter6).format('MMM');
            var bulan7 = moment(tglafter7).format('MMM');


            $("#bln_ini").html(bulan);

            //Untuk mencocokan dengan yang ada didatabase

            $("#dt").html(year1 + '-' + bln1 + '-' + tgl1);
            $("#dt2").html(year2 + '-' + bln2 + '-' + tgl2);
            $("#dt3").html(year3 + '-' + bln3 + '-' + tgl3);
            $("#dt4").html(year4 + '-' + bln4 + '-' + tgl4);
            $("#dt5").html(year5 + '-' + bln5 + '-' + tgl5);
            $("#dt6").html(year6 + '-' + bln6 + '-' + tgl6);
            $("#dt7").html(year7 + '-' + bln7 + '-' + tgl7);

            //Tampilan untuk tanggal di Kalender
            // $("#d1").html(hari + ', ' +(today.getDate())  + ' ' +bulan1);

            
            $("#d1").html(hari1 + ', ' + tgl1 + ' ' + bulan1);
            $("#d2").html(hari2 + ', ' + tgl2 + ' ' + bulan2);
            $("#d3").html(hari3 + ', ' + tgl3 + ' ' + bulan3);
            $("#d4").html(hari4 + ', ' + tgl4 + ' ' + bulan4);
            $("#d5").html(hari5 + ', ' + tgl5 + ' ' + bulan5);
            $("#d6").html(hari6 + ', ' + tgl6 + ' ' + bulan6);
            $("#d7").html(hari7 + ', ' + tgl7 + ' ' + bulan7);
            var aj = $("#dt").text();
            var cekhari = tgl.getDay();
            // alert(aj);
            
            kliktgl = aj;
            if (formattedDate != kliktgl) 
            {                              
                $('.c').removeClass("active");
                $("d1").addClass("active");
                // alert(aj);
                formattedDate = kliktgl;        
                getschedule(aj);
            }
            else
            {
                
                formattedDate = moment(tgl).format('YYYY-MM-DD');
            }
        });
        $("#prevbtn").click(function() {
            $("#d1").css("color","yellow");
            $("#d2").css("color","white");
            $("#d3").css("color","white");
            $("#d4").css("color","white");
            $("#d5").css("color","white");
            $("#d6").css("color","white");
            $("#d7").css("color","white");

            currentDate.setDate(currentDate.getDate() -1);
            var tglafter1 = (currentDate.getTime() + 0 * 24 * 60 * 60 * 1000);
            var tglafter2 = (currentDate.getTime() + 1 * 24 * 60 * 60 * 1000);
            var tglafter3 = (currentDate.getTime() + 2 * 24 * 60 * 60 * 1000);
            var tglafter4 = (currentDate.getTime() + 3 * 24 * 60 * 60 * 1000);
            var tglafter5 = (currentDate.getTime() + 4 * 24 * 60 * 60 * 1000);
            var tglafter6 = (currentDate.getTime() + 5 * 24 * 60 * 60 * 1000);
            var tglafter7 = (currentDate.getTime() + 6 * 24 * 60 * 60 * 1000);

            var bulan = moment(currentDate).format('MMMM - YYYY');

            var tgl1 = moment(tglafter1).format('DD');
            var tgl2 = moment(tglafter2).format('DD');
            var tgl3 = moment(tglafter3).format('DD');
            var tgl4 = moment(tglafter4).format('DD');
            var tgl5 = moment(tglafter5).format('DD');
            var tgl6 = moment(tglafter6).format('DD');
            var tgl7 = moment(tglafter7).format('DD');

            var hari1 = moment(tglafter1).format('ddd');
            var hari2 = moment(tglafter2).format('ddd');
            var hari3 = moment(tglafter3).format('ddd');
            var hari4 = moment(tglafter4).format('ddd');
            var hari5 = moment(tglafter5).format('ddd');
            var hari6 = moment(tglafter6).format('ddd');
            var hari7 = moment(tglafter7).format('ddd');

            var bln1 = moment(tglafter1).format('MM');
            var bln2 = moment(tglafter2).format('MM');
            var bln3 = moment(tglafter3).format('MM');
            var bln4 = moment(tglafter4).format('MM');
            var bln5 = moment(tglafter5).format('MM');
            var bln6 = moment(tglafter6).format('MM');
            var bln7 = moment(tglafter7).format('MM');

            var year1 = moment(tglafter1).format('YYYY');
            var year2 = moment(tglafter2).format('YYYY');
            var year3 = moment(tglafter3).format('YYYY');
            var year4 = moment(tglafter4).format('YYYY');
            var year5 = moment(tglafter5).format('YYYY');
            var year6 = moment(tglafter6).format('YYYY');
            var year7 = moment(tglafter7).format('YYYY');


            //Untuk tampilan di kalender
            var bulan1 = moment(today).format('MMM');
            var bulan2 = moment(tomorrow).format('MMM');
            var bulan3 = moment(twodays).format('MMM');
            var bulan4 = moment(threedays).format('MMM');
            var bulan5 = moment(fourdays).format('MMM');
            var bulan6 = moment(fivedays).format('MMM');
            var bulan7 = moment(sixdays).format('MMM');


            $("#bln_ini").html(bulan);

            //Untuk mencocokan dengan yang ada didatabase

            $("#dt").html(year1 + '-' + bln1 + '-' + tgl1);
            $("#dt2").html(year2 + '-' + bln2 + '-' + tgl2);
            $("#dt3").html(year3 + '-' + bln3 + '-' + tgl3);
            $("#dt4").html(year4 + '-' + bln4 + '-' + tgl4);
            $("#dt5").html(year5 + '-' + bln5 + '-' + tgl5);
            $("#dt6").html(year6 + '-' + bln6 + '-' + tgl6);
            $("#dt7").html(year7 + '-' + bln7 + '-' + tgl7);

            //Tampilan untuk tanggal di Kalender
            // $("#d1").html(hari + ', ' +(today.getDate())  + ' ' +bulan1);

            $("#d1").addClass("active");
            $("#d1").html(hari1 + ', ' + tgl1 + ' ' + bulan1);
            $("#d2").html(hari2 + ', ' + tgl2 + ' ' + bulan2);
            $("#d3").html(hari3 + ', ' + tgl3 + ' ' + bulan3);
            $("#d4").html(hari4 + ', ' + tgl4 + ' ' + bulan4);
            $("#d5").html(hari5 + ', ' + tgl5 + ' ' + bulan5);
            $("#d6").html(hari6 + ', ' + tgl6 + ' ' + bulan6);
            $("#d7").html(hari7 + ', ' + tgl7 + ' ' + bulan7);
            var aj = $("#dt").text();
            var cekhari = tgl.getDay();
            // alert(aj);
            
            kliktgl = aj;
            if (formattedDate != kliktgl) 
            {                              
                $('.c').removeClass("active");
                $("#d1").addClass("active");
                // alert(aj);
                formattedDate = kliktgl;        
                getschedule(aj);
            }
            else
            {
                formattedDate = moment(tgl).format('YYYY-MM-DD');
            }
        });

        var $subscribeInput = $('input[name="orderKrnj[]"]');

        $subscribeInput.on('click', function(){
            if ( $(this).is(':checked') )
                $("#btnbayar").removeAttr('disabled');
            else
                $("#btnbayar").attr('disabled','disabled');
        });
        
        getschedule(formattedDate);

        function timeToSeconds(time) {
            time = time.split(/:/);
            return time[0] * 3600 + time[1] * 60;
        }  

        function dateTimeToSeconds(datetime){
            dt = datetime.split(" ");
            date = dt[0].split("-");
            time = dt[1].split(":");
            return date[0];
            // return JSON.stringify(dt);
        } 

        $(document).on("click", ".datacheck", function () {
            var myBookId = $(this).data('id');
            var ceklink = $(this).data('link');
            $("#proccess").attr("href", ceklink+myBookId+"?t=kids&child_id="+child_id);
            $(".modal-header #classid").val( myBookId );
        });

        $(document).on("click", ".datacheckk", function () {
            var myBookId = $(this).data('id');
            var ceklink = $(this).data('link');
            $("#proccessjoin").attr("href", ceklink+myBookId+"?t=kids&child_id="+child_id);
            $(".modal-header #classid").val( myBookId );
        });

        $(".c").click(function(){
            var aj = $(this).attr('tglskrng');
            var ck = $(this).attr('hariini');
            kliktgl = aj;
            if (formattedDate != kliktgl) 
            {                              
                $('.c').removeClass("active");
                $(this).addClass("active");
                formattedDate = kliktgl;        
                getschedule(aj);
            }
            else
            {
                formattedDate = moment(tgl).format('YYYY-MM-DD');
            }
            
        });
        // getschedule(formattedDate);

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        function getschedule(aj)
        {
            $("#liveboxclass").empty();
            $("#xboxclass").empty();
            $("#xboxrecommended").empty();
            $("#xboxmissed").empty();
            var user_utc = new Date().getTimezoneOffset();
            user_utc = -1 * user_utc;
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/scheduleone/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    id_user: iduser,
                    user_utc: user_utc,
                    user_device: 'web',
                    date: formattedDate
                },
                success: function(response)
                {   
                    var a = JSON.stringify(response);
                    var jsonPretty = JSON.stringify(JSON.parse(a),null,2);                      
                    // $("#aaa").text(jsonPretty);  
                    // alert(response['data'][1]['class_id']);

                    if (response['data'] == null) 
                    {                                                
                        $("#utama1").css('display','none');
                        $("#utama2").css('display','none');
                        $("#utama3").css('display','none');
                        $("#utama5").css('display','none');
                        $("#utama4").css('display','block');
                    }                   
                    else
                    {
                        $("#utama4").css('display','none');
                        $("#utama1").css('display','block');
                        $("#utama3").css('display','block');
                        $("#utama5").css('display','block');
                        $("#utama2").css('display','block');
                        if (response.data.length != null) {
                            for (var i = response.data.length-1; i >=0;i--) {

                                var visible = response['data'][i]['participant']['visible'];
                                var idclass = response['data'][i]['class_id'];
                                var sbjname = response['data'][i]['subject_name'];
                                var desname = response['data'][i]['description'];
                                var desfull = response['data'][i]['description'];
                                var ttrname = response['data'][i]['first_name'];
                                var datee   = response['data'][i]['date'];
                                var enddatee= response['data'][i]['enddate'];
                                datee       = moment(datee).format('DD-MM-YYYY');
                                enddatee    = moment(enddatee).format('DD-MM-YYYY');
                                var time    = response['data'][i]['time'];
                                var endtime = response['data'][i]['endtime'];
                                var timeclass    = response['data'][i]['date']+" "+response['data'][i]['time'];
                                var endtimeclass = response['data'][i]['enddate']+" "+response['data'][i]['endtime'];
                                var usrimg  = response['data'][i]['user_image']+'?'+new Date().getTime();                
                                var hrgkls  = response['data'][i]['harga'];
                                var templatetype  = response['data'][i]['template_type'];
                                var classtype  = response['data'][i]['class_type'];
                                var country = response['data'][i]['country'];
                                desname = desname.substring(0, 33);
                                
                                
                                if (classtype == "multicast") 
                                {
                                	var participant_listt = response['data'][i]['participant']['participant'];
                                    	var im_exist = false;
                                    	if(participant_listt != null){
                                    		for (var iai = 0; iai < participant_listt.length ;iai++) {

	                                    		var a = moment(tgl).format('YYYY-MM-DD');
	                                    		if (aj == null) {
	                                    			if (iduser == participant_listt[iai]['id_user']) {
		                                    			im_exist = true;
	                                    			}
	                                    		}
	                                    		else
	                                    		{
	                                    			if (iduser == participant_listt[iai]['id_user']) {
		                                    			im_exist = true;
	                                    			}
	                                    		}
	                                    	}
                                    	}
                                    	
                                        //KOTAK DESIGN 
                                        var kotak_comingmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2></a></div></a></div>";
                                        var kotak_livemulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2></a></div></a></div>";
                                        var kotak_missedmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2></div></a></div>";

                                        var kotak_join_comingmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer;'><a data-id='"+idclass+"' href='#confrimjoinn' data-toggle='modal' class='datacheckk' data-link='<?php echo base_url('Transaction/multicast_join'); ?>/'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><span class='card-header'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px; cursor:pointer;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2><button class='btn btn-primary btn-block btn-sm m-t-5'>Join</button></span></div></a></div>";
                                        var kotak_join_livemulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer;'><a data-id='"+idclass+"' href='#confrimjoinn' data-toggle='modal' class='datacheckk' data-link='<?php echo base_url('Transaction/multicast_join'); ?>/'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><span class='card-header'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px; cursor:pointer;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2><button class='btn btn-primary btn-block btn-sm m-t-5'>Join</button></span></div></a></div>";
                                        var kotak_join_missedmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2></div></a></div>";

                                    	if(im_exist){
                                    		if (Date.parse(datenow) < Date.parse(timeclass)) 
                                    		{                                  				
	                                            $("#xboxclass").append(kotak_comingmulticast);
                                			}
                                			else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                				
	                                            $("#liveboxclass").append(kotak_livemulticast);
                                			}
                                			else
                                			{                                				
	                                            $("#xboxmissed").append(kotak_missedmulticast);
                                			}
                                    	}else{

                                    		if (Date.parse(datenow) < Date.parse(timeclass)) 
                                    		{                                  			
                                				$("#xboxclass").append(kotak_join_comingmulticast);
                                			}
                                			else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                				$("#liveboxclass").append(kotak_join_livemulticast);
                                			}
                                			else
                                			{
                                				$("#xboxmissed").append(kotak_join_missedmulticast);
                                			}
                                    	}
                                }
                                else if (classtype == "multicast_channel_paid") 
                                {
                                	var participant_listt = response['data'][i]['participant']['participant'];
                                    	var im_exist = false;
                                    	if(participant_listt != null){
                                    		for (var iai = 0; iai < participant_listt.length ;iai++) {
                                    			// alert(participant_listt[iai]['id_user']);
	                                    		var a = moment(tgl).format('YYYY-MM-DD');
	                                    		if (aj == null) {
	                                    			if (iduser == participant_listt[iai]['id_user']) {
		                                    			im_exist = true;
	                                    			}
	                                    		}
	                                    		else
	                                    		{
	                                    			if (iduser == participant_listt[iai]['id_user']) {
		                                    			im_exist = true;
	                                    			}
	                                    		}
	                                    	}
                                    	}
                                    	
                                        //KOTAK DESIGN 
                                        var kotak_comingmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2></a></div></a></div>";
                                        var kotak_livemulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2></a></div></a></div>";
                                        var kotak_missedmulticast = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height:20px; width:35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2></div></a></div>";
                                        

                                    	if(im_exist){
                                    		if (Date.parse(datenow) < Date.parse(timeclass)) 
                                    		{                                  				
	                                            $("#xboxclass").append(kotak_comingmulticast);
                                			}
                                			else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                				
	                                            $("#liveboxclass").append(kotak_livemulticast);
                                			}
                                			else
                                			{                                				
	                                            $("#xboxmissed").append(kotak_missedmulticast);
                                			}
                                    	}
                                }
                                else if (classtype == "multicast_paid") 
                                {
                                    var participant_list = response['data'][i]['participant']['participant'];                                                                    

                                    var number_string = hrgkls.toString(),
                                        sisa    = number_string.length % 3,
                                        rupiah  = number_string.substr(0, sisa),
                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                            
                                    if (ribuan) {
                                        separator = sisa ? '.' : '';
                                        rupiah += separator + ribuan.join('.');
                                    } 
                                    var kotak_buy_comingmulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer;'><a data-id='"+idclass+"' href='#confrim' data-toggle='modal' class='datacheck' data-link='<?php echo base_url('Transaction/multicast_join'); ?>/'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><label style='z-index:1; position:absolute; font-size:26px; color:white; top:23%; left:15%; bottom:23%; cursor:pointer;'>Rp. "+rupiah+"</label><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><span class='card-header'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px; cursor:pointer;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2><button class='btn btn-primary btn-block btn-sm m-t-5'>Beli</button></span></div></a></div>";
                                    var kotak_buy_livemulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:pointer;'><a data-id='"+idclass+"' href='#confrim' data-toggle='modal' class='datacheck' data-link='<?php echo base_url('Transaction/multicast_join'); ?>/'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><label style='z-index:1; position:absolute; font-size:26px; color:white; top:23%; left:15%; bottom:23%; cursor:pointer;'>Rp. "+rupiah+"</label><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><span class='card-header'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px; cursor:pointer;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2><button class='btn btn-primary btn-block btn-sm m-t-5'>Beli</button></span></div></a></div>";
                                    var kotak_buy_missedmulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><label style='z-index:1; position:absolute; font-size:26px; color:white; top:23%; left:15%; bottom:23%;'>Rp. "+rupiah+"</label><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2></div></a></div>";

                                    // alert(participant_list);  
                                    if (participant_list!=null) {
                                        var gueAda = false;
                                        var a = moment(tgl).format('YYYY-MM-DD'); 
                                            
                                        var number_string = hrgkls.toString(),
                                            sisa    = number_string.length % 3,
                                            rupiah  = number_string.substr(0, sisa),
                                            ribuan  = number_string.substr(sisa).match(/\d{3}/g);

                                        if (ribuan) {
                                            separator = sisa ? '.' : '';
                                            rupiah += separator + ribuan.join('.');
                                        }  

                                        var kotak_comingmulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-17px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2></a></div></a></div>"; 
                                        var kotak_livemulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"'><a href='#'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-blue m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-15px;'>"+ttrname+"</small><small style='margin-top:-6px;'>Harga : "+hrgkls+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2></a></div></a></div>";
                                        var kotak_missedmulticastpaid = "<div class='me_timer col-sm-6 col-md-3 col-xs-12' start_time='"+time+"' finish_time='"+endtime+"' style='cursor:default;'><div class='card picture-list'><div class='card blog-post' style='height:250px;'><img class='lv-img-sm' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-gray m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small style='margin-top:-6px;'>"+ttrname+"</small><small style='margin-top:-1px;'>"+timeclass+"  -  "+endtimeclass+"</small></h2></div></a></div>";
                                        
                                        for (var iai = 0; iai < participant_list.length ;iai++) {                                            
                                            
                                             
                                            if (aj == null) {                                             
                                                if (iduser == participant_list[iai]['id_user']) {                           
                                                    var number_string = hrgkls.toString(),
                                                        sisa    = number_string.length % 3,
                                                        rupiah  = number_string.substr(0, sisa),
                                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                                            
                                                    if (ribuan) {
                                                        separator = sisa ? '.' : '';
                                                        rupiah += separator + ribuan.join('.');
                                                    }                                      
                                                    
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        $("#xboxclass").append(kotak_comingmulticastpaid);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        
                                                        $("#liveboxclass").append(kotak_livemulticastpaid);
                                                    }
                                                    else
                                                    {
                                                        $("#xboxmissed").append(kotak_missedmulticastpaid);
                                                    }
                                                }
                                                else
                                                {
                                                    var number_string = hrgkls.toString(),
                                                        sisa    = number_string.length % 3,
                                                        rupiah  = number_string.substr(0, sisa),
                                                        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                                            
                                                    if (ribuan) {
                                                        separator = sisa ? '.' : '';
                                                        rupiah += separator + ribuan.join('.');
                                                    }                                      

                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {                                                        
                                                        $("#xboxclass").append(kotak_buy_comingmulticastpaid);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        $("#liveboxclass").append(kotak_buy_livemulticastpaid);
                                                    }
                                                    else
                                                    {
                                                        $("#xboxmissed").append(kotak_buy_missedmulticastpaid);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var a = moment(tgl).format('YYYY-MM-DD');

                                                if (iduser == participant_list[iai]['id_user']) {
                                                    gueAda = true;
                                                }
                                            }
                                        }
                                        if(gueAda) {
                                            if (aj == a) 
                                            {                                                                                                                                 
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {                                                            
                                                    $("#xboxclass").append(kotak_comingmulticastpaid);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    $("#liveboxclass").append(kotak_livemulticastpaid);
                                                }
                                                else
                                                {                                                            
                                                    $("#xboxmissed").append(kotak_missedmulticastpaid);
                                                }
                                            }
                                            else
                                            {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {                                                            
                                                    $("#xboxclass").append(kotak_comingmulticastpaid);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    $("#liveboxclass").append(kotak_livemulticastpaid);
                                                }
                                                else
                                                {
                                                    $("#xboxmissed").append(kotak_missedmulticastpaid);
                                                }
                                            }
                                        }else{
                                            var number_string = hrgkls.toString(),
                                                sisa    = number_string.length % 3,
                                                rupiah  = number_string.substr(0, sisa),
                                                ribuan  = number_string.substr(sisa).match(/\d{3}/g);

                                            if (ribuan) {
                                                separator = sisa ? '.' : '';
                                                rupiah += separator + ribuan.join('.');
                                            }                                      

                                            if (Date.parse(datenow) < Date.parse(timeclass)) 
                                            {                                                        
                                                $("#xboxclass").append(kotak_buy_comingmulticastpaid);
                                            }
                                            else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                $("#liveboxclass").append(kotak_buy_livemulticastpaid);
                                            }
                                            else
                                            {
                                                $("#xboxmissed").append(kotak_buy_missedmulticastpaid);
                                            }
                                        }
                                    } 
                                    else
                                    {                                        
                                        var number_string = hrgkls.toString(),
                                            sisa    = number_string.length % 3,
                                            rupiah  = number_string.substr(0, sisa),
                                            ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                                
                                        if (ribuan) {
                                            separator = sisa ? '.' : '';
                                            rupiah += separator + ribuan.join('.');
                                        }                                                                                                                

                                        if (Date.parse(datenow) < Date.parse(timeclass)) 
                                        {                                            
                                            $("#xboxclass").append(kotak_buy_comingmulticastpaid);
                                        }
                                        else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                            $("#liveboxclass").append(kotak_buy_livemulticastpaid);
                                        }
                                        else
                                        {                                                     
                                            $("#xboxmissed").append(kotak_buy_missedmulticastpaid);
                                        }
                                    }                                 
                                }
                                else if (classtype == "private") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];

                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);                                    
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {

                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></a></div></a></div>";
                                                    $("#xboxclass").append(kotakprivate);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></a></div></a></div>";
                                                    $("#liveboxclass").append(kotakprivate);
                                                }
                                                else
                                                {
                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></div></a></div>";
                                                    $("#xboxmissed").append(kotakprivate);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {

                                                var kotak_comingprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></a></div></a></div>";
                                                var kotak_liveprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></a></div></a></div>";
                                                var kotak_missedprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></div></a></div>";

                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {                                                         
                                                        $("#xboxclass").append(kotak_comingprivate);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        
                                                        $("#liveboxclass").append(kotak_liveprivate);
                                                    }
                                                    else
                                                    {                                                        
                                                        $("#xboxmissed").append(kotak_missedprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
	                                                {	                                                     
	                                                    $("#xboxclass").append(kotak_comingprivate);
	                                                }
	                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
	                                                    $("#liveboxclass").append(kotak_liveprivate);
	                                                }
	                                                else
	                                                {	                                                   
	                                                    $("#xboxmissed").append(kotak_missedprivate);
	                                                }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "private_channel") 
                                {                                    
                                    var participant = response['data'][i]['participant']['participant'];                                    

                                    var aca = JSON.stringify(participant);                                    
                                    var jsa = JSON.parse(aca); 
                                                                     
                                    // alert(jsa[0]['id_user']);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');                                        
                                        if (aj == null) {
                                        	alert('a');
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {

                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></a></div></a></div>";
                                                    $("#xboxclass").append(kotakprivate);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></a></div></a></div>";
                                                    $("#liveboxclass").append(kotakprivate);
                                                }
                                                else
                                                {
                                                    var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></div></a></div>";
                                                    $("#xboxmissed").append(kotakprivate);
                                                }
                                            }
                                        }
                                        else
                                        {                                        	
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {

                                                var kotak_comingprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></a></div></a></div>";
                                                var kotak_liveprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></a></div></a></div>";
                                                var kotak_missedprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Private Class</small></h2></div></a></div>";

                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {                                                         
                                                        $("#xboxclass").append(kotak_comingprivate);
                                                    }
                                                    else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                        
                                                        $("#liveboxclass").append(kotak_liveprivate);
                                                    }
                                                    else
                                                    {                                                        
                                                        $("#xboxmissed").append(kotak_missedprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
	                                                {	                                                     
	                                                    $("#xboxclass").append(kotak_comingprivate);
	                                                }
	                                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
	                                                    $("#liveboxclass").append(kotak_liveprivate);
	                                                }
	                                                else
	                                                {	                                                   
	                                                    $("#xboxmissed").append(kotak_missedprivate);
	                                                }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "group") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];
                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                     var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></a></div>";
                                                     $("#xboxclass").append(kotakgroup);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) || Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></a></div>";
                                                    $("#liveboxclass").append(kotakgroup);
                                                }
                                                else
                                                {
                                                    var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></div></a></div>";
                                                    $("#xboxmissed").append(kotakgroup);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></a></div>";
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></div></a></div>";
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></div></a></div>";
                                                        $("#xboxmissed").append(kotakprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></a></div>";
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></div></a></div>";
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></div></a></div>";
                                                        $("#xboxmissed").append(kotakprivate);
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }
                                else if (classtype == "group_channel") 
                                {
                                    var participant = response['data'][i]['participant']['participant'];
                                    var aca = JSON.stringify(participant);
                                    var jsa = JSON.parse(aca);
                                    for (var iai = 0; iai < jsa.length ;iai++) {
                                        // $("#bbb").append(jsa[iai]['id_user']+"<br>");
                                        var a = moment(tgl).format('YYYY-MM-DD');
                                        if (aj == null) {
                                            // alert(aj);
                                            // alert(formattedDate);                               
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                {
                                                     var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></a></div>";
                                                     $("#xboxclass").append(kotakgroup);
                                                }
                                                else if (Date.parse(timeclass) <= Date.parse(datenow) || Date.parse(datenow) < Date.parse(endtimeclass)) {
                                                    var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></a></div>";
                                                    $("#liveboxclass").append(kotakgroup);
                                                }
                                                else
                                                {
                                                    var kotakgroup = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-orange' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></div></a></div>";
                                                    $("#xboxmissed").append(kotakgroup);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var a = moment(tgl).format('YYYY-MM-DD');
                                            if (iduser == jsa[iai]['id_user']) {
                                                if (aj == a) 
                                                {  
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></a></div>";
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></div></a></div>";
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></div></a></div>";
                                                        $("#xboxmissed").append(kotakprivate);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Date.parse(datenow) < Date.parse(timeclass)) 
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></a></div>";
                                                        $("#xboxclass").append(kotakprivate);
                                                    }
                                                    else if (Date.parse(timeclass) >= Date.parse(datenow) || Date.parse(datenow) <= Date.parse(endtimeclass)) {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><a href='#'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"' href='#'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></a></div></div></a></div>";
                                                        $("#liveboxclass").append(kotakprivate);
                                                    }
                                                    else
                                                    {
                                                        var kotakprivate = "<div class='col-sm-6 col-md-3 col-xs-12'><div class='card picture-list'><div class='card blog-post bgm-blue' style='height:250px;'><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='filter: grayscale(100%); z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+usrimg+"' width='100%' height='150px' style='filter: grayscale(100%);'><a class='card-header c-white' data-toggle='tooltip' data-placement='top' title='"+desfull+"'><h5 class='c-white m-l-10' style='height:30px; margin-top:-7px;'>"+sbjname+" - "+desname+"</h5><h2 class='m-l-10'><small class='c-white' style='margin-top:-3px;'>"+ttrname+"</small><small class='c-white'  style='margin-top:-6px;'>"+timeclass+"  -  "+endtimeclass+" | Group Class</small></h2></div></a></div>";
                                                        $("#xboxmissed").append(kotakprivate);
                                                    }
                                                }
                                            }
                                        }                               
                                    }                           
                                }                                                                                            
                                
                            }
                        } 
                    }      
                }
            });
        }  


    });
</script>                               