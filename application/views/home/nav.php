<div class="container container-full" id="home" style="z-index: 2;">
    <div class="navbar-header" style="">
        <a class="navbar-brand" data-scroll href="<?php echo base_url(); ?>">        
            <img src="https://indiclass.fisipro.com/assets/img/logos.png" style="height: 40px; width: 150px; margin-top: -2%;" alt="">
        </a>
    </div>
    <div>
        <ul  class="nav navbar-nav navbar-right" style="">
            <a id="logofb" style="margin-right:320px;" href="https://www.facebook.com/telkomindiclass/" target="_blank" class="btn-navbar-menu btn-circle btn-facebook color-white animated zoomInDown animation-delay-7">
                <i class="zmdi zmdi-facebook"></i>
            </a>
            <a id="logotwitter" style="margin-right: 260px;" href="https://twitter.com/telkomindiclass" target="_blank" class="btn-navbar-menu btn-circle btn-circle-primary color-white animated zoomInDown animation-delay-7">
                <i class="zmdi zmdi-twitter"></i>
            </a>
            <a id="nav_login" href="#" data-toggle="modal"  data-target ="#login_modal" class="btn btn-whatsapp animated zoomInDown animation-delay-10 " style="margin-top: 7px; color: #ffffff; font-size: 13px; ">
                <?php echo $this->lang->line('navbartop2')?>
            </a>
            <a href="<?php echo base_url(); ?>Register" class="btn btn-vimeo animated zoomInDown animation-delay-10 " style="margin-top: 7px; color: #ffffff; font-size: 13px; ">
                <?php echo $this->lang->line('navbartop1')?>
            </a>
        </ul>
    </div>
</div>