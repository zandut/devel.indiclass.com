<div id="royal_preloader"></div>	

<!-- Begin Boxed or Fullwidth Layout -->
<div id="bg-boxed">
    <div class="boxed">

		<!-- Begin Header -->
		<header>
			<?php $this->load->view('home/top_nav');?>    
		</header><!-- /header -->
		<!-- End Header -->

		<!-- Begin Recent Work + Clients -->
		<div class="container mt40 mb40">
			<div class="row">
				<div class="col-sm-12">
					<div class="heading mb30">
						<h4><span class="fa fa-user-circle-o mr15"></span>Pengajar</h4>
		                <div class="owl-controls">
		                    <div id="customNav" class="owl-nav"></div>
		                </div>
					</div>
					<div class="tab-content tab-shop mt15">
						<div id="kotak_tutor" class="tab-pane row fade in active">							
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Recent Work + Clients -->

		<!-- Begin Footer -->
		<footer class="footer-light">
		    <?php $this->load->view('home/bottom');?>
		</footer><!-- /footer -->
		<!-- End Footer --> 

	</div><!-- /boxed -->
</div><!-- /bg boxed-->
<!-- End Boxed or Fullwidth Layout -->

<script type="text/javascript">

	$(document).ready(function() {
		
		var boxClass = "";

    	$.ajax({
            url: '<?php echo AIR_API;?>tutorList',
            type: 'POST',
            data: {      
            	channel_id : 52,         
                device: 'web'
            },            
            success: function(data)
            {         
            	var a = JSON.stringify(data);
            	// alert(a);
            	            
                if (data['status'] == true) 
                {                       
                    for (var i = 0; i < data.data.length; i++) {
                        var user_image        	= data['data'][i]['user_image'];
                        var user_name        	= data['data'][i]['user_name'];
                        var self_description    = data['data'][i]['self_description'];
                        var tutor_image 		="https://cdn.classmiles.com/usercontent/"+user_image;
                        boxClass	+= "<div class='col-lg-3 col-md-4 col-sm-6 mb30'>"+
							"<div class='view image-hover-1 no-margin'>"+
								"<div class='product-container'>"+
									"<img class='img-responsive full-width' src='"+tutor_image+"' style='width:100%; height:250px;' alt='...'>"+
								"</div>"+
						        "<div class='mask'>"+							        
								"</div>"+
							"</div>"+
							"<div class='shop-product content-box-shadow' style='height:70px;'>"+
								"<a href='#'><h2>"+user_name+"</h2></a>"+								
							"</div>"+
						"</div>";
                    }

                    $("#kotak_tutor").append(boxClass);
                }
         
            }
        });

    });

</script>