<!-- Begin Top Bar -->
<div class="top-bar">
	<div class="container">
		<div class="row">
			<!-- Address and Phone -->
			<div class="col-sm-7 hidden-xs">
				Indiclass
			</div>
			<!-- Social Buttons -->
			<div class="col-sm-5 text-right">
                <ul class="topbar-list list-inline">
	                <li>						
			            <a class="btn btn-social-icon btn-rw btn-primary btn-xs">
							<i class="fa fa-twitter"></i>
						</a>
						<a class="btn btn-social-icon btn-rw btn-primary btn-xs">
							<i class="fa fa-instagram"></i>
						</a>
						<a class="btn btn-social-icon btn-rw btn-primary btn-xs">
							<i class="fa fa-facebook"></i>
						</a>
					</li><li><a data-toggle="modal" data-target="#login">Masuk</a></li><li><a href="<?php echo BASE_URL();?>Daftar">Daftar</a></li>
				</ul>
			</div>
		</div><!--/row --> 
	</div><!--/container header -->   
</div><!--/top bar -->   
<!-- End Top Bar -->

<!-- Login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="loginLabel">Masuk</h4>
            </div>
            <div class="modal-body">
                <label id="alertWrong" style="color: red; display: none;">Silahkan cek Username atau Password anda !!!</label>
	            <form role="form">
		            <div class="form-group">
		                <label for="exampleInputEmail1">Email</label>
		                <input type="email" class="form-control" id="email_login" placeholder="Masukan Email">
		                <label style="color: red; float: left; display: none;" id="alert_email">Email tidak boleh kosong</label>
		            </div>
		            <div class="form-group">
		                <label for="exampleInputPassword1">Kata Sandi</label>
		                <input type="password" class="form-control" id="password_login" placeholder="Kata Sandi">
		                <label style="color: red; float: left; display: none;" id="alert_email">Kata sandi tidak boleh kosong</label>
		            </div>
		        </form><!-- /form -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-rw btn-primary" id="btnlogin">Masuk</button>
            </div>
        </div><!-- /modal content -->
    </div><!-- /modal dialog -->
</div><!-- /modal holder -->
<!-- End Login -->

<!-- Begin Navigation -->
	<?php $this->load->view('home/navbar');?>
<!-- End Navigation -->

<script type="text/javascript">
    $(document).ready(function() {

        var fType = null;
        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
            function(m,key,value) {
              vars[key] = value;
            });
            return vars;
        };

        $(document.body).on('click', '#btnlogin' ,function(e){
            var email = $('#email_login').val();
            var password = $('#password_login').val();
            if (email=="") {
                $("#alert_password").css('display','none');
                $("#alert_email").css('display','block');
            }
            else if(password == "")
            {
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','block');                
            }
            else
            {            
                $("#btnlogin").attr('disabled','true');
                var link = null;
                fType = getUrlVars()["tlnk"];
                if (fType != undefined) {
                    link = '<?php echo BASE_URL();?>Student';
                }
                else
                {
                    // link = '<?php echo BASE_URL();?>Student/MyClass';
                }
                $.ajax({
                    url: 'https://classmiles.com/api/v1/login',
                    type: 'POST',
                    data: {
                        email: email,
                        password : password,
                        channel_id : '52'
                    },
                    success: function(response)
                    {                        
                        if (response['response']['status'] == true) {
                            $("#login").modal("hide");
                            id_user = response['response']['data']['id_user'];
                            user_name = response['response']['data']['user_name'];
                            user_image = response['response']['data']['user_image'];
                            email = response['response']['data']['email'];
                            usertype_id = response['response']['data']['usertype_id'];
                            status = response['response']['data']['status'];
                            channel_id = response['response']['data']['channel_id'];
                            access_token = response['response']['access_token']; 
                            if (usertype_id == 'student') {
                                member_ship = response['response']['data']['member_ship'];    
                            }
                            else
                            {
                                member_ship = "";
                            }                            
                            if (usertype_id == 'student') {
                                $.ajax({
                                    url: '<?php echo BASE_URL();?>First/setSession',
                                    type: 'POST',
                                    data: {
                                        id_user: id_user,
                                        user_name: user_name,
                                        user_image: user_image,
                                        email: email,
                                        usertype_id: usertype_id,
                                        status: status,
                                        channel_id: channel_id,
                                        access_token: access_token,
                                        member_ship: member_ship
                                    },
                                    success: function(response)
                                    {
                                        $("#alertWrong").css('display','none');
                                        var link = '<?php echo BASE_URL();?>Siswa/KelasSaya';
                                        window.location.replace(link);
                                    }
                                });
                            }   
                            else if (usertype_id == 'tutor') {
                                $.ajax({
                                    url: '<?php echo BASE_URL();?>First/setSession',
                                    type: 'POST',
                                    data: {
                                        id_user: id_user,
                                        user_name: user_name,
                                        user_image: user_image,
                                        email: email,
                                        usertype_id: usertype_id,
                                        status: status,
                                        channel_id: channel_id,
                                        access_token: access_token,
                                        member_ship: member_ship
                                    },
                                    success: function(response)
                                    {
                                        $("#alertWrong").css('display','none');
                                        var link = '<?php echo BASE_URL();?>Tutor/KelasSaya';
                                        window.location.replace(link);
                                    }
                                });
                            }
                            else
                            {                       
                                $("#alertWrong").css('display','block');
                                setTimeout(function(){
                                    location.reload();
                                },3000);
                            }
                            
                        }
                        else
                        {
                            $("#alertWrong").css('display','block');
                            $("#btnlogin").removeAttr('disabled');                            
                        }
                    }
                }); 
            }            
        });

    });
</script>