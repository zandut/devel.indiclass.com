<html lang="en">
	<head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/preload.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/web.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/plugins.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/style.light-blue-500.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/width-boxed.min.css" id="ms-boxed" disabled="">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
	</head>
  <div class="" style="">
      <nav class="navbar ms-lead-navbar navbar-mode" id="nav2" style="z-index: 2; background-color: #FF00FF;; padding: 0;">
          <?php $this->load->view('home/nav.php'); ?>
      </nav> 
  </div>
  <div class="modal modal-dark" id="desc_package_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
          	<input type="text" name="id_paket" id="txt_id_paket" hidden>
              <div class="modal-header shadow-2dp no-pb" style="background-color: #008080;">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                 <span aria-hidden="true">
                 <i class="zmdi zmdi-close"></i>
                 </span>
                 </button>
                 <div class="modal-title text-center">
                     <h2 class="no-m ms-site-title" ><b>Paket Classmiles</b></h2>
                 </div>
              </div>
              <div class="modal-body" style="color: black">
              	<span class="ms-tag ms-tag-success" id="txt_price" style="display: none; margin-top: 5px; margin-left: 5px; position: absolute;"></span>  
          		<img id="txt_poster" src="" style="width: 100%;">
          		<h2 class="color-success"><b  id="txt_name"></b></h2>
				<label class="" style="color: black; margin-top: 2px; font-size: 14px" id="txt_desc"></label>
                 	<div class="col-md-6">Bidang :
                 		<ul id="list_bidang"></ul>
                 	</div>
                 	<div class="col-md-6">Fasilitas :
                 		<ul id="list_fasilitas"></ul>
                 	</div>
             </div>
              <div class="modal-footer">
                <button type="button" class="btn_choose_packagee btn btn-raised btn-success" data-list_id=""><i class="zmdi zmdi-shopping-cart"></i>Beli Paket</button>
            </div>
         </div>
     </div>
 	</div>
 	<div class="modal modal-dark" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div class="modal-header shadow-2dp no-pb" style="background-color: #ed222a;">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                 <span aria-hidden="true">
                 <i class="zmdi zmdi-close"></i>
                 </span>
                 </button>
                 <div class="modal-title text-center">
                     <h2 class="no-m ms-site-title"><?php echo $this->lang->line('wlogin'); ?><b>&nbsp;Indiclass</b></h2>
                 </div>
              </div>
              <div class="modal-body">
                 <div class="tab-content">
                     <div role="tabpanel" class="tab-pane fade active in" id="ms-login-tab">

                         	<!-- <form role="form" action="<?php echo base_url('Master/aksi_login'); ?>" method="post"> -->
                         	<input  type="text" id="channel_id"  name="channel_id" class="" style="display: none;" >
                              <?php if($this->session->flashdata('mes_alert')){ ?>
                              <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->session->flashdata('mes_message'); ?>
                              </div>
                              <?php }?>
                              <div class="alert alert-danger" style="display: none; ?>" id="alertwrong">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('wrongpassword'); ?>
                              </div>
                              <div class="alert alert-danger" style="display: none; ?>" id="alertemailwrong">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  Email Facebook anda tidak mengizinkan!!!
                              </div>
                              <div class="alert alert-danger" style="display: none; ?>" id="alertfailedregister">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('failed');?> Mendaftar 
                              </div>
                              <div class="alert alert-success" style="display: none; ?>" id="alertverifikasisuccess">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('activationsuccess');?> 
                              </div>
                              <div class="alert alert-success" style="display: none; ?>" id="alertverifikasisuccesstutor">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('activationsuccess').' '.$this->lang->line('activationtutor');?> 
                              </div>
                              <div class="alert alert-danger" style="display: none; ?>" id="alertverifikasifailed">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('alreadyactivation');?> 
                              </div>
                              <div class="alert alert-warning" style="display: none; ?>" id="alertverifikasiemail">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php 
                                      $load = $this->lang->line('youremail3');
                                      $load2 = $this->lang->line('here');
                                      $load3 = $this->lang->line('emailresend');
                                      echo $load."<a href=".base_url('/Master/resendEmaill')."> <label style='color:#008080; cursor:pointer;'>".$load2."</label></a> ".$load3;
                                  ?> 
                              </div>
                              <div class="alert alert-success" style="display: none; ?>" id="alertsuccessendemail">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('successfullysent');?>
                              </div>
                              <div class="alert alert-danger" style="display: none; ?>" id="alertfailedsendemail">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('failed');?>
                              </div>
                              <div class="alert alert-warning" style="display: none; ?>" id="alertverifikasitutor">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('youremail2');?>
                              </div>

                              <div class="alert alert-danger" style="display: none; ?>" id="alertfailedchangepassword">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('failedchange');?>
                              </div>
                              <div class="alert alert-warning" style="display: none; ?>" id="alertsuccesschangepassword">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('successfullypassword');?>
                              </div>
                              <div class="alert alert-warning" style="display: none; ?>" id="alertloginfirstpackage">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                  <?php echo $this->lang->line('loginfirst');?>
                              </div>
          
                              <div id="kotakalert"></div>
                              <fieldset>     
                               
                                  <div class="form-group label-floating">
                                     <div class="input-group">
                                         <span class="input-group-addon">
                                         <i class="zmdi zmdi-account"></i>
                                         </span>
                                         <label class="control-label" for="ms-form-user">Email/Kid's Username</label>
                                         <input type="text" name="email" id="emaill" required class="input-sm form-control fg-input"> 
                                     </div>
                                  </div>
                                  <div class="form-group label-floating">
                                      <div class="input-group">
                                         <span class="input-group-addon">
                                         <i class="zmdi zmdi-lock"></i>
                                         </span>
                                         <label class="control-label" for="ms-form-pass"><?php echo $this->lang->line('password'); ?>
                                         </label>
                                         <input type="password" id="ms-form-pass"  required name="kata_sandi" class="input-sm form-control fg-input" >
                                     </div>
                                 </div>
                                 <input type="text" name="user_utc" id="user_utc" hidden>

                                 <script type="text/javascript">
                                 var user_utc = new Date().getTimezoneOffset();
                                 user_utc = -1 * user_utc;
                                 $("#user_utc").val(user_utc);
                                 $("#user_utc_g").val(user_utc);
                                 $("#user_utc_f").val(user_utc);
                                 </script>
                                 <div class="row mt-2">
                                      <div class="col-md-12">
                                         <div class="form-group no-mt">
                                              <div class="checkbox">
                                                 <label style="color:#009688;">
                                                 <input type="checkbox" value="V" name="bapuks" id="bapuks">
                                                 <i class="input-helper"></i>
                                                 <?php echo $this->lang->line('remember'); ?> 
                                                 </label>                            
                                              </div>
                                         </div>
                                     </div>
                                     <div class="col-xs-12">
                                         <button  type="submit" id="loginnih" class="btn btn-raised btn-primary pull-right"><?php echo $this->lang->line('login'); ?>   
                                         </button>
                                     </div>
                                     <center>
                                         <label class="fg-label"><?php echo $this->lang->line('notaccountlogin'); ?></label>
                                         <a href="<?php echo base_url(); ?>Register"> <?php echo $this->lang->line('here'); ?></a>
                                     </center>
                                     <center>
                                         <label style="color:#ff9000;">
                                         <a href="#" data-toggle="modal"  data-target ="#forgot_modal"  data-dismiss="modal" ><?php echo $this->lang->line('lupasandi'); ?></a>
                                          </label>
                                     </center>
                                 </div>
                                 <div class="text-center">
                                     <a class="wave-effect-light btn btn-raised btn-facebook " id="f_signInbtn">
                                         <i class="zmdi zmdi-facebook" ></i><?php echo $this->lang->line('withfacebook'); ?>
                                     </a>
                                     <a class="wave-effect-light btn btn-raised btn-google" id="customBtn" >
                                         <i class="zmdi zmdi-google"></i><?php echo $this->lang->line('withgoogle'); ?>
                                     </a>
                                     <!-- <div class="" >Login</div> -->
                                  </div>
                                  <script>startApp();</script>
                              </fieldset>
                              <input type="text" name="kode" id="kode" hidden>
                         <!-- </form> -->
                         <form hidden method="POST" id='google_form' action="<?php echo base_url();?>master/google_login">
                         	<input  type="text" id="channel_idd"  name="channel_idd" class="" style="display: none;" >
                               <!-- <input type="text" name="email" id="google_email"> -->
                              <textarea name="google_data" id="google_data" style="width: 900px;"></textarea>
                              <input type="text" name="usertype_id" value="student">
                              <input type="text" name="user_utc_g" id="user_utc_g" hidden>
                          </form>
                          <form hidden method="POST" action='<?php echo base_url(); ?>master/facebook_loginn' id='facebook_form'>
                              <!-- <input type="text" name="email" id="google_email"> -->
                              <textarea name="fb_data" id="fb_data"></textarea>
                              <input type="text" name="usertype_id" value="student">
                              <input type="text" name="user_utc_f" id="user_utc_f" hidden>

                          </form>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 	</div>
  <div class="modal modal-dark" id="forgot_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
      <div class="modal-dialog animated zoomIn animated-3x" role="document" id="modaldialog_forgot">
          <div class="modal-content">
              <div class="modal-header" style="text-align: center; background-color: #008080;">
                  <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#login_modal" aria-label="Close"><span aria-hidden="true"> <i class="zmdi zmdi-close"></i></span> 
                  </button>
                  <h3 class="modal-title" id="myModalLabel3"><?php echo $this->lang->line('lupasandi'); ?></h3>
              </div>
              <div class="modal-body">
                  <form role="form" action="<?php echo base_url('master/sendLink'); ?>" method="post">
                      <div class="alert alert-success" style="display: none; ?>" id="alert_successforgot">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                          <?php echo $this->lang->line('sentlink'); ?>
                      </div>
                      <div class="alert alert-danger" style="display: none; ?>" id="alert_failedforgot">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                          <?php echo $this->lang->line('failedsendemail'); ?>
                      </div>
                      <div class="lv-body">
                          <div class="lv-item">
                              <div style="text-align: center;" class="lv-title">
                                  <?php echo $this->lang->line('title1'); ?>
                                  <?php echo $this->lang->line('title2'); ?>
                                  <?php echo $this->lang->line('title3'); ?></div>
                          </div>
                          <div class="form-group label-floating">
                              <div class="input-group"> <span class="input-group-addon"> <i class="zmdi zmdi-email"></i> </span> 
                                  <label class="control-label" for="ms-form-user">Email</label>
                                  <input type="text" name="forgotemail" required class="input-sm form-control fg-input">
                              </div>
                          </div>
                          <div class="modal-footer">
                              <button class="btn btn-raised btn-success pull-right" name="sndmail" id="sndmail" style="background-color: #008080;">
                                  <?php echo $this->lang->line('sendemail'); ?></button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
	<nav hidden="true" class="navbar ms-lead-navbar navbar-mode" id="nav3" style="z-index: 2; background-color: #FF00FF;; padding: 0;">

		<div class="" id="home" style="z-index: 2;">
		  <div class="navbar-header" style="">
		      <a class="navbar-brand" data-scroll href="<?php echo base_url(); ?>">
		          <!-- <img src="assets/img/demo/logo-navbar.png" alt=""> -->          
		          <img style="margin-top: -2px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/LogoIjo.png" alt="">          
		      </a>
		  </div>
		  <!-- <div id="navbar" class="navbar-collapse collapse">
		      <ul class="nav navbar-nav">
		        <li>
		          <a data-scroll href="#fitur">Fitur</a>
		        </li>
		        <li>
		          <a data-scroll href="#contact">Kontak</a>
		        </li>
		      </ul>
		  </div> -->
		  <!-- navbar-collapse collapse -->
		  <div>
		  <ul  class="nav navbar-nav navbar-right" style="">


		    <a id="logofb" style="margin-right:420px;" href="https://www.facebook.com/classmiles" target="_blank" class="btn-navbar-menu btn-circle btn-facebook color-white animated zoomInDown animation-delay-7">
		        <i class="zmdi zmdi-facebook"></i>
	      	</a>
	      	<a id="logotwitter" style="  margin-right: 370px;"" href="https://twitter.com/classmiles" target="_blank" class="btn-navbar-menu btn-circle btn-circle-primary color-white animated zoomInDown animation-delay-7">
	        <i class="zmdi zmdi-twitter"></i>
	     	</a>
		    
	     	<a id="bendera" style="margin-top: 7px;margin-right: 20px; width:  3%;" href="/" id="btn_setindonesialp" class=" btn btn-rss animated zoomInDown animation-delay-10 " >
            	<center><img class="btn_setindonesialp animated zoomInDown animation-delay-10"  width="34px" height="20px" style="margin-left: -15px; cursor: pointer;" src="<?php if ($this->session->userdata('lang') == "indonesia") { echo CDN_URL.STATIC_IMAGE_CDN_URL.'language/flaginggris.png'; } else { echo CDN_URL.STATIC_IMAGE_CDN_URL.'language/flagindo.png'; } ?>" /> </center>
          	</a>

	      	<!-- <a id="" href="#" data-toggle="modal"  data-target ="#classview" class="btn-circle  btn-circle-success"><i class="zmdi zmdi-close"></i></a> -->
	      	<a id="nav_login" href="#" data-toggle="modal"  data-target ="#login_modal" class="btn btn-whatsapp animated zoomInDown animation-delay-10 " style="margin-top: 7px; color: #ffffff; font-size: 13px; ">
	        <?php echo $this->lang->line('navbartop2')?>
	      	</a>
	      	<a href="<?php echo base_url(); ?>Register" class="btn btn-vimeo animated zoomInDown animation-delay-10" style="margin-top: 7px; color: #ffffff; font-size: 13px; ">
		        <?php echo $this->lang->line('navbartop1')?>
	      	</a>

		    <!-- <a href="<?php echo base_url(); ?>login" class="btn-navbar-menu">
		      Login
		    </a>
		    <a style="margin-right: 48px;" href="<?php echo base_url(); ?>first/register" id="untukdaftar" class="btn-navbar-menu">
		      Daftar Sekarang
		    </a> -->
		  </ul>
		  </div>
		</div>
	</nav>
  <div id="slidebanner" class="bg-default ms-bg-fixed" style="background-color: rgba(236, 240, 241, 0.54);">
    <div class="row no-gutters" style="">
      <div id="sliderposter" class="carousel slide" data-ride="carousel">             
        <ol class="carousel-indicators" id="indicator_poster">                
        </ol>
        <div class="carousel-inner " style="cursor: pointer;" id="poster_only"></div>
        <a class="left carousel-control" href="#sliderposter" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#sliderposter" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <div id="carousel-listpackage" class="carousel slide hidden-xs" data-ride="carousel" data-interval="" pause="true">
      
      <div class="carousel-inner">
          <div class="item active" id="package_front">
              <div class="row" id="box_front_package">
              </div>
          </div>
          <div class="item" id="package_back">
              <div class="row" id="box_back_package"></div>
          </div>
      </div>
      </div>
    </div>
  </div>
  <div id="tiga_langkah" class="container" style="">
  	<center>
      	<div class="animated zoomInLeft animation-delay-2">
      		<h1 style="color:#008080 "><b><?php echo $this->lang->line('tiga_langkah')?> :</b></h1>	
      	</div>
      	<div style="cursor: pointer; margin-top: 30px; font-size: 15px; text-align: center;" class="card-hero no-shadow" id="">
            <div class="col-md-1 col-sm-1 col-xs-1"></div>
            <div class="withripple zoom-img col-md-2  col-sm-2 col-xs-2 animated zoomInLeft animation-delay-2" ; ">
                <div class="panel-body ">
                    <div class="">
                        <img class="img-fluid" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/select_tutor.png" style="height: 90%; width: 90%;" alt="" class="">
                    </div>
                    <div style="font-size: 20px; padding-top: 10px; color: #008080" class="">
                    	<p><b><?php echo $this->lang->line('pilih_tutor')?></b></p>
                    </div>
                </div>
            </div>
            <div style="padding-top: 30px; padding-bottom: 30px;"  class="col-md-2  col-sm-2 col-xs-2 animated zoomInLeft animation-delay-5 ; ">
                <div class="panel-body " style="padding-top: 30px; padding-bottom: 30px;">
                    <div>
                        <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/arrow.png" style=" height: 50px; width: auto;" alt="" class="">
                    </div>
                </div>
            </div>
            <div class="withripple zoom-img col-md-2  col-sm-2 col-xs-2 animated zoomInLeft animation-delay-8" ; ">
                <div class="panel-body ">
                    <div class="">
                        <img class="img-fluid" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/select_date-time.png" style="height: 90%; width: 90%;" alt="" class="">
                    </div>
                    <div style="font-size: 20px; padding-top: 10px; color: #008080" class="">
                    	<p><b><?php echo $this->lang->line('atur_waktu')?></b></p>
                    </div>
                </div>
            </div>
            <div  style="padding-top: 30px; padding-bottom: 30px;" class="col-md-2  col-sm-2 col-xs-2 animated zoomInLeft animation-delay-11" ; ">
                  <div class="panel-body " style="padding-top: 30px; padding-bottom: 30px;">
                      <div>
                          <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/arrow.png" style=" height: 50px; width: auto;" alt="" class="">
                      </div>
                  </div>
            </div>
            <div class="withripple zoom-img col-md-2  col-sm-2 col-xs-2 animated zoomInLeft animation-delay-14" ; ">
                <div class="panel-body ">
                    <div class="">
                        <img class="img-fluid" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/enter_class.png" style="height: 90%; width: 90%;" alt="" class="">
                    </div>
                    <div style="font-size: 20px; padding-top: 10px; color: #008080" class="">
                    	<p><b><?php echo $this->lang->line('masuk_kelas')?></b></p>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </center>
  </div>
  
  <div class="container mt-6 mb-6">
      <h2 class="text-center color-primary mb-2 wow fadeInDown animation-delay-4">Kategori</h2>
      <!-- <p class="lead text-center aco wow fadeInDown animation-delay-5 mw-800 center-block mb-4"> Lorem ipsum dolor sit amet,
          <span class="color-primary">consectetur adipisicing</span> elit. Dolor alias provident excepturi eligendi, nam numquam iusto eum illum, ea quisquam.
      </p> -->
      <div class="row mt-4">
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-royal card-body text-center wow zoomInUp animation-delay-2" style="padding: 10px;">
            <h2 class="">Edukasi</h2>
            <i class="color-royal fa fa-4x fa-mortar-board"></i>
            <p class="mt-2 no-mb lead" style="height: 80px;">Pembelajaran SD, SMP dan SMA</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-success card-body text-center wow zoomInUp animation-delay-5" style="padding: 10px;">
            <h2 class="">Seminar</h2>
            <i class="color-success fa fa-4x fa-address-book"></i>
            <p class="mt-2 no-mb lead" style="height: 80px;">Kumpulan seminar umum maupun berbayar</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-danger card-body text-center wow zoomInUp animation-delay-4" style="padding: 10px;">
            <h2 class="">Komunitas</h2>
            <i class="color-danger fa fa-4x fa-group"></i>
            <p class="mt-2 no-mb lead" style="height: 80px;">Saling berbagi ilmu melalui diskusi, ceramah, dll</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-warning card-body text-center wow zoomInUp animation-delay-3" style="padding: 10px;">
            <h2 class="">Konsultasi</h2>
            <i class="color-warning fa fa-4x fa-id-card"></i>
            <p class="mt-2 no-mb lead" style="height: 80px;">Temukan praktisi hukum, psikologi, kesehatan, dll disini</p>
          </div>
        </div>
      </div>
  </div>

  <div class="animasi tada  animation-delay-2" align="center" style="margin-top: 10px; margin-bottom: 30px;">
      <h1><a href="<?php echo base_url(); ?>first/register" style="color:#da0d08 "><b><?php echo $this->lang->line('daftar_disini')?>...!</b></a></h1>  
  </div>
  <!-- <div class="row" style="padding: 0; max-width:100%;height:auto; background-color: #cbcaca; margin: 0; margin-bottom: 5px; display: none;">
  	<div class="col-md-1 col-sm-1 col-xs-1 list_fitur" style="background-color: #008080; cursor: pointer; height:auto; padding-top: 20px; padding-bottom: 20px;" align="center">
  		<div class="withripple zoom-img col-md-12 col-xs-12 col-sm-12" style="padding: 0;">
    		<img class="animasiku tada  img-fluid" id="btn_fitur1" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Interaktive Classroom.png" style="width: 85%; height: 85%; padding: 5px;" >
    	</div>
  		<div class="withripple zoom-img col-md-12 col-xs-12 col-sm-12" style="padding: 0;">
  		<img class="animasiku tada  img-fluid" id="btn_fitur2" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Digital Whiteboard.png" style="width: 85%; height: 85%; padding: 5px;" >
    	</div>
  		<div class=" withripple zoom-img col-md-12 col-xs-12 col-sm-12" style="padding: 0;">
    		<img class="animasiku tada  img-fluid" id="btn_fitur3" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Screenshare.png" style="width: 85%; height: 85%; padding: 5px;" >
    	</div>
  		<div class="withripple zoom-img col-md-12 col-xs-12 col-sm-12" style="padding: 0;">
    		<img class="animasiku tada  img-fluid" id="btn_fitur4" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Raise Hand.png" style="width: 85%; height: 85%; padding: 5px;" >
    	</div>
  	</div>
  	<div class="col-md-11 col-sm-11 col-xs-11" style="">
  		<div id="fitur_video" class="" style="background-color: red;">
  			<div class="col-xs-4 col-md-4 col-sm-4" style=" padding-top: 5%; margin: 0 auto;">
		    	<div class="col-sm-12 col-md-12" style="padding-left: 5px; ">
		    		<div class="embed-responsive embed-responsive-16by9">
					  <iframe width="560" height="315" src="https://www.youtube.com/embed/2bKS6Y8vJKQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>	
		    	</div>
		    </div>
		    <div class="col-xs-4 col-md-4 col-sm-4" style=" padding-top: 5%; margin: 0 auto;">
		    	<div class="col-sm-12 col-md-12" style="padding-left: 5px; ">
		    		<div class="embed-responsive embed-responsive-16by9">
					  <iframe width="560" height="315" src="https://www.youtube.com/embed/6Zl7FL22ooQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
		    	</div>
		    </div>
		    <div class="col-xs-4 col-md-4 col-sm-4" style=" padding-top: 5%; margin: 0 auto;">
		    	<div class="col-sm-12 col-md-12" style="padding-left: 5px; ">
		    		<div class="embed-responsive embed-responsive-16by9">
					  <iframe width="560" height="315" src="https://www.youtube.com/embed/3DVYYiewq-4?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
		    	</div>
		    </div>
  		</div>
  		<div id="container_fitur" class="container" style="display: none;  position: absolute; z-index: 10; border-radius: 20px; padding-left: 0; margin-left: 0; width: 100%; padding-top: 10px; padding-bottom: 10px;">
  			<div align="center" id="detail_fitur" class="col-md-3 col-xs-3 col-sm-3" style="padding-right: 0; margin-right: 0;">
    			<img id="img_fitur" src="" style="width: 70%; height: 70%;" >
    		</div>
    		<div id="" class="col-md-9 col-sm-9 col-xs-9" style="padding: 0; color: #414141;">
    			<h1><b id="judul_fitur"></b></h1>
    			<h3 id="text_fitur"  style=""></h3>
    		</div>
  		</div>
  	</div>
	</div> -->
	<!-- <div class="" id="fiturr" style="display: none;">
	    <div class="row col-md-12" style="margin: 0; padding:0; background-color:white;">
	    	<div class="col-md-6 col-sm-6" style="z-index: 0; padding-left: 0; padding-right: 1px;">
	            <div class="zoomInUp">
	                <div class="col-md-12">
	                	<p class="lead text-left bannerleft" style="">
	                    <?php echo $this->lang->line('bannerb1')?></p>
	                </div>
	                
	                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Time Flexible.png" alt="..." style="width: 100%; margin-bottom: 1px; position: relative;">
	            </div>
	        </div>
	        <div class="col-md-6 col-sm-6" style="z-index: 0; padding-left: 1px; padding-right: 0;">
	            <div class="zoomInUp">
	                
	                <div class="col-md-12">
	                	<p class="lead text-right banner" style="">
	                    <?php echo $this->lang->line('bannerb2')?></p>
	                </div>
	                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Discussion.png" alt="..." style="width: 100%; margin-bottom: 1px; position: relative;">
	            </div>
        	</div>
        	<div class="col-md-6 col-sm-6" style="z-index: 0; padding-left: 0;padding-right: 1px;">
	            <div class="zoomInUp">
	                
	                <div class="col-md-12">
	                	<p class="lead text-left bannerleft" style="">
	                    <?php echo $this->lang->line('bannerb3')?></p>
	                </div>
	                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Private Class.png" alt="..." style="width: 100%; margin-bottom: 1px; position: relative;">
	            </div>
	        </div>
	        <div class="col-md-6 col-sm-6" style="z-index: 0;padding-left: 1px; padding-right: 0;">
	            <div class="zoomInUp">
	                <div class="col-md-12">
	                	<p class="lead text-right banner" style="">
	                    <?php echo $this->lang->line('bannerb4')?></p>
	                </div>
	                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Group Class.png" alt="..." style="width: 100%; margin-bottom: 1px; position: relative;">
	            </div>
	        </div>
    	</div>
	</div> -->

	<nav hidden="true" class="navbar ms-lead-navbar navbar-mode" id="nav4" style="z-index: 2; background-color: #FF00FF;; padding: 0;"> <a class="navbar-brand" data-scroll href="#home"> 	<img style="margin-top: -2px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>banner/LogoIjo.png" alt=""> </a>
	    <ul class="nav navbar-nav navbar-right">
	        <li> <a href="<?php echo base_url(); ?>login" class="btn-circle btn-circle btn-whatsapp"><i class="zmdi zmdi-sign-in"></i></a>  <a href="<?php echo base_url(); ?>first/register" class="btn-circle btn-circle btn-twitter"><i class="zmdi zmdi-account-add" ></i></a>
	        </li>
	    </ul>	    
	</nav>
  <!-- <section class="ms-component-section">
    <div class="container">
      <h2 class="section-title no-margin-top">Featured Class</h2>
      <div class="owl-dots"></div>
      <div class="owl-carousel owl-theme" id="kotak_kelas">
      </div>
    </div>
  </section> -->
	<!-- <div id="kotakfeaturedclass" class="bg-default ms-bg-fixed" style="display: none; background-color: rgba(236, 240, 241, 0.54);">
      <div class="container">
        <div class="row no-gutters" style="margin-bottom: 2%; margin-top: 2%;">
			    <div class="row">
	            <div class="col-md-9 col-sm-9 col-xs-9">
	                <h2>Featured Classes</h2>
	            </div>
	            <div id="controllernya" class="col-md-3 col-xs-3">
	                
	                <div class="controls pull-right hidden-xs">
	                    <a class="left fa fa-chevron-left btn btn-success" href="#carousel-featuredclass"
	                        data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-success" href="#carousel-featuredclass"
	                            data-slide="next"></a>
	                </div>
	            </div>
	        </div>
	        <div id="carousel-featuredclass" class="carousel slide hidden-xs" data-ride="carousel" data-interval="10000" pause="true">
	            
	            <div class="carousel-inner">
	                <div class="item active" id="featured_front">
	                    <div class="row" id="box_front"> 
                      </div>
	                </div>
	                <div class="item" id="featured_back">
	                    <div class="row" id="box_back"></div>
	                </div>
	            </div>
	        </div>
        </div>
      </div>
  </div> -->
	<!-- <div class="bg-default ms-bg-fixed">
	    <div class="" style="">
	    	<div id="fpc_effect-back">
          <div class="product" id="fpc_box">
			 	    <div class="ribbon-wrapper"><div class="ribbon">Free Registration</div></div>
    			  <div id="fpc_content" style="" >
  			  	  <div class="text-center" style="padding-top: 2px; padding-bottom: 2px;">
  		            <h3  class="color-warning uppercase wow fadeInDown animation-delay-2 text-normal"><?php echo $this->lang->line('fitur_tutor1')?></h3>
  		            <h3 class="color-warning uppercase wow fadeInDown animation-delay-2 text-normal"><a href="<?php echo base_url(); ?>RegisterTutor"> <?php echo $this->lang->line('fitur_tutor3')?></a> <?php echo $this->lang->line('free')?>...!!</h3>
		          </div>
    			  </div>
      			<div id="fpc_corner-box">
      			        <a id="fpc_page-tip" href="#">
      			        <div id="fpc_corner-contents" align="center"><div id="fpc_corner-button"><div class="ribbon-wrapper-right"><div class="ribbon-right">No Charge</div></div></div></div></a>
      			</div>
          </div>
			  </div>
        <div class="row" style="padding-right: 2px; padding-left: 2px; padding-top: 4px; color: #000000; ">
    		    <div class="col-md-3 col-sm-3 wow zoomInUp" style="">
    		    	<div class="col-md-3 col-sm-3" style="padding-right: 2px; padding-left: 5px;">
    		        	<img style="width: 100%; margin-top: 7px; " src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Kapanpun.png">	
    		    	</div>
    		    	<div class="col-sm-9 col-md-9" style=" margin-left: -1%; padding-left: 2px; padding-right: 2px;">
    		    		<p class="text-normal" style="font-size: 13px; color: #36b7e2;  text-transform: uppercase;"><u><b>
    		            <?php echo $this->lang->line('fiturtutor1')?></b></u></p>
    		        	<label style=" line-height: 110%; text-align: left; font-size: 13px; color: black;"><?php echo $this->lang->line('isi_fitur1')?></label>	
    		    	</div>
    		    </div>
    		    <div class="col-md-3 col-sm-3 wow zoomInUp" style="padding-left: 5px; border-right: 1px solid #e2e2e2; border-left: 2px solid #e2e2e2">
    		    	<div class="col-md-3 col-sm-3" style="padding-right: 2px; padding-left: 5px;">
    		        	<img style="width: 100%; margin-top: 7px; " src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/DImanapun.png">	
    		    	</div>
    		    	<div class="col-sm-9 col-md-9" style=" margin-left: -1%; padding-left: 2px; padding-right: 2px;">
    		    		<p class="text-normal" style="font-size: 13px; color: #1aab5c;  text-transform: uppercase;"><u><b>
    		            <?php echo $this->lang->line('fiturtutor2')?></b></u></p>
    		        	<label style=" line-height: 110%; text-align: left; font-size: 13px; color: black;"><?php echo $this->lang->line('isi_fitur2')?></label>	
    		    	</div>
    		    </div>
    		    <div class="col-md-3 col-sm-3 wow zoomInUp" style="padding-left: 5px; border-right: 2px solid #e2e2e2; border-left: 1px solid #e2e2e2">
    		    	<div class="col-md-3 col-sm-3" style="padding-right: 2px; padding-left: 5px;">
    		        	<img style="width: 100%; margin-top: 7px; " src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Tentukan Harga.png">	
    		    	</div>
    		    	<div class="col-sm-9 col-md-9" style=" margin-left: -1%; padding-left: 2px; padding-right: 2px;">
    		    		<p class="text-normal" style="font-size: 13px; color: #ea232a;  text-transform: uppercase;"><u><b>
    		            <?php echo $this->lang->line('fiturtutor3')?></b></u></p>
    		        	<label style=" line-height: 110%; text-align: left; font-size: 13px; color: black;"><?php echo $this->lang->line('isi_fitur3')?></label>	
    		    	</div>
    		    </div>
    		    <div class="col-md-3 col-sm-3 wow zoomInUp" style="padding-left: 0px; padding-right: 20px;">
    		    	<div class="col-md-3 col-sm-3" style="padding-right: 10px; margin-right: 1px; padding-left: 5px;">
    		        	<img style="width: 100%; margin-top: 7px; " src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Efisiensi.png">	
    		    	</div>
    		    	<div class="col-sm-9 col-md-9" style=" margin-left: -1%; padding-left: 2px; padding-right: 2px;">
    		    		<p class="text-normal" style="font-size: 13px; color: #f57f20; text-transform: uppercase;"><u><b>
    		            <?php echo $this->lang->line('fiturtutor4')?></b></u></p>
    		        	<label style=" line-height: 110%; text-align: left; font-size: 13px; color: black;"><?php echo $this->lang->line('isi_fitur4')?></label>	
    		    	</div>
    		    </div>
    		</div>
	    </div>
	</div> -->
	<div class="" id="contact" style="background-color: white;">
    <?php $this->load->view('home/footer.php'); ?>
  </div>
    <!-- <div class="btn-back-top" id="tombol_bawah">
        <a href="#" data-scroll id="back-top"  class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised "> <i class="zmdi zmdi-long-arrow-up"></i> 
        </a>
    </div> -->
	<div class="ms-slidebar sb-slidebar sb-left sb-momentum-scrolling sb-style-overlay">
  	<?php $this->load->view('home/sidemenu.php'); ?>
  </div>
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  <script src="https://apis.google.com/js/api:client.js"></script>
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/landing_page/js/plugins.min.js"></script>
  <script src="<?php echo base_url(); ?>aset/landing_page/js/app.min.js"></script>
  <!-- <script src="<?php echo base_url(); ?>aset/landing_page/js/component-carousels.js"></script> -->
  <script src="<?php echo base_url(); ?>aset/sel2/js/select2.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.min.js"></script>
  <script type="text/javascript">
      $(document).ready(function(){
          window.fbAsyncInit = function() {
              FB.init({
                appId      : '587497711688845',
                xfbml      : true,
                version    : 'v2.8'
              });
              FB.AppEvents.logPageView();
              FB.getLoginStatus(function (response) {
              })
          };

          (function(d, s, id){
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js";
          fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));

          $('#f_signInbtn').click(function(){             
              FB.login(function(response){                
                  if(response.status == 'connected'){
                      var new_data = {};
                      FB.api('/me', { locale: 'en_US', fields: 'name, email' }, function(response) {
                          console.log(JSON.stringify(response));
                          new_data['name'] = response.name;
                          new_data['email'] = response.email;
                          var id = response.id;
                          FB.api('/me/picture',{type: 'large'} ,function(response) {
                              // console.log(JSON.stringify(response));
                              new_data['image'] = "https://graph.facebook.com/"+id+"/picture";
                              new_data = JSON.stringify(new_data);
                              $('#fb_data').html(new_data);
                              $('#facebook_form').submit();
                          });
                      });
                  }
              }, {scope: 'public_profile,email'});
          });
      });
  </script>
  <script type="text/javascript">
      function cycleBackgrounds(){var index=0;$imageEls=$('.slidee');setInterval(function(){index=index+1<$imageEls.length?index+1:0;$imageEls.eq(index).addClass('show');$imageEls.eq(index-1).removeClass('show');},9000);};$(function(){cycleBackgrounds();});
      function cycleBackgrounds(){var index=0;$imageEls=$('.mslidee');setInterval(function(){index=index+1<$imageEls.length?index+1:0;$imageEls.eq(index).addClass('show');$imageEls.eq(index-1).removeClass('show');},5000);};$(function(){cycleBackgrounds();});
  </script>
  <script type="text/javascript">
		$('#slideshow').mouseover(function(event) {
    });
	</script>
  <script type="text/javascript">
			$(document).ready(function(){
			    $("#btn_fitur1").hover(function(){
			    	$("#container_fitur").css('display','block');
			    	$("#fitur_video").css('display','none');
			    	$("#container_fitur").css('margin-top','4%');
			    	var img_fitur = document.getElementById('img_fitur');
					img_fitur.src = "<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Interaktive Classroom.png";
			        $("#judul_fitur").text("Interactive Classroom");
			        $("#text_fitur").text("<?php echo $this->lang->line('bannert1')?>");
			        }, function(){
			        $("#container_fitur").css('display','none');
			        $("#fitur_video").css('display','block');
			    });
			    $("#btn_fitur2").hover(function(){
			    	$("#container_fitur").css('display','block');
			    	$("#fitur_video").css('display','none');
			    	$("#container_fitur").css('margin-top','4%');
			    	var img_fitur = document.getElementById('img_fitur');
					img_fitur.src = "<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Digital Whiteboard.png";
			        $("#judul_fitur").text("Digital Whiteboard");
			        $("#text_fitur").text("<?php echo $this->lang->line('bannert2')?>");
			        }, function(){
			        $("#container_fitur").css('display','none');
			        $("#fitur_video").css('display','block');
			    });
			    $("#btn_fitur3").hover(function(){
			    	$("#container_fitur").css('display','block');
			    	$("#fitur_video").css('display','none');
			    	$("#container_fitur").css('margin-top','4%');
			    	var img_fitur = document.getElementById('img_fitur');
					img_fitur.src = "<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Screenshare.png";
			        $("#judul_fitur").text("Screenshare");
			        $("#text_fitur").text("<?php echo $this->lang->line('bannert3')?>");
			        }, function(){
			        $("#container_fitur").css('display','none');
			        $("#fitur_video").css('display','block');
			    });
			    $("#btn_fitur4").hover(function(){
			    	$("#container_fitur").css('display','block');
			    	$("#fitur_video").css('display','none');
			    	$("#container_fitur").css('margin-top','4%');
			    	var img_fitur = document.getElementById('img_fitur');
					img_fitur.src = "<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Raise Hand.png";
			        $("#judul_fitur").text("Raise Hand");
			        $("#text_fitur").text("<?php echo $this->lang->line('bannert4')?>");
			        }, function(){
			        $("#container_fitur").css('display','none');
			        $("#fitur_video").css('display','block');
			    });
			});
	</script>
	<script type="text/javascript">		
		$(document).ready(function(){var baba=$("#kotakdepan").width();$("#kotakdepan").css('background-size',baba);});			     

      function inDulu() {
          auth2.signIn().then(function() {
              console.log(auth2.currentUser.get().getId());
          });
      }
      
			$(document).ready(function () {
        var kode_cek = "<?php echo $this->session->userdata('kode_cek');?>";
        $("#kode").val(kode_cek);
				var code = null;
				var cekk = setInterval(function () {
					code = "<?php echo $this->session->userdata('code');?>";
					cek();
				}, 500);

				function cek() {
          console.log(code);
					if (code == "101") {
						$("#login_modal").modal('show');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertwrong").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "102") {
						$("#login_modal").modal('show');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertwrong").css('display', 'none');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "105") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertemailwrong").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "106") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertfailedregister").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "107") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "108") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "109") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "110") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasisuccesstutor").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "121") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "189") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "188") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "187") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "290") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "301") {
						$("#login_modal").modal('hide');
						$("#forgot_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alert_successforgot").css('display', 'none');
						$("#alert_failedforgot").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "302") {
						$("#login_modal").modal('hide');
						$("#forgot_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alert_failedforgot").css('display', 'none');
						$("#alert_successforgot").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "303") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertsuccesschangepassword").css('display', 'none');
						$("#alertfailedchangepassword").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "304") {
						$("#login_modal").modal('show');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertfailedchangepassword").css('display', 'none');
						$("#alertsuccesschangepassword").css('display', 'block');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "800") {
						$("#login_modal").modal('show');
            $("#alertloginfirstpackage").css('display','block');
						$("#alertwrong").css('display', 'none');
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertverifikasisuccess").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						code == null;
						clearInterval(cekk);
						$.ajax({
							url: '<?php echo base_url(); ?>Rest/clearsession',
							type: 'POST',
							data: {
								code: code
							},
							success: function (response) {
								console.warn(response);
							}
						});
					} else if (code == "999") {
            $("#login_modal").modal('show');
            $("#alertloginfirstpackage").css('display','block');
            $("#alertwrong").css('display', 'none');
            $("#alertemailwrong").css('display', 'none');
            $("#alertfailedregister").css('display', 'none');
            $("#alertverifikasisuccess").css('display', 'none');
            $("#alertverifikasiemail").css('display', 'none');
            $("#alertfailedsendemail").css('display', 'none');
            $("#alertverifikasifailed").css('display', 'none');
            $("#alertsuccessendemail").css('display', 'none');
            $("#alertverifikasitutor").css('display', 'none');
            $("#alertverifikasitutor").css('display', 'none');            
            // code == null;
            // clearInterval(cekk);
            // $.ajax({
            //   url: '<?php echo base_url(); ?>Rest/clearsession',
            //   type: 'POST',
            //   data: {
            //     code: code
            //   },
            //   success: function (response) {
            //     console.warn(response);
            //   }
            // });
          }
          else {
						$("#alertemailwrong").css('display', 'none');
						$("#alertfailedregister").css('display', 'none');
						$("#alertfailedsendemail").css('display', 'none');
						$("#alertsuccessendemail").css('display', 'none');
						$("#alertverifikasiemail").css('display', 'none');
						$("#alertverifikasifailed").css('display', 'none');
						$("#alertverifikasitutor").css('display', 'none');
						$("#alertwrong").css('display', 'none');
						clearInterval(cekk);
					}
					console.warn(code);
				}
			});
			$('.btn_setindonesialp').click(function (e) {
				e.preventDefault();
				var lang = "<?php echo $this->session->userdata('lang'); ?>";
				if (lang == "indonesia") {
					$.get('<?php echo base_url('set_lang/english'); ?>', function (hasil) {
							location.reload();
						});
				} else {
					$.get('<?php echo base_url('set_lang/indonesia'); ?>', function (hasil) {
							location.reload();
						});
				}
			});
		</script>
  <script type="text/javascript">
    var input = document.getElementById("ms-form-pass");
    input.addEventListener("keyup", function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
            document.getElementById("loginnih").click();
        }
    });
    $(document).on("click", "#loginnih", function () {
      $("#alertemailwrong").css('display', 'none');
      $("#alertfailedregister").css('display', 'none');
      $("#alertverifikasisuccess").css('display', 'none');
      $("#alertverifikasiemail").css('display', 'none');
      $("#alertfailedsendemail").css('display', 'none');
      $("#alertsuccessendemail").css('display', 'none');
      $("#alertverifikasifailed").css('display', 'none');
      $("#alertverifikasitutor").css('display', 'none');
      $("#alertwrong").css('display', 'none');
      var email = $("#emaill").val();
      var kata_sandi = $("#ms-form-pass").val();
      var user_utc = $("#user_utc").val();
      var bapuks =  $("#bapuks").val();
      var channel_id = $("#channel_id").val();
      $.ajax({
            url: '<?php echo base_url(); ?>Master/aksi_login_new',
            type: 'POST',
            data: {                
                email: email,
                kata_sandi : kata_sandi,
                user_utc : user_utc,
                bapuks: bapuks,
                channel_id : channel_id
            },            
            success: function(data)
            {         
              result = JSON.parse(data); 
              var login_code = result['login_code'];
              var redirect = result['redirect'];
              if (login_code == 1) {
                window.location.href = "<?php base_url();?>"+redirect;
              }
              else if (login_code == -2) {
                $("#login_modal").modal('show');
                $("#alertemailwrong").css('display', 'none');
                $("#alertfailedregister").css('display', 'none');
                $("#alertverifikasisuccess").css('display', 'none');
                $("#alertverifikasiemail").css('display', 'block');
                $("#alertfailedsendemail").css('display', 'none');
                $("#alertsuccessendemail").css('display', 'none');
                $("#alertverifikasifailed").css('display', 'none');
                $("#alertverifikasitutor").css('display', 'none');
                $("#alertwrong").css('display', 'none');
              }
              else if(login_code == 0){
                $("#login_modal").modal('show');
                $("#alertemailwrong").css('display', 'none');
                $("#alertfailedregister").css('display', 'none');
                $("#alertverifikasisuccess").css('display', 'none');
                $("#alertverifikasiemail").css('display', 'none');
                $("#alertfailedsendemail").css('display', 'none');
                $("#alertsuccessendemail").css('display', 'none');
                $("#alertverifikasifailed").css('display', 'none');
                $("#alertverifikasitutor").css('display', 'none');
                $("#alertwrong").css('display', 'block');
              }
              else{
                $("#login_modal").modal('show');
                $("#alertemailwrong").css('display', 'none');
                $("#alertfailedregister").css('display', 'none');
                $("#alertverifikasisuccess").css('display', 'none');
                $("#alertverifikasiemail").css('display', 'none');
                $("#alertfailedsendemail").css('display', 'none');
                $("#alertsuccessendemail").css('display', 'none');
                $("#alertverifikasifailed").css('display', 'none');
                $("#alertverifikasitutor").css('display', 'none');
                $("#alertwrong").css('display', 'block');
              }
            }
        });
    });
  </script>
	<script type="text/javascript">
  	// var A = "portrait :"+window.matchMedia("(orientation: portrait)").matches;
  	var A = window.matchMedia("(orientation: landscape)").matches;
  	var cek_height = window.screen.availHeight;
		var cek_width = window.screen.availWidth
		// alert("H"+cek_height);
		// alert("W"+cek_width);

		if (A) {

			// alert(A);
			$("#slideshow").css('display','block');
			$("#img_awal").css('display','block');
			$("#mobile_slideshow").css('display','none');
			$("#tiga_langkah").css('margin-top','4%');
		}
		else{
			// alert(A);
			$("#slideshow").css('display','none');
			$("#img_awal").css('display','none');
			$("#tiga_langkah").css('margin-top','7%');
			$("#mobile_slideshow").css('display','block');	
		}
    	// alert(A);
    	// alert(B);

    	var kelascount = 0;
    	var kelasbelakang = 0;
    	var user_utc = new Date().getTimezoneOffset();
      user_utc = -1 * user_utc;        
      var currentdate = new Date(); 
      currentdate = currentdate.getFullYear()+'-'+(currentdate.getMonth()+1)+'-'+currentdate.getDate()+' '+currentdate.getHours()+":"+currentdate.getMinutes()+":"+currentdate.getSeconds();        
    	$.ajax({
          url: '<?php echo base_url(); ?>Master/getFeaturedClass',
          type: 'POST',
          data: {                
            	currentdate: currentdate,
              user_utc : user_utc,
              device: 'web'
          },            
          success: function(data)
          {         
            	result = JSON.parse(data);  
            	
              if (result['message'] == null) {
                  $("#kotakfeaturedclass").css('display','none');
              }   
              else
              {           
                  if (result['status'] == true) 
                  {                       
                      for (var i = 0; i < result['message'].length; i++) {
                          
                          var classid         = result['message'][i]['class_id'];
                          var name          = result['message'][i]['name'];                        
                          
                          var description     = result['message'][i]['description'];
                          var statusall       = result['message'][i]['status_all'];
                          // alert(statusall);
                          var alt_img         ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                          var class_type      = result['message'][i]['class_type'];
                          var user_name       = result['message'][i]['user_name'];
                          var user_image      = result['message'][i]['user_image'];
                          var subject_name    = result['message'][i]['subject_name'];
                          var channel_name    = result['message'][i]['channel_name'];
                          var channel_color    = result['message'][i]['channel_color'];
                          // var jenjang_name    = result['message'][i]['jenjang_name'];
                          // var jenjang_level   = result['message'][i]['jenjang_level'];
                          var jenjangnamelevel    = result['message'][i]['jenjangnamelevel'];  
                          if (jenjangnamelevel == "UMUM - 0") {
                            var jenjangnamelevel = "UMUM";
                          }    
                          
                          var all_sma_ipa = name.substr(name.length - 13);                  
                          // if (jenjang_level==0) {
                          //  jenjang_level="";
                          // }
                          var firstdate       = result['message'][i]['firstdate'];
                          var firsttime       = result['message'][i]['firsttime'];
                          var finishtime      = result['message'][i]['finishtime'];
                          var harga           = result['message'][i]['harga'];
                          var namee           = name.substring(0, 35);
                          var desc            = description.substring(0, 35);  
                          var link            = result['message'][i]['link'];
                          var participant_count = result['message'][i]['participant_count'];
                          if (user_name.length > 30) {
                            user_name           = user_name.substring(0, 32)+"...";
                          }
                          // alert(user_name);
                          if (user_name == "DPP IKA UNY") {
                            // alert("haai");
                            count = 350+participant_count+" Joined";
                          }
                          if (participant_count == "0" && user_name != "DPP IKA UNY") {
                            count = "<br>";
                          }
                          else if (participant_count != "0" && user_name != "DPP IKA UNY") 
                          {
                            count = participant_count+" Joined";
                          }
                          // console.warn(jenjangnamelevel);
                          var number_string = harga.toString(),
                              sisa    = number_string.length % 3,
                              rupiah  = number_string.substr(0, sisa),
                              ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                                  
                          if (ribuan) {
                              separator = sisa ? '.' : '';
                              rupiah += separator + ribuan.join('.');
                          }
                          if (statusall != null && jenjangnamelevel == "SMA - IPA 10, SMA - IPA 11, SMA - IPA 12") {
                            var desc_jenjang = "Semua Jenjang SMA IPA"
                          }
                          else if (statusall != null && jenjangnamelevel == "SMA - IPS 10, SMA - IPS 11, SMA - IPS 12") {
                            var desc_jenjang = "Semua Jenjang SMA IPS"
                          }
                          else if (statusall != null && jenjangnamelevel == "SMP 7, SMP 8, SMP 9") {
                            var desc_jenjang = "Semua Jenjang SMP"
                          }
                          else if (statusall != null && jenjangnamelevel == "SMK 10, SMK 11, SMK 12") {
                            var desc_jenjang = "Semua Jenjang SMK"
                          }
                          else if (statusall == null && jenjangnamelevel == "SD 1, SD 2, SD 3, SD 4, SD 5, SD 6") {
                            var desc_jenjang = "Semua Jenjang SD"
                          }
                          else if (statusall == null && jenjangnamelevel == "TK, SD 1, SD 2, SD 3, SD 4, SD 5, SD 6") {
                            var desc_jenjang = "TK dan SD"
                          }
                          // else if (statusall == null && jenjangnamelevel == "SD 5, SD 6, SMP 7, SMP 8, SMP 9, SMA - IPA 10, SMA - IPA 11, SMA - IPA 12, SMA - IPS 10, SMA - IPS 11, SMA - IPS 12, SMK 10, SMK 11, SMK 12, UMUM, SD 1, SD 2, SD 3, SD 4, TK"){
                          //   var desc_jenjang = "Semua Jenjang";
                          // }
                          else if (statusall == null) {
                            var desc_jenjang = jenjangnamelevel;
                          }
                          else if (statusall != null) {
                            var desc_jenjang = "Semua Jenjang";
                          }
                          if (rupiah=="F") {
                            rupiah = "Free";
                          }
                          else
                          {
                            rupiah = "Rp. "+rupiah;
                          }
                         
                          //Untuk disabled jika kelasnya kurang dari 4
                          
                          kelascount++;

                              if (channel_name != null) {
                                var box_front =
                                				"<div class='card animation-delay-"+i+"' style=''>"+
                                                    "<div class='price-table wow zoomInUp'>"+
                                                      "<header class='price-table-header' style='background-color: "+channel_color+";'>"+
                                                        "<span class='price-table-category'>"+channel_name+"</span>"+
                                                      "</header>"+
                                                      "<div class='col-item'>"+
                                                        "<div class='photo' style='padding:3%'; '>"+
                                                          "<img src='"+user_image+"' class='img-fluid img-responsive' onerror="+alt_img+" style='height: 200px; width: auto;' />"+
                                                        "</div>"+
                                                        "<div class='info'>"+
                                                            "<div class='row'>"+
                                                                "<div class='price col-md-12 col-sm-12 col-xs-12' style='margin-bottom:0'>"+
                                                                    "<h5>"+namee+"</h5>"+
                                                                    "<p style='font-size:13px;'>"+desc+"</p>"+
                                                                    "<p class='price-text-color' style='margin-bottom:0; font-weight: 10px;'>"+user_name+"</p>"+
                                                                "</div>"+
                                                               "<div class='col-md-12 col-md-12 col-xs-12' style=''>"+
                                                                  "<p style='font-size:11px;'>"+desc_jenjang+"</p>"+
                                                                "</div>"+
                                                                "<div class='col-md-8 col-sm-8 col-xs-8' style=''>"+
                                                                  "<p style='font-size:11px; margin-bottom:0;'>"+firstdate+", "+firsttime+" - "+finishtime+"</p>"+
                                                                  "<p style='font-size:11px; margin-bottom:0;'>"+count+"</p>"+
                                                                "</div>"+
                                                                "<div class='col-md-4 col-sm-4 col-xs-4 price-text-color' style='bottom:0; font-size:15px; padding:0' align='center'>"+
                                                                "<b>"+rupiah+"</b>"+
                                                                "</div>"+
                                                            "</div>"+
                                                            "<div class='separator clear-left'>"+                                           
                                                                "<center>"+
                                                                    "<div style='margin-top: 3%;'>"+
                                                                      "<a style='background-color:"+channel_color+"; color:white' href='"+link+"' class='ml-1 btn btn-raised' title='Klik untuk join'>Join</a>"+
                                                                    "</div>"+
                                                                "</center>"+
                                                            "</div>"+
                                                            "<div class='clearfix'>"+
                                                            "</div>"+
                                                        "</div>"+
                                                    "</div>"+
                                                    "</div>"+
                                          
                                                "</div>";
                              }
                              else{
                              	var box_front =
                              					"<div class='card animation-delay-"+i+"' style=''>"+
                                                    "<div class='price-table wow zoomInUp' style=''>"+
                                                      "<header class='price-table-header'>"+
                                                      "</header>"+
                                                      "<div class='col-item' style='padding-top:46px;'>"+
                                                        "<div class='photo' style='padding:3%';  '>"+
                                                          "<img src='"+user_image+"' class='img-fluid img-responsive' onerror="+alt_img+" style='height: 200px; width: auto;' />"+
                                                        "</div>"+
                                                        "<div class='info'>"+
                                                            "<div class='row'>"+
                                                                "<div class='price col-md-12 col-sm-12 col-xs-12' style='margin-bottom:0'>"+
                                                                    "<h5>"+namee+"</h5>"+
                                                                    "<p style='font-size:13px;'>"+desc+"</p>"+
                                                                    "<p class='price-text-color' style='margin-bottom:0; font-weight: 10px;'>"+user_name+"</p>"+
                                                                "</div>"+
                                                               "<div class='col-md-12 col-md-12 col-xs-12' style=''>"+
                                                                  "<p style='font-size:11px;'>"+desc_jenjang+"</p>"+
                                                                "</div>"+
                                                                "<div class='col-md-8 col-sm-8 col-xs-8' style=''>"+
                                                                  "<p style='font-size:11px; margin-bottom:0;'>"+firstdate+", "+firsttime+" - "+finishtime+"</p>"+
                                                                  "<p style='font-size:11px; margin-bottom:0;'>"+count+"</p>"+
                                                                "</div>"+
                                                                "<div class='col-md-4 col-sm-4 col-xs-4 price-text-color' style='bottom:0; font-size:15px; padding:0' align='center'>"+
                                                                "<b>"+rupiah+"</b>"+
                                                                "</div>"+
                                                            "</div>"+
                                                            "<div class='separator clear-left'>"+                                           
                                                                "<center>"+
                                                                    "<div style='margin-top: 3%;'>"+
                                                                      "<a href='"+link+"' class='ml-1 btn btn-raised btn-info' title='Klik untuk join'>Join</a>"+
                                                                    "</div>"+
                                                                "</center>"+
                                                            "</div>"+
                                                            "<div class='clearfix'>"+
                                                            "</div>"+
                                                        "</div>"+
                                                    "</div>"+
                                                    "</div>"+
                                                "</div>";
                                 var box_frosnt = "<div class='card card-primary animation-delay-"+i+"' style='' title='"+name+" - "+description+"'>"+
                                                    "<div class='col-item wow zoomInUp'>"+
                                                        "<div class='photo' style='padding:3%'; '>"+
                                                          "<img src='"+user_image+"' class='img-fluid img-responsive' onerror="+alt_img+" style='margin-top: 10px; margin-bottom:10px; height: 225px; width: auto;' />"+
                                                        "</div>"+
                                                        "<div class='info'>"+
                                                            "<div class='row'>"+
                                                                "<div class='price col-md-12 col-sm-12 col-xs-12' style='margin-bottom:0'>"+
                                                                    "<h5>"+namee+"</h5>"+
                                                                    "<p style='font-size:13px;'>"+desc+"</p>"+
                                                                    "<p class='price-text-color' style='margin-bottom:0; font-weight: 10px;'>"+user_name+"</p>"+
                                                                "</div>"+
                                                               "<div class='col-md-12 col-md-12 col-xs-12' style=''>"+
                                                                  "<p style='font-size:11px;'>"+desc_jenjang+"</p>"+
                                                                "</div>"+
                                                                "<div class='col-md-8 col-sm-8 col-xs-8' style=''>"+
                                                                  "<p style='font-size:11px; margin-bottom:0;'>"+firstdate+", "+firsttime+" - "+finishtime+"</p>"+
                                                                  "<p style='font-size:11px; margin-bottom:0;'>"+count+"</p>"+
                                                                "</div>"+
                                                                "<div class='col-md-4 col-sm-4 col-xs-4 price-text-color' style='bottom:0; font-size:15px; padding:0' align='center'>"+
                                                                "<b>"+rupiah+"</b>"+
                                                                "</div>"+
                                                            "</div>"+
                                                            "<div class='separator clear-left'>"+                                           
                                                                "<center>"+
                                                                    "<div style='margin-top: 3%;'>"+
                                                                      "<a href='"+link+"' class='ml-1' title='Klik untuk join'>Join</a>"+
                                                                    "</div>"+
                                                                "</center>"+
                                                            "</div>"+
                                                            "<div class='clearfix'>"+
                                                            "</div>"+
                                                        "</div>"+
                                                    "</div>"+
                                                "</div>";
                              } 
                              $("#kotak_kelas").append(box_front);
                           //    if (kelascount < 5) {
                            //  $("#carousel-featuredclass").removeAttr("data-interval");
                            //  $("#controllernya").css('display','none');
                            // }  
                      }
                      $(".owl").owlCarousel(),
                              $(".owl-carousel").owlCarousel({loop:!0,margin:10,lazyLoad:!0,autoplay:!0,autoplayHoverPause:!0,animateIn:"fadeIn",slideBy:4,mouseDrag:!1,dotsContainer:".owl-dots",responsive:{0:{items:1},768:{items:2},992:{items:2},1200:{items:4}}})
                      $("#kotakfeaturedclass").css('display','block');
                  }
                  else
                  {
                      $("#kotakfeaturedclass").css('display','none');

                  }
                  /*if (kelasbelakang == 0) {
                      document.getElementById("carousel-featuredclass").removeAttr('class');
                      $("#carousel-featuredclass").removeAttr("data-interval");
                      $("#controllernya").css('display','none');
                      $("#featured_back").css('display','none');
                  }
                  else if (kelasbelakang == 0 && kelascount == 0) {
                      document.getElementById("carousel-featuredclass").removeAttr('class');
                      $("#carousel-featuredclass").removeAttr("data-interval");
                      $("#kotakfeaturedclass").css('display','none');
                  }
                  else
                  {                  
                      // document.getElementById("carousel-featuredclass").setAttribute("class", "carousel slide hidden-xs");
                      $("#carousel-featuredclass").removeAttr("data-interval");
                      // $("#controllernya").css('display','block');
                  }*/
              }                     
          }
      });
    //   console.warn(kelascount);
		  // if (kelascount == 0 & kelasbelakang == 0) {
			 //    $("#kotakfeaturedclass").css('display','block');
		  // }
		  // else{
			 //    $("#kotakfeaturedclass").css('display','block');
		  // }
  </script>
  <script type="text/javascript">
  	$("#box_front_package").empty();
    $.ajax({
      url: '<?php echo BASE_URL();?>Rest/listBanner',
      type: 'GET',
      success: function(response){
        var cek_aktif=null;
        for (var i = 0; i < response.data.length; i++){
          var banner = response['data'][i]['banner'];
          if (i==0) {
            cek_aktif="active";
          }
          else{
            cek_aktif="";
          }
          indikator="<li data-target='#sliderposter' data-slide-to='"+i+i+"' class='"+cek_aktif+"''></li>";
          posteronly="<div class=' item "+cek_aktif+"'>"+
                "<img src='"+banner+"' alt='' style='width:100%;'>"+
              "</div>";
          $("#poster_only").append(posteronly);
        }
      }
    });
  </script>
  <script type="text/javascript">
   // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]" 
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;

    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;

    // alert(isFirefox);
    if (isFirefox) {
    	// alert("Firefox");
    }
  </script>
  <script type="text/javascript">
    	$("#poster_uny").hover(function(){
    	});
  		$("#slideshow > div:gt(0)").hide();
  		setInterval(function() { 
  		  $('#slideshow > div:first')
  		    .fadeOut(1000)
  		    .next()
  		    .fadeIn(1000)
  		    .end()
  		    .appendTo('#slideshow');
  		},  3000);

  		$("#mobile_slideshow > div:gt(0)").hide();
  		  setInterval(function() { 
  			  $('#mobile_slideshow > div:first')
  			    .fadeOut(1000)
  			    .next()
  			    .fadeIn(1000)
  			    .end()
  			    .appendTo('#mobile_slideshow');
  			},  3000);
  </script>
	<!-- LOGIN AUTOMATIC-->
	<script type="text/javascript">
		function getURLParameter(name) {
			return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
		}
		var a = getURLParameter('ch');
		// alert(getURLParameter('ch'));
		if (a != null) {
			$("#login_modal").modal('show');
			$("#channel_id").val(a);
			$("#channel_idd").val(a);
			// alert("HAO");
		}	
		$(document).on("click", ".btn_choose_packagee", function () {
			// var list_id = $(this).data('list_id');
			var list_id = $("#txt_id_paket").val();
			// alert(list_id);
			$("#login_modal").modal('show');
			$("#desc_package_modal").modal('hide');
			$("#kode").val(list_id);
			$("#alertloginfirstpackage").css('display','block');
		});
		$(document).on("click", ".show_modal_desc", function () {
			$("#list_bidang").empty();
			$("#list_fasilitas").empty();
			$('#txt_poster').attr('src', '');
			var price = $(this).data('price');
			var desc = $(this).data('desc');
			var poster = $(this).data('poster');
			var id_paket = $(this).data('id_paket');
			var name = $(this).data('name');
			var bidang = $(this).data('bidang');
			var fasilitas = $(this).data('fasilitas');
			var listbidang = bidang.split(',');
			var listfasilitas = fasilitas.split(',');
			// console.log(listbidang);
			$("#desc_package_modal").modal('show');
			$("#txt_name").text(name);
			// $("#txt_price").text("Rp. "+price);
			$("#txt_desc").text(desc);
			$("#txt_id_paket").val(id_paket);
			$("#txt_fasilitas").text("Fasilitas : "+fasilitas);
			$("#txt_bidang").text("Bidang : "+bidang);
			// alert(fasilitas);
			document.getElementById("txt_poster").src = poster;
			// alert(bidang);
			for (var i = 0; i < listbidang.length; i++) {
				listbidang[i];
				var list_bidang = "<li>"+listbidang[i]+"</li>";
				$("#list_bidang").append(list_bidang);
			}
			for (var b = 0; b < listfasilitas.length; b++) {
				listfasilitas[b];
				var list_fasilitas = "<li>"+listfasilitas[b]+"</li>";
				$("#list_fasilitas").append(list_fasilitas);
			}
			
			
		});
	</script>
  <script type="text/javascript">
          $(document).ready(function() {

              var A = window.matchMedia("(orientation: landscape)").matches;
              if (A) {
                setInterval(function(){  
                 $('#title_aplikasi').css('display','block');

                       
              },1000);
              }
              else{
                setInterval(function(){  
                 $('#aplikasi_bawah').css('display','block');
                       
              },1000);
              }
          });
          jQuery(document).ready(function(){ 
            $(function(){
              $('#title_aplikasi h4:gt(0)').hide();
              setInterval(function(){
                $('#title_aplikasi :first-child').fadeOut(10)
                   .next('h4').fadeIn(2000)
                   .end().appendTo('#title_aplikasi');}, 
                3000);
          });
          $(function(){
              $('#aplikasi_bawah h4:gt(0)').hide();
                setInterval(function(){
                $('#aplikasi_bawah :first-child').fadeOut(10)
                   .next('h4').fadeIn(2000)
                   .end().appendTo('#aplikasi_bawah');}, 
                    3000);
                  });
          });
  </script>
</html>

