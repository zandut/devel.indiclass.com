<div class="container">
    <div class="row">

        <!-- About -->
        <div class="col-sm-6">
            <div class="heading-footer"><h4>Tentang</h4></div>
            <p>Indiclass adalah sebuah layanan edukasi online untuk memberikan pembelajaran dalam bidang teknologi.</p>
        </div>		            		            

        <!-- Contact -->
        <div class="col-sm-6 mg25-xs">
            <div class="heading-footer"><h4>Contact us</h4></div>
            <p><span class="fa fa-home"></span> <small class="address">Jakarta</small></p>
            <p><span class="fa fa-envelope-open"></span> <small class="address"><a href="mailto:info@fisipro.com">info@indiclass.com</a></small></p>
            <p><span class="fa fa-phone"></span> <small class="address">+62-274-370937 </small></p>
        </div>

    </div><!-- /row -->

    <!-- Copyright -->
    <div class="row">
        <hr>
        <div class="col-sm-11 col-xs-10">
            <p class="copyright">© 2018 Indiclass. All rights reserved.</p>
        </div>
        <div class="col-sm-1 col-xs-2 text-right">
            <a href="#" class="scroll-top"><div class="footer-scrolltop-holder"><span class="glyphicon glyphicon-arrow-up footer-scrolltop"></span></div></a>
        </div>
    </div><!-- /row -->
    
</div><!-- /container -->