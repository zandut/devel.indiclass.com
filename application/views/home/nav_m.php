<div class="container container-full" id="home">
  <div class="navbar-header">
      <a class="navbar-brand" data-scroll href="#home">
          <!-- <img src="assets/img/demo/logo-navbar.png" alt=""> -->          
          <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>logo/logoclass6.png" alt="">          
      </a>
      
  </div>
  <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li>
          <a data-scroll href="#fitur">Fitur</a>
        </li>
        <li>
          <a data-scroll href="#faq">FAQ</a>
        </li>
        <li>
          <a data-scroll href="#contact">Kontak</a>
        </li>
      </ul>
  </div>
  <!-- navbar-collapse collapse -->

  <!-- <a href="javascript:void(0)" class="sb-toggle-left btn-navbar-menu">
    <i class="zmdi zmdi-menu"></i>
  </a> -->
  <a href="#" id="btn_setindonesialp" class="btn-navbar-menu" style="margin-right: 77px; margin-top: -1px">

      <img class="btn_setindonesialp" width="26px" height="15px"  src="<?php if ($this->session->userdata('lang') == "indonesia") { echo CDN_URL.STATIC_IMAGE_CDN_URL.'language/flaginggris.png'; } else { echo CDN_URL.STATIC_IMAGE_CDN_URL.'language/flagindo.png'; } ?>" /> 
    </a>
  <a href="<?php echo base_url(); ?>login" class="btn-navbar-menu">
    <i class="zmdi zmdi-sign-in"></i>
  </a>
  <a style="margin-right: 37px;" href="<?php echo base_url(); ?>first/register" id="untukdaftar" class="btn-navbar-menu">
    <i class="zmdi zmdi-account-add"></i>
  </a>

</div>


<script type="text/javascript">
   $('.btn_setindonesialp').click(function(e){
              e.preventDefault();    
              var lang  = "<?php echo $this->session->userdata('lang'); ?>";
              if (lang == "indonesia") {         
                $.get('<?php echo base_url('set_lang/english'); ?>',function(hasil){  location.reload(); });
              } else {
                  $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
              }
               
          });
         
</script>