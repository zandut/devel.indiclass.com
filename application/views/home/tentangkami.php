<div id="royal_preloader"></div>	

<!-- Begin Boxed or Fullwidth Layout -->
<div id="bg-boxed">
    <div class="boxed">

		<!-- Begin Header -->
		<header>			
			<?php $this->load->view('home/top_nav');?>
		</header><!-- /header -->
		<!-- End Header -->

		<!-- Begin Content Section -->
		<section class="content-40mg">
			<div class="container">

				<!-- Begin Intro + Video -->
				<div class="row">
					<div class="col-sm-12">
						<p class="lead text-center flipInY-animated">Tentang <b><mark>Indiclass</mark></b></p>
						<hr style="width:600px">
					</div>
				</div>
				<div class="row">
					<!-- Content -->
					<div class="col-sm-6 fadeInLeft-animated">
						<div class="heading mb20"><h4><span class="fa fa-address-book mr15 swing-animated"></span>Apa itu Indiclass?</h4></div>
						<p>Indiclass adalah sebuah platform classroom secara digital yang disediakan oleh Telkom Indonesia. Dengan adanya Indiclass ini diharapkan semua orang dapat belajar dimana pun dan kapanpun.</p>  
					</div><!-- /column -->
					<!-- End Content -->

					<!-- Video -->
					<div class="col-sm-6 responsive-video mt30-xs mb15-xs fadeInRight-animated">
						<!-- <iframe src="https://www.youtube.com/watch?v=GOxju1OCM_A" width="500" height="281" style="border: none; border-width: 0px;"></iframe> -->
						<iframe width="560" height="281" src="https://www.youtube.com/embed/GOxju1OCM_A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div><!-- /column -->
				</div><!-- /row -->
				<!-- End Intro + Video -->

				<!-- Begin What We Do -->
				<div class="row mt20">
					<div class="col-sm-12">
						<div class="heading mb15"><h4>Apa yang dilakukan Indiclass?</h4></div>
					</div>

					<!-- Content 1 -->
					<div class="col-sm-4">
					    <h4><span class="fa fa-file-video-o bordered-icon-sm mr15 bounce-animated"></span> Pengalaman Belajar Virtual.</h4>
					    <p class="no-margin">Belajar tidak perlu bertemu. Dengan kelas virtual, melakukan terapi bisa dengan mudah.</p>
					</div>

					<!-- Content 2 -->
					<div class="col-sm-4 mt20-xs">
					    <h4><span class="fa fa-user-circle bordered-icon-sm mr15 bounce-animated"></span> Belajar bersama pengajar yang handal.</h4>
					    <p class="no-margin">Pengajar adalah orang terbaik dari mitra kami, atau manajemen puncak dari perusahaan mitra kami.</p>
					</div>

					<!-- Content 3 -->
					<div class="col-sm-4 mt20-xs">
					    <h4><span class="fa fa-home bordered-icon-sm mr15 bounce-animated"></span> Dapat Diakses Dimana Saja dan Kapan Saja Gratis.</h4>
					    <p class="no-margin">Semua kelas dapat diakses di website kami, dan Anda memiliki pilihan untuk mendapatkan kelas yang anda inginkan.</p>
					</div>
				</div>
				<!-- End What We Do -->							

			</div><!-- /container -->
		</section><!-- /section -->
		<!-- End Content Section -->			

		<!-- Begin Footer -->
		<footer class="footer-light">
		    <?php $this->load->view('home/bottom');?>
		</footer><!-- /footer -->
		<!-- End Footer --> 

	</div><!-- /boxed -->
</div><!-- /bg boxed-->
<!-- End Boxed or Fullwidth Layout -->
