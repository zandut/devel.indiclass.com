<div class="container">

    <div class="col-sm-4">
    </div>    
    <div class="col-sm-4" style="margin-top:17%;">

        <div class="card brd-2">

            <div class="listview">

                <div class="lv-header bgm-bluee">                    
                    <div class="c-white f-17 m-t-10 m-b-10" style="font-family:sans-serif;"><?php echo $this->lang->line('lupasandi') ?></div>
                </div>

                <form role="form" action="<?php echo base_url('master/sendLink'); ?>" method="post">
                    <div class="lv-body">
                        <div class="lv-item">
                        <?php if($this->session->flashdata('mes_alert')){ ?>
                        <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                          <?php echo $this->session->flashdata('mes_message'); ?>
                        </div>
                        <?php }?>
                            <div class="lv-title"><?php echo $this->lang->line('title1'); ?></div>
                            <div class="lv-title"><?php echo $this->lang->line('title2'); ?></div>
                            <div class="lv-title m-b-5"><?php echo $this->lang->line('title3'); ?></div>
                        </div>
                        <div class="lv-item">
                            <label class="fg-label f-15">Email</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                                <div class="fg-line">
                                    <input type="email" name="forgotemail" required class="form-control" placeholder="<?php echo $this->lang->line('inputemail'); ?>">
                                </div>
                            </div>                                            
                        </div>
                        <div class="lv-item" style="margin-left:60%">                     
                            <button class="btn btn-primary" name="sndmail" id="sndmail"><?php echo $this->lang->line('sendemail'); ?></button>                                            
                        </div>
                        <div class="lv-item">
                            <a href="<?php echo base_url(); ?>" style="cursor:pointer;"><label class="fg-label f-14 m-r-20"><?php echo $this->lang->line('backlogin'); ?></label></a>                    
                        </div>
                    </div>    
                </form>                                                            

            </div>

        </div>

    </div>
    <div class="col-sm-4">                                                 
    </div>  

</div><!-- akhir container -->
<!-- </section> -->
