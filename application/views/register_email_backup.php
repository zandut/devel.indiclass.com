<header id="header" class="clearfix" data-current-skin="blue">

    <ul class="header-inner bgm-blue">

        
        <li class="pull-left m-l-20">
            <a href="<?php echo base_url(); ?>" class="m-l-10"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" alt=""></a>
        </li>

        <li class="pull-right">
            <ul class="top-menu">
                <li class="m-r-20">                
                    <h4 style="color:white;"><?php echo $this->lang->line('logout'); ?></h4>                
                </li>  
            </ul>
        </li>
    </ul>

</header>

<section id="main" data-layout="layout-1">
    <section id="content">        
        <div class="container m-t-30"> 
            <div class="col-sm-2"></div>

            <div class="col-sm-8">
                <div class="card bgm-white z-depth-2">
                    <button id="showstudent" class="btn btn-primary">student</button>
                    <button id="shownonstudent" class="btn btn-warning">non student</button>
                </div>
            </div>
            <!-- MULAI DISINI STUDENT -->
            <div id="student" style="display:none;">
                <div class="col-sm-8">

                    <div class="card bgm-white z-depth-2">
                        <center>
                            <div class="card-header">
                                <br><h2>Create new password</h2><br>
                                <hr>
                            </div>
                        </center>

                        <form class="form-horizontal" role="form" action="">
                            <div class="card-padding card-body">
                                <div class="form-group row-fluid">
                                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('email_address'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="email" disabled required class="form-control input-lg" style="background-color:#F5F5F5;" name="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('password'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="password" required class="form-control input-sm" name="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="password" required class="form-control input-sm" name="confirm_password">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('birthday'); ?></label>
                                    <div class="col-sm-3">
                                        <div class="fg-line">
                                            <select required name="tanggal_lahir" id="tanggal_lahir" class="selectpicker">
                                                <option disabled selected  value=''>Birth Date</option>
                                                <?php 
                                                for ($date=01; $date <= 31; $date++) {
                                                    ?>                                                  
                                                    <option value="<?php echo $date ?>"><?php echo $date ?></option>                                                  
                                                    <?php 
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                     <div class="fg-line">
                                        <select required name="bulan_lahir" class="selectpicker">
                                            <option disabled selected value=''>Birth Month</option>
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>                                                            
                                        </select>
                                    </div>

                                </div>
                                <div class="col-sm-3">
                                    <div class="fg-line">
                                        <select required name="tahun_lahir" id="tahun_lahir" class="selectpicker">
                                            <option disabled selected value=''>Birth Year</option>
                                            <?php 
                                            for ($i=1960; $i <= 2016; $i++) {
                                                ?>                                                 
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php 
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('mobile_phone'); ?></label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" name="mobile_phone">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('selectgrade'); ?></label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <select required name="bulan_lahir" class="selectpicker">
                                            <option disabled selected value=''>Select Level Education</option>
                                            <option value="01">SD</option>
                                            <option value="02">SMP</option>
                                            <option value="03">SMA</option>
                                            <option value="04">SMK</option>                                                                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('selectlevel'); ?></label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <select required name="bulan_lahir" class="selectpicker">
                                            <option disabled selected value=''>Select Grade Level</option>
                                            <option value="01">4</option>
                                            <option value="02">5</option>
                                            <option value="03">6</option>
                                            <option value="04">7</option>
                                            <option value="05">8</option>
                                            <option value="06">9</option>
                                            <option value="07">10</option>
                                            <option value="08">11</option>
                                            <option value="09">12</option>                                        
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <hr>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="checkbox" name="">
                                    Saya sudah membaca dan menyetujui Syarat dan Ketentuan, serta Kebijakan Privasi dari Classmiles.
                                </div>                        
                            </div>
                            <div class="form-group">
                                <center>
                                    <div class="col-sm-3">
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="fg-line">
                                            <button type="submit" style="height:40px;" class="btn btn-primary btn-block" id="submit_signup_student">Buat Akun</button>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                    </div>
                                </center>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            </div>
            <!-- AKHIR STUDENT -->
            
            <!-- MULAI DISINI NON STUDENT -->
            <div id="nonstudent" style="display:none;">
                <div class="col-sm-8">

                    <div class="card bgm-white z-depth-2">
                        <center>
                            <div class="card-header">
                                <br><h2>Create new password</h2><br>
                                <hr>
                            </div>
                        </center>

                        <form class="form-horizontal" role="form" action="">
                            <div class="card-padding card-body">
                                <div class="form-group row-fluid">
                                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('email_address'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="email" disabled required class="form-control input-lg" style="background-color:#F5F5F5;" name="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('password'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="password" required class="form-control input-sm" name="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="password" required class="form-control input-sm" name="confirm_password">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('birthday'); ?></label>
                                    <div class="col-sm-3">
                                        <div class="fg-line">
                                            <select required name="tanggal_lahir" id="tanggal_lahir" class="selectpicker">
                                                <option disabled selected  value=''>Birth Date</option>
                                                <?php 
                                                for ($date=01; $date <= 31; $date++) {
                                                    ?>                                                  
                                                    <option value="<?php echo $date ?>"><?php echo $date ?></option>                                                  
                                                    <?php 
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                     <div class="fg-line">
                                        <select required name="bulan_lahir" class="selectpicker">
                                            <option disabled selected value=''>Birth Month</option>
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>                                                            
                                        </select>
                                    </div>

                                </div>
                                <div class="col-sm-3">
                                    <div class="fg-line">
                                        <select required name="tahun_lahir" id="tahun_lahir" class="selectpicker">
                                            <option disabled selected value=''>Birth Year</option>
                                            <?php 
                                            for ($i=1960; $i <= 2016; $i++) {
                                                ?>                                                 
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php 
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('mobile_phone'); ?></label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" name="mobile_phone">
                                    </div>
                                </div>
                            </div>                           
                            <br>
                            <hr>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="checkbox" name="">
                                    Saya sudah membaca dan menyetujui Syarat dan Ketentuan, serta Kebijakan Privasi dari Classmiles.
                                </div>                        
                            </div>
                            <div class="form-group">
                                <center>
                                    <div class="col-sm-3">
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="fg-line">
                                            <button type="submit" style="height:40px;" class="btn btn-primary btn-block" id="submit_signup_student">Buat Akun</button>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                    </div>
                                </center>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- AKHIR NON STUDENT -->

        </div>    
        <div class="col-sm-2"></div>
    </div>
</section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>


<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_setindonesia').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
        });
        $('#btn_setenglish').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
        });
    });
</script>

<script>
    $(document).ready(function(){
        $("#showstudent").click(function(){
            $("#nonstudent").css('display','none');
            $("#student").css('display','block');     
        });
        $("#shownonstudent").click(function(){
            $("#student").css('display','none');
            $("#nonstudent").css('display','block');
        });
    });
</script>