<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">

        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <!-- <div id="alert_complete_data" class="alert alert-info" style="">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->lang->line('alertcompletedata'); ?>
            </div> -->
            <?php if(isset($mes_alert)){ ?>
                <div class="alert alert-<?php echo $mes_alert; ?>" style="display: <?php echo $mes_display; ?>">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?php echo $mes_message; ?>
                </div>
                <?php } ?>
            <div class="block-header">
                <h2><?php echo $this->lang->line('profil'); ?></h2>

                <!-- <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_about'); ?></li>
                        </ol>
                    </li>
                </ul> -->
            </div><!-- akhir block header -->            
            <div class="bs-item z-depth-5" style="margin-top: 5%;">
                <div class="card" id="profile-main">

                    <?php
                        $this->load->view('inc/sideprofile');
                    ?>

                    <div class="pm-body clearfix">
                       
                        <ul class="tab-nav tn-justified" role="tablist">
                        	<?php
                        	$status_user = $this->session->userdata('status_user');
                        	if ($status_user =="umum") {
                        	?>
                        		<li class="waves-effect"><a href="<?php echo base_url('About'); ?>"><?php echo $this->lang->line('tab_about'); ?></a></li>
	                            <!-- <li class="active waves-effect"><a href="<?php echo base_url('Profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li>  -->
                        	<?php
                        	}
                        	else{
							?>
								<li class="waves-effect"><a href="<?php echo base_url('About'); ?>"><?php echo $this->lang->line('tab_about'); ?></a></li>
	                            <!-- <li class="active waves-effect"><a href="<?php echo base_url('Profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li>  -->
	                            <!-- <li id="tab_school" class="waves-effect"><a href="<?php echo base_url('Profile-School'); ?>"><?php echo $this->lang->line('tab_ps'); ?></a></li> -->
                        	<?php
                        	}
                        	?>
                            <!-- <li class="waves-effect"><a href="<?php echo base_url('first/profile_forgot'); ?>"><?php echo $this->lang->line('tab_cp'); ?></a></li> -->                                      
                        </ul>   
                        <div class="card-padding card-body">
                            <div class="pmbb-header">
                                <button title="Edit Profile" id="btn_edit_username" class="btn btn-danger btn-icon pull-right"><i class="zmdi zmdi-edit"></i></button>
                                <button title="Back to Profile" style="display: none;" id="btn_save_username" class="btn btn-danger btn-icon pull-right"><i class="zmdi zmdi-check"></i></button>
                            </div>
                            <blockquote class="m-b-25">
                                <p><?php echo $this->lang->line('tab_account'); ?></p>
                            </blockquote> 
                         	
                            <?php
                            $iduser = $this->session->userdata('id_user');
                            $cek_data= $this->db->query("SELECT tu.user_nationality, tu.id_desa, tu.usertype_id, tu.user_religion, tu.user_address, tu.user_image, tps.jenjang_id, tps.school_id FROM tbl_user as tu INNER JOIN tbl_profile_student as tps WHERE tu.id_user='$iduser' AND tps.student_id='$iduser'")->row_array();
                            if ($cek_data['user_address']!=null) {
                                ?>
                                <div class="pmbb-body p-l-30  show_profile" style="display: block;">
                                    <div class="pmbb-view ">
                                
                                        <dl class="dl-horizontal">
                                            <dt><?php echo $this->lang->line('placeofbirth'); ?></dt>                                        
                                            <dd>:
                                                <?php if(isset($alluserprofile)){ echo $alluserprofile['user_birthplace']; } ?>                                          
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt><?php echo $this->lang->line('religion'); ?></dt>                                        
                                            <dd>: 
                                                <?php if(isset($alluserprofile)){ echo $alluserprofile['user_religion']; } ?>                                          
                                            </dd>
                                        </dl>  
                                        <dl class="dl-horizontal">
                                            <dt><?php echo $this->lang->line('home_address'); ?></dt>                                        
                                            <dd>:
                                                <?php if(isset($alluserprofile)){ echo $alluserprofile['user_address']; } ?>                                          
                                            </dd>
                                        </dl> 
                                    </div>                            
                                </div>
                                <?php
                            }
                            else{
                            }
                            ?>
                            
                                
                            <div class="edit_profile" style="display: none;">
								<form action="<?php echo base_url('/master/saveEditProfileStudent') ?>" method="post">                            
	                                <input type="hidden" name="id_user" required class="input-sm form-control fg-input" value="<?php if(isset($alluserprofile)){ echo $alluserprofile['id_user']; } ?>">
	                                <div class="input-group fg-float">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-city"></i></span>
	                                    <div class="fg-line">
	                                        <input id="tempat_lahir" type="text" name="user_birthplace" class="input-sm form-control fg-input" value="<?php if(isset($alluserprofile)){ echo $alluserprofile['user_birthplace']; } ?>">
	                                        <label class="fg-label"><?php echo $this->lang->line('placeofbirth'); ?></label>
	                                    </div>                                    
	                                    <small id="cek_tempat_lahir" style="color: red;"></small>
	                                </div>

	                                <!-- <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-male-female"></i></span>
	                                    <?php 
	                                        //$flag = null;
	                                    	if (isset($alluserprofile)) {
	                                    		$flag = $alluserprofile['user_gender'];
	                                    	}
	                                    ?>
	                                        <label class="fg-label"><small><?php echo $this->lang->line('gender'); ?></small></label>
	                                        <select name="user_gender" class="select2 form-control">
	                                            <option disabled><?php echo $this->lang->line('select_gender'); ?></option>
	                                            <option value="Male" <?php if($flag=="Male"){ echo "selected='true'";} ?> ><?php echo $this->lang->line('male');?></option>
	                                            <option value="Female" <?php if($flag=="Female"){ echo "selected='true'";} ?>><?php echo $this->lang->line('women');?></option>
	                                        </select>                                        
	                                </div> -->


	                                <br/>  
	                                <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-library"></i></span>
	                                    <!-- <div class="select"> -->
	                                     <?php 
	                                        if (isset($alluserprofile)) {
	                                            $flag = $alluserprofile['user_religion'];
	                                        }
	                                    ?>
	                                        <label class="fg-label"><small><?php echo $this->lang->line('religion'); ?></small></label>
	                                        <select name="user_religion" class="select2 form-control" style="width: 100%">
	                                            <option disabled><?php echo $this->lang->line('select_religion'); ?></option>
	                                            <option value="Islam" <?php if($flag=="Islam"){ echo "selected='true'";} ?>>Islam</option>
	                                            <option value="Kristen Protestan" <?php if($flag=="Kristen Protestan"){ echo "selected='true'";} ?>>Kristen Protestan</option>
	                                            <option value="Kristen Katolik" <?php if($flag=="Kristen Katolik"){ echo "selected='true'";} ?>>Kristen Katolik</option>
	                                            <option value="Hindu" <?php if($flag=="Hindu"){ echo "selected='true'";} ?>>Hindu</option>
	                                            <option value="Buddha" <?php if($flag=="Buddha"){ echo "selected='true'";} ?>>Buddha</option>
	                                            <option value="Konghucu" <?php if($flag=="Konghucu"){ echo "selected='true'";} ?>>Konghucu</option>
	                                        </select>
	                                    <!-- </div> -->
	                                </div>

	                                <br> <br>                          
	                            
	                                <div class="input-group fg-float">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-home"></i></span>
	                                    <div class="fg-line">
	                                        <input id="alamat" type="text" name="user_address" class="input-sm form-control fg-input" value="<?php if(isset($alluserprofile)){ echo $alluserprofile['user_address']; } ?>">
	                                        <label class="fg-label"><?php echo $this->lang->line('home_address'); ?></label>
	                                    </div>  
	                                    <small id="cek_alamat" style="color: red;"></small>                                  
	                                </div>
	                                <br/><br>

	                                <!-- <blockquote class="m-b-25">
	                                    <p><?php echo $this->lang->line('school_jenjang'); ?></p>
	                                </blockquote>     -->                             

	                                
	                                <!-- <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
	                                    <select required id="jenjang_name"  class="select2 form-control" data-placeholder="<?php echo $this->lang->line('select_level'); ?>">
	                                         <option value=""></option>
	                                         <option value="<?php echo $alluserprofile['jenjang_name']; ?>" selected='true'>
	                                         <?php echo $alluserprofile['jenjang_name']; ?></option>
	                                     </select>
	                                   
	                                </div> -->

	                                <!-- <div class="input-group">
	                                    <span class="input-group-addon"><i class="zmdi zmdi-graduation-cap"></i></span>
	                                    <select required name="jenjang_id"  class="select2 form-control" id='jenjang_level' data-placeholder="<?php echo $this->lang->line('select_class'); ?>">
	                                            <option value=""></option>
	                                            <?php
	                                                $jen = $this->Master_model->getMasterJenjang($alluserprofile['jenjang_id']);
	                                                if($jen != ''){
	                                                    echo "<option value='".$jen['jenjang_id']."' selected='true'>".$jen['jenjang_name'].' '.$jen['jenjang_level']."</option>";
	                                                }

	                                            ?>
	                                        </select>
	                                  
	                                </div> -->               
	                                              

	                                <div class="card-padding card-body">                                                                           
	                                    <button id="btn_update_profile" type="submit" name="simpanedit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>                    
	                                </div>
	                            </form>                            	
                            </div>
                        </div>                         
                    </div>

                    <div class="pmb-block">
                        <div class="pmbb-header">
                        
                        </div>
                        <div class="pmbb-body p-l-30">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- akhir container -->
</section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
  <script type="text/javascript">
    $('#tempat_lahir').on('keyup',function(){
       if($('#tempat_lahir').val() == ('')){
            $('#cek_tempat_lahir').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');  
        }
        else{
                $('#cek_tempat_lahir').html('').css('color', 'red');
            }
    
    });
    $('#alamat').on('keyup',function(){
        if($('#alamat').val() == ('')){
            $('#cek_alamat').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');  
        }
        else{
                $('#cek_alamat').html('').css('color', 'red');
            }
        
    });
    $('#no_hp_ortu').on('keyup',function(){
        if($('#no_hp_ortu').val() == ('')){
            $('#cek_no_hp_ortu').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');  
        }
        else{
                $('#cek_no_hp_ortu').html('').css('color', 'red');
            }
    });
     $('#desa_id').on('change', function() {
            var cek_hape_ortu = $("#no_hp_ortu").val();
            var cek_tempat_lahir = $("#tempat_lahir").val();
            var cek_alamat = $("#alamat").val();
            
            // if (cek_hape_ortu!=null||cek_tempat_lahir!=null||cek_alamat!=null) {
            //     $("#btn_update_profile").attr('disabled','true');
            // }
            // else
            // {
            //  $("#btn_update_profile").attr('disabled','false');
            // }
     });
        
    </script>

<script>
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
    function validAngka(a)
    {
        if(!/^[0-9.]+$/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
      function validPhone(a)
    {
        if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
</script>
<script type="text/javascript">
        $(document).ready(function(){
            var cek_awal = "<?php echo $this->session->userdata('code_cek');?>";
            if (cek_awal == "889") {
                $(".edit_profile").css('display','block');
                $(".show_profile").css('display','none');
                $("#btn_edit_username").css('display','none');
                $("#btn_save_username").css('display','none');
            }else{
                $(".show_profile").css('display','block');
                $("#btn_edit_username").css('display','block');
            }
            var code_cek = null;
            var status_login = "<?php echo $this->session->userdata('status_user');?>";
            var cekk = setInterval(function(){
                code_cek = "<?php echo $this->session->userdata('code_cek');?>";
                
                cek();
            },500);
            // alert('aasdfsd');
            function cek(){
                if (code_cek == "889") {
                    $("#complete_modal").modal('show');
                    $("#alertcompletedata").css('display','block');
                    
                    code_cek == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>Rest/clearsession',
                        type: 'POST',
                        data: {
                            cek_code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
                else
                {
                    
                }
                // console.warn(code_cek);
                // console.warn(status_login);
            }

            
        });
        jQuery('#btn_edit_username').on('click', function(e){
            $("#btn_save_username").css('display','block');
            $(".edit_profile").css('display','block');
            $(".show_profile").css('display','none');
            $("#btn_edit_username").css('display','none');
            
        });
        jQuery('#btn_save_username').on('click', function(e){
            $("#btn_save_username").css('display','none');
            $(".edit_profile").css('display','none');
            $(".show_profile").css('display','block');
            $("#btn_edit_username").css('display','block');
            
        });
</script>

