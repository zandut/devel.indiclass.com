<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sidefinance'); ?>
		<?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
		?>
	</aside>

	<section id="content">
		<div class="container">            
			<div class="block-header">
				<h2>Payment Tutor</h2>
			</div>      		

			<div class="card" style="margin-top: 2%;">
            	<div class="card-body p-15">
            		<div class="row">
            			<form role="form" action="<?php  echo base_url(); ?>operator/paymentTutor" method="get" >
		            		<div class="col-md-12 col-xs-12">
		            			<div class="col-md-3">
		            				<label>Tutor</label>
		            				<select class="select2 form-control" id="tutor" name="tutor">
		                                <?php
                                        	$id = $this->session->userdata("id_user");
                                            $allsub = $this->db->query("SELECT * FROM `tbl_user` WHERE usertype_id='tutor' AND status='1'")->result_array();
                                            echo '<option selected="" value="">Semua Tutor</option>'; 
                                            foreach ($allsub as $row => $v) {                                            
                                                echo '<option value="'.$v['id_user'].'">'.$v['user_name'].'</option>';
                                            }
                                        ?>
		                            </select>
		            			</div>
		            			<div class="col-md-2">
		            				<label>Mata Pelajaran</label>
		            				<select class="select2 form-control" id="subject" name="subject">
		                                <?php                                        	                                         
                                            echo '<option selected="" value="">Semua Pelajaran</option>'; 
                                            $sub = $this->db->query("SELECT * FROM master_subject")->result_array();
                                            foreach ($sub as $row => $v) {

                                                $jenjangnamelevel = array();
                                                $jenjangid = json_decode($v['jenjang_id'], TRUE);
                                                if ($jenjangid != NULL) {
                                                    if(is_array($jenjangid)){
                                                        foreach ($jenjangid as $key => $value) {                                    
                                                            $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();
                                                            
                                                            if ($selectnew['jenjang_level']=='0') {
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
                                                            }
                                                            else{
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
                                                            }

                                                            
                                                        }   
                                                    }else{
                                                        $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
                                                        if ($selectnew['jenjang_level']=='0') {
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
                                                            }
                                                            else{
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
                                                            }                                                         
                                                    }                                       
                                                }

                                                echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.implode(", ",$jenjangnamelevel).'</option>';
                                            }
                                        ?>
		                            </select>
		            			</div>
		            			<div class="col-md-2">
		            				<label>Bulan</label>
		            				<select class="select2 form-control" id="month" name="month">
		            					<option selected value=''>Semua <?php echo $this->lang->line('monthbirth'); ?></option>
		                                <option value="1">Januari</option>
		                                <option value="2">Februari</option>
		                                <option value="3">Maret</option>
		                                <option value="4">April</option>
		                                <option value="5">Mei</option>
		                                <option value="6">Juni</option>
		                                <option value="7">Juli</option>
		                                <option value="8">Agustus</option>
		                                <option value="9">September</option>
		                                <option value="10">Oktober</option>
		                                <option value="11">November</option>
		                                <option value="12">Desember</option>
		                            </select>
		            			</div>
		            			<div class="col-md-2">
		            				<label>Tahun</label>
		            				<select name="years" id="years" class="select2 form-control">
	                                    <option selected value=''>Semua <?php echo $this->lang->line('yearbirth'); ?></option>
	                                    <?php 
	                                    for ($i=2017; $i <= 2018; $i++) {
	                                        ?>                                                 
	                                        <option value="<?php echo $i; ?>" <?php if(isset($form_cache) && $form_cache['tahun_lahir'] == $i){ echo 'selected="true"';} ?>><?php echo $i; ?></option>
	                                        <?php 
	                                    } 
	                                    ?>
	                                </select>
		            			</div>
		            			<div class="col-md-3">
		            				<label></label>
		            				<button class="btn bgm-bluegray waves-effect btn-block">Cari</button>
		            			</div>
		            		</div>
	            		</form>
            		</div>
            	</div>
            </div>

			<div class="card m-t-20 p-20" style="">				
                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12">                    	
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                	<?php 
                                		$search_sub 	= null;
                                		$search_tutor 	= null;
										$search_month 	= null;
										$search_years 	= null;
										$otherwhere 	= null;
	                                	if (isset($_GET['tutor']) || isset($_GET['subject']) || isset($_GET['month']) || isset($_GET['years'])) {
	                                		$search_tutor 	= $_GET['tutor'];
											$search_sub 	= $_GET['subject'];
											$search_month 	= $_GET['month'];
											$search_years 	= $_GET['years'];
											if ($search_tutor == "" || $search_sub == "" || $search_month == "" || $search_years == "") {
												$otherwhere = "";
											}
											if (!empty($search_tutor) && !empty($search_sub) && !empty($search_month) && !empty($search_years)) {
												$otherwhere .= "AND tc.tutor_id='".$search_tutor."' AND tc.subject_id='".$search_sub."' AND MONTH(tc.start_time) = '".$search_month."' AND YEAR(tc.start_time) = '".$search_years."'";
											}
											if (!empty($search_month) && !empty($search_years)) {
												$otherwhere .= "AND MONTH(tc.start_time) = '".$search_month."' AND YEAR(tc.start_time) = '".$search_years."'";
											}
											if (!empty($search_sub) && !empty($search_month)) {
												$otherwhere .= "AND tc.subject_id='".$search_sub."' AND MONTH(tc.start_time) = '".$search_month."'";
											}
											if (!empty($search_sub) && !empty($search_years)) {
												$otherwhere .= "AND tc.subject_id='".$search_sub."' AND YEAR(tc.start_time) = '".$search_years."'";	
											}				

											if (!empty($search_tutor) && !empty($search_month)) {
												$otherwhere .= "AND tc.tutor_id='".$search_tutor."' AND MONTH(tc.start_time) = '".$search_month."'";
											}
											if (!empty($search_tutor) && !empty($search_years)) {
												$otherwhere .= "AND tc.tutor_id='".$search_tutor."' AND YEAR(tc.start_time) = '".$search_years."'";	
											}											
											if(!empty($search_tutor)){
												$otherwhere .= " AND tc.tutor_id='".$search_tutor."'";
											}
											if(!empty($search_sub)){
												$otherwhere .= " AND tc.subject_id='".$search_sub."'";
											}
											if(!empty($search_month)){							
												$otherwhere .= "AND MONTH(tc.start_time) = '".$search_month."'";
											}
											if(!empty($search_years)){									
												$otherwhere .= "AND YEAR(tc.start_time) = '".$search_years."'";
											}																							
											else
											{
												$otherwhere .= "";
											}
										}
                                		$alllistpayment = $this->db->query("SELECT DISTINCT tcr.id_user, tu.user_name, tu.user_image, tc.class_id, tcr.class_ack, tcr.flag, tcr.notes, tc.name, tc.tutor_id, tc.subject_id, tc.description, tc.class_type, tc.start_time, tc.finish_time FROM `tbl_class_rating` as tcr INNER JOIN tbl_class as tc ON tcr.class_id=tc.class_id INNER JOIN tbl_user as tu ON tu.id_user=tc.tutor_id WHERE tcr.class_ack=1 AND (tc.class_type='multicast_paid' OR tc.class_type='private' OR tc.class_type='group') $otherwhere")->result_array();
                                	?>
                                    <tr>
                                        <th>No</th>
                                        <th>Tutor</th>
                                        <th>Murid</th>
                                        <th>Kelas</th>
                                        <th>Deskripsi Kelas</th>                                        
                                        <th>Tipe Kelas</th>
                                        <th>Mulai Kelas</th>
                                        <th>Akhir Kelas</th>
                                        <th>Status</th>
                                        <th>Harga</th>
                                        <th>Foto</th>
                                        <th>Note</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    foreach ($alllistpayment as $row => $v) { 
                                    	$id_user 		= $v['id_user'];
                                    	$user_name 		= $v['user_name'];
                                    	$class_ack 		= $v['class_ack'];
                                    	$class_id 		= $v['class_id'];
                                    	$flag 			= $v['flag'];
                                    	$notes 			= $v['notes'];
                                    	$name 			= $v['name'];
                                    	$tutor_id 		= $v['tutor_id'];
                                    	$subject_id 	= $v['subject_id'];
                                    	$description 	= $v['description'];
                                    	$class_type 	= $v['class_type'];
                                    	$start_time 	= $v['start_time'];
                                    	$finish_time 	= $v['finish_time'];
                                    	$foto  			= $v['user_image'];
                                    	$image 			= CDN_URL.USER_IMAGE_CDN_URL.$v['user_image'];
                                    	$user_utc 		= "420";
                                    	$server_utcc    = $this->Rumus->getGMTOffset();
							            $intervall      = $user_utc - $server_utcc;
							            $start_time     = DateTime::createFromFormat ('Y-m-d H:i:s',$start_time);
							            $start_time->modify("+".$intervall ." minutes");
							            $start_time     = $start_time->format('d-m-Y H:i:s');

							            $finish_time    = DateTime::createFromFormat ('Y-m-d H:i:s',$finish_time);
							            $finish_time->modify("+".$intervall ." minutes");
							            $finish_time    = $finish_time->format('d-m-Y H:i:s');
							            $namastudent 	= $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$id_user'")->row_array()['user_name'];
							            if ($class_type == 'multicast_paid') {
							            	$harga 		= $this->db->query("SELECT harga FROM tbl_class_price WHERE class_id='$class_id'")->row_array()['harga'];
							            	$hargaprice     = number_format($harga, 0, ".", ".");
							            }
							            else
							            {
							            	$harga 		= $this->db->query("SELECT harga FROM tbl_service_price WHERE subject_id='$subject_id' AND class_type='$class_type' AND tutor_id='$tutor_id'")->row_array()['harga'];
							            	$hargaprice     = number_format($harga, 0, ".", ".");
							            }
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $user_name; ?></td>
                                            <td><?php echo $namastudent; ?></td>
                                            <td><?php echo $name; ?></td>
                                            <td><?php echo $description; ?></td>                                            
                                            <td><?php echo $class_type; ?></td>
                                            <td><?php echo $start_time; ?></td>
                                            <td><?php echo $finish_time; ?></td>
                                            <td>
                                            	<?php
                                            		if($flag == 0){ echo "<label class='c-red'>Kelas telah selesai | Menunggu konfrim Operator";}else{ echo "<label class='c-green'>Sudah Terkonfrim</label>";}
                                            	?>
                                            </td>
                                            <td><?php echo 'Rp. '.$hargaprice; ?></td>
                                            <td>                                            	
                                    			<img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$foto; ?>" class="pv-main" style='width: 100px; height: 100px;' alt="">
                                            </td>       
                                            <td><?php echo $notes;?></td>                                     
                                            <td>
                                                <a class="card" href="#" data-classid="<?php echo $class_id;?>" id="clickCon"><button class="btn btn-success" title="Approve"><i class="zmdi zmdi-mail-reply"></i></button></a>
                                            </td>                                                                                           
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div> 		

			<div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="confrimPayment" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <!-- <h4 class="modal-title c-black">Konfirmasi Join</h4>
                            <hr> -->
                            <p><label class="c-black f-15">Apakah kelas tersebut sudah terbayarkan ke pihak tutor?</label></p>
                            <textarea rows="4" class="form-control" placeholder="Isi Note disini" id="notes"></textarea>
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" id="proccessconfrim" class_id="">Ya</button>
                        </div>                        
                    </div>
                </div>
            </div>

		</section>
	</section>

	<footer id="footer">
		<?php $this->load->view('inc/footer'); ?>
	</footer>
	<script type="text/javascript">		
		$("#clickCon").click(function(){
			var classid = $(this).data('classid');
			$("#proccessconfrim").attr('class_id',classid);
			$("#confrimPayment").modal("show");
		});

		$("#proccessconfrim").click(function(){
			var class_id 	= $(this).attr('class_id');
			var notes 		= $("#notes").val();
			if(notes == ""){
				notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut', 'Harap isi Note terlebih dahulu');
			}
			else
			{
				$.ajax({
	                url: '<?php echo base_url(); ?>process/confrimPaymentFlag',
	                type: 'POST',
	                data: {
	                    class_id: class_id,
	                    notes: notes
	                },               
	                success: function(data)
	                { 
	                	var code = data['status'];
	                	if (code == 1) {
	                		notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',data['message']);
	                		setTimeout(function(){
	                			location.reload();
	                		},1000);
	                	}
	                	else
	                	{
	                		notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
	                		setTimeout(function(){
	                			location.reload();
	                		},1000);
	                	}
	                }
				});
			}
		});

		$('table.display').DataTable( {
	        fixedHeader: {
	            header: true,
	            footer: true
	        }
	    } );
	    $('.data').DataTable(); 
	</script>