<style type="text/css">
    /* Flashing */
    .hover13:hover img {
        opacity: 1;
        -webkit-animation: flash 1.5s;
        animation: flash 1.5s;
    }
    @-webkit-keyframes flash {
        0% {
            opacity: .4;
        }
        100% {
            opacity: 1;
        }
    }
    @keyframes flash {
        0% {
            opacity: .4;
        }
        100% {
            opacity: 1;
        }
    }
    .hover11 img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .hover11:hover img {
        opacity: 0.5;
    }
</style>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

     ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/sidefinance'); ?>
        <?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
        ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="block-header">
                <h2>Invoice Package Invoce<small>list data invoice paket yang sudah terisi data rekening pengirim</small></h2>
            </div> <!-- akhir block header    --> 

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card col-md-12">
                <div class="card-header">
                    <h2>List Package Invoice</h2>
                </div>
                <div class="card-body card-padding">
                    <div class="row">

                        <div class="col-md-12 table-responsive">
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr style="border: 1px solid black;">
                                    	<th>No</th> 
                                        <th>Invoice ID</th>
                                        <th>Order ID</th>
                                        <th>Order Name</th>
                                        <th colspan="3"><center>Akun Bank</center></th>
                                        <th>Harga</th>
                                        <th>Kode unik</th>
                                        <th>Total harga</th>                                        
                                        <!-- <th>Action</th> -->
                                    </tr>
                                    <tr>
                                        <th colspan="3"></th>
                                        <th>Nama</th>
                                        <th>No Rekening</th>
                                        <th>Bank</th>
                                        <th colspan="3"></th>
                                    </tr>
                                </thead>                                
                                <tbody>
                                <?php 
                                	$no = 1;
                                    $log_mutasi = $this->db->query("SELECT tu.user_name, ti.*, tor.*, trk.* FROM tbl_invoice as ti INNER JOIN tbl_order as tor ON ti.order_id=tor.order_id INNER JOIN tbl_rekening as trk ON ti.id_rekening=trk.id_rekening INNER JOIN tbl_user as tu ON tor.buyer_id=tu.id_user WHERE ti.id_rekening IS NOT NULL AND ti.payment_type IS NOT NULL AND (order_status=2 OR order_status=-3)")->result_array();
                                    if (empty($log_mutasi)) {
                                        ?>
                                        <tr>
                                            <td colspan="10" align="center">Tidak ada data</td>
                                        </tr>
                                        <?php
                                    }
                                    foreach ($log_mutasi as $row => $r) {
                                        $harga = $r['total_price']-$r['unix_code'];                                        
                                        $hargaa  = number_format($harga, 0, ".", ".");
                                        $totalharga  = number_format($r['total_price'], 0, ".", ".");      
                                ?>
                                    <tr>
                                        <td><?php echo($no); ?></td>
                                        <td><?php echo $r['invoice_id']; ?></td>
                                        <td><?php echo $r['order_id']; ?></td>
                                        <td><?php echo $r['user_name']; ?></td>
                                        <td><?php echo $r['nama_akun']; ?></td>
                                        <td><?php echo $r['nomer_rekening']; ?></td>
                                        <td><?php echo $r['nama_bank']; ?></td>
                                        <td><?php echo "Rp ".$hargaa; ?></td>
                                        <td><?php echo $r['unix_code']; ?></td>
                                        <td><?php echo "Rp ".$totalharga; ?></td>                                        
                                    </tr>            
                                </tbody>
                                <?php
                                	$no++;
                                    }
                                ?>
                            </table>
                        </div>
                        <!-- <label class="c-red m-t-15">* Mohon untuk perhatikan antara Total Payment dan Total Transfer. Bila Total Transfer tidak sama dengan Total Payment harap Tidak menekan tombol Done</label> -->
                        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">              
						      <div class="modal-body">
						      	<br><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><br><br><br>
						        <img src="" class="imagepreview" style="width: 100%;" ><br><br><br>
						      </div>
						    </div>
						  </div>
						</div>

                    </div>
                </div>
            </div>
            
            <!-- Modal TAMBAH -->  
            <div class="modal fade" id="modalVerify" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Verify Data Transaction</h4>
                            <hr>
                        </div>                        
                        <div class="modal-body">
                            <div class="card">
                            <div class="row p-15">     
                            	<input type="text" name="trxid" id="trxid" class="c-gray" hidden>                            
                                <input type="text" name="rtp" id="rtp" class="c-gray" hidden> 
                                <input type="text" name="id_user_requester" id="id_user_requester" class="c-gray">                            
                                <input type="text" name="tutor_id" id="tutor_id" class="c-gray" hidden>
                                <input type="text" name="status" id="status" class="c-gray" hidden>
                                <input type="text" name="class_id" id="class_id" class="c-gray" hidden>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Jumlah Transfer</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <!-- <input type="text" name="trf_amount" id="trf_amount" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />  -->

	                                            <input type="text" name="nominaltransaksi" id="nominal" required class="input-sm form-control fg-input" placeholder="Isi Jumlah transfer" minlength="9" step="500" maxlength="14" min="10000" max="500000" style="border: 1px solid #BDBDBD; padding: 3px;"/>
	                                            <input type="text" name="trf_amount" id="trf_amount" hidden />  

                                                <label class="c-gray" id="totalamount"></label>
                                            </div>                                                
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nama Pengirim</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="buyer_name" id="buyer_name" class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">No Rekening Pengirim</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="buyer_norek" id="buyer_norek" validAngka(this)" onkeypress="return hanyaAngka(event)" class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Berita</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">                                                
                                                <textarea rows="4" class="form-control input-sm" id="berita" name="berita" style="border: 1px solid #BDBDBD; padding: 3px;"></textarea>  
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Deskripsi</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <textarea rows="4" class="form-control input-sm" id="description" name="description" style="border: 1px solid #BDBDBD; padding: 3px;"></textarea>    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
								
                            </div>
                            <label class="c-red">* Jumlah Transfer tidak boleh kosong</label>
                            <label class="c-red">* Mohon untuk perhatikan antara Total Payment dan Total Transfer. Bila Total Transfer tidak sama dengan Total Payment harap Tidak menekan tombol Done</label>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn bgm-green" id="verifytransaction">Konfirmasi</button>
                            
                        </div>                        
                    </div>
                </div>
            </div> 

        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function() {
    	var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        $('table.display').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        $('.data').DataTable();

        $('.pop').on('click', function() {
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#imagemodal').modal('show');   
		});	

        function validAngka(a)
        {
            if(!/^[0-9.]+$/.test(a.value))
            {
            a.value = a.value.substring(0,a.value.length-31);
            }
        }
        function hanyaAngka(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
          return true;
        }
    
		$('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $("#nominal").keyup(function() {
          var clone = $(this).val();
          var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
          $("#trf_amount").val(cloned);
        });

        $(".confrimuangsaku").on("click", function(){
            var trxid = $(this).data('trx');
            var rtp = $(this).data('rtp');
            var id_user_requester = $(this).data('iduser');
            var tutor_id = $(this).data('tutorid');
            var status = $(this).data('status');
            var class_id = $(this).data('classid');
            var totalpayment = $(this).data('totalpayment');
            $("#trxid").val(trxid);        
            $("#rtp").val(rtp);
            $("#id_user_requester").val(id_user_requester);        
            $("#tutor_id").val(tutor_id);
            $("#status").val(status);
            $("#class_id").val(class_id);
            $("#totalamount").text("Total yang harus di transfer Rp. "+totalpayment);
            $("#modalVerify").modal("show");
         //    $.ajax({
        	// 	url:'<?php echo base_url();?>Rest/GetDataVerify/access_token/'+tokenjwt,
        	// 	type:'POST',
        	// 	data: {
        	// 		trx_id : trxid,
         //            status : status
        	// 	},
        	// 	success: function(response){        			
        	// 		var jmlh = response['message'];        			
        	// 		$("#totalamount").text("Total yang harus di transfer Rp. "+jmlh);
        	// 		$("#modalVerify").modal("show");
        	// 	}
        	// });
            
        });

        $("#verifytransaction").on("click", function(){        	
        	var trx_id 			= $("#trxid").val();
            var rtp             = $("#rtp").val();
            var id_user_requester= $("#id_user_requester").val();
            var tutor_id        = $("#tutor_id").val();
            var status          = $("#status").val();
        	var trf_amount 		= $("#trf_amount").val();
        	var berita 			= $("#berita").val();
        	var description		= $("#description").val();
        	var buyer_norek 	= $("#buyer_norek").val();
        	var buyer_name		= $("#buyer_name").val();
            var class_id        = $("#class_id").val();

        	$.ajax({
        		url:'<?php echo base_url();?>Rest/VerifyTransaction/access_token/'+tokenjwt,
        		type:'POST',
        		data: {
        			trx_id :  trx_id,
        			trf_amount : trf_amount,
        			berita : berita,
        			description : description,
        			buyer_norek : buyer_norek,
        			buyer_name : buyer_name,
                    id_user_requester: id_user_requester,
                    status: status
        		},
        		success: function(response){
        			var stat = response['code'];
                    if (stat == 200) {

                        if(status == "multicast")
                        {
                            $.ajax({
                                url :"<?php echo base_url() ?>Process/ConfirmPaymentMulticast",
                                type:"POST",
                                data: {
                                    trx_id :  trx_id,
                                    tutor_id:tutor_id,
                                    id_user:id_user_requester,
                                    class_id: class_id
                                },
                                success: function(response){             
                                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Verify and Create class");
                                    setTimeout(function(){
                                        location.reload(); 
                                    },2000);
                                                        
                                } 
                            });
                        }
                        else
                        {
                            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Failed Verify and Create class");
                            setTimeout(function(){
                                location.reload(); 
                            },1500);
                        }
                        

                        // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Verify Success");
                        // setTimeout(function(){
                        //     location.reload();
                        // },1500);
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Failed Verify");
                        setTimeout(function(){
                            location.reload();
                        },1500);
                    }
        		}
        	});

        });
    } );
</script>
    