<style type="text/css">
    /* Flashing */
    .hover13:hover img {
        opacity: 1;
        -webkit-animation: flash 1.5s;
        animation: flash 1.5s;
    }
    @-webkit-keyframes flash {
        0% {
            opacity: .4;
        }
        100% {
            opacity: 1;
        }
    }
    @keyframes flash {
        0% {
            opacity: .4;
        }
        100% {
            opacity: 1;
        }
    }
    .hover11 img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .hover11:hover img {
        opacity: 0.5;
    }
</style>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

     ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/sideoperator'); ?>
        <?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
        ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="block-header">
                <h2>Confirm On Demand</h2>
            </div> <!-- akhir block header    --> 

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card col-md-12">

                <div class="card-body card-padding">
                    <div class="row">

                        <div class="col-md-12">
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                    	<th>No</th> 
                                        <th>Order ID</th>
                                        <th>Nama</th>
                                        <th width="120px">Total Payment</th>
                                        <th width="120px">Total Transfer</th>
                                        <th>Bank</th>
                                        <th>No Rekening</th>
                                        <th>Metode Payment</th>
                                        <th>Bukti Payment</th>
                                        <th>Time</th>
                                        <th>Tipe Kelas</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>                                
                                <tbody>
                                <?php 
                                	$no = 1;
                                    $gettrftransaction = $this->db->query("SELECT lt.credit, ltc.*, mb.bank_name, tu.user_name FROM log_trf_confirmation as ltc LEFT JOIN master_bank as mb ON ltc.bank_id=mb.bank_id LEFT JOIN tbl_user as tu ON ltc.id_user=tu.id_user LEFT JOIN log_transaction as lt ON lt.trx_id=ltc.trx_id WHERE ltc.accepted='0' AND ltc.type_confirm='on_demand'")->result_array();
                                    foreach ($gettrftransaction as $row => $r) {                                        
                                        $jumlahpayment  = number_format($r['credit'], 0, ".", ".");
                                        $totaltransfer  = number_format($r['jumlah'], 0, ".", ".");      
                                        $getrtp         = $this->db->query("SELECT product_id FROM tbl_order_detail WHERE order_id='$r[trx_id]'")->row_array()['product_id'];
                                        $getdatareq     = $this->db->query("SELECT tr.id_user_requester as tr_id_user, tr.tutor_id as tr_tutor_id, trg.id_user_requester as trg_id_user, trg.tutor_id as trg_tutor_id FROM  tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON tod.product_id=trg.request_id WHERE tod.product_id='$getrtp'")->row_array();
                                        $id_user_requester = $getdatareq['tr_id_user'] != null ? $getdatareq['tr_id_user'] : $getdatareq['trg_id_user'];
                                        $tutor_id       = $getdatareq['tr_tutor_id'] != null ? $getdatareq['tr_tutor_id'] : $getdatareq['trg_tutor_id'];
                                        $status       = $getdatareq['tr_tutor_id'] != null ? "private" : "group";
                                ?>
                                    <tr>
                                    	<td><?php echo($no); ?></td>
                                        <td><?php echo $r['trx_id']; ?></td>
                                        <td><?php echo $r['user_name']; ?></td>
                                        <td>Rp. <?php echo $jumlahpayment; ?></td>
                                        <td>Rp. <?php echo $totaltransfer; ?></td>
                                        <td><?php echo $r['bank_name'] ?></td>
                                        <td><?php echo $r['norek_tr']; ?></td>
                                        <td><?php echo $r['metod_tr']; ?></td>
                                        <td class="hover11"><a href="#" class="pop"><img src="<?php echo base_url('aset/img/buktipembayaran/'.$r['bukti_tr']); ?>" width="100px" height="100px"></a></td>
                                        <td><?php echo $r['time']; ?></td>
                                        <td><?php echo $status;?></td>
                                        <td><center><a id="confrimuangsaku" class="confrimuangsaku" data-status="<?php echo $status;?>" data-iduser="<?php echo $id_user_requester;?>" data-tutorid="<?php echo $tutor_id;?>" data-rtp="<?php echo $getrtp;?>" data-trx="<?php echo $r['trx_id']; ?>"><button class="btn btn-success"><i class="zmdi zmdi-check zmdi-lg"></i> Verify</button></a></center></td>
                                    </tr>            
                                </tbody>
                                <?php
                                	$no++;
                                    }
                                ?>
                            </table>
                        </div>
                        <label class="c-red m-t-15">* Mohon untuk perhatikan antara Total Payment dan Total Transfer. Bila Total Transfer tidak sama dengan Total Payment harap Tidak menekan tombol Done</label>
                        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">              
						      <div class="modal-body">
						      	<br><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><br><br><br>
						        <img src="" class="imagepreview" style="width: 100%;" ><br><br><br>
						      </div>
						    </div>
						  </div>
						</div>

                    </div>
                </div>
            </div>

            <!-- Modal TAMBAH -->  
            <div class="modal fade" id="modalVerify" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Verify Data Transaction</h4>
                            <hr>
                        </div>                        
                        <div class="modal-body">
                            <div class="card">
                            <div class="row p-15">     
                            	<input type="text" name="trxid" id="trxid" class="c-gray" hidden>                            
                                <input type="text" name="rtp" id="rtp" class="c-gray" hidden> 
                                <input type="text" name="id_user_requester" id="id_user_requester" class="c-gray">                            
                                <input type="text" name="tutor_id" id="tutor_id" class="c-gray" hidden>
                                <input type="text" name="status" id="status" class="c-gray" hidden>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Jumlah Transfer</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <!-- <input type="text" name="trf_amount" id="trf_amount" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />  -->

	                                            <input type="text" name="nominaltransaksi" id="nominal" required class="input-sm form-control fg-input" placeholder="Isi Jumlah transfer" minlength="9" step="500" maxlength="14" min="10000" max="500000" style="border: 1px solid #BDBDBD; padding: 3px;"/>
	                                            <input type="text" name="trf_amount" id="trf_amount" hidden />  

                                                <label class="c-gray" id="totalamount"></label>
                                            </div>                                                
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nama Pengirim</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="buyer_name" id="buyer_name" class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">No Rekening Pengirim</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="buyer_norek" id="buyer_norek" class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Berita</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">                                                
                                                <textarea rows="4" class="form-control input-sm" id="berita" name="berita" style="border: 1px solid #BDBDBD; padding: 3px;"></textarea>  
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Deskripsi</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <textarea rows="4" class="form-control input-sm" id="description" name="description" style="border: 1px solid #BDBDBD; padding: 3px;"></textarea>    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
								
                            </div>
                            <label class="c-red">* Jumlah Transfer tidak boleh kosong</label>
                            <label class="c-red">* Mohon untuk perhatikan antara Total Payment dan Total Transfer. Bila Total Transfer tidak sama dengan Total Payment harap Tidak menekan tombol Done</label>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn bgm-green" id="verifytransaction">Konfirmasi</button>
                            
                        </div>                        
                    </div>
                </div>
            </div> 

        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function() {
    	var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        $('table.display').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        $('.data').DataTable();

        $('.pop').on('click', function() {
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#imagemodal').modal('show');   
		});	

		$('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $("#nominal").keyup(function() {
          var clone = $(this).val();
          var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
          $("#trf_amount").val(cloned);
        });

        $(".confrimuangsaku").on("click", function(){
            var trxid = $(this).data('trx');
            var rtp = $(this).data('rtp');
            var id_user_requester = $(this).data('iduser');
            var tutor_id = $(this).data('tutorid');
            var status = $(this).data('status');
            $("#trxid").val(trxid);        
            $("#rtp").val(rtp);
            $("#id_user_requester").val(id_user_requester);        
            $("#tutor_id").val(tutor_id);
            $("#status").val(status);
            $.ajax({
        		url:'<?php echo base_url();?>Rest/GetDataVerify/access_token/'+tokenjwt,
        		type:'POST',
        		data: {
        			trx_id : trxid,
                    status : status
        		},
        		success: function(response){        			
        			var jmlh = response['message'];        			
        			$("#totalamount").text("Total yang harus di transfer Rp. "+jmlh);
        			$("#modalVerify").modal("show");
        		}
        	});
            
        });

        $("#verifytransaction").on("click", function(){        	
        	var trx_id 			= $("#trxid").val();
            var rtp             = $("#rtp").val();
            var id_user_requester= $("#id_user_requester").val();
            var tutor_id        = $("#tutor_id").val();
            var status          = $("#status").val();
        	var trf_amount 		= $("#trf_amount").val();
        	var berita 			= $("#berita").val();
        	var description		= $("#description").val();
        	var buyer_norek 	= $("#buyer_norek").val();
        	var buyer_name		= $("#buyer_name").val();
        	$.ajax({
        		url:'<?php echo base_url();?>Rest/VerifyTransaction/access_token/'+tokenjwt,
        		type:'POST',
        		data: {
        			trx_id :  trx_id,
        			trf_amount : trf_amount,
        			berita : berita,
        			description : description,
        			buyer_norek : buyer_norek,
        			buyer_name : buyer_name,
                    id_user_requester: id_user_requester,
                    status: status
        		},
        		success: function(response){
        			var stat = response['code'];
                    if (stat == 200) {

                        if (status == "private") {
                            $.ajax({
                                url :"<?php echo base_url() ?>Process/ConfirmPaymentPrivate",
                                type:"POST",
                                data: {
                                    trx_id :  trx_id,
                                    rtp: rtp,
                                    tutor_id:tutor_id,
                                    id_user:id_user_requester
                                },
                                success: function(html){             
                                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Verify and Create class");
                                    setTimeout(function(){
                                        location.reload(); 
                                    },2000);                        
                                } 
                            });
                        }
                        else
                        {
                            $.ajax({
                                url :"<?php echo base_url() ?>Process/ConfirmPaymentGroup",
                                type:"POST",
                                data: {
                                    trx_id :  trx_id,
                                    rtp: rtp,
                                    tutor_id:tutor_id,
                                    id_user:id_user_requester
                                },
                                success: function(response){             
                                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Verify and Create class");
                                    setTimeout(function(){
                                        location.reload(); 
                                    },2000);
                                                        
                                } 
                            });
                        }
                        

                        // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Verify Success");
                        // setTimeout(function(){
                        //     location.reload();
                        // },1500);
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Failed Verify");
                        setTimeout(function(){
                            location.reload();
                        },1500);
                    }
        		}
        	});

        });
    } );
</script>
    