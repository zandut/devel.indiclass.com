<div ng-show="loding" class="page-loader" style="background-color:black; opacity: 0.6;">
    <div class="preloader pl-lg pls-white">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p style="margin-left: -18px;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
    </div>
</div>
<style type="text/css">
    /* Flashing */
    .hover13:hover img {
        opacity: 1;
        -webkit-animation: flash 1.5s;
        animation: flash 1.5s;
    }
    @-webkit-keyframes flash {
        0% {
            opacity: .4;
        }
        100% {
            opacity: 1;
        }
    }
    @keyframes flash {
        0% {
            opacity: .4;
        }
        100% {
            opacity: 1;
        }
    }
    .hover11 img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .hover11:hover img {
        opacity: 0.5;
    }
</style>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

     ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/sideoperator'); ?>
        <?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
        ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="block-header">
                <h2>Confirm Pending</h2>
            </div> <!-- akhir block header    --> 

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card col-md-12">
                <div class="card-header">
                    <h2>List Order Pending</h2>
                </div>
                <div class="card-body card-padding">
                    <div class="row">

                        <div class="col-md-12 table-responsive">
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                    	<th>No</th> 
                                        <th>Bank</th>
                                        <th>Nomer bank rekening</th>
                                        <th>Tanggal Pembayaran</th>
                                        <th>Deskripsi</th>
                                        <th>Uang Masuk</th>
                                        <th>Total Uang Setelah Dana Masuk</th>                                        
                                        <th >Action</th>
                                        <th></th>
                                    </tr>
                                </thead>                                
                                <tbody>
                                <?php 
                                	$no = 1;
                                    $log_mutasi = $this->db->query("SELECT lmb.*, lmb.bank_id as mbi, mb.* FROM log_mutasi_bank as lmb INNER JOIN master_bank as mb ON lmb.bank_id=mb.no WHERE lmb.status=2")->result_array();
                                    foreach ($log_mutasi as $row => $r) {                                        
                                        $uangmasuk  = number_format($r['credit_amount'], 0, ".", ".");
                                        $totaluang  = number_format($r['balance_after_credit'], 0, ".", ".");      
                                ?>
                                    <tr>
                                    	<td><?php echo($no); ?></td>
                                        <td><?php echo $r['bank_name'];?></td>
                                        <td><?php echo $r['bank_rekening']; ?></td>
                                        <td><?php echo $r['date_field']; ?></td>
                                        <td><?php echo $r['description_field']; ?></td>
                                        <td>Rp. <?php echo $uangmasuk; ?></td>
                                        <td>Rp. <?php echo $totaluang; ?></td>                                        
                                        <td>
                                            <button class='aa btn btn-success btn-block' id='action' bi='<?php echo $r['mbi'];?>' df='<?php echo $r['date_field'];?>' ca='<?php echo $r['credit_amount']; ?>' def="<?php echo $r['description_field'];?>" bac="<?php echo $r['balance_after_credit'];?>" title='Terima'>Proses</button>
                                        </td>
                                        <td>
                                            <a data-toggle='modal' class='bb m-t-5' request_id='"+request_id+"' id="reject" bi='<?php echo $r['mbi'];?>' df='<?php echo $r['date_field'];?>' ca='<?php echo $r['credit_amount']; ?>' def="<?php echo $r['description_field'];?>" bac="<?php echo $r['balance_after_credit'];?>"><button class='aa-t btn btn-danger btn-block' request_id='"+request_id+"' title='Hapus'><?php echo $this->lang->line('tolak'); ?></button></a>
                                        </td>
                                    </tr>            
                                </tbody>
                                <?php
                                	$no++;
                                    }
                                ?>
                            </table>
                        </div>
                        <label class="c-red m-t-15">* Mohon untuk perhatikan antara Total Payment dan Total Transfer. Bila Total Transfer tidak sama dengan Total Payment harap Tidak menekan tombol Done</label>
                        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">              
						      <div class="modal-body">
						      	<br><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><br><br><br>
						        <img src="" class="imagepreview" style="width: 100%;" ><br><br><br>
						      </div>
						    </div>
						  </div>
						</div>

                    </div>
                </div>
            </div>

            <!-- Modal TAMBAH -->  
            <div class="modal fade" id="modalVerify" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Verify Transaction</h4>
                            <hr>
                        </div>                        
                        <div class="modal-body">
                            <div class="card">
                            <div class="row p-15">     
                            	<input type="hidden" name="bank_id" id="bank_id" class="c-gray" >                            
                                <input type="hidden" name="date_field" id="date_field" class="c-gray" >
                                <input type="hidden" name="credit_amount" id="credit_amount" class="c-gray">
                                <input type="hidden" name="description_field" id="description_field" class="c-gray">
                                <input type="hidden" name="balance_after_credit" id="balance_after_credit" class="c-gray" >                                
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Invoice ID</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <select class='select2 form-control' required id="invoice_id" name="invoice_id" style="width: 100%;">
                                                    <?php                                                        
                                                        $allinvoice = $this->db->query("SELECT * FROM tbl_invoice as ti INNER JOIN tbl_order as tor ON ti.order_id=tor.order_id WHERE tor.order_status=2 OR tor.order_status=0")->result_array();
                                                        echo '<option disabled="disabled" selected="" value="">Pilih Invoice</option>'; 
                                                        foreach ($allinvoice as $row => $v) {                                            
                                                            echo '<option value="'.$v['invoice_id'].'">'.$v['invoice_id'].'</option>';
                                                        }
                                                    ?>
                                                </select>    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Status Pembayaran</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <select class='select2 form-control' required id="aksi" name="aksi" style="width: 100%;">
                                                    <option disabled="disabled" selected="" value="">Pilih Status</option>
                                                    <option value="1">Pelunasan Pembayaran</option>
                                                    <option value="2">Pembayaran Kurang</option>
                                                </select>
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Keterangan</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">                                                
                                                <textarea rows="4" class="form-control input-sm" id="ket" name="ket" style="border: 1px solid #BDBDBD; padding: 3px;"></textarea>  
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>                                							
                            </div>
                            <<!-- label class="c-red">* Jumlah Transfer tidak boleh kosong</label>
                            <label class="c-red">* Mohon untuk perhatikan antara Total Payment dan Total Transfer. Bila Total Transfer tidak sama dengan Total Payment harap Tidak menekan tombol Done</label> -->
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn bgm-green" id="verifytransaction">Konfirmasi</button>
                            
                        </div>                        
                    </div>
                </div>
            </div> 

            <!-- Modal Tolak -->
            <div class="modal fade" id="modalReject" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Reject Transaction</h4>
                            <hr>
                        </div>                        
                        <div class="modal-body">
                            <div class="card">
                                <div class="row p-15">     
                                    <input type="hidden" name="rjt_bank_id" id="rjt_bank_id" class="c-gray" >                            
                                    <input type="hidden" name="rjt_date_field" id="rjt_date_field" class="c-gray" >
                                    <input type="hidden" name="rjt_credit_amount" id="rjt_credit_amount" class="c-gray">
                                    <input type="hidden" name="rjt_description_field" id="rjt_description_field" class="c-gray">
                                    <input type="hidden" name="rjt_balance_after_credit" id="rjt_balance_after_credit" class="c-gray" >                                
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Invoice ID</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group" style="margin-left: 4%;">                       
                                                <div class="fg-line c-gray">
                                                    <select class='select2 form-control' required id="rjt_invoice_id" name="rjt_invoice_id" style="width: 100%;">
                                                        <?php                                                        
                                                            $allinvoice = $this->db->query("SELECT * FROM tbl_invoice as ti INNER JOIN tbl_order as tor ON ti.order_id=tor.order_id WHERE tor.order_status=-2")->result_array();
                                                            echo '<option disabled="disabled" selected="" value="">Pilih Invoice</option>'; 
                                                            foreach ($allinvoice as $row => $v) {                                            
                                                                echo '<option value="'.$v['invoice_id'].'">'.$v['invoice_id'].'</option>';
                                                            }
                                                        ?>
                                                    </select>    
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>                                
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Keterangan</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group fg-float" style="margin-left: 4%;">                       
                                                <div class="fg-line c-gray">                                                
                                                    <textarea rows="4" class="form-control input-sm" id="rjt_ket" name="rjt_ket" style="border: 1px solid #BDBDBD; padding: 3px;"></textarea>  
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>                                                          
                                </div>                            
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn bgm-green" id="rejecttransaction">Tolak</button>
                            
                        </div>                        
                    </div>
                </div>
            </div> 

        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function() {
    	var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        $('table.display').DataTable({
            fixedHeader: {
                header: true,
                footer: true
            }
        });

        $('.data').DataTable();

        $('.pop').on('click', function() {
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#imagemodal').modal('show');   
		});	

        function validAngka(a)
        {
            if(!/^[0-9.]+$/.test(a.value))
            {
            a.value = a.value.substring(0,a.value.length-31);
            }
        }
        function hanyaAngka(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
          return true;
        }
    
		$('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $("#nominal").keyup(function() {
            var clone = $(this).val();
            var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
            $("#trf_amount").val(cloned);
        });

        $(".aa").on("click", function(){            
            var bi = $(this).attr('bi');
            var df = $(this).attr('df');
            var ca = $(this).attr('ca');
            var def = $(this).attr('def');
            var bac = $(this).attr('bac');
            $("#bank_id").val(bi);        
            $("#date_field").val(df);
            $("#description_field").val(def);
            $("#credit_amount").val(ca);
            $("#balance_after_credit").val(bac);         
            $("#modalVerify").modal("show");
        });

        $(".bb").on("click", function(){            
            var bi = $(this).attr('bi');
            var df = $(this).attr('df');
            var ca = $(this).attr('ca');
            var def = $(this).attr('def');
            var bac = $(this).attr('bac');
            $("#rjt_bank_id").val(bi);        
            $("#rjt_date_field").val(df);
            $("#rjt_description_field").val(def);
            $("#rjt_credit_amount").val(ca);
            $("#rjt_balance_after_credit").val(bac);         
            $("#modalReject").modal("show");
            
        });

        $("#verifytransaction").on("click", function(){        	
        	var bank_id 			= $("#bank_id").val();
            var date_field          = $("#date_field").val();
            var description_field   = $("#description_field").val();
            var credit_amount       = $('#credit_amount').val();
            var balance_after_credit= $("#balance_after_credit").val();
            var invoice_id          = $("#invoice_id").val();
        	var aksi 		        = $("#aksi").val();
        	var ket 			    = $("#ket").val();        	

        	$.ajax({
        		url:'<?php echo base_url();?>Operator/OperatorAction',
        		type:'POST',
        		data: {
        			bi :  bank_id,
        			df : date_field,
        			def : description_field,
                    ca : credit_amount,
        			bac : balance_after_credit,
        			invoice_id : invoice_id,
        			aksi : aksi,
                    ket: ket
        		},
        		success: function(response){
                    response    = JSON.parse(response);
                    console.warn(response);
                    aksi        = response['aksi'];
                    if(aksi == 1){
                        // KASIH LOADING
                        $("#modalVerify").modal("hide");
                        angular.element(document.getElementById('verifytransaction')).scope().loadings(); 
                        for (var i = 0; i < response.data.length; i++) {
                            var trx_id = response.data[i]['trx_id'];
                            var id_user = response.data[i]['id_user'];
                            var tutor_id = response.data[i]['tutor_id'];
                            var class_id = response.data[i]['class_id'];
                            var rtp = response.data[i]['rtp'];
                            var type = response.data[i]['type'];
                            
                            if(type == "multicast") {
                                $.ajax({
                                    url :"<?php echo base_url() ?>Process/ConfirmPaymentMulticast",
                                    type:"POST",
                                    data: {
                                        trx_id:  trx_id,
                                        tutor_id: tutor_id,
                                        id_user: id_user,
                                        class_id: class_id
                                    },
                                    success: function(response){  
                                        angular.element(document.getElementById('verifytransaction')).scope().loadingsclose();           
                                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Verify and Create class");
                                        setTimeout(function(){
                                            location.reload(); 
                                        },2000);
                                    } 
                                });
                            }else if (type == "private") {
                                $.ajax({
                                    url :"<?php echo base_url() ?>Process/ConfirmPaymentPrivate",
                                    type:"POST",
                                    data: {
                                        trx_id:  trx_id,
                                        rtp: rtp,
                                        tutor_id: tutor_id,
                                        id_user: id_user
                                    },
                                    success: function(html){
                                        angular.element(document.getElementById('verifytransaction')).scope().loadingsclose();
                                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Verify and Create class");
                                        setTimeout(function(){
                                            location.reload(); 
                                        },2000);
                                    } 
                                });
                            }else if (type == "group") {
                                $.ajax({
                                    url :"<?php echo base_url() ?>Process/ConfirmPaymentGroup",
                                    type:"POST",
                                    data: {
                                        trx_id: trx_id,
                                        rtp: rtp,                                        
                                        id_user: id_user
                                    },
                                    success: function(response){             
                                        //TUTUP MODAL
                                        angular.element(document.getElementById('verifytransaction')).scope().loadingsclose();
                                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Verify and Create class");
                                        setTimeout(function(){
                                            location.reload(); 
                                        },2000);
                                                            
                                    } 
                                });
                            }else if (type == "program") {
                                $.ajax({
                                    url :"<?php echo base_url() ?>Process/ConfirmPaymentProgram",
                                    type:"POST",
                                    data: {
                                        trx_id: trx_id,
                                        rtp: rtp,                                        
                                        id_user: id_user
                                    },
                                    success: function(response){             
                                        //TUTUP MODAL
                                        angular.element(document.getElementById('verifytransaction')).scope().loadingsclose();
                                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Verify and Create class");
                                        setTimeout(function(){
                                            location.reload(); 
                                        },2000);
                                                            
                                    } 
                                });
                            }
                            else {
                                angular.element(document.getElementById('verifytransaction')).scope().loadingsclose();
                                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Failed Verify and Create class");
                                setTimeout(function(){
                                    location.reload(); 
                                },1500);
                            }
                        }
                    }
                    else if(aksi == 2)
                    {
                        for (var i = 0; i < response.data.length; i++) {
                            var trx_id      = response.data[i]['trx_id'];
                            var type        = response.data[i]['type'];
                            var id_user     = response.data[i]['id_user'];
                            var tutor_id    = response.data[i]['tutor_id'];
                            var class_id    = response.data[i]['class_id'];
                            var rtp         = response.data[i]['rtp'];
                            // KASIH LOADING
                            $("#modalVerify").modal("hide");
                            angular.element(document.getElementById('verifytransaction')).scope().loadings(); 
                            $.ajax({
                                url :"<?php echo base_url() ?>Process/ConfirmUnderPayment",
                                type:"POST",
                                data: {
                                    trx_id: trx_id,
                                    rtp: rtp,
                                    tutor_id: tutor_id,
                                    id_user: id_user,
                                    type: type,
                                    class_id: class_id
                                },
                                success: function(response){             
                                    //TUTUP MODAL
                                    angular.element(document.getElementById('verifytransaction')).scope().loadingsclose();
                                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Email notification successfully send to student");
                                    setTimeout(function(){
                                        location.reload(); 
                                    },2000);                                                    
                                } 
                            });
                        }
                    }
                    else
                    {                        
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Failed Verify");
                        setTimeout(function(){
                            location.reload();
                        },1500);
                    }
        		}
        	});
        });

        $("#rejecttransaction").on("click", function(){         
            var bank_id             = $("#rjt_bank_id").val();
            var date_field          = $("#rjt_date_field").val();
            var description_field   = $("#rjt_description_field").val();
            var credit_amount       = $('#rjt_credit_amount').val();
            var balance_after_credit= $("#rjt_balance_after_credit").val();
            var invoice_id          = $("#rjt_invoice_id").val();            
            var ket                 = $("#rjt_ket").val();
            $("#modalReject").modal("hide");
            angular.element(document.getElementById('rejecttransaction')).scope().loadings();           
            $.ajax({
                url:'<?php echo base_url();?>Operator/OperatorAction',
                type:'POST',
                data: {
                    bi :  bank_id,
                    df : date_field,
                    def : description_field,
                    ca : credit_amount,
                    bac : balance_after_credit,
                    invoice_id : invoice_id,
                    aksi : 3,
                    ket: ket
                },
                success: function(response){
                    response    = JSON.parse(response);
                    result_aksi = response['aksi'];
                    if(result_aksi == 3){
                        angular.element(document.getElementById('rejecttransaction')).scope().loadingsclose();
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Successfully added to student pocket money");
                        setTimeout(function(){
                            location.reload();
                        },1500);
                    }
                    else
                    {
                        angular.element(document.getElementById('rejecttransaction')).scope().loadingsclose();
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Failed Verify");
                        setTimeout(function(){
                            location.reload();
                        },1500);
                    }
                }
            });
        });

    } );
</script>
    