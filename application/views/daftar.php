<div class="site_preloader flex_center">
    <div class="site_preloader_inner">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
    </div>
</div>

<div class="styler">
    <div class="single_styler clearfix sticky_header">
        <h4>Header style</h4>
        <select class="wide">
            <option value="static">Enable header top</option>
            <option value="diseable_ht">Diseable header top</option>
        </select>
    </div>
    <div class="single_styler primary_color clearfix">
        <h4>Primary Color</h4>
        <div class="clearfix">
            <input id="primary_clr" type="text" class="form-control color_input" value="#f9bf3b" />
            <label class="form-control" for="primary_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler secound_color clearfix">
        <h4>Secoundary Color</h4>
        <div class="clearfix">
            <input id="secound_clr" type="text" class="form-control color_input" value="#19b5fe" />
            <label class="form-control" for="secound_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler layout_mode clearfix">
        <h4>Layout Mode</h4>
        <select class="wide">
            <option value="wide">Wide</option>
            <option value="boxed">Boxed</option>
            <option value="wide_box">Wide Boxed</option>
        </select>
    </div>
    <div class="layout_mode_dep">
        <div class="single_styler body_bg_style clearfix">
            <h4>Body Background style</h4>
            <select class="wide">
                <option value="img_bg">Image Background</option>
                <option value="solid">Solid Color</option>
            </select>
        </div>
        <div class="single_styler body_solid_bg clearfix">
            <h4>Select Background Color</h4>
            <div class="clearfix">
                <input id="body_bg_clr" type="text" class="form-control color_input" value="#d3d3d3" />
                <label class="form-control" for="body_bg_clr"><i class="fa fa-angle-down"></i></label>
            </div>
        </div>
    </div>
</div>
   
<div class="main_wrap">
    <?php $this->load->view('inc/navbar');?>

    <!-- 15. breadcrumb_area -->
    <!-- <div class="breadcrumb_area">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 3%;">
                    <center>
                        <h2 class="cPrimary">Register as :</h2>
                    </center>
                </div>
                <div class="col-md-4" id="signup_student" style="background-color: #DADFE1; cursor: pointer;">
                    <center>
                        <img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/SD%20Sederajat.png" style="height: 40%; width: 40%;">
                        <h4>Parent of Grade 1-6 student</h4>
                    </center>                    
                </div>
                <div class="col-md-4" id="signup_smp" style="background-color: transparent; cursor: pointer;">
                    <center>
                        <img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/SMP-SMA.png" style="height: 40%; width: 40%;">
                        <h4>Grade 7 - 12 Student</h4>
                    </center>
                </div>
                <div class="col-md-4" id="signup_umum" style="background-color: transparent; cursor: pointer;">
                    <center>
                        <img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/Mahasiswa-Umum.png" style="height: 40%; width: 40%;">
                        <h4>General</h4>
                    </center>
                </div>
            </div>
        </div>
    </div> -->
    <!-- 15. /breadcrumb_area -->

    <!-- 10. form_area -->
    <div class="form_area sp single_page">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="form_text text-center">
                        <div id="selected_signup1">
                            <h4 class="cPrimary" id="focus1">Siswa daftar disini</h4>
                            <div class="col-md-12" style="background-color: transparent; cursor: pointer; margin-top: 5%;">
                                <center>
                                    <img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/Mahasiswa-Umum.png" style="height: 45%; width: 45%;">
                                    <!-- <h4 style="color: #6C7A89; margin-top: 5%;">General</h4> -->
                                </center>                    
                            </div>
                        </div>
                        
                        <div id="selected_signup1" style="margin-top: 300px;">                            
                            <a href="<?php echo BASE_URL();?>DaftarPengajar">
                                <hr>
                                <br><br>
                                <h4 class="cPrimary" id="focus1">Daftar Tutor</h4>
                                <div class="col-md-12" style="background-color: transparent; cursor: pointer; margin-top: 0%;">
                                    <center>
                                        <!-- <img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/Mahasiswa-Umum.png" style="height: 45%; width: 45%;"> -->
                                        <h4 style="color: #6C7A89; margin-top: 5%;">Anda ingin mendaftar menjadi pengajar ? <br/><br/>Klik disini</h4>
                                    </center>                    
                                </div>
                            </a>
                        </div>
                        <!-- <div id="selected_signup2" style="display: none;">
                            <h4 class="cPrimary" id="focus2">Grade 7 - 12 Student</h4>
                            <div class="col-md-12" style="background-color: transparent; cursor: pointer; margin-top: 5%;">
                                <center>
                                    <img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/SMP-SMA.png" style="height: 40%; width: 40%;">
                                    <h4 style="color: #6C7A89; margin-top: 5%;"></h4>
                                </center>
                            </div>
                        </div>
                        <div id="selected_signup3" style="display: none;">
                            <h4 class="cPrimary" id="focus3">Parent of Grade 1-6 student</h4>
                            <div class="col-md-12" style="background-color: transparent; cursor: pointer; margin-top: 5%;">
                                <center>
                                    <img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/Mahasiswa-Umum.png" style="height: 40%; width: 40%;">
                                </center>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="col-md-7 form_h">
                    <div class="contact_form">
                        <input type="text" id="type_regis" value="ortu" hidden>                        
                        <div class="form-group clearfix">
                            <div class="col-md-6">
                                <p>Nama depan*</p>
                                <input type="text" placeholder="Isi nama depan" name="nama_depan" id="nama_depan">
                                <label style="color: red; float: left; display: none;" id="alert_nama_depan">Nama depan tidak boleh kosong</label>
                            </div>
                            <div class="col-md-6">
                                <p>Nama belakang*</p>
                                <input type="text" placeholder="Isi nama belakang" name="nama_belakang" id="nama_belakang">
                                <label style="color: red; float: left; display: none;" id="alert_name_belakang">Nama belakang tidak boleh kosong</label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-md-12">
                                <p>Email*</p>
                                <input type="email" placeholder="contoh : john@email.com" name="email" id="email">
                                <label style="color: red; float: left; display: none;" id="alert_email">Email tidak boleh kosong</label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-md-6">
                                <p>Kata sandi*</p>
                                <input type="password" placeholder="Isi kata sandi" name="password" id="password">
                                <label style="color: red; float: left; display: none;" id="alert_password">Kata sandi tidak boleh kosong</label>
                            </div>
                            <div class="col-md-6">
                                <p>Konfirm Kata sandi*</p>
                                <input type="password" placeholder="Isi konfirm kata sandi" name="password_cek" id="password_cek">
                                <label style="color: red; float: left; display: none;" id="alert_password_cek">Konfirm kata sandi tidak boleh kosong</label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-md-4">
                                <p>Tanggal lahir*</p>
                                <select class="select2 wide" name="date" id="date" data-live-search="true" style="width: 100%;">
                                    <option disabled selected value="">Pilih Tanggal</option>
                                    <?php 
                                        for ($date=01; $date <= 31; $date++) {
                                            ?>                                                  
                                            <option value="<?php echo $date ?>"><?php echo $date ?></option>                                                  
                                            <?php 
                                        }
                                    ?>
                                </select>
                                <label style="color: red; float: left; display: none;" id="alert_tanggal">Tanggal tidak boleh kosong</label>
                            </div>
                            <div class="col-md-4">
                                <p>Bulan lahir*</p>
                                <select class="select2 wide "  name="month" id="month" data-live-search="true" style="width: 100%;">
                                    <option disabled selected value="">Pilih bulan</option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <label style="color: red; float: left; display: none;" id="alert_bulan">Bulan tidak boleh kosong</label>
                            </div>
                            <div class="col-md-4">
                                <p>Tahun lahir*</p>
                                <select class="select2 wide" name="year" id="year" data-live-search="true" style="width: 100%;">
                                    <option disabled selected>Pilih tahun</option>
                                    <?php 
                                    for ($i=1980; $i <= 2018; $i++) {
                                        ?>                                                 
                                          <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php 
                                    } 
                                    ?>
                                </select>
                                <label style="color: red; float: left; display: none;" id="alert_tahun">Tahun tidak boleh kosong</label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-md-6">
                                <p>Jenis kelamin</p>
                                <select class="select2 wide" name="gender" id="gender" data-live-search="true" style="width: 100%;">
                                    <option disabled selected value="">Pilih jenis kelamin</option>
                                    <option value="Male">Pria</option>
                                    <option value="Female">Wanita</option>
                                </select>
                                <label style="color: red; float: left; display: none;" id="alert_jenis_kelamin">Jenis kelamin tidak boleh kosong</label>
                            </div>
                            <div class="col-md-6">
                                <p>Warga negara</p>
                                <select class="select2 wide" name="name_country" id="name_country" data-live-search="true" style="width: 100%;">
                                    <option disabled selected value="">Pilih Kota</option>
                                    <option value="SG">Singapore</option>
                                </select>
                                <label style="color: red; float: left; display: none;" id="alert_nama_negara">Harap pilih warga negara</label>
                            </div>
                        </div>                        
                        <div class="form-group clearfix">
                            <div class="col-md-4">
                                <p>Kode area</p>
                                <select class="select2 wide" name="kode_area" id="kode_area" data-live-search="true" style="width: 100%;">
                                    <option disabled selected value="" >Pilih kode area</option> 
                                    <option value="SG">Singapore +702</option>                                   
                                </select>
                                <label style="color: red; float: left; display: none;"  id="alert_kode_area">Harap pilih kode area</label>
                            </div>
                            <div class="col-md-8">
                                <p>Nomer Hp</p>
                                <input type="text" placeholder="Contoh : 382 437" onkeypress="return hanyaAngka(event)" name="no_hape" id="no_hape">
                                <label style="color: red; float: left; display: none;" id="alert_no_hape">Nomer hp tidak boleh kosong</label>
                            </div>
                        </div>
                        <div class="form-group" style="display: none;" id="form_jenjang">
                            <div class="col-md-12">
                                <p>Jenjang</p>
                                <select id="jenjang" required name="jenjang" class="select2 form-control wide" data-live-search="true" style="width: 100%;">
                                    <option disabled selected value="">Pilih Jenjang</option>                                        
                                </select>
                                <label style="color: red; float: left; display: none;" id="alert_jenjang">Harap pilih jenjang</label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-md-12">
                                <br>
                                <button type="button" class="button hvr-bounce-to-right" id="daftar"><i class="fa fa-long-arrow-right"></i> Daftar Sekarang</button>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 10. /form_area -->

    <div class="modal" tabindex="-1" id="modal_cekumur" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="alert_title_modal">Warning</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="alertumur"></p>
                </div>          
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" id="alert_modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="alert_title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="alert_message_modal"></p>
                </div>          
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" id="alert_joinmodal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="alert_title_joinmodal"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="alert_message_joinmodal"></p>
                </div>
                <div class="modal-footer" id="footer1">                    
                    <button class="btn btn-default" id="tolakChannel">Tidak</button>
                    <button class="btn btn-info" id="joinChannel">Ya</button>                    
                </div>   
                <div class="modal-footer" id="footer2" style="display: none;">
                    <button class="btn btn-info" id="okChannel">Ok</button>
                </div>     
            </div>
        </div>
    </div>

    <script>
        function hanyaAngka(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
          return true;
        }
        function validAngka(a)
        {
            if(!/^[0-9.]+$/.test(a.value))
            {
            a.value = a.value.substring(0,a.value.length-31);
            }
        }
          function validPhone(a)
        {
            if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
            {
            a.value = a.value.substring(0,a.value.length-31);
            }
        }
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
        var channel_id = '52';
        
        $(document.body).on('click', '.clickHow' ,function(e){
            var step = $(this).data('step');
            var arrayStep = ['1','2','3','4','5','6'];
            for (var i = 0; i < arrayStep.length ; i++) {
                $("#tab"+arrayStep[i]).removeClass();
                if (arrayStep[i] == step) {
                    $("#step"+arrayStep[i]).css('display','block');                    
                    $("#tab"+arrayStep[i]).addClass('active clickHow');
                }
                else
                {
                    $("#tab"+arrayStep[i]).addClass('clickHow');                    
                    $("#step"+arrayStep[i]).css('display','none');
                }                
            }
        });       

        $.ajax({
            url: '<?php echo AIR_API;?>ListCountry',
            type: 'POST',
            success: function(response)
            {
                var a = JSON.stringify(response);
                var geoNow = window.localStorage.getItem('geo_location');                                    
                for (var ii = 0; ii < response.list_country.length; ii++) {
                    var nicename = response.list_country[ii]['nicename'];
                    var phonecode = response.list_country[ii]['phonecode'];
                    var iso = response.list_country[ii]['iso'];
                    
                    if (geoNow == iso) {
                        $('#name_country').append("<option value='"+nicename+"' selected='true'>"+nicename+"</option>");
                        $('#kode_area').append("<option value='"+nicename+"' selected='true'>"+nicename+" +"+phonecode+"</option>");
                    }
                    else
                    {
                        $('#name_country').append("<option value='"+nicename+"'>"+nicename+"</option>");
                        $('#kode_area ').append("<option value='"+nicename+"'>"+nicename+" +"+phonecode+"</option>");
                    }
                }
            }
        });
        
        $.ajax({
            url: '<?php echo AIR_API;?>ListJenjang',
            type: 'POST',
            success: function(response)
            {                    
                for (var i = 0; i < response.list_jenjang.length; i++) {
                    var id = response.list_jenjang[i]['id'];
                    var text = response.list_jenjang[i]['text'];
                    $('#jenjang').append("<option value='"+id+"'>"+text+"</option>")
                }
            }
        });

        function checkBOD(pekerjaan) {              
            var today = new Date();
            var thn_ini = today.getFullYear();
            var bln_ini = today.getMonth();
            var tgl_ini = today.getDate();

            var tgl_lahir = document.getElementById("tanggal_"+pekerjaan).value;
            var bln_lahir = document.getElementById("bulan_"+pekerjaan).value;
            var thn_lahir = document.getElementById("tahun_"+pekerjaan).value;

            var age = thn_ini - thn_lahir;
            var ageMonth = bln_ini - bln_ini;
            var ageDay = tgl_ini - tgl_lahir;
            // alert(age);

            if (ageMonth < 0 || (ageMonth == 0 && ageDay < 0)) {
            age = parseInt(age) - 1;
            // alert(age);
            }
            return age;
        }

        function chooseOccupation(age,type,pekerjaan) {    
            var today = new Date();
            var thn_ini = today.getFullYear();
            var bln_ini = today.getMonth();
            var tgl_ini = today.getDate();

            var tgl_lahir = document.getElementById("date").value;
            var bln_lahir = document.getElementById("month").value;
            var thn_lahir = document.getElementById("year").value;

            var age = thn_ini - thn_lahir;
            var ageMonth = bln_ini - bln_ini;
            var ageDay = tgl_ini - tgl_lahir;                                                  
            if (pekerjaan == "ortu" || pekerjaan == "umum") {
                if (age <= 19) {                                                    
                    return -1;
                }
                else
                {   
                    return 1;
                }
            }
            else
            {
                if (age > 20) {
                    return -2
                }
                else
                {
                    return 1;
                }
            }
        }

        // This is a functions that scrolls to #{blah}link
        function goToByScroll(id){            
              // Scroll
            $('html,body').animate({
                scrollTop: $("#"+id).offset().top-430},
                'slow');
        }

        $(document.body).on('click', '#signup_student' ,function(e){
            $("#type_regis").val("");
            //UMUM
            $("#signup_umum").css('background-color','transparent');            
            //SMP
            $("#signup_smp").css('background-color','transparent');            
            //STUDENT
            $("#signup_student").css('background-color','#DADFE1');
            $("#selected_signup2").css('display','none');
            $("#selected_signup3").css('display','none');
            $("#form_jenjang").css('display','none');
            $("#selected_signup1").css('display','block');
            $("#type_regis").val("ortu");
            goToByScroll('focus1'); 

        });

        $(document.body).on('click', '#signup_smp' ,function(e){
            $("#type_regis").val("");
            //UMUM
            $("#signup_umum").css('background-color','transparent');            
            //STUDENT
            $("#signup_student").css('background-color','transparent');            
            //SMP
            $("#signup_smp").css('background-color','#DADFE1');
            $("#selected_signup1").css('display','none');
            $("#selected_signup3").css('display','none');
            $("#selected_signup2").css('display','block');
            $("#form_jenjang").css('display','block');            
            $("#type_regis").val("sma");
            goToByScroll('focus2'); 
        });

        $(document.body).on('click', '#signup_umum' ,function(e){
            $("#type_regis").val("");
            //SMP
            $("#signup_smp").css('background-color','transparent');            
            //STUDENT
            $("#signup_student").css('background-color','transparent');            
            //UMUM
            $("#signup_umum").css('background-color','#DADFE1');
            $("#selected_signup1").css('display','none');
            $("#selected_signup2").css('display','none');
            $("#form_jenjang").css('display','none');
            $("#selected_signup3").css('display','block'); 
            $("#type_regis").val("umum");           
            goToByScroll('focus3'); 
        });

        //CEK PASSWORD UMUM
        $('#password').on('keyup',function(){
            if($('#password_cek').val() != $(this).val()){
                $('#daftar').attr('disabled','disabled');                                      
                $('#alert_password_cek').html('Password does not match').css('display', 'block');
            }else{                      
                $('#alert_password_cek').html('').css('display', 'none');
                $("#daftar").removeAttr('disabled');
            }
        });
        $('#password_cek').on('keyup',function(){
            if($('#password').val() != $(this).val() ){
                $('#daftar').attr('disabled','disabled');
                $('#alert_password_cek').html('Password does not match').css('display', 'block');
            }else{
                $('#alert_password_cek').html('').css('display', 'none');
                $("#daftar").removeAttr('disabled');
            }
        });

        $("#daftar").click(function(){
            $("#daftar").attr('disabled','disabled');
            var type_regis     = $("#type_regis").val();
            var name_depan     = $("#nama_depan").val();
            var name_belakang  = $("#nama_belakang").val();
            var email          = $("#email").val();
            var password       = $("#password").val();
            var password_cek   = $("#password_cek").val();
            var tanggal        = $("#date").val();            
            var bulan          = $("#month").val();
            var tahun          = $("#year").val();
            var jenis_kelamin  = $("#gender").val();
            var nama_negara    = $("#name_country").val();            
            var kode_area      = $("#kode_area").val();
            var no_hape        = $("#no_hape").val();              
            if (type_regis == 'sma') {
                var jenjang    = $("#jenjang").val();
                if (jenjang == null || jenjang == "") {
                    $("#alert_nama_depan").css('display','none');
                    $("#alert_name_belakang").css('display','none');
                    $("#alert_email").css('display','none');
                    $("#alert_password").css('display','none');
                    $("#alert_password_cek").css('display','none');
                    $("#alert_tanggal").css('display','none');
                    $("#alert_bulan").css('display','none');
                    $("#alert_tahun").css('display','none');
                    $("#alert_jenis_kelamin").css('display','none');
                    $("#alert_nama_negara").css('display','none');
                    $("#alert_kode_area").css('display','none');
                    $("#alert_no_hape").css('display','none');
                    $("#alert_jenjang").css('display','block');
                    $("#daftar").removeAttr('disabled');
                    goToByScroll("jenjang");
                }            
            }
            if (name_depan == "") {
                $("#alert_nama_depan").css('display','block');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("nama_depan");
            }
            else if (name_belakang == "") {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','block');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("nama_belakang");
            }
            else if (email == "") {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','block');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("email");
            }
            else if (password == "") {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','block');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("password");
            }
            else if (password_cek == "") {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','block');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("password_cek");
            }
            else if (tanggal == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','block');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("date");
            }
            else if (bulan == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','block');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("month");
            }
            else if (tahun == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','block');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("year");
            }
            else if (jenis_kelamin == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','block');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("gender");
            }
            else if (nama_negara == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','block');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("name_country");
            }
            else if (kode_area == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','block');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("kode_area");
            }
            else if (no_hape == "") {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#alert_no_hape").css('display','block');
                $("#daftar").removeAttr('disabled');
                goToByScroll("no_hape");
            }
            else
            {                
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');

                $("#daftar").attr('disabled','disabled');
                $("#daftar").attr('background-color','grey');
                $.ajax({
                    url: '<?php echo AIR_API;?>Channel_daftarMurid',
                    type: 'POST',
                    data: {
                        channel_id: channel_id,
                        first_name: name_depan,
                        last_name: name_belakang,
                        email: email,
                        kata_sandi: password,
                        tanggal_lahir: tanggal,
                        bulan_lahir: bulan,
                        tahun_lahir: tahun,
                        jenis_kelamin: jenis_kelamin,
                        kode_area: kode_area,
                        nama_negara: nama_negara,
                        no_hape: no_hape,
                        type: type_regis,
                        linkweb : '<?php echo BASE_URL();?>'
                    },
                    success: function(response)
                    {                             
                        var code = response['code'];
                        
                        if (code == 200) {
                            $("#alert_title_modal").text("General Registration Success");
                            $("#alert_message_modal").text("Thank you for registering, please check your email for activation link.");
                            $("#alert_modal").modal("show");
                            setTimeout(function(){
                                location.reload();
                            },5000);
                        }
                        else if(code == -102){
                            var id_user = response['id_user'];
                            var user_type = response['user_type'];
                            
                            $("#alert_title_joinmodal").text("Pemberitahuan");
                            if (user_type == 'tutor') {
                                $("#alert_message_joinmodal").text("Email atau akun Anda sudah terdaftar di Classmiles sebagai PENGAJAR. Dan website ini ber-afiliasi dengan Classmiles. Silakan gunakan email atau akun lain untuk mendaftar sebagai SISWA di website ini");                                
                                $("#footer1").css('display','none');
                                $("#footer2").css('display','block');
                            }
                            else
                            {
                                $("#alert_message_joinmodal").text("Email atau akun Anda sudah terdaftar di Classmiles. Dan website ini ber-afiliasi dengan Classmiles. Apakah Anda ingin menggunakan email atau akun yang sama untuk bisa masuk ke website ini?");

                                $("#joinChannel").attr('id_user',id_user);
                                $("#joinChannel").attr('user_type', user_type);                                
                                $("#footer2").css('display','none');
                                $("#footer1").css('display','block');
                            }                                                        
                            $("#alert_joinmodal").modal("show");
                        }
                        else if(code == -100){
                            $("#alert_title_modal").text("Registration failed"); 
                            $("#alert_message_modal").text("Please try again.");
                            $("#alert_modal").modal("show");
                            setTimeout(function(){
                                location.reload();
                            },5000);
                        }
                        else
                        {
                            $("#alert_title_modal").text("There is an error");
                            $("#alert_message_modal").text("Please try again.");
                            $("#alert_modal").modal("show");
                            setTimeout(function(){
                                location.reload();
                            },5000);
                        }                        
                    }
                });
            }
        });

        $(document.body).on('click', '#joinChannel' ,function(e){
            var id_user = $(this).attr('id_user');
            var user_type = $(this).attr('user_type');
            $("#alert_joinmodal").modal('hide');
            $.ajax({
                url: '<?php echo AIR_API;?>Channel_joindaftarMurid',
                type: 'POST',
                data: {
                    channel_id: channel_id,
                    id_user: id_user,
                    user_type: user_type
                },
                success: function(response)
                {                             
                    var code = response['code'];
                    
                    if (code == 200) {
                        $("#alert_title_modal").text("Sukses"); 
                        $("#alert_message_modal").text("Terima kasih. Anda telah terdaftar di Fisipro.com. Silahkan login untuk memulai kelas.");
                        $("#alert_modal").modal("show");
                        setTimeout(function(){
                            location.reload();
                        },5000);
                    }
                    else
                    {
                        $("#alert_title_modal").text("Registrasi gagal"); 
                        $("#alert_message_modal").text("Akun anda telah terdaftar menjadi siswa di channel ini.");
                        $("#alert_modal").modal("show");
                        setTimeout(function(){
                            location.reload();
                        },5000);
                    }
                }
            });
        });

        $(document.body).on('click', '#tolakChannel' ,function(e){
            location.reload();
        });

        $(document.body).on('click', '#okChannel' ,function(e){
            location.reload();
        });

    });    
    </script>
</div>