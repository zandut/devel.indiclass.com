<?php
	/*
	Arabic
	*/

	$lang['home'] = 'صفي';
	$lang['subject'] = 'موضوع';
	$lang['allsub'] = 'جميع الموضوعات'; 
	$lang['ondemand'] = 'الطلب';
	$lang['history'] = 'سجل';
	$lang['profil'] = 'البيانات الشخصية';
	$lang['schedule'] = 'جدول';
	$lang['viewprofile'] = 'رؤية البيانات الشخصية';
	$lang['privacysetting'] = 'إعدادات الخصوصية';
	$lang['settings'] = 'تنظيم';
	$lang['logout'] = 'خروج';

	//Footer
	$lang['reports'] = 'تقرير';
	$lang['support'] = 'موافقة';
	$lang['contact'] = 'اتصال';
	$lang['copyright'] = 'حق النشر';

	//Page Index
	$lang['title_pg_index'] = 'شرفة';

	//Page Subject
	$lang['title_pg_subject'] = 'موضوع';
	$lang['follow'] = 'تابع';
	$lang['followed'] = 'يتبع';

	//Page All Subject
	$lang['title_pg_allsub'] = 'جميع الموضوعات';
	$lang['select_class'] = 'حدد فئة';
	$lang['select_level'] = 'تحديد مستوى التعليم';
	$lang['status'] = 'الحالة';
	$lang['button_search'] = 'البحث';

	//Page Profile
	$lang['tab_about'] = 'حول';
	$lang['tab_account'] = 'حساب';
	$lang['tab_cp'] = 'تغيير كلمة المرور';
	$lang['upd_profpic'] = 'صورة الملف التحديث';
	$lang['summary'] = 'ملخص';
	$lang['basic_info'] = 'معلومات اساسية';
	$lang['contact_info'] = 'معلومات الاتصال';
	$lang['contact_left_side'] ='اتصال';
	$lang['totcon'] = 'الاتصال الكلي';
	$lang['edit'] = 'تحرير';
	$lang['full_name'] = 'الاسم الكامل';
	$lang['gender'] ='جنس';
	$lang['birthday'] = 'تاريخ الميلاد';
	$lang['rel_stat'] = 'الحالة الاجتماعية';
	$lang['mobile_phone'] = 'تليفون محمول';
	$lang['email_address'] = 'عنوان البريد الإلكتروني';

			//On Account Tab
	$lang['select_gender'] = 'حدد نوع الجنس';
	$lang['home_address'] = 'عنوان البيت';
	$lang['old_pass'] = 'كلمة المرور القديمة';
	$lang['new_pass'] = 'كلمة السر الجديدة';
	$lang['confirm_pass'] = 'توكيد كلمة المرور الجديدة';
	$lang['button_save'] = 'حفظ';
	$lang['button_cancel'] = 'إلغاء';

	

?>