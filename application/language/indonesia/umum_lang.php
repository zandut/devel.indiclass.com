<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	/*
	Indonesia
	*/

	//BULAN
	$lang['jan']					= 'Januari';
	$lang['feb']					= 'Februari';
	$lang['mar']					= 'Maret';
	$lang['apr']					= 'April';
	$lang['mei']					= 'Mei';
	$lang['jun']					= 'Juni';
	$lang['jul']					= 'Juli';
	$lang['ags']					= 'Agustus';
	$lang['sep']					= 'September';
	$lang['okt']					= 'Oktober';
	$lang['nov']					= 'November';
	$lang['des']					= 'Desember';

	//HARI
	$lang['Sun']					= 'Minggu';
	$lang['Mon']					= 'Senin';
	$lang['Tue']					= 'Selasa';
	$lang['Wed']					= 'Rabu';
	$lang['Thu']					= 'Kamis';
	$lang['Fri']					= 'Jumat';
	$lang['Sat']					= 'Sabtu';

	// BuyCredit
	$lang['classcredit']			= 'Kami akan mengirimkan Anda dan faktur melalui email. Mohon lanjutkan dengan pembayaran.';
	$lang['btnclasscredit']			= 'Kirim Faktur';

	// TAMBAHAN
	
	$lang['buyforkid']				= 'Beli paket ini untuk';
	$lang['pilihakun']				= 'Pilih Akun';
	$lang['textbelipaket']			= 'Saya membeli paket ini untuk :';
	$lang['program_reguler']		= 'Program Reguler';
	$lang['text_program']		= 'Atau ikuti program-program dari lembaga yang sudah  bergabung dengan Indiclass:';
	$lang['daftarakun']				= 'Daftar Akun';
	$lang['tambahanak']				= 'Tambah Akun Anak';
	$lang['birth']					= 'Tempat Tgl Lahir';
	$lang['tiga_langkah']			= 'Belajar online  dengan 3 langkah mudah';
	$lang['hello']					= 'Hai,';
	$lang['pilih_tutor']			= 'Pilih Tutor';
	$lang['atur_waktu']				= 'Atur Waktu';
	$lang['masuk_kelas']			= 'Masuk Kelas';
	$lang['daftar_disini']			= 'Daftar Disini Sekarang';
	$lang['kami_siap']				= 'Kami siap melayani Anda';
	$lang['setiap_hari_kerja']		= 'Setiap hari kerja';


    $lang['banner2']				= 'Interaksi langsung secara online dengan guru les pilihanmu dimanapun dia berada.';
    $lang['banner3']				= 'Silahkan tentukan waktumu sendiri sesuaikan dengan waktu yang kamu miliki. ';
    $lang['banner4']				= 'Berdiskusi langsung dengan tutor sedekat berdiskusi dengan orangtuamu.';
    $lang['banner5']				= 'Tersedia fitur les privat dengan guru yang bervariasi, menyenangkan dan sangat berpengalaman.';
    $lang['banner6']				= 'Jarak bukan lagi penghambat. Belajar kelompok jadi jauh lebih mudah. Bisa dari mana saja.';

	$lang['joined']					= 'Peserta';
	$lang['invitedby']				= 'Diundang oleh';
	$lang['classinvitation']		= 'Ajakan Bergabung';
	$lang['extraclass']				= 'Kelas Tambahan';
	$lang['createextraclass']		= 'Buat Kelas Baru';
	$lang['createnewclass']			= 'Buat Kelas Baru';

	$lang['free']					= 'Gratis';

	$lang['waitapprovedme']			= 'Menunggu persetujuan dari anda';
	$lang['waitapprovedtutor']		= 'Menunggu persetujuan dari tutor';
	$lang['waitapprovedfriend']		= 'Menunggu persetujuan dari teman';
	$lang['waitforpayment']			= 'Menunggu pembayaran';
	$lang['paymentsuccess']			= 'Pembelian Berhasil';
	$lang['paymentfailed']			= 'Pembelian Gagal';
	$lang['waitingunderpayment']	= 'Menunggu Kurang Bayar';
	$lang['rejectedbytutor']		= 'Ditolak oleh tutor';
	$lang['classexpired']			= 'Kelas ini kadaluarsa';
	$lang['tutornotavailable']		= 'Tutor tidak tersedia';
	$lang['confirmedbytutor']		= 'Disetujui oleh Tutor';


	$lang['completeprofiletitle']	= 'Lengkapi Profil';
	$lang['completeschooltitle']	= 'Lengkapi Data Sekolah';
	$lang['alertcompleteschool']	= 'Silakan lengkapi data sekolah Anda dibawah ini';
	$lang['alertcompleteaddress']	= 'Silakan lengkapi alamat Anda dibawah ini';
	$lang['alertcompletedata']		= 'Silakan lengkapi data diri Anda dibawah ini agar dapat mengakses penuh fitur Indiclass';
	$lang['alertnotif_complain']	= 'Hai Tutor, Siswa ';
	$lang['alertnotif_complain2']	= 'mengkomplain kelas ';
	$lang['alertnotif_complain3']	= 'harap berdiskusi di fitur chatting.';
	$lang['join']					= 'Gabung';
	$lang['groupclass']				= 'Kelas Kelompok';
	$lang['privateclass']			= 'Kelas Privat';
	$lang['yearsold']				= "tahun";
	$lang['detailgroup']			= 'Sampai dengan 4 siswa di kelas';
	$lang['detailprivate']			= 'Hanya kamu dan tutor di kelas';
	$lang['emailkosong']			= 'Harap masukkan minimal 1 email teman anda!';
	$lang['emailnotfound']			= 'Maaf terdapat email yang tidak terdaftar di Indiclass. Silakan ulangi kembali';
	$lang['emailown']				= 'Maaf Anda tidak dapat mengajak diri sendiri';
	$lang['topikbelajar']			= 'Topik belajar : ';
	$lang['isitopikanda']			= 'Isi topik yang ingin Anda pelajari ...';
	$lang['topikkosong']			= 'Harap isi topik pelajaran terlebih dahulu !';
	$lang['friendinvitation']		= 'Undangan Teman';
	$lang['durasi']					= 'Durasi';
	$lang['tanggaldurasi']			= 'Tanggal Permintaan';
	$lang['belumsetuju']			= 'Belum Disetujui';
	$lang['sudahsetuju']			= 'Telah Disetujui';
	$lang['bayarsendiri']			= 'Sendiri';
	$lang['bayarbersama']			= 'Ramai ramai';
	$lang['waitapproved']			= 'Menunggu perstujuan dari tutor';
	$lang['temangroup']				= 'Teman Grup';
	$lang['diajakoleh']				= 'Diajak oleh';
	$lang['terima']					= 'Terima';
	$lang['tolak']					= 'Tolak';
	$lang['kelasapprove']			= 'Permintaan Kelas sudah disetujui oleh tutor. Jadwal kelas sudah di ';
	$lang['waitingfriend']			= 'Menunggu Persetujuan Teman';
	$lang['waitingtutor']			= 'Permintaan Sudah diterima oleh tutor, tunggu hingga tutor kami menerimanya.';
	$lang['menit']					= 'Menit';
	$lang['jam']					= 'Jam';
	$lang['lihatkelaskids']			= 'Melihat Jadwal Kelas Anak';
	$lang['pilihkids']				= "silakan pilih anak Anda untuk melihat jadwal mereka";
	$lang['jadwalkelas']			= "Jadwal Kelas";
	$lang['kidsselected']			= "Anak yang dipilih";
	$lang['pilihyanglain']			= "Pilih lainnya";
	$lang['registerkid']			= "Daftarkan Anak Anda";
	$lang['typeusername']			= "Ketik nama pengguna disini...";
	$lang['typepassword']			= "Ketik kata sandi disini...";
	$lang['typefirstname']			= "Ketik nama depan disini...";
	$lang['typelastname']			= "Ketik nama terakhir disini...";
	$lang['typebirthplace']			= "Ketik tempat lahir disini...";
	$lang['typeschoolname']			= "Ketik nama sekolah disini...";
	$lang['typeschooladdress']		= "Ketik alamat sekolah disini...";
	$lang['aksesloginkid']			= "Akses untuk login Anak";
	$lang['register']				= "Daftar";

	$lang['lihatpelajarankids']		= "Memilih Pelajaran anak";
	$lang['pilihpelajaran']			= "silakan pilih mata pelajaran untuk anak sesuai keinganan anda";
	$lang['pilihanak']				= "silakan pilih anak anda";

	$lang['masukkelas']				= "Masuk";
	$lang['kelasberakhir']			= "Berakhir";
	$lang['belikelas']				= "Beli";
	$lang['aturkelas']				= "Atur Kelas";
	$lang['kelaspribadi']			= "Kelas Pribadi";
	$lang['mengatur']				= "Mengatur jadwal";
	$lang['atur']					= "Atur";
	$lang['kelaschannel']			= "Kelas Channel";
	$lang['gratis']					= "Kelas Terbuka";
	$lang['berbayar']				= "Kelas Berbayar";
	$lang['buy_class_credit']		= "Membeli kuota kelas";
	$lang['request_trial_class']	= "Meminta kelas percobaan";
	$lang['private']				= "Privat";
	$lang['group']					= "Grup";
	$lang['invitation']				= "Undangan";
	$lang['nodata_ondemand']		= "Silakan klik '+' dibawah ini";
	$lang['nodata_group']			= "untuk menambah permintaan kelas grup";
	$lang['nodata_private']			= "untuk menambah permintaan kelas privat";
	$lang['nodata_invitation']		= "Tidak ada data undangan";
	$lang['noorder_class']			= "Tidak ada pemesanan kelas";
	$lang['noinvoice']				= "Tidak ada pembelian";
	$lang['keranjangorder']			= "Keranjang pesanan";
	$lang['listinvoice']			= "Daftar Pembelian";
	$lang['listdetailinvoice']		= "klik untuk detail";
	$lang['ortu']					= "ORANG TUA siswa Sekolah Dasar (SD)";
	$lang['levelortu']				= "(Orang tua / wali murid)";
	$lang['smp-sma']				= "Siswa SMP / SMA";


	$lang['typeyournumberid']			= "Ketik nomor ID Anda disini...";
	$lang['typeyourbirthplace']			= "Ketik tempat lahir Anda disini...";
	$lang['chooseyourreligion']			= "Pilih agama Anda disini...";
	$lang['typeyouraddress']			= "Ketik alamat Anda disini...";
	$lang['chooseyourpropinsi']			= "Pilih Propinsi Anda disini...";
	$lang['chooseyourkabupaten']		= "Pilih Kabutapen/Kota Anda disini...";
	$lang['chooseyourkecamatan']		= "Pilih Kecamatan Anda disini...";
	$lang['chooseyourkelurahan']		= "Pilih Kelurahan/Desa Anda disini...";







	/* LOGIN */
	
	$lang['wlogin'] 				= 'Selamat datang di ';
	$lang['notaccountlogin'] 		= 'Belum punya akun? Silakan daftar';
	$lang['here'] 					= 'disini';
	$lang['login'] 					= 'Masuk';
	$lang['forgotpassword'] 		= 'Lupa Password ?';
	$lang['description_loginfooter']= 'Indiclass menyediakan layanan pembelajaran online. Ini berisi banyak ruang kelas virtual untuk siswa yaitu SD (kelas 5-6), SMP (kelas 7-9) dan SMA (kelas 10-12). Indiclass juga menyediakan kursus umum seperti bahasa Inggris, Jepang, Arab, Cooking, Yoga, Tari, dll Indiclass memungkinkan komunikasi interaktif antara guru dan siswa serta antara siswa. Silakan mendaftar untuk menikmati sesi gratis kami kami sediakan sehari-hari.';
	$lang['withfacebook'] 			= 'Masuk dengan Facebook';
	$lang['withgoogle'] 			= 'Masuk dengan Google';
	$lang['remember']				= 'Ingat saya';

	// FORGOT 
	$lang['lupasandi'] 				= 'Lupa Kata sandi';	
	$lang['title1'] 				= 'Masukkan alamat email Anda di bawah ini ';
	$lang['title2'] 				= 'dan kami akan mengirimkan email petunjuk ';
	$lang['title3'] 				= 'tentang cara mengubah kata sandi Anda';
	$lang['inputemail'] 			= 'Masukkan email Anda';
	$lang['sendemail'] 				= 'Kirim Email';
	$lang['backlogin'] 				= 'Kembali';

	//FITUR TUTOR
	$lang['fitur_tutor1']			= 'INGIN JADI PENGAJAR? ATAU KESULITAN MENCARI TEMPAT MENGAJAR?';
	$lang['fitur_tutor2']			= 'Segera';
	$lang['fitur_tutor3']			= 'Daftar Disini';
	$lang['fiturtutor1'] 			= 'Kapan Saja';
	$lang['isi_fitur1']				= 'Pagi, siang, sore ataupun malam tidak lagi menjadi hambatan bagi Anda untuk bisa mengajar. Anda bebas menentukan waktu mengajar.';
	$lang['fiturtutor2']			= 'Dimana Saja';
	$lang['isi_fitur2'] 			= 'Jarak dengan siswa bukan lagi halangan. Indiclass memfasilitasi Anda para guru/pengajar untuk dapat mengajar darimanapun Anda berada.
   dimanapun Anda berada.';
	$lang['fiturtutor3'] 			= 'Tentukan Harga Sendiri';
	$lang['isi_fitur3'] 			= 'Bagai memiliki tempat les sendiri, disini Anda bebas menentukan biaya mengajar.
   Raih penghasilan tak terhingga dengan segera menjadi bagian dari Indiclass.';
	$lang['fiturtutor4'] 			= 'Efisiensi waktu dan tenaga';
	$lang['isi_fitur4']				= 'Anda tidak perlu lagi mengeluarkan biaya perjalanan dan tenaga guna bertatap muka langsung dengan siswa. 
   Gunakan waktu berharga Anda untuk mengajar jarak jauh di Indiclass.';



	// REGISTER

   	$lang['detail_ortu']			= 'Untuk mendaftarkan siswa SD, orang tua siswa SD tersebut harus mendaftar sendiri terlebih dahulu di Indiclass dengan memilih pilihan ini. Setelah mendaftar, orang tua bisa menambahkan anaknya yang siswa SD tersebut melalui menu My Kids dan akan mendapatkan nama pengguna dan kata kunci untuk masing-masing anak yang didaftarkan. Anak selanjutnya bisa mengakases kelas melalui website atau aplikasi Indiclass Kids.';
	$lang['detail_siswa']			= 'SISWA SMP atau SMA atau yang sederajat';
	$lang['detail_umum']			= 'Mahasiswa, Profesional, dll';
	$lang['detail_tutor']			= 'Apabila Anda adalah guru, pengajar, pelatih, tentor atau siapapun yang ingin MENGAJAR di Indiclass, silakan klik tombol ini untuk mendaftar';
   	$lang['tutor']					= 'GURU / TUTOR';
   	$lang['pilihlevel']				= 'Daftarlah sesuai dengan umur, tingkat pendidikan atau pekerjaan anda';
   	$lang['levelpelajar']			= '(SMP , SMA dan Sederajat)';
   	$lang['leveltutor']				= '(Pengajar, Tentor)';
   	$lang['pelajar']				= 'SISWA';	
   	$lang['umum']					= 'UMUM';		
   	$lang['signup_tutor']			= 'PENDAFTARAN GURU';
   	$lang['levelsiswa']				= '(SMP, SMA dan Sederajat)';
   	$lang['levelumum']				= '(Mahasiswa, Profesional, dll)';		
   	$lang['or']						= 'atau';
   	$lang['sayaadalah']				= 'Saya adalah';
   	$lang['pilihpekerjaan']			= 'Pilih Pekerjaan';
   	$lang['pesanregistertutor1']	= 'Apabila Anda adalah guru, pengajar, pelatih, tentor atau siapapun yang ingin MENGAJAR di Indiclass, silakan lanjutkan untuk mendaftar.';
   	$lang['pesanregistertutor2']	= 'Jika Anda pelajar, mahasiswa, professional atau siapapun yang ingin BELAJAR di Indiclass, silakan mendaftar sebagai SISWA.';
	$lang['pesanumurumum']			= 'Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur mahasiswa, professional, dsbnya.';	
   	$lang['pesanumursiswa']			= 'Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur pelajar.';
	$lang['signup'] 				= 'PENDAFTARAN';
	$lang['sebagai']				= 'MENDAFTAR SEBAGAI';
   	$lang['pendaftaransiswa']		= 'PENDAFTARAN SISWA';
	$lang['daftar_siswa']			= 'PELAJAR';
	$lang['daftar_umum']			= 'UMUM';
	$lang['sstudent'] 				= 'PELAJAR';
	$lang['nonstudent'] 			= 'UMUM';
	$lang['datebirth'] 				= 'Tgl';
	$lang['monthbirth'] 			= 'Bulan';
	$lang['yearbirth'] 				= 'Tahun';
	$lang['male'] 					= 'Pria';
	$lang['women'] 					= 'Wanita';
	$lang['countrycode'] 			= 'Kode negara';
	$lang['alreadyaccount'] 		= 'Sudah punya akun? Masuk';
	$lang['orsignup'] 				= 'Atau silakan isi data dibawah ini';
	$lang['signupfacebook'] 		= ' Daftar dengan Facebook';
	$lang['signupgoogle'] 			= ' Daftar dengan Google';
	
	$lang['invalidfirstname'] 		= 'Nama Depan harap diisi';
	$lang['invalidlastname'] 		= 'Nama Belakang harap diisi';
	$lang['invalidpassword'] 		= 'Password harap diisi';
	$lang['invalidconfirmpassword'] = 'Password tidak sama';
	$lang['invalidtanggallahir'] 	= 'Tanggal Lahir harap dipilih';
	$lang['invalidbulanlahir'] 		= 'Bulan Lahir harap dipilih';
	$lang['invalidtahunlahir'] 		= 'Tahun Lahir harap dipilih';
	$lang['invalidnohape'] 			= 'No Hape harap diisi';
	$lang['invalidjenjanglevel']	= 'Jenjang harap dipilih';
	$lang['invalidemail'] 			= 'Alamat Email salah';


	$lang['btnsignup']				= 'Daftar';	
	$lang['createpassword'] 		= 'Membuat Password';
	$lang['laststep']				= 'Langkah terakhir, mohon lengkapi data diri Anda pada kolom di bawah ini untuk keamanan dan kenyamanan bertransaksi selanjutnya.';
	$lang['syarat']					= 'Saya telah membaca dan menyetujui';
	$lang['syarat1']			 	= 'Syarat dan Ketentuan';
	$lang['syarat2']				= 'dan Kebijakan Privasi Indiclass.';
	$lang['activationsuccess']		= 'Aktivasi akun berhasil,';
	$lang['activationsuccess2']		= 'Silakan masuk untuk memulai kelas';
	$lang['activationtutor']		= 'harap lengkapi data pribadi Anda, riwayat pendidikan, dan pengalaman mengajar untuk permintaan persetujuan untuk menjadi tutor Indiclass.';
	$lang['kewarganegaraan']		= 'Kewarganegaraan';
	$lang['kodereferral']			= 'Kode Referral';
	$lang['tidakpunyareferral']		= 'Tidak memiliki kode referral';
	$lang['referralsalah']			= 'kode referral salah';
	$lang['referralbenar']			= 'kode referral sesuai';
	$lang['lanjutkan']				= 'Lanjutkan';
	$lang['akunsudahada']			= 'Akun Sudah Terdaftar';
	$lang['akungoogle']				= 'Akun Google Anda sudah terdaftar, silakan gunakan akun yang lain';
	$lang['akunfacebook']			= 'Akun Facebook Anda sudah terdaftar, silakan gunakan akun yang lain';



	//Syarat Dan Ketentuan
	$lang['headersyarat']			= 'Syarat dan Ketentuan';
	$lang['sebagai_siswa']			= 'Sebagai Siswa / Peserta Didik';
	$lang['sebagai_tutor']			= 'Sebagai Tutor';
	$lang['isi_siswa']				= 'Indiclass.com adalah sebuah situs yang menghubungkan pelajar, mahasiswa dan masyarakat dengan pengajar yang tepat untuk membantu siswa maupun peserta di dalam kelas mempelajari keahlian/ pengetahuan baru, mendapatkan bantuan tambahan di luar sekolah/ kampus, maupun mengembangkan keahlian tertentu. Situs www.Indiclass.com ini dikelola oleh PT. Meetaza Prawira Media. Dengan mengakses dan menggunakan Situs www.Indiclass.com serta mendaftar menjadi Siswa atau Peserta Kelas di www.Indiclass.com, berarti Anda telah memahami dan menyetujui untuk terikat dan tunduk dengan semua peraturan yang berlaku di Indiclass.com.
Indiclass berhak memodifikasi ketentuan setiap saat tanpa pemberitahuan sebelumnya, dimana Anda secara sah terikat dengan ketentuan tersebut jika Anda menggunakan website dan/atau layanan terkait. Setiap perubahan di ketentuan akan segera berlaku efektif sejak dimuat di halaman ini, dengan tanggal efektif yang diperbaharui. Pastikan Anda kembali ke halaman ini secara berkala untuk memastikan Anda memahami versi terbaru dari ketentuan Indiclass.';

	$lang['isi_tutor']				= 'Indiclass.com adalah sebuah situs yang menghubungkan pelajar, mahasiswa dan masyarakat dengan pengajar yang tepat untuk membantu siswa maupun peserta di dalam kelas mempelajari keahlian/ pengetahuan baru, mendapatkan bantuan tambahan di luar sekolah/ kampus, maupun mengembangkan keahlian tertentu. Situs www.Indiclass.com ini dikelola oleh PT. Meetaza Prawira Media. Dengan mengakses dan menggunakan Situs www.Indiclass.com serta mendaftar menjadi Tutor di www.Indiclass.com, berarti Anda telah memahami dan menyetujui untuk terikat dan tunduk dengan semua peraturan yang berlaku di Indiclass.com. Indiclass berhak memodifikasi ketentuan setiap saat tanpa pemberitahuan sebelumnya, dimana Anda secara sah terikat dengan ketentuan tersebut jika Anda menggunakan website dan/atau layanan terkait. Setiap perubahan di ketentuan akan segera berlaku efektif sejak dimuat di halaman ini, dengan tanggal efektif yang diperbaharui. Pastikan Anda kembali ke halaman ini secara berkala untuk memastikan Anda memahami versi terbaru dari ketentuan Indiclass.com 
';

	$lang['sk_daftarsiswa']			= '1. Pendaftaran';
	$lang['sk_daftarsiswa1']		= 'Untuk registrasi menjadi Siswa, Anda harus mengisi semua kelengkapan biodata tanpa terkecuali dan menyertakan identitas asli Anda.';
	$lang['sk_daftarsiswa2']		= 'Anda harus mencantumkan nama lengkap (bukan nama alias).';
	$lang['sk_daftarsiswa3']		= 'Nomor telepon yang dicantumkan adalah nomor yang aktif, sehingga Indiclass.com dapat menghubungi sewaktu-waktu apabila diperlukan.';
	$lang['sk_daftarsiswa4']		= 'Memilih kelas sesuai dengan umur, tingkat pendidikan maupun keahlian yang sesuai.';
	$lang['sk_daftarsiswa5']		= 'Indiclass.com berhak untuk menolak aplikasi pendaftaran Anda sebagai Siswa dengan alasan yang jelas.';	

	$lang['sk_akunsiswa']			= '2. Akun Siswa';
	$lang['sk_akunsiswa1']			= 'Anda bertanggung jawab untuk menjaga kerahasiaan akun dan password dan membatasi akses ke komputer Anda.';
	$lang['sk_akunsiswa2']			= 'Anda setuju bertanggung jawab atas semua yang terjadi perihal penggunaan akun pribadi dan password Anda.';
	$lang['sk_akunsiswa3']			= 'Anda setuju untuk segera memberitahukan Indiclass.com tentang setiap penyalahgunaan akun atau pelanggaran keamanan lainnya yang berkaitan dengan akun Anda.';
	$lang['sk_akunsiswa4']			= 'Indiclass.com tidak akan bertanggung jawab atas kerugian atau kerusakan yang timbul dari kegagalan untuk mematuhi persyaratan-persyaratan ini.';
	$lang['sk_akunsiswa5']			= 'Indiclass.com berhak untuk menolak layanan atau menghentikan akun.';

	$lang['sk_profilsiswa']			= '3. Profil Siswa';
	$lang['sk_profilsiswa1']		= 'Anda setuju untuk mengisi semua informasi pribadi seakurat mungkin.';
	$lang['sk_profilsiswa2']		= 'Anda, dan bukan Indiclass.com, bertanggung jawab penuh atas semua materi yang Anda ikuti di Indiclass.com.';

	$lang['sk_caritutor']			= '4. Pencarian Tutor';
	$lang['sk_caritutor1']			= 'Anda memahami bahwa semua informasi mengenai Tutor disediakan oleh pihak ketiga yang berada di luar kontrol Indiclass.com.';
	$lang['sk_caritutor2']			= 'Kami tidak bertanggung jawab atas segala kerugian yang disebabkan oleh informasi pihak ketiga.';
	$lang['sk_caritutor3']			= 'Sebelum memasuki kelas private atau kelas umum, peserta maupun siswa wajib mengikuti pelajaran Tutor yang akan di pilih sebagai pengajar atau pemberi materi 	pelajaran.';
	$lang['sk_caritutor4']			= 'Setelah mengikuti pelajaran dari Tutor yang telah dipilih, siswa akan mendapat informasi apabila Tutor tersebut memberikan kelas umum maupun private.';
	$lang['sk_caritutor5']			= 'Tanggal dan jam kursus private yang Anda pilih pada form pencarian perlu mendapatkan persetujuan dari Tutor atau staff pengajar. Apabila Anda belum 	mendapatkan persetujuan dari Tutor atau Staff Pengajar yang telah Anda pilih. Anda 	dapat mencari Tutor atau pengajar lain setelah melakukan pembatalan pada permintaan kursus private sebelumnya.';
	$lang['sk_caritutor6']			= 'Jika Anda belum merubah atau membatalkan kelas private yang telah disepakati oleh Tutor atau staff pengajar, Anda akan dianggap mengikuti kelas private yang telah telah disetujui oleh Tutor atau staff pengajar tersebut.';
	

	$lang['sk_bayarsiswa']			= '5. Pembayaran';
	$lang['sk_bayarsiswa1']			= 'Anda setuju untuk membayar paket belajar sesuai dengan waktu yang ditetapkan oleh Indiclass.com.';
	$lang['sk_bayarsiswa2']			= 'Apabila Anda tidak membayar dalam waktu 2x24 jam, maka pemesanan dianggap dibatalkan.';
	$lang['sk_bayarsiswa3']			= 'Indiclass.com tidak menjamin pengembalian pembayaran apabila terjadi pembatalan atau keterlambatan proses belajar.';
	$lang['sk_bayarsiswa4']			= 'Apabila peserta kelas mengundurkan diri atau tidak bisa melanjutkan pembelajaran sampai kelas tersebut berakhir, maka uang yang sudah dibayarkan tidak bisa 	diambil lagi.';

	$lang['sk_perilakusiswa']		= '6. Perilaku dan Tata Tertib Siswa';
	$lang['sk_perilakusiswa1']		= 'Siswa yang telat untuk membayar paket belajar akan berdampak pada pembatalan pendaftaran belajar mengajar';
	$lang['sk_perilakusiswa2']		= 'Siswa harus menjaga etika dan kesopanan saat belajar dengan Tutor agar suasana belajar mengajar menjadi kondusif dan nyaman.';
	$lang['sk_perilakusiswa3']		= 'Siswa dan setiap pengguna layanan dilarang melanggar setiap hukum yang berlaku, hak-hak pihak lain baik hak intelektual, asasi, dan lainnya, dan aturan-aturan yang diatur pada Perjanjian ini.';
	$lang['sk_perilakusiswa4']		= 'Siswa dan setiap pengguna layanan dilarang memberikan informasi dan konten yang salah, tidak akurat, bersifat menyesatkan, bersifat memfitnah, bersifat 	asusila, 	mengandung pornografi, bersifat diskriminasi atau rasis.';
	$lang['sk_perilakusiswa5']		= 'Siswa dan setiap pengguna layanan dilarang menyebarkan spam, hal-hal yang tidak berasusila, atau pesan elektronik yang berjumlah besar, pesan bersambung';
	$lang['sk_perilakusiswa6']		= 'Siswa dan setiap pengguna layanan dilarang menyebarkan virus atau seluruh teknolog lainnya yang sejenis yang dapat merusak dan/atau merugikan Situs, afiliasinya, Penyedia, dan Pengguna lainnya.';
	$lang['sk_perilakusiswa7']		= 'Siswa dan setiap pengguna layanan dilarang memasukkan atau memindahkan fitur pada Situs tidak terkecuali tanpa sepengetahuan dan persetujuan dari Kami';
	$lang['sk_perilakusiswa8']		= 'Siswa dan setiap pengguna layanan dilarang menyimpan, meniru, mengubah, atau menyebarkan konten dan fitur Situs, termasuk cara pelayanan, konten, hak cipta 	dan intelektual yang terdapat pada Situs.';
	$lang['sk_perilakusiswa9']		= 'Siswa dan setiap pengguna layanan dilarang mengambil atau mengumpulkan informasi dari pengguna lain, termasuk alamat email, tanpa sepengetahuan 	pengguna lain';

	$lang['sk_informasisiswa']		= '7. Penggunaan Informasi dan Ketentuan Umum';
	$lang['sk_informasisiswa1']		= 'Penggunaan informasi akun Tutor dan Siswa untuk kepentingan Indiclass.com maupun  diperbolehkan dengan syarat tetap oleh persetujuan pihak Indiclass.com tanpa melalui perantara dan segala hal yang berkaitan informasi adalah hak Indiclass.com sepenuhnya';
	$lang['sk_informasisiswa2']		= 'Dengan berpartisipasi dalam Indiclass ini, maka berarti pendaftar telah membaca dan menyetujui syarat dan ketentuan yang berlaku. Indiclass berhak untuk menggunakan data serta informasi yang Anda berikan.';
	$lang['sk_informasisiswa3']		= 'Kami berhak membatasi atau tidak memberikan akses, atau memberikan akses yang berbeda untuk dapat membuka Situs dan fitur di dalamnya kepada masing-masing Pengguna, atau mengganti salah satu fitur atau memasukkan fitur baru tanpa pemberitahuan sebelumnya. Setiap Pengguna sadar bahwa jika Situs tidak dapat digunakan seluruhnya atau sebagian karena alasan apapun, maka usaha atau kegiatan apapun yang dilakukan Pengguna dapat terganggu. Setiap Pengguna dengan ini sepakat bahwa karena alasan apapun membebaskan Kami dari segala bentuk pertanggungjawaban terhadap Pengguna atau terhadap pihak ketiga jika yang bersangkutan tidak dapat menggunakan Situs (baik karena gangguan dalam bentuk apapun, dibatasinya akses, dilakukannya perubahan fitur atau tidak dimasukkannya lagi fitur tertentu atau karena alasan lain); atau jika komunikasi atau transmisi tertunda, gagal atau tidak dapat berlangsung; atau jika timbul kerugian (secara langsung, tidak langsung) karena digunakannya atau tidak dapat digunakannya Situs atau salah satu fitur di dalamnya. ';
	$lang['sk_informasisiswa4']		= 'Pengguna dengan ini mengetahui dan menyetujui bahwa penggunaan Situs ini merupakan tanggung jawab dan resiko dari Pengguna dan oleh karena itu 	Pengguna dengan ini melepaskan atau membebaskan Kami dari dan terhadap 	semua atau segala bentuk pertanggungjawaban kerugian atas penggunaan Situs';


	// SK TUTOR  

	$lang['sk_daftartutor']			= '1. Pendaftaran';
	$lang['sk_daftartutor1']		= 'Untuk registrasi menjadi Tutor, Anda harus mengisi semua kelengkapan biodata tanpa terkecuali dan menyertakan identitas asli Anda';
	$lang['sk_daftartutor2']		= 'Anda harus mencantumkan nama lengkap (bukan nama alias).';
	$lang['sk_daftartutor3']		= 'Nomor telepon yang dicantumkan adalah nomor yang aktif, sehingga Indiclass.com 	dapat menghubungi sewaktu-waktu apabila diperlukan';
	$lang['sk_daftartutor4']		= 'Memilih kelas sesuai dengan umur, tingkat pendidikan maupun keahlian yang sesuai';
	$lang['sk_daftartutor5']		= 'Indiclass.com berhak untuk menolak aplikasi pendaftaran Anda sebagai Tutor 	dengan alasan yang jelas';


	$lang['sk_akuntutor']			= '2. Akun Tutor';
	$lang['sk_akuntutor1']			= 'Anda bertanggung jawab untuk menjaga kerahasiaan akun dan password dan 	membatasi akses ke komputer Anda.';
	$lang['sk_akuntutor2']			= 'Anda setuju bertanggung jawab atas semua yang terjadi perihal penggunaan akun 	pribadi dan password Anda.';
	$lang['sk_akuntutor3']			= 'Anda setuju untuk segera memberitahukan Indiclass.com tentang setiap penyalahgunaan akun atau pelanggaran keamanan lainnya yang berkaitan dengan akun Anda.';
	$lang['sk_akuntutor4']			= 'Indiclass.com tidak akan bertanggung jawab atas kerugian atau kerusakan yang 	timbul dari kegagalan untuk mematuhi persyaratan-persyaratan ini.';
	$lang['sk_akuntutor5']			= 'Indiclass.com berhak untuk menolak layanan atau menghentikan akun.';


	$lang['sk_profiltutor']			= '3. Profil Tutor';
	$lang['sk_profiltutor1']		= 'Anda setuju untuk mengisi semua informasi pribadi seakurat mungkin.';
	$lang['sk_profiltutor2']		= 'Anda, dan bukan Indiclass.com, bertanggung jawab penuh atas semua materi yang Anda ikuti di Indiclass.com.';

	$lang['sk_bayartutor']			= '4. Pembayaran Untuk Tutor';
	$lang['sk_bayartutor1']			= 'Anda setuju untuk mengikuti prosedur pembayaran yang ditetapkan oleh Indiclass.com, yaitu pembayaran akan dibayarkan setiap bulannya dengan cut-off date pada tanggal 25 setiap bulannya';
	$lang['sk_bayartutor2']			= 'Jumlah yang akan Anda terima adalah sebesar 90% dari total jumlah biaya yang disepakati oleh Siswa, dipotong pajak penghasilan sesuai peraturan yang berlaku. ';

	$lang['sk_perilakututor']		= '5. Perilaku Tutor dan Tata Tertib Tutor';
	$lang['sk_perilakututor1']		= 'Tutor dan setiap pengguna layanan dilarang melanggar setiap hukum yang berlaku, hak-hak pihak lain baik hak intelektual, asasi, dan lainnya, dan aturan-aturan yang diatur pada Perjanjian ini.';
	$lang['sk_perilakututor2']		= 'Tutor harus mempersiapkan materi ajar secarang sebelum mengajar.';
	$lang['sk_perilakututor3']		= 'Tutor dan setiap pengguna layanan dilarang memberikan informasi dan konten yang salah, tidak akurat, bersifat menyesatkan, bersifat memfitnah, bersifat asusila, mengandung pornografi, bersifat diskriminasi atau rasis.';
	$lang['sk_perilakututor4']		= 'Tutor harus hadir tepat waktu sesuai jadwal yang telah disepakati.';
	$lang['sk_perilakututor5']		= 'Tutor dan setiap pengguna layanan dilarang menyebarkan spam, hal-hal yang tidak berasusila, atau pesan elektronik yang berjumlah besar, pesan bersambung.';
	$lang['sk_perilakututor6']		= 'Tutor dan setiap pengguna layanan dilarang menyebarkan virus atau seluruh teknologi lainnya yang sejenis yang dapat merusak dan/atau merugikan Situs, 	afiliasinya, Penyedia, dan Pengguna lainnya.';
	$lang['sk_perilakututor7']		= 'Tutor dan setiap pengguna layanan dilarang memasukkan atau memindahkan fitur pada Situs tidak terkecuali tanpa sepengetahuan dan persetujuan dari Kami.';
	$lang['sk_perilakututor8']		= 'Tutor dan setiap pengguna layanan dilarang menyimpan, meniru, mengubah, atau	menyebarkan konten dan fitur Situs, termasuk cara pelayanan, konten, hak cipta 	dan intelektual yang terdapat pada Situs.';
	$lang['sk_perilakututor9']		= 'Tutor dan setiap pengguna layanan dilarang mengambil atau mengumpulkan informasi dari pengguna lain, termasuk alamat email, tanpa sepengetahuan 	pengguna lain.';

	$lang['sk_informasitutor']		= '6. Penggunaan Informasi dan Ketentuan Umum';
	$lang['sk_informasitutor1']		= 'Penggunaan informasi akun Siswa dan Tutor untuk kepentingan Indiclass.com diperbolehkan dengan syarat tetap oleh persetujuan pihak Indiclass.com 	tanpa 	melalui perantara dan segala hal yang berkaitan informasi adalah hak Indiclass.com sepenuhnya.';
	$lang['sk_informasitutor2']		= 'Dengan berpartisipasi dalam Indiclass.com ini, maka berarti pendaftar telah membaca dan menyetujui syarat dan ketentuan yang berlaku. Indiclass.com 	berhak 	untuk 	menggunakan data serta informasi yang Anda berikan.';
	$lang['sk_informasitutor3']		= 'Kami berhak membatasi atau tidak memberikan akses, atau memberikan akses	yang berbeda untuk dapat membuka Situs dan fitur di dalamnya kepada masing-	masing Pengguna, atau mengganti salah satu fitur atau memasukkan fitur baru tanpa pemberitahuan sebelumnya. Setiap Pengguna sadar bahwa jika Situs tidak dapat digunakan seluruhnya atau sebagian karena alasan apapun, maka usaha atau kegiatan apapun yang dilakukan Pengguna dapat terganggu. Setiap Pengguna dengan ini sepakat bahwa karena alasan apapun membebaskan Kami dari segala bentuk pertanggungjawaban terhadap Pengguna atau terhadap pihak ketiga jika yang bersangkutan tidak dapat menggunakan Situs (baik karena gangguan dalam bentuk apapun, dibatasinya akses, dilakukannya perubahan fitur atau tidak dimasukkannya lagi fitur tertentu atau karena alasan lain); atau jika komunikasi atau transmisi tertunda, gagal atau tidak dapat berlangsung; atau jika timbul kerugian (secara langsung, tidak langsung) karena digunakannya atau tidak dapat digunakannya Situs atau salah satu fitur di dalamnya.';
	$lang['sk_informasitutor4']		= 'Pengguna dengan ini mengetahui dan menyetujui bahwa penggunaan Situs ini merupakan tanggung jawab dan resiko dari Pengguna dan oleh karena itu 	Pengguna dengan ini melepaskan atau membebaskan Kami dari dan terhadap 	semua atau segala bentuk pertanggungjawaban kerugian atas penggunaan Situs';


	// MAIN MENU
	$lang['home'] 					= 'Beranda';
	$lang['myclass'] 				= 'Kelasku';
	$lang['subject'] 				= 'Pelajaran';
	$lang['allsub'] 				= 'Semua Mapel'; 
	$lang['ondemand'] 				= 'Kelas Tambahan';
	$lang['history'] 				= 'Riwayat';
	$lang['profil'] 				= 'Data Diri';
	$lang['schedule'] 				= 'Jadwal';
	$lang['viewprofile'] 			= 'Lihat Data Diri';
	$lang['privacysetting'] 		= 'Pengaturan Privasi';
	$lang['settings'] 				= 'Pengaturan';
	$lang['logout'] 				= 'Keluar';
	$lang['togglefullscreen'] 		= 'Layar Penuh';
	$lang['no_schedule'] 			= "Tidak ada jadwal untuk minggu ini";
	$lang['no_class'] 				= "Tidak ada kelas";
	$lang['no_program']				= "Tidak ada program / penawaran yang tersedia untuk Anda saat ini";
	$lang['nothing_here'] 			= "Tidak ada apa-apa di sini, akun Anda belum di setujui";
	$lang['no_subjects'] 			= "Tidak ada pelajaran untuk jenjang pendidikan Anda";
	$lang['no_subjectskids'] 		= "Tidak ada pelajaran untuk jenjang pendidikan Anak Anda";
	$lang['no_tutor_here']			= "Tidak ada Tutor di Pelajaran ";
	$lang['checkdemand'] 			= 'Dengan menekan tombol PILIH maka permintaan kelas secara otomatis disampaikan kepada Tutor bersangkutan. Apabila Tutor menerima permintaan Anda tersebut, maka Anda akan menerima Email detail pembayaran kelas yang sudah Anda request.';
	$lang['allsubject']				= 'Semua Pelajaran';
	$lang['competency']				= 'Kompetensi';
	$lang['notification']			= 'Notifikasi';
	$lang['error']					= 'Kesalahan!';
	$lang['alreadyonline'] 			= "Anda hanya bisa masuk satu kelas dalam satu waktu.";
	$lang['alreadyactivation'] 		= "Email Anda telah diaktifkan sebelumnya. Jika Anda lupa kata sandi Anda, silakan klik tautan Lupa Kata Sandi di bawah ini.";	
	
	$lang['livenow']				= 'Sedang Berlangsung';
	$lang['comingup']				= 'Akan Datang';
	$lang['missed']					= 'Sudah Lewat';
	$lang['purchasecomfirmation']	= 'Konfirmasi Pembelian';
	$lang['konfirmasibelikelas']	= 'Apakah Anda ingin membeli kelas ini ?';
	$lang['konfirmasigabungkelas']	= 'Apakah Anda ingin bergabung dengan kelas ini ?';
	$lang['berbagilinkkelas']		= 'Bagikan tautan Kelas Anda';

	//Footer
	$lang['reports'] 				= 'Laporan';
	$lang['support'] 				= 'Dukungan';
	$lang['contact'] 				= 'Kontak';
	$lang['copyright'] 				= 'Hak Cipta';

	//Page Index
	$lang['title_pg_index'] 		= 'Beranda';

	//Page Subject
	$lang['title_pg_subject'] 		= 'Pelajaran';
	$lang['follow'] 				= 'Ikuti';
	$lang['unfollow']				= 'Berhenti ikuti';
	$lang['followed'] 				= 'Diikuti';
	$lang['no_tutor'] 				= 'Tidak ada tutor di pelajaran ini';
	$lang['tutorname']				= 'Nama Guru';
	$lang['transactionlist']		= 'Detail';
	$lang['yourrequest']			= 'Permintaan Anda';
	$lang['accountlist']			= 'Daftar Rekening';
	$lang['paymentconfirmation']    = 'Konfirmasi Pembayaran';
	$lang['underpaymentnotif']    	= 'Terdapat pembayaran yang kurang, silakan cek email Anda untuk lebih detail.';

	//Page OnDemand
	$lang['findfriends']			= 'Tambahkan temanmu melalui email';
	$lang['titleinvite']			= 'Undang Teman Grup anda';
	$lang['note']					= 'Catatan';
	$lang['note1']					= 'Minimal Undangan 1 Orang';
	$lang['note2']					= 'Maksimal Undangan 3 Orang';
	$lang['note3']					= 'Pastikan Anda memiliki Saldo yang cukup';
	$lang['note4']					= 'Pastikan teman Anda memiliki Saldo yang cukup';
	$lang['note5']					= 'Kami akan memotong Saldo Anda sementara';
	$lang['note6']					= 'Maksimal teman Anda menyetujui ajakan Anda 2 Jam';
	$lang['searchduration'] 		= 'Cari Durasi';
	$lang['titlesearchpriv']		= 'Cari Tutor - Kelas Privat';
	$lang['titlesearchgrup']		= 'Cari Tutor - Kelas Grup';
	$lang['titleresult']			= 'Hasil Pencarian Tutor';
	$lang['alertfirst']				= 'Hasil pencarian tutor akan muncul disini';
	$lang['demandprice']			= 'Harga';
	$lang['demandname']				= 'Tutor';
	$lang['demandtime']				= 'Waktu';	
	$lang['demanddate']				= 'Tanggal';
	$lang['demandpayown']			= 'Dibayar 1 orang';
	$lang['demandpayshare']			= 'Bagi Rata';
	$lang['alertsubjectnull']		= 'Harap pilih pelajaran !';
	$lang['alertdatenull']			= 'Harap pilih tanggal !';
	$lang['alertdurationnull']		= 'Harap pilih durasi !';

	//Page All Subject
	$lang['title_pg_allsub'] 		= 'Semua Mapel';
	$lang['select_class'] 			= 'Kelas';
	$lang['select_level'] 			= 'Pilih Jenjang Pendidikan';
	$lang['status'] 				= 'Status';
	$lang['button_search'] 			= 'Cari';

	//Page Profile
	$lang['noage'] 					= 'Tidak ada umur';
	$lang['nogender'] 				= 'Tidak ada Jenis Kelamin';
	$lang['noreligion'] 			= 'Tidak ada Agama';
	$lang['noaddress'] 				= 'Tidak ada Alamat';
	$lang['noemail'] 				= 'Tidak ada Email';
	$lang['nonumber'] 				= 'Tidak ada Nomor Handphone';
	$lang['nofamilynumber'] 		= 'Tidak ada Nomor Keluarga';
	$lang['nobirthday']				= 'Tidak ada Ulang tahun';
	$lang['nobirthplace']			= 'Tidak ada Tempat lahir';
	$lang['nousername']				= 'Tidak ada Nama';
	$lang['uploadcompleted'] 		= 'Unggah Selesai...';
	$lang['uploadphoto'] 			= 'Unggah Foto Anda';
	$lang['upload'] 				= 'Unggah';

	$lang['tab_about'] 				= 'Tentang';
	$lang['tab_account'] 			= 'Akun';
	$lang['tab_cp'] 				= 'Ubah Kata Sandi';
	$lang['tab_pr'] 				= 'Data Diri';
	$lang['tab_ps'] 				= 'Sekolah';
	$lang['school_jenjang'] 		= 'Tingkatan Sekolah';
	$lang['school_name'] 			= 'Nama Sekolah';
	$lang['school_address'] 		= 'Alamat Sekolah';
	$lang['upd_profpic'] 			= 'Ganti Foto Profil';
	$lang['summary'] 				= 'Ringkasan';
	$lang['basic_info'] 			= 'Informasi Dasar';
	$lang['contact_info'] 			= 'Informasi Kontak';
	$lang['contact_left_side'] 		= 'Kontak';
	$lang['totcon'] 				= 'Jumlah Koneksi';
	$lang['edit'] 					= 'Sunting';
	$lang['username'] 				= 'Nama Pengguna';
	$lang['first_name'] 			= 'Nama Depan';
	$lang['last_name'] 				= 'Nama Belakang';
	$lang['full_name'] 				= 'Nama Lengkap';
	$lang['gender'] 				= 'Jenis kelamin';
	$lang['placeofbirth'] 			= 'Tempat Lahir';
	$lang['birthday'] 				= 'Tanggal Lahir';
	$lang['religion'] 				= 'Agama';
	$lang['rel_stat'] 				= 'Status Hubungan';
	$lang['mobile_phone'] 			= 'Nomor Handphone';
	$lang['email_address'] 			= 'Alamat Email';
	$lang['select_religion'] 		= 'Pilih Agama';
	$lang['postalcode'] 			= 'Kode Pos';
	$lang['age'] 					= 'Umur';
	$lang['phonefamily'] 			= 'Nomor Orang Tua';	
	$lang['selectprovinsi'] 		= 'Pilih Provinsi';
	$lang['selectkabupaten'] 		= 'Pilih Kabupaten/Kota';
	$lang['selectkecamatan'] 		= 'Pilih Kecamatan';
	$lang['selectkelurahan'] 		= 'Pilih Kelurahan/Desa';
	$lang['password'] 				= 'Kata Sandi';	
	$lang['passwordnotmatch'] 		= 'Kata sandi tidak cocok';

	// TUTOR
	$lang['next'] 					= 'Selanjutnya';
	$lang['previous'] 				= 'Sebelumnya';

	//Khusus Approval Tutor 
	$lang['select_religionn'] 		= 'Agama';
	$lang['selectprovinsii'] 		= 'Propinsi';
	$lang['selectkabupatenn'] 		= 'Kabupaten/Kota';
	$lang['selectkecamatann'] 		= 'Kecamatan';
	$lang['selectkelurahann'] 		= 'Kelurahan/Desa';
	$lang['selectgrade'] 			= 'Tingkatan Kelas';
	$lang['selectlevel'] 			= 'Tingkat Pendidikan';
	$lang['numberid']				= 'Nomor ID';
	$lang['selectnumberid']			= 'KTP / SIM / Paspor';
	$lang['inputtahun']				= 'Masukkan tahun yang benar';
	$lang['next']					= 'Berikutnya';
	$lang['back']					= 'Kembali';
	$lang['submitapproval']			= 'Kirim untuk perstujuan';
	$lang['not_graduated']			= 'Belum lulus';
	$lang['fotowajah']				= 'Silakan unggah foto diri Anda dengan jelas menunjukkan wajah anda';


	//Khusus AGAMA
	$lang['Islam'] 					= 'Islam';
	$lang['Protestan'] 				= 'Kristen Protestan';
	$lang['Katolik'] 				= 'Kristen Katolik';
	$lang['Hindu'] 					= 'Hindu';
	$lang['Buddha'] 				= 'Buddha';
	$lang['Konghucu'] 				= 'Konghucu';

	//On Account Tab
	$lang['select_gender'] 			= 'Jenis kelamin';
	$lang['home_address'] 			= 'Alamat';
	$lang['old_pass'] 				= 'Kata Sandi Lama';
	$lang['new_pass'] 				= 'Kata Sandi Baru';
	$lang['confirm_pass'] 			= 'Konfirmasi Kata Sandi';
	$lang['button_save'] 			= 'Simpan';
	$lang['button_cancel']			= 'Batal';

	//profile tutor
	$lang['educational_level'] 		= 'Tingkat';
	$lang['educational_competence'] = 'Kompetensi';
	$lang['educational_institution']= 'Lembaga';
	$lang['institution_address'] 	= 'Alamat Institusi';
	$lang['graduation_year'] 		= 'Tahun Pendidikan';
	$lang['description_experience'] = 'Pelajaran / Bidang';
	$lang['year_experience'] 		= 'Tahun Mengajar';
	$lang['place_experience'] 		= 'Nama Lembaga / Institusi';
	$lang['information_experience'] = ' Informasi Tambahan';
	$lang['fromyear'] 				= 'Dari Tahun';
	$lang['from'] 					= 'Dari';
	$lang['toyear'] 				= 'Sampai Tahun';
	$lang['to'] 					= 'Sampai';
	$lang['pengalaman_mengajar'] 	= 'Pengalaman Mengajar';
	$lang['latbelpend'] 			= 'Latar Belakang Pendidikan';
	$lang['selfdescription'] 		= 'Deskripsi Diri';
	$lang['nodescription']			= 'Tidak ada Deskripsi';
	$lang['spasi']					= '';
	// $lang['contoh']

	//Main Menu Tutor
	$lang['hometutor'] 				= 'Dashboard';
	$lang['setavailabilitytime'] 	= 'Atur Ketersediaan Waktu';
	$lang['selectsubjecttutor'] 	= 'Pilih Pelajaran';
	$lang['approvalondemand'] 		= 'Persetujuan Kelas Tambahan';
	$lang['select'] 				= 'Pilih';
	$lang['selected'] 				= 'Telah dipilih';
	$lang['profiltutor'] 			= 'Data Diri';
	$lang['description_tutor'] 		= 'Deskripsi Guru';
	$lang['report']					= 'Laporan';
	$lang['reportteaching']			= 'Laporan Tutor';

	//Main Menu Admin
	$lang['homeadminn'] 			= 'Dashboard';
	$lang['approval_tutor']			= 'Persetujuan ( Tutor )';
	$lang['verify_subject'] 		= 'Verifikasi ( Pelajaran Tutor )';
	$lang['unverify_subject'] 		= 'Batal Verifikasi ( Pelajaran Tutor )';
	$lang['schedule_engine'] 		= 'Jadwal';
	$lang['buttonapproval'] 		= 'Kirim Persetujuan';
	$lang['approval'] 				= 'Lengkapi Profil';
	$lang['approve'] 				= 'Setuju';
	$lang['decline'] 				= 'Tolak';
	$lang['adminsupport'] 			= 'Bantuan';
	$lang['adminaddsubject'] 		= 'Tambah Pelajaran';
	$lang['adminreferralcode'] 		= 'Kode promo';
	$lang['adminaddreferral'] 		= 'Tambah Kode promo';
	$lang['adminusereferral'] 		= 'Penggunaan promo';
	$lang['adminpayment'] 			= 'Pembayaran';
	$lang['adminclassmulticast'] 	= 'Kelas multicast';
	$lang['adminclassprivate'] 		= 'Kelas private';
	$lang['adminclassgroup'] 		= 'Kelas kelompok';
	$lang['adminchannel'] 			= 'Channel';
	$lang['adminregisterchannel'] 	= 'Daftar channel';
	$lang['adminpointchannel'] 		= 'Poin channel';
	$lang['adminusepointchannel'] 	= 'Penggunaan poin channel';
	$lang['adminactivitychannel'] 	= 'Aktivitas channel';
	$lang['adminpocketmoney'] 		= 'Uang saku';
	$lang['admindatatransaction'] 	= 'Data transaksi';
	$lang['adminconfirmtransaction']= 'Konfirmasi transaksi';
	$lang['adminannouncement']		= 'Pengumuman';
	$lang['adminalltutor']			= 'Semua Tutor';
	$lang['tambahadminkuotakelas']	= 'Kuota Kelas';
	$lang['adminkuotakelas']		= 'Permintaan Kuota Kelas';

	//choosesubjectutor
	$lang['ikutisubject'] 			= 'Pilih';
	$lang['tidakikutisubject'] 		= 'Berhenti Pilih';

	//else
	$lang['subdisc'] 				= "Sub Bahasan";

	//quizmenu
	$lang['sidequiz']				= "Kuis";
	$lang['quizlist']				= "Daftar Kuis";

	//choosee
	$lang['confrimdemand'] 			= 'Pilih Kelas';
	$lang['confrimpayment'] 		= 'Rincian Order';
	$lang['canceldemand'] 			= 'batal';
	$lang['ikutidemand'] 			= 'Pilih';
	$lang['diikutidemand'] 			= 'Dipilih';
	$lang['nohistory'] 				= 'Tidak ada Riwayat ';
	$lang['duration'] 				= 'Pilih Durasi';
	$lang['searchtime'] 			= 'Pilih Waktu';
	$lang['searchdate'] 			= 'Pilih Tanggal';
	$lang['cancel'] 				= 'Batal';
	$lang['nodata'] 				= 'Tidak ada data';
	$lang['loading']        		= 'Mohon tunggu...';

	//resend Email
	$lang['resendaccount']			= 'Aktivasi Akun Kirim Ulang';
	$lang['enteremail']				= "Masukkan alamat email Anda di bawah ini";
	$lang['accountactivation']		= 'email aktivasi akun';

	//approval tutor
	$lang['photo']				 	= 'Foto Profil';
	$lang['photoupload']			= 'Unggah Foto';
	$lang['usecamera']				= 'Gunakan kamera';
	$lang['capture']				= 'Ambil Foto';
	$lang['retakecapture']			= 'Ambil ulang Foto';
	$lang['tambahdata']				= 'Tambah';
	$lang['hapusdata']				= 'Hapus';


	//aprovalondemand
	$lang['norequest'] 				= 'Tidak ada permintaan';
	$lang['areyousure']			 	= 'Apakah Anda yakin';
	$lang['yes']					= 'Ya';
	$lang['no']						= 'Tidak';
	$lang['aksi']					= 'Aksi';
	$lang['kelas']					= 'Kelas';

	$lang['uangsaku']				= 'Uang Saku';
	$lang['chooselanguage']			= 'Pilih Bahasa';
	$lang['passed']					= 'Telah berlalu';
	
	//SUBJECT_NAME
	$lang['subject_4'] 				= "Bahasa Indonesia";
	$lang['subject_5'] 				= "Bahasa Indonesia";
	$lang['subject_6'] 				= "Matematika";
	$lang['subject_7'] 				= "Matematika";
	$lang['subject_8'] 				= "Bahasa Inggris";
	$lang['subject_9'] 				= "Bahasa Inggris";
	$lang['subject_10'] 			= "IPA (Ilmu Pengetahuan Alam)";
	$lang['subject_11'] 			= "IPS (Ilmu Pengetahuan Sosial)";
	$lang['subject_12'] 			= "Olahraga";
	$lang['subject_13'] 			= "PKn";
	$lang['subject_15'] 			= "Komputer";
	$lang['subject_408'] 			= "Keuangan";
	$lang['subject_16'] 			= "Kelas Memasak";
	$lang['subject_404'] 			= "Room Tester";

	//REPLAY
	$lang['replay']					= "Putar Ulang";
	$lang['replaytitle']			= "Memutar video dimana saja dan kapan saja."; 	

	// ------------------------- KHUSUS ALERT-------------------------------- //

	// ini untuk aksi login
	$lang['wrongpassword']			= 'Email atau password Anda salah';
	$lang['checkemail'] 			= 'Silakan Periksa email Anda untuk memverifikasi akun';
	$lang['completeprofile']		= 'Silakan melengkapi profil Anda untuk persetujuan pendaftaran guru';
	$lang['requestsent']			= 'Permintaan Anda telah dikirim, silakan menunggu persetujuan dari Tim Indiclass. Paling mungkin, kami akan mencoba untuk menghubungi Anda melalui email atau nomor telepon Anda. Terima kasih.';
	$lang['youremail']				= 'Email Anda sebelumnya terdaftar, silakan cek email Anda untuk aktivasi akun atau klik';
	$lang['youremail2'] 			= 'Email Anda sebelumnya terdaftar, silakan cek email Anda untuk aktivasi akun';
	$lang['youremail3']				= 'Silakan cek email Anda untuk aktivasi akun atau klik';

	//ini untuk daftarbaru
	$lang['previously1']			= 'Email Anda sebelumnya terdaftar, silakan cek email Anda untuk aktivasi akun atau klik';
	$lang['previously2']			= 'untuk mengirim ulang email';
	$lang['accountregistered']  	= 'akun telah terdaftar dan diaktifkan sebelumnya, klik';
	$lang['accountregistered2']  	= 'akun telah terdaftar dan diaktifkan sebelumnya';
	$lang['successfullyregistered']	= 'Berhasil terdaftar, silakan cek email Anda untuk aktivasi akun';
	$lang['successfullyregistered']	= 'Berhasil terdaftar';
	$lang['captcha'] 			 	= 'Silakan periksa CAPTCHA Anda';
	$lang['errorregister']			= 'Kesalahan mendaftar';
	$lang['successfullyregis']		= 'Berhasil mendaftar';
	$lang['checkemail']				= 'Silakan, periksa email Anda untuk memverifikasi akun ini';
	$lang['completetutorapproval']	= 'Silakan, melengkapi profil Anda untuk persetujuan pendaftaran guru';
	$lang['linkexpired'] 			= 'Link sudah kedaluwarsa';
	$lang['approvesuccess']			= 'Setujui sukses';

	//ini untuk resendemail
	$lang['successfullysent']		= 'Berhasil dikirim, silakan cek email Anda untuk proses aktivasi akun.';
	$lang['failed']					= 'Gagal';

	//ini untuk daftartutor
	$lang['previouslyregistered']	= 'Email Anda sebelumnya terdaftar, silakan cek email Anda untuk aktivasi akun atau klik';
	$lang['emailresend']			= 'untuk mengirim ulang email';
	$lang['tutoraccountregistered'] = 'Email telah terdaftar dan diaktifkan sebelumnya, silakan gunakan email lain untuk mendaftar';
	$lang['tologin']				= 'untuk login';
	$lang['tutorregistered']		= 'Berhasil terdaftar, silakan cek email Anda untuk proses verifikasi.';

	//ini untuk aksi ubah password
	$lang['failedpassword'] 		= 'Gagal, konfirmasi password tidak cocok';
	$lang['oldpasswordfailed']		= 'Gagal, password lama Anda salah';
	$lang['successfullypassword']   = 'Sandi Anda berhasil diubah';
	$lang['successfullyselecting']  = 'Berhasil memilih guru dan pelajaran';
	$lang['unfollowsuccessfully']   = 'Berhasil berhenti mengikuti';
	$lang['failedselect']			= 'Gagal untuk memilih guru dan pelajaran';

	//ini untuk choosesub
	$lang['successfullychoose'] 	= 'Berhasil memilih pelajaran';
	$lang['failedchoosen'] 			= 'Gagal, Anda telah memilih pelajaran tersebut';
	$lang['erroroccured'] 			= 'Terjadi kesalahan !';

	//ini untuk unchoosesub
	$lang['successfullyunfollow'] 	= 'Berhasil berhenti mengikuti pelajaran';
	$lang['failedunchoose']			= 'Gagal, Anda memiliki pelajaran yang sudah terjadwal';

	//ini untuk sendlinkweb+android
	$lang['failedsendemail'] 		= 'Gagal mengirim, email Anda tidak terdaftar';
	$lang['sentlink']				= 'Email berhasil dikirim, silakan cek email Anda';

	//ini untuk aksiforgot
	$lang['failedchange'] 			= 'Gagal mengubah password Anda';
	$lang['registered']			 	= 'Berhasil terdaftar!';

	$lang['logsuccess'] 			= 'Berhasil keluar';
	$lang['settingdone']			= 'Pengaturan selesai';
	$lang['enterclass']				= 'Masuk Kelas';
	$lang['successfullyupdate']		= 'Sukses perbarui data Anda';
	$lang['failedupdate']			= 'Gagal perbarui data Anda';
	$lang['successfullydemand']		= 'Permintaan kelas Anda sedang menunggu persetujuan Tutor bersangkutan. dan Pembayaran menunggu hingga tutor kami menerima kelas anda.';
	$lang['successaddtocart']		= 'Kelas berhasil dimasukkan ke Keranjang Belanja';
	$lang['faileddemand'] 			= 'Permintaan permintaan gagal';
	$lang['approvalcomplete']		= 'Persetujuan lengkap!';
	$lang['approvalfailed']			= 'Persetujuan gagal!';
	$lang['saved']					= 'Disimpan.';
	$lang['successfullyprofile']	= 'Profil Berhasil Diperbarui.';
	$lang['titledemandconfrim']		= 'Pembelian Kelas';

	// ini untuk tutor
	$lang['registrationfailed']		= 'Pendaftaran guru gagal. Silakan hubungi kami.';
	$lang['step']					= 'Langkah';
	$lang['account']				= 'Akun';
	$lang['settime']				= 'Atur Waktu';
	$lang['noempty']				= 'Tidak boleh kosong';
	$lang['multigrade'] 			= 'Multi Jenjang';

	//ini untuk admin
	$lang['welcomeadmin']			= 'Selamat datang admin ';
	
	// ini untuk log_avtime
	$lang['sukses_delete']				= 'Berhasil menghapus.';

	// ---------------------------END ALERT--------------------------------- //

	// ------------------------- START RIWAYAT ATAU LOG USER ------------------------------ //

	$lang['choosesubject']			= 'Memilih pelajaran';
	$lang['classenter']				= 'Masuk Kelas';

	// ------------------------- END RIWAYAT ATAU LOG USER -------------------------------- //


	// ------------------------- START UANG SAKU ------------------------------ //

	$lang['menu1']					= 'Detail';
	$lang['menu2']					= 'Daftar Rekening';
	$lang['menu3']					= 'Top Up';
	$lang['menu4']					= 'Cara Top Up';

	// MENU DETAIL
	$lang['yourbalance']			= 'Saldo anda';
	$lang['usebalance']				= 'Penggunaan Saldo';
	$lang['detailusages']			= 'Detail Penggunaan';

	// DETAIL
	$lang['tanggal']				= 'Tanggal';
	$lang['keterangan'] 			= 'Keterangan';
	$lang['debet']					= 'Debit';
	$lang['kredit']					= 'Kredit';
	$lang['saldoawal']				= 'Saldo Awal';
	$lang['saldoakhir']				= 'Saldo Akhir';
	$lang['detailtopup']			= 'Rincian Top Up';
	$lang['jumlah']					= 'Jumlah';
	$lang['metodepembayaran']		= 'Metode Pembayaran';
	$lang['notransaction']			= 'Tidak ada Transaksi Top up';
	$lang['pendingtransaction']		= 'Menunggu pembayaran';
	$lang['suksestransaction']		= 'Pembayaran sukses';
	$lang['ubahkonfirmasi']			= 'Ubah Konfirmasi';
	$lang['tidakadadata']			= 'Tidak ada data';
	$lang['konfirmasi']				= 'Konfirmasi';
	$lang['deskripsitopup']			= 'Isi saldo Anda untuk pembelian yang cepat dan mudah.';
	$lang['pembayaran']				= 'Pembayaran';
	$lang['nominalinput']			= 'Masukan Nominal';
	$lang['pilihpayment']			= 'Pilih metode pembayaran yang ingin digunakan';

	$lang['transferbank']			= 'Transfer Bank';
	$lang['kartukredit']			= 'Kartu Kredit';
	$lang['selanjutnya']			= 'Lanjut';
	$lang['transaksi']				= 'Transaksi';
	$lang['segera']					= 'Segera melakukan transfer pembayaran untuk tambah saldo sebelum :';
	$lang['segerabayar']			= 'Segera melakukan transfer pembayaran sebelum :';
	$lang['expiredtext']			= 'Sebelum batas waktu dibawah ini :';
	$lang['amountpayment']			= 'Segera melakukan transfer pembayaran sebesar :';
	$lang['bedatransfer']			= 'Perbedaan nilai transfer akan menghambat proses verifikasi';
	$lang['konfirmasipembayaran']	= 'Konfirmasi Pembayaran';
	$lang['konfirmasiunderpayment']	= 'Konfirmasi kurang bayar';
	$lang['pembayaranmelalui']		= 'Pembayaran dapat dilakukan ke salah satu rekening berikut:';
	$lang['notes']					= 'Catatan';
	$lang['notesdes']				= 'Top up saldo Anda dianggap BATAL jika sampai pukul';
	$lang['notesorder']				= "Kelas Anda dianggap BATAL jika sampai pukul";
	$lang['hari']					= 'Hari';
	$lang['notpaid']				= '(2×12 jam) tidak dibayar.';
	$lang['notpaidorder']			= '(1x1 jam) tidak dibayar.';
	$lang['kembali']				= 'Kembali';
	$lang['notif_waitingpayment'] 	= 'Menunggu pembayaran Anda';
	$lang['notif_successpayment'] 	= 'Pembayaran telah selesai';
	$lang['notif_classexpired'] 	= 'Pembayaran melewati batas waktu yang ditentukan.';
	$lang['notif_classexpired2']	= 'Apabila Anda sudah melakukan pembayaran, silakan lakukan konfirmasi pembayaran di atas. Untuk selanjutnya mohon hubungi customer service atau email ke finance@meetaza.com dengan menyertakan nama lengkap, email/username dan bukti transfer.';
	$lang['notif_underpayment'] 	= 'Menunggu kekurangan pembayaran';
	//KONFIRMASI
	$lang['tujuantransfer']			= 'Tujuan Transfer';
	$lang['pilih']					= 'Pilih';
	$lang['jumlahtransfer']			= 'Sejumlah';
	$lang['inputjumlah']			= 'Masukan jumlah transfer';
	$lang['jumlahharus']			= 'Jumlah yang harus dibayar ';
	$lang['fromrekening']			= 'Dari Rekening';
	$lang['masukanrekening']		= 'Masukan nomer rekening anda';
	$lang['namadirekening']			= 'Nama yang tertera di rekening';
	$lang['denganmetode']			= 'Dengan metode';
	$lang['pilihmetode']			= 'Pilih metode transfer';
	$lang['buktipembayaran']		= 'Bukti Pembayaran';
	$lang['peringatan']				= '( Tidak usah di isi jika bukti sebelumnya sudah benar )';
	$lang['totalpayment']			= 'Jumlah Pembayaran';
	$lang['besarpayment']			= 'Sebesar';


	//NOTIF
	$lang['student'] 				= "Pelajar";
	$lang['ask_class'] 				= "meminta kelas tambahan untuk kelas";
	$lang['has_call_support'] 		= "telah melakukan request untuk melakukan panggilan support";
	$lang['readysupport'] 			= "call support sudah siap untuk membantu anda, klik ";
	$lang['starting'] 				= "untuk memulai.";
	$lang['finishverifikasi'] 		= "Yeay verifikasi telah berhasil, silakan pilih pelajaran dan guru untuk memulai kelas private.";
	$lang['tutorsuccessregis'] 		= "Berhasil terdaftar, silakan cek email Anda untuk proses verifikasi.";
	$lang['successverifikasitutor'] = "Verifikasi telah berhasil, Silakan lengkapi data diri Anda agar kami bisa segera memproses.";
	$lang['successsendapproval'] 	= "Permintaan Anda telah dikirim, silakan tunggu persetujuan dari Tim Indiclass. Terima kasih.";

	$lang['heyadmin'] 				= "Hai admin";
	$lang['heyadmin2'] 				= "Terdapat Tutor yang baru registrasi, silakan cek di menu persetujuan.";
	$lang['tutornotcoming'] 		= 'Mohon untuk menghubungi support kami kembali dikarenakan Anda tidak menjawab panggilan kami.';
	$lang['paymentmulticast'] 		= 'Terdapat transaksi baru di menu transaksi multicast. Segera lakukan pemeriksaan.';
	$lang['paymentprivate'] 		= 'Terdapat transaksi baru di menu transaksi private. Segera lakukan pemeriksaan.';
	$lang['paymentgroup'] 			= 'Terdapat transaksi baru di menu transaksi group. Segera lakukan pemeriksaan.';
	$lang['notifpaymentstudent']	= 'Terima kasih telah melakukan pembelian kelas di Indiclass. Detail pembayaran bisa diklik Disini.';
	$lang['yakinkeluar']			= "Apakah Anda yakin ingin keluar kelas?";
	$lang['ubahcamera']				= "Ubah perangkat video";
	$lang['alertlimakali']			= "Maaf, Sebelum mendapat persetujuan dari pihak kami, anda hanya diperbolehkan membuat kelas sebanyak 5 kali.";
	$lang['alertdibawahwaktu'] 		= "Anda sudah memiliki kelas pada rentang waktu yang Anda masukkan.Mohon atur waktu atau durasi yang berbeda.";
	$lang['alertalreadyclass'] 		= "Anda sudah memiliki kelas pada rentang waktu yang Anda masukkan.";

	$lang['infosucces_groupuser'] 	= "Pembayaran kelas kelompok Anda BERHASIL kami terima. Kelas sudah bisa dimulai.";
	$lang['infosucces_grouptouser'] = "Yeay kelas kelompok Anda BERHASIL terbuat. Kelas sudah bisa dimulai.";
	$lang['infosucces_grouptotutor1']= "Kelas";
	$lang['infosucces_grouptotutor2']= "berhasil terbuat dan uang sudah masuk ke saldo anda. Kelas akan mulai pada ";
	$lang['waitingpaymentuser1']	= "Anda berhasil menyetujui permintaan kelas kelompok student";
	$lang['waitingpaymentuser2']	= "dan menunggu pembayaran oleh student tersebut.";
	$lang['alertrejectclass1']		= "Mohon maaf, permintaan ajakan kelas telah DITOLAK oleh tutor";
	$lang['alertrejectclass2']		= "anda tetap bisa mencari tutor lain di menu Extra class";

	// -------------------------- END UANG SAKU ------------------------------- //

	// -------------------------- LANDING PAGE  ------------------------------- // 
	$lang['navbartop2']				= "MASUK";
	$lang['navbartop1']				= "DAFTAR";
	$lang['banneratas1'] 			= "Fitur belajar online interaktif yang lengkap serta banyak pilihan guru atau tutor handal dan menyenangkan.";
	$lang['banneratas2']			= "Tersedia di :";

	$lang['bannert1'] 				= "Memberikan suasana belajar yang sangat interaktif layaknya dalam kelas sesungguhnya.";
	$lang['bannert2'] 				= "Membuat materi pembelajaran jadi lebih mudah dipahami seperti menggunakan papan tulis sesungguhnya.";
	$lang['bannert3'] 				= "Berbagi tampilan layar dari tutor untuk lebih memudahkan dalam penyampaian pelajaran.";
	$lang['bannert4'] 				= "Siswa dapat bertanya langsung kepada tutor dengan tatap muka online.";
	$lang['bannert5'] 				= "Memungkinkan komunikasi chatting baik antar siswa maupun antara siswa dengan tutor.";

	$lang['bannerb1'] 				= "Masuk kelas tak melulu harus pagi ...! Di Indiclass kamu bebas menentukan sendiri waktu untuk kelas yang ingin kamu ikuti, sesuaikan dengan waktu yang kamu miliki, kapanpun.";
	$lang['bannerb2']				= "Di Indiclass kamu bebas bertanya, berdiskusi dengan para tutor yang handal dan menyenangkan tentang pelajaran yang belum kamu pahami, sedekat berdiskusi dengan teman atau bahkan orangtuamu sendiri.";
	$lang['bannerb3']				= "Buat kalian yang ingin les privat, Indiclass juga menyediakan fitur tersebut dengan guru yang bervariasi, menyenangkan dan pastinya sangat berpengalaman.";
	$lang['bannerb4']				= "Kini jarak bukan lagi halangan buat kalian yang ingin belajar kelompok, ajak temanmu untuk mengikuti kelas yang sama di Indiclass. Belajar secara berkelompok pun kini jadi lebih mudah dilakukan.";

	$lang['bannerdapat'] 			= "Dapat diakses ";
	$lang['bannerdimana']			= "Dimana saja"; 
	$lang['dan']					= " dan ";
	$lang['bannerkapan']			= "Kapan saja";
	$lang['bannerunduh']			= "Unduh aplikasi Indiclass sekarang juga.";

	$lang['bannertentang'] 			= "Tentang Situs";
	$lang['bannersyarat']			= "Syarat dan Ketentuan";
	$lang['bannerbawah1']			= "Indiclass menyediakan layanan pembelajaran online dimana di dalamnya berisi banyak ruang kelas virtual.";
	$lang['bannerbawah2']			= "Indiclass memungkinkan komunikasi interaktif antara guru dan siswa serta antar siswa.";



	// Khusus Channel

	$lang['settingchannel']			= "Pengaturan Channel";
	$lang['klik']					= "Klik";
	$lang['kechannel']				= "untuk menuju ke halaman channel anda";
	$lang['welcomechannel']			= "Selamat Datang di Channel, ";
	$lang['channelname']			= "Nama Channel";
	$lang['channeldescription']		= "Deskripsi Channel";
	$lang['channelphone']			= "Kontak Channel";
	$lang['channeladdress']			= "Alamat Channel";
	$lang['channellink']			= "Link Channel";
	$lang['channelabout']			= "Tentang Channel";
	$lang['channelemail']			= "Emai Channel";
	$lang['changelogo']				= "Ganti Logo Channel";
	$lang['changebanner']			= "Ganti Banner Channel";
	$lang['maxsizelogo']			= "Maksimal File 2 MB.";
	$lang['sizelogo']				= "Ukuran logo harap 512 x 512 pixels atau lebih.";
	$lang['maxsizebanner']			= "Maksimal File 5 MB.";
	$lang['sizebanner']				= "Ukuran banner yang disarankan: 1386 x 462 pixels atau lebih.";
	$lang['successchangedata']		= "Berhasil Mengubah Data";
	$lang['failedchangedata']		= "Nama channel link sudah ada, silakan ganti dengan yang lain";
	$lang['successupload']			= "Gagal mengunggah gambar";
	$lang['failedupload']			= "Berhasil mengunggah gambar";
	$lang['loginfirst']				= "Silahkan masuk terlebih dahulu sebelum membeli paket";

	// Khusus KIDS

	$lang['mykids']					= "Anak Saya";
	$lang['mykids_registrasion']	= "Daftar";
	$lang['mykids_class']			= "Kelas";
	$lang['mykids_subject']			= "Pelajaran";
	$lang['mykids_demand']			= "Kelas tambahan";
	$lang['mykids_ondemand']		= "Data kelas tambahan";