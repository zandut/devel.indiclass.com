<header id="header-2" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('topup/navbar'); ?>
</header>

<section id="main">

    <section id="content">
        <div class="container invoice">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Konfirmasi Pembayaran</h2>
                <ul class="actions">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><a href="<?php echo base_url();?>first/topup">Top up</a></li>
                        <li>Konfirmasi Pembayaran</li>
                    </ol>                    
                </ul>
            </div>
<!-- 
            <div class="card m-t-20">
                <div class="card-header">
                    <h2>Rincian Top up Saldo</h2><small class="m-t-15">Transaksi tanggal 16-12-2016</small>
                    <hr>               
                </div>

                <div class="card-header" style="margin-top: -3%;">
                    
                    <div class="row">
                        
                    </div>
                    <hr>
                    <div class="pull-left" style="margin-top: -10px;">
                    	  <button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;">Kembali</button>                 
                    </div>
                </div>


                <hr>
            </div>  -->          
            <?php
            if ($_GET['trans'] == 'edt') {
                $trasaksiid = $_GET['transaksiid'];
                $getjumlah = $this->db->query("SELECT * FROM log_trf_confirmation WHERE trx_id='$trasaksiid'")->row_array();
                $hasiljumlah = number_format($getjumlah['jumlah'], 0, ".", ".");
                $flag = $getjumlah['metod_tr'];
                $flagbank = $getjumlah['bank_id'];
            ?>
            <div class="card">
                <div class="card-header ch-alt">
                    <div class="pull-left f-14" style="margin-top: 8px;">No transaksi <?php echo $this->session->userdata('idorder'); ?></div><br>
                    <div class="pull-right f-14" style="margin-top: -10px;">Transaksi tanggal <?php echo $getjumlah['time']; ?></div><br>
                </div>
                
                <div class="card-body card-padding" style="background: white;">                
                    <div class="row">                        
                        <form method="post" action="<?php echo base_url('Process/confrim'); ?>" enctype="multipart/form-data">
                            <div class="col-md-2 m-t-25"></div>
                            <div class="col-md-8 m-t-25 m-b-25" style="border: 1px solid gray; border-radius: 5px; padding: 15px;">
                                

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Tujuan Transfer</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <!-- <select required name="bank" class="select2 col-md-6 form-control">
                                            <?php
                                                $jen = $this->db->query("SELECT * FROM master_bank WHERE bank_id='008'")->result_array();
                                                
                                                    echo "<option value='".$getjumlah['bank_id']."' selected='true'>".$jen['bank_name']."</option>";
                                                                                               
                                            ?>
                                            </select> -->
                                            <select required name="bank" class="select2 col-md-6 form-control">                                      
                                                <option disabled selected>Pilih Metode Transfer</option>
                                                <option value="014" <?php if($flagbank=="014"){ echo "selected='true'";} ?>>BC</option>                                                  
                                                <option value="009" <?php if($flagbank=="009"){ echo "selected='true'";} ?>>BNI</option>
                                                <option value="008" <?php if($flagbank=="008"){ echo "selected='true'";} ?>>MANDIRI</option>
                                                <option value="002" <?php if($flagbank=="002"){ echo "selected='true'";} ?>>BRI</option>
                                                <option value="022" <?php if($flagbank=="022"){ echo "selected='true'";} ?>>CIMB NIAGA</option>
                                            </select>
                                            <input type="text" name="order_id" id="order_id" value="<?php echo $trasaksiid; ?>" />                                               
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Sejumlah</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="nominaltransaksi" id="nominal" required class="input-sm form-control fg-input" placeholder="Isi Jumlah transfer" minlength="9" step="500" maxlength="14" min="10000" max="500000" value="<?php echo $getjumlah['jumlah'] ?>"/>
                                            <input type="text" name="nominalsave" id="nominalsave" value="<?php echo $getjumlah['jumlah'] ?>" hidden />
                                        </div>
                                        <label class="c-gray m-t-5"><ins><small>( Jumlah pembayaran Rp. <?php echo $hasiljumlah; ?> )</small></ins></label>
                                    </div>
                                </div>                                  
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Dari Rekening</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="norekeningbank" placeholder="No rekening anda" value="<?php echo $getjumlah['norek_tr'] ?>" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Atas nama pemilik</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="namapemilikbank" placeholder="Nama yang tertera di rekening" value="<?php echo $getjumlah['nama_tr'] ?>" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Dengan metode</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="metodepembayaran" class="select2 col-md-6 form-control">                                      
                                                <option disabled selected>Pilih Metode Transfer</option>
                                                <option value="atm" <?php if($flag=="atm"){ echo "selected='true'";} ?>>ATM Transfer</option>                                                  
                                                <option value="Internet Banking" <?php if($flag=="internetbanking"){ echo "selected='true'";} ?>>Internet Banking</option>
                                                <option value="Mobile Banking" <?php if($flag=="mobilebanking"){ echo "selected='true'";} ?>>Mobile Banking</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-5">Bukti Bayar</label>
                                    <div class="col-sm-9 m-t-5">
                                        <div class="fg-line">
                                            <input type="file" required name="buktibayar" accept="image/*" value="<?php echo $getjumlah['bukti_tr']; ?>">
                                        </div>
                                        <small class="c-gray">( Tidak usah di isi jika bukti sebelumnya sudah benar )</small>
                                    </div>
                                </div>                                

                                <br><br><br>
                                <button class="btn btn-md c-white btn-block m-t-10 bgm-blue" style="border: 1px solid gray;">Ubah Konfirmasi</button>
                            </div>
                            <div class="col-md-2 m-t-25"></div>
                                                    
                        </form>

                    </div>
                </div>
                
                <!-- <footer class="p-20">
                    <a href="<?php echo base_url();?>first/topup"><button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;">Konfirmasi</button></a>
                </footer> -->
            </div>
            <?php
            }
            else if ($_GET['trans'] == 'knf') {                               
                $trasaksiid = $_GET['transaksiid'];
                $getjumlah = $this->db->query("SELECT * FROM log_transaction WHERE trx_id='$trasaksiid'")->row_array();
                $hasiljumlah = number_format($getjumlah['credit'], 0, ".", ".");
            ?>
            <div class="card">
                <div class="card-header ch-alt">
                    <div class="pull-left f-14" style="margin-top: 8px;">No transaksi <?php echo $this->session->userdata('idorder'); ?></div><br>
                    <div class="pull-right f-14" style="margin-top: -10px;">Transaksi tanggal <?php echo $getjumlah['timestamp']; ?></div><br>
                </div>
                
                <div class="card-body card-padding" style="background: white;">                
                    <div class="row">                        
                        <form method="post" action="<?php echo base_url('Process/confrim'); ?>" enctype="multipart/form-data">
                            <div class="col-md-2 m-t-25"></div>
                            <div class="col-md-8 m-t-25 m-b-25" style="border: 1px solid gray; border-radius: 5px; padding: 15px;">

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Tujuan Transfer</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="bank" class="select2 col-md-6 form-control">
                                            <?php
                                               $allbank = $this->db->query("SELECT * FROM master_bank")->result_array();
                                               echo '<option disabled="disabled" selected="" value="0">Pilih Tujuan Rekening</option>'; 
                                               foreach ($allbank as $row => $v) {
                                                    echo '<option value="'.$v['bank_id'].'">'.$v['bank_name'].'</option>';
                                                }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Sejumlah</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="nominaltransaksi" id="nominal" required class="input-sm form-control fg-input" placeholder="Isi Jumlah transfer" minlength="9" step="500" maxlength="14" min="10000" max="500000"/>
                                            <input type="text" name="nominalsave" id="nominalsave" hidden />
                                        </div>
                                        <label class="c-gray m-t-5"><ins><small>( Jumlah pembayaran Rp. <?php echo $hasiljumlah; ?> )</small></ins></label>
                                    </div>
                                </div>                                  
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Dari Rekening</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="norekeningbank" placeholder="No rekening anda" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Atas nama pemilik</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="namapemilikbank" placeholder="Nama yang tertera di rekening" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10">Dengan metode</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="metodepembayaran" class="select2 col-md-6 form-control">                                      
                                                <option disabled selected>Pilih Metode Transfer</option>
                                                <option value="atm">ATM Transfer</option>                                                  
                                                <option value="internetbanking">Internet Banking</option>
                                                <option value="mobilebanking">Mobile Banking</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-5">Bukti Bayar</label>
                                    <div class="col-sm-9 m-t-5">
                                        <div class="fg-line">
                                            <input type="file" required name="buktibayar" accept="image/*">
                                        </div>
                                    </div>
                                </div>                                

                                <br><br><br>
                                <button class="btn btn-md c-white btn-block m-t-10 bgm-blue" style="border: 1px solid gray;">Konfirmasi</button>
                            </div>
                            <div class="col-md-2 m-t-25"></div>
                                                    
                        </form>

                    </div>
                </div>
                
                <!-- <footer class="p-20">
                    <a href="<?php echo base_url();?>first/topup"><button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;">Konfirmasi</button></a>
                </footer> -->
            </div>
            <?php
                }
            ?>
        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function(){
        $('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $("#nominal").keyup(function() {
          var clone = $(this).val();
          var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
          $("#nominalsave").val(cloned);
        });
    });

</script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({
                    locale: 'id'
                });
            });
        </script>
