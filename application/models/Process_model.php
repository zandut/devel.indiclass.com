<?php 

class Process_model extends CI_Model{

	function __construct() {
        $this->transTable = 'log_paypal';
    }

	public function demand_me($subject_id='',$tutor_id='',$start_time='',$date='',$duration='',$user_utc='', $topic='', $id_requester='', $hargaakhir ='', $hargatutor='')
	{
		$fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm WHERE id_user={$tutor_id}")->row_array();
		if(!empty($fcm_token)){
			$fcm_token = $fcm_token['fcm_token'];
		}

		$server_utc = $this->Rumus->getGMTOffset();
		$interval = $user_utc - $server_utc;
		$date_requested = DateTime::createFromFormat ('Y-m-d H:i', $date." ".$start_time );
		/*$start_time = intval(substr($start_time, 0,2))*60 + intval(substr($start_time, 3,2));
		$start_time = $start_time+$interval;
		$start_time = sprintf('%02d',($start_time/60)).":".sprintf('%02d',($start_time%60));*/
		$date_requested->modify("-".$interval ." minutes");
		$date_requested = $date_requested->format('Y-m-d H:i:s');

		// $id_requester = $this->session->userdata('id_user');
		// $fcm_token = $data['fcm_token'];
		// $start_time .= ":00";
		$topic = $this->db->escape_str($topic);
		$student_name = $this->session->userdata('user_name');
		$request_id = $this->Rumus->get_new_request_id('private');
		$qr = $this->db->simple_query("INSERT INTO tbl_request(request_id,id_user_requester,tutor_id,subject_id,topic,price,price_tutor,date_requested,duration_requested,user_utc) VALUES('$request_id','{$id_requester}','{$tutor_id}','{$subject_id}','{$topic}','{$hargaakhir}','{$hargatutor}','{$date_requested}','{$duration}','{$user_utc}')");
		// $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
		$typeclass = 'private';
		if($qr){
			$this->Rumus->notif_extra_class($tutor_id,$fcm_token,$student_name,$subject_id,$typeclass);
			/*$simpan_notif = $this->Rumus->create_notif($notif='Siswa '.$student_name.' telah melakukan request ke anda. Silahkan cek di persetujuan kelas tambahan.',$notif_mobile='Siswa '.$student_name.' telah melakukan request ke anda. Silahkan cek di persetujuan kelas tambahan.',$notif_type='single',$id_user=$tutor_id,$link='https://indiclass.id/tutor/approval_ondemand');*/
			return 1;
		}else{
			return -1;
		}
	}

	public function demand_me_kids($subject_id='',$tutor_id='',$start_time='',$date='',$duration='',$user_utc='',$child_id='', $topic='')
	{
		$fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm WHERE id_user={$tutor_id}")->row_array();
		if(!empty($fcm_token)){
			$fcm_token = $fcm_token['fcm_token'];
		}

		$server_utc = $this->Rumus->getGMTOffset();
		$interval = $user_utc - $server_utc;
		$date_requested = DateTime::createFromFormat ('Y-m-d H:i', $date." ".$start_time );
		/*$start_time = intval(substr($start_time, 0,2))*60 + intval(substr($start_time, 3,2));
		$start_time = $start_time+$interval;
		$start_time = sprintf('%02d',($start_time/60)).":".sprintf('%02d',($start_time%60));*/
		$date_requested->modify("-".$interval ." minutes");
		$date_requested = $date_requested->format('Y-m-d H:i:s');

		$id_requester = $child_id;
		// $fcm_token = $data['fcm_token'];
		// $start_time .= ":00";
		$topic = $this->db->escape_str($topic);
		$student_name = $this->session->userdata('user_name');
		$qr = $this->db->simple_query("INSERT INTO tbl_request(id_user_requester,tutor_id,subject_id,topic,date_requested,duration_requested,user_utc) VALUES('{$id_requester}','{$tutor_id}','{$subject_id}','{$topic}','{$date_requested}','{$duration}','{$user_utc}')");

		if($qr){
			$this->Rumus->notif_extra_class($tutor_id,$fcm_token,$student_name,$subject_id);
			/*$simpan_notif = $this->Rumus->create_notif($notif='Siswa '.$student_name.' telah melakukan request ke anda. Silahkan cek di persetujuan kelas tambahan.',$notif_mobile='Siswa '.$student_name.' telah melakukan request ke anda. Silahkan cek di persetujuan kelas tambahan.',$notif_type='single',$id_user=$tutor_id,$link='https://indiclass.id/tutor/approval_ondemand');*/
			return 1;
		}else{
			return -1;
		}
	}

	public function demand_me_grup($subject_id='',$tutor_id='',$start_time='',$date='',$duration='', $id_friends='', $metod='',$user_utc='',$topic='', $harga='', $harga_tutor='')
	{
		$fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm WHERE id_user={$tutor_id}")->row_array();
		if(!empty($fcm_token)){
			$fcm_token = $fcm_token['fcm_token'];
		}

		$id_requester = $this->session->userdata('id_user');
		$server_utc = $this->Rumus->getGMTOffset();
		$interval = $user_utc - $server_utc;
		$date_requested = DateTime::createFromFormat ('Y-m-d H:i', $date." ".$start_time );
		/*$start_time = intval(substr($start_time, 0,2))*60 + intval(substr($start_time, 3,2));
		$start_time = $start_time+$interval;
		$start_time = sprintf('%02d',($start_time/60)).":".sprintf('%02d',($start_time%60));*/
		$date_requested->modify("-".$interval ." minutes");
		$date_requested = $date_requested->format('Y-m-d H:i:s');

		// $date = $data['date'];
		$student_name = $this->session->userdata('user_name');
		// $participant['id_friends'] = [];
			
		// $ids = explode(",", $id_friends);
		// foreach($ids as $key => $id) {
		// 	$participant['id_friends'][$key]['id_user'] = $id;
		// 	$participant['id_friends'][$key]['status'] = 0;
		// }
		// array_push($participant['id_friends'], array('id_user' => $id_requester, 'status' => 1));

		// $price = $this->db->query("SELECT harga_cm FROM tbl_service_price WHERE tutor_id='$tutor_id' AND subject_id='$subject_id' AND class_type='group'")->row_array()['harga_cm'];
		// $hargaakhir = ($duration/900)*$price;
		// if($metod == 'bagi_rata'){
		// 	$price = ($price/(count($participant['id_friends'])));	
		// }

		$topic = $this->db->escape_str($topic);		
		// $idfriends = {$id_friends};      
		$request_id = $this->Rumus->get_new_request_id('group');
		$qr = $this->db->simple_query("INSERT INTO tbl_request_grup(request_id,id_user_requester,tutor_id,subject_id,topic,price,price_tutor,id_friends,method_pembayaran,harga_kelas,date_requested,duration_requested,user_utc) VALUES('$request_id','{$id_requester}','{$tutor_id}','{$subject_id}','{$topic}','{$harga}','{$harga_tutor}','','{$metod}','{$harga}','{$date_requested}','{$duration}','{$user_utc}')");
		$typeclass = 'group';
		if($qr){
			// $ids = explode(",", $id_friends);
			// foreach($ids as $id) {				
			// 	$simpan_notif = $this->Rumus->create_notif($student_name.' telah mengajak anda untuk ikut Group Class. Silahkan cek di menu Extra Class untuk melihat detail pelajaran dan biaya, atau anda bisa klik link disini.',$id.' telah mengajak anda untuk ikut Group Class. Silahkan cek di menu Extra Class untuk melihat detail pelajaran dan biaya, atau anda bisa klik link disini.',$notif_type='single',$id_user=$id,$link='https://indiclass.id/Dataondemand');
			// }

			$this->Rumus->notif_extra_class($tutor_id,$fcm_token,$student_name,$subject_id,$typeclass);
			
			// "Fikri telah mengajak anda untuk ikut group class. Silahkan cek di menu Extra class untuk melihat detail pelajaran dan biaya, atau bisa anda klik disini.";
			// $simpan_notif = $this->Rumus->create_notif($notif='',$notif_mobile='',$notif_type='single',$id_user=$tutor_id,$link='https://indiclass.id/tutor/approval_ondemand');

			return 1;
		}else{
			return -1;
		}
	}

	public function demand_me_grup_kids($subject_id='',$tutor_id='',$start_time='',$date='',$duration='', $id_friends='', $metod='',$user_utc='', $child_id='')
	{
		$fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm WHERE id_user={$tutor_id}")->row_array();
		if(!empty($fcm_token)){
			$fcm_token = $fcm_token['fcm_token'];
		}

		$id_requester = $child_id;
		$server_utc = $this->Rumus->getGMTOffset();
		$interval = $user_utc - $server_utc;
		$date_requested = DateTime::createFromFormat ('Y-m-d H:i', $date." ".$start_time );
		/*$start_time = intval(substr($start_time, 0,2))*60 + intval(substr($start_time, 3,2));
		$start_time = $start_time+$interval;
		$start_time = sprintf('%02d',($start_time/60)).":".sprintf('%02d',($start_time%60));*/
		$date_requested->modify("-".$interval ." minutes");
		$date_requested = $date_requested->format('Y-m-d H:i:s');

		$date = $data['date'];
		$student_name = $this->session->userdata('user_name');
		$participant['id_friends'] = [];
			
		$ids = explode(",", $id_friends);
		foreach($ids as $key => $id) {
			$participant['id_friends'][$key]['id_user'] = $id;
			$participant['id_friends'][$key]['status'] = 0;
		}
		array_push($participant['id_friends'], array('id_user' => $id_requester, 'status' => 1));

		$price = $this->db->query("SELECT harga FROM tbl_service_price WHERE tutor_id='$tutor_id' AND subject_id='$subject_id' AND class_type='group'")->row_array()['harga'];
		if($metod == 'bagi_rata'){
			$price = ($price/(count($participant['id_friends'])));	
		}

		$participant = json_encode($participant);
		// $idfriends = {$id_friends};      
		$qr = $this->db->simple_query("INSERT INTO tbl_request_grup(id_user_requester,tutor_id,subject_id,id_friends,method_pembayaran,harga_kelas,date_requested,duration_requested,user_utc) VALUES('{$id_requester}','{$tutor_id}','{$subject_id}','{$participant}','{$metod}','$price','{$date_requested}','{$duration}','{$user_utc}')");

		if($qr){
			$ids = explode(",", $id_friends);
			foreach($ids as $id) {				
				$simpan_notif = $this->Rumus->create_notif($student_name.' telah mengajak anda untuk ikut Group Class. Silahkan cek di menu Extra Class untuk melihat detail pelajaran dan biaya, atau anda bisa klik link disini.',$id.' telah mengajak anda untuk ikut Group Class. Silahkan cek di menu Extra Class untuk melihat detail pelajaran dan biaya, atau anda bisa klik link disini.',$notif_type='single',$id_user=$id,$link='https://indiclass.id/Dataondemand');
			}

			$this->Rumus->notif_extra_class($tutor_id,$fcm_token,$student_name,$subject_id);
			
			// "Fikri telah mengajak anda untuk ikut group class. Silahkan cek di menu Extra class untuk melihat detail pelajaran dan biaya, atau bisa anda klik disini.";
			// $simpan_notif = $this->Rumus->create_notif($notif='',$notif_mobile='',$notif_type='single',$id_user=$tutor_id,$link='https://indiclass.id/tutor/approval_ondemand');

			return 1;
		}else{
			return -1;
		}
	}

	public function approve_demand($request_id='',$template='',$user_utc='',$tutor_id='')
	{
		$id_user = $tutor_id;
		$exist = $this->db->query("SELECT trq.*, uangsaku.us_id, uangsaku.active_balance FROM tbl_request as trq INNER JOIN uangsaku ON trq.id_user_requester=uangsaku.id_user WHERE trq.tutor_id='{$id_user}' AND trq.request_id='{$request_id}'")->row_array();

		if(!empty($exist)){
			
			$id_user_requester = $exist['id_user_requester'];
			$balance = $exist['active_balance'];
			$uangsaku_id = $exist['us_id'];
			$subject_id = $exist['subject_id'];
			// $price = $this->db->query("SELECT harga, harga_cm FROM tbl_service_price WHERE subject_id={$subject_id} AND tutor_id={$id_user} AND class_type='private'")->row_array();
			// $priceh = $price['harga'];
			// $pricecm = $price['harga_cm'];
			// if($balance >= $pricecm){

				// PROCESSING DEMAND TOTAL
				$date_requested = $exist['date_requested'];
				// $time_requested = $exist['time_requested'];
				$duration = $exist['duration_requested'];
				// $avtime_id = $exist['avtime_id'];
				$id_user_requester = $exist['id_user_requester'];
				
				$subject_name = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id={$subject_id}")->row_array()['subject_name'];

				$server_utc = $this->Rumus->getGMTOffset();
				$interval   = $user_utc - $server_utc;

				$this->db->simple_query("UPDATE tbl_request SET approve=2, template='$template' WHERE request_id='{$request_id}'");
				$simpan_notif = $this->Rumus->create_notif($notif='Permintaan anda telah di setujui oleh tutors. Email detail pembayaran sudah kami kirimkan. Silahkan melakukan pembayaran kelas anda.',$notif_type='single',$id_user=$id_user_requester,$link='https://indiclass.id/');

				return 1;
			// }
			// return -1;
		}
		return "SELECT trq.*, uangsaku.us_id, uangsaku.active_balance FROM tbl_request as trq INNER JOIN uangsaku ON trq.id_user_requester=uangsaku.id_user WHERE trq.tutor_id='{$id_user}' AND trq.request_id='{$request_id}'";
	}

	public function approve_demand_group($request_id='',$template='',$user_utc='',$tutorid='')
	{
		$id_user = $tutorid;
		$exist = $this->db->query("SELECT * FROM tbl_request_grup WHERE tutor_id='{$id_user}' AND request_id='{$request_id}'")->row_array();

		if(!empty($exist)){
			$subject_id = $exist['subject_id'];
			$id_user_requester = $exist['id_user_requester'];
			// $price = $exist['harga_kelas'];
			// $new_pricecm = $this->db->query("SELECT harga FROM tbl_service_price WHERE subject_id='$subject_id' AND tutor_id='$id_user' AND class_type='group'")->row_array()['harga'];
			// PROCESSING DEMAND TOTAL
			$enough = true;

				$date_requested = $exist['date_requested'];
				$duration = $exist['duration_requested'];
				$id_user_requester = $exist['id_user_requester'];
				
				$subject_name = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id={$subject_id}")->row_array()['subject_name'];

				$server_utc = $this->Rumus->getGMTOffset();
				$interval   = $user_utc - $server_utc;

				$this->db->simple_query("UPDATE tbl_request_grup SET approve=2, template='{$template}' WHERE request_id='{$request_id}'");
				$simpan_notif = $this->Rumus->create_notif($notif='Permintaan anda telah di setujui oleh tutor. Kelas akan dimulai pada '.$date_requested.'.',$notif_mobile='Permintaan anda telah di setujui oleh tutor. Kelas akan dimulai pada '.$date_requested.'.',$notif_type='single',$id_user=$id_user_requester,$link='https://indiclass.id/');
				return 1;
			// }		
		}
		return -1;
	}

	public function reject_demand($request_id='')
	{
		$processreject = $this->db->query("UPDATE tbl_request SET approve='-1' WHERE request_id='{$request_id}'");
		if ($processreject) {
			$getstudent = $this->db->query("SELECT id_user_requester FROM tbl_request WHERE request_id={$request_id}")->row_array();
			$nametutor = $this->session->userdata('user_name');
			$simpan_notif = $this->Rumus->create_notif($notif='Mohon maaf permintaan kelas private anda dibatalkan / tidak diterima oleh tutor, Mohon maaf atas ketidaknyamannya. Terima kasih.'.$nametutor.'.',$notif_mobile='Mohon maaf permintaan kelas private anda dibatalkan / tidak diterima oleh tutor, Mohon maaf atas ketidaknyamannya. Terima kasih. '.$nametutor.'.',$notif_type='single',$id_user=$getstudent['id_user_requester'],$link='https://indiclass.id/');
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function reject_demand_group($request_id='')
	{
		$processreject = $this->db->query("UPDATE tbl_request_grup SET approve='-1' WHERE request_id='{$request_id}'");
		if ($processreject) {
			$getstudent = $this->db->query("SELECT id_user_requester,id_friends FROM tbl_request_grup WHERE request_id={$request_id}")->row_array();

			$nametutor = $this->session->userdata('user_name');
			$list_student = json_decode($getstudent['id_friends'],true);
			foreach ($list_student['id_friends'] as $key => $value) {
				$ids = $value['id_user'];
				$simpan_notif = $this->Rumus->create_notif($notif='Mohon maaf permintaan kelas group anda dibatalkan / tidak diterima oleh tutor, Mohon maaf atas ketidaknyamannya. Terima kasih.'.$nametutor.'.',$notif_mobile='Mohon maaf permintaan kelas group anda dibatalkan / tidak diterima oleh tutor, Mohon maaf atas ketidaknyamannya. Terima kasih. '.$nametutor.'.',$notif_type='single',$id_user=$ids,$link='https://indiclass.id/');
			}			
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function midadd($table, $where)
	{		
		if ($where['payment_type'] == "echannel") {
			$processadd = $this->db->query("INSERT INTO log_midtrans_transaction (transaction_id,id_user,order_id,status_code,gross_amount,payment_type,bill_key,biller_code,pdf_url,signature_key,transaction_status,fraud_status,status_message,transaction_time) VALUES ('$where[transaction_id]','$where[id_user]','$where[order_id]','$where[status_code]','$where[gross_amount]','$where[payment_type]','$where[bill_key]','$where[biller_code]','$where[pdf_url]','$where[signature_key]','$where[transaction_status]','$where[fraud_status]','success','$where[transaction_time]')");

			if ($processadd) {	
				$datetimee = new DateTime($where['transaction_time']);
				$datetimee->modify('+1 day');
				$datelimitt = $datetimee->format('Y-m-d H:i:s');
				$this->db->query("INSERT INTO log_transaction (trx_id, id_user, code_transaction, keterangan, payment_method, credit, debit, flag, timelimit) VALUES ('$where[order_id]','$where[id_user]','401','Top up Saldo dengan no Transaksi $where[order_id]','$where[payment_type]','$where[gross_amount]','0','0','$datelimitt')");	
				$return = 1;
			}
			else
			{
				$return = 0;
			}
		}
		else if ($where['payment_type'] == "bank_transfer") {
			$processadd = $this->db->query("INSERT INTO log_midtrans_transaction (transaction_id,id_user,order_id,status_code,gross_amount,payment_type,pdf_url,permata_va,signature_key,transaction_status,fraud_status,status_message,transaction_time) VALUES ('$where[transaction_id]','$where[id_user]','$where[order_id]','$where[status_code]','$where[gross_amount]','$where[payment_type]','$where[pdf_url]','$where[permata_va_number]','$where[signature_key]','$where[transaction_status]','$where[fraud_status]','success','$where[transaction_time]')");

			if ($processadd) {	
				$datetimee = new DateTime($where['transaction_time']);
				$datetimee->modify('+1 day');
				$datelimitt = $datetimee->format('Y-m-d H:i:s');
				$this->db->query("INSERT INTO log_transaction (trx_id, id_user, code_transaction, keterangan, payment_method, credit, debit, flag, timelimit) VALUES ('$where[order_id]','$where[id_user]','401','Top up Saldo dengan no Transaksi $where[order_id]','$where[payment_type]','$where[gross_amount]','0','0','$datelimitt')");	
				$return = 1;
			}
			else
			{
				$return = 0;
			}
		}
		else
		{
			$processadd = $this->db->query("INSERT INTO log_midtrans_transaction (transaction_id, id_user, order_id, status_code, masked_card, approval_code, bank, gross_amount, bill_key, biller_code, pdf_url, payment_type, signature_key, transaction_status, fraud_status, status_message, transaction_time) VALUES ('$where[transaction_id]','$where[id_user]','$where[order_id]','$where[status_code]','$where[masked_card]','$where[approval_code]','$where[bank]','$where[gross_amount]','$where[bill_key]','$where[biller_code]','$where[pdf_url]','$where[payment_type]','$where[signature_key]','$where[transaction_status]','$where[fraud_status]','success','$where[transaction_time]')");

			if ($processadd) {	
				$datetimee = new DateTime($where['transaction_time']);
				$datetimee->modify('+1 day');
				$datelimitt = $datetimee->format('Y-m-d H:i:s');
				$this->db->query("INSERT INTO log_transaction (trx_id, id_user, code_transaction, keterangan, payment_method, credit, debit, flag, timelimit) VALUES ('$where[order_id]','$where[id_user]','401','Top up Saldo dengan no Transaksi $where[order_id]','$where[payment_type]','$where[gross_amount]','0','1','$datelimitt')");	
				$return = 1;
			}
			else
			{
				$return = 0;
			}
		}		

		return $return;
		
	}

	public function midtradd($table, $where)
	{
		$date = date("Y-m-d H:i:s");
		$datetime = new DateTime($date);
		$datetime->modify('+2 day');
		$limitdate = $datetime->format('Y-m-d H:i:s');

		$processtradd = $this->db->query("INSERT INTO log_transaction (trx_id, id_user, code_transaction, keterangan, payment_method, credit, debit, flag, timelimit) VALUES ('$where[order_id]','$where[id_user]','402','Top up saldo dengan no Transaksi $where[order_id]','bank_transfer','$where[jumlah]','0','0','$limitdate')");
		if ($processtradd) {
			$return['tanggalhariini'] = $date;
			$return['datelimit'] = $limitdate; 			
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function tradd($table, $where)
	{
		$type_confirm = $where['type_confirm'];
		if ($type_confirm == "private" || $type_confirm == "group") {
			$type_confirm = "on_demand";
		}

		if ($type_confirm=="topup") {
            $getbankid = $this->db->query("SELECT * FROM tbl_rekening WHERE nomer_rekening='$where[frombank]' AND id_user='$where[id_user]'")->row_array();
			$frombankk = $getbankid['bank_id'];
        }
        else if ($type_confirm=="on_demand" || $type_confirm=="multicast") {		   
			$frombankk = $where['frombank'];
        }

		$processadd = $this->db->query("INSERT INTO log_trf_confirmation (trfc_id, trx_id, id_user, bank_id, from_bank, jumlah, norek_tr, nama_tr, metod_tr, bukti_tr, accepted, type_confirm) VALUES ('','$where[order_id]','$where[id_user]','$where[bank_id]','$frombankk','$where[jumlah]','$where[frombank]','$where[nama_tr]','$where[metod_tr]','$where[bukti_tr]','0','$type_confirm'
		)");
 
		if ($processadd) {
			$this->db->query("UPDATE tbl_order SET order_status=2 WHERE order_id='$where[order_id]'");
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function edttr($table, $where)
	{
		$getbankid = $this->db->query("SELECT * FROM tbl_rekening WHERE nomer_rekening='$where[norekeningbank]' AND id_user='$where[id_user]'")->row_array();
		$frombankk = $getbankid['bank_id'];
		$edtconfirm = $this->db->query("UPDATE log_trf_confirmation SET bank_id='$where[bank_id]', from_bank='$frombankk', jumlah='$where[jumlah]', norek_tr='$where[norekeningbank]', nama_tr='$where[nama_tr]', metod_tr='$where[metod_tr]',bukti_tr='$where[bukti_tr]' WHERE trx_id='$where[order_id]'");

		if ($edtconfirm) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function bankadd($table, $where)
	{
		$add = $this->db->query("INSERT INTO tbl_rekening (id_user,nama_akun,nomer_rekening,bank_id,nama_bank,cabang) VALUES ('$where[iduser]','$where[namaakun]','$where[norekening]','$where[bank_id]','$where[namabank]','$where[cabangbank]')");

		if ($add) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function bankedit($table, $where)
	{
		$getbankname = $this->db->query("SELECT bank_name FROM master_bank WHERE bank_id='$where[bank_id]'")->row_array();
		$edit = $this->db->query("UPDATE tbl_rekening SET nama_akun='$where[nama_akun]',nomer_rekening='$where[nomer_rekening]',bank_id='$where[bank_id]',nama_bank='$getbankname[bank_name]',cabang='$where[cabang]' WHERE id_rekening='$where[id_rekening]'");

		if ($edit) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function bankdelete($table, $where)
	{
		$delete = $this->db->query("DELETE FROM tbl_rekening WHERE id_rekening='$where[id_rekening]'");
		
		if ($delete) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function support($table, $where)
	{
		$ceksupport = $this->db->query("SELECT * FROM tbl_support WHERE id_tutor='$where[id_tutor]' AND status='0'")->row_array();
		if ($ceksupport) {
			/*$addnotif = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('Request kamu sudah masuk ke data kami. Harap tunggu hingga support kami menghubungi anda. Terima kasih','single','$where[id_tutor]','','0')");*/
			$return = 2;
		}
		else
		{
			$namatutor = $this->session->userdata('user_name');
			$addsupport = $this->db->query("INSERT INTO tbl_support (id_tutor, type, status) VALUES ('$where[id_tutor]','$where[type]','$where[status]')");			

			//NOTIF
			/*$addnotif = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('Silahkan tunggu hingga Support kami menghubungi anda. Terima kasih','single','$where[id_tutor]','','0')");*/
			if ($addsupport) {
				$notif_message = json_encode( array("code" => 40, "tutor_name" => $namatutor) );
				$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','5','https://indiclass.id/admin/support','0')");
				$return = 1;
			}
			else
			{
				$return = 0;
			}
		}
		return $return;
	}

	function getnorek() {
        $data = array();
        
        $query = $this->db->get("log_trf_confirmation WHERE id_user ='$id_user'");
        if ($query->num_rows() > 0) {
            return $result->$result_array();
        }
        else{
        	return array();
        }
    }

    function getnorekn() {
        $data = array();
        
        $query = $this->db->get("log_trf_confirmation WHERE id_user ='$id_user'");
        if ($query->num_rows() > 0) {
            return $result->$result_array();
        }
        else{
        	return array();
        }
    }

    function getid() {
        $data = array();
        
        $query = $this->db->get("log_trf_confirmation WHERE id_user ='$id_user'");
        if ($query->num_rows() > 0) {
            return $result->$result_array();
        }
        else{
        	return array();
        }
    }

    function getidn() {
        $data = array();
        
        $query = $this->db->get("log_trf_confirmation WHERE id_user ='$id_user'");
        if ($query->num_rows() > 0) {
            return $result->$result_array();
        }
        else{
        	return array();
        }
    }

    public function announ_add($table, $where)
	{
		$add = $this->db->query("INSERT INTO tbl_announcement (text_announ) VALUES ('$where[text_announ]')");

		if ($add) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function announ_edit($table, $where)
	{		
		$edit = $this->db->query("UPDATE tbl_announcement SET text_announ='$where[text_announ]' WHERE id='$where[id_announ]'");

		if ($edit) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

   	public function announ_delete($table, $where)
	{
		$delete = $this->db->query("DELETE FROM tbl_announcement WHERE id='$where[id]'");
		
		if ($delete) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}
	public function add_channel($data='',$id_user='')
	{
		$channel_name = $data['channel_name'];
		$channel_country_code = $data['channel_country_code'];
		$channel_callnum = $data['channel_callnum'];
		$channel_email = $data['channel_email'];
		$channel_address = $data['channel_address'];

		$insert = $this->db->query("INSERT INTO master_channel (channel_name,channel_country_code,channel_callnum,channel_email,channel_address,channel_logo,channel_color,status) VALUES('$channel_name','$channel_country_code','$channel_callnum','$channel_email','$channel_address','empty.jpg','#2196f3','1')");
		if($insert){
			$id_channel = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
			$this->db->query("INSERT INTO uangpoint_channel (channel_id,active_point,freeze_point,status) VALUES ('$id_channel','0','0','1')");
			$exploded = explode(' ', $channel_name);
			$operator_name = "";
			foreach ($exploded as $str) {
				if(ctype_alnum(substr($str, 0,1))){
					$operator_name.= substr($str, 0,1);
				}
			}
			$operator_name.="_user";
			$operator_name = strtolower($operator_name);
			$rand_pass = $this->Rumus->RandomString(5);
			$hash = password_hash($rand_pass,PASSWORD_DEFAULT);
			$id_user = $this->Rumus->gen_id_user(sprintf('%03d',$id_channel));
			$x = 0;
			while(1){
				$x++;
				if($this->db->query("INSERT INTO tbl_user(id_user, username, user_name, first_name, last_name, password, user_gender, user_religion, status, usertype_id, user_image, st_tour) VALUES('$id_user','$operator_name','$channel_name User','$channel_name','User','$hash','Male','Islam',1,'user channel','$operator_name.jpg',0)")){
					// $res['mes_message'] = "1";
					$this->db->query("UPDATE master_channel SET channel_iduser = '$id_user' WHERE channel_id = '$id_channel'");
					file_put_contents("aset/img/user/$operator_name.jpg",file_get_contents("aset/img/user/empty.jpg"));

					// $return['channel_email'] = $channel_email;
					// $return['operator_name'] = $operator_name;
					// $return['rand_pass'] = $rand_pass;
					// $return['key'] = 0;
					// Open connection
			        $ch = curl_init();

			        // Set the url, number of POST vars, POST data
			        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
			        curl_setopt($ch, CURLOPT_POST, true);
			        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_addChannel','content' => array("channel_email" => "$channel_email", "channel_name" => "$channel_name", "operator_name" => "$operator_name", "rand_pass" => "$rand_pass") )));

			        // Execute post
			        $result = curl_exec($ch);
			        curl_close($ch);
			        // echo $result;


		            if ($result) {
	                  	$load = $this->lang->line('successfullyregistered');
						$array_balikann = array("message" => $load, "status" => true);
						$return = 1;
		            }else{
	                  	$load = $this->lang->line('errorregister');
						$array_balikannn = array("message" => $load, "status" => false);	
						echo json_encode($array_balikannn);
						$return = 0;    
		            }

					return $return;
				}else{
					$err = $this->db->error();
					if($err['code'] == 1062 && strpos($err['message'], "username") > 0){
						$operator_name.=$x;
					}else{
						print_r($err);
						return 0;
					}
				}
			}
			return $return;
		}
	}

	public function channeledit($table, $where)
	{		
		$edit = $this->db->query("UPDATE master_channel SET channel_name='$where[channel_name]',channel_country_code='$where[channel_country_code]',channel_callnum='$where[channel_callnum]',channel_email='$where[channel_email]',channel_address='$where[channel_address]' WHERE channel_id='$where[channel_id]'");
		if ($edit) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function add_tutorchannel($table,$where)
	{
		$add = $this->db->query("INSERT INTO master_channel_tutor (channel_id, tutor_id) VALUES ('$where[channel_id]','$where[tutor_id]')");

		if ($add) {
			$myip = $_SERVER['REMOTE_ADDR'];
			$plogc_id = $this->Rumus->getLogChnTrxID();
			$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$where[channel_id]','Tambah tutor channel','$where[tutor_id]','$myip',now())");
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function add_studentchannel($table,$where)
	{
		$add = $this->db->query("INSERT INTO master_channel_student (channel_id, id_user) VALUES ('$where[channel_id]','$where[id_user]')");

		if ($add) {
			$myip = $_SERVER['REMOTE_ADDR'];
			$plogc_id = $this->Rumus->getLogChnTrxID();
			$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$where[channel_id]','Delete student channel','$where[id_user]','$myip',now())");
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function studentchanneldelete($table, $where)
	{
		$delete = $this->db->query("DELETE FROM master_channel_student WHERE id_user='$where[id_user]' AND channel_id='$where[channel_id]'");
		
		if ($delete) {
			$myip = $_SERVER['REMOTE_ADDR'];
			$plogc_id = $this->Rumus->getLogChnTrxID();
			$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$where[channel_id]','Delete student channel','$where[id_user]','$myip',now())");
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function tutorchanneldelete($table, $where)
	{
		$delete = $this->db->query("DELETE FROM master_channel_tutor WHERE tutor_id='$where[tutor_id]'");
		$channel_id = $this->session->userdata('channel_id');
		if ($delete) {
			$myip = $_SERVER['REMOTE_ADDR'];
			$plogc_id = $this->Rumus->getLogChnTrxID();
			$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Delete tutor channel','$where[tutor_id]','$myip',now())");
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function channeldelete($table, $where)
	{
		$delete = $this->db->query("DELETE FROM master_channel WHERE channel_id='$where[channel_id]'");
		
		if ($delete) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function addsubject($table, $where)
	{
		// $cekdata = $this->db->query("SELECT * FROM `master_subject` WHERE subject_name='$where[subject_name]' && jenjang_id='$where[jenjang_id]'")->row_array();
		$cekdata = $this->db->query("SELECT * FROM `master_subject` WHERE subject_name='$where[subject_name]'")->row_array();
		if (!empty($cekdata)) {
			$return = 3;
		}
		else
		{
			$processadd = $this->db->query("INSERT INTO master_subject (jenjang_id, subject_name, indonesia, english, icon) VALUES ('$where[jenjang_id]','$where[subject_name]','$where[subject_name_indonesia]','$where[subject_name_inggris]','$where[icon]')");
	 		
			if ($processadd) {
				$return = 1;
			}
			else
			{
				$return = 0;
			}
		}
		return $return;
	}

	public function subjectdelete($table, $where)
	{
		$delete = $this->db->query("DELETE FROM master_subject WHERE subject_id='$where[subject_id]'");
		
		if ($delete) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function edtsubject($table, $where)
	{
		$edtsubject = $this->db->query("UPDATE master_subject SET subject_name='$where[subject_name]', jenjang_id='$where[jenjang_id]', indonesia='$where[subject_name_indonesia]', english='$where[subject_name_inggris]', icon='$where[fotoicon]' WHERE subject_id='$where[subject_id]'");
		
		if ($edtsubject) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function addreferral($where)
	{
		$addreferral = $this->db->query("INSERT INTO master_referral (referral_code,start_datetime,end_datetime,description,type,created_at) VALUES ('$where[referral_code]','$where[start_datetime]','$where[end_datetime]','$where[description]','$where[type]','')");
		
		if ($addreferral) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function edtreferral($where)
	{
		$edtreferral = $this->db->query("UPDATE master_referral SET referral_code='$where[referral_code]', start_datetime='$where[start_datetime]', end_datetime='$where[end_datetime]', description='$where[description]', type='$where[type]' WHERE referral_id='$where[referral_id]'");

		if ($edtreferral) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function referraldelete($where)
	{
		$delete = $this->db->query("DELETE FROM master_referral WHERE referral_id='$where[referral_id]'");
		if ($delete) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	public function reject_request($request_id='',$type='', $status='')
	{
		// unapproved , unavailable
		if ($type == "private") {
			$getdatareq 	= $this->db->query("SELECT * FROM tbl_request WHERE request_id='$request_id'")->row_array();

			$tutor_id		= $getdatareq['tutor_id'];
			$subject_id		= $getdatareq['subject_id'];
			$user_utc 		= $getdatareq['user_utc'];
			$topic 			= $getdatareq['topic'];
			$getdataclass 	= $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();

			$iduserrequest		= $getdataclass['id_user_requester'];
			// $datauser 			= $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
			$usertype_id 		= $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
            if ($usertype_id == 'student kid') {
                $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                $forFcm_id_requester= $getidparent;
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
            }
            else
            {
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                $forFcm_id_requester= $iduserrequest;
            }
			$namauser			= $datauser['user_name'];
			$emailuser			= $datauser['email'];
			$duration 			= $getdataclass['duration_requested'];
			$hargahitung		= ($duration/900)*$getdataclass['harga_cm'];
			$hargakelas         = number_format($hargahitung, 0, ".", ".");
	        $server_utcc        = $this->Rumus->getGMTOffset();
	        $intervall          = $user_utc - $server_utcc;
	        $tanggal            = DateTime::createFromFormat('Y-m-d H:i:s',$getdataclass['date_requested']);
	        $tanggal->modify("+".$intervall ." minutes");
	        $tanggal            = $tanggal->format('Y-m-d H:i:s');
	        $datelimit          = date_create($tanggal);
	        $dateelimit         = date_format($datelimit, 'd/m/y');
	        $harilimit          = date_format($datelimit, 'd');
	        $hari               = date_format($datelimit, 'l');
	        $tahunlimit         = date_format($datelimit, 'Y');
	        $waktulimit         = date_format($datelimit, 'H:i');   
	        $datelimit          = $dateelimit;
	        $sepparatorlimit    = '/';
	        $partslimit         = explode($sepparatorlimit, $datelimit);
	        $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
	        $seconds 			= $getdataclass['duration_requested'];
		    $hours 				= floor($seconds / 3600);
		    $mins 				= floor($seconds / 60 % 60);
		    $secs 				= floor($seconds % 60);
		    $durationrequested 	= sprintf('%02d:%02d', $hours, $mins);		 
			
			if ($status == "unapprove") {
				$sub 			= "Permintaan Kelas Gagal";
				$alert_text 	= "Maaf, permintaan kelas Anda secara otomatis dibatalkan karena hingga waktu yang ditentukan sistem kami belum menerima pembayaran yang ditentukan. Apabila Anda sudah melakukan pembayaran sebelum ini, mohon hubungi customer service atau email ke finance@meetaza.com dengan menyertakan nama lengkap, email/username dan bukti transfer.<br>Berikut adalah detail permintaan kelas Anda:";	
			}
			else
			{
				$sub 			= "Permintaan Kelas Gagal";
				$alert_text	= "Maaf, permintaan kelas Anda gagal karena kami tidak mendapatkan respon dari  tutor yang bersangkutan. <br> Berikut adalah detail permintaan kelas Anda:";
			}

			// Open connection
	        $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_rejectRequest_Private','content' => array("emailuser" => $emailuser, "sub" => $sub, "namauser" => $namauser, "alert_text" => $alert_text, "getdataclass" => $getdataclass, "topic" => $topic, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "durationrequested" => $durationrequested ) )));

	        // Execute post
	        $result = curl_exec($ch);
	        curl_close($ch);
	        // echo $result;


            if ($result) {
                  return 1;
            }else{
                  return 0;   
            }
		}
		else if($type == "group")
		{
			$getdatareqgrup 		= $this->db->query("SELECT * FROM tbl_request_grup WHERE request_id='$request_id'")->row_array();

			if (empty($getdatareqgrup)) {
				return 0;
			}
			else
			{
				$tutor_id			= $getdatareqgrup['tutor_id'];
				$user_utc 			= $getdatareqgrup['user_utc'];
				$iduserrequest		= $getdatareqgrup['id_user_requester'];
				$q                  = $this->db->query("SELECT * FROM tbl_request_grup as tr INNER JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id WHERE tsp.class_type='$type' AND tsp.tutor_id='$tutor_id' AND tr.request_id='$request_id'")->row_array();

	            $subject_id         = $q['subject_id'];	            
	            $subject_name       = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
	            $topic              = $q['topic'];
	            $tutorname          = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
	            $hargakelas         = number_format($q['harga_cm'], 0, ".", ".");            
	            $list_student       = json_decode($q['id_friends'],true);	
	            $duration           = $q['duration_requested'];
            	$start_st           = $q['date_requested'];

	            // $getdatauser        = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
	            $usertype_id 		= $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
	            if ($usertype_id == 'student kid') {
	                $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
	                $forFcm_id_requester= $getidparent;
	                $getdatauser    = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
	            }
	            else
	            {
	                $getdatauser    = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
	                $forFcm_id_requester= $iduserrequest;
	            }
	            $emailuser          = $getdatauser['email'];
	            $usernameuser       = $getdatauser['user_name'];

	            $server_utcc        = $this->Rumus->getGMTOffset();
	            $intervall          = $user_utc - $server_utcc;
	            $starttime          = DateTime::createFromFormat ('Y-m-d H:i:s',$start_st);
	            $starttime->modify("+".$intervall ." minutes");
	            $starttime          = $starttime->format('Y-m-d H:i:s');

	            $datelimit          = date_create($starttime);
	            $dateelimit         = date_format($datelimit, 'd/m/y');
	            $harilimit          = date_format($datelimit, 'd');
	            $hari               = date_format($datelimit, 'l');
	            $tahunlimit         = date_format($datelimit, 'Y');
	            $waktulimit         = date_format($datelimit, 'H:i');   
	            $datelimit          = $dateelimit;
	            $sepparatorlimit    = '/';
	            $partslimit         = explode($sepparatorlimit, $datelimit);
	            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

	            $seconds            = $duration;
	            $hours              = floor($seconds / 3600);
	            $mins               = floor($seconds / 60 % 60);
	            $secs               = floor($seconds % 60);
	            $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

				if ($status == "unapprove") {
					$sub 			= "Permintaan Kelas Gagal";
					$alert_text 	= "Maaf, permintaan kelas Anda secara otomatis dibatalkan karena hingga waktu yang ditentukan sistem kami belum menerima pembayaran yang ditentukan. Apabila Anda sudah melakukan pembayaran sebelum ini, mohon hubungi customer service atau email ke finance@meetaza.com dengan menyertakan nama lengkap, email/username dan bukti transfer.<br>Berikut adalah detail permintaan kelas Anda:";	
				}
				else
				{
					$sub 			= "Permintaan Kelas Gagal";
					$alert_text	= "Maaf, permintaan kelas Anda gagal karena kami tidak mendapatkan respon dari  tutor yang bersangkutan.<br> Berikut adalah detail permintaan kelas Anda:";
				}

	            $config1 = Array(
	                'protocol' => 'smtp',
	                'smtp_host' => 'ssl://smtp.googlemail.com',
	                'smtp_port' => 465,
	                'smtp_user' =>'info@classmiles.com',
	                'smtp_pass' => 'kerjadiCLASSMILES', 
	                'mailtype' => 'html',   
	                'charset' => 'iso-8859-1'
	            );

	            $this->load->library('email', $config1);
	            $this->email->initialize($config1);
	            $this->email->set_mailtype("html");
	            $this->email->set_newline("\r\n");
	            $this->email->from('info@classmiles.com', 'Classmiles');
	            $this->email->to($emailuser);
	            $this->email->subject('Classmiles - '.$sub);
	            $templateToStudent = 
	                "
	                <div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
	                    <table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
	                        <tbody>
	                              <tr>        
	                                  <td style='padding-left:10px;padding-right:10px'>
	                                      <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:700px' width='100%'>
	                                          <tbody>
	                                            <tr>
	                                                <td style='text-align:center;padding-top:3%'>
	                                                    <a href='https://indiclass.id/' style='text-decoration:none' target='_blank'>
	                                                        <img src='".CDN_URL.STATIC_IMAGE_CDN_URL."maillogo_classmiles.png' style='display:block;margin:0;border:0;max-width:700px' width='100%'>
	                                                    </a>
	                                                </td>
	                                            </tr>

	                                            <tr>
	                                                <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
	                                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
	                                                        <tbody>
	                                                            <tr>
	                                                                <td style='padding-right:5%' width='72%'>
	                                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
	                                                                        Dear ".$usernameuser.",
	                                                                    </h1>
	                                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
	                                                                        ".$alert_text."    
	                                                                    </p>                  
	                                                                    <div style='border-bottom:2px solid #eeeeee;'>&nbsp;</div>
	                                                                    <br>                         
	                                                                    <p>                                                      
	                                                                        <table style='width:100%; border: 1px solid gray;
	                                                                            border-collapse: collapse; color: #6C7A89;'>
	                                                                            <tr>
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>TUTOR</th>      
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>MATA PELAJARAN</th>
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>TOPIK</th>
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>WAKTU</th> 
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>DURASI</th>         
	                                                                            </tr> 
	                                                                            <tr>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$tutorname."</td>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$subject_name."</td>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$topic."</td>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", ".$waktulimit."</td>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td>         
	                                                                            </tr>                                                                                   
	                                                                        </table>
	                                                                    </p>                                                    
	                                                                </td>
	                                                            </tr>                                                          
	                                                            <tr>
	                                                                <td style='background-color:#ffffff'>
	                                                                    <tbody>                                                                          
	                                                                        <tr>
	                                                                            <td style='padding-bottom:20px;line-height:20px'>
	                                                                                <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
	                                                                            </td>
	                                                                        </tr>
	                                                                        <tr>
	                                                                            <td align='center' style='padding-right:5%' width='100%'>
	                                                                                  <p style='color:gray;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
	                                                                                  Diharapkan jangan membalas email ini.<br>
	                                                                                  Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
	                                                                                  <br>Terima kasih,                                           
	                                                                                  <br>
	                                                                                    Tim Classmiles
	                                                                                  </p>                                                        
	                                                                            </td>                                                   
	                                                                        </tr>
	                                                                    </tbody>                                               
	                                                                </td>
	                                                            </tr>
	                                                        </tbody>
	                                                    </table>
	                                                </td>
	                                            </tr>
	                                            
	                                            <tr>
	                                                <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
	                                                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
	                                                        
	                                                    </tbody></table>
	                                                    <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
	                                                        
	                                                    </tbody></table>
	                                                </td>
	                                            </tr>
	                                        </tbody>
	                                      </table>
	                                  </td>
	                              </tr>
	                        </tbody>
	                    </table>
	                </div>
	                ";
	            /**/                
	            $this->email->message($templateToStudent);          

	            if ($this->email->send()) 
	            {
	            	return 1;	                  
	            }
	            else
	            {
	            	return 0;
	            }

            }
		}
		else if($type == "multicast")
		{
			$getkeranjang 		= $this->db->query("SELECT * FROM tbl_keranjang")->result_array();
		    foreach ($getkeranjang as $row => $v) {		    			    
			    $getreqid     	= $v['request_id'] != null ? $v['request_id'] : ( $v['request_grup_id'] != null ? $v['request_grup_id'] : $v['request_multicast_id'] );
			    if ($getreqid == $request_id) {
			    	$krnj_id = $v['krnj_id'];			    	
			    }
			}			
			$this->db->simple_query("DELETE FROM tbl_keranjang WHERE krnj_id='$krnj_id'");	
		}
		else if($type == "program")
		{
			$getkeranjang 		= $this->db->query("SELECT * FROM tbl_keranjang")->result_array();
		    foreach ($getkeranjang as $row => $v) {		    			    
			    $getreqid     	= $v['request_id'] != null ? $v['request_id'] : ( $v['request_grup_id'] != null ? $v['request_grup_id'] : ($v['request_multicast_id'] != null ? $v['request_multicast_id'] : $v['request_program_id']));
			    if ($getreqid == $request_id) {
			    	$krnj_id = $v['krnj_id'];			    	
			    }
			}			
			if ($krnj_id != "") {				
				$this->db->simple_query("DELETE FROM tbl_keranjang WHERE krnj_id='$krnj_id'");
			}
		}
		else
		{
			return 0;
		}
		
	}

	public function inbound_transfer($date_field='',$description_field='',$credit_amount='',$bank_rekening='',$bank_an='')
    {
        $credit_amount_last = number_format($credit_amount, 0, ".", ".");
        $transaction        = date_create($date_field);
        $date               = date_format($transaction, 'd/m/y');
        $harii              = date_format($transaction, 'd');
        $hari               = date_format($transaction, 'l');
        $tahun              = date_format($transaction, 'Y');
        $waktu              = date_format($transaction, 'H:i');
                                                
        $dateakhir          = $date;
        $sepparator         = '/';
        $parts              = explode($sepparator, $dateakhir);
        $bulan              = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
        curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_inboundTransfer','content' => array("bank_rekening" => "$bank_rekening", "credit_amount_last" => "$credit_amount_last", "bank_an" => "$bank_an", "hari" => "$hari", "harii" => "$harii", "bulan" => "$bulan", "tahun" => "$tahun", "waktu" => "$waktu") )));

        // Execute post
        $result = curl_exec($ch);
        curl_close($ch);
        // echo $result;


        if ($result) {
            return 1;
        }
        else
        {
            return 0;
        }
        
    }

	public function reject_request_unpaid($request_id='',$type='')
	{
		if ($type == "private") {
			$getdatareq 	= $this->db->query("SELECT * FROM tbl_request WHERE request_id='$request_id'")->row_array();

			$tutor_id		= $getdatareq['tutor_id'];
			$subject_id		= $getdatareq['subject_id'];
			$user_utc 		= $getdatareq['user_utc'];
			$topic 			= $getdatareq['topic'];
			$getdataclass 	= $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();

			$iduserrequest		= $getdataclass['id_user_requester'];
			// $datauser 			= $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
			$usertype_id 		= $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
            if ($usertype_id == 'student kid') {
                $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                $forFcm_id_requester= $getidparent;
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
            }
            else
            {
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                $forFcm_id_requester= $iduserrequest;
            }
			$namauser			= $datauser['user_name'];
			$emailuser			= $datauser['email'];
			$duration 			= $getdataclass['duration_requested'];
			$hargahitung		= ($duration/900)*$getdataclass['harga_cm'];
			$hargakelas         = number_format($hargahitung, 0, ".", ".");
	        $server_utcc        = $this->Rumus->getGMTOffset();
	        $intervall          = $user_utc - $server_utcc;
	        $tanggal            = DateTime::createFromFormat('Y-m-d H:i:s',$getdataclass['date_requested']);
	        $tanggal->modify("+".$intervall ." minutes");
	        $tanggal            = $tanggal->format('Y-m-d H:i:s');
	        $datelimit          = date_create($tanggal);
	        $dateelimit         = date_format($datelimit, 'd/m/y');
	        $harilimit          = date_format($datelimit, 'd');
	        $hari               = date_format($datelimit, 'l');
	        $tahunlimit         = date_format($datelimit, 'Y');
	        $waktulimit         = date_format($datelimit, 'H:i');   
	        $datelimit          = $dateelimit;
	        $sepparatorlimit    = '/';
	        $partslimit         = explode($sepparatorlimit, $datelimit);
	        $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
	        $seconds 			= $getdataclass['duration_requested'];
		    $hours 				= floor($seconds / 3600);
		    $mins 				= floor($seconds / 60 % 60);
		    $secs 				= floor($seconds % 60);
		    $durationrequested 	= sprintf('%02d:%02d', $hours, $mins);
		    
			// Open connection
	        $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_rejectRequest_unpaid_private','content' => array("emailuser" => $emailuser, "namauser" => $namauser, "getdataclass" => $getdataclass, "topic" => $topic, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "durationrequested" => $durationrequested) )));

	        // Execute post
	        $result = curl_exec($ch);
	        curl_close($ch);
	        // echo $result;


            if ($result) 
            {
				return 1;
			}
			else
			{
				return 0;
			}
	            
		}
		else if($type == "group")
		{
			$getdatareqgrup 		= $this->db->query("SELECT * FROM tbl_request_grup WHERE request_id='$request_id'")->row_array();

			if (empty($getdatareqgrup)) {
				return 0;
			}
			else
			{
				$tutor_id			= $getdatareqgrup['tutor_id'];
				$user_utc 			= $getdatareqgrup['user_utc'];
				$iduserrequest		= $getdatareqgrup['id_user_requester'];
				$q                  = $this->db->query("SELECT * FROM tbl_request_grup as tr INNER JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id WHERE tsp.class_type='$type' AND tsp.tutor_id='$tutor_id' AND tr.request_id='$request_id'")->row_array();

	            $subject_id         = $q['subject_id'];
	            $topic 				= $q['topic'];
	            $subject_name       = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
	            $topic              = $q['topic'];
	            $tutorname          = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
	            $hargakelas         = number_format($q['harga_cm'], 0, ".", ".");            
	            $list_student       = json_decode($q['id_friends'],true);	
	            $duration           = $q['duration_requested'];
            	$start_st           = $q['date_requested'];

	            // $getdatauser        = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
	            $usertype_id 		= $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
                if ($usertype_id == 'student kid') {
                    $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                    $forFcm_id_requester= $getidparent;
                    $getdatauser    = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
                }
                else
                {
                    $getdatauser    = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                    $forFcm_id_requester= $iduserrequest;
                }
	            $emailuser          = $getdatauser['email'];
	            $usernameuser       = $getdatauser['user_name'];

	            $server_utcc        = $this->Rumus->getGMTOffset();
	            $intervall          = $user_utc - $server_utcc;
	            $starttime          = DateTime::createFromFormat ('Y-m-d H:i:s',$start_st);
	            $starttime->modify("+".$intervall ." minutes");
	            $starttime          = $starttime->format('Y-m-d H:i:s');

	            $datelimit          = date_create($starttime);
	            $dateelimit         = date_format($datelimit, 'd/m/y');
	            $harilimit          = date_format($datelimit, 'd');
	            $hari               = date_format($datelimit, 'l');
	            $tahunlimit         = date_format($datelimit, 'Y');
	            $waktulimit         = date_format($datelimit, 'H:i');   
	            $datelimit          = $dateelimit;
	            $sepparatorlimit    = '/';
	            $partslimit         = explode($sepparatorlimit, $datelimit);
	            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

	            $seconds            = $duration;
	            $hours              = floor($seconds / 3600);
	            $mins               = floor($seconds / 60 % 60);
	            $secs               = floor($seconds % 60);
	            $durationrequested  = sprintf('%02d:%02d', $hours, $mins);
	            
				// Open connection
		        $ch = curl_init();

		        // Set the url, number of POST vars, POST data
		        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
		        curl_setopt($ch, CURLOPT_POST, true);
		        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_rejectRequest_unpaid_group','content' => array("emailuser" => $emailuser, "namauser" => $namauser, "tutorname" => $tutorname, "subject_name" => $subject_name, "topic" => $topic, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "durationrequested" => $durationrequested) )));

		        // Execute post
		        $result = curl_exec($ch);
		        curl_close($ch);
		        // echo $result;

	            $config1 = Array(
	                'protocol' => 'smtp',
	                'smtp_host' => 'ssl://smtp.googlemail.com',
	                'smtp_port' => 465,
	                'smtp_user' =>'info@classmiles.com',
	                'smtp_pass' => 'kerjadiCLASSMILES', 
	                'mailtype' => 'html',   
	                'charset' => 'iso-8859-1'
	            );

	            $this->load->library('email', $config1);
	            $this->email->initialize($config1);
	            $this->email->set_mailtype("html");
	            $this->email->set_newline("\r\n");
	            $this->email->from('info@classmiles.com', 'Classmiles');
	            $this->email->to($emailuser);
	            $this->email->subject('Classmiles - Group class expired');
	            $templateToStudent = 
	                "
	                <div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
	                    <table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
	                        <tbody>
	                              <tr>        
	                                  <td style='padding-left:10px;padding-right:10px'>
	                                      <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:700px' width='100%'>
	                                          <tbody>
	                                            <tr>
	                                                <td style='text-align:center;padding-top:3%'>
	                                                    <a href='https://indiclass.id/' style='text-decoration:none' target='_blank'>
	                                                        <img src='".CDN_URL.STATIC_IMAGE_CDN_URL."maillogo_classmiles.png' style='display:block;margin:0;border:0;max-width:700px' width='100%'>
	                                                    </a>
	                                                </td>
	                                            </tr>

	                                            <tr>
	                                                <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
	                                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
	                                                        <tbody>
	                                                            <tr>
	                                                                <td style='padding-right:5%' width='72%'>
	                                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
	                                                                        Dear ".$usernameuser.",
	                                                                    </h1>
	                                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
	                                                                        Maaf, permintaan kelas kamu sudah melawati batas akhir pembayaran 1 jam dari jam permintaan.<br>
	                                                                        Berikut adalah detail kelas :    
	                                                                    </p>                  
	                                                                    <div style='border-bottom:2px solid #eeeeee;'>&nbsp;</div>
	                                                                    <br>                         
	                                                                    <p>                                                      
	                                                                        <table style='width:100%; border: 1px solid gray;
	                                                                            border-collapse: collapse; color: #6C7A89;'>
	                                                                            <tr>
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>TUTOR</th>      
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>MATA PELAJARAN</th>
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>TOPIK</th>
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>START CLASS</th> 
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>DURASI KELAS</th>                                                                                        
	                                                                            </tr> 
	                                                                            <tr>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$tutorname."</td>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$subject_name."</td>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$topic."</td>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit."</td>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td>                                                                                        
	                                                                            </tr>                                                                                   
	                                                                        </table>
	                                                                    </p>                                                    
	                                                                </td>
	                                                            </tr>                                                          
	                                                            <tr>
	                                                                <td style='background-color:#ffffff'>
	                                                                    <tbody>                                                                          
	                                                                        <tr>
	                                                                            <td style='padding-bottom:20px;line-height:20px'>
	                                                                                <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
	                                                                            </td>
	                                                                        </tr>
	                                                                        <tr>
	                                                                            <td align='center' style='padding-right:5%' width='100%'>
	                                                                                  <p style='color:gray;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
	                                                                                  Diharapkan jangan membalas email ini.<br>
	                                                                                  Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
	                                                                                  <br>Terima kasih,                                           
	                                                                                  <br>
	                                                                                    Tim Classmiles
	                                                                                  </p>                                                        
	                                                                            </td>                                                   
	                                                                        </tr>
	                                                                    </tbody>                                               
	                                                                </td>
	                                                            </tr>
	                                                        </tbody>
	                                                    </table>
	                                                </td>
	                                            </tr>
	                                            
	                                            <tr>
	                                                <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
	                                                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
	                                                        
	                                                    </tbody></table>
	                                                    <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
	                                                        
	                                                    </tbody></table>
	                                                </td>
	                                            </tr>
	                                        </tbody>
	                                      </table>
	                                  </td>
	                              </tr>
	                        </tbody>
	                    </table>

	                </div>
	                ";
	            /**/                
	            $this->email->message($templateToStudent);          

	            if ($this->email->send()) 
	            {
	            	
	            }
	            else
	            {
	            	return 0;
	            }

            }
		}
		else if($type == "multicast")
		{
		    $getkeranjang 		= $this->db->query("SELECT * FROM tbl_keranjang")->result_array();
		    foreach ($getkeranjang as $row => $v) {		    			    
			    $getreqid     	= $v['request_id'] != null ? $v['request_id'] : ( $v['request_grup_id'] != null ? $v['request_grup_id'] : $v['request_multicast_id'] );
			    if ($getreqid == $request_id) {
			    	$krnj_id = $v['krnj_id'];			    	
			    }
			}			
			if ($krnj_id != "") {				
				$this->db->simple_query("DELETE FROM tbl_keranjang WHERE krnj_id='$krnj_id'");
			}
		}
		else if($type == "program")
		{
			$getkeranjang 		= $this->db->query("SELECT * FROM tbl_keranjang")->result_array();
		    foreach ($getkeranjang as $row => $v) {		    			    
			    $getreqid     	= $v['request_id'] != null ? $v['request_id'] : ( $v['request_grup_id'] != null ? $v['request_grup_id'] : ($v['request_multicast_id'] != null ? $v['request_multicast_id'] : $v['request_program_id']));
			    if ($getreqid == $request_id) {
			    	$krnj_id = $v['krnj_id'];			    	
			    }
			}			
			if ($krnj_id != "") {				
				$this->db->simple_query("DELETE FROM tbl_keranjang WHERE krnj_id='$krnj_id'");
			}
			
			$getdatapro 		= $this->db->query("SELECT trp.create_at as date_request trp.*, tpp.* FROM tbl_request_program as trp INNER JOIN tbl_price_program as tpp ON trp.program_id=tpp.list_id WHERE trp.request_id='$request_id'")->row_array();

			$program_id			= $getdatapro['program_id'];
			$iduserrequest		= $getdatapro['id_requester'];
			$harga_program 		= $getdatapro['price'];
			$user_utc 			= $getdatapro['user_utc'];
			$description 		= $getdatapro['description'];
			$jumlahkarakter		= 10;
			$cetak = substr($description,$jumlahkarakter,1);
			if($cetak !=" "){
				while($cetak !=" "){
					$i=1;
					$jumlahkarakter=$jumlahkarakter+$i;
					$description=$getdatapro['description'];
					$cetak = substr($description,$jumlahkarakter,1);
				}
			}
			$topic 				= substr($description, 0, $jumlahkarakter);
			
			// $datauser 			= $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
			$usertype_id 		= $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
            if ($usertype_id == 'student kid') {
                $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                $forFcm_id_requester= $getidparent;
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
            }
            else
            {
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                $forFcm_id_requester= $iduserrequest;
            }
			$namauser			= $datauser['user_name'];
			$emailuser			= $datauser['email'];

			$hargakelas         = number_format($harga_program, 0, ".", ".");
	        $server_utcc        = $this->Rumus->getGMTOffset();
	        $intervall          = $user_utc - $server_utcc;
	        $tanggal            = DateTime::createFromFormat('Y-m-d H:i:s',$getdatapro['date_request']);
	        $tanggal->modify("+".$intervall ." minutes");
	        $tanggal            = $tanggal->format('Y-m-d H:i:s');
	        $datelimit          = date_create($tanggal);
	        $dateelimit         = date_format($datelimit, 'd/m/y');
	        $harilimit          = date_format($datelimit, 'd');
	        $hari               = date_format($datelimit, 'l');
	        $tahunlimit         = date_format($datelimit, 'Y');
	        $waktulimit         = date_format($datelimit, 'H:s');  
	        $datelimit          = $dateelimit;
	        $sepparatorlimit    = '/';
	        $partslimit         = explode($sepparatorlimit, $datelimit);
	        $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

			$config2 = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'finance@meetaza.com',
				'smtp_pass' => 'xmbmpuwuxlzmcjsl',	
				'mailtype' => 'html',					
				'charset' => 'iso-8859-1'
			);

			$this->load->library('email', $config2);
			$this->email->initialize($config2);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('finance@meetaza.com', 'Classmiles');
			$this->email->to($emailuser);
			$this->email->subject('Classmiles - Program Package expired');
			$templateToTutor = 
			"
			<div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
	            <table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
                    <tbody>
                      	<tr>        
                          	<td style='padding-left:10px;padding-right:10px'>
                              	<table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:700px' width='100%'>
                                  	<tbody>
	                                    <tr>
	                                        <td style='text-align:center;padding-top:3%'>
	                                            <a href='https://indiclass.id/' style='text-decoration:none' target='_blank'>
	                                                <img src='".CDN_URL.STATIC_IMAGE_CDN_URL."maillogo_classmiles.png' style='display:block;margin:0;border:0;max-width:700px' width='100%'>
	                                            </a>
	                                        </td>
	                                    </tr>

	                                    <tr>
	                                        <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
	                                            <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
	                                                <tbody>
	                                                    <tr>
	                                                          <td style='padding-right:5%' width='72%'>
	                                                                <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
	                                                                    Dear ".$namauser.",
	                                                                </h1>
	                                                                <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
	                                                                    Maaf, permintaan kelas Anda sudah melawati batas akhir pembayaran 1 jam dari jam permintaan.<br>
	                                                                    Berikut adalah detail Kelas Anda:   
	                                                                </p>     
	                                                                <center>
	                                                                    <div style='background: #edecec; width: 50%; height: 70px; margin-top: 20px; border-radius: 5px;'>
	                                                                        <label style='color: #6C7A89; margin-top: 20px; top:20px; font-size: 24px;'><br>Rp. ".$hargakelas."</label>
	                                                                    </div> 
	                                                                </center>              
	                                                                <div style='border-bottom:2px solid #eeeeee;'>&nbsp;</div>
	                                                                <br><br>
	                                                                        <label style='color: #95A5A6;'>Berikut adalah detail permintaan kelas anda</label>
	                                                                        <br><br>
	                                                                        <table style='width:100%; border: 1px solid gray;
	                                                                            border-collapse: collapse; color: #6C7A89;'>
	                                                                            <tr>
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>TUTOR</th>                                                                                        
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>MATA PELAJARAN</th>
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>TOPIK</th>
	                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>WAKTU</th>                                                                                       
	                                                                            </tr> 
	                                                                            <tr>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$namauser."</td>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdatapro['name']."</td>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$topic."</td>
	                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", ".$waktulimit."</td>                                                                                       
	                                                                            </tr>                                                                                   
	                                                                        </table>
	                                                                    </center>
	                                                                </p>                                                    
	                                                          </td>
	                                                    </tr>                                                          
	                                                    <tr>
	                                                        <td style='background-color:#ffffff'>                                                                      
	                                                            <table border='0' cellpadding='0' cellspacing='0' style='width:100%'>
	                                                                <tbody>
	                                                                  <tr>
	                                                                      <td style='padding-right:5%; text-align:left;' width='72%'>
	                                                                            <p style='color:red;font-size:12px;font-weight:normal;line-height:20px;margin:0 top:50px;'>
	                                                                              <blockquote style='color: red; font-size: 12px;'>
	                                                                            * Permintaan anda otomatis batal jika tutor tidak menjawab dalam waktu 1 jam dari sekarang.<br>
	                                                                            * Jika permintaan anda telah di setujui oleh tutor. Email untuk pembayaran akan dikirimkan ke Anda.         
	                                                                              </blockquote>                                                                      
	                                                                            </p>                                                        
	                                                                      </td>                                                   
	                                                                  </tr>
	                                                                 
	                                                              </tbody>
	                                                            </table>                                                                                                                         
	                                                            <tbody>                                                                          
	                                                                <tr>
	                                                                    <td style='padding-bottom:20px;line-height:20px'>
	                                                                        <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
	                                                                    </td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <td align='center' style='padding-right:5%' width='100%'>
	                                                                          <p style='color:gray;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
	                                                                          Diharapkan jangan membalas email ini.<br>
	                                                                          Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
	                                                                          <br>Terima kasih,                                           
	                                                                          <br>
	                                                                            Tim Classmiles
	                                                                          </p>                                                        
	                                                                    </td>                                                   
	                                                                </tr>
	                                                            </tbody>                                               
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </td>
	                                    </tr>
	                                    
	                                    <tr>
	                                        <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
	                                            <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
	                                                
	                                            </tbody></table>
	                                            <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
	                                                
	                                            </tbody></table>
	                                        </td>
	                                    </tr>
                                	</tbody>
                              	</table>
                          	</td>
                      	</tr>
                	</tbody>
	            </table>
	        </div>
			";			
			
			$this->email->message($templateToTutor);
			if ($this->email->send()) 
			{
				return 1;
			}
			else
			{
				return 0;
			}				   
		}
		else
		{
			return 0;
		}	
	}

	public function payment_return($data='')
	{
		$email = "";
		$namauser = "";
		$trx_id = "";
		$kotaktd = null;
		$config2 = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' =>'finance@meetaza.com',
            'smtp_pass' => 'xmbmpuwuxlzmcjsl', 
            'mailtype' => 'html',                    
            'charset' => 'iso-8859-1'
        );
		foreach ($data as $key => $value) {
			$trx_id 	= $value['trx_id'];
			$type 		= $value['type'];		
			$id_user 	= $value['id_user'];
			$tutor_id 	= $value['tutor_id'];			
			$rtp 		= $value['rtp'];
			$getdata_profile 	= $this->db->query("SELECT * FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
			$data 		= $this->db->query("SELECT email,user_name FROM tbl_user WHERE id_user='$id_user'")->row_array();
			$email 		= $data['email'];
			$namauser 	= $data['user_name'];
			if ($type=='private') {
				$getdatareq 	= $this->db->query("SELECT ms.subject_name, tr.* FROM tbl_request as tr INNER JOIN master_subject as ms ON tr.subject_id=ms.subject_id WHERE tr.request_id='$rtp'")->row_array();
				$date_requested = $getdatareq['date_requested'];
				$duration 		= $getdatareq['duration_requested'];
			}
			else if ($type=='group') {
				$getdatareq 	= $this->db->query("SELECT ms.subject_name, tr.* FROM tbl_request_grup as tr INNER JOIN master_subject as ms ON tr.subject_id=ms.subject_id WHERE tr.request_id='$rtp'")->row_array();
				$date_requested = $getdatareq['date_requested'];
				$duration 		= $getdatareq['duration_requested'];
			}
			else if ($type=='multicast') {
				$getdatareq 	= $this->db->query("SELECT ms.subject_name, tc.description as topic, tr.*, tc.start_time, tc.finish_time FROM tbl_request_multicast as tr INNER JOIN tbl_class as tc ON tr.class_id=tc.class_id INNER JOIN master_subject as ms ON tc.subject_id=ms.subject_id WHERE tr.request_id='$rtp'")->row_array();
				$date_requested = $getdatareq['start_time'];
				$duration 		= strtotime($getdatareq['start_time']) - strtotime($getdatareq['finish_time']);
			}
			$user_utc 			= $this->session->userdata('user_utc');
			$server_utcc        = $this->Rumus->getGMTOffset();
            $intervall          = $user_utc - $server_utcc;
            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$date_requested);
            $tanggal->modify("+".$intervall ." minutes");
            $tanggal            = $tanggal->format('Y-m-d H:i:s');
            $datelimit          = date_create($tanggal);
            $dateelimit         = date_format($datelimit, 'd/m/y');
            $harilimit          = date_format($datelimit, 'd');
            $hari               = date_format($datelimit, 'l');
            $tahunlimit         = date_format($datelimit, 'Y');
            $waktulimit         = date_format($datelimit, 'H:i');
            $datelimit          = $dateelimit;
            $sepparatorlimit    = '/';
            $partslimit         = explode($sepparatorlimit, $datelimit);
            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

            $hours              = floor($duration / 3600);
            $mins               = floor($duration / 60 % 60);
            $secs               = floor($duration % 60);
            $durationrequested  = sprintf('%02d:%02d', $hours, $mins);
			$kotaktd = $kotaktd."<tr>
                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdata_profile['user_name']."</td>
                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdatareq['subject_name']."</td>
                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdatareq['topic']."</td>
                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit."</td>
                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td> 
            </tr>";            
		}
		$data_trx	= $this->db->query("SELECT * FROM tbl_order as tod INNER JOIN tbl_invoice as ti ON tod.order_id=ti.order_id WHERE tod.order_id='$trx_id'")->row_array();
		$invoice_id = $data_trx['invoice_id'];
		$harga 		= $data_trx['total_price'];
		$hargatotal = number_format($harga, 0, ".", ".");	

		// Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
        curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_paymentReturn','content' => array("email" => $email, "namauser" => $namauser, "hargatotal" => $hargatotal, "invoice_id" => $invoice_id, "kotaktd" => $kotaktd ) )));

        // Execute post
        $result = curl_exec($ch);
        curl_close($ch);
        // echo $result;


        if ($result) {
              
        }else{
                
        }
            	

		/*$this->load->library('email', $config2);
        $this->email->initialize($config2);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from('finance@meetaza.com', 'Classmiles');
        $this->email->to($email);
        $this->email->subject('Classmiles - Payment Return');
        $templateToStudent = 
        "
        <div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
            <table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
                    <tbody>
                      <tr>        
                          <td style='padding-left:10px;padding-right:10px'>
                              <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:700px' width='100%'>
                                  <tbody>
                                    <tr>
                                        <td style='text-align:center;padding-top:3%'>
                                            <a href='https://indiclass.id/' style='text-decoration:none' target='_blank'>
                                                <img src='".CDN_URL.STATIC_IMAGE_CDN_URL."maillogo_classmiles.png' style='display:block;margin:0;border:0;max-width:700px' width='100%'>
                                            </a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
                                            <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
                                                <tbody>
                                                    <tr>
                                                          <td style='padding-right:5%' width='72%'>
                                                                <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
                                                                    Dear ".$namauser.",
                                                                </h1>
                                                                <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
                                                                    Mohon maaf, pembayaran anda gagal. Uang yang telah anda transfer telah kami masukan ke Uang saku akun anda.<br> 
                                                                </p> 
                                                                <br><br>                                                                        
                                                                <center>
                                                                    <br>
                                                                    <label style='color: #6C7A89; font-size: 16px; margin-top: 5%;'>Total Pengembalian</label>
                                                                    <div style='background: #edecec; width: 50%; height: 70px; margin-top: 20px; border-radius: 5px;'>
                                                                        <label style='color: #6C7A89; margin-top: 20px; top:20px; font-size: 24px;'><br>Rp. ".$hargatotal."</label>
                                                                    </div> 
                                                                    <br>                                                                        
                                                                </center>              
                                                                <div style='border-bottom:2px solid #eeeeee;'>&nbsp;</div>
                                                                <br><br>
                                                                        <label style='color: #95A5A6;'>Berikut adalah detail kelas anda</label>
                                                                        <br><br>
                                                                        <label style='color: #95A5A6'><strong>INVOICE ID : ".$invoice_id."</strong></label>
                                                                        <br><br>
                                                                        <table style='width:100%; border: 1px solid gray;
                                                                            border-collapse: collapse; color: #6C7A89;'>
                                                                            <tr>
                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>TUTOR</th>                                                                                        
                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>MATA PELAJARAN</th>
                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse; width:10px;'>TOPIK</th>
                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>WAKTU PERMINTAAN</th> 
                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>DURASI KELAS</th>                                                                                        
                                                                            </tr> 
                                                                            ".$kotaktd."                                                                                
                                                                        </table>
                                                                    </center>
                                                                </p>                                                    
                                                          </td>
                                                    </tr> 
                                                    <tr>
                                                        <td style='padding-bottom:20px;line-height:20px'>
                                                            <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style='background-color:#ffffff'>                                                                             
                                                            <tbody>
                                                                <tr>
                                                                    <td align='center' style='padding-right:5%' width='100%'>
                                                                          <p style='color:gray;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
                                                                          Diharapkan jangan membalas email ini.<br>
                                                                          Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
                                                                          <br>Terima kasih,                                           
                                                                          <br>
                                                                            Tim Classmiles
                                                                          </p>                                                        
                                                                    </td>                                                   
                                                                </tr>
                                                            </tbody>                                               
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
                                            <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
                                                
                                            </tbody></table>
                                            <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
                                                
                                            </tbody></table>
                                        </td>
                                    </tr>
                                </tbody>
                              </table>
                          </td>
                      </tr>
                </tbody>
            </table>
        </div>
        ";          
        
        $this->email->message($templateToStudent);
        if ($this->email->send()) 
        {
        	
        }
        else
        {
        	
        }*/

	}

	// Fetch and return product data
    public function getOrder($id = ''){
        $this->db->select('*');
        $this->db->from('tbl_order');
        if($id){
            $this->db->where('order_id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            $this->db->order_by('created_at', 'asc');
            $query = $this->db->get();
            $result = $query->result_array();
        }
        return !empty($result)?$result:false;
    }
    
    // Insert transaction data
    public function addTransaction($data = array()){
        $user_id = $data['user_id'];
        $product_id = $data['product_id'];
        $txn_id = $data['txn_id'];
        $payment_gross = $data['payment_gross'];
        $currency_code = $data['currency_code'];
        $payer_email = $data['payer_email'];
        $payment_status = $data['payment_status'];
        if ($payment_status == "Completed") {
        	$this->db->query("UPDATE tbl_order SET order_status=2 WHERE order_id='$product_id'");
            $updatemethod = $this->db->query("UPDATE tbl_invoice SET payment_type='Paypal' WHERE order_id='$product_id'");

            $this->db->query("INSERT INTO payments (user_id, product_id, txn_id, payment_gross, currency_code, payer_email, payment_status) VALUES ('$user_id','$product_id','$txn_id','$payment_gross','$currency_code','$payer_email','$payment_status')");
            // $insert = $this->db->insert('payments',$data);

        }
        else if($payment_status == "Pending"){
            $this->db->query("INSERT INTO payments (user_id, product_id, txn_id, payment_gross, currency_code, payer_email, payment_status) VALUES ('$user_id','$product_id','$txn_id','$payment_gross','$currency_code','$payer_email','$payment_status')");
            // $insert = $this->db->insert('payments',$data);
        }
        else
        {
            $this->db->query("INSERT INTO payments (user_id, product_id, txn_id, payment_gross, currency_code, payer_email, payment_status) VALUES ('$user_id','$product_id','$txn_id','$payment_gross','$currency_code','$payer_email','$payment_status')");
            // $insert = $this->db->insert('payments',$data);
        }
        
        return $insert?true:false;
    }

    public function createInvoice($request_id='',$type='',$billing_type='',$billing_options='')
    {
        $order_id       = $this->Rumus->gen_order_id();
        $UnixCode       = $this->Rumus->RandomUnixCode();
        $user_utc       = $this->session->userdata('user_utc');
        $total_price    = 0;
        $buyer_id       = "";
        $hargatotal     = 0;
        $date_expired   = "";

        if (!empty($request_id)) {            
            if ($type == 'private') {
            	$server_utc         = $this->Rumus->getGMTOffset();
	            $intervall          = $user_utc - $server_utc;
	            $datenow            = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
	            $datenow->modify("+3600 seconds");
	            $datenow            = strtotime($datenow->format('Y-m-d H:i:s'));
	            
	            $datenow            = date('Y-m-d H:i:s', $datenow);
	            $date_expired       = $datenow;

                $data               = $this->db->query("SELECT * FROM tbl_request WHERE request_id='$request_id'")->row_array();
                if ($buyer_id == "") {                    
                    $buyer_id   = $buyer_id+$data['id_user_requester'];
                } 
                $duration_requested = $data['duration_requested'];
                $subject_id         = $data['subject_id'];
                $tutorid            = $data['tutor_id'];
                $this->db->simple_query("UPDATE tbl_request SET approve=2 WHERE request_id='$request_id'");

                $get_pricetutor     = $this->db->query("SELECT price_tutor FROM tbl_request where request_id = '$request_id'")->row_array()['price_tutor'];
                $total_pricetutor   = $get_pricetutor;                
                $product_properties = $this->db->query("SELECT topic FROM tbl_request WHERE request_id = '$request_id'")->row_array()['topic'];
                $total_pricestudent = $this->db->query("SELECT price FROM tbl_request where request_id = '$request_id'")->row_array()['price'];                

                $total_price    = $total_price+$total_pricestudent;
                $save_to_tbl_order_detail = $this->db->query("INSERT INTO tbl_order_detail (order_id,product_id,product_properties,total_amt,init_price,discount,final_price,final_pricetutor,TYPE) VALUES ('$order_id','$request_id','$product_properties','','$total_pricestudent','0','$total_pricestudent','$total_pricetutor','$type')"); 
                $product_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
            }
            else if($type == 'group')
            {
            	$server_utc         = $this->Rumus->getGMTOffset();
	            $intervall          = $user_utc - $server_utc;
	            $datenow            = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
	            $datenow->modify("+3600 seconds");
	            $datenow            = strtotime($datenow->format('Y-m-d H:i:s'));
	            
	            $datenow            = date('Y-m-d H:i:s', $datenow);
	            $date_expired       = $datenow;
	            
            	$data               = $this->db->query("SELECT * FROM tbl_request_grup WHERE request_id='$request_id'")->row_array();
                if ($buyer_id == "") {                    
                    $buyer_id   = $buyer_id+$data['id_user_requester'];
                } 
                $duration_requested = $data['duration_requested'];
                $subject_id         = $data['subject_id'];
                $tutorid            = $data['tutor_id'];
                $this->db->simple_query("UPDATE tbl_request_grup SET approve=2 WHERE request_id='$request_id'");

            	$duration_requested = $this->db->query("SELECT duration_requested FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['duration_requested'];
                $subject_id = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['subject_id'];
                $tutorid = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['tutor_id']; 

                $get_pricetutor     = $this->db->query("SELECT price_tutor FROM tbl_request_grup where request_id = '$request_id'")->row_array()['price_tutor'];
                $total_pricetutor   = $get_pricetutor;                
                $product_properties = $this->db->query("SELECT topic FROM tbl_request_grup WHERE request_id = '$request_id'")->row_array()['topic'];
                $price              = $this->db->query("SELECT price FROM tbl_request_grup where request_id = '$request_id'")->row_array()['price'];
                $total_pricestudent = $price;

                $total_price    = $total_price+$total_pricestudent;
                $save_to_tbl_order_detail = $this->db->query("INSERT INTO tbl_order_detail (order_id,product_id,product_properties,total_amt,init_price,discount,final_price,final_pricetutor,TYPE) VALUES ('$order_id','$request_id','$product_properties','','$total_pricestudent','0','$total_pricestudent','$total_pricetutor','$type')");    
                $product_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];           
            }
            else if($type == 'program')
            {
            	$server_utc         = $this->Rumus->getGMTOffset();
	            $intervall          = $user_utc - $server_utc;
	            $datenow            = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
	            $datenow->modify("+3600 seconds");
	            $datenow            = strtotime($datenow->format('Y-m-d H:i:s'));
	            
	            $datenow            = date('Y-m-d H:i:s', $datenow);
	            $date_expired       = $datenow;
	            
            	$data               = $this->db->query("SELECT * FROM tbl_request_program WHERE request_id='$request_id'")->row_array();
	    		$list_id            = $this->db->query("SELECT program_id FROM tbl_request_program WHERE request_id = '$request_id'")->row_array()['program_id'];
	    		if ($buyer_id == "") {                    
                    $buyer_id   = $buyer_id+$data['id_requester'];
                } 
	            $product_properties = $this->db->query("SELECT name FROM tbl_price_program WHERE list_id = '$list_id'")->row_array()['name'];
	            $price 				= $this->db->query("SELECT price FROM tbl_price_program WHERE list_id = '$list_id'")->row_array()['price'];
	            $total_price    	= $total_price+$price; 
	            $save_to_tbl_order_detail = $this->db->query("INSERT INTO tbl_order_detail (order_id,product_id,product_properties,total_amt,init_price,discount,final_price,final_pricetutor,TYPE) VALUES ('$order_id','$request_id','$product_properties','','$price','0','$price','$price','$type')");
	            $product_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];


            }
            else if($type == 'multicast')
            {
        		$product_idclass = $this->db->query("SELECT class_id FROM tbl_request_multicast WHERE request_id='$request_id'")->row_array()['class_id'];
        		$id_requester = $this->db->query("SELECT id_requester FROM tbl_request_multicast WHERE request_id='$request_id'")->row_array()['id_requester'];
                $product_properties = $this->db->query("SELECT description FROM tbl_class WHERE class_id='$product_idclass'")->row_array()['description'];

                $get_pricetutor = $this->db->query("SELECT harga FROM tbl_class_price WHERE class_id='$product_idclass'")->row_array()['harga'];
                $price 			= $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id = '$product_idclass'")->row_array()['harga_cm'];
                $total_price    = $total_price+$price;
                if ($buyer_id == "") {                    
                    $buyer_id   = $buyer_id+$id_requester;
                }                 
                $save_to_tbl_order_detail = $this->db->query("INSERT INTO tbl_order_detail (order_id,class_id,product_id,product_properties,total_amt,init_price,discount,final_price,final_pricetutor,TYPE) VALUES ('$order_id','$product_idclass','$request_id','$product_properties','','$price','0','$price','$get_pricetutor','multicast')");

                $date_expired 	= $this->db->query("SELECT last_order FROM tbl_class WHERE class_id = '$product_idclass'")->row_array()['last_order'];
            }
            
            $hargatotal = $total_price+$UnixCode;
            $invoice_id = $this->Rumus->gen_invoice_id($order_id);
            $nowdate    = date("Y-m-d H:i:s");

            //CHECK IF ORDER IF READY
            $ckOrderId 	= $this->db->query("SELECT * FROM tbl_order_detail WHERE product_id='$request_id'")->row_array();
            if (!empty($ckOrderId)) {
	            $save_to_tbl_order      = $this->db->query("INSERT INTO tbl_order (order_id,total_price,unix_code,buyer_id,order_status, payment_due, created_at) VALUES ('$order_id','$hargatotal','$UnixCode','$buyer_id','0','$date_expired','$nowdate')");          
	            if ($save_to_tbl_order) {
	            	$save_to_tbl_invoice = 0;
	            	if ($type != 'program') {	            		
	                	$save_to_tbl_invoice    = $this->db->query("INSERT INTO tbl_invoice (invoice_id,order_id,payment_type,total_price,underpayment) VALUES ('$invoice_id','$order_id',null,'$hargatotal','$hargatotal')");
					}
					
	                ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
	                $json_notif = "";
	                $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$buyer_id}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
	                if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
	                    $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "40","title" => "Classmiles", "text" => "")));
	                }
	                if($json_notif != ""){
	                    $headers = array(
	                        'Authorization: key=' . FIREBASE_API_KEY,
	                        'Content-Type: application/json'
	                        );
	                    // Open connection
	                    $ch = curl_init();

	                    // Set the url, number of POST vars, POST data
	                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
	                    curl_setopt($ch, CURLOPT_POST, true);
	                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

	                    // Execute post
	                    $result = curl_exec($ch);
	                    curl_close($ch);
	                    // echo $result;
	                }
	                ////------------ END -------------------------------------//

	                if ($save_to_tbl_invoice) {

	                	$this->db->query("UPDATE tbl_order SET order_status=2 WHERE order_id='$order_id'");
				        $config2 = Array(
				            'protocol' => 'smtp',
				            'smtp_host' => 'ssl://smtp.googlemail.com',
				            'smtp_port' => 465,
				            'smtp_user' =>'finance@meetaza.com',
				            'smtp_pass' => 'xmbmpuwuxlzmcjsl', 
				            'mailtype' => 'html',                    
				            'charset' => 'iso-8859-1'
				        );
	                	$kotaktd = "";                	 
	                	
	                	$get_datadetail = $this->db->query("SELECT * FROM tbl_order as tor INNER JOIN tbl_invoice as ti ON tor.order_id=ti.order_id WHERE tor.order_id='$order_id'")->row_array();
	                    $invoice_id     = $get_datadetail['invoice_id'];
	                    $total_price    = $get_datadetail['total_price'];
	                    $UnixCode       = $get_datadetail['unix_code'];
	                    $hargatotal     = $total_price+$UnixCode;
	                    $harga_kelas    = $total_price-$UnixCode;
	                	if ($type == 'private') {

	                		$tutor_id   = $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['tutor_id'];
	                        $subject_id = $this->db->query("SELECT subject_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['subject_id'];
	                        $getdataclass  = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();                            
	                        $iduserrequest  = $getdataclass['id_user_requester'];  
	                        $daterequesttt  = $getdataclass['date_requested'];    
	                        $durationrequest= $getdataclass['duration_requested']; 

	                        $hargaperkelas  = $this->db->query("SELECT price FROM tbl_request_grup where request_id = '$request_id'")->row_array()['price'];
	                        // $hargaperkelas  = ($durationrequest/900)*$harga_k;    
	                        $hargaperkelas  = number_format($hargaperkelas, 0, ".", "."); 

	                        $email_username = $getdataclass['user_name'];
	                        $email_subject  = $getdataclass['subject_name'];
	                        $email_topic    = $getdataclass['topic'];  
	                	}
	                	else if($type == "group")
	                    {
	                        $tutor_id       = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['tutor_id'];
	                        $subject_id     = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['subject_id'];
	                        $getdataclass   = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request_grup as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();
	                        $iduserrequest  = $getdataclass['id_user_requester'];     
	                        $daterequesttt  = $getdataclass['date_requested'];      
	                        $durationrequest= $getdataclass['duration_requested'];

	                        $hargaperkelas        = $this->db->query("SELECT price FROM tbl_request_grup where request_id = '$request_id'")->row_array()['price'];
	                        // $hargaperkelas  = ($durationrequest/900)*$harga_k;    
	                        $hargaperkelas  = number_format($hargaperkelas, 0, ".", ".");    

	                        $email_username = $getdataclass['user_name'];
	                        $email_subject  = $getdataclass['subject_name'];
	                        $email_topic    = $getdataclass['topic']; 
	                    }
	                    else if ($type == "program") {
	                    	$list_id        = $this->db->query("SELECT program_id FROM tbl_request_program WHERE request_id = '$request_id'")->row_array()['program_id'];
	                                                
	                        $getdataclass   = $this->db->query("SELECT * FROM tbl_price_program WHERE list_id = '$list_id'")->row_array();
	                        $iduserrequest  = $this->db->query("SELECT id_requester FROM tbl_request_program WHERE request_id = '$request_id'")->row_array()['id_requester'];    
	                        $daterequesttt  = $getdataclass['created_at'];      
	                        $durationrequest= 0;
	                                       
	                        $hargaperkelas  = number_format($getdataclass['price'], 0, ".", ".");  
	                        $email_username = $getdataclass['name'];
	                        $email_subject  = '';
	                        $email_topic    = '';
	                    } 
	                    else if ($type == "multicast") {
	                    	$class_id       = $this->db->query("SELECT class_id FROM tbl_request_multicast WHERE request_id = '$request_id'")->row_array()['class_id'];
	                                                
	                        $getdataclass   = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id'")->row_array();
	                        $iduserrequest  = $this->db->query("SELECT id_requester FROM tbl_request_program WHERE request_id = '$request_id'")->row_array()['id_requester'];    
	                        $daterequesttt  = $getdataclass['start_time'];      
	                        $durationrequest= 0;

	                        $price 				= $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id = '$class_id'")->row_array()['harga_cm'];

	                        $hargaperkelas  = number_format($price, 0, ".", ".");  
	                        $email_username = $getdataclass['name'];
	                        $email_subject  = '';
	                        $email_topic    = '';
	                    }                   

	                	$iduserrequest      = $this->db->query("SELECT buyer_id FROM tbl_order WHERE order_id='$order_id'")->row_array()['buyer_id'];
	                    $user_utc           = $this->session->userdata('user_utc');
	                    if ($user_utc == "") {
	                    	$user_utc = '420';
	                    }      

	                    $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
	                    $email   			= $this->db->query("SELECT email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['email'];
	                    $durasi             = $durationrequest;
	                    $total_price        = number_format($total_price, 0, ".", ".");
	                    $hargatotal         = number_format($hargatotal, 0, ".", ".");
	                    $harga_kelas        = number_format($harga_kelas, 0, ".", ".");
	                    $server_utcc        = $this->Rumus->getGMTOffset();
	                    $intervall          = $user_utc - $server_utcc;
	                    $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$daterequesttt);
	                    $tanggal->modify("+".$intervall ." minutes");
	                    $tanggal            = $tanggal->format('Y-m-d H:i:s');
	                    $datelimit          = date_create($tanggal);
	                    $dateelimit         = date_format($datelimit, 'd/m/y');
	                    $harilimit          = date_format($datelimit, 'd');
	                    $hari               = date_format($datelimit, 'l');
	                    $tahunlimit         = date_format($datelimit, 'Y');
	                    $waktulimit         = date_format($datelimit, 'H:i');   
	                    $datelimit          = $dateelimit;
	                    $sepparatorlimit    = '/';
	                    $partslimit         = explode($sepparatorlimit, $datelimit);
	                    $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
	                    $seconds            = $durationrequest;
	                    $hours              = floor($seconds / 3600);
	                    $mins               = floor($seconds / 60 % 60);
	                    $secs               = floor($seconds % 60);
	                    $durationrequested  = sprintf('%02d:%02d', $hours, $mins);
	                    
	                    $kotaktd = $kotaktd."<tr>
	                        <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$email_username."</td>
	                        <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$email_subject."</td>
	                        <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$email_topic."</td>
	                        <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", ".$waktulimit."</td>
	                        <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td> 
	                    </tr>";

	                    $last_update        = $this->db->query("SELECT payment_due FROM tbl_order WHERE order_id='$order_id'")->row_array()['payment_due'];
		                $dateee             = $last_update;
		                $server_utcc        = $this->Rumus->getGMTOffset();
		                $intervall          = $user_utc - $server_utcc;
		                $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$dateee);
		                $tanggaltransaksi->modify("+".$intervall ." minutes");
		                
		                $hariakhirr 	= $tanggaltransaksi->format('d');
		                $hariakhir 		= $tanggaltransaksi->format('l');
		                $tahunakhir 	= $tanggaltransaksi->format('Y');
		                $waktuakhir 	= $tanggaltransaksi->format('H:i');
		                $bulanakhir 	= $tanggaltransaksi->format('F');

		                // Open connection
		                $ch = curl_init();

		                // Set the url, number of POST vars, POST data
		                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
		                curl_setopt($ch, CURLOPT_POST, true);
		                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_submitMethodPayment','content' => array("email" => $email, "invoice_id" => $invoice_id, "namauser" => $namauser, "hargakelas" => $harga_kelas, "UnixCode" => $UnixCode, "hargatotal" => $total_price, "hariakhir" => $hariakhir, "hariakhirr" => $hariakhirr, "bulanakhir" => $bulanakhir, "tahunakhir" => $tahunakhir, "waktuakhir" => $waktuakhir, "kotaktd" => $kotaktd) )));

		                // Execute post
		                $result = curl_exec($ch);
		                curl_close($ch);
		                // echo $result;
		                if ($result) {
	        				
	        				if ($type == 'private' || $type == 'group') {
	        					
	        					if ($type == 'private')
	        					{
	        						$tutor_id 		= $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id = '$request_id'")->row_array()['tutor_id'];        					
	        					}
	        					else
	        					{
	        						$tutor_id 		= $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id = '$request_id'")->row_array()['tutor_id'];
	        					}

		        				$usernametutor 	= $this->db->query("SELECT user_name FROM tbl_user WHERE id_user = '$tutor_id'")->row_array()['user_name'];
		        				$emailtutor 	= $this->db->query("SELECT email FROM tbl_user WHERE id_user = '$tutor_id'")->row_array()['email'];;
		        				
			                	// Open connection
				                $ch = curl_init();

				                // Set the url, number of POST vars, POST data
				                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
				                curl_setopt($ch, CURLOPT_POST, true);
				                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_sendConfrimRequestFromTutor','content' => array("emailtutor" => $emailtutor, "usernametutor" => $usernametutor, "hargakelas" => $harga_kelas, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "durationrequested" => $durationrequested, "email_subject" => $email_subject, "email_topic" => $email_topic, "tanngalapprove" => $datenow) )));

				                // Execute post
				                $result = curl_exec($ch);
				                curl_close($ch);
				                // echo $result;
				            }

	                    	return 1;
	                    }
	                    else
	                    {
	                    	return -1;
	                    }
	                }
	                else if ($type == 'program'){
	                	if ($billing_type == 'bycounter') {
	                		$this->db->query("INSERT INTO tbl_billing_mgmt (order_id, name, amount, counters_left_for_bycounter) VALUES ('$order_id','$billing_type', $billing_options, $billing_options) ");
	                	}
	                	else
	                	{
	                		$this->db->query("INSERT INTO tbl_billing_mgmt (order_id, name, amount) VALUES ('$order_id','$billing_type',$billing_options) ");
	                	}
	                	return 1;
	                }
	                else
	                {
	                    return -2;
	                }
	            }
	            else
	            {
	                return -3;
	            }
	        }
	        else
	        {
	        	return -4;
	        }
        }

    }

    public function approveRequestKuota($table, $where)
	{
		$delete = $this->db->query("UPDATE tbl_multicast_volume SET status = '1' WHERE idv='$where[idv]'");
		
		if ($delete) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

    public function rejectRequestKuota($table, $where)
	{
		$delete = $this->db->query("UPDATE tbl_multicast_volume SET status = '-1' WHERE idv='$where[idv]'");
		
		if ($delete) {
			$return = 1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

} 