<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model{	

	function gen_session($email = null){

			$digit = 5;
			$time = time();

			/*$karakter = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+{}|:<>?';,./[]";
			srand((double)microtime()*1000000);
			$i = 0;
			$pass = "";
			while ($i <= $digit-1)
			{
			$num = rand() % 32;
			$tmp = substr($karakter,$num,1);
			$pass = $pass.$tmp;
			$i++;
			}*/
			//echo $email.$time."<br>";
			$sha = sha1($email.$time);
			return $sha;

		// 	$where = array(
		// 			'random_key' => $pass
		// 		);

		// $cek = $this->M_login->saveRandKey("gen_auth", $where);
		// $this->session->set_flashdata('cek', $cek);	


	}
	public function second_genses($value='')
	{
		$sha = sha1($value);
		return $sha;
	}

	private $tablesss = "tbl_user";
    private $pks = "id_user";

	// update user
    public function update($data, $id_user)
    {
         $setcookie = $this->db->query("UPDATE tbl_user set cookie_code = '$data' WHERE id_user = '$id_user'");
         if ($setcookie) {
         	return 1;
         }
         return 0 ;
    }

    function get_by_cookie($data){
    	$a = $this->db->query("SELECT * from tbl_user WHERE cookie_code = '$data'")->row_array();
    	return $a;
    }

	function cl_daftartutor($table,$where){		
		// check user ada apa gak di database
		$res = $this->db->query("SELECT ts.*, tps.jenjang_id FROM tbl_user as ts LEFT JOIN tbl_profile_student as tps ON ts.id_user=tps.student_id WHERE ( email='$where[email]' || username='$where[email]' )")->row_array();
		if(empty($res) || !password_verify($where['kata_sandi'], $res['password'])){
			return 0;			
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://classmiles.com/Rest/googleAuthGetJWT");
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$res);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,4000); 

		$curl_rets = curl_exec($ch);
		// if(curl_errno($ch)){
    		// echo 'Curl error: ' . curl_error($ch);
		// }
		$access_token = json_decode($curl_rets,true);
		
		$this->session->set_userdata('access_token_jwt',$access_token['access_token']);

		$nm_lengkap = $res['first_name']+$res['last_name'];
		$this->session->set_userdata('id_user',$res['id_user']);
		$this->session->set_userdata('email',$res['email']);
		$this->session->set_userdata('kode_area',$res['user_countrycode']);
		$this->session->set_userdata('no_hp',$res['user_callnum']);
		$this->session->set_userdata('jenis_kelamin',$res['user_gender']);
		$this->session->set_userdata('nama_lengkap',$nm_lengkap);
		$this->session->set_userdata('tanggal_lahir',$res['user_birthdate']);
		$this->session->set_userdata('umur',$res['user_age']);
		$this->session->set_userdata('agama',$res['user_religion']);
		$this->session->set_userdata('usertype_id', $res['usertype_id']);
		$this->session->set_userdata('jenjang_id', $res['jenjang_id']);	
		$this->session->set_userdata('user_image', CDN_URL.USER_IMAGE_CDN_URL.$res['user_image']);
		$this->session->set_userdata('status', $res['status']);		
		$this->session->set_userdata('address', $res['user_address']);
		$this->session->set_userdata('number', $where['kata_sandi']);
		// $this->session->set_userdata('lang','indonesia');
		$this->session->set_userdata('color','blue');
		return 1;
	}

	function cek_loginn($table,$where){		
		// check user ada apa gak di database
		$res = $this->db->query("SELECT ts.*, tps.jenjang_id FROM tbl_user as ts LEFT JOIN tbl_profile_student as tps ON ts.id_user=tps.student_id WHERE ( email='$where[email]' || username='$where[email]' )")->row_array();
		if(empty($res) || !password_verify($where['kata_sandi'], $res['password'])){
			return 0;			
		}
		$cek_ch = $this->db->query("SELECT * FROM master_channel WHERE channel_id='$where[channel_id]'")->row_array();
		$this->session->set_userdata('channel_id', $cek_ch['channel_id']);
		$this->session->set_userdata('channel_logo', $cek_ch['channel_logo']);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://classmiles.com/Rest/googleAuthGetJWT");
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$res);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,4000); 

		$curl_rets = curl_exec($ch);
		// if(curl_errno($ch)){
    		// echo 'Curl error: ' . curl_error($ch);
		// }
		$access_token = json_decode($curl_rets,true);
		
		$this->session->set_userdata('access_token_jwt',$access_token['access_token']);

		// $nm_lengkap = $res['first_name']+$res['last_name'];
		$this->session->set_userdata('id_user',$res['id_user']);
		$this->session->set_userdata('id_user_kids',$res['id_user']);
		$this->session->set_userdata('email',$res['email']);
		$this->session->set_userdata('kode_area',$res['user_countrycode']);
		$this->session->set_userdata('no_hp',$res['user_callnum']);
		$this->session->set_userdata('jenis_kelamin',$res['user_gender']);
		$this->session->set_userdata('user_name', $res['user_name']);
		$this->session->set_userdata('tanggal_lahir',$res['user_birthdate']);
		$this->session->set_userdata('umur',$res['user_age']);
		$this->session->set_userdata('agama',$res['user_religion']);
		$this->session->set_userdata('usertype_id', $res['usertype_id']);
		$this->session->set_userdata('jenjang_id', $res['jenjang_id']);	
		$this->session->set_userdata('user_image', CDN_URL.USER_IMAGE_CDN_URL.$res['user_image']);
		$this->session->set_userdata('status', $res['status']);		
		$this->session->set_userdata('address', $res['user_address']);
		$this->session->set_userdata('number', $where['kata_sandi']);
		// $this->session->set_userdata('lang','indonesia');
		$this->session->set_userdata('color','blue');
		return 1;

	}



	function cek_user($table,$where){		
		// check user ada apa gak di database
		$res = $this->db->query("SELECT ts.*, tpk.parent_id, tpk.jenjang_id FROM tbl_user as ts LEFT JOIN tbl_profile_kid as tpk ON ts.id_user=tpk.kid_id WHERE id_user='$where[id_user]'")->row_array();
		// $parent_id   = $this->session->set_userdata('parent_id',$res['parent_id']);
		$id_user_kids   = $this->session->set_userdata('id_user_kids',$res['id_user']);
		$umur_kids   = $this->session->set_userdata('umur_kids',$res['user_age']);
		$jenjang_id_kids = $this->session->set_userdata('jenjang_id_kids',$res['jenjang_id']);
		$img_user_kids   = $this->session->set_userdata('img_user_kids', CDN_URL.USER_IMAGE_CDN_URL.$res['user_image']);
		$username_kids = $this->session->set_userdata('username_kids',$res['user_name']);
		$this->session->set_userdata('usertype_now',$res['usertype_id']);
		$this->session->set_userdata('kodeku','CHANGE');
		$this->session->set_userdata('id_user', $res['id_user']);
		
		// $this->session->set_userdata('id_user_kids',$res['id_user']);
				
	}

	function cek_login($table,$where){		
		// check user ada apa gak di database
		$res = $this->db->query("SELECT ts.*, tps.jenjang_id FROM tbl_user as ts LEFT JOIN tbl_profile_student as tps ON ts.id_user=tps.student_id WHERE ( email='$where[email]' || username='$where[email]' )")->row_array();
		$id_user   = $this->session->set_userdata('id_user',$res['id_user']);
		$cek_school_id = $this->db->query("SELECT school_id FROM `tbl_profile_student` where student_id='$id_user'")->row_array();
		if ($res != null) {
			if ($res['status'] != 0) {
				if (password_verify($where['kata_sandi'], $res['password'])) {
					$cek_ch = $this->db->query("SELECT * FROM master_channel WHERE channel_id='$where[channel_id]'")->row_array();
					$this->session->set_userdata('channel_id', $cek_ch['channel_id']);
					$this->session->set_userdata('channel_logo', $cek_ch['channel_logo']);

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "https://classmiles.com/Rest/googleAuthGetJWT");
					// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
					curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
					curl_setopt($ch, CURLOPT_POST,1);
					curl_setopt($ch, CURLOPT_POSTFIELDS,$res);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
					curl_setopt($ch, CURLOPT_TIMEOUT_MS,4000); 

					$curl_rets = curl_exec($ch);
					// if(curl_errno($ch)){
			    		// echo 'Curl error: ' . curl_error($ch);
					// }
					$access_token = json_decode($curl_rets,true);

					// $basename     = $sessioniduser.$this->Rumus->RandomString();
					
					$this->session->set_userdata('access_token_jwt',$access_token['access_token']);
					$this->session->set_userdata('kode_login', '1');
					// return 1;

					if ($res['usertype_id'] == 'user channel') {
						$channel_id = substr($res['id_user'], -3);
						$this->session->set_userdata('channel_id',$channel_id);
					}
					
					$this->session->set_userdata('img_user_kids', CDN_URL.USER_IMAGE_CDN_URL.$res['user_image']);
					$this->session->set_userdata('id_user_kids',$res['id_user']);
					$this->session->set_userdata('id_user',$res['id_user']);
					$this->session->set_userdata('email',$res['email']);
					$this->session->set_userdata('kode_area',$res['user_countrycode']);
					$this->session->set_userdata('no_hp',$res['user_callnum']);
					$this->session->set_userdata('jenis_kelamin',$res['user_gender']);
					$this->session->set_userdata('user_name', $res['user_name']);
					$this->session->set_userdata('tanggal_lahir',$res['user_birthdate']);
					$this->session->set_userdata('umur',$res['user_age']);
					$this->session->set_userdata('agama',$res['user_religion']);
					$this->session->set_userdata('usertype_id', $res['usertype_id']);
					$this->session->set_userdata('jenjang_id', $res['jenjang_id']);	
					$this->session->set_userdata('user_image', CDN_URL.USER_IMAGE_CDN_URL.$res['user_image']);
					$this->session->set_userdata('status', $res['status']);		
					$this->session->set_userdata('address', $res['user_address']);
					$this->session->set_userdata('number', $where['kata_sandi']);
					// $this->session->set_userdata('lang','indonesia');
					$this->session->set_userdata('color','blue');		
					$cek_school_id = $this->db->query("SELECT school_id FROM `tbl_profile_student` where student_id='$res[id_user]'")->row_array();
					// $this->session->set_userdata('lang','english');
					//$this->session->set_userdata('self_description', $res['self_description']);			

					// if ($res['usertype_id'] == "tutor") {
					// 	$select_session = $this->db->query("SELECT session_id FROM tbl_session WHERE tutor_id='$res[id_user]'")->row_array();
					// 	$this->session->set_userdata('temp_session', $select_session['session_id']);
					// }else if($res['usertype_id'] == "student"){
						// $select_session = $this->db->query("SELECT session_id FROM tbl_participant_session WHERE id_user='$res[id_user]'")->row_array();
						// $this->session->set_userdata('temp_session', $select_session['id']);

					/*$select_subject_id = $this->db->query("SELECT subject_id FROM master_subject WHERE jenjang_id = '$select_jenjang_id[jenjang_id]'")->row_array();
					$subject_id = $this->session->set_userdata('subject_id', $select_subject_id['subject_id']);*/

					//$show_tutor = $this->db->query("SELECT *FROM tbl_booking WHERE subject_id = '$select_subject_id[subject_id]'")->result();
					//print_r($show_tutor);
					
					// $this->M_Login->createSess();	
					// $session_auth = $this->gen_session();
					// $ids = $res['id_user'];
					// $save = $this->db->simple_query("INSERT INTO temp_session (id_user, temp_session) VALUES ('$ids','$session_auth')");
					// if ($save) {
					// 	return 1;
					// }else{
					// 	return $save;
					// }
					return 1;
				}
				else{
					// password tidak sama
					$this->session->set_userdata('kode_login', '0');
					return 0;	
				}
			}
			else{
				// status belum aktifasi
				$this->session->set_userdata('kode_login', '-1');
				return -2;		
			}
		}
		else{
			// data tidak ada 
			$this->session->set_userdata('kode_login', '0');
			return 0;
		}
		
	}

	function cek_cokeis($table,$where){		
		// check user ada apa gak di database
		$res = $this->db->query("SELECT ts.*, tps.jenjang_id FROM tbl_user as ts LEFT JOIN tbl_profile_student as tps ON ts.id_user=tps.student_id WHERE ( email='$where[email]' || username='$where[email]' )")->row_array();
		if(empty($res) ){
			return 0;			
		}
				
		// check session generated nya masih login atau udh logout
		// $ids = $res['id_user'];
		// $query = $this->db->query("SELECT *FROM temp_session WHERE id_user='$ids'")->row_array();
		// if (!empty($query)) {
		// 	return 0;
		// }

		// $nm_lengkap = $res['first_name']+$res['last_name'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://classmiles.com/Rest/googleAuthGetJWT");
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$res);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,4000); 

		$curl_rets = curl_exec($ch);
		// if(curl_errno($ch)){
    		// echo 'Curl error: ' . curl_error($ch);
		// }
		$access_token = json_decode($curl_rets,true);
		
		$this->session->set_userdata('access_token_jwt',$access_token['access_token']);

		$this->session->set_userdata('img_user_kids', CDN_URL.USER_IMAGE_CDN_URL.$res['user_image']);
		$this->session->set_userdata('id_user_kids',$res['id_user']);
		$this->session->set_userdata('id_user',$res['id_user']);
		$this->session->set_userdata('email',$res['email']);
		$this->session->set_userdata('kode_area',$res['user_countrycode']);
		$this->session->set_userdata('no_hp',$res['user_callnum']);
		$this->session->set_userdata('jenis_kelamin',$res['user_gender']);
		$this->session->set_userdata('user_name', $res['user_name']);
		$this->session->set_userdata('tanggal_lahir',$res['user_birthdate']);
		$this->session->set_userdata('umur',$res['user_age']);
		$this->session->set_userdata('agama',$res['user_religion']);
		$this->session->set_userdata('usertype_id', $res['usertype_id']);
		$this->session->set_userdata('jenjang_id', $res['jenjang_id']);	
		$this->session->set_userdata('user_image', CDN_URL.USER_IMAGE_CDN_URL.$res['user_image']);
		$this->session->set_userdata('status', $res['status']);		
		$this->session->set_userdata('address', $res['user_address']);
		$this->session->set_userdata('number', $res['password']);
		// $this->session->set_userdata('lang','indonesia');
		$this->session->set_userdata('color','blue');		
		// $this->session->set_userdata('lang','english');
		//$this->session->set_userdata('self_description', $res['self_description']);			

		// if ($res['usertype_id'] == "tutor") {
		// 	$select_session = $this->db->query("SELECT session_id FROM tbl_session WHERE tutor_id='$res[id_user]'")->row_array();
		// 	$this->session->set_userdata('temp_session', $select_session['session_id']);
		// }else if($res['usertype_id'] == "student"){
			// $select_session = $this->db->query("SELECT session_id FROM tbl_participant_session WHERE id_user='$res[id_user]'")->row_array();
			// $this->session->set_userdata('temp_session', $select_session['id']);

		/*$select_subject_id = $this->db->query("SELECT subject_id FROM master_subject WHERE jenjang_id = '$select_jenjang_id[jenjang_id]'")->row_array();
		$subject_id = $this->session->set_userdata('subject_id', $select_subject_id['subject_id']);*/

		//$show_tutor = $this->db->query("SELECT *FROM tbl_booking WHERE subject_id = '$select_subject_id[subject_id]'")->result();
		//print_r($show_tutor);
		
		// $this->M_Login->createSess();
		// $session_auth = $this->gen_session();
		// $ids = $res['id_user'];
		// $save = $this->db->simple_query("INSERT INTO temp_session (id_user, temp_session) VALUES ('$ids','$session_auth')");
		// if ($save) {
		// 	return 1;
		// }else{
		// 	return $save;
		// }

		return 1;
		
		//Create session login 

		
		
	}


	function google_login($email = '',$channel_id = ''){		
		// check user ada apa gak di database
		$res = $this->db->query("SELECT ts.*, tps.jenjang_id FROM tbl_user as ts LEFT JOIN tbl_profile_student as tps ON ts.id_user=tps.student_id WHERE email='".$email."' ")->row_array();
		
		if(empty($res) ){
			return 0;			
		}
		$cek_ch = $this->db->query("SELECT * FROM master_channel WHERE channel_id='".$channel_id."'")->row_array();
		$this->session->set_userdata('channel_id', $cek_ch['channel_id']);
		$this->session->set_userdata('channel_logo', $cek_ch['channel_logo']);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://classmiles.com/Rest/googleAuthGetJWT");
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$res);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,4000); 

		$curl_rets = curl_exec($ch);
		// if(curl_errno($ch)){
    		// echo 'Curl error: ' . curl_error($ch);
		// }
		$access_token = json_decode($curl_rets,true);
		$this->session->set_userdata('access_token_jwt',$access_token['access_token']);
		$this->session->set_userdata('id_user',$res['id_user']);
		$this->session->set_userdata('id_user_kids',$res['id_user']);
		$this->session->set_userdata('email',$res['email']);
		$this->session->set_userdata('kode_area',$res['user_countrycode']);
		$this->session->set_userdata('no_hp',$res['user_callnum']);
		$this->session->set_userdata('jenis_kelamin',$res['user_gender']);
		$this->session->set_userdata('nama_lengkap',$res['user_name']);
		$this->session->set_userdata('user_name',$res['user_name']);
		$this->session->set_userdata('tanggal_lahir',$res['user_birthdate']);
		$this->session->set_userdata('umur',$res['user_age']);
		$this->session->set_userdata('agama',$res['user_religion']);
		$this->session->set_userdata('usertype_id', $res['usertype_id']);
		$this->session->set_userdata('jenjang_id', $res['jenjang_id']);	
		$this->session->set_userdata('user_image', CDN_URL.USER_IMAGE_CDN_URL.$res['user_image']);
		$this->session->set_userdata('img_user_kids', CDN_URL.USER_IMAGE_CDN_URL.$res['user_image']);
		$this->session->set_userdata('status', $res['status']);		
		$this->session->set_userdata('address', $res['user_address']);
		$this->session->set_userdata('number', $res['password']);
		$this->session->set_userdata('color','blue');

		$iduser = $this->session->userdata('id_user');
		if ($this->session->userdata('jenjang_id') == '15') {
			$this->session->set_userdata('status_user', 'umum');	
			$cek_anak 	= $this->db->query("SELECT tpk.kid_id, tpk.jenjang_id, tu.user_name, tu.first_name, tu.last_name, tu.user_image from tbl_profile_kid as tpk INNER JOIN tbl_user as tu where tu.id_user = tpk.kid_id and tpk.parent_id='$iduser'")->row_array();
			if (!empty($cek_anak)) {
				 $this->session->set_userdata('punya_anak','yes');
			}
			else{
				$this->session->set_userdata('punya_anak','no');	
			}
		}
		else if ($this->session->userdata('jenjang_id') != '15') {
			$this->session->set_userdata('status_user', 'pelajar');	
		}
		if ($this->session->userdata('usertype_id') == 'student') {
			if ($this->session->userdata('status')==0) {
				$load = $this->lang->line('checkemail');
				$rets['mes_alert'] ='warning';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				$rets['redirect_path'] = 'Subjects';
				return null;
			}
			$rets['redirect_path'] = '/';
			return null;
		}else if($this->session->userdata('usertype_id') == 'tutor'){
			if ($this->session->userdata('status')==3) {
				$load = $this->lang->line('completetutorapproval');
				$rets['mes_alert'] ='warning';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				$rets['redirect_path'] = '/tutor/approval';
				return $rets;
			}else if($this->session->userdata('status')==2){
				$load = $this->lang->line('requestsent');
				$rets['mes_alert'] ='info';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				$rets['redirect_path'] = '/';
				return $rets;
			}else if($this->session->userdata('status')==0){
				$load = $this->lang->line('checkemail');
				$rets['mes_alert'] ='warning';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				$rets['redirect_path'] = '/';
				return $rets;
			}
		}else if($this->session->userdata('usertype_id') == 'admin'){
			$rets['redirect_path'] = '/admin';
			return $rets;
		}else{
			$rets['redirect_path'] = '/moderator';
			return $rets;
		}

			
		return 1;
		
	}

	function createSess($id_user, $email){
		//Create session login 
		//alert('Heii');
		$session_auth = $this->gen_session($email);
		$ids = $id_user;
		$save = $this->db->simple_query("INSERT INTO temp_session (id_user, temp_session, roomNumber) VALUES ('$id_user','$session_auth','1234567')");
		if ($save) {
			return $session_auth;
		}else{
			return $save;
		}
	}

	function randomKey(){

			$digit = 32;
			$karakter = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+{}|:<>?';,./[]";
			srand((double)microtime()*1000000);
			$i = 0;
			$pass = "";
			while ($i <= $digit-1)
			{
			$num = rand() % 32;
			$tmp = substr($karakter,$num,1);
			$pass = $pass.$tmp;
			$i++;
			}
			return $pass;
	}



	function daftar_baru($table,$where){
		//$pass = md5($where['kata_sandi']);
		$email = $where['email'];
		$umur = $where['tanggal_lahir'];
		$skrg = date_diff(date_create($umur), date_create('today'))->y;
		$cek = $this->db->query("SELECT * FROM tbl_user WHERE email='$email'")->row_array();
		if (empty($cek)) {
			$id_user = $this->Rumus->gen_id_user();
			// echo "INSERT INTO tbl_user (id_user, user_name, first_name, last_name, email, password, user_birthdate, user_callnum, user_gender, usertype_id, user_image, st_tour, user_age) VALUES ('$id_user','$where[user_name]','$where[first_name]','$where[last_name]','$where[email]','$where[kata_sandi]','$where[tanggal_lahir]','$where[no_hp_murid]','$where[jenis_kelamin]','student','empty.jpg','0','$skrg')";
			// return 1;
			$res = $this->db->simple_query("INSERT INTO tbl_user (id_user, user_name, first_name, last_name, email, password, user_birthdate, user_nationality,  user_countrycode, user_callnum, user_gender, usertype_id, user_image, st_tour, reg_by, user_age) VALUES ('$id_user','$where[user_name]','$where[first_name]','$where[last_name]','$where[email]','$where[kata_sandi]','$where[tanggal_lahir]','$where[user_nationality]','$where[user_countrycode]','$where[no_hp_murid]','$where[jenis_kelamin]','student','".base64_encode('user/empty.jpg')."','0','$where[reg_by]','$skrg')");
				if ($res) {
					$return['status'] = true;
					$return['message'] = "succeed";
					$r_key = $this->M_login->randomKey();

					//TAMBAH NOTIF
					$notif_message = json_encode( array("code" => 33));
					$link = BASE_URL()."master/resendEmaill";
					$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','$link','0')");

					$simpan = $this->db->simple_query("INSERT INTO gen_auth (random_key, id_user) VALUES ('$r_key','$id_user')");
					$simpan_profile_student = $this->db->simple_query("INSERT INTO tbl_profile_student (student_id, jenjang_id) VALUES ('$id_user','$where[jenjang_id]')");

					// $simpan_notif = $this->Rumus->create_notif($notif='Please, check your email to verify this account',$notif_mobile='Please, check your email to verify this account',$notif_type='single',$id_user=$id_user,$link='');
					// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,status) VALUES ('Please, check your email to verify this account','register','$id_user','','0')");
					$cekcek = "1";
					//$simpan_prof_stu = $this->db->simple_query("INSERT INTO tbl_profile_student () VALUES ()");
					$return['email'] = $email;
					// $return['first_name'] = $where['first_name'];
					$return['kata_sandi'] = $where['kata_sandi'];
					$return['id_user'] = $id_user;
					$return['r_key'] = $r_key;
					$return['cekcek'] = $cekcek;
					$return['key'] = 0;										
					return $return;
				}
				else
				{
					return 0;
				}
		}else if(!empty($cek)){
			$last_data = $this->db->query("SELECT * FROM tbl_user WHERE email='$email'")->row_array();
			$cek_status = $last_data['status'];
			$id_user = $last_data['id_user'];
			$email = $last_data['email'];
			$cekcek = "2";
			$r_key = $this->M_login->randomKey();
			if ($cek_status == 0) {
				//key 2 =  email sudah ada
				$return['email'] = $email;				
				$return['id_user'] = $id_user;
				$return['r_key'] = $r_key;
				$return['key'] = 2;
				$return['cekcek'] = $cekcek;
				return $return;
			}else{
				//key 3 email ada dan status bukan nol
				$return['email'] = $email;				
				$return['id_user'] = $id_user;
				$return['r_key'] = $r_key;
				$return['key'] = 3;
				$return['cekcek'] = $cekcek;
				return $return;
			}
		}
		$load = $this->lang->line('failed');
		$return['status'] = false;
		$return['message'] = $load;
		return $return;
	}

	function daftar_tutor($table,$where)
	{
		$email = $where['email'];
		$umur = $where['tanggal_lahir'];
		$skrg = date_diff(date_create($umur), date_create('today'))->y;
		$cek = $this->db->query("SELECT *FROM tbl_user WHERE email='$email'")->row_array();	
		$r_key = $this->M_login->randomKey();
		
		if (empty($cek)) {
			$id_user = $this->Rumus->gen_id_user();
			$res = $this->db->simple_query("INSERT INTO tbl_user (id_user, user_name, first_name, last_name, email, password, user_birthdate, user_nationality, user_countrycode, user_callnum, user_gender, usertype_id, user_image, user_age, referral_id, reg_by, st_tour) VALUES ('$id_user','$where[nama_lengkap]','$where[first_name]','$where[last_name]','$where[email]','$where[kata_sandi]','$where[tanggal_lahir]','$where[user_nationality]','$where[user_countrycode]','$where[no_hp_tutor]','$where[jenis_kelamin]','tutor','".base64_encode('user/empty.jpg')."',$skrg,'$where[referral_id]','$where[reg_by]','0')");
			if ($res) {
				$return['status'] = true;
				$return['message'] = "succeed";		 	
				$r_key = $this->M_login->randomKey();
				$simpan = $this->db->simple_query("INSERT INTO gen_auth (random_key, id_user) VALUES ('$r_key','$id_user')");
				$simpan_profile_tutor = $this->db->simple_query("INSERT INTO tbl_profile_tutor (tutor_id) VALUES ('$id_user')");

				//NOTIF
				$notif_message = json_encode( array("code" => 42));					
					$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','','0')");

				// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,read_status) VALUES ('Successfully registered, please check your email for verification process.','tutor register','$id_user','','0')");
				$return['email'] = $email;
				$return['first_name'] = $where['first_name'];
				$return['kata_sandi'] = $where['kata_sandi'];
				$return['id_user'] = $id_user;
				$return['r_key'] = $r_key;
				$return['key'] = 0;
				return $return;
			}
		}else if(!empty($cek)){
			$cek_status = $this->db->query("SELECT status FROM tbl_user WHERE email='$email'")->row_array()['status'];
			if ($cek_status == 0) {
				//key 2 =  email sudah ada
				$r_key = $this->M_login->randomKey();
				$return['key'] = 2;
				$return['r_key'] = $r_key;
				return $return;
			}else{
				//key 3 email ada dan status bukan nol
				$r_key = $this->M_login->randomKey();
				$return['key'] = 3;
				$return['r_key'] = $r_key;
				return $return;
			}
		}

		$return['da'] = $id_user;
		$return['status'] = false;
		$return['message'] = "failed";
		return $return;
	}

	function getUser($unix = null){
		if ($unix == null) {
			$res = $this->db->query("SELECT *FROM tbl_user")->result_array();
			return $res;
		}else{
			$res = $this->db->query("SELECT *FROM tbl_user WHERE id_user='$unix'")->row_array();
			return $res;
		}
	}

	function getAuth($r_key = null){
		if ($r_key == null) {
			$res = $this->db->query("SELECT * FROM gen_auth")->result_array();
			return $res;
		}else{
			$res = $this->db->query("SELECT * FROM gen_auth WHERE random_key='$r_key'")->row_array();
			return $res;
		}
	}

	function delete_session($table,$where){
		//id yang sekarang lagi login(session)
		$id_user = $this->session->userdata('id_user');
		$del = $this->db->simple_query("DELETE FROM temp_session WHERE id_user='$id_user'");
		if ($del) {
			return 1;
		}else{
			return $del;
		}
	}
	

}