var app = angular.module('cmAir', []);

app.controller('ModelController', ['$scope','$http', '$location', '$log','$timeout','$window', '$interval', function( $scope, $http, $location, $log, $timeout,$window,$interval){

	$scope.homelink = window.location.origin;
	$scope.datas = [];
	$scope.datasg = [];
	$scope.datur = [];
	$scope.daturg= [];
	$scope.dataf = [];
	$scope.ne = [];
	$scope.times = [];
	$scope.statusdata = [];
	$scope.hasil = 0;
	$scope.ghasil = 0;
	$scope.dates = "";
	$scope.awals = true;
	$scope.firste = false;
	$scope.resulte = false;
	$scope.loding = false;
	$scope.lodingnav = false;
   	$scope.reachedLast = false;
	$scope.NextpageNumber = 0;
	$scope.nes = [];
	$scope.notiflast = 0;

	$scope.loadings = function () {
		// body...
		console.log("Lodings");
		$scope.loding = true;		
	}

	$scope.loadingsclose = function () {
		// body...
		console.log("Lodings False");
		$scope.loding = false;		
	}

	$scope.loginToApi = function (response){
		timer = $timeout(function () {
			var username = $("#username").val();
			var password = $("#password").val();

			var param = {};
	        param.username = username;
	        param.password = password;
	        $http.post("https://air.classmiles.com/Rest/channel_login", param).then(function successCallback(response) {
	        	var data = response;
	        	console.warn('INI DATA '+data);
	        });
		}, 1000);
	}

	$scope.showIt = function (response) {
		timer = $timeout(function () {

			var subject = $("#subjecton").val();
			var date = $("#dateon").val();
			var time = $("#timeee").val();
			var duration = $('#durationon').val();
			var user_utc = new Date().getTimezoneOffset();
        	user_utc = -1 * user_utc;        	

			// console.log('https://beta.dev.meetaza.com/classmiles/ajaxer/ajax_get_ondemand?subject_id='+subject+'&date='+date+'&time='+time+'');
			// $http.get($scope.homelink + '/ajaxer/ajax_get_ondemand?subject_id='+subject+'&date='+date+'&time='+time+'&duration='+duration+'')
			$http.get($scope.homelink + '/ajaxer/ajax_get_ondemand?subject_id='+subject+'&dtrq='+date+'&tmrq='+time+'&drrq='+duration+'&user_utc='+user_utc)
			.success(function(response) {
				// body...
				$scope.statusdata = response.status;
				if (response.status) {					
					console.log(response.data.length);
					if (response.data.length > 0) {
						$scope.firste = false;
						$scope.resulte = true;
						$scope.loding = false;
						$scope.datas = response.data;
						$scope.times = time;
						$scope.subjects = response.subject_name;
						$scope.durations = duration;
						$scope.dates = date;
						$scope.hasil = response.data.length;						
					}
				}
				else{
					$scope.firste = true;
					$scope.resulte = false;
					$scope.loding = false;
					$scope.datasg = [];
					$scope.ghasil = response.data.length;					
				}
  				console.log(response);
  			})
			.error(function (data, status, header, config) {
				$scope.status = status;
				if (status != 200) {

					$scope.firste = true;
					$scope.resulte = false;
					$scope.loding = false;
				}
			});

		}, 1000);
	};

	$scope.showItGroup = function (response) {
		timer = $timeout(function () {

			var subject = $("#subjectong").val();
			var date = $("#dateong").val();
			var time = $("#timeeee").val();
			var duration = $('#durationong').val();
			var user_utc = new Date().getTimezoneOffset();
        	user_utc = -1 * user_utc;

			// console.log($scope.homelink + '/ajaxer/ajax_get_ondemand?subject_id='+subject+'&date='+date+'&time='+time+'&duration='+duration+'');
			$http.get($scope.homelink + '/ajaxer/ajax_get_ondemand_group?subject_id='+subject+'&dtrq='+date+'&tmrq='+time+'&drrq='+duration+'&user_utc='+user_utc)
			.success(function(response) {
				// body...
				$scope.statusdata = response.status;
				if (response.status) {
					if (response.data.length > 0) {
						$scope.firste = false;
						$scope.resulte = true;
						$scope.loding = false;
						$scope.datasg = response.data;
						$scope.times = time;
						$scope.subjects = response.subject_name;
						$scope.dates = date;
						$scope.durations = duration;
						$scope.ghasil = response.data.length;
					} else {
						$scope.firste = true;
						$scope.resulte = false;
						$scope.loding = false;
						$scope.datasg = [];
						$scope.ghasil = response.data.length;
					}
				}else{
					$scope.firste = true;
					$scope.resulte = false;
					$scope.loding = false;
					$scope.datasg = [];
					$scope.ghasil = response.data.length;
				}
  				console.log(response);
  			})
			.error(function (data, status, header, config) {
				$scope.status = status;
				if (status != 200) {

					$scope.firste = true;
					$scope.resulte = false;
					$scope.loding = false;
				}
			});

		}, 1000);
	};


	$scope.gettutorapproval = function () {
		// body...
		var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;
        console.log("UTC "+user_utc);
		$http.get($scope.homelink + '/ajaxer/ajax_get_approval_ondemand/'+user_utc).success(function(response) {
			if (response.status) {
				if (response.data.length > 0) {
					$scope.datur = response.data;
					$scope.firste = false;
				} else {
					$scope.datur = [];
					$scope.firste = true;
				}
			}
			console.log(response);
		});
	}

	$scope.gettutorapproval_group = function () {
		// body...
		console.warn("Masuk Group");
		var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;
        console.log("UTC "+user_utc);

		$http.get($scope.homelink + '/ajaxer/ajax_get_approval_ondemand_group/'+user_utc).success(function(response) {
			if (response.status) {
				if (response.data.length > 0) {
					$scope.daturg = response.data;
					console.log($scope.daturg);
					$scope.firste = false;
				} else {
					$scope.daturg = [];
					$scope.firste = true;
				}
			}
			console.log(response);
		});
	}

	$(document.body).on('click', '.btn-confirm' ,function(e){
		var request_id = $(this).attr('request_id');
		var template = $(this).attr('template');		
		$('.btn-choose-demand').attr('demand-link',$scope.homelink + '/process/approve_demand?request_id='+request_id);
		$('#modalconfrim').modal('show');
	});

	$(document.body).on('click', '.btn-reject' ,function(e){
		var request_id = $(this).attr('request_id');
		$('.btn-reject-demand').attr('demand-link',$scope.homelink + '/process/reject_demand?request_id='+request_id);
		$('#modalreject').modal('show');
	});

	$(document.body).on('click', '.btn-confirm-group' ,function(e){
		var request_id = $(this).attr('request_id');

		$('.btn-choose-demand-group').attr('demand-link-group',$scope.homelink + '/process/approve_demand_group?request_id='+request_id);
		$('#modalconfrim').modal('show');
	});

	$(document.body).on('click', '.btn-reject-group' ,function(e){
		var request_id = $(this).attr('request_id');

		$('.btn-reject-demand-group').attr('demand-link-group',$scope.homelink + '/process/reject_demand_group?request_id='+request_id);
		$('#modalreject').modal('show');
	});

	$(document.body).on('click', '.btn-choose-demand' ,function(e){
		var link = $(this).attr('demand-link');
		var template = $('#template').val();
		var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;
		var linksebenarnya = link+'&template='+template+'&user_utc='+user_utc;	
		$('#modalconfrim').modal('hide');
		$scope.loadings();	
		if (template!=""||link!="") {
			$.get(linksebenarnya,function(state_data){
	            // location.reload();
	            console.log(state_data);
	            state_data = JSON.parse(state_data);
	            
	            if(state_data['status'] == 1){
	            	notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',state_data['message']);
	            	$scope.loadingsclose();
	            	$scope.gettutorapproval();	
	            }
	        });
        }
        else
        {
        	notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', 'Harap Pilih Template!!!');
        }
	});

	$(document.body).on('click', '.btn-choose-demand-group' ,function(e){
		var link = $(this).attr('demand-link-group');
		var template = $('#templategroup').val();
		var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;
		var linksebenarnya = link+'&template='+template+'&user_utc='+user_utc;
		$('#modalconfrim').modal('hide');
		$scope.loadings();	
		$.get(linksebenarnya,function(state_data){
            // location.reload();
            console.warn("inidata"+state_data);
            state_data = JSON.parse(state_data);            
            if(state_data['status'] == 1){
            	notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',state_data['message']);
            	$scope.loadingsclose();
            	$scope.gettutorapproval_group();	
            }
            
        });
	});

	$(document.body).on('click', '.btn-reject-demand' ,function(e){
		var link = $(this).attr('demand-link');
		$.get(link,function(state_data){
			console.log(state_data);
			state_data = JSON.parse(state_data);
			$('#modalreject').modal('hide');
			console.warn("INI STATUSNYA "+state_data['status'])
			if(state_data['status'] == 1){
				notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', 'Berhasil menolak kelas private tambahan.');
				// setTimeout(function(){
			 	//      	location.reload();
			 	//  	},3000);				
			 	$scope.gettutorapproval();
			}
			else
			{
				notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', 'Gagal menolak kelas private tambahan. Mohon lakukan kembali.');
				// setTimeout(function(){
			 	//      	location.reload();
			 	//  	},3000);
			 	$scope.gettutorapproval();
			}
		});
	});

	$(document.body).on('click', '.btn-reject-demand-group' ,function(e){
		$.get($(this).attr('demand-link-group'),function(state_data){
			state_data = JSON.parse(state_data);
			$('#modalreject').modal('hide');
			if(state_data['status'] == 1){
				notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', 'Berhasil menolak kelas group tambahan.');
				// setTimeout(function(){
			 	//      	location.reload();
			 	//  	},3000);				
			 	$scope.gettutorapproval_group();
			}
			else
			{
				notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', 'Gagal menolak kelas group tambahan. Mohon lakukan kembali.');
				// setTimeout(function(){
			 	//      	location.reload();
			 	//  	},3000);
			 	$scope.gettutorapproval_group();
			}
		});
	});

	// NOTIF
	$(document.body).on('click', '#notif' ,function(e){
		var id_user = $(this).attr('id_user');
		$scope.lodingnav = true;	

		getNotif();
	});

	$(document.body).on('click', '.readsa' ,function(e){
		var id_user = $(this).attr('id_user');
		console.log(id_user + "Click");
		$http.get($scope.homelink + '/ajaxer/ajax_notifread?id_user_requester=' + id_user).success(function(response) {
			if (response.status) {
				console.log("masuk ke getnotif");
				$http.get($scope.homelink + '/ajaxer/ajax_notif?id_user=' + id_user).success(function(response) {
					if (response.status) {
						if (response.data.length > 0) {
							$scope.dataf = response.data;
							$scope.NextpageNumber = 0;
                			$scope.reachedLast = false;
						} else {
							$scope.dataf = [];
						}
					}
					console.log(response);
				});
			}
			console.log(response);
		});
	});

	$scope.getnotif = function() {
		console.log("Fungsi getnotif");
		var id_user = $('#notif').attr('id_user');
				$http.get($scope.homelink + '/ajaxer/ajax_count?id_user=' + id_user).success(function(response) {
					if (response.status) {
						if (response.data.length > 0) {
							$scope.nes = [];
							$scope.nes = response.data;
						} else {
							$scope.nes = [];
						}
					}
				
					// console.log(response);
				});
				
				console.log("masuk ke getnotif");
				$http.get($scope.homelink + '/ajaxer/ajax_notif?id_user=' + id_user).success(function(response) {
					if (response.status) {
						if (response.data.length > 0) {
							$scope.dataf = response.data;
							$scope.NextpageNumber = 0;
                			$scope.reachedLast = false;
						} else {
							$scope.dataf = [];
						}
					}
					console.log(response);
				});
		$interval(getNotif, 2500);
	}

	$(document.body).on('click', '.tif' ,function(e){
		var id_user = $('#notif').attr('id_user');
		var notif_id = $(this).attr('notif_id');
		var haref = $(this).attr('hrefa');
		$http.get($scope.homelink + '/ajaxer/ajax_notifreadone?id_user=' + id_user + '&notif_id=' + notif_id).success(function(response) {
			// console.log($scope.homelink + '/ajaxer/ajax_notifreadone?id_user=' + id_user + '&notif_id=' + notif_id);
			if (response.status) {
				// $scope.loadData();
				$window.location.href = haref;
			}
			// console.log(response);
		});
	});
	
	function getNotif() {
		$scope.lodingnav = false;
	    console.log("Interval occurred getNotif up");
			var id_user = $('#notif').attr('id_user');
			$http.get($scope.homelink + '/ajaxer/ajax_count?id_user=' + id_user).success(function(response) {
				if (response.status) {
					if (response.data.length > 0) {
						if ($scope.notiflast != response.data) {
							$scope.notiflast = response.data;
							console.log($scope.notiflast);
							$scope.url = $scope.homelink + '/ajaxer/ajax_notif?id_user=' + id_user + '&page=0'; 
				        	// console.log($scope.url);
				         	$http.get($scope.url).success(function(response) {
					         	// console.log(response);
					            
								if (response.status) {
	
									if (response.data.length > 0) {
										$scope.dataf = [];
					            		$scope.dataf = response.data;
					            		if (response.data.length == 5) {
					            			$scope.NextpageNumber = response.nextpage;
                							$scope.reachedLast = false;
					            		}
									} else {
										$scope.dataf = [];
									}
								}
				         	});
						}
						$scope.nes = [];
						$scope.nes = response.data;
					} else {
						$scope.nes = [];
					}
				}
			// console.log("getNotif "+response);
		});
	}

	$scope.loadData = function() {
		console.log("Loading loadData");
		$scope.lodingnav = true;	
		
		var id_user = $('#notif').attr('id_user');

		if($scope.reachedLast) {

			console.log("Loading Here");
			$scope.lodingnav = false;	
            return false;
        }

     	$scope.url = $scope.homelink + '/ajaxer/ajax_notif?id_user=' + id_user + '&page=' + $scope.NextpageNumber; 
    	// console.log($scope.url);
     	$http.get($scope.url).success(function(response) {
         	console.log(response);
            var currentpage = $scope.NextpageNumber;//taking current pagenumber
            var lastpage = response.lastpage; //setting last page number
            $scope.NextpageNumber = response.nextpage;

            if((currentpage == $scope.NextpageNumber)){

				$scope.lodingnav = false;	
                return false;
            }

            if((currentpage == lastpage)){
                //reached at the bottom
                $scope.reachedLast = true;
				$scope.lodingnav = false;	
                $scope.loadmore = "Reached at bottom";
            }

			if (response.status) {

				$scope.lodingnav = false;	
				if (response.data.length > 0) {
            		$scope.dataf = $scope.dataf.concat(response.data);
				} else {
					$scope.dataf = [];
				}
			}
     	});
	}	

}]);




// we create a simple directive to modify behavior of <ul>
app.directive("whenScrolled", function(){
  return{
    
    restrict: 'A',
    link: function(scope, elem, attrs){
    
      // we get a list of elements of size 1 and need the first element
      raw = elem[0];
    
      // we load more elements when scrolled past a limit
      elem.bind("scroll", function(){
        if(raw.scrollTop+raw.offsetHeight+5 >= raw.scrollHeight){
          scope.loading = true;
          
        // we can give any function which loads more elements into the list
          scope.$apply(attrs.whenScrolled);
        }
      });
    }
  }
});