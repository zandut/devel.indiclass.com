$(document).ready(function(){
 	
 	setTimeout(function(){
 	var session = window.localStorage.getItem('session');
 	var usertypee = window.localStorage.getItem('usertype');
 	var sttour = window.localStorage.getItem('st_tour');
 	var iduser = window.localStorage.getItem('iduser');
 	// alert(sttour);
 	// alert(usertypee);

 	if (usertypee == "student") 
 	{
	    var tour = new Tour({
	        steps: [
	        {
	          element: "#tour1",
	          title: "<center><h5 class='c-white'>Raise Hand </h5></center>",
	          content: "<h5>Tekan tombol ini untuk mengajukan pertanyaan atau berdiskusi dengan tutor. Tunggu beberapa saat sampai tutor merespon. </h5>",
	          placement: "bottom"
	        },
	        {
	          element: "#tour2",
	          title: "<center><h5 class='c-white'>Screen Set</h5></center>",
	          content: "<h5>Konfigurasi tampilan screenshare, video dan whiteboard.</h5>",
	          placement: "bottom"          
	        },
	        {
	          element: "#tour3",
	          title: "<center><h5 class='c-white'>Live Chat</h5></center>",
	          content: "<h5>Tekan tombol ini untuk chatting dengan tutor.</h5>",
	          placement: "bottom"          
	        },
	        // {
	        //   element: "#tour4",
	        //   title: "<center><h5 class='c-white'>Logout</h5></center>",
	        //   content: "<h5>Tour adalah blablabla </h5>",
	        //   placement: "bottom"          
	        // },
	        // {
	        //   element: "#tour5",
	        //   title: "<center><h5 class='c-white'>Logout</h5></center>",
	        //   content: "<h5>Logout adalah blablabla </h5>",
	        //   placement: "bottom"          
	        // },        
	        {
	          element: "#myvideostudent",
	          title: "<center><h5 class='c-white'>Video Tutor</h5></center>",
	          content: "<h5>Video Tampilan tutor</h5>",
	          placement: "top"          
	        },
	        {
	          element: "#wbcard",
	          title: "<center><h5 class='c-white'>Whiteboard</h5></center>",
	          content: "<h5>Whiteboard untuk tutor menulis layaknya papan tulis pada umumnya.</h5>",
	          placement: "top"          
	        },
	        {
	          element: "#screensharestudent",
	          title: "<center><h5 class='c-white'>Screen Share</h5></center>",
	          content: "<h5>Tampilan screen share dari tutor.</h5>",
	          placement: "top"          
	        }        
	        ],
	        // backdrop: true,
	        animation: true,
	        template: 
	        "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title bgm-blue c-white'></h3><div class='popover-content'></div><hr><div class='popover-navigation' style='margin:0px; padding:0px;'><div class='col-md-12'><div class='col-md-6'><button class='btn btn-default btn-block btn-sm' data-role='prev'>« Prev</button></div><div class='col-md-6'><button class='btn btn-default btn-block btn-sm' data-role='next'>Next »</button></div><div class='col-md-12' style='margin-top:2%; margin-bottom:4%;'><button class='btn btn-default btn-block btn-sm' data-role='end'>End tour</button></div></div></div></div>"
	        
	    });    

	    var cek = 1;    
	    if (sttour == 0) {
	    	$('#tourlagi').trigger('click');
	    	
	    	$.ajax({
				url: 'https://classmiles.com/Rest/up_tour',
				type: 'POST',
				data: {
					st_tour: cektutor,
					iduser: iduser
				},
				success: function(data)
				{
					if (data['code'] == '200') {
						window.localStorage.setItem('st_tour', cek);
						setTimeout(function(){
							tour.init();
					      	tour.end();
					    	tour.restart();

						},5000);
					}
				}
			});	    	
	    		
	    }
	        
	    $("#tourlagi").click(function(){    	
	    	tour.init();
	      	tour.end();
	    	tour.restart();	
	    });
    }
    else if(usertypee == "tutor")
    {
    	var tour = new Tour({
	        steps: [
	        {
	          element: "#tourtutor1",
	          title: "<center><h5 class='c-white'>Raise Hand </h5></center>",
	          content: "<h5>Raise Hand adalah blablabla </h5>",
	          placement: "bottom"
	        },
	        {
	          element: "#tourtutor2",
	          title: "<center><h5 class='c-white'>Break</h5></center>",
	          content: "<h5>Break adalah blablabla </h5>",
	          placement: "bottom"          
	        },
	        {
	          element: "#tourtutor3",
	          title: "<center><h5 class='c-white'>Video Off/On</h5></center>",
	          content: "<h5>Video Off/On adalah blablabla </h5>",
	          placement: "bottom"          
	        },
	        {
	          element: "#tourtutor4",
	          title: "<center><h5 class='c-white'>Sound Off/On</h5></center>",
	          content: "<h5>Sound Off/On adalah blablabla </h5>",
	          placement: "bottom"          
	        },        
	        {
	          element: "#tourtutor5",
	          title: "<center><h5 class='c-white'>Open Screen Share</h5></center>",
	          content: "<h5>Open Screen Share adalah blablabla</h5>",
	          placement: "bottom"          
	        },
	        {
	          element: "#tourtutor6",
	          title: "<center><h5 class='c-white'>Chat</h5></center>",
	          content: "<h5>Chat adalah blablabla</h5>",
	          placement: "bottom"          
	        },
	        {
	          element: "#tourtutor7",
	          title: "<center><h5 class='c-white'>Setting</h5></center>",
	          content: "<h5>Setting adalah blablabla</h5>",
	          placement: "bottom"          
	        },
	        {
	          element: "#tourtutor8",
	          title: "<center><h5 class='c-white'>Tour</h5></center>",
	          content: "<h5>Tour adalah blablabla</h5>",
	          placement: "bottom"          
	        },
	        {
	          element: "#tourtutor9",
	          title: "<center><h5 class='c-white'>Logout</h5></center>",
	          content: "<h5>Logout adalah blablabla</h5>",
	          placement: "bottom"          
	        },
	        {
	          element: "#wbcardd",
	          title: "<center><h5 class='c-white'>Whiteboard</h5></center>",
	          content: "<h5>Whiteboard adalah blablabla</h5>",
	          placement: "top"          
	        },
	        {
	          element: "#myvideo",
	          title: "<center><h5 class='c-white'>Video Stream</h5></center>",
	          content: "<h5>Video Stream adalah blablabla</h5>",
	          placement: "top"          
	        }        
	        ],
	        // backdrop: true,
	        animation: true,
	        template: 
	        "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title bgm-blue c-white' style='height:60px;'></h3><div class='popover-content'></div><hr><div class='popover-navigation' style='margin:0px; padding:0px;'><div class='col-md-12'><div class='col-md-6'><button class='btn btn-default btn-block btn-sm' data-role='prev'>« Prev</button></div><div class='col-md-6'><button class='btn btn-default btn-block btn-sm' data-role='next'>Next »</button></div><div class='col-md-12' style='margin-top:2%; margin-bottom:4%;'><button class='btn btn-default btn-block btn-sm' data-role='end'>End tour</button></div></div></div></div>"
	        
	    });    

	    var cektutor = 1;    
	    if (sttour == 0) {
	    	$('#tourlagi').trigger('click');
	    	
	    	$.ajax({
				url: 'https://classmiles.com/Rest/up_tour',
				type: 'POST',
				data: {
					st_tour: cektutor,
					iduser: iduser
				},
				success: function(data)
				{
					if (data['code'] == '200') {
						window.localStorage.setItem('st_tour', cektutor);
						setTimeout(function(){
							tour.init();
					      	tour.end();
					    	tour.restart();

						},5000);
					}
				}
			});	    	
	    		
	    }
	        
	    $("#tourlagii").click(function(){    	
	    	tour.init();
	      	tour.end();
	    	tour.restart();	
	    });
    }
},10000);
 
});