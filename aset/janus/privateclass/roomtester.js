// var server = "wss://c2.classmiles.com:8989";
var janus = null;
var sfutest = null;
var opaqueId = "videoroomtest-"+Janus.randomString(12);

var started = false;

var myusername = null;
var myid = null;
var mystream = null;
// We use this other ID just to map our subscriptions to us
var mypvtid = null;
var parameter = true;

var feeds = [];
var bitrateTimer = [];
var sendrequest = null;
// var sendrequestjsep = null;
var mypvtid = null;
var listUserActive = null;
var listDevices = [];
var raisehandlist = {};

var idtutor = null;
var iduser  = null;
var displayname = null;
var tutorname = null;
var firstname = null;
var token = null;
var classid = null;
var usertype = null;
var classname = null;
var currentDeviceId = null;
var tutorin = null;
var st_tutor = 0;
var startTime = null;
var endTime = null;
var cektangan = 0;
var vartutor = 0;
var privateid = null;

//KHUSUS CHAT
var chatData = [];
var transactions = {};
var textroom = null;
var chatParticipants = {};
var chatReady = false;
var classidcp = "";
var idusercp = "";
var displaynamecp = "";
var archiveChats = [];

var d = new Date();
var month = d.getMonth()+1;
var day = d.getDate();

var feeds = [];
var bitrateTimer = [];
var iceServer = [];
var listVideo = null;
var iceServers = [];
// [
// 	{        
// 	"url": "turn:m1.classmiles.com",
// 	"credential": "tyh4g92chiwugahrj7hqwt8978sdiguf",
// 	"username": "c1397542364"
// 	},
// 	{
// 	"url": "stun:m1.classmiles.com",
// 	"credential": "tyh4g92chiwugahrj7hqwt8978sdiguf",
// 	"username": "c1397542364"
// 	}
// ];

var checkdate = ((''+day).length<2 ? '0' : '')+ day + '/' + ((''+month<2 ? '0' : '') +  month + '/'+ d.getFullYear());
var session = window.localStorage.getItem('session');
var server = null;
var link = "https://classmiles.com/";
var timer = 0;
var parameter = true;

var temp_break_state = -1;
var temp_play_pos = -1;
var break_state = 0; 
var play_pos = 0;
var page_Type = null; 
var wktu = 0;
var tempatsimpancek = null;
var st_break = 0;
var geo_location = [];
$(document).ready(function() {

	function getURLParameter(name) {
	  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
	}

	$.get("https://ipinfo.io/json", function (response) {
       geo_location = {city:response.city,region:response.region,country:response.country};              
  	}, "jsonp");

	

  	var lebarlayar = $(window).width();		

	if (lebarlayar < 900 ) {
    	page_Type = "android";
    	$(".tampilanweb").css('display','none');
        $("#header").css('display','none');    	
        $("#chat").css('display','none');
        $(".tampilanandro").css('display','block');
        var a = $(window).height();
	    var b = $(window).width(); //UNTUK HEIGHT
	    // var b = $(window).height() / 0.65;
	    var c = b * 0.5625;
	    var d = a - c;
	    var e = a / 0.5625; //UNTUK WIDTH LANDSCAPE
	    if (a > b * 0.5625) {                       
	        $("#player").css('height', c);
	        $("#player").css('width', b);

	        $(".papanwhiteboard").css('height','70vh');
	        $(".papanscreen").css('height','70vh');
	    }
	    else
	    {               
	        $("#player").css('width', e)
	        $("#player").css('height', a);
	    }
    }
    else
    {
    	var a = $(window).height();
	    var b = $(window).width(); //UNTUK HEIGHT
	    // var b = $(window).height() / 0.65;
	    var c = b * 0.5625;
	    var d = a - c;
	    var e = a / 0.5625; //UNTUK WIDTH LANDSCAPE
    	page_Type = "web";
        $(".tampilanandro").css('display','none');
        $(".tampilanweb").css('display','block');
        $("#chat").css('display','block');
        $("#header").css('display','block');      
    }

    setTimeout(function(){
		$("#loader").css('display', 'none');
		getpollingdata(parameter);
	},3000);

	setInterval(function(){
		$.ajax({
			url: 'https://classmiles.com/Rest/list_video',
			type: 'GET',
			data: {
				session: session
			},
			success: function(data)
			{	
				
				if (data['list'].length < 0) 
				{
					listVideo = data['list'][play_pos]['video_source'];
					// alert(listVideo);
				}
				else
				{
					listVideo = "../aset/video/aqua.mp4";
				}			
			}
		});		
		
		getpollingdata(parameter);
	},10000);

	setInterval(function(){
		if (play_pos == 3) 
		{
			st_tutor = 3;
			$("#tempatwaktu").css('display','block');
			wktu = 1;
			clearInterval(getpollingdata);
			console.warn("SUKSES CLEAR");
			generate();
		}
		if (cektangan == 1) {
			$("#tanganbiasa").css('display','none');
			$("#tanganmerah").css('display','block');
		}		
		else
		{
			$("#tanganmerah").css('display','none');
			$("#tanganbiasa").css('display','block');
		}
	},1000);

	var heightchat = $("#chat").height()-270;	
	$("#chatBoxScroll").height(heightchat);

	$("#pindah2").css('display','block');
	$("#pindah3").css('display','block');
	$("#pindah2").click(function(){		
		if ($("#vdio").height() != 100 && $("#whboard").height() != 100) {			
			$("#pindah2").css('display','none');
			$("#pindah1").css('display','block');
			$("#pindah3").css('display','block');
		}
	});
	$("#myvideostudent").click(function(){		
		if ($("#vdio").height() != 100 && $("#whboard").height() != 100) {			
			$("#pindah2").css('display','none');
			$("#pindah1").css('display','block');
			$("#pindah3").css('display','block');
		}
	});
	$("#pindah3").click(function(){		
		if ($("#sshare").height() != 100 && $("#whboard").height() != 100) {			
			$("#pindah3").css('display','none');
			$("#pindah2").css('display','block');
			$("#pindah1").css('display','block');
		}
	});
	$("#pindah1").click(function(){		
		if ($("#vdio").height() != 100 && $("#whboard").height() != 100) {			
			$("#pindah1").css('display','none');
			$("#pindah2").css('display','block');
			$("#pindah3").css('display','block');
		}
	});

	function getpollingdata(firsttime)
	{		
		$.ajax({
			url: 'https://classmiles.com/Rest/get_token',
			type: 'POST',
			data: {
				session: session
			},
			success: function(data)
			{							
				if (data['error']['code'] == '200' && data['response']['token'] != null) {
				// 	var a = JSON.stringify(geo_location); 
				// alert(a);			
					// =====SET DOM COMPONENT ======//
					if (server == null) 
					{
						server = "wss://"+data['response']['janus']+":8989";
					}
					console.warn(server+" inibro");
					usertype = data['response']['usertype'];					
					displayname = data['response']['first_name'];
					classname = data['response']['class_name'];
			        firstname = data['response']['first_name'];
			        tutorname = data['response']['nama_tutor'];

			        startTime = data['response']['start_time'];
			        endTime = data['response']['finish_time'];
			        console.warn(server);
			        // if (displayname != null) {
			        	$("#classname").text(classname);
				        $("#datenow").text(checkdate);
				        $("#nametutor").text(tutorname);
			        // }			        

			        if (data['response']['usertype'] == "tutor") {
	                	console.warn("TUTOR");
	                	$("#tempatstudent").css('display', 'none');
	                	$("#usertypestudent").css('display', 'none');	                	
	                	$("#tempattutor").css('display', 'block');
	                	$("#usertypetutor").css('display', 'block');	                	                	                	
	                }       
	                else if(data['response']['usertype'] == "admin")
	                {
	                	console.warn("admin");
	                	$("#tempattutor").css('display', 'none');
	                	$("#usertypetutor").css('display', 'none');
	                	$("#tempatstudent").css('display', 'block');
	                	$("#usertypestudent").css('display', 'block');	                	
	                }
	                // ===== !SET DOM COMPONENT ======//
	                if (archiveChats.length > 0 && data['response']['usertype'] == "tutor") {
						archiveChat(archiveChats);
						console.warn("Save Chats");
					}
					// alert(data['room_state']['break_state']);				
					if (data['room_state']['break_state'] == "" && data['room_state']['play_pos'] == "") 
					{
						console.warn("Tidak ada Break state");
					}

					break_state = data['room_state']['break_state'];
					console.warn(break_state);					
					play_pos = data['room_state']['play_pos'];
					console.warn(play_pos);	
					// $("#myvideo").attr('muted','true');
					token = data['response']['token'];
					privateid = 1;

	                classid = parseInt(data['response']['class_id']);	                
	                idtutor = parseInt(data['response']['id_tutor']);
	                iduser = parseInt(data['response']['id_user']);	
					// alert(vartutor);

					tempatsimpancek = $("#myvideostudent").attr('src');
					if (break_state == 1 ) {
						$("#myvideo").attr('muted','false');
						console.warn(temp_break_state);
						console.warn("IKLAN CUY");
												
						if (temp_break_state != break_state) {
							temp_break_state = break_state;

							$.ajax({
								url: 'https://classmiles.com/Rest/list_video',
								type: 'GET',
								data: {
									session: session
								},
								success: function(data)
								{													
									if (data['list'].length < 0) 
									{										
										if (vartutor == 0) 
										{
											listVideo = data['list'][play_pos]['video_source'];										
											console.warn("AAAAAAAAAAAAAAAAAAAA");
											if (usertype == "admin") {
												if (page_Type == "web") 
												{																
													$("#myvideostudent").attr('src',listVideo);
													$("#myvideostudent").attr('loop','true');
												}
												else
												{
													$("#player").attr('src',listVideo);
													$("#player").attr('loop','true');
												}
											}
											if (usertype == "tutor") {																
												$("#myvideo").attr('src',listVideo);
												$("#myvideo").attr('loop','true');
											}
										}
									}
									else
									{
										console.warn("TIDAK ADA VIDEO SOURCE");
										if (vartutor == 0) 
										{
											var listVideoo = "../aset/video/aqua.mp4";
											if (usertype == "admin") {
												if (page_Type == "web") 
												{	
													// $("#myvideostudent").attr('src','');												
													$("#myvideostudent").attr('src',listVideoo);
													$("#myvideostudent").attr('loop','true');
												}
												else
												{
													$("#player").attr('src',listVideoo);
													$("#player").attr('loop','true');
												}
											}
											else if(usertype == "tutor"){
												$("#myvideo").attr('src',listVideoo);
												$("#myvideo").attr('loop','true');
											}
										}
									}
								}
							});
						}
						return null;
																		
						// parameter = true;
						
					}
					else
					{					
						if (temp_break_state != break_state) {
							console.warn(break_state);
							console.warn("BBBBBBBBBBBBBBBB");
							temp_break_state = break_state;
							console.warn("UDAH MASUK LAGI");
							if (janus == null) {	                		                	

									tunner(parameter);
				                	// $("#screensharestudent").attr("src", "https://classmiles.com/screen_share/#access_session="+session);
				                	// $("#screensharestudenttt").attr("src", "https://classmiles.com/screen_share/#access_session="+session);
				                	// $("#screensharestudent").css("width", "100%");
				                	// $("#screensharestudent").css("height", "100%");
				                	// $("#screensharestudenttt").css("width", "100%");
				                	// $("#screensharestudenttt").css("height", "100%");	                	
				                	parameter = false;
				                	// $("#nametutor").text(displayname);	
				                	console.warn("SUDAH MASUK JANGAN MASUK LAGI");	
			                }
			                
						}
					}					

		        	console.warn("ADA DATA");		                	                	                		             	
	                				
		        } 
		        else if (data['error']['code'] == '504') {
		        	console.warn("MASUK ELSE IF 504");
					// setInterval(function(){
					// 	getpollingdata(parameter);
					// },2000);	
					// timer;
		        }
		        else if (data['error']['code'] == '404') {
		        	console.warn("MASUK ELSE IF 404");
					// setInterval(function(){
					// 	getpollingdata(parameter);
					// },2000);	
					destroyToken();
		        	// clearInterval(timer);
		        }   
		        else
		        {
		        	console.warn("SAATNYA DESTROY TOKEN");
		        	destroyToken();
		        	// clearInterval(timer);
		        }
		         
			}
		});
	}

	$("#clearinterval").click(function(){
		clearInterval(getpollingdata);
		$("#tempatwaktu").css('display','block');
		console.warn("SUKSES CLEAR");
		generate();
	});

	var seconds = 60; // Berapa detik waktu mundurnya
	function generate() { // Nama fungsi countdownnya
		if(getpollingdata) {
	        clearInterval(getpollingdata);
	    }
		console.warn("GENERATE");	
				
		var id;
		id = setInterval(function () {
		if (seconds < 1){
		clearInterval(id);
		wktu = 0;
		// Perintah yang akan dijalankan
		// apabila waktu sudah habis
		$("#tempatwaktu").css('display','none');
		destroyToken();

		}else {			
			document.getElementById('countdown').innerHTML = --seconds;
		}
		}, 1000);					
	}

	function tunner()
	{
		$.ajax({
			url: 'https://classmiles.com/Rest/turner_stuner',
			type: 'GET',
			data: {
				session: session
			},
			success: function(data)
			{				       
				var dataServer = data.response;
		        var responseIceServer = dataServer.list_stun.concat(dataServer.list_turn);
		        iceServers = [];

		        var config = {};
		        config.audioCodec = dataServer.audiocodec;
		        config.videoCodec = dataServer.videocodec;
		        config.maxBitRate = dataServer.maxbitrate;
		        config.resolution = dataServer.resolution;

		        window.localStorage.setItem("videoConfig", config);

		        if (config.videoCodec === 'lowres') {
		          // Small resolution, 4:3
		          config.height = 240;
		          config.width = 320;
		        } else if (config.videoCodec === 'lowres-16:9') {
		          // Small resolution, 16:9
		          config.height = 180;
		          config.width = 320;
		        } else if (config.videoCodec === 'hires' || config.videoCodec === 'hires-16:9') {
		          // High resolution is only 16:9
		          config.height = 720;
		          config.width = 1280;
		          if (navigator.mozGetUserMedia) {
		            var firefoxVer = parseInt(window.navigator.userAgent.match(/Firefox\/(.*)/)[1], 10);
		            if (firefoxVer < 38) {
		              config.height = 480;
		              config.width = 640;
		            }
		          }
		        } else if (config.videoCodec === 'stdres') {
		          // Normal resolution, 4:3
		          config.height = 480;
		          config.width = 640;
		        } else if (config.videoCodec === 'stdres-16:9') {
		          // Normal resolution, 16:9
		          config.height = 360;
		          config.width = 640;
		        } else {
		          config.height = 480;
		          config.width = 640;
		        }

		        window.localStorage.setItem('videoConfig', JSON.stringify(config));

		        for (var i = 0; i < responseIceServer.length; i++) {
		          var server = {};
		          server.urls = responseIceServer[i].url;
		          server.credential = responseIceServer[i].credential;
		          server.username = responseIceServer[i].username;
		          iceServers.push(server);

		        }
		        console.warn(iceServers);
		        start(iceServers);
			}
		});
	}

	function start(inidingin)
	{
		// Initialize the library (all console debuggers enabled)
		Janus.init({debug: "all", callback: function() {
			// Use a button to start the demo
						
				if(started)
					return;
				started = true;
				$(this).attr('disabled', true).unbind('click');
				if(!Janus.isWebrtcSupported()) {
					bootbox.alert("No WebRTC support... ");
					return;
				}
				// Create session
				janus = new Janus
				({
					server: server,
					token: token,
					iceServers: inidingin,
					success: function() {
						// Attach to video room test plugin
						attachPublisher();						
					},

					error: function(error) {
						Janus.error(error);
						bootbox.alert(error, function() {
							window.location.reload();
						});
					},
					destroyed: function() {
						window.location.reload();
					}
				});		
		}});
	}


function attachPublisher()
{
	janus.attach(
	{
		plugin: "janus.plugin.videoroom",
		opaqueId: opaqueId,
		success: function(pluginHandle) {
			$('#details').remove();
			sfutest = pluginHandle;
			sendrequest = pluginHandle;
			Janus.log("Plugin attached! (" + sfutest.getPlugin() + ", id=" + sfutest.getId() + ")");
			Janus.log("  -- This is a publisher/manager");
			// Prepare the username registration
			getListParticipants();                
			var timer2 = setInterval(function() {
		        getListParticipants();
		    }, 5000);									
			// var register = { "request": "join", "room": classid, "ptype": "publisher", "display": displayname, "id":iduser };
			// sfutest.send({"message": register});							
			createroom();
		},
		error: function(error) {
			Janus.error("  -- Error attaching plugin...", error);
			bootbox.alert("Error attaching plugin... " + error);
		},
		consentDialog: function(on) {
			Janus.debug("Consent dialog should be " + (on ? "on" : "off") + " now");			
		},
		mediaState: function(medium, on) {
			Janus.log("Janus " + (on ? "started" : "stopped") + " receiving our " + medium);
		},
		webrtcState: function(on) {
			Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
			$("#videolocal").parent().parent().unblock();
			$("#videoremote1").parent().parent().unblock();
		},
		onmessage: function(msg, jsep) {
			Janus.debug(" ::: Got a message (publisher) :::");
			Janus.debug(JSON.stringify(msg));
			var event = msg["videoroom"];
			Janus.debug("Event: " + event);
			if(event != undefined && event != null) {
				if(event === "joined") {
					// Publisher/manager created, negotiate WebRTC and attach to existing feeds, if any
					myid = msg["id"];
					mypvtid = msg["private_id"];
					Janus.log("Successfully joined room " + msg["room"] + " with ID " + myid);
					publishOwnFeed(true);
					// Any new feed to attach to?
					if(msg["publishers"] !== undefined && msg["publishers"] !== null) {
						var list = msg["publishers"];
						Janus.debug("Got a list of available publishers/feeds:");
						Janus.debug(list);
						for(var f in list) {
							var id = list[f]["id"];
							var display = list[f]["display"];
							Janus.debug("  >> [" + id + "] " + display);
							newRemoteFeed(id, display)
						}
					}
				} else if(event === "destroyed") {
					// The room has been destroyed
					Janus.warn("The room has been destroyed!");
					bootbox.alert("The room has been destroyed", function() {
						window.location.reload();
					});
				} else if(event === "event") {
					// Any new feed to attach to?
					if(msg["publishers"] !== undefined && msg["publishers"] !== null) {
						var list = msg["publishers"];
						Janus.debug("Got a list of available publishers/feeds:");
						Janus.debug(list);
						for(var f in list) {
							var id = list[f]["id"];
							var display = list[f]["display"];
							Janus.debug("  >> [" + id + "] " + display);
							newRemoteFeed(id, display)
						}
					} else if(msg["leaving"] !== undefined && msg["leaving"] !== null) {
						// One of the publishers has gone away?
						var leaving = msg["leaving"];
						Janus.log("Publisher left: " + leaving);
						var remoteFeed = null;
						for(var i=1; i<6; i++) {
							if(feeds[i] != null && feeds[i] != undefined && feeds[i].rfid == leaving) {
								remoteFeed = feeds[i];
								break;
							}
						}
						if (leaving == idtutor) {
							if(sfutest != null) {
								console.warn("HILANG");
								Janus.debug("Feed  (" + remoteFeed.rfdisplay + ") has left the room, detaching");
								$('#remote').empty().hide();
								$('#videoremote').empty();
								feeds[remoteFeed.rfindex] = null;
								// alert(usertype);
								// alert(usertype);			
								$.ajax({
									url: 'https://classmiles.com/Rest/unbreak',
									type: 'GET',
									data: {
										session: session
									},
									success: function(data)
									{							
										console.warn("BERHASIL UNBREAK");

										// var aa = $("#tempatx").attr("title").remove();
										// a.text("Play");
										st_tutor = 1;
										publishOwnFeed(true);

									}
								});					
								if (usertype == "admin") {
									if (listVideo != null) 
									{
										// videonih = data['list'][play_pos]['video_source'];
										// alert(dataa['list']['video_source']);
										// alert(page_Type + " publish leave");
										if (page_Type == "web") {
											$("#myvideostudent").attr('src',listVideo);
											// $("#myvideostudent").attr('muted','true');
											$("#myvideostudent").attr('loop','true');
										} else {
											$("#player").attr('src',listVideo);
											// $("#player").attr('muted','true');
											$("#player").attr('loop','true');
										}									
									}
									else
									{
										console.warn("TUTOR LEAVING");													
										$("#myvideostudent").attr('src','../aset/video/juki.mp4');
									}
								}
								
									return null;
								$("#myvideo").css('width','90%');
							}
						}
						if(remoteFeed != null) {
							Janus.debug("Feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") has left the room, detaching");
							$('#remote'+remoteFeed.rfindex).empty().hide();
							$('#videoremote'+remoteFeed.rfindex).empty();
							feeds[remoteFeed.rfindex] = null;
							remoteFeed.detach();
						}
					} else if(msg["unpublished"] !== undefined && msg["unpublished"] !== null) {
						// One of the publishers has unpublished?
						var unpublished = msg["unpublished"];
						Janus.log("Publisher left: " + unpublished);
						if(unpublished === 'ok') {
							// That's us
							sfutest.hangup();
							return;
						}
						var remoteFeed = null;
						for(var i=1; i<6; i++) {
							if(feeds[i] != null && feeds[i] != undefined && feeds[i].rfid == unpublished) {
								remoteFeed = feeds[i];
								break;
							}
						}
						if(remoteFeed != null) {
							Janus.debug("Feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") has left the room, detaching");
							$('#remote'+remoteFeed.rfindex).empty().hide();
							$('#videoremote'+remoteFeed.rfindex).empty();
							feeds[remoteFeed.rfindex] = null;
							remoteFeed.detach();
						}
					} else if(msg["error"] !== undefined && msg["error"] !== null) {
						bootbox.alert(msg["error"]);
					}
				}
			}
			if(jsep !== undefined && jsep !== null) {
				Janus.debug("Handling SDP as well...");
				Janus.debug(jsep);
				sfutest.handleRemoteJsep({jsep: jsep});
			}
		},
		onlocalstream: function(stream) {
			Janus.debug(" ::: Got a local stream :::");
			mystream = stream;
			Janus.debug(JSON.stringify(stream));
				if (usertype == "admin") {
					Janus.attachMediaStream($('#myvideo').get(0), stream);
					// $("myvideo").attr('muted', 'true');
				} else {
					Janus.attachMediaStream($('#myvideot').get(0), stream);					
				}
				var videoTracks = stream.getVideoTracks();
				if(videoTracks === null || videoTracks === undefined || videoTracks.length === 0) {
					// No webcam
					$('#myvideo').hide();
					$('#videolocal').append(
						'<div class="no-video-container">' +
							'<i class="fa fa-video-camera fa-5 no-video-icon" style="height: 100%;"></i>' +
							'<span class="no-video-text" style="font-size: 16px;">No webcam available</span>' +
						'</div>');
				}
		},
		onremotestream: function(stream) {
			// The publisher stream is sendonly, we don't expect anything here
		},
		oncleanup: function() {
			Janus.log(" ::: Got a cleanup notification: we are unpublished now :::");
			mystream = null;
			$('#videolocal').html('<button id="publish" class="btn btn-primary">Publish</button>');
			$('#publish').click(function() { publishOwnFeed(true); });
			$("#videolocal").parent().parent().unblock();
		}
	});
}

function createroom()
	{
		console.warn("CREATEROOM");
		var jsonRequest = {
          "request": "create",
          "room": classid,
          "ptype": "publisher",
          "description": "",
          "publishers": 2, //hanya 1 orang sebagai publisher
          "bitrate": 256000,
          "audiocodec": "opus",
          "videocodec": "vp9",
          "record": false,
          // "rec_dir": "/home/videos/" + userData.roomNumber,
          "is_private": true
        };	
        sfutest.send(
        { 
        	"message": jsonRequest,
        	success: function(resp){
			console.warn("Berhasil Create ",  resp);
        		joinroomNumber();        	
		}	
        });
	}

	function joinroomNumber()
	{
		var jsonRequest = {
          "request": "join",
          "room": classid,
          "ptype": "publisher",
          "display": displayname,
          "id": iduser          
        };
        sfutest.send({
        	"message": jsonRequest
        });
	}

function publishOwnFeed(deviceId) {
	// Publish our stream
	$('#publish').attr('disabled', true).unbind('click');
	// sfutest.createOffer(
	// {
	// 	// Add data:true here if you want to publish datachannels as well
	// 	media: { audioRecv: false, videoRecv: false, audioSend: useAudio, videoSend: true,  },	// Publishers are sendonly
	// 	success: function(jsep) {
	// 		Janus.debug("Got publisher SDP!");
	// 		Janus.debug(jsep);
	// 		var publish = { "request": "configure", "audio": useAudio, "video": true };
	// 		sfutest.send({"message": publish, "jsep": jsep});
	// 	},
	// 	error: function(error) {
	// 		Janus.error("WebRTC error:", error);
	// 		if (useAudio) {
	// 			 publishOwnFeed(false);
	// 		} else {
	// 			bootbox.alert("WebRTC error... " + JSON.stringify(error));
	// 			$('#publish').removeAttr('disabled').click(function() { publishOwnFeed(true); });
	// 		}
	// 	}
	// });
	var videoConfig = JSON.parse(window.localStorage.getItem('videoConfig'));
        if (currentDeviceId != null) {
        	deviceId = currentDeviceId;
        }
        var jsonRequest = {};
        // console.warn("== DEVICES ==" + deviceId);
        if (deviceId) {
          jsonRequest = {
            "audioRecv": false,
            "videoRecv": false,
            "audioSend": true,
            "videoSend": true,
            "video":{"deviceId": deviceId, "width": "1024", "height": "702"},
            "data": false
          };
          // console.warn("== CHECK ==" + deviceId);
        } else {
          jsonRequest = {
            "audioRecv": false,
            "videoRecv": false,
            "audioSend": true,
            "videoSend": true,
            "video": videoConfig.defaultResolution,
            "data": false
          };
        }
        sfutest.createOffer
        ({
        	media: jsonRequest,
        	trickle: true,
        	success: function(jsep)
        	{
        		sendPublish(jsep);
        	},
        	error: function(error)
        	{
        		// console.log("error bego", error);
        	}
        });
}

function sendPublish(jsep)
	{
		var jsonRequest = {};
		jsonRequest.request = "configure";
		jsonRequest.audio = true;
		jsonRequest.video = true;
		jsonRequest.bitrate = 256000;
		jsonRequest.audiocodec = "opus";
		jsonRequest.videocodec = "vp9";
		sfutest.send({ "message": jsonRequest, "jsep": jsep});		
	}

function newRemoteFeed(id, display) {
	// A new feed has been published, create a new plugin handle and attach to it as a listener
	var remoteFeed = null;
	janus.attach(
		{
			plugin: "janus.plugin.videoroom",
			opaqueId: opaqueId,
			success: function(pluginHandle) {
				remoteFeed = pluginHandle;
				Janus.log("Plugin attached! (" + remoteFeed.getPlugin() + ", id=" + remoteFeed.getId() + ")");
				Janus.log("  -- This is a subscriber");
				// We wait for the plugin to send us an offer
				var listen = { "request": "join", "room": classid, "ptype": "listener", "feed": id, "private_id": mypvtid, "id":iduser };
				remoteFeed.send({"message": listen});
			},
			error: function(error) {
				Janus.error("  -- Error attaching plugin...", error);
				bootbox.alert("Error attaching plugin... " + error);
			},
			onmessage: function(msg, jsep) {
				Janus.debug(" ::: Got a message (listener) :::");
				Janus.debug(JSON.stringify(msg));
				var event = msg["videoroom"];
				Janus.debug("Event: " + event);
				if(event != undefined && event != null) {
					if(event === "attached") {
						// Subscriber created and attached
						for(var i=1;i<6;i++) {
							if(feeds[i] === undefined || feeds[i] === null) {
								feeds[i] = remoteFeed;
								remoteFeed.rfindex = i;
								break;
							}
						}
						remoteFeed.rfid = msg["id"];
						remoteFeed.rfdisplay = msg["display"];
						// if(remoteFeed.spinner === undefined || remoteFeed.spinner === null) {
						// 	var target = document.getElementById('videoremote'+remoteFeed.rfindex);
						// 	remoteFeed.spinner = new Spinner({top:100}).spin(target);
						// } else {
						// 	remoteFeed.spinner.spin();
						// }
						Janus.log("Successfully attached to feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") in room " + msg["room"]);
						// $('#remote'+remoteFeed.rfindex).removeClass('hide').html(remoteFeed.rfdisplay).show();
					} else if(msg["error"] !== undefined && msg["error"] !== null) {
						bootbox.alert(msg["error"]);
					} else {
						// What has just happened?
					}
				}
				if(jsep !== undefined && jsep !== null) {
					Janus.debug("Handling SDP as well...");
					Janus.debug(jsep);
					// Answer and attach
					remoteFeed.createAnswer(
						{
							jsep: jsep,
							// Add data:true here if you want to subscribe to datachannels as well
							// (obviously only works if the publisher offered them in the first place)
							media: { audioSend: false, videoSend: false },	// We want recvonly audio/video
							success: function(jsep) {
								Janus.debug("Got SDP!");
								Janus.debug(jsep);
								var body = { "request": "start", "room": classid };
								remoteFeed.send({"message": body, "jsep": jsep});
							},
							error: function(error) {
								Janus.error("WebRTC error:", error);
								bootbox.alert("WebRTC error... " + JSON.stringify(error));
							}
						});
				}
			},
			webrtcState: function(on) {
				Janus.log("Janus says this WebRTC PeerConnection (feed #" + remoteFeed.rfindex + ") is " + (on ? "up" : "down") + " now");
			},
			onlocalstream: function(stream) {
				// The subscriber stream is recvonly, we don't expect anything here
			},
			onremotestream: function(stream) {
				Janus.debug("Remote feed #" + remoteFeed.rfindex);
			
					if (usertype == "admin") {
						Janus.attachMediaStream($('#remotevideo').get(0), stream);
						// $("remotevideo").attr('muted', 'true');
					} else {
						Janus.attachMediaStream($('#remotevideot').get(0), stream);
					}
				
			},
			oncleanup: function() {
				Janus.log(" ::: Got a cleanup notification (remote feed " + id + ") :::");
				if(remoteFeed.spinner !== undefined && remoteFeed.spinner !== null)
					remoteFeed.spinner.stop();
				remoteFeed.spinner = null;
				$('#waitingvideo'+remoteFeed.rfindex).remove();
				$('#curbitrate'+remoteFeed.rfindex).remove();
				$('#curres'+remoteFeed.rfindex).remove();
				if(bitrateTimer[remoteFeed.rfindex] !== null && bitrateTimer[remoteFeed.rfindex] !== null) 
					clearInterval(bitrateTimer[remoteFeed.rfindex]);
				bitrateTimer[remoteFeed.rfindex] = null;
			}
		});
}

function getListParticipants()
{
	var display="";
	var jsonRequest = {
      "request": "listparticipants",
      "room": classid,
      "description": "",
      "is_private": true
    };
	sendrequest.send({
		"message": jsonRequest,
		success: function (resp) {
		  	listUserActive = resp.participants;
		  	var a = JSON.stringify(listUserActive);		  	
		  	console.warn("INI LIST USERACTIOVE"+a);
		  	console.warn("DAFTAR 1 : "+listUserActive[0]['id']);

		  	if (usertype == "tutor") 
		  	{
		  		$("#nyalalist").click(function(){
		  			$("#kotakraisehand").empty();
				  	for (var f = 0; f < listUserActive.length; f++) {
						id_userraise = listUserActive[f]["id"];
						usernameraise = listUserActive[f]["display"];
						if (listUserActive[f]['id'] != idtutor) 
						{	
							// alert(listUserActive[f]['id']);
							if ($("#remotevideot").prop('muted')) {
								$("#kotakraisehand").append("<button class='d btn btn-info btn-block btn-sm' iduser='"+listUserActive[f]['id']+"' username='"+listUserActive[f]['display']+"' style='margin:10px; padding:5px; cursor:pointer;' ><i class='zmdi zmdi-phone'></i>     <label class='iniclick' >"+ listUserActive[f]['display'] +"</label></button>");										
							}
							else
							{
								$("#kotakraisehand").append("<button class='d btn btn-warning btn-block btn-sm' iduser='"+listUserActive[f]['id']+"' username='"+listUserActive[f]['display']+"' style='margin:10px; padding:5px; cursor:pointer;' ><i class='zmdi zmdi-phone'></i>     <label class='iniclick' >"+ listUserActive[f]['display'] +"</label></button>");									
							}						
							$("#kotakmenulist").modal("show");						
						}
					}
		  		});			  	
			}	

		  	$("#totalstudent").text(listUserActive.length+" people attending now");
		  	$("#totalstudentt").text(listUserActive.length+" people attending now");
			
			var as=$(listUserActive).filter(function (i,n){return n.publisher==='true'});
			// console.warn("AS = " + as);
			if (as.length > 0) {
				console.warn("Ada Tutor Mang");
				vartutor = 1;
				st_tutor = 1;
				st_break = 1;
				// $("#myvideostudent").attr("src",'');
			} else {
				console.warn("Tidak ada tutor Mang");
				vartutor = 0;
				st_break = 0;					
				if (st_tutor == 0) 
				{						
					if (listVideo == null) {
						if (page_Type == "web") {
							if (usertype == "admin") 
							{
								console.warn("MASUK WEB VIDEO IKLAN TIDAK KOSONG");
								$("#myvideostudent").attr("src",'../aset/video/cocacola1.mp4');									
								$("#myvideostudent").attr('loop','true');
								st_tutor = 1;
							}
						} else {
							console.warn("MASUK ANDROID VIDEO IKLAN TIDAK KOSONG");
							$("#player").attr('src',listVideo);
							// $("#player").attr('muted','true');
							$("#player").attr('loop','true');
						}
					}
					else
					{
						console.warn("MASUK LIST VIDEO IKLAN TIDAK KOSONG");
						if (usertype == "admin") 
						{
							$("#myvideostudent").attr("src",listVideo);
							st_tutor = 1;	
						}
					}
				}
				return null;

			}
			return null;
		  	
			$("#listuser").val(display);					
		}
	});
}

$('#kotakraisehand').on('click', '.d', function(){
	var iduser = $(this).attr("iduser");
	var username = $(this).attr("username");
    $("#kotakmenulist").modal("hide");
	
	if( $("#remotevideot").prop('muted') ) {
          $("#remotevideot").prop('muted', false);
    } else {
      $("#remotevideot").prop('muted', true);
    }
	// $("#remotevideot").attr('muted',false);
	// $("#remotevideo").attr('muted',false);
	// $("#myvideo").attr('muted',false);
	// $("#myvideot").attr('muted',false);

});

function getDevice() {
	console.warn("get Devices");
	Janus.listDevices(function (devices) {
        var deviceVideo = [];
        var deviceAudio = [];

        for (var i = 0; i < devices.length; i++) {
          if (devices[i].kind == "videoinput") {
            deviceVideo.push(devices[i]);
          } else if (devices[i].kind == "audioinput") {
            deviceAudio.push(devices[i]);
          }
        }
        $("#buttonlist").empty();
        listDevices.video = deviceVideo;
        listDevices.audio = deviceAudio;

    });
}

function unpublishOwnFeed() {
	// Unpublish our stream
	$('#unpublish').attr('disabled', true).unbind('click');
	var unpublish = { "request": "unpublish" };
	sfutest.send({"message": unpublish});
}

$("#devicedetect").click(function(){
	getListDevice();
});

function getListDevice() {
	var listdevicee = "";
	var iddevice = "";
	Janus.listDevices(function (devices) {
        var deviceVideo = [];
        var deviceAudio = [];

        for (var i = 0; i < devices.length; i++) {
          if (devices[i].kind == "videoinput") {
            deviceVideo.push(devices[i]);
          } else if (devices[i].kind == "audioinput") {
            deviceAudio.push(devices[i]);
          }
        }

        $("#buttonlist").empty();
        listDevices.video = deviceVideo;
        listDevices.audio = deviceAudio;

        noid = 1;
        for(var f in listDevices.video) {					
			listdevicee = listDevices.video[f]["label"];
			iddevice = listDevices.video[f]["deviceId"];

			$("#buttonlist").append("<button class='b btn btn-default btn-block btn-sm' style='margin:10px; padding:5px;' id='iddevice"+noid+"' ><label class='iniclick' id='listdevice"+noid+"' ></label></button>");			
			$("#iddevice"+noid+"").attr("cm-device",iddevice);
			$("#listdevice"+noid+"").text(listdevicee);
			noid++;
		}			
		
        $("#deviceConfigurationModal").modal("show");


    });
}

$('#buttonlist').on('click', '.b', function(){
	var id_device = $(this).attr("cm-device");
	changeDeviceConfiguration(id_device);
});

function changeDeviceConfiguration(deviceId) 
{
    console.warn("== CHECK ==" + deviceId);
	if (deviceId) {
       currentDeviceId = deviceId;
        if (usertype == "tutor") {
          	sfutest.hangup();
         	sfutest.detach();
          	sfutest = "READY";
          	attachPublisher();
        }
    }

    $("#deviceConfigurationModal").modal("hide");
}

$("#keluarcobaaa").click(function(){		
	$("#classkeluar").modal("show");
});

$("#keluarcobaa").click(function(){		
	$("#classkeluar").modal("show");
});

$("#iddevice1").click(function(){
	var id_device = $(this).attr("cm-device");
});

$('#mute').click(toggleMute);
function toggleMute() {
	var muted = sfutest.isAudioMuted();
	Janus.log((muted ? "Unmuting" : "Muting") + " local stream...");
	if(muted)
		sfutest.unmuteAudio();
	else
		sfutest.muteAudio();
	muted = sfutest.isAudioMuted();
	$('#mute').html(muted ? "<i class='tm-icon zmdi zmdi-volume-off' title='Audio Off'></i>" : "<i class='tm-icon zmdi zmdi-volume-up' title='Audio On'></i>");
}

$('#mute_murid').click(toggleMutee);
function toggleMutee() {
	var muted = sfutest.isAudioMuted();
	Janus.log((muted ? "Unmuting" : "Muting") + " local stream...");
	if(muted)
		sfutest.unmuteAudio();
	else
		sfutest.muteAudio();
	muted = sfutest.isAudioMuted();
	$('#mute_murid').html(muted ? "<i class='tm-icon zmdi zmdi-volume-off' title='Audio Off'></i>" : "<i class='tm-icon zmdi zmdi-volume-up' title='Audio On'></i>");
}

$('#unpublish').click(unpublishOwnFeed);
function unpublishOwnFeed() {
	// 
	var muted = sfutest.isVideoMuted();
	Janus.log((muted ? "Unmuting" : "Muting") + " local stream...");
	if(muted)
		sfutest.unmuteVideo();
	else
		sfutest.muteVideo();
	muted = sfutest.isVideoMuted();
	$('#unpublish').html(muted ? "<i class='tm-icon zmdi zmdi-eye-off' title='Video Off'></i>" : "<i class='tm-icon zmdi zmdi-eye' title='Video On'></i>");
}

$("#keluar").click(function(){
	destroyToken();
});	

$('#pause').click(matikanstreamsaya);
function matikanstreamsaya() {
	$("#pause").css('display','none');		
	// var aa = $("#tempatx").attr("title").remove();
	// a.text("Break");
	
	var unpublishh = { "request": "unpublish" };
	sfutest.send({"message": unpublishh});

	$.ajax({
		url: 'https://classmiles.com/Rest/break',
		type: 'GET',
		data: {
			session: session
		},
		success: function(data)
		{	
			st_break = 1;
			// if (play_pos == 1) {
				// $.ajax({
				// 	url: 'https://classmiles.com/Rest/list_video',
				// 	type: 'GET',
				// 	data: {
				// 		session: session
				// 	},
				// 	success: function(data)
				// 	{										
						console.warn("BERHASIL BREAK");
						// $("#myvideo").attr('muted','false');
						// $("#play").remove('attr','id');
						$("#playboongan").css('display','block');
						setTimeout(function(){
							$("#playboongan").css('display','none');
							$("#play").css('display','block');
						},10000); 
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Silahkan Tunggu 10 detik untuk memulai kelas kembali");
						if (vartutor == 0 && st_break == 1) 
						{						
							if (usertype == "admin") 
							{		
								$("#myvideostudent").attr('src', listVideo);
								$("#myvideostudent").attr('loop','true');
							}
						}
						else
						{

						}
						// $("#myvideostudent").attr('muted','false');
						// $("#myvideostudent").attr('src', '');
						// if (page_Type == "web") {
						// 	$("#myvideostudent").attr('src', listVideo);
							// $("#myvideostudent").attr('muted','true');
						// 	$("#myvideostudent").attr('loop','true');									
						// } else {
						// 	$("#player").attr('src', listVideo);
						// 	// $("#player").attr('muted','true');
						// 	$("#player").attr('loop','true');
						// }
				// 	}
				// });
			// }

		}
	});
	
}

$('#play').click(nyalakanstream);

function nyalakanstream()
{
	// alert('a');
	$("#play").css('display','none');
	$("#playboongan").css('display','none'); 
	$("#pause").css('display','block');
	$("#myvideo").attr("src",'');
	$("#myvideostudent").attr("src",'');
	$.ajax({
		url: 'https://classmiles.com/Rest/unbreak',
		type: 'GET',
		data: {
			session: session
		},
		success: function(data)
		{							
			st_break = 0;
			$("#myvideo").attr('muted','true');
			console.warn("BERHASIL UNBREAK");

			// var aa = $("#tempatx").attr("title").remove();
			// a.text("Play");
			publishOwnFeed(true);
		}
	});
	 
}

function destroyToken()
{
	window.localStorage.removeItem("session");
	window.localStorage.removeItem("type_page");
	$.ajax({
        url: 'https://classmiles.com/Rest/destroy_token', 
        type: 'POST',
        data : {
            token : token                    
        },
        success: function(data) {
            // janus.destroy();
            // sendrequestjsep.detach();
        	location.href='https://classmiles.com/first';
        }
    });
}

function destroyRaiseHand(){
	$.ajax({
		url: 'https://classmiles.com/Rest/raise_hand_destroy/',
		type: 'GET',
		data: {
			session: session
		},
		success: function(data)
		{
			console.warn("SUKSES destroy REQUEST");
		}
	});
}

$("#openscreenshare").click(function(){
	$(this).attr("href",'https://classmiles.com/screen_share/#access_session='+session);
});


});