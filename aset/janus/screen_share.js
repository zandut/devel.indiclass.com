// var janus = null; // Variable Untuk Janus Webrtc
// var user = {}; // { id, displayName, roomNumber }
// var listFeeds = [];
// var publisherID = null;
// var isAlreadyIn = false;
// var showButtonShare = false;
// var servers = "wss://c2.classmiles.com:8989";

//      function initializeJanus(iceServer) {
//       // Initialize the library (all console debuggers enabled)
//       Janus.init({
//         debug: false, callback: function () {

//           // Make sure the browser supports WebRTC
//           if (!Janus.isWebrtcSupported()) {
//             // bootbox.alert(LangConfigService.labelNoWebRTC);
//             return;
//           }

//           // Create session
//           janus = new Janus({
//             server: servers,
//             iceServers: iceServer,
//             token: user.token,
//             success: function () {
//               // Attach to video room test plugin
//               attachPublisher();
//             },
//             mediaState: function (medium, on) {
//               Janus.log("Janus " + (on ? "started" : "stopped") + " receiving our " + medium);
//             },
//             webrtcState: function (on) {
//               Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
//             },
//             error: function (error) {
//               Janus.log("Error Session : " + error);
//             },
//             destroyed: function () {
//               closeOrRedirect();
//             }
//           })
//         }
//       })
//     }


//     function getValidateData(firstTimeRun) {
//       var response_data = {};
//       var access_session = window.localStorage.getItem('session');
     
// 		$.ajax({
// 			url: 'https://classmiles.com/Rest/sshare_polling',
// 			type: 'POST',
// 			data: {
// 				session: access_session
// 			},
// 			success: function(data)
// 			{
// 				var data = response.data;
// 				response_data.class_id = parseInt(data.class_id);
// 				response_data.active = data.active == "0" ? false : true;
// 				response_data.token = data.token;
// 				response_data.id = parseInt(data.id_user);
// 				response_data.presentation_id = parseInt(data.presentation_id);
// 				response_data.id_tutor = parseInt(data.tutor_id);

// 				if (response_data.id == response_data.id_tutor) {
// 					response_data.userType = "tutor";
// 				} else {
// 					response_data.userType = "student";
// 				}

// 				user = response_data;
// 				if (firstTimeRun) {
// 					if (user.userType == "tutor") {
// 						AuthService.setDeactiveScreenSharing(access_session).then(function successCallback(response) {
// 							getIceServer(access_session);
// 						});
// 					} else {
// 						getIceServer(access_session);
// 					}
// 					polling();
// 				} else {
// 					if (janus != null && user.userType == "student") {
// 						if (!isAlreadyIn && user.active) {
// 							joinScreen(user.presentation_id);
// 							isAlreadyIn = true;
// 						} else {
// 							polling();
// 						}
// 					}
// 				}
// 		    }
//       	});
//     };

//     getIceServer function (access_session) {
//       AuthService.getIceServer(access_session).then(function successCallback(response) {
//         var dataServer = response.data.response;
//         var responseIceServer = dataServer.list_stun.concat(dataServer.list_turn);
//         var iceServers = [];

//         for (var i = 0; i < responseIceServer.length; i++) {
//           var server = {};
//           server.urls = responseIceServer[i].url;
//           server.credential = responseIceServer[i].credential;
//           server.username = responseIceServer[i].username;
//           iceServers.push(server);
//         }
//         initializeJanus(iceServers);
//       }, function errorCallback(response) {
//         destroySession();
//       });
//     };

//     polling function () {
//       $timeout(function () {
//         getValidateData(false);
//       }, 5000);
//     };

//     getValidateData(true);

//     checkStatusLog function () {
//       $interval(function () {
//         console.log("Polling Log On");
//       }, 1000);
//     };
//     checkStatusLog();

//     preScreenShare function () {

//       var access_session = window.localStorage.getItem('access_session');
//       // Make sure HTTPS is being used
//       if (window.location.protocol !== 'https:') {
//         bootbox.alert('Sharing your screen only works on HTTPS');
//         return;
//       }

//       if (!Janus.isExtensionEnabled()) {
//         bootbox.alert("Please install janus extention <b><a href='https://chrome.google.com/webstore/detail/janus-webrtc-screensharin/hapfgfdkleiggjjpfpenajgdnfckjpaj' target='_blank'>here</a></b>", function () {
//           window.location.reload();
//         });
//         return;
//       }

//       // Create a new room
//       user.capture = "screen";
//       if (navigator.mozGetUserMedia) {
//         // Firefox needs a different constraint for screen and window sharing
//         bootbox.dialog({
//           title: "Share whole screen or a window?",
//           message: "Firefox handles screensharing in a different way: are you going to share the whole screen, or would you rather pick a single window/application to share instead?",
//           buttons: {
//             screen: {
//               label: "Share screen",
//               className: "btn-primary",
//               callback: function () {
//                 user.capture = "screen";
//                 shareScreen();
//               }
//             },
//             window: {
//               label: "Pick a window",
//               className: "btn-success",
//               callback: function () {
//                 user.capture = "window";
//                 shareScreen();
//               }
//             }
//           },
//           onEscape: function () {

//           }
//         });
//       } else {
//         shareScreen();
//       }
//     };

//     shareScreen function () {
//       // Create a new room
//       user.description = "-";
//       var create = {
//         "request": "create",
//         "description": user.description,
//         "bitrate": 0,
//         "record": true,
//         "rec_dir": "/home/ssharing/" + user.class_id,
//         "publishers": 1
//       };

//       listFeeds[0].send({
//         "message": create, 
//         success: function (result) {
//           var event = result["videoroom"];
//           if (event != undefined && event != null) {
//             user.presentation_id = result["room"];
//             user.username = randomString(12);

//             var access_session = window.localStorage.getItem('access_session');
//             AuthService.setActiveScreenSharing(access_session, user.presentation_id).then(function successCallback(response) {
//               isAlreadyIn = true;
//               var register = { "request": "join", "id": user.id, "room": user.presentation_id, "ptype": "publisher", "display": user.username };
//               listFeeds[0].send({ "message": register });
//             });
//           }
//         }
//       });
//     };

//     joinScreen function (x) {
//       // Join an existing screen sharing session
//       user.presentation_id = parseInt(x);
//       user.username = randomString(12);
//       var register = { "request": "join", "room": user.presentation_id, "ptype": "publisher", "display": user.username };
//       listFeeds[0].send({ "message": register });
//     }

//     // Just an helper to generate random usernames
//     randomString function (len, charSet) {
//       charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//       var randomString = '';
//       for (var i = 0; i < len; i++) {
//         var randomPoz = Math.floor(Math.random() * charSet.length);
//         randomString += charSet.substring(randomPoz, randomPoz + 1);
//       }
//       return randomString;
//     };

//     exitPresentation function () {
//       var access_session = window.localStorage.getItem('access_session');
//       AuthService.setDeactiveScreenSharing(access_session).then(function successCallback(response) {
//         window.location.reload();
//       });
//     };

//     getButtonLabel function () {
//       var result;
//       showButtonShare ? result = "Create New Screen Share" : result = "Loading . . .";
//       return result;
//     };

//     checkIframeOrNot function (x) {
//       var access_session = window.localStorage.getItem('access_session');
//       if (self != top && x == "html") {
//         return "open in new tab (not in iframe) <b><a href='" + $location.protocol() + "://" + $location.host() + "/screen_share/#access_session=" + access_session + "' target='_blank'>here</a></b>";
//       } else if (self != top && x == "boolean") {
//         return false;
//       } else if (self == top && x == "html") {
//         return "";
//       } else {
//         return true
//       }
//     };  
//     // .directive('dynamic', function ($compile) {
//     //   return {
//     //     restrict: 'A',
//     //     replace: true,
//     //     link: function (scope, ele, attrs) {
//     //       scope.watch(attrs.dynamic, function (html) {
//     //         ele.html(html);
//     //         $compile(ele.contents())(scope);
//     //       });
//     //     }
//     //   };
//     // });

//     attachPublisher function () {
//       janus.attach(
//       {
//         plugin: "janus.plugin.videoroom",
//         success: function (pluginHandle) {
//           listFeeds.push(pluginHandle);
//           showButtonShare = true;
//           timeout(function () {
//             window.loading_screen.finish();
//           }, 3000);
//         },
//         error: function (error) {
//           Janus.error("  -- Error attaching plugin...", error);
//             // bootbox.alert("Error attaching plugin... " + error);
//           },
//           consentDialog: function (on) {
//             Janus.debug("Consent dialog should be " + (on ? "on" : "off") + " now");
//           },
//           webrtcState: function (on) {
//             Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
//           },
//           onmessage: function (msg, jsep) {
//             Janus.debug(" ::: Got a message (publisher) :::");
//             var event = msg["videoroom"];
//             if (event != undefined && event != null) {
//               if (event === "joined") {
//                 // myid = msg["id"];
//                 if (user.userType == "tutor") {
//                   listFeeds[0].createOffer(
//                   {
//                       media: { video: user.capture, audio: false, videoRecv: false },	// Screen sharing doesn't work with audio, and Publishers are sendonly
//                       success: function (jsep) {
//                         var publish = { "request": "configure", "audio": true, "video": true };
//                         listFeeds[0].send({ "message": publish, "jsep": jsep });
//                       },
//                       error: function (error) {
//                         Janus.error("WebRTC error:", error);
//                         // bootbox.alert("WebRTC error... " + JSON.stringify(error));
//                       }
//                     });
//                 } else {
//                   // We're just watching a session, any feed to attach to?
//                   if (msg["publishers"] !== undefined && msg["publishers"] !== null) {
//                     var list = msg["publishers"];
//                     for (var f in list) {
//                       var id = list[f]["id"];
//                       var display = list[f]["display"];
//                       Janus.debug("  >> [" + id + "] " + display);
//                       attachListener(id, display);
//                     }
//                   }
//                 }
//               } else if (event === "event") {
//                 // Any feed to attach to?
//                 if (user.userType == "student" && msg["publishers"] !== undefined && msg["publishers"] !== null) {
//                   var list = msg["publishers"];
//                   Janus.debug("Got a list of available publishers/feeds:");
//                   Janus.debug(list);
//                   for (var f in list) {
//                     var id = list[f]["id"];
//                     var display = list[f]["display"];
//                     Janus.debug("  >> [" + id + "] " + display);
//                     attachListener(id, display)
//                   }
//                 } else if (msg["leaving"] !== undefined && msg["leaving"] !== null) {
//                   // One of the publishers has gone away?
//                   var leaving = msg["leaving"];
//                   if (user.userType == "student" && msg["leaving"] == user.id_tutor) {
//                     // bootbox.alert("The screen sharing session is over, the publisher left", function () {
//                       window.location.reload();
//                     // });
//                   }
//                 } else if (msg["error"] !== undefined && msg["error"] !== null) {
//                   // bootbox.alert(msg["error"]);
//                 }
//               }
//             }
//             if (jsep !== undefined && jsep !== null) {
//               Janus.debug("Handling SDP as well...");
//               Janus.debug(jsep);
//               listFeeds[0].handleRemoteJsep({ jsep: jsep });
//             }
//           },
//           onlocalstream: function (stream) {
//             Janus.debug(" ::: Got a local stream :::");
//             listFeeds[0].rf_stream = URL.createObjectURL(stream);
//             listFeeds[0].rf_userType = user.userType;
//           },
//           onremotestream: function (stream) {
//             // The publisher stream is sendonly, we don't expect anything here
//           },
//           oncleanup: function () {
//             Janus.log(" ::: Got a cleanup notification :::");
//           }
//         });
// };

// attachListener function (id, display) {
//       // A new feed has been published, create a new plugin handle and attach to it as a listener
//       var remoteFeed = null;
//       janus.attach(
//       {
//         plugin: ConfigService.pluginVideoRoom,
//         success: function (pluginHandle) {
//           remoteFeed = pluginHandle;

//             // We wait for the plugin to send us an offer
//             var listen = { "request": "join", "room": user.presentation_id, "ptype": "listener", "feed": id };
//             remoteFeed.send({ "message": listen });
//           },
//           error: function (error) {
//             Janus.error("  -- Error attaching plugin...", error);
//             // bootbox.alert("Error attaching plugin... " + error);
//           },
//           onmessage: function (msg, jsep) {
//             Janus.debug(" ::: Got a message (listener) :::");
//             var event = msg["videoroom"];
//             if (event != undefined && event != null) {
//               if (event === "attached") {
//                 // Subscriber created and attached
//                 remoteFeed.rf_id = msg["id"];
//                 if (remoteFeed.rf_id === user.id_tutor) {
//                   remoteFeed.rf_userType = "tutor";
//                 } else {
//                   remoteFeed.rf_userType = "student";
//                 }
//                 $apply(function () {
//                   listFeeds.push(remoteFeed);
//                 });
//               } else {
//                 // What has just happened?
//               }
//             }
//             if (jsep !== undefined && jsep !== null) {
//               // Answer and attach
//               remoteFeed.createAnswer(
//               {
//                 jsep: jsep,
//                   media: { audioSend: false, videoSend: false },	// We want recvonly audio/video
//                   success: function (jsep) {
//                     Janus.debug("Got SDP!");
//                     Janus.debug(jsep);
//                     var body = { "request": "start", "room": user.presentation_id };
//                     remoteFeed.send({ "message": body, "jsep": jsep });
//                   },
//                   error: function (error) {
//                     Janus.error("WebRTC error:", error);
//                     // bootbox.alert("WebRTC error... " + error);
//                   }
//                 });
//             }
//           },
//           onlocalstream: function (stream) {
//             // The subscriber stream is recvonly, we don't expect anything here
//           },
//           onremotestream: function (stream) {
//             $apply(function () {
//               remoteFeed.rf_stream = URL.createObjectURL(stream);
//             });
//           },
//           oncleanup: function () {
//           }
//         });
//     };