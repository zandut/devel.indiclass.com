$(document).ready(function() {

	// Generate JSON here
    return {
      createRoomNumber: function (userData) {
        var videoConfig = JSON.parse($window.localStorage.getItem('videoConfig'));
        var jsonRequest = {
          "request": "create",
          "room": userData.roomNumber,
          "ptype": "publisher",
          "description": "",
          "publishers": 1, //hanya 1 orang sebagai publisher
          "bitrate": parseInt(videoConfig.maxBitRate),
          "audiocodec": videoConfig.audioCodec,
          "videocodec": videoConfig.videoCodec,
          "record": true,
          "rec_dir": "/home/videos/" + userData.roomNumber,
          "is_private": true
        };
        return jsonRequest;
      }, //=====================================================
      getListParticipants: function (userData) {
        var jsonRequest = {
          "request": "listparticipants",
          "room": userData.roomNumber,
          "description": "",
          "is_private": true
        };
        return jsonRequest;
      }, //=====================================================
      joinRoomNumber: function (userData) {
        var jsonRequest = {
          "request": "join",
          "room": userData.roomNumber,
          "ptype": "publisher",
          "display": userData.displayName,
          "id": userData.id,
          "record": true,
          "filename": "test"
        };
        return jsonRequest;
      },//=====================================================
      answerRequest: function () {
        var videoConfig = JSON.parse($window.localStorage.getItem('videoConfig'));
        var jsonRequest = {
            "audioRecv": true,
            "videoRecv": true,
            "audioSend": false,
            "videoSend": false,
            "video": videoConfig.defaultResolution,
            "data": false
        };
        return jsonRequest;
      },//=====================================================
      attachRequest: function (userData) {
        var jsonRequest = {
          "request": "start",
          "room": userData.roomNumber
        };
        return jsonRequest;
      },//=====================================================
      publishOwnFeed: function (deviceId) {
        var videoConfig = JSON.parse($window.localStorage.getItem('videoConfig'));
        var jsonRequest = {};
        if (deviceId) {
          jsonRequest = {
            "audioRecv": false,
            "videoRecv": false,
            "audioSend": true,
            "videoSend": true,
            "video":{"deviceId": deviceId, "width": videoConfig.width, "height": videoConfig.height},
            "data": false
          };
          console.warn("== CHECK ==" + deviceId);
        } else {
          jsonRequest = {
            "audioRecv": false,
            "videoRecv": false,
            "audioSend": true,
            "videoSend": true,
            "video": videoConfig.defaultResolution,
            "data": false
          };
        }

        return jsonRequest;
      },//=====================================================
      sendPublish: function (deviceId) {
        var videoConfig = JSON.parse($window.localStorage.getItem('videoConfig'));
        var jsonRequest = {};
        jsonRequest.request = "configure";
        jsonRequest.audio = true;
        jsonRequest.video = true;
        jsonRequest.bitrate = parseInt(videoConfig.maxBitRate);
        jsonRequest.audiocode = videoConfig.audioCodec;
        jsonRequest.videocodec = videoConfig.videoCodec;
        return jsonRequest;
      },//=====================================================
    };

    var videoCall = null;
    var vc_user1 = {};
    var vc_user2 = {};

    // ===========================================================================
    // ========================= VIDEO CALL FUNCTION DISINI ======================
    // ===========================================================================

    function attachVideoCall () {
      // Attach to echo test plugin
      janus.attach(
        {
          plugin: JanusConfigService.pluginVideoCall,
          success: function (pluginHandle) {
            videoCall = pluginHandle;
            vc_user1.id = "rm" + user.roomNumber + "vc" + user.id + "|" + randomString(10);

            vc_user1.username = user.displayName;
            registerUserForCall();

            $("#videoCallModal").modal("hide");
            $("#callingModal").modal("hide");
            Janus.log("Plugin attached! (" + videoCall.getPlugin() + ", id=" + videoCall.getId() + ")");
          },
          error: function (error) {
            addNewLog("Error VideoCall : " + error, "ERROR");
          },
          consentDialog: function (on) {
            Janus.debug("Consent dialog should be " + (on ? "on" : "off") + " now");
            // if (on) {
            //   // Darken screen and show hint
            //   $.blockUI({
            //     message: '<div><img src="up_arrow.png"/></div>',
            //     css: {
            //       border: 'none',
            //       padding: '15px',
            //       backgroundColor: 'transparent',
            //       color: '#aaa',
            //       top: '10px',
            //       left: (navigator.mozGetUserMedia ? '-100px' : '300px')
            //     }
            //   });
            // } else {
            //   // Restore screen
            //   $.unblockUI();
            // }
          },
          mediaState: function (medium, on) {
            Janus.log("Janus " + (on ? "started" : "stopped") + " receiving our " + medium);
          },
          webrtcState: function (on) {
            Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
            // $("#videoleft").parent().unblock();
          },
          onmessage: function (msg, jsep) {
            Janus.debug(" ::: Got a message :::");
            Janus.debug(JSON.stringify(msg));
            var result = msg["result"];
            if (result !== null && result !== undefined) {
              if (result["list"] !== undefined && result["list"] !== null) {
                var list = result["list"];
                Janus.debug("Got a list of registered peers:");
                Janus.debug(list);
                var isExist = false;
                for (var mp in list) {
                  if (vc_user2.id == list[mp].split("|", 1)) {
                    doCall(list[mp]);
                    isExist = true;
                    break;
                  }
                }

                if (!isExist) {
                  bootbox.alert(vc_user2.username + " is Offline");
                }
              } else if (result["event"] !== undefined && result["event"] !== null) {
                var event = result["event"];
                if (event === 'registered') {
                  vc_user1.id = result["username"];
                  Janus.log("Successfully registered as " + vc_user1.username + "!");
                } else if (event === 'calling') {
                  Janus.log("Waiting for the peer to answer...");
                  // TODO Any ringtone?
                } else if (event === 'incomingcall') {
                  Janus.log("Incoming call from " + result["username"] + "!");
                  vc_user2.id = result["username"];
                  // TODO Enable buttons to answer
                  // bootbox.confirm("Want to answer ?", function (result) {
                  // Telah selesai diproses, ubah status raise hand
                  //   destroyRaiseHand();

                  //   if (result) {
                  //     doAccept(jsep);
                  //   } else {
                  //     doHangup();
                  //   }
                  // });

                  doAccept(jsep);

                } else if (event === 'accepted') {
                  var peer = result["username"];
                  $("#callingModal").modal("hide");
                  $("#videoCallModal").modal("show");
                  if (peer === null || peer === undefined) {
                    Janus.log("Call started!");
                  } else {
                    Janus.log(peer + " accepted the call!");
                    vc_user2.id = peer;
                  }
                  // TODO Video call can start
                  if (jsep !== null && jsep !== undefined)
                    videoCall.handleRemoteJsep({ jsep: jsep });
                } else if (event === 'hangup') {
                  Janus.log("Call hung up by " + result["username"] + " (" + result["reason"] + ")!");
                  // TODO Reset status
                  videoCall.hangup();
                  clearVideoCall();
                }
              }
            } else {
              // FIXME Error?
              var error = msg["error"];
              addNewLog("Error VideoCall : " + error, "ERROR");
              // TODO Reset status
              videoCall.hangup();
            }
          },
          onlocalstream: function (stream) {
            Janus.debug(" ::: Got a local stream :::");
            if ($('#videoCallLocal').length === 0) {
              $('#videoBoxLocal').append('<video id="videoCallLocal" autoplay></video>');
            }

            $("#videoCallLocal").bind("playing", function () {
              if (webrtcDetectedBrowser == "firefox") {
                // Firefox Stable has a bug: width and height are not immediately available after a playing
                setTimeout(function () {
                  var width = $("#videoCallLocal").get(0).videoWidth;
                  var height = $("#videoCallLocal").get(0).videoHeight;
                }, 2000);
              }
            });
            attachMediaStream($('#videoCallLocal').get(0), stream);
          },
          onremotestream: function (stream) {
            Janus.debug(" ::: Got a remote stream :::");
            if ($('#videoCallRemote').length === 0) {
              $('#videoBoxRemote').append('<video id="videoCallRemote" autoplay></video>');
            }

            $("#videoCallRemote").bind("playing", function () {
              if (webrtcDetectedBrowser == "firefox") {
                // Firefox Stable has a bug: width and height are not immediately available after a playing
                setTimeout(function () {
                  var width = $("#videoCallRemote").get(0).videoWidth;
                  var height = $("#videoCallRemote").get(0).videoHeight;
                }, 2000);
              }
            });
            attachMediaStream($('#videoCallRemote').get(0), stream);
          },
          oncleanup: function () {
            Janus.log(" ::: Got a cleanup notification :::");
            $('#videoCallLocal').remove();
            $('#videoCallRemote').remove();
            // clearVideoCall();
          }
        });
    };

    function checkBeforeCall (id, name) {
      vc_user2.id = "rm" + user.roomNumber + "vc" + id;
      vc_user2.username = name;
      videoCall.send({ "message": { "request": "list" } });
    };

    function getJsonOffer () {
      var videoConfig = JSON.parse($window.localStorage.getItem('videoConfig'));
      var jsonRequest = {};

      if (currentDeviceId) {
        jsonRequest = {
          "video": { "deviceId": currentDeviceId, "width": videoConfig.width, "height": videoConfig.height },
          "data": false
        };
      } else {
        jsonRequest = {
          "video": videoConfig.defaultResolution,
          "data": false
        };
      };

      return jsonRequest;
    };

    function  doCall(x) {
      var jsonOffer = getJsonOffer();
      // Call this user
      videoCall.createOffer(
        {
          // By default, it's sendrecv for audio and video...
          media: jsonOffer, // ... let's negotiate data channels as well
          success: function (jsep) {
            Janus.debug("Got SDP!");
            Janus.debug(jsep);
            // disableVideoRoomTutor();
            $("#callingModal").modal("show");
            $("#messageCalling").html("Memanggil " + name + " . . . ");
            var body = { "request": "call", "username": x };
            videoCall.send({ "message": body, "jsep": jsep });
          },
          error: function (error) {
            addNewLog("Error VideoCall : " + error, "ERROR");
          }
        });
    };

    function registerUserForCall() {
      var register = { "request": "register", "username": vc_user1.id, "token": user.token };
      videoCall.send({ "message": register });
    };

    function doAccept(jsep) {
      var jsonAccept = getJsonOffer();

      videoCall.createAnswer(
        {
          jsep: jsep,
          // No media provided: by default, it's sendrecv for audio and video
          media: jsonAccept,  // Let's negotiate data channels as well
          success: function (jsep) {
            Janus.debug("Got SDP!");
            Janus.debug(jsep);
            var body = { "request": "accept" };
            videoCall.send({ "message": body, "jsep": jsep });
          },
          error: function (error) {
            addNewLog("Error AcceptCall : " + JSON.stringify(error), "ERROR");
            doHangup();
          }
        });
    };

    function doHangup() {
      var hangup = { "request": "hangup" };
      videoCall.send({ "message": hangup });
      videoCall.hangup();
      videoCall.detach();
      clearVideoCall();
    };

    function clearVideoCall() {
      // videoCall = null;      
      $('#videoCallLocal').remove();
      $('#videoCallRemote').remove();
      vc_user1 = {};
      vc_user2 = {};
      enableVideoRoomTutor();
      attachVideoCall();

      if (user.userType == "student") {
        destroyRaiseHand();
      }
    };

	
}

