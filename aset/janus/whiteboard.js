var session = null;
var parameter = true;
var token = null;
var idtutor = null;
var iduser  = null;
var displayname = null;
var tutorname = null;
var firstname = null;
var link = "https://classmiles.com/";
var janus = null;
var classid = null;

$(document).ready(function(){

	function getURLParameter(name) {
	  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
	}

	session = window.localStorage.getItem('session');

	if (session == null) {
		session = getURLParameter("access_session");	
		console.warn('Test Token Null = ' + session);
	}

	setTimeout(function(){
		$("#loader").css('display', 'none');
		getpollingdata(parameter);
	},3000);

	

	// ============= TUTOR LOGIN ==============================
	function getpollingdata(firsttime)
	{
		// alert("E");
		$.ajax({
			url: 'https://classmiles.com/Rest/get_token',
			type: 'POST',
			data: {
				session: session
			},
			success: function(data)
			{			
				if (data['error']['code'] == '200' && data['response']['token'] != null) {
					token = data['response']['token'];
					classid = parseInt(data['response']['class_id']);
					idtutor = parseInt(data['response']['id_tutor']);
	                iduser = parseInt(data['response']['id_user']);
	                janus = data['response']['janus'];
	                console.warn("ADA NIH GAN");
	                console.warn(janus);
	                setTimeout(function(){
						$("#logoclassmiles").css('display','block');
					},4500);
	                $("#whiteboardonly").attr("src", "https://"+janus+"/wb/"+classid+"/session/"+session);
				}
				else if (data['error']['code'] == '504') {
		        	// console.warn("MASUK ELSE IF 504");
		        	// Android();
					// setInterval(function(){
					// 	getpollingdata(parameter);
					// },2000);	
					// timer;
		        }
		        else if (data['error']['code'] == '404') {
		        	// console.warn("MASUK ELSE IF 404");
					// setInterval(function(){
					// 	getpollingdata(parameter);
					// },2000);	
					// clearInterval(timer);
		        	Android();
		        }   
		        else
		        {
		        	Android();
		        	// console.warn("SAATNYA DESTROY TOKEN");
		        	destroyToken();
		        	// clearInterval(timer);
		        }
			}
		});
	}

	function Android(isi)
	{
		if (isi) {
			AndroidFunction.ErrorMessage(isi);
		} else {
			AndroidFunction.FinishClass();
		}
	}

});